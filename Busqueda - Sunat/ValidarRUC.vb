﻿Public Class ValidarRUC

    Public Shared Function validarRuc(ByVal ruc As String) As Boolean

        If VAL_RUC(ruc) = False Then
            Return False
        End If
        Return True

    End Function

    'Ejm:
    'RUC = 10254824220
    'FACTOR = 5432765432

    'Se separa los 10 primeros digitos de la izquierda y se hace un calculo inividual
    '1 * 5 =5
    '0 * 4 = 0
    '2 * 3 = 6
    '5 * 2 = 10
    '4 * 7 = 28
    '8 * 6 = 48
    '2 * 5 = 10
    '4 * 4 = 16
    '2 * 3 = 6
    '2 * 2 = 4

    'Se suma el resultado de todas las multiplicaciones
    'SUMA = 133

    'Se calcula el residuo de la division por 11
    '133/ 11 = 1
    'RESIDUO = 1

    'Se resta 11 menos el residuo
    '11 - 1
    'RESTA = 10

    'digito de chequeo = RESTA
    'si resta = 10 entonces digito de cheque = 0
    'si resta = 11 entonces digito de cheque = 1

    'RUC 10254824220 es valido por que su digito numero 11 es 0 y el digito de chekeo es 0.

    Private Shared Function LeftC(ByVal str As String, ByVal Length As Integer) As String

        Dim LenT As Integer = str.Length
        If LenT <= Length Then
            Return str
        Else
            Return str.Substring(0, Length)
        End If

    End Function

    Private Shared Function RightC(ByVal str As String, ByVal Length As Integer) As String

        Dim LenT As Integer = str.Length
        If LenT <= Length Then
            Return str
        Else
            Return str.Substring(LenT - Length)
        End If

    End Function

    Private Shared Function VAL_RUC(ByVal ruc As String) As Boolean
        Dim FACTOR() As Integer = {5, 4, 3, 2, 7, 6, 5, 4, 3, 2}
        Dim suma As Integer = 0
        'ERROR SI NO ES NUMERO
        If Not IsNumeric(ruc) Then
            Return False
        End If

        'ERROR SI NO CUMPLE LOS 11 DIGITOS
        If ruc.Length <> 11 Then
            Return False
        End If

        'ERROR SI NO TIENE LOS 2 PRIMEROS DIGITOS
        '10 persona natural.
        '20 persona juridica.
        '17 o 15 extranjeros

        Dim VAL_DIGIT() As String = {"20", "17", "15", "10"}
        Dim DIGIT As String = LeftC(ruc, 2)
        Array.Sort(VAL_DIGIT)
        If Array.BinarySearch((VAL_DIGIT), DIGIT) < 0 Then
            Return False
        End If

        For I = 0 To ruc.Length - 2
            suma += Integer.Parse(ruc.Substring(I, 1)) * FACTOR(I)
        Next

        Dim residuo As Integer = suma Mod 11
        Dim resta As Integer = 11 - residuo

        Dim digChk As Integer
        If resta = 10 Then
            digChk = 0
        ElseIf resta = 11 Then
            digChk = 1
        Else
            digChk = resta
        End If
        'messagebox.Show(digChk.ToString)
        If digChk = RightC(ruc, 1) Then
            Return True
        Else
            Return False
        End If

    End Function

    'DEBE RECIBIR UN STRING DE 10 DIGITOS
    'POR EJEMPLO 1040454545

    Public Shared Function getDigito(ByVal nro As String) As Integer

        Dim FACTOR() As Integer = {5, 4, 3, 2, 7, 6, 5, 4, 3, 2}
        Dim suma As Integer = 0
        For I = 0 To nro.Length - 1
            suma += Integer.Parse(nro.Substring(I, 1)) * FACTOR(I)
        Next

        Dim residuo As Integer = suma Mod 11
        Dim resta As Integer = 11 - residuo

        Dim digChk As Integer
        If resta = 10 Then
            digChk = 0
        ElseIf resta = 11 Then
            digChk = 1
        Else
            digChk = resta
        End If
        'messagebox.Show(digChk.ToString)
        Return digChk

    End Function

End Class
