﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms
Imports Tesseract
Imports AForge
Imports AForge.Imaging
Imports AForge.Imaging.Filters
Imports AForge.Imaging.Textures
Imports Microsoft.VisualBasic.PowerPacks

Public Class Retornar_Sunat
    Dim red As New IntRange(0, 255)
    Dim green As New IntRange(0, 255)
    Dim blue As New IntRange(0, 255)
    Dim MyInfoSunat As Sunat
    Dim Texto As String = String.Empty
    Dim pictureCapcha As System.Drawing.Image
    Dim captchaTxt As String
    Dim Contador As Integer = 0
    Public Function Retornar_Sunat(ByVal RUC As String) As Contable.Entidad_Entidad
        Dim Datos_Sunat As New Contable.Entidad_Entidad

        Texto = Nothing
        Try
            CargarImagenSunat()
        Catch ex As Exception

        End Try
        Try
            MyInfoSunat.GetInfo(RUC.Trim, captchaTxt.Trim)
            Select Case MyInfoSunat.GetResul
                Case Sunat.Resul.Ok
                    'limpiarSunat()
                    Datos_Sunat.Ent_RUC_DNI = MyInfoSunat.Ruc.Trim
                    Datos_Sunat.Ent_Domicilio_Fiscal = MyInfoSunat.Direcion.Trim
                    Datos_Sunat.Ent_Razon_Social_Nombre = MyInfoSunat.RazonSocial.Trim
                    Datos_Sunat.EstadoContr = MyInfoSunat.EstadoContr.Trim
                    Datos_Sunat.Ent_Telefono = MyInfoSunat.Telefono.Trim
                    'Datos_Sunat.TipoContribuyente = MyInfoSunat.TipoContr.Trim
                    Datos_Sunat.Estado_De_Consulta = True

                    Exit Select

                    MessageBox.Show("No Existe RUC")
                    Datos_Sunat.Estado_No_Existe = True

                Case Sunat.Resul.ErrorCapcha

                    Retornar_Sunat(RUC)

                Case Else

                    Exit Select
            End Select

        Catch ex As Exception
            Contador = Contador + 1
            If Contador <= 2 Then
                Retornar_Sunat(RUC)
            End If
        End Try
        Return Datos_Sunat
    End Function
    'Private Sub Ciudad(Direccion As String)
    '    Dim array As [String]() = Direccion.Split("-"c)
    '    If array.Length > 1 Then
    '        Dim a As Integer = array.Length
    '        Dim DirTemp As [String] = array(a - 3).Trim()
    '        DirTemp = DirTemp.TrimEnd(" "c)
    '        Dim ArrayDir As [String]() = DirTemp.Split(" "c)
    '        Dim i As Integer = ArrayDir.Length
    '        txtdepa.Text = ArrayDir(i - 1).Trim()
    '        txtPrv.Text = array(a - 2).Trim()
    '        txtDist.Text = array(a - 1).Trim()
    '    End If
    'End Sub

    Private Sub CargarImagenSunat()
        Try
            If MyInfoSunat Is Nothing Then
                MyInfoSunat = New Sunat()
            End If
            pictureCapcha = MyInfoSunat.GetCapcha
            LeerCaptchaSunat()
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub LeerCaptchaSunat()

        Dim engine As TesseractEngine = New TesseractEngine("./tessdata", "eng", EngineMode.Default)
        Dim image As System.Drawing.Bitmap = New System.Drawing.Bitmap(pictureCapcha)
        Dim pix As Pix = PixConverter.ToPix(image)
        Dim page As Page = engine.Process(pix)
        Dim Porcentaje As String = String.Format("{0:P}", page.GetMeanConfidence())
        Dim CaptchaTexto As String = page.GetText()
        Dim eliminarChars As Char() = {ControlChars.Lf, " "c}
        CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars)
        CaptchaTexto = CaptchaTexto.Replace(" ", String.Empty)
        CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", String.Empty)
        If CaptchaTexto <> String.Empty And CaptchaTexto.Length = 4 Then
            captchaTxt = CaptchaTexto.ToUpper().Trim
        Else
            Try
                CargarImagenSunat()
            Catch ex As Exception

            End Try

        End If
    End Sub
End Class
