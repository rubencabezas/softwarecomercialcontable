﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("BusquedaSunat")> 
<Assembly: AssemblyDescription("Busqueda de RUC Sunat")> 
<Assembly: AssemblyCompany("DEC Business Advisory")> 
<Assembly: AssemblyProduct("Busqueda de RUC Sunat")> 
<Assembly: AssemblyCopyright("Copyright by David Contreras ©  2016")> 
<Assembly: AssemblyTrademark("Sin Marca")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("6afb4e3d-2834-417c-9a4f-646af92b5750")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
