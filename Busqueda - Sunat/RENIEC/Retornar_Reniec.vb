﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms
Imports Tesseract
Imports AForge
Imports AForge.Imaging
Imports AForge.Imaging.Filters
Imports AForge.Imaging.Textures
Imports System.Math
Public Class Retornar_Reniec
    Dim red As New IntRange(0, 255)
    Dim green As New IntRange(0, 255)
    Dim blue As New IntRange(0, 255)
    Dim MyInfoReniec As Reniec
    Dim pictureCapcha As System.Drawing.Image
    Dim Texto As String = String.Empty
    Dim Contador As Integer = 0
    Private Sub AplicacionFiltros()
        Dim bmp As New Bitmap(pictureCapcha)
        FiltroInvertir(bmp)
        ColorFiltros()
        Dim bmp1 As New Bitmap(pictureCapcha)
        FiltroInvertir(bmp1)
        Dim bmp2 As New Bitmap(pictureCapcha)
        FiltroSharpen(bmp2)
    End Sub
    Private Sub FiltroInvertir(bmp As Bitmap)
        Dim Filtro As IFilter = New Invert()
        Dim XImage As Bitmap = Filtro.Apply(bmp)
        pictureCapcha = XImage
    End Sub
    Private Sub ColorFiltros()
        red.Min = System.Math.Min(red.Max, Byte.Parse("229"))
        red.Max = System.Math.Max(red.Min, Byte.Parse("255"))
        green.Min = System.Math.Min(green.Max, Byte.Parse("0"))
        green.Max = System.Math.Max(green.Min, Byte.Parse("255"))
        blue.Min = System.Math.Min(blue.Max, Byte.Parse("0"))
        blue.Max = System.Math.Max(blue.Min, Byte.Parse("130"))
        ActualizarFiltro()
    End Sub
    Private Sub ActualizarFiltro()
        Dim FiltroColor As New ColorFiltering()
        FiltroColor.Red = red
        FiltroColor.Green = green
        FiltroColor.Blue = blue
        Dim Filtro As IFilter = FiltroColor
        Dim bmp As New Bitmap(pictureCapcha)
        Dim XImage As Bitmap = Filtro.Apply(bmp)
        pictureCapcha = XImage
    End Sub
    Private Sub FiltroSharpen(bmp As Bitmap)
        Dim Filtro As IFilter = New Sharpen()
        Dim XImage As Bitmap = Filtro.Apply(bmp)
        pictureCapcha = XImage
    End Sub
    Private Sub CargarImagenReniec()
        Try
            If MyInfoReniec Is Nothing Then
                MyInfoReniec = New Reniec()
            End If
            pictureCapcha = MyInfoReniec.GetCapcha
            AplicacionFiltros()
            LeerCaptchaReniec()
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub LeerCaptchaReniec()
        Using engine = New TesseractEngine("./tessdata", "eng", EngineMode.[Default])
            Using image = New System.Drawing.Bitmap(pictureCapcha)
                Using pix = PixConverter.ToPix(image)
                    Using page = engine.Process(pix)
                        Dim Porcentaje = [String].Format("{0:P}", page.GetMeanConfidence())
                        Dim CaptchaTexto As String = page.GetText()
                        Dim eliminarChars As Char() = {ControlChars.Lf, " "c}
                        CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars)
                        CaptchaTexto = CaptchaTexto.Replace(" ", String.Empty)
                        CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z0-9]+", String.Empty)
                        If CaptchaTexto <> String.Empty And CaptchaTexto.Length = 4 Then
                            Texto = CaptchaTexto.ToUpper()
                        Else
                            Try
                                CargarImagenReniec()
                            Catch ex As Exception

                            End Try

                        End If
                    End Using
                End Using

            End Using
        End Using

    End Sub

    Public Function Retornar_Reniec(ByVal DNI As String) As Contable.Entidad_Entidad
        Dim Datos_Sunat As New Contable.Entidad_Entidad
        Try
            Texto = Nothing
            CargarImagenReniec()
            'AplicacionFiltros()
            'LeerCaptchaReniec()
            MyInfoReniec.GetInfo(DNI, Texto)
            Select Case MyInfoReniec.GetResul
                Case Reniec.Resul.Ok
                    Datos_Sunat.Ent_RUC_DNI = MyInfoReniec.Dni
                    Datos_Sunat.Ent_Razon_Social_Nombre = MyInfoReniec.Nombres
                    Datos_Sunat.Ent_Ape_Paterno = MyInfoReniec.ApePaterno
                    Datos_Sunat.Ent_Ape_Materno = MyInfoReniec.ApeMaterno
                    Datos_Sunat.Estado_De_Consulta = True
                    Exit Select
                Case Reniec.Resul.NoResul
                    '    limpiarReniec()
                    Datos_Sunat.Estado_No_Existe = True
                    MessageBox.Show("No Existe DNI", "RENIEC")

                    Exit Select
                Case Reniec.Resul.ErrorCapcha
                    'Retornar_Reniec(DNI)
                    'MessageBox.Show("Ingrese imagen correctamente")
                    Exit Select
                Case Else
                    'Retornar_Reniec(DNI)
                    'MessageBox.Show("Error Desconocido")
                    Exit Select
            End Select
            'CargarImagenReniec()
        Catch ex As Exception
            Contador = Contador + 1
            If Contador <= 2 Then
                Retornar_Reniec(DNI)
            End If
        End Try
        Return Datos_Sunat

    End Function

End Class
