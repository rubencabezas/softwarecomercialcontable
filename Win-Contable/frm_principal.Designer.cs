﻿namespace Contable
{
    partial class frm_principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.btncomprobantes = new DevExpress.XtraBars.BarButtonItem();
            this.btnmoneda = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.btnTipoImpuesto = new DevExpress.XtraBars.BarButtonItem();
            this.btnAsignacionImp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.btnempresa = new DevExpress.XtraBars.BarButtonItem();
            this.btnejercicio = new DevExpress.XtraBars.BarButtonItem();
            this.btnlibroscontables = new DevExpress.XtraBars.BarButtonItem();
            this.btnelemento = new DevExpress.XtraBars.BarButtonItem();
            this.btnestructura = new DevExpress.XtraBars.BarButtonItem();
            this.btnplangeneral = new DevExpress.XtraBars.BarButtonItem();
            this.btnplanempresarial = new DevExpress.XtraBars.BarButtonItem();
            this.btnventas = new DevExpress.XtraBars.BarButtonItem();
            this.btncompras = new DevExpress.XtraBars.BarButtonItem();
            this.btninicial = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.btnentidad = new DevExpress.XtraBars.BarButtonItem();
            this.btnunidad = new DevExpress.XtraBars.BarButtonItem();
            this.btncentrocostogasto = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.btntipocambio = new DevExpress.XtraBars.BarButtonItem();
            this.btngrupo = new DevExpress.XtraBars.BarButtonItem();
            this.btnfamilia = new DevExpress.XtraBars.BarButtonItem();
            this.btncatalogo = new DevExpress.XtraBars.BarButtonItem();
            this.btnmarca = new DevExpress.XtraBars.BarButtonItem();
            this.btnunidadmedida = new DevExpress.XtraBars.BarButtonItem();
            this.btndiario = new DevExpress.XtraBars.BarButtonItem();
            this.btninventario = new DevExpress.XtraBars.BarButtonItem();
            this.btnalmacen = new DevExpress.XtraBars.BarButtonItem();
            this.btntipooperacion = new DevExpress.XtraBars.BarButtonItem();
            this.btntesoreria = new DevExpress.XtraBars.BarButtonItem();
            this.btnbalancecomprobacion = new DevExpress.XtraBars.BarButtonItem();
            this.btnregistrocompras = new DevExpress.XtraBars.BarButtonItem();
            this.btnregistroventa = new DevExpress.XtraBars.BarButtonItem();
            this.btnorden = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.txtempresa = new DevExpress.XtraBars.BarHeaderItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.btngruposervicios = new DevExpress.XtraBars.BarButtonItem();
            this.btnfamiliaservicios = new DevExpress.XtraBars.BarButtonItem();
            this.btncatalogoservicios = new DevExpress.XtraBars.BarButtonItem();
            this.Btnventaa = new DevExpress.XtraBars.BarButtonItem();
            this.btndiarioo = new DevExpress.XtraBars.BarButtonItem();
            this.btnanalisis = new DevExpress.XtraBars.BarButtonItem();
            this.btnrequerimientos = new DevExpress.XtraBars.BarButtonItem();
            this.btncargo = new DevExpress.XtraBars.BarButtonItem();
            this.btnarea = new DevExpress.XtraBars.BarButtonItem();
            this.btnlibrodiario = new DevExpress.XtraBars.BarButtonItem();
            this.btnlibromayor = new DevExpress.XtraBars.BarButtonItem();
            this.btnmediopago = new DevExpress.XtraBars.BarButtonItem();
            this.btncuentacorriente = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.btningresos = new DevExpress.XtraBars.BarButtonItem();
            this.btnegresos = new DevExpress.XtraBars.BarButtonItem();
            this.btnentidadfinanciera = new DevExpress.XtraBars.BarButtonItem();
            this.btnseriesdocumento = new DevExpress.XtraBars.BarButtonItem();
            this.btnaperturach = new DevExpress.XtraBars.BarButtonItem();
            this.btnrendicion = new DevExpress.XtraBars.BarButtonItem();
            this.btncajachica = new DevExpress.XtraBars.BarButtonItem();
            this.btnreembolso = new DevExpress.XtraBars.BarButtonItem();
            this.btncierre = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.btnreplicacion = new DevExpress.XtraBars.BarButtonItem();
            this.btnigv = new DevExpress.XtraBars.BarButtonItem();
            this.btnestadosfianacierostributarios = new DevExpress.XtraBars.BarButtonItem();
            this.btnestadofinansmv = new DevExpress.XtraBars.BarButtonItem();
            this.btnLibroSimplificado = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.btnesatdosituacionfinanciera = new DevExpress.XtraBars.BarButtonItem();
            this.btnflujoefectivo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btncambiopatrimonionetosmv = new DevExpress.XtraBars.BarButtonItem();
            this.btnestadodflujoefectivo = new DevExpress.XtraBars.BarButtonItem();
            this.btnestadocambiopatrimonioneto = new DevExpress.XtraBars.BarButtonItem();
            this.btnesatdofinancierosunat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup16 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage6 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup20 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup21 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup18 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup19 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup15 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup14 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage8 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup24 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup27 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup25 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup26 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup17 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup22 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup28 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup31 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage7 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup23 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboanio = new System.Windows.Forms.ComboBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboperiodo = new System.Windows.Forms.ComboBox();
            this.timerTipodeCambio = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Controller = this.barAndDockingController1;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonControl1.SearchEditItem,
            this.btncomprobantes,
            this.btnmoneda,
            this.barButtonItem3,
            this.btnTipoImpuesto,
            this.btnAsignacionImp,
            this.barButtonItem6,
            this.barButtonItem7,
            this.btnempresa,
            this.btnejercicio,
            this.btnlibroscontables,
            this.btnelemento,
            this.btnestructura,
            this.btnplangeneral,
            this.btnplanempresarial,
            this.btnventas,
            this.btncompras,
            this.btninicial,
            this.barButtonItem13,
            this.btnentidad,
            this.btnunidad,
            this.btncentrocostogasto,
            this.barButtonItem4,
            this.btntipocambio,
            this.btngrupo,
            this.btnfamilia,
            this.btncatalogo,
            this.btnmarca,
            this.btnunidadmedida,
            this.btndiario,
            this.btninventario,
            this.btnalmacen,
            this.btntipooperacion,
            this.btntesoreria,
            this.btnbalancecomprobacion,
            this.btnregistrocompras,
            this.btnregistroventa,
            this.btnorden,
            this.barEditItem1,
            this.barEditItem2,
            this.barEditItem3,
            this.txtempresa,
            this.barStaticItem1,
            this.btngruposervicios,
            this.btnfamiliaservicios,
            this.btncatalogoservicios,
            this.Btnventaa,
            this.btndiarioo,
            this.btnanalisis,
            this.btnrequerimientos,
            this.btncargo,
            this.btnarea,
            this.btnlibrodiario,
            this.btnlibromayor,
            this.btnmediopago,
            this.btncuentacorriente,
            this.barButtonItem5,
            this.btningresos,
            this.btnegresos,
            this.btnentidadfinanciera,
            this.btnseriesdocumento,
            this.btnaperturach,
            this.btnrendicion,
            this.btncajachica,
            this.btnreembolso,
            this.btncierre,
            this.barButtonItem2,
            this.barButtonItem8,
            this.btnreplicacion,
            this.btnigv,
            this.btnestadosfianacierostributarios,
            this.btnestadofinansmv,
            this.btnLibroSimplificado,
            this.barButtonItem1,
            this.barButtonItem9,
            this.barButtonItem10,
            this.barButtonItem12,
            this.barButtonItem14,
            this.btnesatdosituacionfinanciera,
            this.btnflujoefectivo,
            this.barButtonItem11,
            this.barButtonItem16,
            this.barSubItem1,
            this.btncambiopatrimonionetosmv,
            this.btnestadodflujoefectivo,
            this.btnestadocambiopatrimonioneto,
            this.btnesatdofinancierosunat,
            this.barButtonItem15});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 105;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage6,
            this.ribbonPage2,
            this.ribbonPage5,
            this.ribbonPage4,
            this.ribbonPage8,
            this.ribbonPage3,
            this.ribbonPage7});
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.barEditItem2);
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.txtempresa);
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.barEditItem3);
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemTextEdit1});
            this.ribbonControl1.Size = new System.Drawing.Size(1384, 150);
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Above;
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            // 
            // btncomprobantes
            // 
            this.btncomprobantes.Caption = "Comprobantes";
            this.btncomprobantes.Id = 1;
            this.btncomprobantes.ImageOptions.Image = global::Contable.Properties.Resources.Bill_32px;
            this.btncomprobantes.Name = "btncomprobantes";
            this.btncomprobantes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncomprobantes_ItemClick);
            // 
            // btnmoneda
            // 
            this.btnmoneda.Caption = "Monedas";
            this.btnmoneda.Id = 2;
            this.btnmoneda.ImageOptions.Image = global::Contable.Properties.Resources.Coins_24px;
            this.btnmoneda.Name = "btnmoneda";
            this.btnmoneda.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Medios de pago";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.ImageOptions.LargeImage = global::Contable.Properties.Resources.PaymentMethod_32px;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // btnTipoImpuesto
            // 
            this.btnTipoImpuesto.Caption = "Tipo de impuesto";
            this.btnTipoImpuesto.Id = 4;
            this.btnTipoImpuesto.ImageOptions.Image = global::Contable.Properties.Resources.Donate_32px;
            this.btnTipoImpuesto.Name = "btnTipoImpuesto";
            this.btnTipoImpuesto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTipoImpuesto_ItemClick);
            // 
            // btnAsignacionImp
            // 
            this.btnAsignacionImp.Caption = "Asignacion de Impuestos";
            this.btnAsignacionImp.Id = 5;
            this.btnAsignacionImp.ImageOptions.Image = global::Contable.Properties.Resources.Todo_List_32px;
            this.btnAsignacionImp.Name = "btnAsignacionImp";
            this.btnAsignacionImp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAsignacionImp_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "barButtonItem6";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "barButtonItem7";
            this.barButtonItem7.Id = 7;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // btnempresa
            // 
            this.btnempresa.Caption = "Empresas";
            this.btnempresa.Id = 8;
            this.btnempresa.ImageOptions.LargeImage = global::Contable.Properties.Resources.Factory_32px;
            this.btnempresa.Name = "btnempresa";
            this.btnempresa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // btnejercicio
            // 
            this.btnejercicio.Caption = "Ejercicios";
            this.btnejercicio.Id = 9;
            this.btnejercicio.ImageOptions.LargeImage = global::Contable.Properties.Resources.Calendar_32px;
            this.btnejercicio.Name = "btnejercicio";
            this.btnejercicio.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnejercicio_ItemClick);
            // 
            // btnlibroscontables
            // 
            this.btnlibroscontables.Caption = "Libros Contables";
            this.btnlibroscontables.Id = 10;
            this.btnlibroscontables.ImageOptions.Image = global::Contable.Properties.Resources.Books_32px;
            this.btnlibroscontables.Name = "btnlibroscontables";
            this.btnlibroscontables.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlibroscontables_ItemClick);
            // 
            // btnelemento
            // 
            this.btnelemento.Caption = "Elemento";
            this.btnelemento.Id = 11;
            this.btnelemento.ImageOptions.Image = global::Contable.Properties.Resources.Elemento;
            this.btnelemento.Name = "btnelemento";
            this.btnelemento.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnelemento_ItemClick);
            // 
            // btnestructura
            // 
            this.btnestructura.Caption = "Estructura";
            this.btnestructura.Id = 12;
            this.btnestructura.ImageOptions.Image = global::Contable.Properties.Resources.Estructura;
            this.btnestructura.Name = "btnestructura";
            this.btnestructura.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnestructura_ItemClick);
            // 
            // btnplangeneral
            // 
            this.btnplangeneral.Caption = "Plan Contable General";
            this.btnplangeneral.Id = 13;
            this.btnplangeneral.ImageOptions.Image = global::Contable.Properties.Resources.planGeneral;
            this.btnplangeneral.Name = "btnplangeneral";
            this.btnplangeneral.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnplangeneral_ItemClick);
            // 
            // btnplanempresarial
            // 
            this.btnplanempresarial.Caption = "Plan Contable Empresarial";
            this.btnplanempresarial.Id = 14;
            this.btnplanempresarial.ImageOptions.Image = global::Contable.Properties.Resources.PlanEmpresarial;
            this.btnplanempresarial.ImageOptions.LargeImage = global::Contable.Properties.Resources.PlanEmpresarial;
            this.btnplanempresarial.Name = "btnplanempresarial";
            this.btnplanempresarial.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnplanempresarial_ItemClick);
            // 
            // btnventas
            // 
            this.btnventas.Caption = "Ventas";
            this.btnventas.Id = 15;
            this.btnventas.ImageOptions.LargeImage = global::Contable.Properties.Resources.Move_by_Trolley_32px;
            this.btnventas.Name = "btnventas";
            this.btnventas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnventas_ItemClick);
            // 
            // btncompras
            // 
            this.btncompras.Caption = "Compras";
            this.btncompras.Id = 16;
            this.btncompras.ImageOptions.LargeImage = global::Contable.Properties.Resources.Paid_32px;
            this.btncompras.Name = "btncompras";
            this.btncompras.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncompras_ItemClick);
            // 
            // btninicial
            // 
            this.btninicial.Caption = "Inicio";
            this.btninicial.Id = 19;
            this.btninicial.ImageOptions.Image = global::Contable.Properties.Resources.Inicio;
            this.btninicial.ImageOptions.LargeImage = global::Contable.Properties.Resources.Inicio;
            this.btninicial.Name = "btninicial";
            this.btninicial.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btninicial_ItemClick);
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "barButtonItem13";
            this.barButtonItem13.Id = 22;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // btnentidad
            // 
            this.btnentidad.Caption = "Entidades";
            this.btnentidad.Id = 27;
            this.btnentidad.ImageOptions.LargeImage = global::Contable.Properties.Resources.Entidades;
            this.btnentidad.Name = "btnentidad";
            this.btnentidad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnentidad_ItemClick);
            // 
            // btnunidad
            // 
            this.btnunidad.Caption = "Unidad";
            this.btnunidad.Id = 28;
            this.btnunidad.ImageOptions.Image = global::Contable.Properties.Resources.uni;
            this.btnunidad.Name = "btnunidad";
            this.btnunidad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnunidad_ItemClick);
            // 
            // btncentrocostogasto
            // 
            this.btncentrocostogasto.Caption = "Centro Costo - Gasto";
            this.btncentrocostogasto.Id = 29;
            this.btncentrocostogasto.ImageOptions.Image = global::Contable.Properties.Resources.centro_de_costo;
            this.btncentrocostogasto.Name = "btncentrocostogasto";
            this.btncentrocostogasto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncentrocostogasto_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 30;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // btntipocambio
            // 
            this.btntipocambio.Caption = "Tipo de Cambio";
            this.btntipocambio.Id = 31;
            this.btntipocambio.Name = "btntipocambio";
            this.btntipocambio.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btntipocambio_ItemClick);
            // 
            // btngrupo
            // 
            this.btngrupo.Caption = "Grupo";
            this.btngrupo.Id = 35;
            this.btngrupo.ImageOptions.Image = global::Contable.Properties.Resources.Group;
            this.btngrupo.Name = "btngrupo";
            this.btngrupo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btngrupo_ItemClick);
            // 
            // btnfamilia
            // 
            this.btnfamilia.Caption = "Familia";
            this.btnfamilia.Id = 36;
            this.btnfamilia.ImageOptions.Image = global::Contable.Properties.Resources.Familia;
            this.btnfamilia.Name = "btnfamilia";
            this.btnfamilia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnfamilia_ItemClick);
            // 
            // btncatalogo
            // 
            this.btncatalogo.Caption = "Catalogo";
            this.btncatalogo.Id = 37;
            this.btncatalogo.ImageOptions.Image = global::Contable.Properties.Resources.Catalogo_;
            this.btncatalogo.Name = "btncatalogo";
            this.btncatalogo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncatalogo_ItemClick);
            // 
            // btnmarca
            // 
            this.btnmarca.Caption = "Marca";
            this.btnmarca.Id = 38;
            this.btnmarca.ImageOptions.Image = global::Contable.Properties.Resources.Marca;
            this.btnmarca.Name = "btnmarca";
            this.btnmarca.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnmarca_ItemClick);
            // 
            // btnunidadmedida
            // 
            this.btnunidadmedida.Caption = "Unidad de Medida";
            this.btnunidadmedida.Id = 39;
            this.btnunidadmedida.ImageOptions.Image = global::Contable.Properties.Resources.UnidadMedida;
            this.btnunidadmedida.Name = "btnunidadmedida";
            this.btnunidadmedida.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnunidadmedida_ItemClick);
            // 
            // btndiario
            // 
            this.btndiario.Caption = "Diario";
            this.btndiario.Id = 40;
            this.btndiario.Name = "btndiario";
            this.btndiario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btndiario_ItemClick);
            // 
            // btninventario
            // 
            this.btninventario.Caption = "Inventario";
            this.btninventario.Id = 41;
            this.btninventario.ImageOptions.LargeImage = global::Contable.Properties.Resources.Inventario;
            this.btninventario.Name = "btninventario";
            this.btninventario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btninventario_ItemClick);
            // 
            // btnalmacen
            // 
            this.btnalmacen.Caption = "Almacen";
            this.btnalmacen.Id = 42;
            this.btnalmacen.ImageOptions.LargeImage = global::Contable.Properties.Resources.Alamcen;
            this.btnalmacen.Name = "btnalmacen";
            this.btnalmacen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnalmacen_ItemClick);
            // 
            // btntipooperacion
            // 
            this.btntipooperacion.Caption = "Tipo de Operacion";
            this.btntipooperacion.Id = 43;
            this.btntipooperacion.ImageOptions.Image = global::Contable.Properties.Resources.tipodeanalisis;
            this.btntipooperacion.Name = "btntipooperacion";
            this.btntipooperacion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btntipooperacion_ItemClick);
            // 
            // btntesoreria
            // 
            this.btntesoreria.Caption = "Tesoreria";
            this.btntesoreria.Id = 44;
            this.btntesoreria.Name = "btntesoreria";
            this.btntesoreria.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btntesoreria_ItemClick);
            // 
            // btnbalancecomprobacion
            // 
            this.btnbalancecomprobacion.Caption = "Balance de comprobacion";
            this.btnbalancecomprobacion.Id = 45;
            this.btnbalancecomprobacion.ImageOptions.LargeImage = global::Contable.Properties.Resources.Balance_comprobacion;
            this.btnbalancecomprobacion.Name = "btnbalancecomprobacion";
            this.btnbalancecomprobacion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnbalancecomprobacion_ItemClick);
            // 
            // btnregistrocompras
            // 
            this.btnregistrocompras.Caption = "Registro de compras";
            this.btnregistrocompras.Id = 46;
            this.btnregistrocompras.ImageOptions.Image = global::Contable.Properties.Resources.RegistroCompra;
            this.btnregistrocompras.Name = "btnregistrocompras";
            this.btnregistrocompras.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnregistrocompras_ItemClick);
            // 
            // btnregistroventa
            // 
            this.btnregistroventa.Caption = "Registro de ventas";
            this.btnregistroventa.Id = 47;
            this.btnregistroventa.ImageOptions.Image = global::Contable.Properties.Resources.RegistroVenta;
            this.btnregistroventa.Name = "btnregistroventa";
            this.btnregistroventa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnregistroventa_ItemClick);
            // 
            // btnorden
            // 
            this.btnorden.Caption = "Orden de servicio / compra";
            this.btnorden.Id = 50;
            this.btnorden.ImageOptions.LargeImage = global::Contable.Properties.Resources.OrdenesServicio;
            this.btnorden.Name = "btnorden";
            this.btnorden.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnorden_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Edit = null;
            this.barEditItem1.Id = 58;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "Empresa";
            this.barEditItem2.Edit = null;
            this.barEditItem2.Id = 54;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // barEditItem3
            // 
            this.barEditItem3.Edit = null;
            this.barEditItem3.Id = 59;
            this.barEditItem3.Name = "barEditItem3";
            // 
            // txtempresa
            // 
            this.txtempresa.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempresa.Appearance.Options.UseFont = true;
            this.txtempresa.Caption = "EMPRESA";
            this.txtempresa.Id = 56;
            this.txtempresa.Name = "txtempresa";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 57;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // btngruposervicios
            // 
            this.btngruposervicios.Caption = "Grupo";
            this.btngruposervicios.Id = 60;
            this.btngruposervicios.ImageOptions.Image = global::Contable.Properties.Resources.Group;
            this.btngruposervicios.Name = "btngruposervicios";
            this.btngruposervicios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btngruposervicios_ItemClick);
            // 
            // btnfamiliaservicios
            // 
            this.btnfamiliaservicios.Caption = "Familia";
            this.btnfamiliaservicios.Id = 61;
            this.btnfamiliaservicios.ImageOptions.Image = global::Contable.Properties.Resources.Familia;
            this.btnfamiliaservicios.Name = "btnfamiliaservicios";
            this.btnfamiliaservicios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnfamiliaservicios_ItemClick);
            // 
            // btncatalogoservicios
            // 
            this.btncatalogoservicios.Caption = "Catalogo";
            this.btncatalogoservicios.Id = 62;
            this.btncatalogoservicios.ImageOptions.Image = global::Contable.Properties.Resources.Catalogo_;
            this.btncatalogoservicios.Name = "btncatalogoservicios";
            this.btncatalogoservicios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncatalogoservicios_ItemClick);
            // 
            // Btnventaa
            // 
            this.Btnventaa.Caption = "Ventas";
            this.Btnventaa.Id = 63;
            this.Btnventaa.ImageOptions.LargeImage = global::Contable.Properties.Resources.Move_by_Trolley_32px;
            this.Btnventaa.Name = "Btnventaa";
            this.Btnventaa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Btnventaa_ItemClick);
            // 
            // btndiarioo
            // 
            this.btndiarioo.Caption = "Diario";
            this.btndiarioo.Id = 64;
            this.btndiarioo.ImageOptions.Image = global::Contable.Properties.Resources.LibroDiario;
            this.btndiarioo.ImageOptions.LargeImage = global::Contable.Properties.Resources.LibroDiario;
            this.btndiarioo.Name = "btndiarioo";
            this.btndiarioo.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btndiarioo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btndiarioo_ItemClick);
            // 
            // btnanalisis
            // 
            this.btnanalisis.Caption = "Analisis Contable";
            this.btnanalisis.Id = 65;
            this.btnanalisis.ImageOptions.LargeImage = global::Contable.Properties.Resources.analisiscontable;
            this.btnanalisis.Name = "btnanalisis";
            this.btnanalisis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnanalisis_ItemClick);
            // 
            // btnrequerimientos
            // 
            this.btnrequerimientos.Caption = "Requerimientos";
            this.btnrequerimientos.Id = 66;
            this.btnrequerimientos.ImageOptions.LargeImage = global::Contable.Properties.Resources.requerimeinto;
            this.btnrequerimientos.Name = "btnrequerimientos";
            this.btnrequerimientos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnrequerimientos_ItemClick);
            // 
            // btncargo
            // 
            this.btncargo.Caption = "Cargo";
            this.btncargo.Id = 67;
            this.btncargo.ImageOptions.Image = global::Contable.Properties.Resources.cargo;
            this.btncargo.Name = "btncargo";
            this.btncargo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncargo_ItemClick);
            // 
            // btnarea
            // 
            this.btnarea.Caption = "Area";
            this.btnarea.Id = 68;
            this.btnarea.ImageOptions.Image = global::Contable.Properties.Resources.area;
            this.btnarea.Name = "btnarea";
            this.btnarea.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnarea_ItemClick);
            // 
            // btnlibrodiario
            // 
            this.btnlibrodiario.Caption = "Libro diario";
            this.btnlibrodiario.Id = 69;
            this.btnlibrodiario.ImageOptions.Image = global::Contable.Properties.Resources.libdiario;
            this.btnlibrodiario.Name = "btnlibrodiario";
            this.btnlibrodiario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlibrodiario_ItemClick);
            // 
            // btnlibromayor
            // 
            this.btnlibromayor.Caption = "Libro mayor";
            this.btnlibromayor.Id = 70;
            this.btnlibromayor.ImageOptions.Image = global::Contable.Properties.Resources.libromayor;
            this.btnlibromayor.Name = "btnlibromayor";
            this.btnlibromayor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlibromayor_ItemClick);
            // 
            // btnmediopago
            // 
            this.btnmediopago.Caption = "Medio de pago";
            this.btnmediopago.Id = 71;
            this.btnmediopago.ImageOptions.Image = global::Contable.Properties.Resources.MedioPago1;
            this.btnmediopago.Name = "btnmediopago";
            this.btnmediopago.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnmediopago_ItemClick);
            // 
            // btncuentacorriente
            // 
            this.btncuentacorriente.Caption = "Cuenta corriente";
            this.btncuentacorriente.Id = 72;
            this.btncuentacorriente.ImageOptions.Image = global::Contable.Properties.Resources.Cuenta_Corriente;
            this.btncuentacorriente.Name = "btncuentacorriente";
            this.btncuentacorriente.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncuentacorriente_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 73;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // btningresos
            // 
            this.btningresos.Caption = "Ingresos Egresos";
            this.btningresos.Id = 74;
            this.btningresos.ImageOptions.LargeImage = global::Contable.Properties.Resources.Ingresos;
            this.btningresos.Name = "btningresos";
            this.btningresos.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btningresos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btningresos_ItemClick);
            // 
            // btnegresos
            // 
            this.btnegresos.Caption = "Egresos";
            this.btnegresos.Id = 75;
            this.btnegresos.ImageOptions.LargeImage = global::Contable.Properties.Resources.Egresos;
            this.btnegresos.Name = "btnegresos";
            this.btnegresos.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnegresos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnegresos_ItemClick);
            // 
            // btnentidadfinanciera
            // 
            this.btnentidadfinanciera.Caption = "Enti. Financiera";
            this.btnentidadfinanciera.Id = 76;
            this.btnentidadfinanciera.ImageOptions.Image = global::Contable.Properties.Resources.Entidad_Financiera;
            this.btnentidadfinanciera.Name = "btnentidadfinanciera";
            this.btnentidadfinanciera.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnentidadfinanciera_ItemClick);
            // 
            // btnseriesdocumento
            // 
            this.btnseriesdocumento.Caption = "Series de documentos";
            this.btnseriesdocumento.Id = 77;
            this.btnseriesdocumento.ImageOptions.Image = global::Contable.Properties.Resources.series;
            this.btnseriesdocumento.Name = "btnseriesdocumento";
            this.btnseriesdocumento.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnaperturach
            // 
            this.btnaperturach.Caption = "Apertura Caja chica ( 1 )";
            this.btnaperturach.Id = 78;
            this.btnaperturach.ImageOptions.Image = global::Contable.Properties.Resources.AperturaCajachica;
            this.btnaperturach.Name = "btnaperturach";
            this.btnaperturach.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnaperturach_ItemClick);
            // 
            // btnrendicion
            // 
            this.btnrendicion.Caption = "Rendicion ( 2 )";
            this.btnrendicion.Id = 79;
            this.btnrendicion.ImageOptions.Image = global::Contable.Properties.Resources.RendicioCaja;
            this.btnrendicion.Name = "btnrendicion";
            this.btnrendicion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnrendicion_ItemClick);
            // 
            // btncajachica
            // 
            this.btncajachica.Caption = "Caja chica";
            this.btncajachica.Id = 80;
            this.btncajachica.ImageOptions.Image = global::Contable.Properties.Resources.CajaChica;
            this.btncajachica.Name = "btncajachica";
            this.btncajachica.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncajachica_ItemClick);
            // 
            // btnreembolso
            // 
            this.btnreembolso.Caption = "Reembolso ( 3 )";
            this.btnreembolso.Id = 81;
            this.btnreembolso.ImageOptions.Image = global::Contable.Properties.Resources.Reembolso;
            this.btnreembolso.Name = "btnreembolso";
            this.btnreembolso.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnreembolso_ItemClick);
            // 
            // btncierre
            // 
            this.btncierre.Caption = "Cierre ( 4 )";
            this.btncierre.Id = 82;
            this.btncierre.ImageOptions.Image = global::Contable.Properties.Resources.Cierre;
            this.btncierre.Name = "btncierre";
            this.btncierre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncierre_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 83;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItem8";
            this.barButtonItem8.Id = 84;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // btnreplicacion
            // 
            this.btnreplicacion.Caption = "Replicacion";
            this.btnreplicacion.Id = 85;
            this.btnreplicacion.ImageOptions.LargeImage = global::Contable.Properties.Resources.Replicacion;
            this.btnreplicacion.Name = "btnreplicacion";
            this.btnreplicacion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnreplicacion_ItemClick);
            // 
            // btnigv
            // 
            this.btnigv.Caption = "I.G.V.";
            this.btnigv.Id = 86;
            this.btnigv.ImageOptions.Image = global::Contable.Properties.Resources.Impuesto;
            this.btnigv.Name = "btnigv";
            this.btnigv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // btnestadosfianacierostributarios
            // 
            this.btnestadosfianacierostributarios.Caption = "Estados Financieros Tributarios";
            this.btnestadosfianacierostributarios.Id = 87;
            this.btnestadosfianacierostributarios.ImageOptions.Image = global::Contable.Properties.Resources.tributario;
            this.btnestadosfianacierostributarios.Name = "btnestadosfianacierostributarios";
            this.btnestadosfianacierostributarios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnestadosfianacierostributarios_ItemClick);
            // 
            // btnestadofinansmv
            // 
            this.btnestadofinansmv.Caption = "Estado Financiero SMW";
            this.btnestadofinansmv.Id = 88;
            this.btnestadofinansmv.ImageOptions.Image = global::Contable.Properties.Resources.estadofinanciero;
            this.btnestadofinansmv.Name = "btnestadofinansmv";
            this.btnestadofinansmv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnestadofinansmv_ItemClick);
            // 
            // btnLibroSimplificado
            // 
            this.btnLibroSimplificado.Caption = "Libro diario simplificado";
            this.btnLibroSimplificado.Id = 89;
            this.btnLibroSimplificado.ImageOptions.Image = global::Contable.Properties.Resources.librodiariosimpli;
            this.btnLibroSimplificado.Name = "btnLibroSimplificado";
            this.btnLibroSimplificado.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLibroSimplificado_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Estados Financieros Tributarios";
            this.barButtonItem1.Id = 90;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Estados Financieros Tributarios";
            this.barButtonItem9.Id = 91;
            this.barButtonItem9.ImageOptions.Image = global::Contable.Properties.Resources.tributario;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick_1);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Estado Financiero SMW";
            this.barButtonItem10.Id = 92;
            this.barButtonItem10.ImageOptions.Image = global::Contable.Properties.Resources.estadofinanciero;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Formato Flujo de efectivo";
            this.barButtonItem12.Id = 93;
            this.barButtonItem12.ImageOptions.Image = global::Contable.Properties.Resources.flujoefectivo;
            this.barButtonItem12.Name = "barButtonItem12";
            this.barButtonItem12.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem12_ItemClick);
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Cambios Patrimonio Neto SMV";
            this.barButtonItem14.Id = 94;
            this.barButtonItem14.ImageOptions.Image = global::Contable.Properties.Resources.patrimonioneto;
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem14_ItemClick);
            // 
            // btnesatdosituacionfinanciera
            // 
            this.btnesatdosituacionfinanciera.Caption = "Estado de Situacion Financiera";
            this.btnesatdosituacionfinanciera.Id = 95;
            this.btnesatdosituacionfinanciera.ImageOptions.Image = global::Contable.Properties.Resources.estadofinanciero;
            this.btnesatdosituacionfinanciera.Name = "btnesatdosituacionfinanciera";
            this.btnesatdosituacionfinanciera.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnesatdosituacionfinanciera_ItemClick);
            // 
            // btnflujoefectivo
            // 
            this.btnflujoefectivo.Caption = "Formato Flujo de efectivo";
            this.btnflujoefectivo.Id = 96;
            this.btnflujoefectivo.ImageOptions.Image = global::Contable.Properties.Resources.flujoefectivo;
            this.btnflujoefectivo.Name = "btnflujoefectivo";
            this.btnflujoefectivo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnflujoefectivo_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "barButtonItem11";
            this.barButtonItem11.Id = 97;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "barButtonItem16";
            this.barButtonItem16.Id = 98;
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Estados de Cambio Patrimonio Neto";
            this.barSubItem1.Id = 99;
            this.barSubItem1.ImageOptions.Image = global::Contable.Properties.Resources.cambiopatrimonioneto;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btncambiopatrimonionetosmv)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // btncambiopatrimonionetosmv
            // 
            this.btncambiopatrimonionetosmv.Caption = "Cambios Patrimonio Neto SMV";
            this.btncambiopatrimonionetosmv.Id = 100;
            this.btncambiopatrimonionetosmv.ImageOptions.Image = global::Contable.Properties.Resources.patrimonioneto;
            this.btncambiopatrimonionetosmv.Name = "btncambiopatrimonionetosmv";
            this.btncambiopatrimonionetosmv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncambiopatrimonionetosmv_ItemClick);
            // 
            // btnestadodflujoefectivo
            // 
            this.btnestadodflujoefectivo.Caption = "Estado de Flujo de Efectivo";
            this.btnestadodflujoefectivo.Id = 101;
            this.btnestadodflujoefectivo.ImageOptions.Image = global::Contable.Properties.Resources.flujoefectivo;
            this.btnestadodflujoefectivo.Name = "btnestadodflujoefectivo";
            this.btnestadodflujoefectivo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnestadodflujoefectivo_ItemClick);
            // 
            // btnestadocambiopatrimonioneto
            // 
            this.btnestadocambiopatrimonioneto.Caption = "Estado de Cambios en el Patrimonio Neto";
            this.btnestadocambiopatrimonioneto.Id = 102;
            this.btnestadocambiopatrimonioneto.ImageOptions.Image = global::Contable.Properties.Resources.cambiopatrimonioneto;
            this.btnestadocambiopatrimonioneto.Name = "btnestadocambiopatrimonioneto";
            this.btnestadocambiopatrimonioneto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnestadocambiopatrimonioneto_ItemClick);
            // 
            // btnesatdofinancierosunat
            // 
            this.btnesatdofinancierosunat.Caption = "Estado Financiero Sunat";
            this.btnesatdofinancierosunat.Id = 103;
            this.btnesatdofinancierosunat.ImageOptions.Image = global::Contable.Properties.Resources.tributario;
            this.btnesatdofinancierosunat.Name = "btnesatdofinancierosunat";
            this.btnesatdofinancierosunat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnesatdofinancierosunat_ItemClick);
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "barButtonItem15";
            this.barButtonItem15.Id = 104;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup6,
            this.ribbonPageGroup7,
            this.ribbonPageGroup3,
            this.ribbonPageGroup5,
            this.ribbonPageGroup8,
            this.ribbonPageGroup9,
            this.ribbonPageGroup10,
            this.ribbonPageGroup16});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Configuraciones";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup1.ItemLinks.Add(this.btncomprobantes);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnmoneda);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnlibroscontables);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Tablas SUNAT";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup6.ItemLinks.Add(this.btnelemento);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnestructura);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnplangeneral);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Plan Contable";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup7.ItemLinks.Add(this.btnplanempresarial);
            this.ribbonPageGroup7.ItemLinks.Add(this.btnanalisis);
            this.ribbonPageGroup7.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Plan Contable Empresarial";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup3.ItemLinks.Add(this.btnTipoImpuesto);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnAsignacionImp);
            this.ribbonPageGroup3.ItemLinks.Add(this.btntipocambio);
            this.ribbonPageGroup3.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.ThreeRows;
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Impuesto";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup5.ItemLinks.Add(this.btnempresa);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnejercicio);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnreplicacion);
            this.ribbonPageGroup5.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Generales";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup8.ItemLinks.Add(this.btninicial);
            this.ribbonPageGroup8.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "Inicial";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup9.ItemLinks.Add(this.btnentidad);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "Entidades";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup10.ItemLinks.Add(this.btnunidad);
            this.ribbonPageGroup10.ItemLinks.Add(this.btncentrocostogasto);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "Centro de costo - gasto";
            // 
            // ribbonPageGroup16
            // 
            this.ribbonPageGroup16.ItemLinks.Add(this.btncargo);
            this.ribbonPageGroup16.ItemLinks.Add(this.btnarea);
            this.ribbonPageGroup16.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup16.Name = "ribbonPageGroup16";
            this.ribbonPageGroup16.Text = "Area / Cargo";
            // 
            // ribbonPage6
            // 
            this.ribbonPage6.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup20});
            this.ribbonPage6.Name = "ribbonPage6";
            this.ribbonPage6.Text = "Diario";
            // 
            // ribbonPageGroup20
            // 
            this.ribbonPageGroup20.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup20.ItemLinks.Add(this.btndiarioo);
            this.ribbonPageGroup20.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup20.Name = "ribbonPageGroup20";
            this.ribbonPageGroup20.Text = "Diario";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup21,
            this.ribbonPageGroup13,
            this.ribbonPageGroup18,
            this.ribbonPageGroup2});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Compras";
            // 
            // ribbonPageGroup21
            // 
            this.ribbonPageGroup21.ItemLinks.Add(this.btngruposervicios);
            this.ribbonPageGroup21.ItemLinks.Add(this.btnfamiliaservicios);
            this.ribbonPageGroup21.ItemLinks.Add(this.btncatalogoservicios);
            this.ribbonPageGroup21.Name = "ribbonPageGroup21";
            this.ribbonPageGroup21.Text = "Servicios";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.ItemLinks.Add(this.btnrequerimientos);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.Text = "Requerimientos";
            // 
            // ribbonPageGroup18
            // 
            this.ribbonPageGroup18.ItemLinks.Add(this.btnorden);
            this.ribbonPageGroup18.Name = "ribbonPageGroup18";
            this.ribbonPageGroup18.Text = "Ordenes";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup2.ItemLinks.Add(this.btncompras);
            this.ribbonPageGroup2.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Compras";
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup19});
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "Ventas";
            // 
            // ribbonPageGroup19
            // 
            this.ribbonPageGroup19.ItemLinks.Add(this.Btnventaa);
            this.ribbonPageGroup19.Name = "ribbonPageGroup19";
            this.ribbonPageGroup19.Text = "Ventas";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup11,
            this.ribbonPageGroup12,
            this.ribbonPageGroup15,
            this.ribbonPageGroup14});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Inventario";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup11.ItemLinks.Add(this.btngrupo);
            this.ribbonPageGroup11.ItemLinks.Add(this.btnfamilia);
            this.ribbonPageGroup11.ItemLinks.Add(this.btncatalogo);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "Bienes";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup12.ItemLinks.Add(this.btnmarca);
            this.ribbonPageGroup12.ItemLinks.Add(this.btnunidadmedida);
            this.ribbonPageGroup12.ItemLinks.Add(this.btntipooperacion);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            // 
            // ribbonPageGroup15
            // 
            this.ribbonPageGroup15.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup15.ItemLinks.Add(this.btnalmacen);
            this.ribbonPageGroup15.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup15.Name = "ribbonPageGroup15";
            this.ribbonPageGroup15.Text = "Almacen";
            // 
            // ribbonPageGroup14
            // 
            this.ribbonPageGroup14.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup14.ItemLinks.Add(this.btninventario);
            this.ribbonPageGroup14.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup14.Name = "ribbonPageGroup14";
            this.ribbonPageGroup14.Text = "Inventario";
            // 
            // ribbonPage8
            // 
            this.ribbonPage8.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup24,
            this.ribbonPageGroup27,
            this.ribbonPageGroup25,
            this.ribbonPageGroup26});
            this.ribbonPage8.Name = "ribbonPage8";
            this.ribbonPage8.Text = "Tesoreria";
            // 
            // ribbonPageGroup24
            // 
            this.ribbonPageGroup24.ItemLinks.Add(this.btnmediopago);
            this.ribbonPageGroup24.ItemLinks.Add(this.btnentidadfinanciera);
            this.ribbonPageGroup24.ItemLinks.Add(this.btncuentacorriente);
            this.ribbonPageGroup24.ItemLinks.Add(this.btncajachica);
            this.ribbonPageGroup24.ItemLinks.Add(this.btnseriesdocumento);
            this.ribbonPageGroup24.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup24.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup24.Name = "ribbonPageGroup24";
            this.ribbonPageGroup24.Text = "Configuracion";
            // 
            // ribbonPageGroup27
            // 
            this.ribbonPageGroup27.ItemLinks.Add(this.btnaperturach);
            this.ribbonPageGroup27.ItemLinks.Add(this.btnrendicion);
            this.ribbonPageGroup27.ItemLinks.Add(this.btnreembolso);
            this.ribbonPageGroup27.ItemLinks.Add(this.btncierre);
            this.ribbonPageGroup27.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup27.Name = "ribbonPageGroup27";
            this.ribbonPageGroup27.Text = "Caja Chica";
            // 
            // ribbonPageGroup25
            // 
            this.ribbonPageGroup25.ItemLinks.Add(this.btningresos);
            this.ribbonPageGroup25.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.ThreeRows;
            this.ribbonPageGroup25.Name = "ribbonPageGroup25";
            this.ribbonPageGroup25.Text = "Ingresos";
            // 
            // ribbonPageGroup26
            // 
            this.ribbonPageGroup26.ItemLinks.Add(this.btnegresos);
            this.ribbonPageGroup26.Name = "ribbonPageGroup26";
            this.ribbonPageGroup26.Text = "Egresos";
            this.ribbonPageGroup26.Visible = false;
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup17,
            this.ribbonPageGroup22,
            this.ribbonPageGroup28,
            this.ribbonPageGroup31});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Reportes";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnbalancecomprobacion);
            this.ribbonPageGroup4.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            // 
            // ribbonPageGroup17
            // 
            this.ribbonPageGroup17.ItemLinks.Add(this.btnregistrocompras);
            this.ribbonPageGroup17.ItemLinks.Add(this.btnregistroventa);
            this.ribbonPageGroup17.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup17.Name = "ribbonPageGroup17";
            this.ribbonPageGroup17.Text = "Registros";
            // 
            // ribbonPageGroup22
            // 
            this.ribbonPageGroup22.ItemLinks.Add(this.btnlibrodiario);
            this.ribbonPageGroup22.ItemLinks.Add(this.btnlibromayor);
            this.ribbonPageGroup22.ItemLinks.Add(this.btnigv);
            this.ribbonPageGroup22.ItemLinks.Add(this.btnLibroSimplificado);
            this.ribbonPageGroup22.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup22.Name = "ribbonPageGroup22";
            this.ribbonPageGroup22.Text = "Libros";
            // 
            // ribbonPageGroup28
            // 
            this.ribbonPageGroup28.ItemLinks.Add(this.btnestadosfianacierostributarios);
            this.ribbonPageGroup28.ItemLinks.Add(this.btnestadofinansmv);
            this.ribbonPageGroup28.ItemLinks.Add(this.btnflujoefectivo);
            this.ribbonPageGroup28.ItemLinks.Add(this.barSubItem1);
            this.ribbonPageGroup28.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup28.Name = "ribbonPageGroup28";
            this.ribbonPageGroup28.Text = "Formatos Estados Financieros";
            // 
            // ribbonPageGroup31
            // 
            this.ribbonPageGroup31.ItemLinks.Add(this.btnesatdosituacionfinanciera);
            this.ribbonPageGroup31.ItemLinks.Add(this.btnestadodflujoefectivo);
            this.ribbonPageGroup31.ItemLinks.Add(this.btnestadocambiopatrimonioneto);
            this.ribbonPageGroup31.ItemLinks.Add(this.btnesatdofinancierosunat);
            this.ribbonPageGroup31.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup31.Name = "ribbonPageGroup31";
            this.ribbonPageGroup31.Text = "Reportes Formatos Estados Financieros";
            // 
            // ribbonPage7
            // 
            this.ribbonPage7.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup23});
            this.ribbonPage7.Name = "ribbonPage7";
            this.ribbonPage7.Text = "Reportes administrativos";
            this.ribbonPage7.Visible = false;
            // 
            // ribbonPageGroup23
            // 
            this.ribbonPageGroup23.Name = "ribbonPageGroup23";
            this.ribbonPageGroup23.Text = "ribbonPageGroup23";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // dockManager1
            // 
            this.dockManager1.Controller = this.barAndDockingController1;
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.ribbonControl1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // documentManager1
            // 
            this.documentManager1.BarAndDockingController = this.barAndDockingController1;
            this.documentManager1.MdiParent = this;
            this.documentManager1.MenuManager = this.ribbonControl1;
            this.documentManager1.View = this.tabbedView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(109)))), ((int)(((byte)(156)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(409, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(26, 14);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Año:";
            // 
            // comboanio
            // 
            this.comboanio.FormattingEnabled = true;
            this.comboanio.Location = new System.Drawing.Point(440, 4);
            this.comboanio.Name = "comboanio";
            this.comboanio.Size = new System.Drawing.Size(147, 21);
            this.comboanio.TabIndex = 3;
            this.comboanio.SelectedIndexChanged += new System.EventHandler(this.comboanio_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(109)))), ((int)(((byte)(156)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(591, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 14);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Periodo:";
            // 
            // comboperiodo
            // 
            this.comboperiodo.FormattingEnabled = true;
            this.comboperiodo.Location = new System.Drawing.Point(639, 4);
            this.comboperiodo.Name = "comboperiodo";
            this.comboperiodo.Size = new System.Drawing.Size(121, 21);
            this.comboperiodo.TabIndex = 6;
            this.comboperiodo.SelectedIndexChanged += new System.EventHandler(this.comboperiodo_SelectedIndexChanged);
            // 
            // timerTipodeCambio
            // 
            this.timerTipodeCambio.Interval = 1000;
            this.timerTipodeCambio.Tick += new System.EventHandler(this.timerTipodeCambio_Tick);
            // 
            // frm_principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 602);
            this.Controls.Add(this.comboperiodo);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.comboanio);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Name = "frm_principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_principal_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_principal_FormClosed);
            this.Load += new System.EventHandler(this.frm_principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btncomprobantes;
        private DevExpress.XtraBars.BarButtonItem btnmoneda;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem btnTipoImpuesto;
        private DevExpress.XtraBars.BarButtonItem btnAsignacionImp;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.BarButtonItem btnempresa;
        private DevExpress.XtraBars.BarButtonItem btnejercicio;
        private DevExpress.XtraBars.BarButtonItem btnlibroscontables;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem btnelemento;
        private DevExpress.XtraBars.BarButtonItem btnestructura;
        private DevExpress.XtraBars.BarButtonItem btnplangeneral;
        private DevExpress.XtraBars.BarButtonItem btnplanempresarial;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem btnventas;
        private DevExpress.XtraBars.BarButtonItem btncompras;
        private DevExpress.XtraBars.BarButtonItem btninicial;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem btnentidad;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.BarButtonItem btnunidad;
        private DevExpress.XtraBars.BarButtonItem btncentrocostogasto;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.BarButtonItem btntipocambio;
        private DevExpress.XtraBars.BarButtonItem btngrupo;
        private DevExpress.XtraBars.BarButtonItem btnfamilia;
        private DevExpress.XtraBars.BarButtonItem btncatalogo;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.BarButtonItem btnmarca;
        private DevExpress.XtraBars.BarButtonItem btnunidadmedida;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        public DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        public DevExpress.XtraBars.Docking.DockManager dockManager1;
        public DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.BarButtonItem btndiario;
        private DevExpress.XtraBars.BarButtonItem btninventario;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup14;
        private DevExpress.XtraBars.BarButtonItem btnalmacen;
        private DevExpress.XtraBars.BarButtonItem btntipooperacion;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup15;
        private DevExpress.XtraBars.BarButtonItem btntesoreria;
        private DevExpress.XtraBars.BarButtonItem btnbalancecomprobacion;
        private DevExpress.XtraBars.BarButtonItem btnregistrocompras;
        private DevExpress.XtraBars.BarButtonItem btnregistroventa;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup17;

        public DevExpress.XtraBars.BarEditItem cbxanio;
        private DevExpress.XtraBars.BarButtonItem btnorden;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup18;
        private DevExpress.XtraBars.BarEditItem barEditItem1;

        public DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        public DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;

        public DevExpress.XtraBars.BarEditItem cb;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraBars.BarEditItem barEditItem3;

        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        public DevExpress.XtraBars.BarHeaderItem txtempresa;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraBars.BarButtonItem btngruposervicios;
        private DevExpress.XtraBars.BarButtonItem btnfamiliaservicios;
        private DevExpress.XtraBars.BarButtonItem btncatalogoservicios;
        private DevExpress.XtraBars.BarButtonItem Btnventaa;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup20;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup21;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup19;
        private DevExpress.XtraBars.BarButtonItem btndiarioo;
        public System.Windows.Forms.ComboBox comboanio;
        public System.Windows.Forms.ComboBox comboperiodo;
        private DevExpress.XtraBars.BarButtonItem btnanalisis;
        private DevExpress.XtraBars.BarButtonItem btnrequerimientos;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
        private DevExpress.XtraBars.BarButtonItem btncargo;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup16;
        private DevExpress.XtraBars.BarButtonItem btnarea;
        private DevExpress.XtraBars.BarButtonItem btnlibrodiario;
        private DevExpress.XtraBars.BarButtonItem btnlibromayor;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup22;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup23;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage8;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup24;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup25;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup26;
        private DevExpress.XtraBars.BarButtonItem btnmediopago;
        private DevExpress.XtraBars.BarButtonItem btncuentacorriente;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem btningresos;
        private DevExpress.XtraBars.BarButtonItem btnegresos;
        private DevExpress.XtraBars.BarButtonItem btnentidadfinanciera;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup27;
        private DevExpress.XtraBars.BarButtonItem btnseriesdocumento;
        private DevExpress.XtraBars.BarButtonItem btnaperturach;
        private DevExpress.XtraBars.BarButtonItem btnrendicion;
        private DevExpress.XtraBars.BarButtonItem btncajachica;
        private DevExpress.XtraBars.BarButtonItem btnreembolso;
        private DevExpress.XtraBars.BarButtonItem btncierre;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem btnreplicacion;
        private DevExpress.XtraBars.BarButtonItem btnigv;
        private DevExpress.XtraBars.BarButtonItem btnestadosfianacierostributarios;
        private DevExpress.XtraBars.BarButtonItem btnestadofinansmv;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup28;
        private DevExpress.XtraBars.BarButtonItem btnLibroSimplificado;
        private System.Windows.Forms.Timer timerTipodeCambio;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem btnesatdosituacionfinanciera;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup31;
        private DevExpress.XtraBars.BarButtonItem btnflujoefectivo;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btncambiopatrimonionetosmv;
        private DevExpress.XtraBars.BarButtonItem btnestadodflujoefectivo;
        private DevExpress.XtraBars.BarButtonItem btnestadocambiopatrimonioneto;
        private DevExpress.XtraBars.BarButtonItem btnesatdofinancierosunat;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
    }
}