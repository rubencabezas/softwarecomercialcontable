﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._2_Alertas
{
    public partial class frm_alerta_anulacion : MetroFramework.Forms.MetroForm
    {
        public frm_alerta_anulacion()
        {
            InitializeComponent();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            if (DatosCompletos())
            {
                DialogResult =DialogResult.OK;
            }
        }

        public bool DatosCompletos()
        {
            if (chkinformar.Checked)
            {
                if (!IsDate(TxtFecha.Text))
                {
                    TxtFecha.Focus();
                    return false;
                }
                else if (string.IsNullOrEmpty(TxtMotivo.Text))
                {
                    TxtMotivo.Focus();
                    return false;
                }
            }

            return true;
        }


        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        private void chkinformar_CheckedChanged(object sender, EventArgs e)
        {
         
                if (chkinformar.Checked)
                {
                    TxtFecha.Enabled = true;
                    TxtMotivo.Enabled = true;
                    TxtFecha.Focus();
                }
                else
                {
                    TxtFecha.Enabled = false;
                    TxtMotivo.Enabled = false;
                }
          

        }

        private void BtnAceptar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode ==  Keys.Escape)
            {
                DialogResult =  DialogResult.Cancel;
            }
        }

        private void frm_alerta_anulacion_Load(object sender, EventArgs e)
        {
            //TxtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //TxtFecha.EditValue = DateTime.Now.ToString("dd/MM/yyyy");
            TxtFecha.Text = String.Format("{0:yyyy-MM-dd}", DateTime.Now.ToString("dd/MM/yyyy"));
        }

        private void frm_alerta_anulacion_Shown(object sender, EventArgs e)
        {
            TxtFecha.Focus();
        }
    }
}
