﻿namespace Contable._2_Alertas
{
    partial class frm_alerta_anulacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.chkinformar = new System.Windows.Forms.CheckBox();
            this.Label67 = new System.Windows.Forms.Label();
            this.BtnAceptar = new MetroFramework.Controls.MetroButton();
            this.BtnCancelar = new MetroFramework.Controls.MetroButton();
            this.TxtMotivo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtFecha = new DevExpress.XtraEditors.TextEdit();
            this.LblMensaje = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMotivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFecha.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.BackColor = System.Drawing.Color.DarkRed;
            this.metroLabel1.CustomBackground = true;
            this.metroLabel1.CustomForeColor = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.ForeColor = System.Drawing.Color.White;
            this.metroLabel1.Location = new System.Drawing.Point(7, 8);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(446, 30);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Desea anular?";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkinformar
            // 
            this.chkinformar.AutoSize = true;
            this.chkinformar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkinformar.Location = new System.Drawing.Point(7, 113);
            this.chkinformar.Name = "chkinformar";
            this.chkinformar.Size = new System.Drawing.Size(185, 17);
            this.chkinformar.TabIndex = 60;
            this.chkinformar.Text = "Informar comunicacion baja";
            this.chkinformar.UseVisualStyleBackColor = true;
            this.chkinformar.CheckedChanged += new System.EventHandler(this.chkinformar_CheckedChanged);
            // 
            // Label67
            // 
            this.Label67.AutoSize = true;
            this.Label67.Location = new System.Drawing.Point(6, 136);
            this.Label67.Name = "Label67";
            this.Label67.Size = new System.Drawing.Size(75, 13);
            this.Label67.TabIndex = 59;
            this.Label67.Text = "Razón/Motivo";
            // 
            // BtnAceptar
            // 
            this.BtnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.BtnAceptar.Location = new System.Drawing.Point(54, 177);
            this.BtnAceptar.Name = "BtnAceptar";
            this.BtnAceptar.Size = new System.Drawing.Size(155, 23);
            this.BtnAceptar.TabIndex = 61;
            this.BtnAceptar.Text = "Anular";
            this.BtnAceptar.Click += new System.EventHandler(this.BtnAceptar_Click);
            this.BtnAceptar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BtnAceptar_KeyDown);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancelar.Location = new System.Drawing.Point(278, 177);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(155, 23);
            this.BtnCancelar.TabIndex = 61;
            this.BtnCancelar.Text = "Cancelar";
            // 
            // TxtMotivo
            // 
            this.TxtMotivo.EnterMoveNextControl = true;
            this.TxtMotivo.Location = new System.Drawing.Point(7, 155);
            this.TxtMotivo.Name = "TxtMotivo";
            this.TxtMotivo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtMotivo.Size = new System.Drawing.Size(446, 20);
            this.TxtMotivo.TabIndex = 62;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Fecha";
            // 
            // TxtFecha
            // 
            this.TxtFecha.EnterMoveNextControl = true;
            this.TxtFecha.Location = new System.Drawing.Point(298, 111);
            this.TxtFecha.Name = "TxtFecha";
            this.TxtFecha.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.TxtFecha.Properties.Mask.BeepOnError = true;
            this.TxtFecha.Properties.Mask.EditMask = "d";
            this.TxtFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.TxtFecha.Properties.Mask.SaveLiteral = false;
            this.TxtFecha.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TxtFecha.Properties.MaxLength = 10;
            this.TxtFecha.Size = new System.Drawing.Size(89, 20);
            this.TxtFecha.TabIndex = 63;
            // 
            // LblMensaje
            // 
            this.LblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMensaje.Location = new System.Drawing.Point(4, 42);
            this.LblMensaje.Name = "LblMensaje";
            this.LblMensaje.Size = new System.Drawing.Size(446, 56);
            this.LblMensaje.TabIndex = 64;
            // 
            // frm_alerta_anulacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 209);
            this.Controls.Add(this.LblMensaje);
            this.Controls.Add(this.TxtFecha);
            this.Controls.Add(this.TxtMotivo);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.BtnAceptar);
            this.Controls.Add(this.chkinformar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Label67);
            this.Controls.Add(this.metroLabel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_alerta_anulacion";
            this.Resizable = false;
            this.Load += new System.EventHandler(this.frm_alerta_anulacion_Load);
            this.Shown += new System.EventHandler(this.frm_alerta_anulacion_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.TxtMotivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFecha.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        internal System.Windows.Forms.Label Label67;
        private MetroFramework.Controls.MetroButton BtnAceptar;
        private MetroFramework.Controls.MetroButton BtnCancelar;
        internal System.Windows.Forms.Label label1;
        public System.Windows.Forms.CheckBox chkinformar;
        public DevExpress.XtraEditors.TextEdit TxtMotivo;
        public DevExpress.XtraEditors.TextEdit TxtFecha;
        public System.Windows.Forms.Label LblMensaje;
    }
}