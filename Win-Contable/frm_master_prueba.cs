﻿using Comercial;
using DevExpress.CodeParser;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable
{
    public partial class frm_master_prueba : frm_fuente
    {
        public frm_master_prueba()
        {
            InitializeComponent();
        }

        public string EmpresaNombre;
        public string CodigoEmpresa;
        public string RucEmpresa;

        public string AnioSelect;
        public string PeriodoSelect;

        //FACTURACION ELECTRONICA
        public string Emp_DireccionCorta;
        public string Emp_Urbanizacion;
        public string Emp_Direccion_Departamento;
        public string Emp_Direccion_Provincia;
        public string Emp_Direccion_Distrito;


        public string UserID;
        public string UserName = "user";
        public string UserNameEmployee;

        private void frm_master_prueba_Load(object sender, EventArgs e)
        {
            CargarMenu(usuario);

            Actual_Conexion.CodigoEmpresa = CodigoEmpresa;
            Actual_Conexion.RucEmpresa = RucEmpresa;

            Actual_Conexion.UserID = UserID;
            Actual_Conexion.UserName = UserName;
            Actual_Conexion.UserNameEmployee = UserNameEmployee;

            //FACTURACION ELECTRONICA
            Actual_Conexion.Emp_DireccionCorta = Emp_DireccionCorta;
            Actual_Conexion.Emp_Urbanizacion = Emp_Urbanizacion;
            Actual_Conexion.Emp_Direccion_Departamento = Emp_Direccion_Departamento;
            Actual_Conexion.Emp_Direccion_Provincia = Emp_Direccion_Provincia;
            Actual_Conexion.Emp_Direccion_Distrito = Emp_Direccion_Distrito;



            IniciarBarra();

            // timerTipodeCambio.Enabled = true;
        }

        List<Entidad_Ejercicio> Anios = new List<Entidad_Ejercicio>();
        List<Entidad_Ejercicio> Periodos = new List<Entidad_Ejercicio>();
        List<Entidad_Ejercicio> Anios_Aux = new List<Entidad_Ejercicio>();
        public void IniciarBarra()
        {
            try
            {

                txtempresa.Caption = Actual_Conexion.EmpresaNombre;
                Logica_Ejercicio LogAnio = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

                Anios = LogAnio.Listar(Ent);

                var PrivView = from item in Anios
                               orderby item.Id_Anio descending
                               select item;
                Anios_Aux = PrivView.ToList();


                comboanio.ValueMember = "Id_Anio";
                comboanio.DisplayMember = "Id_Anio";
                comboanio.DataSource = Anios_Aux;

                ListarPeriodos();

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        public void ListarPeriodos()
        {
            try
            {

                Logica_Ejercicio LogPer = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;

                Periodos = LogPer.Listar_Periodo(Ent);


                comboperiodo.ValueMember = "Id_Periodo";
                comboperiodo.DisplayMember = "Descripcion_Periodo";
                comboperiodo.DataSource = Periodos;


                DateTime time = DateTime.Now;
                string format = "00";
                comboperiodo.SelectedValue = time.Month.ToString(format);

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void comboanio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboanio.Items.Count > 0)
            {

                Actual_Conexion.AnioSelect = comboanio.SelectedValue.ToString();
                ListarPeriodos();
                Actual_Conexion.PeriodoSelect = comboperiodo.SelectedValue.ToString();
                Accion.RefreshDateDefault();
                ActualizarBusquedaPorAnio();
            }
        }

        public override void ActualizarBusquedaPorAnio()
        {
            try
            {
                //FormCollection loColFormularios;
                //string name = typeof(frm_fuente).AssemblyQualifiedName;
                //Type type = Type.GetType(name);

                //loColFormularios = Application.OpenForms;
                //foreach (Form loForm in loColFormularios)
                //{
                //    if ((loForm.GetType() == loForm.GetType()))
                //    {
                //        frm_fuente FrmChildFather = new frm_fuente();
                //        FrmChildFather.ActualizarBusquedaPorAnio();
                //    }
                //}
            
                FormCollection loColFormularios;
                //string name = typeof(frm_fuente).AssemblyQualifiedName;
                //Type type = Type.GetType(name);

                loColFormularios = Application.OpenForms;
                foreach (Form loForm in loColFormularios)
                {
                    //if ((loForm.GetType() == loForm.GetType()))
                    //{
                    //    frm_fuente FrmChildFather = new frm_fuente();
                    //    FrmChildFather.ActualizarBusquedaPorAnio();
                    //}

                    //Type T = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();

                    //if ((T.Name == frm_fuente))
                    //{
                    //    frm_fuente FrmChildFather = new frm_fuente();
                    //    FrmChildFather.ActualizarBusquedaPorAnio();
                    //}

                    //if (typeof(loForm) is frm_fuente)
                    //{
                    //    frm_fuente FrmChildFather = new frm_fuente();
                    //    FrmChildFather.ActualizarBusquedaPorAnio();
                    //}

                }


            }
            catch (Exception ex)
            {
            }

        }

        public override void ActualizarBusquedaPorPeriodo()
        {
            try
            {
                FormCollection loColFormularios;
                string name = typeof(frm_fuente).AssemblyQualifiedName;
                Type type = Type.GetType(name);

                loColFormularios = Application.OpenForms;
                foreach (Form loForm in loColFormularios)
                {
                    if ((loForm.GetType() == loForm.GetType()))
                    {
                        frm_fuente FrmChildFather = new frm_fuente();
                        FrmChildFather.ActualizarBusquedaPorPerio();
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }


        private void comboperiodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboperiodo.Items.Count > 0)
            {
                Actual_Conexion.PeriodoSelect = comboperiodo.SelectedValue.ToString();
                Accion.RefreshDateDefault();
                ActualizarBusquedaPorPeriodo();
                //Cambio = true;
                frm_fuente FrmChildFather = new frm_fuente();
                FrmChildFather.cambio = true;
            }
        }



        public string usuario;

        List<Entidad_Accesos> Opciones = new List<Entidad_Accesos>();
        List<Entidad_Accesos> SoloGrupos = new List<Entidad_Accesos>();
        DevExpress.XtraTreeList.TreeList di = new DevExpress.XtraTreeList.TreeList();

        bool Existe(Entidad_Accesos op)
        {
            foreach (Entidad_Accesos Grup in SoloGrupos)
            {
                if ((op.Id_Grupo == Grup.Id_Grupo))
                {
                    return true;
                }

            }

            return false;
        }

        ArrayList ExisteSubGrupo(Entidad_Accesos Gr, Entidad_Accesos SGr)
        {
            ArrayList Retorno = new ArrayList();
            foreach (Entidad_Accesos op in Gr.ListaOpciones)
            {
                if ((SGr.Id_Opcion == op.Id_Opcion))
                {
                    Retorno.Add(true);
                    Retorno.Add(op);
                    return Retorno;
                }

            }

            Retorno.Add(false);
            return Retorno;
        }

        void CargarMenu(string NUser)
        {
            Logica_Accesos log = new Logica_Accesos();
            Entidad_Accesos  Entidad = new  Entidad_Accesos ();
            Entidad.Usuario_dni = usuario;

            Opciones = log.Listar_Opciones_Por_Usuario(Entidad);

            // Codigo Modulo
            //List<SEG_OPCIONES> ArbolJerarquias = new List<SEG_OPCIONES>();

            foreach (Entidad_Accesos Op in Opciones)
            {
                if (!Existe(Op))
                {
                    SoloGrupos.Add(Op);
                }

            }

            // Filtrando los subgrupos
            foreach (Entidad_Accesos Gr in SoloGrupos)
            {
                foreach (Entidad_Accesos SGr in Opciones)
                {
                    if ((Gr.Id_Grupo == SGr.Id_Grupo))
                    {
                        ArrayList RR = ExisteSubGrupo(Gr, SGr);

                        if (Convert.ToBoolean(RR[0]) == false)
                        {
                            Gr.ListaOpciones.Add(SGr);
                        }

                    }

                }

            }

            // Creando Menu
            NavBarControl1.OptionsNavPane.ShowOverflowButton = false;
            NavBarControl1.SuspendLayout();
            foreach (Entidad_Accesos Gr in SoloGrupos)
            {
                DevExpress.XtraNavBar.NavBarGroup BotonGrupo = new DevExpress.XtraNavBar.NavBarGroup();
                BotonGrupo.Caption = Gr.Grupo_Descripcion;
                BotonGrupo.Tag = Gr.Id_Grupo;
               // BotonGrupo.SmallImage = global::Contable.Properties.Resources.pincomercial16;// My.Resources.ResourceManager.GetObject(Gr.Icono_opc);
//                BotonGrupo.SmallImages = Resourses

                NavBarControl1.Groups.Add(BotonGrupo);
                BotonGrupo.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer;
                DevExpress.XtraTreeList.TreeList dc = new DevExpress.XtraTreeList.TreeList();
                DevExpress.XtraTreeList.Columns.TreeListColumn colKey = new DevExpress.XtraTreeList.Columns.TreeListColumn();
                colKey.Caption = "treeListColumn1";
                colKey.FieldName = "treeListColumn1";
                colKey.Name = "treeListColumn1";
                colKey.Visible = true;
                colKey.VisibleIndex = 0;
                colKey.OptionsColumn.AllowEdit = false;
                colKey.OptionsColumn.FixedWidth = true;
                colKey.MinWidth = 350;
                colKey.Width = 350;
                DevExpress.XtraTreeList.Columns.TreeListColumn colKey2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
                colKey2.Caption = "treeListColumn2";
                colKey2.FieldName = "treeListColumn2";
                colKey2.Name = "treeListColumn2";
                colKey2.Visible = false;
                colKey2.VisibleIndex = -1;
                colKey2.OptionsColumn.AllowEdit = false;
                dc.Name = "treeList1";
                dc.Dock = DockStyle.Fill;
                dc.OptionsView.ShowHorzLines = false;
                dc.OptionsView.ShowIndicator = false;
                dc.OptionsView.ShowVertLines = false;
                dc.OptionsView.ShowColumns = false;

                dc.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {colKey,colKey2});

                dc.SelectImageList = ImageCollection1;
                BotonGrupo.ControlContainer.Controls.Add(dc);

                dc.Click += new System.EventHandler(this.NodeClick);


                foreach (Entidad_Accesos SGr in Gr.ListaOpciones)
                {
                    dc.BeginUnboundLoad();

                    DevExpress.XtraTreeList.Nodes.TreeListNode NodoPadre = dc.AppendNode(new object[] { SGr.Opcion_Descripcion, SGr.Nombre_frm }, -1, 0, 0, -1);

                    dc.EndUnboundLoad();

 
                }

                di = dc;
                dc.ExpandAll();
                NavBarControl1.ResumeLayout(false);
              //  foreach (NavBarControl.gro Ga in NavBarControl1.Groups)
               // {
               //     NavBarControl1.ActiveGroup = Ga;
               // }

            }

        }


        private void NodeClick(object sender, System.EventArgs e)
        {
          
            try
            {
                DevExpress.XtraTreeList.TreeList TreeList=sender as DevExpress.XtraTreeList.TreeList ;
       
                if ((TreeList.FocusedNode.Nodes.Count == 0))
                {
                    NodoActualSeleccionado(TreeList.FocusedNode, TreeList);
 
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        //static T DirectCast<T>(object o) where T : class
        //{
        //    T value = o as T;
        //    if (value == null && o != null)
        //    {
        //        throw new InvalidCastException();
        //    }
        //    return value;
        //}

        static T DirectCast<T>(object o, Type type) where T : class
        {
            if (!(type.IsInstanceOfType(o)))
            {
                throw new ArgumentException();
            }
            T value = o as T;
            if (value == null && o != null)
            {
                throw new InvalidCastException();
            }
            return value;
        }


        private void NodoActualSeleccionado(DevExpress.XtraTreeList.Nodes.TreeListNode Node, DevExpress.XtraTreeList.TreeList TreeList)
        {

            try
            {
                    string ValueTextForm;
                    ValueTextForm = Node.GetDisplayText(TreeList.Columns[0]);
                    string ValueNameForm;
                    ValueNameForm = Node.GetDisplayText(TreeList.Columns[1]);
                    string ValueOpcionForm;
                    ValueOpcionForm = Node.GetDisplayText(TreeList.Columns[2]);
                    BaseDocument document = null;
                    bool Existe=false;

                    if ((tabbedView3.Documents.Count > 0))
                    {
                        foreach (BaseDocument tab in tabbedView3.Documents)
                        {
                            if ((tab.Caption.ToString().Trim() == ValueTextForm))
                            {
                                Existe = true;
                                document = tab;
                                break;
                            }

                        }

                        if ((Existe == true))
                        {
                            tabbedView3.Controller.Activate(document);
                        }
                        else
                        {
                            //frm_fuente frm;
                            //string Fname = ValueNameForm;
                            //frm = (frm_fuente)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance("Contable." + Fname);
                            //frm.MyIDMenu = ValueOpcionForm;
                            //frm.MdiParent = this;
                            //frm.Show();

                            frm_fuente frm = null;
                            string Fname = ValueNameForm;
                            foreach (Type t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes())
                            {
                                if (t.Name == Fname)
                                {
                                    object o = Activator.CreateInstance(t);
                                    frm = o as frm_fuente;
                                    break;
                                }
                            }

                            frm.MdiParent = this;
                            frm.Show();

                            if (tabbedView3.Documents.TryGetValue(frm,out document))
                            {
                                document.Caption = ValueTextForm;
                            }

                        }

                    }
                    else
                    {
                       // frm_fuente frm;
                        string Fname = ValueNameForm;
                        //frm = (frm_fuente)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(("Win-Contable" + Fname));
                        ////frm.MainForm = Me
                        //frm.MdiParent = this;
                        //frm.Show();


                        string nomvre = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

                    try
                    {
                        frm_fuente frm = null; 
                        foreach (Type t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()) 
                        { 
                            if (t.Name == Fname) 
                            { 
                                object o = Activator.CreateInstance(t);
                                frm = o as frm_fuente;
                                break;
                            } 
                        }

                        frm.MdiParent = this;
                        frm.Show();

                        if (tabbedView3.Documents.TryGetValue(frm, out document))
                        {
                            document.Caption = ValueTextForm;
                        }

                    }catch(Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }





                    }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }




        }




    }
}
