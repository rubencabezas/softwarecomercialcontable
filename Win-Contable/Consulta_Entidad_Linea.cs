﻿using Contable;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Comercial
{
  public  class Consulta_Entidad_Linea
    {


        #region CONSULTAR DE ENTIDAD EN LINEA
        //FUNCION DE REEMPLAZO DE CARACTERES
        private string ReformatCaracterString(string strInput)
        {
            string strOutput = "";
            // Replace accented characters
            strInput = strInput.Replace(@"\u00c0", "À");
            strInput = strInput.Replace(@"\u00c1", "Á");
            strInput = strInput.Replace(@"\u00c2", "Â");
            strInput = strInput.Replace(@"\u00c3", "Ã");
            strInput = strInput.Replace(@"\u00c4", "Ä");
            strInput = strInput.Replace(@"\u00c5", "Å");
            strInput = strInput.Replace(@"\u00c6", "Æ");
            strInput = strInput.Replace(@"\u00c7", "Ç");
            strInput = strInput.Replace(@"\u00c8", "È");
            strInput = strInput.Replace(@"\u00c9", "É");
            strInput = strInput.Replace(@"\u00ca", "Ê");
            strInput = strInput.Replace(@"\u00cb", "Ë");
            strInput = strInput.Replace(@"\u00cc", "Ì");
            strInput = strInput.Replace(@"\u00cd", "Í");
            strInput = strInput.Replace(@"\u00ce", "Î");
            strInput = strInput.Replace(@"\u00cf", "Ï");
            strInput = strInput.Replace(@"\u00d1", "Ñ");
            strInput = strInput.Replace(@"\u00d2", "Ò");
            strInput = strInput.Replace(@"\u00d3", "Ó");
            strInput = strInput.Replace(@"\u00d4", "Ô");
            strInput = strInput.Replace(@"\u00d5", "Õ");
            strInput = strInput.Replace(@"\u00d6", "Ö");
            strInput = strInput.Replace(@"\u00d8", "Ø");
            strInput = strInput.Replace(@"\u00d9", "Ù");
            strInput = strInput.Replace(@"\u00da", "Ú");
            strInput = strInput.Replace(@"\u00db", "Û");
            strInput = strInput.Replace(@"\u00dc", "Ü");
            strInput = strInput.Replace(@"\u00dd", "Ý");
            // Now lower case accents
            strInput = strInput.Replace(@"\u00df", "ß");
            strInput = strInput.Replace(@"\u00e0", "à");
            strInput = strInput.Replace(@"\u00e1", "á");
            strInput = strInput.Replace(@"\u00e2", "â");
            strInput = strInput.Replace(@"\u00e3", "ã");
            strInput = strInput.Replace(@"\u00e4", "ä");
            strInput = strInput.Replace(@"\u00e5", "å");
            strInput = strInput.Replace(@"\u00e6", "æ");
            strInput = strInput.Replace(@"\u00e7", "ç");
            strInput = strInput.Replace(@"\u00e8", "è");
            strInput = strInput.Replace(@"\u00e9", "é");
            strInput = strInput.Replace(@"\u00ea", "ê");
            strInput = strInput.Replace(@"\u00eb", "ë");
            strInput = strInput.Replace(@"\u00ec", "ì");
            strInput = strInput.Replace(@"\u00ed", "í");
            strInput = strInput.Replace(@"\u00ee", "î");
            strInput = strInput.Replace(@"\u00ef", "ï");
            strInput = strInput.Replace(@"\u00f0", "ð");
            strInput = strInput.Replace(@"\u00f1", "ñ");
            strInput = strInput.Replace(@"\u00f2", "ò");
            strInput = strInput.Replace(@"\u00f3", "ó");
            strInput = strInput.Replace(@"\u00f4", "ô");
            strInput = strInput.Replace(@"\u00f5", "õ");
            strInput = strInput.Replace(@"\u00f6", "ö");
            strInput = strInput.Replace(@"\u00f8", "ø");
            strInput = strInput.Replace(@"\u00f9", "ù");
            strInput = strInput.Replace(@"\u00fa", "ú");
            strInput = strInput.Replace(@"\u00fb", "û");
            strInput = strInput.Replace(@"\u00fc", "ü");
            strInput = strInput.Replace(@"\u00fd", "ý");
            strInput = strInput.Replace(@"\u00ff", "ÿ");
            strOutput = strInput.ToUpper();
            return strOutput;
        }

        //EXTRAER DATOS DE UNA CADENA HASTA UN CARACTER ESPECIFICO
        private string ExtractData(string Dato, string Caracter)
        {
            byte index = System.Convert.ToByte(Dato.IndexOf(Caracter));
            string str = Dato.Substring(0, index);
            return str.Trim();
        }

        //REESTRUCTURAR DATOS 
        private void ReconstructionNameSUNAT(Entidad_Entidad DatosConsulta)
        {
            string Val = DatosConsulta.Ent_RUC_DNI.Substring(0, 1);
            if (Val == "1")
            {
                string RZ_Temporal = DatosConsulta.Ent_Razon_Social_Nombre;
                DatosConsulta.Ent_Ape_Paterno = ExtractData(RZ_Temporal, " ").Trim();
                RZ_Temporal = RZ_Temporal.Remove(0, DatosConsulta.Ent_Ape_Paterno.Length).Trim();
                DatosConsulta.Ent_Ape_Materno = (ExtractData(RZ_Temporal, " ").Trim()).TrimEnd(',');
                RZ_Temporal = RZ_Temporal.Remove(0, DatosConsulta.Ent_Ape_Materno.Length).Trim();
                DatosConsulta.Ent_Razon_Social_Nombre = RZ_Temporal;

                ConsultDataRENIEC01(DatosConsulta, true);
            }
            else if (Val == "2")
            {
                DatosConsulta.Ent_Razon_Social_Nombre = DatosConsulta.Ent_Razon_Social_Nombre;
            }
        }
        //CONSULTAS DE INFORMACION A RENIEC
        private void ConsultDataRENIEC01(Entidad_Entidad DatosConsulta, bool opt = false)
        {
            try
            {
                string DNI = DatosConsulta.Ent_RUC_DNI;
                if (DNI.Trim().Length == 11)
                {
                    DNI = DNI.Substring(2, 8);
                }
                WebClient CLIENTE = new WebClient();
                string myUrl = String.Format("http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI={0}", DNI);
                Stream PAGINA = CLIENTE.OpenRead(myUrl);
                StreamReader LECTOR = new StreamReader(PAGINA);
                string MIHTML = LECTOR.ReadToEnd();
                MIHTML = (MIHTML.Replace("\n", "").Replace("\r", "").Replace("<br />", "").Replace("]", "").Replace("[", "")).Trim().Trim('"');
                string[] datosRpta = MIHTML.Split(new char[] { '|' });
                DatosConsulta.Ent_Ape_Paterno = datosRpta[0];
                DatosConsulta.Ent_Ape_Materno = datosRpta[1];
                DatosConsulta.Ent_Razon_Social_Nombre = datosRpta[2];
                PAGINA.Close();
            }
            catch (Exception ex)
            {
                if (opt == false)
                {
                    DatosConsulta = new Entidad_Entidad()/* TODO Change to default(_) if this is not a reference type */;
                }
            }
        }
        private Entidad_Entidad ConsultDataRENIEC02(string DNI)
        {
            Entidad_Entidad DatosConsulta = new Entidad_Entidad();
            try
            {
                DatosConsulta.Ent_RUC_DNI = DNI;
                try
                {
                    ConsultDataRENIEC01(DatosConsulta);
                }
                catch (Exception ex)
                {
                }


                var url = $"https://searchpe.herokuapp.com/public/api/dni/" + DNI;
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Accept = "application/json";

                try
                {
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream strReader = response.GetResponseStream())
                        {
                            if (strReader == null) {
                                //return; 
                                DatosConsulta = new Entidad_Entidad();
                            }
                            using (StreamReader objReader = new StreamReader(strReader))
                            {
                                string responseBody = objReader.ReadToEnd();
                                // Do something with responseBody
                                Console.WriteLine(responseBody);
                                // lo qeu devuelve : {"dni":"45344025","nombres":"RUBEN KELVIN","apellidoPaterno":"CABEZAS","apellidoMaterno":"HUANIO","codVerifica":2}
                                var res = JsonConvert.DeserializeObject<dynamic>(responseBody);
                                string _Name = res["dni"].ToString();

                                if (string.IsNullOrWhiteSpace(DatosConsulta.Ent_Razon_Social_Nombre))
                                {

                                    DatosConsulta.Ent_Razon_Social_Nombre = res["nombres"].ToString();
                                    DatosConsulta.Ent_Ape_Paterno = res["apellidoPaterno"].ToString();
                                    DatosConsulta.Ent_Ape_Materno = res["apellidoMaterno"].ToString();
                                }

                            }
                        }
                    }
                }
                catch (WebException ex)
                {
                    // Handle error
                }





                //HttpWebRequest webRequest;

                //string requestParams = "{\"CODDNI\":\"45344025\"}"; //format information you need to pass into that string ('info={ "EmployeeID": [ "1234567", "7654321" ], "Salary": true, "BonusPercentage": 10}');

                //webRequest = (HttpWebRequest)WebRequest.Create("https://aplicaciones007.jne.gob.pe/srop_publico/Consulta/api/AfiliadoApi/GetNombresCiudadano");

                //webRequest.Method = "POST";
                //webRequest.ContentType = "application/json";
                ////webRequest.ContentType.to

                //byte[] byteArray = Encoding.UTF8.GetBytes(requestParams);
                //webRequest.ContentLength = byteArray.Length;
                //using (Stream requestStream = webRequest.GetRequestStream())
                //{
                //    requestStream.Write(byteArray, 0, byteArray.Length);
                //}

                //// Get the response.
                //using (WebResponse response = webRequest.GetResponse())
                //{
                //    using (Stream responseStream = response.GetResponseStream())
                //    {
                //        StreamReader rdr = new StreamReader(responseStream, Encoding.UTF8);
                //        string Json = rdr.ReadToEnd(); // response from server

                //    }
                //}



                //using (var httpClient = new HttpClient())
                //{
                //    using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://aplicaciones007.jne.gob.pe/srop_publico/Consulta/api/AfiliadoApi/GetNombresCiudadano"))
                //    {
                //        request.Headers.TryAddWithoutValidation("RequestVerificationToken", "30OB7qfO2MmL2Kcr1z4S0ttQcQpxH9pDUlZnkJPVgUhZOGBuSbGU4qM83JcSu7DZpZw-IIIfaDZgZ4vDbwE5-L9EPoBIHOOC1aSPi4FS_Sc1:clDOiaq7mKcLTK9YBVGt2R3spEU8LhtXEe_n5VG5VLPfG9UkAQfjL_WT9ZDmCCqtJypoTD26ikncynlMn8fPz_F_Y88WFufli38cUM-24PE1");

                //        request.Content = new StringContent("{\"CODDNI\":\"46658592\"}");
                //        request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json;chartset=utf-8");

                //       var response =   httpClient.SendAsync(request);
                //    }
                //}


                //*********************************************

                //string NOMBRES, DISTRITO, PROVINCIA, DEPARTAMENTO;
                //WebClient CLIENTE = new WebClient();
                //string myUrl = String.Format("http://clientes.reniec.gob.pe/padronElectoral2012/consulta.htm?hTipo=2&hDni={0}", DNI);

                //Stream PAGINA = CLIENTE.OpenRead(myUrl);
                //StreamReader LECTOR = new StreamReader(PAGINA);
                //string MIHTML = LECTOR.ReadToEnd();
                //MIHTML = (MIHTML.Replace("\n", "").Replace("\r", "").Replace("<br />", "").Replace("]", "").Replace("[", "")).Trim().Trim('"');

                //try
                //{

                //    if (string.IsNullOrWhiteSpace(DatosConsulta.Ent_Razon_Social_Nombre))
                //    {
                //        NOMBRES = MIHTML.Remove(0, MIHTML.IndexOf("Apellidos y Nombres") + 79);
                //        NOMBRES = NOMBRES.Substring(0, NOMBRES.IndexOf("</td>")).Trim();
                //        DatosConsulta.Ent_Razon_Social_Nombre = NOMBRES.Substring(NOMBRES.IndexOf(",") + 1, NOMBRES.Length - NOMBRES.IndexOf(",") - 1).Trim();
                //        NOMBRES = NOMBRES.Substring(0, NOMBRES.IndexOf(",")).Trim();
                //        DatosConsulta.Ent_Ape_Paterno = NOMBRES.Substring(0, NOMBRES.IndexOf(" "));
                //        NOMBRES = NOMBRES.Remove(0, NOMBRES.IndexOf(" ")).Trim();
                //        DatosConsulta.Ent_Ape_Materno = NOMBRES.Trim();
                //    }
                //}
                //catch (Exception ex)
                //{
                //    DatosConsulta = new Entidad_Entidad()/* TODO Change to default(_) if this is not a reference type */;
                //}

                //try
                //{
                //    if (MIHTML.Contains("NO ES POSIBLE PROPORCIONAR LA INFORMACI&Oacute;N SOLICITADA"))
                //        // Datos_Sunat.Estado_No_Existe = True
                //        // MessageBox.Show("No Existe DNI", "RENIEC")
                //        // Return Datos_Sunat
                //        DatosConsulta.Ent_Domicilio_Fiscal = "";
                //    else
                //    {
                //        DISTRITO = MIHTML.Remove(0, MIHTML.IndexOf("Distrito / Ciudad") + ("Distrito / Ciudad").Length + 48);
                //        DISTRITO = DISTRITO.Substring(0, DISTRITO.IndexOf("</td>")).Trim();

                //        PROVINCIA = MIHTML.Remove(0, MIHTML.IndexOf("Provincia / Pa&iacute;s") + ("Provincia / Pa&iacute;s").Length + 48);
                //        PROVINCIA = PROVINCIA.Substring(0, PROVINCIA.IndexOf("</td>")).Trim();

                //        DEPARTAMENTO = MIHTML.Remove(0, MIHTML.IndexOf("Departamento / Continente") + ("Departamento / Continente").Length + 48);
                //        DEPARTAMENTO = DEPARTAMENTO.Substring(0, DEPARTAMENTO.IndexOf("</td>")).Trim();

                //        DatosConsulta.Ent_Domicilio_Fiscal = DISTRITO + " - " + PROVINCIA + " - " + DEPARTAMENTO;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    DatosConsulta.Ent_Domicilio_Fiscal = "";
                //}

                //PAGINA.Close();

            }
            catch (Exception ex)
                {
                    DatosConsulta = new Entidad_Entidad();/* TODO Change to default(_) if this is not a reference type */;
                }

            return DatosConsulta;
        }

        
 
        private static void ConsumirServicioDNI(string DNI)
        {
            var url = $"https://searchpe.herokuapp.com/public/api/dni/"+ DNI;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            // Do something with responseBody
                            Console.WriteLine(responseBody);
                            // lo qeu devuelve : {"dni":"45344025","nombres":"RUBEN KELVIN","apellidoPaterno":"CABEZAS","apellidoMaterno":"HUANIO","codVerifica":2}
                            var res = JsonConvert.DeserializeObject<dynamic>(responseBody);
                            string _Name = res["dni"].ToString();
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                // Handle error
            }
        }

        //CONSULTAS DE INFORMACION A SUNAT 
        private Entidad_Entidad ConsultDataSUNAT(string DNI)
        {
            Entidad_Entidad DatosConsulta = new Entidad_Entidad();
            DatosConsulta.Ent_RUC_DNI = DNI;
            try
            {
                WebClient CLIENTE = new WebClient();
                string myUrl = String.Format("https://api.sunat.cloud/ruc/{0}", DatosConsulta.Ent_RUC_DNI);
                Stream PAGINA = new MemoryStream();
                //PAGINA.ReadTimeout=15000;
                PAGINA = CLIENTE.OpenRead(myUrl);
                StreamReader LECTOR = new StreamReader(PAGINA);
                string MIHTML = LECTOR.ReadToEnd();
                MIHTML = MIHTML.TrimStart(new char[] { '"', '{', '\n' }).Trim();
                MIHTML = MIHTML.Replace('\"'.ToString(), "");
                MIHTML = MIHTML.Replace('"'.ToString(), "");
                string[] datosRpta = MIHTML.Split(new char[] { '\n' });
                bool Existe_RUC = false;
                foreach (string st in datosRpta)
                {
                    string item = st.Trim();
                    int it = item.IndexOf(":") + 1;
                    if (item.Contains("ruc"))
                    {
                        DatosConsulta.Ent_RUC_DNI = item.Remove(0, it).Trim().TrimEnd(new char[] { ',' }).Trim();
                        Existe_RUC = true;
                    }
                    else if (item.Contains("razon_social"))
                    {
                        DatosConsulta.Ent_Razon_Social_Nombre = item.Remove(0, it).Trim().TrimEnd(new char[] { ',' }).Trim();
                        DatosConsulta.Ent_Razon_Social_Nombre = ReformatCaracterString(DatosConsulta.Ent_Razon_Social_Nombre);
                        ReconstructionNameSUNAT(DatosConsulta);
                    }
                    else if (item.Contains("domicilio_fiscal"))
                    {
                        DatosConsulta.Ent_Domicilio_Fiscal = item.Remove(0, it).Trim().TrimEnd(new char[] { ',' }).Trim();
                        DatosConsulta.Ent_Domicilio_Fiscal = ReformatCaracterString(DatosConsulta.Ent_Domicilio_Fiscal);
                    }
                }

                if (Existe_RUC == false)
                {
                    DatosConsulta = new Entidad_Entidad()/* TODO Change to default(_) if this is not a reference type */;
                }

                PAGINA.Close();
            }
            catch (Exception ex)
            {
                DatosConsulta = new Entidad_Entidad()/* TODO Change to default(_) if this is not a reference type */;

            }
            return DatosConsulta;
        }
        public Entidad_Entidad Buscar_Entidad_Linea(string rucdni)
        {
            try
            {
                Entidad_Entidad cat = new Entidad_Entidad();

                if (rucdni.Length == 8)
                {
                    cat = ConsultDataRENIEC02(rucdni);
                }
                else if (rucdni.Length == 11)
                {
                    cat = ConsultDataSUNAT(rucdni);
                }
                else
                {
                    cat = new Entidad_Entidad();
                }
                return cat;
                //return Json(data);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


    }
}
