
CREATE FUNCTION [dbo].[FN_FACTE_VERIFICAR_PROGRAMACION_RESUMEN] (
@RUC CHAR(11),
@TP_DOC CHAR(2),
@SERIE_DOC CHAR(4),
@ES_RESUMEN BIT ,
@ES_BAJA BIT 
)
RETURNS BIT
AS 
BEGIN
DECLARE @Retornar BIT=NULL

/*PARAMETROS DE RETORNO
	NULL= No tiene programacion establecida 
	0 = Mostrar todo los habilitados (baja, y los no afectados por el resumen de baja) 
	1 = Existe resumen de boletas */

--ACTUALIZAR 
SET @ES_RESUMEN=ISNULL(@ES_RESUMEN,0)
SET @ES_BAJA=ISNULL(@ES_BAJA,0)

DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

IF @ES_RESUMEN=0 AND @ES_BAJA=0
BEGIN
	--VERIFICAMOS SI EXISTE LA PROGRAMACION DE ENVIO DE COMPROBANTES
	IF EXISTS(SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO DocTp ON 
    DocTp.TipoDoc_Codigo=ProgX.Progra_Appli_TipoDoc
	WHERE ProgX.Emp_RUC = @RUC
	AND DocTp.TipoDoc_Codigo=@TP_DOC  
	AND ProgX.Progra_Appli_TipoDoc = ProgX.Progra_Appli_TipoDoc 
	AND ProgX.Progra_EsResumenBoletas = @ES_RESUMEN 
	AND ProgX.Progra_EsComunicacionBaja = @ES_BAJA   
	AND ProgX.Progra_Habilitado = 1
	AND ProgX.Progra_Estado='001'
	AND @AhoraFechaHora >= ProgX.Progra_Fecha_Desde 
	AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora 
	AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora)
	BEGIN
		--VERIFICAR SI HAY RESUMEN Y SELECCIONAR LOS QUE NO SON DE RESUMEN
		SET @Retornar = (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI NO CONSIDERA BOLETAS NI SUS NOTAS DE CREDITO Y DEBITO
		INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
		ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
		WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
		AND ProgX.Progra_Estado='001' 
		AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
		AND ProgX.Emp_RUC=@RUC) 
		THEN (CASE WHEN (@TP_DOC ='03' OR @TP_DOC ='07' OR @TP_DOC ='08') 
		THEN (CASE WHEN @SERIE_DOC LIKE 'B%' THEN 1 ELSE 0 END)
		ELSE 0 END) 
		ELSE 0 END) 
	END

RETURN @Retornar
END
--IF (@ES_RESUMEN=1 OR @ES_BAJA=1)
--BEGIN
--END



RETURN @Retornar
END


GO
 

--CREATE PROC [dbo].[paVTS_PRODUCTO_STOCK_BUSCAR_BARRA]
--  (             
-- @PEmp_cCodigo Char(3),           
-- @PTipoPro char(4),           
-- @PAlm_Codigo CHAR(5),     
-- @PCTC_CodigoBarra VARCHAR(40)=NULL
--  )             
--AS BEGIN             
-- set nocount on  
-- IF @PTipoPro='1011' BEGIN
--	SELECT    
--    LCC.CTC_Codigo,LTRIM(RTRIM(lcc.CTC_DescripCorta)),  ISNULL(RTRIM(LTRIM(lcc.CTC_DescripLarga)),''),            
--  lcc.GRC_Codigo,            
--  lcc.CLC_Codigo,            
--  lcc.FMC_Codigo,            
--  ISNULL(lcc.CTC_CodigoBarra,''),            
--  ISNULL(lcc.CTC_CodigoPropio,''),            
--  ISNULL(lcc.Tab_cCodigo,''),            
--  ISNULL(lcc.CTC_ControlStock,''),            
--  ISNULL(lcc.CTC_StockMin,''),            
--  ISNULL(lcc.CTC_StockMax,''),            
--  ISNULL(lcc.CTC_StockReorden,''),            
--  ISNULL(lcc.Pai_cCodigo,''),            
--  ISNULL(lcc.CTC_LoteSerie,''),            
--  ISNULL(lcc.CTC_ConjuntoCATALOGO,''),                       
--  '',   
--  lcm.Mar_cDescripCorta,            
--  ISNULL(lcc.CTC_Serie,''), 
--    lum.UNM_Descripcion,
--	imd.Stock
--	,ISNULL(lum.UNM_CoigodFE,'') AS UNM_CoigodFE   ,
--	ISNULL(lum.UNM_Abreviado,'') AS UNM_Descripcion
--FROM INV_ALMACEN_STOCK imd WITH(NOLOCK)

--inner join LGT_CATALOGO lcc WITH(NOLOCK) on          
--imd.Emp_cCodigo=lcc.Emp_cCodigo and         
--lcc.CTC_Tipo=@PTipoPro and imd.CTC_Codigo= lcc.CTC_Codigo

--left join LGT_CATALOGO_MARCA lcm WITH(NOLOCK) on 
--lcm.Emp_cCodigo=lcc.Emp_cCodigo and 
--lcm.Mar_cMarca=lcc.Mar_cMarca
         
--inner join GNL_UNIDAD_MEDIDA lum WITH(NOLOCK) on         
--imd.CTC_CodUnidad=lum.UNM_Codigo        

         
--where           
--imd.Emp_cCodigo=@PEmp_cCodigo and           
--imd.ALM_Codigo= @PAlm_Codigo   
--and lcc.CTC_Estado='0001'     
--AND (@PCTC_CodigoBarra IS NULL OR lcc.CTC_CodigoBarra =@PCTC_CodigoBarra)

-- END
    
--  IF @PTipoPro='1012' BEGIN
--	SELECT    
--    LCC.CTC_Codigo,LTRIM(RTRIM(lcc.CTC_DescripCorta)),  ISNULL(LTRIM(RTRIM(lcc.CTC_DescripLarga)),''),            
--  lcc.GRC_Codigo,            
--  lcc.CLC_Codigo,            
--  lcc.FMC_Codigo,            
--  ISNULL(lcc.CTC_CodigoBarra,''),            
--  ISNULL(lcc.CTC_CodigoPropio,''),            
--  ISNULL(lcc.Tab_cCodigo,''),            
--  ISNULL(lcc.CTC_ControlStock,''),            
--  ISNULL(lcc.CTC_StockMin,0),            
--  ISNULL(lcc.CTC_StockMax,0),            
--  ISNULL(lcc.CTC_StockReorden,0),            
--  ISNULL(lcc.Pai_cCodigo,''),            
--  ISNULL(lcc.CTC_LoteSerie,''),            
--  ISNULL(lcc.CTC_ConjuntoCATALOGO,''),                        
--  '',            
--  '' Mar_cDescripCorta,
--  ISNULL(lcc.CTC_Serie,''), 
--  lum.UNM_Descripcion
--  ,1.00 AS Stock,
--  	ISNULL(lum.UNM_CoigodFE,'') AS UNM_CoigodFE   ,
--	ISNULL(lum.UNM_Abreviado,'') AS UNM_Descripcion
--FROM  LGT_CATALOGO lcc WITH(NOLOCK)
 
--inner join GNL_UNIDAD_MEDIDA lum WITH(NOLOCK) on         
--lcc.Tab_cCodigo=lum.UNM_Codigo        
--inner join LGT_CATALOGO_GRUPO g on g.Emp_cCodigo=lcc.Emp_cCodigo and g.grc_codigo=lcc.grc_codigo and g.ctc_tipo =lcc.ctc_tipo 
         
--where           
--lcc.Emp_cCodigo=@PEmp_cCodigo and           
--lcc.CTC_Tipo=@PTipoPro and
--lcc.CTC_Estado='0001' and    
--(g.GRC_Proceso IS NULL) OR (g.GRC_Proceso='1106') AND 
--(@PCTC_CodigoBarra IS NULL OR lcc.CTC_CodigoBarra =@PCTC_CodigoBarra)

-- END       

--SET NOCOUNT  OFF             
--END

--GO
 
CREATE PROCEDURE [dbo].[UP_BACK_01_FACTE_SOCIO_NEGOCIO]
(
@PFecha DATE,
@PHasta DATE
)
AS
BEGIN
	SELECT DISTINCT TipoDocIdent_Codigo, SocNeg_TipoDocIdent_Numero, SocNeg_RazonSocialNombres, ISNULL(SocNeg_Correo_Electronico,'') 
	FROM FACTE_SOCIO_NEGOCIO Soc WITH(NOLOCK) 
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON 
	Doc.Doc_Cliente_TipoDocIdent_Codigo = Soc.TipoDocIdent_Codigo 
	AND Doc.Doc_Cliente_TipoDocIdent_Numero = Soc.SocNeg_TipoDocIdent_Numero
	WHERE Doc.Doc_FechaEmision BETWEEN @PFecha AND @PHasta;
END


GO
 

CREATE PROCEDURE [dbo].[UP_BACK_01_REPLICA_FACTE_SOCIO_NEGOCIO]
AS
BEGIN
	SELECT TipoDocIdent_Codigo, SocNeg_TipoDocIdent_Numero, SocNeg_RazonSocialNombres, ISNULL(SocNeg_Correo_Electronico,'') 
	FROM FACTE_SOCIO_NEGOCIO Soc WITH(NOLOCK) 
	WHERE SOC.Replicacion_Estado=0;
END


GO
 


CREATE PROCEDURE [dbo].[UP_BACK_01_UP_FACTE_SOCIO_NEGOCIO]
(
@PTipoDocIdent_Codigo CHAR(1), 
@PSocNeg_TipoDocIdent_Numero VARCHAR(15), 
@PSocNeg_RazonSocialNombres VARCHAR(150), 
@PSocNeg_Correo_Electronico VARCHAR(150)
)
AS
BEGIN
	IF EXISTS(SELECT Soc.TipoDocIdent_Codigo FROM FACTE_SOCIO_NEGOCIO Soc WITH(NOLOCK) WHERE Soc.TipoDocIdent_Codigo = @PTipoDocIdent_Codigo AND Soc.SocNeg_TipoDocIdent_Numero = @PSocNeg_TipoDocIdent_Numero)
	BEGIN
		UPDATE FACTE_SOCIO_NEGOCIO SET SocNeg_RazonSocialNombres = @PSocNeg_RazonSocialNombres, SocNeg_Correo_Electronico = CASE WHEN (LTRIM(RTRIM(@PSocNeg_Correo_Electronico))='') THEN SocNeg_Correo_Electronico ELSE @PSocNeg_Correo_Electronico END 
		WHERE TipoDocIdent_Codigo = @PTipoDocIdent_Codigo AND SocNeg_TipoDocIdent_Numero = @PSocNeg_TipoDocIdent_Numero ;
	END
	ELSE
	BEGIN
		INSERT INTO FACTE_SOCIO_NEGOCIO
		( TipoDocIdent_Codigo , SocNeg_TipoDocIdent_Numero , SocNeg_RazonSocialNombres , SocNeg_Correo_Electronico )
		VALUES  ( @PTipoDocIdent_Codigo , @PSocNeg_TipoDocIdent_Numero , @PSocNeg_RazonSocialNombres , @PSocNeg_Correo_Electronico );
	END
END


GO
 

CREATE PROCEDURE [dbo].[UP_BACK_01_UP_REPLICA_FACTE_SOCIO_NEGOCIO]
(
@PTipoDocIdent_Codigo CHAR(1), 
@PSocNeg_TipoDocIdent_Numero VARCHAR(15)
)
AS
BEGIN
	UPDATE FACTE_SOCIO_NEGOCIO SET Replicacion_Estado = 1
	WHERE TipoDocIdent_Codigo = @PTipoDocIdent_Codigo AND SocNeg_TipoDocIdent_Numero = @PSocNeg_TipoDocIdent_Numero ;
END


GO
 

CREATE PROCEDURE [dbo].[UP_BACK_02_FACTE_DOCUMENTO_ELECTRONICO]
(
@PFecha DATE,
@PHasta DATE
)
AS
BEGIN
	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero, Doc.Doc_FechaEmision, Doc.Doc_Moneda, Doc.Doc_Oper_Gravadas, 
	Doc.Doc_Oper_Exoneradas, Doc.Doc_Oper_Inafectas, Doc.Doc_Oper_Gratuitas, Doc.Doc_ISC, Doc.Doc_Otros_Tributos, Doc.Doc_Otros_Conceptos, 
	Doc.Doc_IGV, Doc.Doc_Importe, ISNULL(Doc_Certificado_Correlativo,0) AS Doc_Certificado_Correlativo, Doc_Cliente_TipoDocIdent_Codigo, Doc_Cliente_TipoDocIdent_Numero, 
	ISNULL(Doc_Modif_Doc_Tipo,'') AS Doc_Modif_Doc_Tipo, ISNULL(Doc_Modif_Doc_Serie,'') AS Doc_Modif_Doc_Serie, ISNULL(Doc_Modif_Doc_Numero,'') AS Doc_Modif_Doc_Numero, Doc_NombreArchivoXML, 
	ISNULL(Doc_NombreArchivoPDF,'') AS Doc_NombreArchivoPDF, Doc_NombreArchivoWeb, ISNULL(Doc_Resumen_FechaGeneracion,'01-01-01') AS Doc_Resumen_FechaGeneracion, 
	ISNULL(Doc_Resumen_Correlativo,0) AS Doc_Resumen_Correlativo, ISNULL(Doc_Baja_GrpTipo_Codigo,'') AS Doc_Baja_GrpTipo_Codigo, ISNULL(Doc_Baja_FechaGeneracion,'01-01-01') AS Doc_Baja_FechaGeneracion, 
	ISNULL(Doc_Baja_Correlativo,0) AS Doc_Baja_Correlativo, ISNULL(Doc_Correo_Enviado,0) AS Doc_Correo_Enviado,ISNULL(Doc_Correo_EstaLeido,0) AS Doc_Correo_EstaLeido, ISNULL(Doc_Correo_ContadorLectura,0) AS  Doc_Correo_ContadorLectura, ISNULL( Doc_Correo_LecturaFechaPrimera,'01-01-01') AS Doc_Correo_LecturaFechaPrimera, 
	ISNULL(Doc_Correo_LecturaFechaUltima,'01-01-01') AS Doc_Correo_LecturaFechaUltima, EstDoc_Codigo, Emp.Path_Root_App, ISNULL(DocXML.EstEnv_Codigo,'') AS EstEnv_Codigo
	FROM FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Doc.Doc_Emisor_RUC
	INNER JOIN FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1 AND DocXML.TipXML_Codigo<>'003'
	WHERE Doc.Doc_FechaEmision BETWEEN @PFecha AND @PHasta;
END


GO
 

CREATE PROCEDURE [dbo].[UP_BACK_02_REPLICA_FACTE_DOCUMENTO_ELECTRONICO]
AS
BEGIN
	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero, Doc.Doc_FechaEmision, Doc.Doc_Moneda, Doc.Doc_Oper_Gravadas, Doc.Doc_Oper_Exoneradas, Doc.Doc_Oper_Inafectas, Doc.Doc_Oper_Gratuitas, Doc.Doc_ISC, Doc.Doc_Otros_Tributos, Doc.Doc_Otros_Conceptos, Doc.Doc_IGV, Doc.Doc_Importe, ISNULL(Doc_Certificado_Correlativo,0), Doc_Cliente_TipoDocIdent_Codigo, Doc_Cliente_TipoDocIdent_Numero, ISNULL(Doc_Modif_Doc_Tipo,''), ISNULL(Doc_Modif_Doc_Serie,''), ISNULL(Doc_Modif_Doc_Numero,''), Doc_NombreArchivoXML, ISNULL(Doc_NombreArchivoPDF,''), Doc_NombreArchivoWeb, ISNULL(Doc_Resumen_FechaGeneracion,'01-01-01'), ISNULL(Doc_Resumen_Correlativo,0), ISNULL(Doc_Baja_GrpTipo_Codigo,''), ISNULL(Doc_Baja_FechaGeneracion,'01-01-01'), ISNULL(Doc_Baja_Correlativo,0), ISNULL(Doc_Correo_Enviado,0), ISNULL(Doc_Correo_EstaLeido,0), ISNULL(Doc_Correo_ContadorLectura,0), ISNULL( Doc_Correo_LecturaFechaPrimera,'01-01-01'), ISNULL(Doc_Correo_LecturaFechaUltima,'01-01-01'), EstDoc_Codigo, Emp.Path_Root_App, ISNULL(DocXML.EstEnv_Codigo,'')
	FROM FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Doc.Doc_Emisor_RUC
	INNER JOIN FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1 AND DocXML.TipXML_Codigo<>'003'
	WHERE Doc.Replicacion_Estado=0 AND DocXML.EstEnv_Codigo='003' ;
END


GO



CREATE PROCEDURE [dbo].[UP_BACK_02_REPLICA_UP_FACTE_DOCUMENTO_ELECTRONICO]
(
@PDoc_Emisor_RUC CHAR(11) ,
@PDoc_Tipo CHAR(2) ,
@PDoc_Serie CHAR(4) ,
@PDoc_Numero CHAR(8)
)
AS
BEGIN
	UPDATE FACTE_DOCUMENTO_ELECTRONICO SET Replicacion_Estado = 1
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero ;
END


GO



CREATE PROCEDURE [dbo].[UP_BACK_02_UP_FACTE_DOCUMENTO_ELECTRONICO]
(
@PDoc_Emisor_RUC CHAR(11) ,
@PDoc_Tipo CHAR(2) ,
@PDoc_Serie CHAR(4) ,
@PDoc_Numero CHAR(8) ,
@PDoc_FechaEmision DATE ,
@PDoc_Moneda CHAR(3) ,
@PDoc_Oper_Gravadas DECIMAL(15, 2) ,
@PDoc_Oper_Exoneradas DECIMAL(15, 2) ,
@PDoc_Oper_Inafectas DECIMAL(15, 2) ,
@PDoc_Oper_Gratuitas DECIMAL(15, 2) ,
@PDoc_ISC DECIMAL(15, 2) ,
@PDoc_Otros_Tributos DECIMAL(15, 2) ,
@PDoc_Otros_Conceptos DECIMAL(15, 2) ,
@PDoc_IGV DECIMAL(15, 2) ,
@PDoc_Importe DECIMAL(15, 2) ,
@PDoc_Certificado_Correlativo INT ,
@PDoc_Cliente_TipoDocIdent_Codigo CHAR(1) ,
@PDoc_Cliente_TipoDocIdent_Numero VARCHAR(15) ,
@PDoc_Modif_Doc_Tipo CHAR(2) ,
@PDoc_Modif_Doc_Serie CHAR(4) ,
@PDoc_Modif_Doc_Numero VARCHAR(8) ,
@PDoc_NombreArchivoXML VARCHAR(50) ,
@PDoc_NombreArchivoPDF VARCHAR(50) ,
@PDoc_NombreArchivoWeb VARCHAR(50) ,
@PDoc_Resumen_FechaGeneracion DATE ,
@PDoc_Resumen_Correlativo INT ,
@PDoc_Baja_GrpTipo_Codigo CHAR(3) ,
@PDoc_Baja_FechaGeneracion DATE ,
@PDoc_Baja_Correlativo INT ,
@PDoc_Correo_Enviado BIT ,
@PDoc_Correo_EstaLeido BIT ,
@PDoc_Correo_ContadorLectura INT ,
@PDoc_Correo_LecturaFechaPrimera DATETIME ,
@PDoc_Correo_LecturaFechaUltima DATETIME,
@PEstDoc_Codigo CHAR(3)
)
AS
BEGIN
	IF EXISTS(SELECT doc.Doc_Emisor_RUC FROM FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_Tipo = @PDoc_Tipo AND Doc.Doc_Serie = @PDoc_Serie AND Doc.Doc_Numero = @PDoc_Numero)
	BEGIN
		UPDATE FACTE_DOCUMENTO_ELECTRONICO SET 
		Doc_FechaEmision = @PDoc_FechaEmision , Doc_Moneda = @PDoc_Moneda , 
		Doc_Oper_Gravadas = @PDoc_Oper_Gravadas , Doc_Oper_Exoneradas = @PDoc_Oper_Exoneradas , 
		Doc_Oper_Inafectas = @PDoc_Oper_Inafectas , Doc_Oper_Gratuitas = @PDoc_Oper_Gratuitas ,
		Doc_ISC = @PDoc_ISC , Doc_Otros_Tributos = @PDoc_Otros_Tributos , 
		Doc_Otros_Conceptos = @PDoc_Otros_Conceptos , Doc_IGV = @PDoc_IGV , Doc_Importe = @PDoc_Importe ,
		Doc_Certificado_Correlativo = @PDoc_Certificado_Correlativo , 
		Doc_Cliente_TipoDocIdent_Codigo = Doc_Cliente_TipoDocIdent_Codigo , Doc_Cliente_TipoDocIdent_Numero = @PDoc_Cliente_TipoDocIdent_Numero ,
		Doc_Modif_Doc_Tipo = @PDoc_Modif_Doc_Tipo , Doc_Modif_Doc_Serie = @PDoc_Modif_Doc_Serie ,  Doc_Modif_Doc_Numero = @PDoc_Modif_Doc_Numero ,
		Doc_NombreArchivoXML = @PDoc_NombreArchivoXML , Doc_NombreArchivoPDF = @PDoc_NombreArchivoPDF ,  Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ,
		Doc_Resumen_FechaGeneracion = @PDoc_Resumen_FechaGeneracion ,  Doc_Resumen_Correlativo = @PDoc_Resumen_Correlativo , Doc_Baja_GrpTipo_Codigo = @PDoc_Baja_GrpTipo_Codigo ,
		Doc_Baja_FechaGeneracion = @PDoc_Baja_FechaGeneracion , Doc_Baja_Correlativo = @PDoc_Baja_Correlativo ,
		Doc_Correo_Enviado = @PDoc_Correo_Enviado , Doc_Correo_EstaLeido = @PDoc_Correo_EstaLeido ,
		Doc_Correo_ContadorLectura = @PDoc_Correo_ContadorLectura , Doc_Correo_LecturaFechaPrimera = @PDoc_Correo_LecturaFechaPrimera , Doc_Correo_LecturaFechaUltima = @PDoc_Correo_LecturaFechaUltima ,
		EstDoc_Codigo = @PEstDoc_Codigo
		WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero ;
	END
	ELSE
    BEGIN
		INSERT INTO FACTE_DOCUMENTO_ELECTRONICO
				( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
				  Doc_FechaEmision , Doc_Moneda , 
				  Doc_Oper_Gravadas , Doc_Oper_Exoneradas , Doc_Oper_Inafectas , Doc_Oper_Gratuitas ,
				  Doc_ISC , Doc_Otros_Tributos , Doc_Otros_Conceptos , Doc_IGV , Doc_Importe ,
				  Doc_Certificado_Correlativo , Doc_Cliente_TipoDocIdent_Codigo , Doc_Cliente_TipoDocIdent_Numero ,
				  Doc_Modif_Doc_Tipo , Doc_Modif_Doc_Serie ,  Doc_Modif_Doc_Numero ,
				  Doc_NombreArchivoXML , Doc_NombreArchivoPDF ,  Doc_NombreArchivoWeb ,
				  Doc_Resumen_FechaGeneracion ,  Doc_Resumen_Correlativo , Doc_Baja_GrpTipo_Codigo ,
				  Doc_Baja_FechaGeneracion , Doc_Baja_Correlativo ,
				  Doc_Correo_Enviado , Doc_Correo_EstaLeido ,
				  Doc_Correo_ContadorLectura , Doc_Correo_LecturaFechaPrimera , Doc_Correo_LecturaFechaUltima ,
				  EstDoc_Codigo
				)
		VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
				  @PDoc_FechaEmision , @PDoc_Moneda , 
				  @PDoc_Oper_Gravadas , @PDoc_Oper_Exoneradas , @PDoc_Oper_Inafectas , @PDoc_Oper_Gratuitas ,
				  @PDoc_ISC , @PDoc_Otros_Tributos , @PDoc_Otros_Conceptos , @PDoc_IGV , @PDoc_Importe ,
				  @PDoc_Certificado_Correlativo , @PDoc_Cliente_TipoDocIdent_Codigo , @PDoc_Cliente_TipoDocIdent_Numero ,
				  @PDoc_Modif_Doc_Tipo , @PDoc_Modif_Doc_Serie ,  @PDoc_Modif_Doc_Numero ,
				  @PDoc_NombreArchivoXML , @PDoc_NombreArchivoPDF ,  @PDoc_NombreArchivoWeb ,
				  @PDoc_Resumen_FechaGeneracion ,  @PDoc_Resumen_Correlativo , @PDoc_Baja_GrpTipo_Codigo ,
				  @PDoc_Baja_FechaGeneracion , @PDoc_Baja_Correlativo ,
				  @PDoc_Correo_Enviado , @PDoc_Correo_EstaLeido ,
				  @PDoc_Correo_ContadorLectura , @PDoc_Correo_LecturaFechaPrimera , @PDoc_Correo_LecturaFechaUltima ,
				  @PEstDoc_Codigo)    	
    END

END


GO
 

CREATE PROCEDURE [dbo].[UP_BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO]
(
@PFecha DATE,
@PHasta DATE
)
AS
BEGIN
	SELECT Adj.Doc_Emisor_RUC, Adj.Doc_Tipo, Adj.Doc_Serie, Adj.Doc_Numero, 
	Adj.XML_Archivo_Item, Adj.XML_Archivo_NombreLocal, Adj.XML_Archivo_Nombre, 
	Adj.Aud_Registro_FechaHora, Adj.Aud_Registro_Equipo, Adj.Aud_Registro_Usuario, Emp.Path_Root_App
	FROM FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO Adj WITH(NOLOCK)
	INNER JOIN FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON Doc.Doc_Emisor_RUC = Adj.Doc_Emisor_RUC AND Doc.Doc_Tipo = Adj.Doc_Tipo AND Doc.Doc_Serie = Adj.Doc_Serie AND Doc.Doc_Numero = Adj.Doc_Numero
	INNER JOIN FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Doc.Doc_Emisor_RUC
	WHERE Doc.Doc_FechaEmision BETWEEN @PFecha AND @PHasta;
END


GO
 


CREATE PROCEDURE [dbo].[UP_BACK_03_REPLICA_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8))
AS
BEGIN
	SELECT Adj.Doc_Emisor_RUC, Adj.Doc_Tipo, Adj.Doc_Serie, Adj.Doc_Numero, 
	Adj.XML_Archivo_Item, Adj.XML_Archivo_NombreLocal, Adj.XML_Archivo_Nombre, 
	Adj.Aud_Registro_FechaHora, Adj.Aud_Registro_Equipo, Adj.Aud_Registro_Usuario, Emp.Path_Root_App
	FROM FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO Adj WITH(NOLOCK)
	--INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON Doc.Doc_Emisor_RUC = Adj.Doc_Emisor_RUC AND Doc.Doc_Tipo = Adj.Doc_Tipo AND Doc.Doc_Serie = Adj.Doc_Serie AND Doc.Doc_Numero = Adj.Doc_Numero
	INNER JOIN FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Adj.Doc_Emisor_RUC
	WHERE Adj.Replicacion_Estado=0  AND Adj.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND 
	Adj.Doc_Tipo=@PDoc_Tipo AND Adj.Doc_Serie=@PDoc_Serie AND Adj.Doc_Numero=@PDoc_Numero ;
END


GO



CREATE PROCEDURE [dbo].[UP_BACK_03_REPLICA_UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8),
@PXML_Archivo_Item int
)
AS
BEGIN
	UPDATE FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO SET Replicacion_Estado = 1
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Archivo_Item = @PXML_Archivo_Item  ;
END


GO



CREATE PROCEDURE [dbo].[UP_BACK_03_UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8),
@PXML_Archivo_Item int,
@PXML_Archivo_NombreLocal varchar(30),
@PXML_Archivo_Nombre varchar(150),
@PAud_Registro_FechaHora datetime,
@PAud_Registro_Equipo varchar(50),
@PAud_Registro_Usuario varchar(50)
)
AS
BEGIN
	IF EXISTS(SELECT Adj.Doc_Emisor_RUC FROM FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO Adj WITH(NOLOCK) WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND Adj.XML_Archivo_Item = @PXML_Archivo_Item)
	BEGIN
		UPDATE FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO SET
		XML_Archivo_NombreLocal = @PXML_Archivo_NombreLocal, XML_Archivo_Nombre = @PXML_Archivo_Nombre, Aud_Registro_FechaHora = @PAud_Registro_FechaHora, Aud_Registro_Equipo = @PAud_Registro_Equipo, Aud_Registro_Usuario = @PAud_Registro_Usuario
		WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Archivo_Item = @PXML_Archivo_Item  ;
	END
	ELSE
    BEGIN
    	INSERT INTO FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO
    	( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero , XML_Archivo_Item ,
    	  XML_Archivo_NombreLocal , XML_Archivo_Nombre ,
    	  Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario )
    	VALUES ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero , @PXML_Archivo_Item ,
    	  @PXML_Archivo_NombreLocal , @PXML_Archivo_Nombre ,
    	  @PAud_Registro_FechaHora , @PAud_Registro_Equipo , @PAud_Registro_Usuario );
    END
END


GO



CREATE PROCEDURE [dbo].[UP_BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]
(
@PFecha DATE,
@PHasta DATE
)
AS
BEGIN
	SELECT Cli.Doc_Emisor_RUC, Cli.Doc_Tipo, Cli.Doc_Serie, Cli.Doc_Numero, 
	Cli.XML_Correlativo, Cli.XML_Reciente, Cli.XML_Archivo, Cli.TipXML_Codigo, 
	Cli.EstEnv_Codigo, ISNULL(Cli.XML_DigestValue,'') AS XML_DigestValue, ISNULL(Cli.XML_SignatureValue,'') AS XML_DigestValue,
	ISNULL(Cli.SUNAT_Respuesta_Codigo,'') AS SUNAT_Respuesta_Codigo, ISNULL(Cli.XML_MotivoBaja,'') AS XML_MotivoBaja, Cli.XML_FilaActiva, 
	Cli.XML_EnviadoFTP, Cli.XML_Revercion_Anulacion, ISNULL(Cli.XML_Revercion_Anulacion_FechaHora,'01-01-01') AS XML_Revercion_Anulacion_FechaHora, ISNULL(Cli.XML_Revercion_Anulacion_Razon,'') AS XML_Revercion_Anulacion_Razon, 
	ISNULL(Cli.XML_Creado_FechaHora,'01-01-01') AS XML_Creado_FechaHora, ISNULL(Cli.XML_Enviado_FechaHora,'01-01-01') AS XML_Enviado_FechaHora, ISNULL(Cli.Aud_Registro_FechaHora,'01-01-01') AS Aud_Registro_FechaHora, 
	ISNULL(Cli.Aud_Registro_Equipo,'')AS Aud_Registro_Equipo , ISNULL(Cli.Aud_Registro_Usuario,'') AS Aud_Registro_Usuario, Emp.Path_Root_App
	
	FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Cli WITH(NOLOCK)
	INNER JOIN FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON Doc.Doc_Emisor_RUC = Cli.Doc_Emisor_RUC AND Doc.Doc_Tipo = Cli.Doc_Tipo AND Doc.Doc_Serie = Cli.Doc_Serie AND Doc.Doc_Numero = Cli.Doc_Numero
	INNER JOIN FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Doc.Doc_Emisor_RUC
	WHERE Doc.Doc_FechaEmision BETWEEN @PFecha AND @PHasta AND Cli.EstEnv_Codigo<>'001'
	ORDER BY Cli.Doc_Emisor_RUC ASC, Cli.Doc_Tipo ASC, Cli.Doc_Serie ASC, Cli.Doc_Numero ASC, 
	Cli.XML_Correlativo DESC ;
END


GO


CREATE PROCEDURE [dbo].[UP_BACK_04_REPLICA_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8))
AS
BEGIN
	DECLARE @VarDT AS DATETIME=CAST(0 AS DATETIME);
	SELECT Cli.Doc_Emisor_RUC, Cli.Doc_Tipo, Cli.Doc_Serie, Cli.Doc_Numero, 
	Cli.XML_Correlativo, Cli.XML_Reciente, Cli.XML_Archivo, Cli.TipXML_Codigo, 
	Cli.EstEnv_Codigo, ISNULL(Cli.XML_DigestValue,''), ISNULL(Cli.XML_SignatureValue,''), 
	ISNULL(Cli.SUNAT_Respuesta_Codigo,''), ISNULL(Cli.XML_MotivoBaja,''), ISNULL(Cli.XML_FilaActiva,0), 
	ISNULL(Cli.XML_EnviadoFTP,0), ISNULL(Cli.XML_Revercion_Anulacion,0), ISNULL(Cli.XML_Revercion_Anulacion_FechaHora,@VarDT), ISNULL(Cli.XML_Revercion_Anulacion_Razon,''), 
	ISNULL(Cli.XML_Creado_FechaHora,@VarDT), ISNULL(Cli.XML_Enviado_FechaHora,@VarDT), Cli.Aud_Registro_FechaHora, Cli.Aud_Registro_Equipo, Cli.Aud_Registro_Usuario, Emp.Path_Root_App
	FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Cli WITH(NOLOCK)
	INNER JOIN FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON Doc.Doc_Emisor_RUC = Cli.Doc_Emisor_RUC AND Doc.Doc_Tipo = Cli.Doc_Tipo AND Doc.Doc_Serie = Cli.Doc_Serie AND Doc.Doc_Numero = Cli.Doc_Numero
	INNER JOIN FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Doc.Doc_Emisor_RUC
	WHERE Doc.Replicacion_Estado = 0 AND Cli.EstEnv_Codigo='003' AND Cli.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND 
	Cli.Doc_Tipo=@PDoc_Tipo AND Cli.Doc_Serie=@PDoc_Serie AND Cli.Doc_Numero=@PDoc_Numero
	ORDER BY Cli.Doc_Emisor_RUC ASC, Cli.Doc_Tipo ASC, Cli.Doc_Serie ASC, Cli.Doc_Numero ASC, 
	Cli.XML_Correlativo DESC ;
END


GO



CREATE PROCEDURE [dbo].[UP_BACK_04_REPLICA_UP_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8),
@PXML_Correlativo int
)
AS
BEGIN
	UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET
	Replicacion_Estado = 1
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Correlativo = @PXML_Correlativo ;
END


GO




CREATE PROCEDURE [dbo].[UP_BACK_04_UP_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8),
@PXML_Correlativo int,
@PXML_Reciente bit,
@PXML_Archivo varchar(150),
@PTipXML_Codigo char(3),
@PEstEnv_Codigo char(3),
@PXML_DigestValue varchar(50),
@PXML_SignatureValue varchar(500),
@PSUNAT_Respuesta_Codigo char(4),
@PXML_MotivoBaja varchar(100),
@PXML_FilaActiva bit,
@PXML_EnviadoFTP bit ,
@PXML_Revercion_Anulacion bit,
@PXML_Revercion_Anulacion_FechaHora datetime,
@PXML_Revercion_Anulacion_Razon varchar(150),
@PXML_Creado_FechaHora datetime,
@PXML_Enviado_FechaHora datetime,
@PAud_Registro_FechaHora datetime,
@PAud_Registro_Equipo varchar(50),
@PAud_Registro_Usuario varchar(50)
)
AS
BEGIN
	IF EXISTS(SELECT ArchCli.Doc_Emisor_RUC FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE ArchCli WITH(NOLOCK) WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND ArchCli.XML_Correlativo = @PXML_Correlativo)
	BEGIN
		UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET
        XML_Reciente = @PXML_Reciente, XML_Archivo = @PXML_Archivo, TipXML_Codigo = @PTipXML_Codigo, 
		EstEnv_Codigo = @PEstEnv_Codigo, XML_DigestValue = @PXML_DigestValue, XML_SignatureValue = @PXML_SignatureValue,
		SUNAT_Respuesta_Codigo = @PSUNAT_Respuesta_Codigo, XML_MotivoBaja = @PXML_MotivoBaja, XML_FilaActiva = @PXML_FilaActiva, 
		XML_EnviadoFTP = @PXML_EnviadoFTP, XML_Revercion_Anulacion = @PXML_Revercion_Anulacion, 
		XML_Revercion_Anulacion_FechaHora = @PXML_Revercion_Anulacion_FechaHora, XML_Revercion_Anulacion_Razon = @PXML_Revercion_Anulacion_Razon, 
		XML_Creado_FechaHora = @PXML_Creado_FechaHora, XML_Enviado_FechaHora = @PXML_Enviado_FechaHora, 
		Aud_Registro_FechaHora = @PAud_Registro_FechaHora, Aud_Registro_Equipo = @PAud_Registro_Equipo, Aud_Registro_Usuario = @PAud_Registro_Usuario
		WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Correlativo = @PXML_Correlativo ;
	END
	ELSE
	BEGIN
		INSERT INTO FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
		( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero , XML_Correlativo ,
		  XML_Reciente , XML_Archivo , TipXML_Codigo , EstEnv_Codigo ,
		  XML_DigestValue , XML_SignatureValue , SUNAT_Respuesta_Codigo ,
		  XML_MotivoBaja , XML_FilaActiva , XML_EnviadoFTP ,
		  XML_Revercion_Anulacion , XML_Revercion_Anulacion_FechaHora , XML_Revercion_Anulacion_Razon ,
		  XML_Creado_FechaHora , XML_Enviado_FechaHora ,
		  Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario )
		VALUES ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero , @PXML_Correlativo ,
		  @PXML_Reciente , @PXML_Archivo , @PTipXML_Codigo , @PEstEnv_Codigo ,
		  @PXML_DigestValue , @PXML_SignatureValue , @PSUNAT_Respuesta_Codigo ,
		  @PXML_MotivoBaja , @PXML_FilaActiva , @PXML_EnviadoFTP ,
		  @PXML_Revercion_Anulacion , @PXML_Revercion_Anulacion_FechaHora , @PXML_Revercion_Anulacion_Razon ,
		  @PXML_Creado_FechaHora , @PXML_Enviado_FechaHora ,
		  @PAud_Registro_FechaHora , @PAud_Registro_Equipo , @PAud_Registro_Usuario )
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UP_CFG_APPLICATION_INSERT_UPDATE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_CFG_APPLICATION_INSERT_UPDATE]
(
	@PApp_Replicacion BIT,
	@PApp_Replicacion_Servidor VARCHAR(100),
	@PApp_Replicacion_BD VARCHAR(50),
	@PApp_Replicacion_Usuario VARCHAR(50),
	@PApp_Replicacion_Contrasenia VARCHAR(50),
	@PApp_Replicacion_Prog_Hora_Establecido BIT,
	@PApp_Replicacion_Prog_Hora_CadaRangoHoras BIT,
	@PApp_Replicacion_Prog_Hora INT,
	@PApp_Replicacion_Prog_Hora_Desde TIME,
	@PApp_Replicacion_Prog_Hora_Hasta TIME,
	@PApp_Replicacion_RutaPrincipalArchivos VARCHAR(250),
	@PUsuario VARCHAR(50),
	@PEquipo VARCHAR(50)
)
AS
BEGIN
	IF EXISTS(SELECT TOP 1 App.App_Replicacion FROM CFG_APPLICATION App WITH(NOLOCK))
	BEGIN
		UPDATE CFG_APPLICATION SET
        App_Replicacion = @PApp_Replicacion, App_Replicacion_Servidor = @PApp_Replicacion_Servidor,
		App_Replicacion_BD = @PApp_Replicacion_BD,
		App_Replicacion_Usuario = @PApp_Replicacion_Usuario, App_Replicacion_Contrasenia = @PApp_Replicacion_Contrasenia,
		App_Replicacion_Prog_Hora_Establecido = @PApp_Replicacion_Prog_Hora_Establecido,
		App_Replicacion_Prog_Hora_CadaRangoHoras = @PApp_Replicacion_Prog_Hora_CadaRangoHoras,
		App_Replicacion_Prog_Hora = @PApp_Replicacion_Prog_Hora,
		App_Replicacion_RutaPrincipalArchivos = @PApp_Replicacion_RutaPrincipalArchivos,
		App_Replicacion_Prog_Hora_Desde = @PApp_Replicacion_Prog_Hora_Desde,
		App_Replicacion_Prog_Hora_Hasta = @PApp_Replicacion_Prog_Hora_Hasta
	END
	ELSE
    BEGIN
    	INSERT INTO CFG_APPLICATION
    	( App_Replicacion , App_Replicacion_Servidor , App_Replicacion_BD,
    	  App_Replicacion_Usuario , App_Replicacion_Contrasenia ,
    	  App_Replicacion_Prog_Hora_Establecido , App_Replicacion_Prog_Hora_CadaRangoHoras ,
    	  App_Replicacion_Prog_Hora ,
    	  App_Replicacion_Prog_Hora_Desde , App_Replicacion_Prog_Hora_Hasta ,
		  App_Replicacion_RutaPrincipalArchivos,
    	  Aud_Actualizacion_FechaHora , Aud_Actualizacion_Equipo )
    	VALUES  
		( @PApp_Replicacion , @PApp_Replicacion_Servidor , @PApp_Replicacion_BD,
    	  @PApp_Replicacion_Usuario , @PApp_Replicacion_Contrasenia ,
    	  @PApp_Replicacion_Prog_Hora_Establecido , @PApp_Replicacion_Prog_Hora_CadaRangoHoras ,
    	  @PApp_Replicacion_Prog_Hora ,
    	  @PApp_Replicacion_Prog_Hora_Desde , @PApp_Replicacion_Prog_Hora_Hasta ,
		  @PApp_Replicacion_RutaPrincipalArchivos,
    	  GETDATE() , @PEquipo );
    END
END


GO
/****** Object:  StoredProcedure [dbo].[UP_CFG_APPLICATION_SELECT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_CFG_APPLICATION_SELECT]
AS
BEGIN
	DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
	DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

	SELECT CfgApp.App_Replicacion , CfgApp.App_Replicacion_Servidor , CfgApp.App_Replicacion_BD,
    	  CfgApp.App_Replicacion_Usuario , CfgApp.App_Replicacion_Contrasenia ,
    	  CfgApp.App_Replicacion_Prog_Hora_Establecido , CfgApp.App_Replicacion_Prog_Hora_CadaRangoHoras ,
    	  CfgApp.App_Replicacion_Prog_Hora ,
    	  CfgApp.App_Replicacion_Prog_Hora_Desde , CfgApp.App_Replicacion_Prog_Hora_Hasta ,
		 CfgApp.App_Replicacion_RutaPrincipalArchivos,
		  CAST(ISNULL((CASE WHEN CfgApp.App_Replicacion_Prog_Hora_Desde <= @AhoraSoloHora AND CfgApp.App_Replicacion_Prog_Hora_Hasta >= @AhoraSoloHora THEN 1 ELSE 0 END),0) AS BIT)---1 'Es hora de enviar'
		  FROM CFG_APPLICATION CfgApp WITH(NOLOCK)
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_CERTIFICADO_DELETE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_CERTIFICADO_DELETE]
(
@PEmp_RUC CHAR(11),
@PCert_Correlativo INT
)
AS
BEGIN
	DELETE dbo.FACTE_CERTIFICADO
	WHERE Emp_RUC = @PEmp_RUC AND Cert_Correlativo = @PCert_Correlativo ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_CERTIFICADO_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_CERTIFICADO_INSERT]
(
@PEmp_RUC CHAR(11),
@PCert_Correlativo INT OUTPUT,
@PCert_Codigo VARCHAR(15),
@PCert_Vigencia_Desde DATE,
@PCert_Vigencia_Hasta DATE,
@PCert_Contrasena VARCHAR(100),
@PCert_Ruta VARCHAR(MAX),
@PCert_EsPropio BIT,
@PCert_Ext_RUC VARCHAR(15)=NULL,
@PCert_Ext_RazonSocial VARCHAR(150)=NULL
)
AS
BEGIN
	 SET @PCert_Correlativo = (ISNULL((SELECT MAX(Certi.Cert_Correlativo) FROM dbo.FACTE_CERTIFICADO Certi WHERE Certi.Emp_RUC = @PEmp_RUC ),0) + 1);

	INSERT INTO dbo.FACTE_CERTIFICADO
	( Emp_RUC , Cert_Correlativo, Cert_Codigo , Cert_Vigencia_Desde , Cert_Vigencia_Hasta , Cert_Contrasena , Cert_Ruta ,
	 Cert_EsPropio, Cert_Ext_RUC, Cert_Ext_RazonSocial,
	 Aud_Registro_Fecha , Aud_Registro_Autor , Aud_Registro_Equipo )
	VALUES  
	( @PEmp_RUC , @PCert_Correlativo, @PCert_Codigo , @PCert_Vigencia_Desde , @PCert_Vigencia_Hasta , @PCert_Contrasena , @PCert_Ruta , -- Cert_Ruta - varchar(300)
	@PCert_EsPropio, @PCert_Ext_RUC, @PCert_Ext_RazonSocial,
	GETDATE() , 'User' , HOST_NAME() );
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_CERTIFICADO_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_CERTIFICADO_LISTA]
(
@PEmp_RUC CHAR(11)
)
AS
BEGIN
	SELECT Certi.Emp_RUC, Certi.Cert_Correlativo, Certi.Cert_Codigo, 
	Certi.Cert_Vigencia_Desde, Certi.Cert_Vigencia_Hasta, 
	Certi.Cert_Contrasena, Certi.Cert_Ruta,
	Certi.Cert_EsPropio, (CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_RUC ELSE Certi.Cert_Ext_RUC END) AS 'Propietario_RUC',
	(CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_NombreRazonSocial ELSE Certi.Cert_Ext_RazonSocial END) AS 'Propietario_RazonSocial'
	FROM dbo.FACTE_CERTIFICADO Certi WITH(NOLOCK)
	INNER JOIN FACTE_EMPRESA Config WITH(NOLOCK) ON Config.Emp_RUC = Certi.Emp_RUC
	WHERE Certi.Emp_RUC = @PEmp_RUC
	ORDER BY Certi.Cert_Vigencia_Desde DESC;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_CERTIFICADO_OBTENER]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_CERTIFICADO_OBTENER]
(
@PEmp_RUC CHAR(11)
)
AS
BEGIN
	SELECT Config.Sunat_Usuario, Config.Sunat_Contrasena,
	Config.Correo_Usuario, Config.Correo_Contrasena, Config.Correo_ServidorSMTP, Config.Correo_ServidorPuerto, Config.Correo_ServidorSSL,
	Config.Etapa_Codigo,
	Config.Path_Root_App,
	--ISNULL(Config.PathServer,''), --En caso FTP no exista entonces solo guardamos en pathServer (Srvidor local), se supone ahi se ejecuta nuestra app
	ISNULL(Config.FTP_Direccion,''), ISNULL(Config.FTP_Carpeta,''), ISNULL(Config.FTP_Usuario,''), ISNULL(Config.FTP_Contrasena,''),
	Config.Sunat_Autorizacion, Config.Emisor_Web_Visualizacion
	FROM FACTE_EMPRESA Config WITH(NOLOCK)
	WHERE Config.Emp_RUC = @PEmp_RUC
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_CERTIFICADO_OBTENER_VIGENTE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_CERTIFICADO_OBTENER_VIGENTE]
(
@PEmp_RUC CHAR(11),
@PFecha_Emision DATE,
@PTipoDoc_Codigo CHAR(2)=NULL
)
AS
BEGIN
	IF (@PTipoDoc_Codigo IS NULL)
	BEGIN
		SELECT Certi.Cert_Ruta, Certi.Cert_Contrasena, 
		Config.Sunat_Usuario, Config.Sunat_Contrasena,
		Config.Correo_Usuario, Config.Correo_Contrasena, Config.Correo_ServidorSMTP, Config.Correo_ServidorPuerto, Config.Correo_ServidorSSL,
		Config.Etapa_Codigo,
		Config.Path_Root_App,
		--ISNULL(Config.PathServer,''), --En caso FTP no exista entonces solo guardamos en pathServer (Srvidor local), se supone ahi se ejecuta nuestra app
		ISNULL(Config.FTP_Direccion,''), ISNULL(Config.FTP_Carpeta,''), ISNULL(Config.FTP_Usuario,''), ISNULL(Config.FTP_Contrasena,''),
		Config.Sunat_Autorizacion, Config.Emisor_Web_Visualizacion, 
		Certi.Cert_Codigo,
		Certi.Cert_Vigencia_Desde, Certi.Cert_Vigencia_Hasta,
		Certi.Cert_EsPropio, (CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_RUC ELSE Certi.Cert_Ext_RUC END) AS 'Propietario_RUC',
		(CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_NombreRazonSocial ELSE Certi.Cert_Ext_RazonSocial END) AS 'Propietario_RazonSocial',
		''
		FROM FACTE_CERTIFICADO Certi
		INNER JOIN FACTE_EMPRESA Config WITH(NOLOCK) ON Config.Emp_RUC = Certi.Emp_RUC
		WHERE Certi.Emp_RUC = @PEmp_RUC AND (Certi.Cert_Vigencia_Desde <= @PFecha_Emision AND @PFecha_Emision <=Certi.Cert_Vigencia_Hasta)		
	END
	ELSE
    BEGIN
    	SELECT Certi.Cert_Ruta, Certi.Cert_Contrasena, 
		Config.Sunat_Usuario, Config.Sunat_Contrasena,
		Config.Correo_Usuario, Config.Correo_Contrasena, Config.Correo_ServidorSMTP, Config.Correo_ServidorPuerto, Config.Correo_ServidorSSL,
		Config.Etapa_Codigo,
		Config.Path_Root_App,
		--ISNULL(Config.PathServer,''), --En caso FTP no exista entonces solo guardamos en pathServer (Srvidor local), se supone ahi se ejecuta nuestra app
		ISNULL(Config.FTP_Direccion,''), ISNULL(Config.FTP_Carpeta,''), ISNULL(Config.FTP_Usuario,''), ISNULL(Config.FTP_Contrasena,''),
		Config.Sunat_Autorizacion, Config.Emisor_Web_Visualizacion, 
		Certi.Cert_Codigo,
		Certi.Cert_Vigencia_Desde, Certi.Cert_Vigencia_Hasta,
		Certi.Cert_EsPropio, (CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_RUC ELSE Certi.Cert_Ext_RUC END) AS 'Propietario_RUC',
		(CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_NombreRazonSocial ELSE Certi.Cert_Ext_RazonSocial END) AS 'Propietario_RazonSocial',
		ISNULL(TURL.GrpTipo_URL,'')
		FROM FACTE_CERTIFICADO Certi
		INNER JOIN FACTE_EMPRESA Config WITH(NOLOCK) ON Config.Emp_RUC = Certi.Emp_RUC
		LEFT JOIN FACTE_DOCUMENTO_TIPO DocTipo WITH(NOLOCK) ON DocTipo.TipoDoc_Codigo = @PTipoDoc_Codigo
		LEFT JOIN dbo.FACTE_DOCUMENTO_TIPO_GRUPO_URL TURL WITH(NOLOCK) ON TURL.GrpTipo_Codigo = DocTipo.GrpTipo_Codigo AND TURL.Etapa_Codigo = Config.Etapa_Codigo
		WHERE Certi.Emp_RUC = @PEmp_RUC AND (Certi.Cert_Vigencia_Desde <= @PFecha_Emision AND @PFecha_Emision <=Certi.Cert_Vigencia_Hasta)	

    END
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_CERTIFICADO_UPDATE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_CERTIFICADO_UPDATE]
(
@PEmp_RUC CHAR(11),
@PCert_Correlativo INT,
@PCert_Codigo VARCHAR(15),
@PCert_Vigencia_Desde DATE,
@PCert_Vigencia_Hasta DATE,
@PCert_Contrasena VARCHAR(100),
@PCert_Ruta VARCHAR(MAX),
@PCert_EsPropio BIT,
@PCert_Ext_RUC VARCHAR(15)=NULL,
@PCert_Ext_RazonSocial VARCHAR(150)=NULL
)
AS
BEGIN
	UPDATE dbo.FACTE_CERTIFICADO
	SET Cert_Codigo = @PCert_Codigo, Cert_Vigencia_Desde = @PCert_Vigencia_Desde,
	Cert_Vigencia_Hasta = @PCert_Vigencia_Hasta, Cert_Contrasena = @PCert_Contrasena, Cert_Ruta = @PCert_Ruta,
	Cert_EsPropio = @PCert_EsPropio, Cert_Ext_RUC = @PCert_Ext_RUC, Cert_Ext_RazonSocial = @PCert_Ext_RazonSocial,
	Aud_Actualizacion_Fecha = GETDATE(), Aud_Actualizacion_Autor ='USER', Aud_Actualizacion_Equipo = HOST_NAME()
	WHERE Emp_RUC = @PEmp_RUC AND Cert_Correlativo = @PCert_Correlativo ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_COMUNICACION_BAJA_CAMBIAR_ESTADO_ENVIO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--SELECT * FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CL 
--INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CC ON 
--CC.Doc_Emisor_RUC = CL.Doc_Emisor_RUC AND CC.Doc_Tipo = CL.Doc_Tipo 
--AND CC.Doc_Serie = CL.Doc_Serie AND CC.Doc_Numero = CL.Doc_Numero AND  CL.TipXML_Codigo='003' AND CC.TipXML_Codigo<>'003'
--AND (CC.EstEnv_Codigo='003' OR CC.EstEnv_Codigo='007')
--AND ISNULL(CC.XML_Aceptado_CDR_FechaHora,GETDATE())>=DATEADD(DAY,-3,CAST(GETDATE() AS DATE))


--SELECT dc.* FROM dbo.FACTE_DOCUMENTO_ELECTRONICO dc 
--INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE xc ON 
--xc.Doc_Emisor_RUC = dc.Doc_Emisor_RUC AND xc.Doc_Tipo = dc.Doc_Tipo AND xc.Doc_Serie = dc.Doc_Serie AND xc.Doc_Numero = dc.Doc_Numero
--WHERE xc.TipXML_Codigo='003'

CREATE PROC [dbo].[UP_FACTE_COMUNICACION_BAJA_CAMBIAR_ESTADO_ENVIO]
(
@PDoc_Emisor_RUC CHAR(11),
@PGrpTipo_Codigo CHAR(3),
@PDoc_FechaGeneracion DATE,
@PDoc_Resumen_Correlativo INT,
@PDoc_NombreArchivoXML VARCHAR(50)=NULL,
@PDoc_SUNATTicket_ID VARCHAR(50)=NULL,
@PSUNAT_Respuesta_Codigo CHAR(4)=NULL,
@PEstEnv_Codigo CHAR(3)
)
AS
BEGIN
	UPDATE dbo.FACTE_COMUNICACION_BAJA  SET EstEnv_Codigo = @PEstEnv_Codigo, 
	RsmBj_NombreArchivoXML = @PDoc_NombreArchivoXML, RsmBj_SUNATTicket_ID = @PDoc_SUNATTicket_ID, 
	SUNAT_Respuesta_Codigo = @PSUNAT_Respuesta_Codigo
	WHERE RsmBj_Emisor_RUC = @PDoc_Emisor_RUC AND RsmBj_GrpTipo_Codigo = @PGrpTipo_Codigo 
	AND RsmBj_FechaGeneracion = @PDoc_FechaGeneracion AND RsmBj_Resumen_Correlativo = @PDoc_Resumen_Correlativo ;

	--UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET EstEnv_Codigo = @PEstEnv_Codigo
	--FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
	--INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc ON Doc.Doc_Emisor_RUC = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Emisor_RUC AND Doc.Doc_Tipo = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Tipo AND Doc.Doc_Serie = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Serie AND Doc.Doc_Numero = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Numero AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.XML_Reciente = 1
	--WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_Resumen_FechaGeneracion = @PDoc_FechaGeneracion AND Doc.Doc_Resumen_Correlativo = @PDoc_Resumen_Correlativo ;

	UPDATE xmlcli SET xmlcli.EstEnv_Codigo=@PEstEnv_Codigo
	FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE xmlcli WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON 
	Doc.Doc_Emisor_RUC = xmlcli.Doc_Emisor_RUC AND Doc.Doc_Tipo = xmlcli.Doc_Tipo 
	AND Doc.Doc_Serie = xmlcli.Doc_Serie AND Doc.Doc_Numero = xmlcli.Doc_Numero
	--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.XML_Reciente = 1
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc WITH(NOLOCK) ON Tdoc.TipoDoc_Codigo = Doc.Doc_Tipo
	INNER JOIN dbo.FACTE_COMUNICACION_BAJA_DOCUMENTO dcbj WITH(NOLOCK) ON 
	dcbj.RsmBj_Emisor_RUC=xmlcli.Doc_Emisor_RUC AND dcbj.Doc_Tipo = xmlcli.Doc_Tipo AND 
    dcbj.Doc_Serie = xmlcli.Doc_Serie AND dcbj.Doc_Numero = xmlcli.Doc_Numero AND dcbj.RsmBj_GrpTipo_Codigo=Tdoc.GrpTipo_Codigo
	INNER JOIN dbo.FACTE_COMUNICACION_BAJA bj WITH(NOLOCK) ON 
    bj.RsmBj_Emisor_RUC = dcbj.RsmBj_Emisor_RUC AND bj.RsmBj_GrpTipo_Codigo = dcbj.RsmBj_GrpTipo_Codigo 
	AND bj.RsmBj_FechaGeneracion = dcbj.RsmBj_FechaGeneracion AND bj.RsmBj_Resumen_Correlativo = dcbj.RsmBj_Resumen_Correlativo	
	WHERE bj.RsmBj_Emisor_RUC = @PDoc_Emisor_RUC
	--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.EstEnv_Codigo='001' -- PENDIENTE ENVIO
	AND bj.RsmBj_GrpTipo_Codigo = @PGrpTipo_Codigo AND bj.RsmBj_FechaGeneracion = @PDoc_FechaGeneracion 
	AND bj.RsmBj_Resumen_Correlativo = @PDoc_Resumen_Correlativo 
	AND xmlcli.TipXML_Codigo='003' -- BAJA;

---ACTUALIZAMOS EL DOCUMENTO ELECTRONICO A ANULADO 

UPDATE Doc SET Doc.EstDoc_Codigo='002'
	FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE xmlcli WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK) ON 
	Doc.Doc_Emisor_RUC = xmlcli.Doc_Emisor_RUC AND Doc.Doc_Tipo = xmlcli.Doc_Tipo 
	AND Doc.Doc_Serie = xmlcli.Doc_Serie AND Doc.Doc_Numero = xmlcli.Doc_Numero
	--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.XML_Reciente = 1
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc WITH(NOLOCK) ON Tdoc.TipoDoc_Codigo = Doc.Doc_Tipo
	INNER JOIN dbo.FACTE_COMUNICACION_BAJA_DOCUMENTO dcbj WITH(NOLOCK) ON 
	dcbj.RsmBj_Emisor_RUC=xmlcli.Doc_Emisor_RUC AND dcbj.Doc_Tipo = xmlcli.Doc_Tipo AND 
    dcbj.Doc_Serie = xmlcli.Doc_Serie AND dcbj.Doc_Numero = xmlcli.Doc_Numero AND dcbj.RsmBj_GrpTipo_Codigo=Tdoc.GrpTipo_Codigo
	INNER JOIN dbo.FACTE_COMUNICACION_BAJA bj WITH(NOLOCK) ON 
    bj.RsmBj_Emisor_RUC = dcbj.RsmBj_Emisor_RUC AND bj.RsmBj_GrpTipo_Codigo = dcbj.RsmBj_GrpTipo_Codigo 
	AND bj.RsmBj_FechaGeneracion = dcbj.RsmBj_FechaGeneracion AND bj.RsmBj_Resumen_Correlativo = dcbj.RsmBj_Resumen_Correlativo	
	WHERE bj.RsmBj_Emisor_RUC = @PDoc_Emisor_RUC
	--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.EstEnv_Codigo='001' -- PENDIENTE ENVIO
	AND bj.RsmBj_GrpTipo_Codigo = @PGrpTipo_Codigo AND bj.RsmBj_FechaGeneracion = @PDoc_FechaGeneracion 
	AND bj.RsmBj_Resumen_Correlativo = @PDoc_Resumen_Correlativo 
	AND xmlcli.TipXML_Codigo='003' -- BAJA;

END






--SELECT * FROM FACTE_COMUNICACION_BAJA

--SELECT * FROM FACTE_COMUNICACION_BAJA_DOCUMENTO
GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_COMUNICACION_BAJA_ESPERA_RESPUESTA_TICKET]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_COMUNICACION_BAJA_ESPERA_RESPUESTA_TICKET]
AS
BEGIN
	--ENVIADOS - PENDIENTES DE RESPUESTA
	SELECT Diar.RsmBj_Emisor_RUC, Diar.RsmBj_FechaGeneracion, Diar.RsmBj_Resumen_Correlativo,
	Diar.RsmBj_Fecha_Generacion_Doc_Baja, Diar.RsmBj_NombreArchivoXML, Diar.RsmBj_SUNATTicket_ID, 
	Emp.Path_Root_App + '\' + Diar.RsmBj_Emisor_RUC, Emp.Etapa_Codigo,Diar.RsmBj_GrpTipo_Codigo
	FROM dbo.FACTE_COMUNICACION_BAJA Diar WITH(NOLOCK) 
	INNER JOIN dbo.FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Diar.RsmBj_Emisor_RUC
	WHERE (Diar.EstEnv_Codigo='006' OR Diar.EstEnv_Codigo='010') 
	AND (Diar.RsmBj_SUNATTicket_ID IS NOT NULL AND LTRIM(RTRIM(Diar.RsmBj_SUNATTicket_ID))<>'')
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_COMUNICACION_BAJA_PENDIENTES_DETALLE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UP_FACTE_COMUNICACION_BAJA_PENDIENTES_DETALLE]
(
@PDoc_Emisor_RUC CHAR(11),
@PGrpTipo_Codigo CHAR(3),
@PDoc_FechaGeneracion DATE,
@PDoc_Resumen_Correlativo INT
)
AS
BEGIN
	SELECT
	RsmDoc.RsmBj_Item, DocOrig.Doc_Moneda, 
	RsmDoc.Doc_Tipo, RsmDoc.Doc_Serie, RsmDoc.Doc_Numero,
	XmlC.XML_MotivoBaja
	FROM FACTE_COMUNICACION_BAJA_DOCUMENTO RsmDoc
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO DocOrig ON DocOrig.Doc_Emisor_RUC = RsmDoc.RsmBj_Emisor_RUC AND DocOrig.Doc_Tipo = RsmDoc.Doc_Tipo AND DocOrig.Doc_Serie = RsmDoc.Doc_Serie AND DocOrig.Doc_Numero = RsmDoc.Doc_Numero
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE XmlC ON XmlC.Doc_Emisor_RUC = DocOrig.Doc_Emisor_RUC AND XmlC.Doc_Tipo = DocOrig.Doc_Tipo AND XmlC.Doc_Serie = DocOrig.Doc_Serie AND XmlC.Doc_Numero = DocOrig.Doc_Numero AND XmlC.XML_Reciente = 1 AND XmlC.TipXML_Codigo='003'
	WHERE RsmDoc.RsmBj_Emisor_RUC = @PDoc_Emisor_RUC AND RsmDoc.RsmBj_GrpTipo_Codigo = @PGrpTipo_Codigo 
	AND RsmDoc.RsmBj_FechaGeneracion = @PDoc_FechaGeneracion AND RsmDoc.RsmBj_Resumen_Correlativo = @PDoc_Resumen_Correlativo ;
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_COMUNICACION_BAJA_PENDIENTES_ENVIO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UP_FACTE_COMUNICACION_BAJA_PENDIENTES_ENVIO]
AS
BEGIN
	--Habilitados y listos para enviar
	DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
	DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

	UPDATE FACTE_PROGRAMACIONES_ENVIO  SET Progra_Ejecucion_Ultimo = GETDATE() WHERE Progra_Ejecucion_Ultimo IS NULL;

	--SOLO DOCUMENTOS ACTIVOS
	--CARGAR TEMPORAL
	DECLARE @Tar_Emisor_RUC CHAR(11), @Tar_TipoDoc_Codigo CHAR(2), @Tar_TipoDoc_Serie CHAR(4);
	DECLARE @Num_Emisor CHAR(11), @Num_Grp CHAR(3),  @Num_Fecha_Emision DATE;
	CREATE TABLE #Temp_Prog_Resumenes
	( Emisor_RUC CHAR(11), TipoDoc_Codigo CHAR(2), TipoDoc_Serie CHAR(4) );
	INSERT INTO #Temp_Prog_Resumenes
	( Emisor_RUC , TipoDoc_Codigo , TipoDoc_Serie )
	SELECT ProgX.Emp_RUC, ProgX.Progra_Appli_TipoDoc,
	(CASE WHEN ProgX.Progra_Appli_Filtra_Serie = 1 THEN ProgX.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie'
	FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK)
	WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsComunicacionBaja = 1
	AND ProgX.Progra_Estado='001'
	AND @AhoraFechaHora >= ProgX.Progra_Fecha_Desde 
	AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	AND DATEADD(HOUR,ProgX.Progra_FrecuEnvio_Hora_XCadaHora, ProgX.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora
	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora;

	--SELECT * FROM #Temp_Prog_Resumenes;
	--DROP TABLE #Temp_Prog_Resumenes

	WHILE EXISTS(SELECT Emisor_RUC FROM #Temp_Prog_Resumenes WITH(NOLOCK))
	BEGIN
		--Item programado -- seleccionado
		SELECT TOP 1 @Tar_Emisor_RUC = Emisor_RUC , 
		@Tar_TipoDoc_Codigo = TipoDoc_Codigo , @Tar_TipoDoc_Serie = TipoDoc_Serie 
		FROM #Temp_Prog_Resumenes WITH(NOLOCK);
		--Item programado -- eliminado
		DELETE #Temp_Prog_Resumenes 
		WHERE Emisor_RUC = @Tar_Emisor_RUC AND TipoDoc_Codigo = @Tar_TipoDoc_Codigo
		AND (@Tar_TipoDoc_Serie IS NULL OR TipoDoc_Serie= @Tar_TipoDoc_Serie);
		--CONSULTA SUNAT: Se puede enviar comunicacion de baja pero con un rango no consecutivo?

		--Tabla correlativos de series
		CREATE TABLE #fe_grupo_series_correlativos
		(
			fe_grp_emisor CHAR(11),
			fe_grp_Codigo CHAR(3),
			fe_grp_fecha_emision DATE,
			fe_grp_ejecutado BIT
		);

		INSERT INTO #fe_grupo_series_correlativos
		( fe_grp_emisor , fe_grp_Codigo , fe_grp_fecha_emision , fe_grp_ejecutado)
		SELECT Doc.Doc_Emisor_RUC, Tdoc.GrpTipo_Codigo, Doc.Doc_FechaEmision, 0
		FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo 
		AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero --AND DocXML.XML_Reciente = 1
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CC WITH(NOLOCK) ON  --VERIFICAR SI EL XML A SIDO INFORMADO Y ACEPTADO CORRECTAMENTE, SI ES ASI SE GENERA EL XML DE BAJA
		CC.Doc_Emisor_RUC = DocXML.Doc_Emisor_RUC AND CC.Doc_Tipo = DocXML.Doc_Tipo
		AND CC.Doc_Serie = DocXML.Doc_Serie AND CC.Doc_Numero = DocXML.Doc_Numero 
		AND  DocXML.TipXML_Codigo='003' AND CC.TipXML_Codigo<>'003' --RELACIONAR XML DE ENVIO, CON XML DE BAJA
		AND (CC.EstEnv_Codigo='003' OR CC.EstEnv_Codigo='007') -- VERIFICAR SI EL XML DE ENVIO ESTA ACEPTADO 
		AND CAST(ISNULL((CC.XML_Aceptado_CDR_FechaHora),doc.Doc_FechaEmision) AS DATE)>=DATEADD(DAY,-3,CAST(@AhoraFechaHora AS DATE)) --VERIFICAR EL DIA QUE ACEPTO EL CDR, Y SOLO DEBE SER 3 DIAS ANTES DE LA FECHA ACTUAL.
		INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc WITH(NOLOCK) ON Tdoc.TipoDoc_Codigo = Doc.Doc_Tipo
		WHERE Doc.Doc_Emisor_RUC = @Tar_Emisor_RUC AND Doc.Doc_Tipo = @Tar_TipoDoc_Codigo
		AND (@Tar_TipoDoc_Serie IS NULL OR Doc.Doc_Serie = @Tar_TipoDoc_Serie)
		AND DocXML.EstEnv_Codigo='001' -- PENDIENTE ENVIO
		AND DocXML.TipXML_Codigo='003' -- BAJA
		AND (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI NO CONSIDERA BOLETAS NI SUS NOTAS DE CREDITO Y DEBITO
		INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
		ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
		WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
		AND ProgX.Progra_Estado='001' 
		AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
		AND ProgX.Emp_RUC=doc.Doc_Emisor_RUC)
		THEN (CASE WHEN (doc.Doc_Tipo ='03' OR doc.Doc_Tipo ='07' OR doc.Doc_Tipo ='08') 
		THEN (CASE WHEN doc.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
		ELSE 0 END) 
		ELSE 0 END)=0 
		GROUP BY Doc.Doc_Emisor_RUC , Tdoc.GrpTipo_Codigo , Doc.Doc_FechaEmision
		ORDER BY Tdoc.GrpTipo_Codigo ASC, Doc.Doc_FechaEmision ASC ;
		
		--SELECT * FROM #fe_grupo_series_correlativos ;

		WHILE EXISTS(SELECT fe_grp_emisor FROM #fe_grupo_series_correlativos WITH(NOLOCK) WHERE fe_grp_ejecutado=0)
		BEGIN
			
			DECLARE @Correlativo_Actual INT, @Fecha_Comun DATE;
			SET @Fecha_Comun = GETDATE();

			-- P1 - Seleccionamos y actualizamoz
			SELECT TOP 1 @Num_Emisor = fe_grp_emisor, @Num_Grp = fe_grp_Codigo, @Num_Fecha_Emision = fe_grp_fecha_emision
			FROM #fe_grupo_series_correlativos WITH(NOLOCK) WHERE fe_grp_ejecutado = 0;

			UPDATE #fe_grupo_series_correlativos 
			SET fe_grp_ejecutado = 1
			WHERE fe_grp_emisor = @Num_Emisor AND fe_grp_Codigo = @Num_Grp AND fe_grp_fecha_emision = @Num_Fecha_Emision ;
			-- P1 - Fin

			--CB-Cabecera
			SET @Correlativo_Actual = ISNULL((SELECT MAX(ComBaja.RsmBj_Resumen_Correlativo) FROM FACTE_COMUNICACION_BAJA ComBaja WHERE ComBaja.RsmBj_Emisor_RUC = @Num_Emisor AND ComBaja.RsmBj_GrpTipo_Codigo = @Num_Grp AND ComBaja.RsmBj_FechaGeneracion = @Fecha_Comun),0) + 1;
			INSERT INTO dbo.FACTE_COMUNICACION_BAJA
			( RsmBj_Emisor_RUC , RsmBj_GrpTipo_Codigo , RsmBj_FechaGeneracion , RsmBj_Resumen_Correlativo ,
			  RsmBj_Fecha_Generacion_Doc_Baja , RsmBj_NombreArchivoXML ,
			  RsmBj_SUNATTicket_ID , SUNAT_Respuesta_Codigo , EstEnv_Codigo , RsmBj_Estado ,
			  Doc_Aud_Registro_FechaHora , Doc_Aud_Registro_Equipo )
			VALUES  ( @Num_Emisor , @Num_Grp , @Fecha_Comun , @Correlativo_Actual ,
			  @Num_Fecha_Emision , NULL , 
			  NULL , NULL , '001' , '0001' ,
			  GETDATE() , HOST_NAME() );
			--CB-Detalles
			INSERT INTO dbo.FACTE_COMUNICACION_BAJA_DOCUMENTO
			( RsmBj_Emisor_RUC , RsmBj_GrpTipo_Codigo , RsmBj_FechaGeneracion , RsmBj_Resumen_Correlativo ,
			  RsmBj_Item , Doc_Tipo , Doc_Serie , Doc_Numero )
			  SELECT @Num_Emisor , @Num_Grp , @Fecha_Comun , @Correlativo_Actual ,
			  ROW_NUMBER() OVER(PARTITION BY Tdoc.GrpTipo_Codigo ORDER BY Doc.Doc_Tipo,Doc.Doc_Serie ASC, Doc.Doc_Numero ASC), Doc.Doc_Tipo , Doc.Doc_Serie , Doc.Doc_Numero
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
			INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1
			INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc WITH(NOLOCK) ON Tdoc.TipoDoc_Codigo = Doc.Doc_Tipo
			INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CC WITH(NOLOCK) ON  --VERIFICAR SI EL XML A SIDO INFORMADO Y ACEPTADO CORRECTAMENTE, SI ES ASI SE GENERA EL XML DE BAJA
			CC.Doc_Emisor_RUC = DocXML.Doc_Emisor_RUC AND CC.Doc_Tipo = DocXML.Doc_Tipo
			AND CC.Doc_Serie = DocXML.Doc_Serie AND CC.Doc_Numero = DocXML.Doc_Numero 
			AND  DocXML.TipXML_Codigo='003' AND CC.TipXML_Codigo<>'003' --RELACIONAR XML DE ENVIO, CON XML DE BAJA
			AND (CC.EstEnv_Codigo='003' OR CC.EstEnv_Codigo='007') -- VERIFICAR SI EL XML DE ENVIO ESTA ACEPTADO 
			AND CAST(ISNULL((CC.XML_Aceptado_CDR_FechaHora),doc.Doc_FechaEmision) AS DATE)>=DATEADD(DAY,-3,CAST(@AhoraFechaHora AS DATE)) --VERIFICAR EL DIA QUE ACEPTO EL CDR, Y SOLO DEBE SER 3 DIAS ANTES DE LA FECHA ACTUAL.	
			WHERE Doc.Doc_Emisor_RUC = @Num_Emisor
			AND DocXML.EstEnv_Codigo='001' -- PENDIENTE ENVIO
			AND DocXML.TipXML_Codigo='003' -- BAJA
			AND Doc.Doc_FechaEmision = @Num_Fecha_Emision AND Tdoc.GrpTipo_Codigo = @Num_Grp
			AND (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK)--VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI NO CONSIDERA BOLETAS NI SUS NOTAS DE CREDITO Y DEBITO
			INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
			ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
			WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
			AND ProgX.Progra_Estado='001' 
			AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
			AND ProgX.Emp_RUC=doc.Doc_Emisor_RUC)
			THEN (CASE WHEN (doc.Doc_Tipo ='03' OR doc.Doc_Tipo ='07' OR doc.Doc_Tipo ='08') 
			THEN (CASE WHEN doc.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
			ELSE 0 END) 
			ELSE 0 END)=0
			ORDER BY Doc.Doc_Tipo ASC, Tdoc.GrpTipo_Codigo ASC, Doc.Doc_FechaEmision ASC, Doc.Doc_Serie ASC, Doc.Doc_Numero ASC ;
		--ACTUALIZAR ESTADO DEL DOCUMENTO XML DE BAJA
			UPDATE dex
			SET dex.EstEnv_Codigo='009'
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
			INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc WITH(NOLOCK) ON Tdoc.TipoDoc_Codigo = de.Doc_Tipo
			INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
			dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
			AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero AND DEX.TipXML_Codigo='003'
			INNER JOIN dbo.FACTE_COMUNICACION_BAJA_DOCUMENTO dcbj WITH(NOLOCK) ON 
			dcbj.RsmBj_Emisor_RUC=de.Doc_Emisor_RUC AND dcbj.Doc_Tipo = de.Doc_Tipo AND 
            dcbj.Doc_Serie = de.Doc_Serie AND dcbj.Doc_Numero = de.Doc_Numero AND dcbj.RsmBj_GrpTipo_Codigo=Tdoc.GrpTipo_Codigo
			INNER JOIN dbo.FACTE_COMUNICACION_BAJA bj WITH(NOLOCK) ON 
            bj.RsmBj_Emisor_RUC = dcbj.RsmBj_Emisor_RUC AND bj.RsmBj_GrpTipo_Codigo = dcbj.RsmBj_GrpTipo_Codigo 
			AND bj.RsmBj_FechaGeneracion = dcbj.RsmBj_FechaGeneracion AND bj.RsmBj_Resumen_Correlativo = dcbj.RsmBj_Resumen_Correlativo
			WHERE  bj.RsmBj_Emisor_RUC = @Num_Emisor AND bj.RsmBj_GrpTipo_Codigo = @Num_Grp 
			AND bj.RsmBj_FechaGeneracion = @Fecha_Comun AND bj.RsmBj_Resumen_Correlativo = @Correlativo_Actual;
			
			--ACTUALIZAR CAMPOS DEL DOCUMENTO ELECTRONICO
			UPDATE de
			SET  de.Doc_Baja_GrpTipo_Codigo = @Num_Grp, de.Doc_Baja_FechaGeneracion = @Fecha_Comun, de.Doc_Baja_Correlativo = @Correlativo_Actual
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
			INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc WITH(NOLOCK) ON Tdoc.TipoDoc_Codigo = de.Doc_Tipo
			INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
			dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
			AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero AND DEX.TipXML_Codigo='003'
			INNER JOIN dbo.FACTE_COMUNICACION_BAJA_DOCUMENTO dcbj WITH(NOLOCK) ON 
			dcbj.RsmBj_Emisor_RUC=de.Doc_Emisor_RUC AND dcbj.Doc_Tipo = de.Doc_Tipo AND 
            dcbj.Doc_Serie = de.Doc_Serie AND dcbj.Doc_Numero = de.Doc_Numero AND dcbj.RsmBj_GrpTipo_Codigo=Tdoc.GrpTipo_Codigo
			INNER JOIN dbo.FACTE_COMUNICACION_BAJA bj WITH(NOLOCK) ON 
            bj.RsmBj_Emisor_RUC = dcbj.RsmBj_Emisor_RUC AND bj.RsmBj_GrpTipo_Codigo = dcbj.RsmBj_GrpTipo_Codigo 
			AND bj.RsmBj_FechaGeneracion = dcbj.RsmBj_FechaGeneracion AND bj.RsmBj_Resumen_Correlativo = dcbj.RsmBj_Resumen_Correlativo
			WHERE  bj.RsmBj_Emisor_RUC = @Num_Emisor AND bj.RsmBj_GrpTipo_Codigo = @Num_Grp 
			AND bj.RsmBj_FechaGeneracion = @Fecha_Comun AND bj.RsmBj_Resumen_Correlativo = @Correlativo_Actual
			--actualizando el XML enviado
			--UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET EstEnv_Codigo='005'
			--FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
			--INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc ON 
			--Doc.Doc_Emisor_RUC = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Emisor_RUC 
			--AND Doc.Doc_Tipo = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Tipo 
			--AND Doc.Doc_Serie = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Serie 
			--AND Doc.Doc_Numero = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Numero 
			--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.XML_Reciente = 1
			--INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tdoc ON Tdoc.TipoDoc_Codigo = Doc.Doc_Tipo
			--WHERE Doc.Doc_Emisor_RUC = @Num_Emisor
			--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.EstEnv_Codigo='001' -- PENDIENTE ENVIO
			--AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.TipXML_Codigo='003' -- BAJA
			--AND Doc.Doc_FechaEmision = @Num_Fecha_Emision AND Tdoc.GrpTipo_Codigo = @Num_Grp;
			--actualizando la cabecera con los datos del resumen
			--UPDATE FACTE_DOCUMENTO_ELECTRONICO SET Doc_Baja_GrpTipo_Codigo = @Num_Grp , Doc_Baja_FechaGeneracion = @Fecha_Comun, Doc_Baja_Correlativo = @Correlativo_Actual
			--FROM dbo.FACTE_DOCUMENTO_ELECTRONICO
			--INNER JOIN FACTE_COMUNICACION_BAJA_DOCUMENTO BjDoc ON BjDoc.RsmBj_Emisor_RUC = FACTE_DOCUMENTO_ELECTRONICO.Doc_Emisor_RUC AND BjDoc.Doc_Tipo = FACTE_DOCUMENTO_ELECTRONICO.Doc_Tipo AND BjDoc.Doc_Serie = FACTE_DOCUMENTO_ELECTRONICO.Doc_Serie AND BjDoc.Doc_Numero = FACTE_DOCUMENTO_ELECTRONICO.Doc_Numero
			--WHERE BjDoc.RsmBj_Emisor_RUC = @Num_Emisor AND BjDoc.RsmBj_GrpTipo_Codigo = @Num_Grp AND BjDoc.RsmBj_FechaGeneracion = @Fecha_Comun AND BjDoc.RsmBj_Resumen_Correlativo = @Correlativo_Actual ;
			
		END
		
		DROP TABLE #fe_grupo_series_correlativos;
	END

	DROP TABLE #Temp_Prog_Resumenes;

	--SOLO MOSTRAMOS LAS CABECERAS
	SELECT Diar.RsmBj_Emisor_RUC,Emp.Emp_NombreRazonSocial, Diar.RsmBj_FechaGeneracion, Diar.RsmBj_Resumen_Correlativo, Diar.RsmBj_Fecha_Generacion_Doc_Baja, Emp.Path_Root_App + '\' + Diar.RsmBj_Emisor_RUC, Diar.RsmBj_GrpTipo_Codigo
	FROM FACTE_COMUNICACION_BAJA Diar WITH(NOLOCK)
	INNER JOIN dbo.FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Diar.RsmBj_Emisor_RUC
	WHERE Diar.EstEnv_Codigo='001'
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_INSERT]
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8),
@PXML_Correlativo INT,
@PXML_Archivo_Item INT OUTPUT,
@PXML_Archivo_Alias VARCHAR(30),
@PXML_Archivo_Nombre varchar(150)
)
AS
BEGIN
	SET @PXML_Archivo_Item =ISNULL((SELECT MAX(Adj.XML_Archivo_Item) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO Adj 
	WHERE Adj.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Adj.Doc_Tipo = @PDoc_Tipo AND Adj.Doc_Serie = @PDoc_Serie
	AND Adj.Doc_Numero = @PDoc_Numero),0)+1;

	INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO
	( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
			  --XML_Correlativo ,
		XML_Archivo_Item ,
		XML_Archivo_NombreLocal, XML_Archivo_Nombre ,
		Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
	)
	VALUES  
	( @PDoc_Emisor_RUC, @PDoc_Tipo, @PDoc_Serie, @PDoc_Numero,
			  --@PXML_Correlativo,
		@PXML_Archivo_Item,
		@PXML_Archivo_Alias, @PXML_Archivo_Nombre,
		GETDATE() , HOST_NAME(), 'User');			
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_BUSQUEDA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_BUSQUEDA]
(
@PDoc_Fecha_Desde DATE,
@PDoc_Fecha_Hasta DATE,
@PDoc_Emisor_RUC CHAR(11)=NULL,
@PDoc_Tipo CHAR(2)=NULL,
@PDoc_Serie CHAR(4)=NULL,
@PDoc_Numero CHAR(8)=NULL
)
AS
BEGIN
--SELECT
--DocX.Doc_Emisor_RUC, DocX.Doc_Tipo, Tipo.TipoDoc_Descripcion, 
--DocX.Doc_Serie, DocX.Doc_Numero, DocX.Doc_FechaEmision, ISNULL(DocX.Doc_Moneda,''), ISNULL(Mon.Mon_Simbolo,''), ISNULL(DocX.Doc_Importe,0.00), 
--Docx.Doc_Cliente_TipoDocIdent_Codigo, DocX.Doc_Cliente_TipoDocIdent_Numero, SocNeg.SocNeg_RazonSocialNombres,
--DocX.EstDoc_Codigo, EstDoc.EstDoc_Descripcion, DocX.EstEnv_Codigo, Est.EstEnv_Descripcion,
--Empr.Path_Root_App,
--DocX.Doc_NombreArchivoXML, ISNULL(DocX.Doc_NombreArchivoPDF,''),
--DocX.Doc_NombreArchivoWeb, DocX.Doc_Correo_Enviado, DocX.Doc_Correo_EstaLeido,
--DocX.Doc_Resumen_FechaGeneracion, DocX.Doc_Resumen_Correlativo,
--DocX.Doc_Baja_FechaGeneracion, DocX.Doc_Baja_Correlativo
--FROM (
--	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero, Doc.Doc_FechaEmision, Doc.Doc_Moneda, Doc.Doc_Importe,
--	Doc.Doc_Cliente_TipoDocIdent_Codigo, Doc.Doc_Cliente_TipoDocIdent_Numero, Doc.EstDoc_Codigo,
--	(SELECT TOP 1 cXML.EstEnv_Codigo FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML
--	WHERE cXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND cXML.Doc_Tipo = Doc.Doc_Tipo AND cXML.Doc_Serie = Doc.Doc_Serie AND cXML.Doc_Numero = Doc.Doc_Numero ORDER BY cXML.XML_Correlativo DESC) AS 'EstEnv_Codigo',
--	ISNULL(Doc.Doc_NombreArchivoXML,'') AS 'Doc_NombreArchivoXML', ISNULL(Doc.Doc_NombreArchivoPDF,'') AS 'Doc_NombreArchivoPDF', 
--	ISNULL(Doc.Doc_NombreArchivoWeb,'') AS 'Doc_NombreArchivoWeb', ISNULL(Doc.Doc_Correo_Enviado,0) AS 'Doc_Correo_Enviado', ISNULL(Doc.Doc_Correo_EstaLeido,0) AS 'Doc_Correo_EstaLeido',
--	Doc.Doc_Resumen_FechaGeneracion, Doc.Doc_Resumen_Correlativo,
--	Doc.Doc_Baja_FechaGeneracion, Doc.Doc_Baja_Correlativo
--	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
--	WHERE (Doc.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
--	AND (@PDoc_Emisor_RUC IS NULL OR Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
--	AND (@PDoc_Tipo IS NULL OR Doc.Doc_Tipo = @PDoc_Tipo)
--	AND (@PDoc_Serie IS NULL OR Doc.Doc_Serie = @PDoc_Serie)
--	AND (@PDoc_Numero IS NULL OR Doc.Doc_Numero = @PDoc_Numero)
--	) DocX
--	INNER JOIN dbo.FACTE_SOCIO_NEGOCIO SocNeg WITH(NOLOCK) ON DocX.Doc_Cliente_TipoDocIdent_Codigo = SocNeg.TipoDocIdent_Codigo AND DocX.Doc_Cliente_TipoDocIdent_Numero = SocNeg.SocNeg_TipoDocIdent_Numero
--	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON DocX.Doc_Tipo = Tipo.TipoDoc_Codigo
--	LEFT JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON DocX.Doc_Moneda = Mon.Mon_Codigo
--	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = DocX.Doc_Emisor_RUC
--	INNER JOIN dbo.FACTE_ESTADO_DOCUMENTO_ELECTRONICO EstDoc WITH(NOLOCK) ON DocX.EstDoc_Codigo = EstDoc.EstDoc_Codigo
--	INNER JOIN dbo.FACTE_ESTADO_ENVIO Est WITH(NOLOCK) ON DocX.EstEnv_Codigo = Est.EstEnv_Codigo
--	ORDER BY DocX.Doc_FechaEmision DESC, DocX.Doc_Emisor_RUC DESC, DocX.Doc_Tipo ASC, DocX.Doc_Serie DESC, DocX.Doc_Numero DESC ;

	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo, Tipo.TipoDoc_Descripcion, 
Doc.Doc_Serie, Doc.Doc_Numero, Doc.Doc_FechaEmision, ISNULL(Doc.Doc_Moneda,''), ISNULL(Mon.Mon_Simbolo,''), ISNULL(Doc.Doc_Importe,0.00), 
Doc.Doc_Cliente_TipoDocIdent_Codigo, Doc.Doc_Cliente_TipoDocIdent_Numero, SocNeg.SocNeg_RazonSocialNombres,
Doc.EstDoc_Codigo, EstDoc.EstDoc_Descripcion, DocXML.EstEnv_Codigo, Est.EstEnv_Descripcion,
Empr.Path_Root_App,
ISNULL(Doc.Doc_NombreArchivoXML,''), ISNULL(Doc.Doc_NombreArchivoPDF,''),
ISNULL(Doc.Doc_NombreArchivoWeb,''), ISNULL(Doc.Doc_Correo_Enviado,0), ISNULL(Doc.Doc_Correo_EstaLeido,0),
Doc.Doc_Resumen_FechaGeneracion, Doc.Doc_Resumen_Correlativo,
Doc.Doc_Baja_FechaGeneracion, Doc.Doc_Baja_Correlativo,ISNULL(SocNeg.SocNeg_Correo_Electronico,''), ISNULL(Empr.Emp_NombreRazonSocial,'')
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1
	INNER JOIN dbo.FACTE_SOCIO_NEGOCIO SocNeg WITH(NOLOCK) ON SocNeg.TipoDocIdent_Codigo = Doc.Doc_Cliente_TipoDocIdent_Codigo AND SocNeg.SocNeg_TipoDocIdent_Numero = Doc.Doc_Cliente_TipoDocIdent_Numero
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON Tipo.TipoDoc_Codigo = Doc.Doc_Tipo
	LEFT JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON Mon.Mon_Codigo = Doc.Doc_Moneda
	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = Doc.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_ESTADO_DOCUMENTO_ELECTRONICO EstDoc WITH(NOLOCK) ON EstDoc.EstDoc_Codigo = Doc.EstDoc_Codigo
	INNER JOIN dbo.FACTE_ESTADO_ENVIO Est WITH(NOLOCK) ON Est.EstEnv_Codigo = DocXML.EstEnv_Codigo
	WHERE 
	--DocXML.TipXML_Codigo<>'003' AND 
	(Doc.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
	AND (@PDoc_Emisor_RUC IS NULL OR Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
	AND (@PDoc_Tipo IS NULL OR Doc.Doc_Tipo = @PDoc_Tipo)
	AND (@PDoc_Serie IS NULL OR Doc.Doc_Serie = @PDoc_Serie)
	AND (@PDoc_Numero IS NULL OR Doc.Doc_Numero = @PDoc_Numero)
	ORDER BY Doc.Doc_FechaEmision DESC, Doc.Doc_Tipo ASC, Doc.Doc_Serie DESC, Doc.Doc_Numero DESC ;
END

GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_DISABLE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_DISABLE]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_RazonAnulacion VARCHAR(150),

@Retorno VARCHAR(5) OUTPUT,

@PDoc_Usuario VARCHAR(50),
@PDoc_XML_NombreIN VARCHAR(150)=NULL,
@PDoc_XML_Nombre VARCHAR(150) OUTPUT
)
AS
BEGIN
	DECLARE @Existe INT, @Estado CHAR(3), @Correlativo INT, @TipXML_Codigo CHAR(3);

	SELECT TOP 1 @Existe = DocElect.Doc_Numero, @Estado = ISNULL(cXML.EstEnv_Codigo,'001'), @TipXML_Codigo = cXML.TipXML_Codigo
	FROM FACTE_DOCUMENTO_ELECTRONICO DocElect WITH(NOLOCK)
	LEFT JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML ON cXML.Doc_Emisor_RUC = DocElect.Doc_Emisor_RUC 
	AND cXML.Doc_Tipo = DocElect.Doc_Tipo AND cXML.Doc_Serie = DocElect.Doc_Serie 
	AND cXML.Doc_Numero = DocElect.Doc_Numero AND cXML.XML_FilaActiva = 1 AND cXML.TipXML_Codigo='003' --baja
	WHERE DocElect.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND DocElect.Doc_Tipo = @PDoc_Tipo 
	AND DocElect.Doc_Serie = @PDoc_Serie AND DocElect.Doc_Numero = @PDoc_Numero
	
	ORDER BY cXML.XML_Correlativo DESC ;

	--SET @Estado = ISNULL(cXML.EstEnv_Codigo,'001');

	IF (@Existe > 0)
	BEGIN
		IF (@Estado = '001')
		BEGIN
			--UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO
			--SET EstDoc_Codigo = ''
			--WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero;
			IF (@PDoc_XML_NombreIN IS NULL)
			BEGIN
				SET @Correlativo = (ISNULL((SELECT MAX(Arch.XML_Correlativo) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Arch 
				WITH(NOLOCK) WHERE Arch.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Arch.Doc_Tipo = @PDoc_Tipo 
				AND Arch.Doc_Serie = @PDoc_Serie AND Arch.Doc_Numero = @PDoc_Numero),0)+1);
				SET @PDoc_XML_Nombre = @PDoc_Emisor_RUC + '-' + @PDoc_Tipo + '-' + @PDoc_Serie + '-' + @PDoc_Numero + '_V' + CAST(@Correlativo AS VARCHAR(3))+ '.xml';			
			END

			INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
					( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
						XML_Correlativo , XML_Reciente, XML_Archivo , TipXML_Codigo , EstEnv_Codigo, XML_FilaActiva,
						XML_MotivoBaja,
						Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
					)
			VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
						@Correlativo , 1, @PDoc_XML_Nombre , '003' , '001', 1,
						@PDoc_RazonAnulacion,
						GETDATE() , '' , @PDoc_Usuario
					);

			UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET XML_Reciente = 0
			WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero 
			AND XML_Correlativo < @Correlativo AND TipXML_Codigo<>'003';

			SET @Retorno = 1;		
		END
		IF (@Estado <> '001')
		BEGIN
			SET @Retorno = 0;--El ULTIMO XML LO DIO BAJA
		END
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_ENVIADO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_ENVIADO]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PXML_Correlativo INT,
@PEstEnv_Codigo CHAR(3),
@PSUNAT_Respuesta_Codigo CHAR(4)=NULL,
@PNombre_PDF VARCHAR(50)=NULL
)
AS
BEGIN
	UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE 
	SET EstEnv_Codigo = @PEstEnv_Codigo, SUNAT_Respuesta_Codigo = @PSUNAT_Respuesta_Codigo,
	XML_Enviado_FechaHora=GETDATE(),XML_Aceptado_CDR_FechaHora=GETDATE()
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie
	AND Doc_Numero = @PDoc_Numero AND XML_Correlativo = @PXML_Correlativo AND TipXML_Codigo<>'003' ;

	UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO  SET Doc_NombreArchivoPDF = @PNombre_PDF
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie
	AND Doc_Numero = @PDoc_Numero;
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_ESTADO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[UP_FACTE_DOCUMENTO_ELECTRONICO_ESTADO] '20447370272','01',	'FT01',	'00000003'
CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_ESTADO]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8)
)
AS
BEGIN
	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero, Doc.Doc_IGV, Doc.Doc_Importe, 
	Doc.Doc_FechaEmision, Doc.Doc_Cliente_TipoDocIdent_Codigo, Doc.Doc_Cliente_TipoDocIdent_Numero,
	Tip.TipXML_Descripcion, 
	(CASE WHEN xml_Baja.Doc_Emisor_RUC IS NOT NULL 
	THEN (CASE  xml_Baja.EstEnv_Codigo
  WHEN '005' THEN (SELECT TOP 1 EstEnv_Descripcion FROM dbo.FACTE_ESTADO_ENVIO WITH(NOLOCK) WHERE EstEnv_Codigo='009') 
  WHEN '006' THEN (SELECT TOP 1 EstEnv_Descripcion FROM dbo.FACTE_ESTADO_ENVIO WITH(NOLOCK) WHERE EstEnv_Codigo='010')  
  WHEN '007' THEN (SELECT TOP 1 EstEnv_Descripcion FROM dbo.FACTE_ESTADO_ENVIO WITH(NOLOCK) WHERE EstEnv_Codigo='011')
  WHEN '008' THEN (SELECT TOP 1 EstEnv_Descripcion FROM dbo.FACTE_ESTADO_ENVIO WITH(NOLOCK) WHERE EstEnv_Codigo='012')
  ELSE Est.EstEnv_Descripcion end) 
	ELSE Est.EstEnv_Descripcion END) AS 'EstEnv_Descripcion',
	--Est.EstEnv_Descripcion, 
	ISNULL(Act.XML_DigestValue,'') XML_DigestValue, ISNULL(Act.XML_SignatureValue,'') XML_SignatureValue, 
	Doc.Doc_NombreArchivoWeb,
	CASE WHEN xml_Baja.Doc_Emisor_RUC IS NULL THEN '0' ELSE '1' END, 
	(CASE WHEN xml_Baja.Doc_Emisor_RUC IS NOT NULL 
	THEN (CASE  xml_Baja.EstEnv_Codigo
  WHEN '005' THEN '009'
  WHEN '006' THEN '010' 
  WHEN '007' THEN '011'
  WHEN '008' THEN '012'
  ELSE Est.EstEnv_Codigo end) 
	ELSE Est.EstEnv_Codigo END) AS 'EstEnv_Codigo',
	--Est.EstEnv_Codigo AS 'Sunat Respuesta',
	DATEDIFF(DAY,ISNULL(Act.XML_Aceptado_CDR_FechaHora,Doc.Doc_FechaEmision),GETDATE()) AS 'Dias_Aceptacion'
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Act WITH(NOLOCK) ON Act.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND Act.Doc_Tipo = Doc.Doc_Tipo AND Act.Doc_Serie = Doc.Doc_Serie AND Act.Doc_Numero = Doc.Doc_Numero AND Act.XML_FilaActiva = 1
	INNER JOIN dbo.FACTE_TIPO_XML Tip WITH(NOLOCK) ON Tip.TipXML_Codigo = Act.TipXML_Codigo
	INNER JOIN dbo.FACTE_ESTADO_ENVIO Est WITH(NOLOCK) ON Est.EstEnv_Codigo = Act.EstEnv_Codigo
	LEFT JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE xml_Baja WITH(NOLOCK) ON
    xml_Baja.Doc_Emisor_RUC = Act.Doc_Emisor_RUC AND xml_Baja.Doc_Tipo = Act.Doc_Tipo 
	AND xml_Baja.Doc_Serie = Act.Doc_Serie AND xml_Baja.Doc_Numero = Act.Doc_Numero
	AND xml_Baja.TipXML_Codigo = '003'
	WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_Tipo = @PDoc_Tipo 
	AND Doc.Doc_Serie = @PDoc_Serie AND Doc.Doc_Numero = @PDoc_Numero AND Act.TipXML_Codigo<>'003';
END


--select * from FACTE_DOCUMENTO_ELECTRONICO

GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_INSERT_UPDATE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_INSERT_UPDATE]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_Moneda CHAR(3)=NULL,
@PDoc_FechaEmision DATE,

@PDoc_Oper_Gravadas DECIMAL(15,2)=NULL, --nuevo
@PDoc_Oper_Exoneradas DECIMAL(15,2)=NULL, --nuevo
@PDoc_Oper_Inafectas DECIMAL(15,2)=NULL, --nuevo
@PDoc_Oper_Gratuitas DECIMAL(15,2)=NULL, --nuevo
@PDoc_ISC DECIMAL(15,2)=NULL, --nuevo
@PDoc_Otros_Tributos DECIMAL(15,2)=NULL, --nuevo
@PDoc_Otros_Conceptos DECIMAL(15,2)=NULL, --nuevo

@PDoc_IGV DECIMAL(15,2),
@PDoc_Importe DECIMAL(15,2),

@PDoc_TipoDocIdent_Codigo CHAR(1),
@PDoc_TipoDocIdent_Numero VARCHAR(12),
@PDoc_SocNeg_RazonSocialNombres VARCHAR(150),
@PDoc_Cliente_Correo VARCHAR(50)=NULL,

@PDoc_Modif_Doc_Tipo CHAR(2)=NULL, --nuevo
@PDoc_Modif_Doc_Serie CHAR(4)=NULL, --nuevo
@PDoc_Modif_Doc_Numero VARCHAR(8)=NULL, --nuevo

@PDoc_NombreArchivoXML VARCHAR(50) = NULL,

@PDoc_XML_DigestValue VARCHAR(50)=NULL,
@PDoc_XML_SignatureValue VARCHAR(500)=NULL,

@Retorno VARCHAR(5) OUTPUT,
@Correlativo INT OUTPUT,
@PDoc_Usuario VARCHAR(50),
@PDoc_XML_Nombre VARCHAR(150) OUTPUT,
@PDoc_NombreArchivoWeb VARCHAR(50) OUTPUT
)
AS
BEGIN
	DECLARE @Existe INT, @Estado CHAR(3), @TipXML_Codigo CHAR(3);

	--INSERTAMOS Y ACTUALIZAMOS -- CLIENTE -- INICIO
	UPDATE FACTE_SOCIO_NEGOCIO SET SocNeg_RazonSocialNombres = @PDoc_SocNeg_RazonSocialNombres, SocNeg_Correo_Electronico = ISNULL(@PDoc_Cliente_Correo, SocNeg_Correo_Electronico)
	WHERE TipoDocIdent_Codigo = @PDoc_TipoDocIdent_Codigo AND SocNeg_TipoDocIdent_Numero = @PDoc_TipoDocIdent_Numero ;

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO dbo.FACTE_SOCIO_NEGOCIO
				( TipoDocIdent_Codigo ,
				  SocNeg_TipoDocIdent_Numero ,
				  SocNeg_RazonSocialNombres,
				  SocNeg_Correo_Electronico
				)
		VALUES  ( @PDoc_TipoDocIdent_Codigo , -- TipoDocIdent_Codigo - char(1)
				  @PDoc_TipoDocIdent_Numero , -- SocNeg_TipoDocIdent_Numero - varchar(15)
				  @PDoc_SocNeg_RazonSocialNombres,  -- SocNeg_RazonSocialNombres - varchar(150)
				  @PDoc_Cliente_Correo
				);
	END
	--INSERTAMOS Y ACTUALIZAMOS -- CLIENTE -- FIN

	SELECT TOP 1 @Existe =cXML.XML_Correlativo, @Estado = cXML.EstEnv_Codigo, @TipXML_Codigo = cXML.TipXML_Codigo,
	 @PDoc_NombreArchivoWeb = DocElect.Doc_NombreArchivoWeb
	FROM FACTE_DOCUMENTO_ELECTRONICO DocElect WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML WITH(NOLOCK) ON cXML.Doc_Emisor_RUC = DocElect.Doc_Emisor_RUC AND cXML.Doc_Tipo = DocElect.Doc_Tipo AND cXML.Doc_Serie = DocElect.Doc_Serie AND cXML.Doc_Numero = DocElect.Doc_Numero
	WHERE cXML.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND cXML.Doc_Tipo = @PDoc_Tipo 
	AND cXML.Doc_Serie = @PDoc_Serie AND cXML.Doc_Numero = @PDoc_Numero
	AND cXML.XML_FilaActiva = 1
	AND cXML.TipXML_Codigo <> '003'
	ORDER BY cXML.XML_Correlativo DESC ;


	IF (@Existe > 0)
	BEGIN
		--IF (@TipXML_Codigo<>'003')
		--BEGIN
			IF (@Estado = '001') --SIN ENVIAR
			BEGIN
				UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO 
				SET Doc_FechaEmision = @PDoc_FechaEmision, Doc_Moneda = @PDoc_Moneda,

				Doc_Oper_Gravadas = @PDoc_Oper_Gravadas , Doc_Oper_Exoneradas = @PDoc_Oper_Exoneradas , 
				Doc_Oper_Inafectas = @PDoc_Oper_Inafectas ,
		        Doc_Oper_Gratuitas = @PDoc_Oper_Gratuitas , Doc_ISC = @PDoc_ISC , 
				Doc_Otros_Tributos = @PDoc_Otros_Tributos , Doc_Otros_Conceptos = @PDoc_Otros_Conceptos ,   

				Doc_IGV = @PDoc_IGV, Doc_Importe = @PDoc_Importe, 
				Doc_Cliente_TipoDocIdent_Codigo = @PDoc_TipoDocIdent_Codigo, Doc_Cliente_TipoDocIdent_Numero = @PDoc_TipoDocIdent_Numero ,

				Doc_Modif_Doc_Tipo = @PDoc_Modif_Doc_Tipo , Doc_Modif_Doc_Serie = @PDoc_Modif_Doc_Serie , Doc_Modif_Doc_Numero = @PDoc_Modif_Doc_Numero

				WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero;

				SET @Correlativo = (ISNULL((SELECT MAX(Arch.XML_Correlativo) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Arch WITH(NOLOCK) WHERE Arch.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Arch.Doc_Tipo = @PDoc_Tipo AND Arch.Doc_Serie = @PDoc_Serie AND Arch.Doc_Numero = @PDoc_Numero),0)+1);
				SET @PDoc_XML_Nombre = @PDoc_Emisor_RUC + '-' + @PDoc_Tipo + '-' + @PDoc_Serie + '-' + @PDoc_Numero + '_V' + CAST(@Correlativo AS VARCHAR(3))+ '.xml';

				INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
						( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
						  XML_Correlativo , XML_Reciente, XML_Archivo , TipXML_Codigo , EstEnv_Codigo, XML_FilaActiva,
						  XML_DigestValue, XML_SignatureValue, XML_Creado_FechaHora,
						  Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
						)
				VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
						  @Correlativo , 1, @PDoc_XML_Nombre , '001' , '001', 1,
						  @PDoc_XML_DigestValue, @PDoc_XML_SignatureValue,GETDATE(),
						  GETDATE() , '' , @PDoc_Usuario
						);
				
				--UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET XML_Reciente = 0
				--WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Correlativo < @Correlativo --AND TipXML_Codigo<>'003'; elimina la anulacion, no puede actualizar si tiene un pendiente de anulacion, por eso eliminamos la anulacion

				SET @Retorno = 1;
			END
			IF (@Estado <> '001') --YA SE ENVIO
			BEGIN
				SET @Retorno = 0;--YA SE ENVIO NO SE PUEDE ACTUALIZAR EL DOCUMENTO
			END			
		--END
		--IF (@TipXML_Codigo='003')
		--BEGIN
		--	SET @Retorno = -1;--El ULTIMO XML LO DIO BAJA
		--END
	END
	ELSE
	BEGIN
		
		SET @PDoc_NombreArchivoWeb = newid();
		WHILE EXISTS(SELECT Acep.Doc_NombreArchivoWeb FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Acep WITH(NOLOCK) WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb)
		BEGIN
			SET @PDoc_NombreArchivoWeb = newid();
		END

		INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO
		        ( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero , Doc_FechaEmision ,
		          Doc_Moneda , 
				  Doc_Oper_Gravadas , Doc_Oper_Exoneradas , Doc_Oper_Inafectas ,
		          Doc_Oper_Gratuitas , Doc_ISC , Doc_Otros_Tributos , Doc_Otros_Conceptos ,   
				  Doc_IGV, Doc_Importe , 
				  Doc_Cliente_TipoDocIdent_Codigo , Doc_Cliente_TipoDocIdent_Numero ,
				  Doc_Modif_Doc_Tipo , Doc_Modif_Doc_Serie , Doc_Modif_Doc_Numero ,
				  Doc_NombreArchivoXML,
		          EstDoc_Codigo , --, EstEnv_Codigo
				  Doc_NombreArchivoWeb
		        )
		VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero , @PDoc_FechaEmision , -- Doc_FechaEmision - date
		          @PDoc_Moneda , 
				  @PDoc_Oper_Gravadas , @PDoc_Oper_Exoneradas , @PDoc_Oper_Inafectas ,
		          @PDoc_Oper_Gratuitas , @PDoc_ISC , @PDoc_Otros_Tributos , @PDoc_Otros_Conceptos ,
				  @PDoc_IGV, @PDoc_Importe , 
				  @PDoc_TipoDocIdent_Codigo , @PDoc_TipoDocIdent_Numero , -- Doc_Cliente_Rs - varchar(150)
				  @PDoc_Modif_Doc_Tipo , @PDoc_Modif_Doc_Serie , @PDoc_Modif_Doc_Numero ,
				  @PDoc_NombreArchivoXML,
				  '001', --, '001'
				  @PDoc_NombreArchivoWeb
		        );
		SET @Correlativo = 1;
		SET @PDoc_XML_Nombre = @PDoc_Emisor_RUC + '-' + @PDoc_Tipo + '-' + @PDoc_Serie + '-' + @PDoc_Numero + '_V' + CAST(@Correlativo AS VARCHAR(3)) + '.xml';

		INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
			    ( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
			        XML_Correlativo , XML_Reciente , XML_Archivo , TipXML_Codigo , EstEnv_Codigo , XML_FilaActiva,
					XML_DigestValue, XML_SignatureValue,XML_Creado_FechaHora,
			        Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
			    )
		VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
			        @Correlativo , 1, @PDoc_XML_Nombre , '001' , '001' , 1,
					@PDoc_XML_DigestValue, @PDoc_XML_SignatureValue,GETDATE(),
			        GETDATE() , '' , @PDoc_Usuario
			    );
		SET @Retorno = 1;
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_MODIFICABLE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_MODIFICABLE]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
--@PEstado INT OUTPUT,
@PEstado BIT OUTPUT,
@PMensaje VARCHAR(50) OUTPUT
)
AS
BEGIN
	DECLARE @Existe INT, @EstadoEnvio CHAR(3), @TipoXML CHAR(3);
	SELECT TOP 1 @Existe = 1, @EstadoEnvio = cXML.EstEnv_Codigo, @TipoXML = cXML.TipXML_Codigo
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML
	WHERE cXML.XML_FilaActiva = 1 AND cXML.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND cXML.Doc_Tipo = @PDoc_Tipo 
	AND cXML.Doc_Serie = @PDoc_Serie AND cXML.Doc_Numero = @PDoc_Numero
	ORDER BY cXML.XML_Correlativo DESC
	;
	IF (@Existe IS NULL)
	BEGIN
		--SET @PEstado = -1 ;
		SET @PEstado = 0 ;
		SET @PMensaje = 'No existe el documento';
	END
	ELSE
    BEGIN
    	IF (@TipoXML <> '003')
		BEGIN
			IF (@EstadoEnvio = '001') --SIN ENVIAR
			BEGIN
				SET @PEstado = 1 ;
				SET @PMensaje = 'El documento aun no se envia a la sunat';
			END
			IF (@EstadoEnvio <> '001')
			BEGIN
				SET @PEstado = 0 ;
				SET @PMensaje = 'El documento fue enviado a la sunat';
			END
		END
		IF (@TipoXML = '003')
		BEGIN
			IF (@EstadoEnvio = '001') --SIN ENVIAR
			BEGIN
				--SET @PEstado = 2 ;
				SET @PEstado = 0 ;
				SET @PMensaje = 'El documento fue dado de baja, aun no se envia a la sunat';
			END
			IF (@EstadoEnvio <> '001')
			BEGIN
				SET @PEstado = 0 ;
				SET @PMensaje = 'El documento fue dado de baja y enviado a la sunat';
			END
		END
    END
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_VERSIONES]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_ELECTRONICO_VERSIONES]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8)
)
AS
BEGIN
	SELECT Ver.XML_Correlativo AS 'Item', Ver.Aud_Registro_FechaHora AS 'Fecha', Ver.XML_Archivo AS 'Archivo', 
	Tip.TipXML_Descripcion AS 'Tipo', Est.EstEnv_Descripcion AS 'Estado', Ver.SUNAT_Respuesta_Codigo AS 'Sunat',
	ISNULL(NomExep.Descripcion,'') AS 'Descrip_Sunat_Mensaje'
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Ver WITH(NOLOCK) ON Ver.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND Ver.Doc_Tipo = Doc.Doc_Tipo AND Ver.Doc_Serie = Doc.Doc_Serie AND Ver.Doc_Numero = Doc.Doc_Numero
	INNER JOIN dbo.FACTE_TIPO_XML Tip WITH(NOLOCK) ON Tip.TipXML_Codigo = Ver.TipXML_Codigo
	INNER JOIN dbo.FACTE_ESTADO_ENVIO Est WITH(NOLOCK) ON Est.EstEnv_Codigo = Ver.EstEnv_Codigo 
	left JOIN dbo.FACTE_CLASIFICACION_ESTADOS_EXEPCION NomExep WITH(NOLOCK) ON NomExep.Codigo= ver.SUNAT_Respuesta_Codigo
	WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_Tipo = @PDoc_Tipo AND Doc.Doc_Serie = @PDoc_Serie AND Doc.Doc_Numero = @PDoc_Numero
	ORDER BY Doc.Doc_FechaEmision DESC, Doc.Doc_Emisor_RUC DESC, Doc.Doc_Tipo ASC, Doc.Doc_Serie DESC, Doc.Doc_Numero DESC;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_TIPO_GRUPO_URL_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_TIPO_GRUPO_URL_LISTA]
AS
BEGIN
	SELECT GrpTipo_Codigo , Etapa_Codigo , GrpTipo_URL , ISNULL(GrpTipo_URL_Validez,''), ISNULL(GrpTipo_URL_Estado_Envio,'')
	FROM FACTE_DOCUMENTO_TIPO_GRUPO_URL Url WITH(NOLOCK)
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTO_TIPO_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTO_TIPO_LISTA]
(
@PTipoDoc_Codigo CHAR(2)=NULL
)
AS
BEGIN
	SELECT Dt.TipoDoc_Codigo, Dt.TipoDoc_Descripcion FROM FACTE_DOCUMENTO_TIPO Dt WITH(NOLOCK)
	WHERE (@PTipoDoc_Codigo IS NULL OR Dt.TipoDoc_Codigo = @PTipoDoc_Codigo);
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADO_CONTAR_LEIDOS]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADO_CONTAR_LEIDOS]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_NombreArchivoWeb VARCHAR(50)
)
AS
BEGIN
	DECLARE @Contador INT;
	SET @Contador =ISNULL((SELECT Doc.Doc_Correo_ContadorLectura FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb),0) + 1 ;
	
	UPDATE FACTE_DOCUMENTO_ELECTRONICO SET Doc_Correo_ContadorLectura = @Contador, Doc_Correo_EstaLeido = 1,
	Doc_Correo_LecturaFechaPrimera = (CASE WHEN @Contador = 1 THEN GETDATE() ELSE Doc_Correo_LecturaFechaPrimera END), Doc_Correo_LecturaFechaUltima = GETDATE()
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADOS_ACTUALIZAR_ESTADO_ENVIO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADOS_ACTUALIZAR_ESTADO_ENVIO]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_NombreArchivoWeb VARCHAR(50)
)
AS
BEGIN
	UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO
	SET Doc_Correo_Enviado = 1
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADOS_BUSQUEDA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADOS_BUSQUEDA]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_FechaEmision DATE,
@PDoc_Importe DECIMAL(15,2),
@PExiste BIT OUTPUT,
@PDoc_NombreArchivoWeb VARCHAR(50) OUTPUT
)
AS
BEGIN
	SELECT @PExiste = 1, @PDoc_NombreArchivoWeb = Acep.Doc_NombreArchivoWeb
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	WHERE Acep.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Acep.Doc_Tipo = @PDoc_Tipo 
	AND Acep.Doc_Serie = @PDoc_Serie AND Acep.Doc_Numero = @PDoc_Numero AND Acep.Doc_FechaEmision = @PDoc_FechaEmision AND Acep.Doc_Importe = @PDoc_Importe ;
	SET @PExiste = ISNULL(@PExiste,0);
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADOS_IDWEB]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_ACEPTADOS_IDWEB]
(
@PDoc_NombreArchivoWeb VARCHAR(50),---PARA LOS ENLACES ENVIADOS POR CORREO
@PDoc_TipoDoc_Descripcion VARCHAR(40) OUTPUT,
@PDoc_Serie CHAR(4) OUTPUT,
@PDoc_Numero CHAR(8) OUTPUT,
@PDoc_FechaEmision DATE OUTPUT,
@PDoc_Moneda_Abreviado VARCHAR(5) OUTPUT,
@PDoc_Importe DECIMAL(15,2) OUTPUT,
@PDoc_Cliente_Rs VARCHAR(150) OUTPUT
)
AS
BEGIN
	SELECT @PDoc_TipoDoc_Descripcion = DocTipo.TipoDoc_Descripcion , @PDoc_Serie = Acep.Doc_Serie, @PDoc_Numero = Acep.Doc_Numero,
	@PDoc_FechaEmision = Acep.Doc_FechaEmision, @PDoc_Moneda_Abreviado = Mon.Mon_Simbolo ,
	@PDoc_Importe = Acep.Doc_Importe, @PDoc_Cliente_Rs = SocNeg.SocNeg_RazonSocialNombres
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO DocTipo ON Acep.Doc_Tipo = DocTipo.TipoDoc_Codigo
	INNER JOIN dbo.FACTE_SOCIO_NEGOCIO SocNeg ON SocNeg.TipoDocIdent_Codigo = Acep.Doc_Cliente_TipoDocIdent_Codigo AND SocNeg.SocNeg_TipoDocIdent_Numero = Acep.Doc_Cliente_TipoDocIdent_Numero
	INNER JOIN dbo.FACTE_MONEDA Mon ON Acep.Doc_Moneda = Mon.Mon_Codigo
	WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_BUSQUEDA_IDWEB_PDF]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	PROC [dbo].[UP_FACTE_DOCUMENTOS_BUSQUEDA_IDWEB_PDF]
(
@PDoc_NombreArchivoWeb VARCHAR(50),
@PDoc_NombreArchivoPDF VARCHAR(250) OUTPUT
)
AS
BEGIN
	SELECT @PDoc_NombreArchivoPDF = Emp.Path_Root_App +'\'+ Acep.Doc_Emisor_RUC +'\xml_sunat_aceptados\' +CASE Acep.Doc_Tipo WHEN '01' THEN 'factura\' WHEN'07' THEN 'nota_credito\' WHEN'08' THEN 'nota_debito\' ELSE 'boleta\' END + Acep.Doc_NombreArchivoPDF --Emp.Path_Root_App + Acep.Doc_NombreArchivoPDF
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	INNER JOIN dbo.FACTE_EMPRESA Emp ON Emp.Emp_RUC = Acep.Doc_Emisor_RUC
	WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
	SET @PDoc_NombreArchivoPDF = ISNULL(@PDoc_NombreArchivoPDF,'');
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_BUSQUEDA_IDWEB_XML]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_BUSQUEDA_IDWEB_XML]
(
@PDoc_NombreArchivoWeb VARCHAR(50),
@PDoc_NombreArchivoXML VARCHAR(250) OUTPUT
)
AS
BEGIN
	SELECT @PDoc_NombreArchivoXML = Emp.Path_Root_App +'\'+ Acep.Doc_Emisor_RUC +'\xml_sunat_aceptados\' +CASE Acep.Doc_Tipo WHEN '01' THEN 'factura\' WHEN'07' THEN 'nota_credito\' WHEN'08' THEN 'nota_debito\' ELSE 'boleta\' END + Acep.Doc_NombreArchivoXML--Emp.Path_Root_App + Acep.Doc_NombreArchivoXML
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	INNER JOIN dbo.FACTE_EMPRESA Emp ON Emp.Emp_RUC = Acep.Doc_Emisor_RUC
	WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
	SET @PDoc_NombreArchivoXML = ISNULL(@PDoc_NombreArchivoXML,'');
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_LISTA]
(
@PDoc_Fecha_Desde DATE,
@PDoc_Fecha_Hasta DATE,
@PDoc_Emisor_RUC CHAR(11)=NULL,
@PDoc_Tipo CHAR(2)=NULL,
@PDoc_Serie CHAR(4)=NULL,
@PDoc_Numero CHAR(8)=NULL
)
AS
BEGIN
	SELECT Doc_Emisor_RUC, Doc_Tipo, Tipo.TipoDoc_Descripcion, Doc_Serie, Doc_Numero, Doc_FechaEmision, Doc_Moneda, Mon.Mon_Simbolo, Doc_Importe, 
	Doc_Cliente_NumDoc, Doc_Cliente_Rs,doc.Doc_Estado, Est.Est_Descripcion,
	Empr.PathLocal,Doc_NombreArchivoXML, Doc_NombreArchivoPDF, Doc_NombreArchivoWeb, ISNULL(Doc.Doc_Correo_Enviado,0), ISNULL(Doc.Doc_Correo_EstaLeido,0)
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON Doc.Doc_Tipo = Tipo.TipoDoc_Codigo
	INNER JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON Doc.Doc_Moneda = Mon.Mon_Codigo
	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = Doc.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_ESTADO Est WITH(NOLOCK) ON Doc.Doc_Estado = Est.Est_Codigo_Cliente
	WHERE (Doc.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
	AND (@PDoc_Emisor_RUC IS NULL OR Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
	AND (@PDoc_Tipo IS NULL OR Doc.Doc_Tipo = @PDoc_Tipo)
	AND (@PDoc_Serie IS NULL OR Doc.Doc_Serie = @PDoc_Serie)
	AND (@PDoc_Numero IS NULL OR Doc.Doc_Numero = @PDoc_Numero);
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_DOCUMENTOS_RESUMEN_BOLETAS_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_DOCUMENTOS_RESUMEN_BOLETAS_LISTA]
(
@PDoc_Fecha_Desde DATE,
@PDoc_Fecha_Hasta DATE,
@PDoc_Emisor_RUC CHAR(11)=NULL,
@PDoc_Tipo CHAR(2)=NULL,
@PDoc_Serie CHAR(4)=NULL,
@PDoc_Numero CHAR(8)=NULL
)
AS
BEGIN
	SELECT Resu.Doc_Emisor_RUC, Resu.Doc_FechaGeneracion, Resu.Doc_Resumen_Correlativo, Resu.Doc_Moneda, Mon.Mon_Simbolo, 
	Resu.Doc_FechaEmision, Resu.Doc_Tipo, Tipo.TipoDoc_Descripcion, Resu.Doc_Serie, Resu.Doc_NumeroInicio, Resu.Doc_NumeroFin, Resu.Doc_ImporteResumen, 	
	Empr.PathLocal, Resu.Doc_NombreArchivoXML, Resu.Doc_SUNATTicket_ID, Resu.Doc_Estado, Est.Est_Descripcion,
	Resu.Doc_NombreArchivoWeb
	FROM dbo.FACTE_RESUMEN_DOCUMENTOS_ACEPTADO Resu
	INNER JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON Resu.Doc_Moneda = Mon.Mon_Codigo
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON Resu.Doc_Tipo = Tipo.TipoDoc_Codigo	
	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = Resu.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_ESTADO Est WITH(NOLOCK) ON Resu.Doc_Estado = Est.Est_Codigo_Cliente
	WHERE (Resu.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
	AND (@PDoc_Emisor_RUC IS NULL OR Resu.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
	AND (@PDoc_Tipo IS NULL OR Resu.Doc_Tipo = @PDoc_Tipo)
	AND (@PDoc_Serie IS NULL OR Resu.Doc_Serie = @PDoc_Serie)
	AND (@PDoc_Numero IS NULL OR (CAST(@PDoc_Numero AS INT) BETWEEN CAST(Resu.Doc_NumeroInicio AS INT) AND CAST(Resu.Doc_NumeroFin AS INT)));
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_EMPRESA_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_EMPRESA_INSERT]
(
@PEmp_RUC CHAR(11),
@PEmp_NombreRazonSocial VARCHAR(150),
--@PEmp_EnviaResumenBoletas BIT,

@PSunat_Usuario VARCHAR(50),
@PSunat_Contrasena VARCHAR(50),
@PSunat_Autorizacion VARCHAR(50),---esto deberia estar registrado en las series

@PEmisor_Web_Visualizacion VARCHAR(50),--web donde va ver el cliente su factura boleta

@PCorreo_Usuario VARCHAR(50),
@PCorreo_Contrasena VARCHAR(50),
@PCorreo_ServidorSMTP VARCHAR(50),
@PCorreo_ServidorPuerto VARCHAR(6),
@PCorreo_ServidorSSL BIT,

@PEtapa_Codigo CHAR(3),

@PPath_Root_App VARCHAR(150),

--@PPathLocal VARCHAR(150), --Ruta que se va almacenar localmente el xml original
--@PPathServer VARCHAR(150), --Ruta del servidor

@PFTP_Direccion VARCHAR(50),
@PFTP_Carpeta VARCHAR(200),
@PFTP_Usuario VARCHAR(50),
@PFTP_Contrasena VARCHAR(50),

@PActualizarClientes BIT
)
AS
BEGIN
	INSERT INTO dbo.FACTE_EMPRESA
	        ( Emp_RUC , Emp_NombreRazonSocial , --Emp_EnviaResumenBoletas ,
	          Sunat_Usuario , Sunat_Contrasena , Sunat_Autorizacion , Emisor_Web_Visualizacion ,
	          Correo_Usuario , Correo_Contrasena , Correo_ServidorSMTP , Correo_ServidorPuerto , Correo_ServidorSSL ,
	          Etapa_Codigo , Path_Root_App, 
			  --PathLocal , PathServer ,
	          FTP_Direccion , FTP_Carpeta , FTP_Usuario , FTP_Contrasena ,
	          ActualizarClientes ,
	          Aud_Actualizacion_Fecha , Aud_Actualizacion_Autor , Aud_Actualizacion_Equipo
	        )
	VALUES  ( 
			@PEmp_RUC , @PEmp_NombreRazonSocial , --@PEmp_EnviaResumenBoletas ,
			@PSunat_Usuario , @PSunat_Contrasena , @PSunat_Autorizacion , @PEmisor_Web_Visualizacion ,--web donde va ver el cliente su factura boleta
			@PCorreo_Usuario , @PCorreo_Contrasena , @PCorreo_ServidorSMTP , @PCorreo_ServidorPuerto , @PCorreo_ServidorSSL ,
			@PEtapa_Codigo , @PPath_Root_App, 
			--@PPathLocal , @PPathServer , --Ruta del servidor
			@PFTP_Direccion , @PFTP_Carpeta , @PFTP_Usuario , @PFTP_Contrasena ,
			@PActualizarClientes,
			GETDATE(), 'App', HOST_NAME() );
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_EMPRESA_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_EMPRESA_LISTA]
(
@PEmp_RUC CHAR(11)=NULL
)
AS
BEGIN
	SELECT Emp.Emp_RUC, Emp.Emp_NombreRazonSocial, --Emp.Emp_EnviaResumenBoletas,
	Emp.Sunat_Usuario, Emp.Sunat_Contrasena, Emp.Sunat_Autorizacion,
	Emp.Emisor_Web_Visualizacion,
	Emp.Correo_Usuario, Emp.Correo_Contrasena, Emp.Correo_ServidorSMTP, Emp.Correo_ServidorPuerto, Emp.Correo_ServidorSSL,
	Emp.Etapa_Codigo,Etapa.Etapa_Descripcion,
	--Emp.PathLocal_XMLCliente, Emp.PathLocal, Emp.PathServe
	Emp.Path_Root_App,
	Emp.FTP_Direccion, Emp.FTP_Carpeta, Emp.FTP_Usuario, Emp.FTP_Contrasena, Emp.ActualizarClientes
	FROM dbo.FACTE_EMPRESA Emp WITH(NOLOCK)
	INNER JOIN dbo.FACTE_ETAPA Etapa WITH(NOLOCK) ON Etapa.Etapa_Codigo = Emp.Etapa_Codigo
	WHERE (@PEmp_RUC IS NULL OR Emp.Emp_RUC = @PEmp_RUC);
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_EMPRESA_UPDATE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_EMPRESA_UPDATE]
(
@PEmp_RUC CHAR(11),
@PEmp_NombreRazonSocial VARCHAR(150),
--@PEmp_EnviaResumenBoletas BIT,

@PSunat_Usuario VARCHAR(50),
@PSunat_Contrasena VARCHAR(50),
@PSunat_Autorizacion VARCHAR(50),---esto deberia estar registrado en las series

@PEmisor_Web_Visualizacion VARCHAR(50),--web donde va ver el cliente su factura boleta

@PCorreo_Usuario VARCHAR(50),
@PCorreo_Contrasena VARCHAR(50),
@PCorreo_ServidorSMTP VARCHAR(50),
@PCorreo_ServidorPuerto VARCHAR(6),
@PCorreo_ServidorSSL BIT,

@PEtapa_Codigo CHAR(3),

@PPath_Root_App VARCHAR(150),

--@PPathLocal VARCHAR(150), --Ruta que se va almacenar localmente el xml original
--@PPathServer VARCHAR(150), --Ruta del servidor

@PFTP_Direccion VARCHAR(50),
@PFTP_Carpeta VARCHAR(200),
@PFTP_Usuario VARCHAR(50),
@PFTP_Contrasena VARCHAR(50),

@PActualizarClientes BIT
)
AS
BEGIN
	UPDATE FACTE_EMPRESA SET
	Emp_RUC = @PEmp_RUC, Emp_NombreRazonSocial = @PEmp_NombreRazonSocial, --Emp_EnviaResumenBoletas = @PEmp_EnviaResumenBoletas,
	Sunat_Usuario = @PSunat_Usuario , Sunat_Contrasena = @PSunat_Contrasena, Sunat_Autorizacion = @PSunat_Autorizacion, Emisor_Web_Visualizacion = @PEmisor_Web_Visualizacion ,
	Correo_Usuario = @PCorreo_Usuario, Correo_Contrasena = @PCorreo_Contrasena, Correo_ServidorSMTP = @PCorreo_ServidorSMTP, Correo_ServidorPuerto = @PCorreo_ServidorPuerto, Correo_ServidorSSL = @PCorreo_ServidorSSL,
	Etapa_Codigo = @PEtapa_Codigo, 
	Path_Root_App = @PPath_Root_App, 
	--PathLocal = @PPathLocal , PathServer = @PPathServer,
	FTP_Direccion = @PFTP_Direccion, FTP_Carpeta = @PFTP_Carpeta, FTP_Usuario = @PFTP_Usuario, FTP_Contrasena = @PFTP_Contrasena,
	ActualizarClientes = @PActualizarClientes,
	Aud_Actualizacion_Fecha = GETDATE(), Aud_Actualizacion_Equipo = HOST_NAME()
	WHERE  Emp_RUC = @PEmp_RUC ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_ETAPA_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_ETAPA_LISTA]
(
@PEtapa_Codigo CHAR(3)=NULL
)
AS
BEGIN
	SELECT Etapa_Codigo, Etapa_Descripcion FROM dbo.FACTE_ETAPA
	WHERE (@PEtapa_Codigo IS NULL OR Etapa_Codigo = @PEtapa_Codigo);
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_DELETE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_DELETE]
(
@PEmp_RUC char(11),
@PProgra_Correlativo INT
)
AS
BEGIN
	UPDATE dbo.FACTE_PROGRAMACIONES_ENVIO
	SET Progra_Estado='002'
	WHERE Emp_RUC = @PEmp_RUC AND Progra_Correlativo = @PProgra_Correlativo ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_INSERT]
(
@PEmp_RUC char(11),
@PProgra_Correlativo INT OUTPUT,
@PProgra_Descripcion varchar(100),
@PProgra_Habilitado BIT,
@PProgra_Appli_TipoDoc char(2),
@PProgra_Appli_Filtra_Serie BIT,
@PProgra_Appli_Serie char(4)=NULL,
@PProgra_EsResumenBoletas BIT,
@PProgra_EsComunicacionBaja BIT,
@PProgra_FrecuEnvio_Hora_Es_Establecido BIT,
@PProgra_FrecuEnvio_Hora_Es_XCadaHora BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesEmitido BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado BIT,
@PProgra_FrecuEnvio_Hora_Desde TIME,
@PProgra_FrecuEnvio_Hora_Hasta TIME,
@PProgra_FrecuEnvio_Hora_XCadaHora INT,

@PProgra_Fecha_Desde DATE,
@PProgra_Fecha_SinFin BIT,
@PProgra_Fecha_Hasta DATE=NULL
)
AS
BEGIN
	SET @PProgra_Correlativo = (ISNULL((SELECT MAX(Prog.Progra_Correlativo) FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog WITH(NOLOCK) WHERE Prog.Emp_RUC = @PEmp_RUC),0) + 1 );
	INSERT INTO dbo.FACTE_PROGRAMACIONES_ENVIO
	        ( 
			  Emp_RUC , Progra_Correlativo , Progra_Descripcion , Progra_Habilitado ,
	          Progra_Appli_TipoDoc , Progra_Appli_Filtra_Serie , Progra_Appli_Serie ,
	          Progra_EsResumenBoletas , Progra_EsComunicacionBaja ,
	          Progra_FrecuEnvio_Hora_Es_Establecido , Progra_FrecuEnvio_Hora_Es_XCadaHora ,
	          Progra_FrecuEnvio_Hora_Es_DespuesEmitido , Progra_FrecuEnvio_Hora_Es_DespuesConfirmado ,
	          Progra_FrecuEnvio_Hora_Desde , Progra_FrecuEnvio_Hora_Hasta , Progra_FrecuEnvio_Hora_XCadaHora ,
			  Progra_Fecha_Desde,Progra_Fecha_SinFin,Progra_Fecha_Hasta,
	          Progra_Estado ,
	          Aud_Registro_FechaHora , Aud_Registro_Equipo
	        )
	VALUES  ( @PEmp_RUC , @PProgra_Correlativo , @PProgra_Descripcion , @PProgra_Habilitado ,
	          @PProgra_Appli_TipoDoc , @PProgra_Appli_Filtra_Serie , @PProgra_Appli_Serie ,
	          @PProgra_EsResumenBoletas , @PProgra_EsComunicacionBaja ,
	          @PProgra_FrecuEnvio_Hora_Es_Establecido , @PProgra_FrecuEnvio_Hora_Es_XCadaHora ,
	          @PProgra_FrecuEnvio_Hora_Es_DespuesEmitido , @PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado ,
	          @PProgra_FrecuEnvio_Hora_Desde , @PProgra_FrecuEnvio_Hora_Hasta , @PProgra_FrecuEnvio_Hora_XCadaHora ,
			  @PProgra_Fecha_Desde,@PProgra_Fecha_SinFin,@PProgra_Fecha_Hasta,
	          '001' , GETDATE() , HOST_NAME()
	        )
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_LISTA]
(
@PEmp_RUC char(11)
)
AS
BEGIN
	SELECT Prog.Emp_RUC, Prog.Progra_Correlativo, Prog.Progra_Descripcion, Prog.Progra_Habilitado, 
	Prog.Progra_Appli_TipoDoc, Prog.Progra_Appli_Filtra_Serie, ISNULL(Prog.Progra_Appli_Serie,''), Prog.Progra_EsResumenBoletas, Prog.Progra_EsComunicacionBaja, 
	Prog.Progra_FrecuEnvio_Hora_Es_Establecido, Prog.Progra_FrecuEnvio_Hora_Es_XCadaHora, Prog.Progra_FrecuEnvio_Hora_Es_DespuesEmitido, 
	Prog.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Prog.Progra_FrecuEnvio_Hora_Desde, Prog.Progra_FrecuEnvio_Hora_Hasta, 
	Prog.Progra_FrecuEnvio_Hora_XCadaHora,
	Prog.Progra_Fecha_Desde , Prog.Progra_Fecha_SinFin , ISNULL(Prog.Progra_Fecha_Hasta,CAST('0001-01-01' AS DATE)) ,
	Prog.Progra_Estado, GETDATE(), TDoc.TipoDoc_Descripcion
	FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO TDoc ON Prog.Progra_Appli_TipoDoc = TDoc.TipoDoc_Codigo
	WHERE Prog.Emp_RUC = @PEmp_RUC AND Prog.Progra_Estado='001';
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_LISTA_HABILITADOS]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_LISTA_HABILITADOS]
AS
BEGIN
	--Habilitados y listos para enviar
	DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
	DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

	UPDATE FACTE_PROGRAMACIONES_ENVIO SET Progra_Ejecucion_Ultimo = GETDATE() WHERE Progra_Ejecucion_Ultimo IS NULL;

	--SOLO DOCUMENTOS ACTIVOS
	SELECT TOP 1 * FROM (SELECT Doc.Doc_Emisor_RUC, Emp.Etapa_Codigo, Emp.Path_Root_App, 
	Doc.Doc_NombreArchivoXML, Doc.Doc_NombreArchivoWeb AS 'key',
	Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero, DocXML.XML_Correlativo,
	Doc.Doc_FechaEmision, ISNULL(Doc.Doc_Moneda,'') AS 'Doc_Moneda', Doc.Doc_IGV, Doc.Doc_Importe,
	Doc.Doc_Cliente_TipoDocIdent_Codigo, Doc.Doc_Cliente_TipoDocIdent_Numero,
	ISNULL(DocXML.XML_DigestValue,'') AS 'XML_DigestValue', ISNULL(DocXML.XML_SignatureValue,'') AS 'XML_SignatureValue', 
	ISNULL(Soc.SocNeg_Correo_Electronico,'') AS 'SocNeg_Correo_Electronico',
	Emp.Emp_NombreRazonSocial, TDoc.TipoDoc_Descripcion,
	(CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI NO CONSIDERA BOLETAS NI SUS NOTAS DE CREDITO Y DEBITO
		INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
		ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
		WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
		AND ProgX.Progra_Estado='001' 
		AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
		AND ProgX.Emp_RUC=doc.Doc_Emisor_RUC)
		THEN (CASE WHEN (doc.Doc_Tipo ='03' OR doc.Doc_Tipo ='07' OR doc.Doc_Tipo ='08') 
		THEN (CASE WHEN doc.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
		ELSE 0 END) 
		ELSE 0 END) AS Estado
		, ISNULL(DocXML.XML_Archivo,'') AS XML_Archivo
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Doc.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie 
	AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1
	INNER  JOIN dbo.FACTE_SOCIO_NEGOCIO Soc WITH(NOLOCK) ON Soc.TipoDocIdent_Codigo = Doc.Doc_Cliente_TipoDocIdent_Codigo AND Soc.SocNeg_TipoDocIdent_Numero = Doc.Doc_Cliente_TipoDocIdent_Numero
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO TDoc WITH(NOLOCK) ON TDoc.TipoDoc_Codigo = Doc.Doc_Tipo
	INNER JOIN (
	SELECT ProgX.Emp_RUC, ProgX.Progra_Appli_TipoDoc,
	(CASE WHEN ProgX.Progra_Appli_Filtra_Serie = 1 THEN ProgX.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie', 
	ProgX.Progra_EsResumenBoletas, ProgX.Progra_EsComunicacionBaja
	FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO DocTp ON 
    DocTp.TipoDoc_Codigo=ProgX.Progra_Appli_TipoDoc
	WHERE ProgX.Progra_Habilitado = 1
	AND ProgX.Progra_Estado='001'
	AND @AhoraFechaHora >= ProgX.Progra_Fecha_Desde AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	AND DATEADD(HOUR,ProgX.Progra_FrecuEnvio_Hora_XCadaHora, ProgX.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora
	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora
	) AS Progra ON doc.Doc_Emisor_RUC = Progra.Emp_RUC AND Doc.Doc_Tipo = Progra.Progra_Appli_TipoDoc AND Progra.Progra_EsResumenBoletas <> 1 AND Progra.Progra_EsComunicacionBaja <> 1
	WHERE DocXML.EstEnv_Codigo='001'  --OR DocXML.EstEnv_Codigo='004')  --TOMAR LOS AUN NO ENVIADOS
	AND DocXML.TipXML_Codigo<>'003' )
	
	 Tbl1
	WHERE Tbl1.Estado=0 --NO TOMAR DOCUMENTOS DE BAJA 
	ORDER BY Tbl1.Doc_Emisor_RUC,Tbl1.Doc_Tipo,Tbl1.Doc_Serie,Tbl1.Doc_Numero 

	--SELECT * FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
	--WHERE Doc_Serie='BD01'
	--SELECT * FROM dbo.FACTE_TIPO_XML
	--SELECT Prog.Emp_RUC, Prog.Progra_Appli_TipoDoc, 
	----Prog.Progra_Appli_Filtra_Serie,
	--(CASE WHEN Prog.Progra_Appli_Filtra_Serie = 1 THEN Prog.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie', 
	--Prog.Progra_EsResumenBoletas, Prog.Progra_EsComunicacionBaja, 
	----Prog.Progra_FrecuEnvio_Hora_Es_Establecido, --Se produce a las x horas
	----Prog.Progra_FrecuEnvio_Hora_Es_XCadaHora, --Ocurre cada (Hora inicio - Hora fin)
	----Prog.Progra_FrecuEnvio_Hora_Es_DespuesEmitido, --Ocurre cada x horas emitido el documento
	----Prog.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, --Falta
	 
	----Prog.Progra_FrecuEnvio_Hora_Desde, --Hora inicio
	----Prog.Progra_FrecuEnvio_Hora_Hasta, --Hora fin

	----Prog.Progra_FrecuEnvio_Hora_XCadaHora, --hora x

	--Prog.Progra_Fecha_Desde , 
	----Prog.Progra_Fecha_SinFin , 
	--(CASE WHEN Prog.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE Prog.Progra_Fecha_Hasta END)
	----, Prog.Progra_Estado

	--FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog
	--WHERE Prog.Progra_Habilitado = 1
	--AND Prog.Progra_Estado='001'
	
	--AND @AhoraFechaHora >= Prog.Progra_Fecha_Desde AND @AhoraFechaHora <= (CASE WHEN Prog.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE Prog.Progra_Fecha_Hasta END)

	--AND DATEADD(HOUR,Prog.Progra_FrecuEnvio_Hora_XCadaHora, Prog.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora

	--AND Prog.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND Prog.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora
	--ORDER BY Prog.Emp_RUC ASC,Prog.Progra_Appli_TipoDoc ASC,Prog.Progra_Appli_Filtra_Serie DESC,Prog.Progra_Appli_Serie ASC,
	--Prog.Progra_EsResumenBoletas ASC, Prog.Progra_EsComunicacionBaja ASC;

	--SELECT Prog.Emp_RUC, Prog.Progra_Correlativo, Prog.Progra_Descripcion, Prog.Progra_Habilitado, 
	--Prog.Progra_Appli_TipoDoc, Prog.Progra_Appli_Filtra_Serie, ISNULL(Prog.Progra_Appli_Serie,'9999'), Prog.Progra_EsResumenBoletas, Prog.Progra_EsComunicacionBaja, 
	--Prog.Progra_FrecuEnvio_Hora_Es_Establecido, Prog.Progra_FrecuEnvio_Hora_Es_XCadaHora, Prog.Progra_FrecuEnvio_Hora_Es_DespuesEmitido, 
	--Prog.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Prog.Progra_FrecuEnvio_Hora_Desde, Prog.Progra_FrecuEnvio_Hora_Hasta, 
	--Prog.Progra_FrecuEnvio_Hora_XCadaHora,
	--Prog.Progra_Fecha_Desde , Prog.Progra_Fecha_SinFin , ISNULL(Prog.Progra_Fecha_Hasta,GETDATE()) ,
	--Prog.Progra_Estado, @Ahora
	--FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog
	--WHERE Prog.Progra_Habilitado = 1 
	--AND Prog.Progra_Estado='001'
	--ORDER BY Prog.Emp_RUC ASC,Prog.Progra_Appli_TipoDoc ASC,Prog.Progra_Appli_Filtra_Serie DESC,Prog.Progra_Appli_Serie ASC,
	--Prog.Progra_EsResumenBoletas ASC, Prog.Progra_EsComunicacionBaja ASC;
	--ESTE ORDEN ES IMPORTANTE PARA ORDENANAR PRIMERO A LOS FILTROS POR DOCUMENTO Y SERIE
END

	--Habilitados y listos para enviar
--	DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
--	DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;


--SELECT ProgX.Emp_RUC, ProgX.Progra_Appli_TipoDoc,
--	(CASE WHEN ProgX.Progra_Appli_Filtra_Serie = 1 THEN ProgX.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie', 
--	ProgX.Progra_EsResumenBoletas, ProgX.Progra_EsComunicacionBaja
--	FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX
--	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO DocTp ON 
--    DocTp.TipoDoc_Codigo=ProgX.Progra_Appli_TipoDoc
--	WHERE ProgX.Progra_Habilitado = 1
--	AND ProgX.Progra_Estado='001'
--	AND @AhoraFechaHora >= ProgX.Progra_Fecha_Desde AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
--	AND DATEADD(HOUR,ProgX.Progra_FrecuEnvio_Hora_XCadaHora, ProgX.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora
--	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora

GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_UPDATE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_PROGRAMACIONES_ENVIO_UPDATE]
(
@PEmp_RUC char(11),
@PProgra_Correlativo INT,
@PProgra_Descripcion varchar(100),
@PProgra_Habilitado BIT,
@PProgra_Appli_TipoDoc char(2),
@PProgra_Appli_Filtra_Serie BIT,
@PProgra_Appli_Serie char(4)=NULL,
@PProgra_EsResumenBoletas BIT,
@PProgra_EsComunicacionBaja BIT,
@PProgra_FrecuEnvio_Hora_Es_Establecido BIT,
@PProgra_FrecuEnvio_Hora_Es_XCadaHora BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesEmitido BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado BIT,
@PProgra_FrecuEnvio_Hora_Desde TIME,
@PProgra_FrecuEnvio_Hora_Hasta TIME,
@PProgra_FrecuEnvio_Hora_XCadaHora INT,

@PProgra_Fecha_Desde DATE,
@PProgra_Fecha_SinFin BIT,
@PProgra_Fecha_Hasta DATE=NULL
)
AS
BEGIN
	UPDATE dbo.FACTE_PROGRAMACIONES_ENVIO
	SET Progra_Descripcion = @PProgra_Descripcion , Progra_Habilitado = @PProgra_Habilitado,
	Progra_Appli_TipoDoc = @PProgra_Appli_TipoDoc, Progra_Appli_Filtra_Serie = @PProgra_Appli_Filtra_Serie, Progra_Appli_Serie = @PProgra_Appli_Serie,
	Progra_EsResumenBoletas = @PProgra_EsResumenBoletas, Progra_EsComunicacionBaja = @PProgra_EsComunicacionBaja,
	Progra_FrecuEnvio_Hora_Es_Establecido = @PProgra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora = @PProgra_FrecuEnvio_Hora_Es_XCadaHora,
	Progra_FrecuEnvio_Hora_Es_DespuesEmitido = @PProgra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado = @PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado,
	Progra_FrecuEnvio_Hora_Desde = @PProgra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta = @PProgra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora = @PProgra_FrecuEnvio_Hora_XCadaHora,
	Progra_Fecha_Desde = @PProgra_Fecha_Desde, Progra_Fecha_SinFin = @PProgra_Fecha_SinFin, Progra_Fecha_Hasta = @PProgra_Fecha_Hasta,
	Progra_Ejecucion_Ultimo = NULL
	WHERE Emp_RUC = @PEmp_RUC AND Progra_Correlativo = @PProgra_Correlativo ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RECEPTORES_CORREO_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--GUARDO EL CORREO DEL CLIENTE
CREATE PROC [dbo].[UP_FACTE_RECEPTORES_CORREO_INSERT]
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6', --Por defecto es RUC
@PReceptor_TipoDocIdent_Numero VARCHAR(11),
@PReceptor_Email VARCHAR(50)
)
AS
BEGIN
	DECLARE @PReceptor_Registro INT;
	SET @PReceptor_Registro = ISNULL((SELECT MAX(Corr.Receptor_Registro)+ 1 FROM FACTE_RECEPTORES_CORREO Corr WHERE Corr.Receptor_TipoDocIdent_Numero = @PReceptor_TipoDocIdent_Numero),1);
	INSERT INTO dbo.FACTE_RECEPTORES_CORREO
	( Receptor_TipoDocIdent_Codigo, Receptor_TipoDocIdent_Numero , Receptor_Registro , 
	Receptor_Email , Receptor_Validado, Receptor_CopiadoACliente , Aud_Registro_Fecha )
	VALUES (
	@PReceptor_TipoDocIdent_Codigo, @PReceptor_TipoDocIdent_Numero , @PReceptor_Registro , 
	@PReceptor_Email , 0,  0 , GETDATE() );
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RECEPTORES_CORREO_PENDIENTES_REGISTRO_CLIENTE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RECEPTORES_CORREO_PENDIENTES_REGISTRO_CLIENTE]
AS
BEGIN
	SELECT Corr.Receptor_TipoDocIdent_Numero, Corr.Receptor_Email FROM FACTE_RECEPTORES_CORREO Corr
	WHERE Corr.Receptor_CopiadoACliente = 0  
	AND EXISTS(SELECT Emp.ActualizarClientes FROM dbo.FACTE_EMPRESA Emp WHERE Emp.ActualizarClientes = 1);
	--AL MENOS UNO DEB ESTAR ACTIVADO PARA COPIAR LOS DATOS AL CLIENTE
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RECEPTORES_CORREO_PENDIENTES_VALIDACION]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RECEPTORES_CORREO_PENDIENTES_VALIDACION]
AS
BEGIN
	SELECT Corr.Receptor_TipoDocIdent_Codigo, Corr.Receptor_TipoDocIdent_Numero, Corr.Receptor_Email 
	FROM FACTE_RECEPTORES_CORREO Corr
	WHERE Corr.Receptor_Validado = 0  
	AND EXISTS(SELECT Emp.ActualizarClientes FROM dbo.FACTE_EMPRESA Emp WHERE Emp.ActualizarClientes = 1);
	--AL MENOS UNO DEB ESTAR ACTIVADO PARA COPIAR LOS DATOS AL CLIENTE
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RECEPTORES_CORREO_PENDIENTES_VALIDACION_CAMBIAR_ESTADO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RECEPTORES_CORREO_PENDIENTES_VALIDACION_CAMBIAR_ESTADO]
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6', --Por defecto es RUC
@PReceptor_TipoDocIdent_Numero VARCHAR(11),
@PReceptor_Registro INT,
@PReceptor_Email VARCHAR(50)
)
AS
BEGIN
	UPDATE FACTE_RECEPTORES_CORREO SET Receptor_Validado = 1
	WHERE Receptor_TipoDocIdent_Codigo = @PReceptor_TipoDocIdent_Codigo  
	AND Receptor_TipoDocIdent_Numero = @PReceptor_TipoDocIdent_Numero AND Receptor_Registro = @PReceptor_Registro ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RECEPTORES_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RECEPTORES_LISTA]
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6',
@PReceptor_TipoDocIdent_Numero VARCHAR(11)=NULL
)
AS
BEGIN
	SELECT Receptor_TipoDocIdent_Codigo, Receptor_TipoDocIdent_Numero, Receptor_Registro, 
	Receptor_Email, Receptor_Validado, Receptor_CopiadoACliente, Aud_Registro_Fecha
	FROM dbo.FACTE_RECEPTORES_CORREO
	WHERE (@PReceptor_TipoDocIdent_Numero IS NULL OR Receptor_TipoDocIdent_Numero = @PReceptor_TipoDocIdent_Numero)
	ORDER BY Receptor_TipoDocIdent_Numero ASC,Receptor_Registro DESC;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_INSERT]
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_FechaGeneracion DATE,
@PRsm_Correlativo INT,
@PRsm_Doc_Item INT,

@PRsm_Doc_Tipo CHAR(2),
@PRsm_Doc_Serie CHAR(4),
@PRsm_Doc_Numero CHAR(8)
)
AS
BEGIN
	INSERT INTO dbo.FACTE_RESUMEN_BAJA_DOCUMENTO
	        ( Rsm_Emisor_RUC , Rsm_FechaGeneracion , Rsm_Correlativo , Rsm_Doc_Item ,
	          Rsm_Doc_Tipo , Rsm_Doc_Serie , Rsm_Doc_Numero )
	VALUES  ( @PRsm_Emisor_RUC , @PRsm_FechaGeneracion, @PRsm_Correlativo , @PRsm_Doc_Item , -- Rsm_Doc_Item - int
	          @PRsm_Doc_Tipo , @PRsm_Doc_Serie , @PRsm_Doc_Numero );
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_LISTA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_LISTA]
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_SUNATTicket_ID VARCHAR(50)
)
AS
BEGIN
	SELECT doc.Rsm_Emisor_RUC ,Doc.Rsm_Doc_Tipo, doc.Rsm_Doc_Serie, doc.Rsm_Doc_Numero FROM dbo.FACTE_RESUMEN_BAJA_DOCUMENTO Doc
	INNER JOIN dbo.FACTE_RESUMEN_BAJA Cab WITH(NOLOCK) ON Cab.Rsm_Emisor_RUC = Doc.Rsm_Emisor_RUC AND Cab.Rsm_FechaGeneracion = Doc.Rsm_FechaGeneracion AND Cab.Rsm_Correlativo = Doc.Rsm_Correlativo
	WHERE Cab.Rsm_Emisor_RUC = @PRsm_Emisor_RUC AND Cab.Rsm_SUNATTicket_ID = @PRsm_SUNATTicket_ID ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_CAMBIARESTADO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_CAMBIARESTADO]
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_SUNATTicket_ID VARCHAR(50),
@PRsm_Estado CHAR(4)
)
AS
BEGIN
	UPDATE FACTE_RESUMEN_BAJA SET Rsm_Estado = @PRsm_Estado
	WHERE Rsm_Emisor_RUC = @PRsm_Emisor_RUC AND Rsm_SUNATTicket_ID = @PRsm_SUNATTicket_ID ;
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_PENDIENTES]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_PENDIENTES]
AS
BEGIN
	SELECT Res.Rsm_SUNATTicket_ID, Res.Rsm_Emisor_RUC FROM dbo.FACTE_RESUMEN_BAJA Res 
	WITH(NOLOCK) WHERE Res.Rsm_Estado='4106'--4106-ENVIADO 4107-ACEPTADOS 4108-RECHAZADO
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_BAJA_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RESUMEN_BAJA_INSERT]
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_FechaGeneracion DATE,
@PRsm_Correlativo INT,

@PRsm_SUNATTicket_ID VARCHAR(50),
@PRsm_NombreArchivoXML VARCHAR(50),
@PRsm_NombreArchivoWeb VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET @PRsm_NombreArchivoWeb = newid();
	WHILE EXISTS(SELECT Acep.Rsm_NombreArchivoWeb FROM dbo.FACTE_RESUMEN_BAJA Acep WHERE Acep.Rsm_NombreArchivoWeb = @PRsm_NombreArchivoWeb)
	BEGIN
		SET @PRsm_NombreArchivoWeb = newid();
	END

	INSERT INTO dbo.FACTE_RESUMEN_BAJA
	( Rsm_Emisor_RUC , Rsm_FechaGeneracion , Rsm_Correlativo ,
	  Rsm_SUNATTicket_ID , Rsm_NombreArchivoXML , Rsm_NombreArchivoWeb , Rsm_Estado ,
	  Doc_Aud_Registro_FechaHora , Doc_Aud_Registro_Equipo )
	VALUES
	( @PRsm_Emisor_RUC , @PRsm_FechaGeneracion , @PRsm_Correlativo ,
	  @PRsm_SUNATTicket_ID , @PRsm_NombreArchivoXML , @PRsm_NombreArchivoWeb , '4106' ,
	  GETDATE() ,HOST_NAME());
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_BAJA_OBTENER_CORRELATIVO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_RESUMEN_BAJA_OBTENER_CORRELATIVO]
(
@PRsm_FechaGeneracion DATE OUTPUT,
@PRsm_Correlativo INT OUTPUT
)
AS
BEGIN
	SET @PRsm_FechaGeneracion = GETDATE();
	SET @PRsm_Correlativo = (SELECT MAX(Rsm.Rsm_Correlativo) FROM dbo.FACTE_RESUMEN_BAJA Rsm WHERE Rsm.Rsm_FechaGeneracion = @PRsm_FechaGeneracion);
	SET @PRsm_Correlativo = (ISNULL(@PRsm_Correlativo,0) + 1);
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_DIARIO_CAMBIAR_ESTADO_ENVIO]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_RESUMEN_DIARIO_CAMBIAR_ESTADO_ENVIO]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_FechaGeneracion DATE,
@PDoc_Resumen_Correlativo INT,
@PDoc_NombreArchivoXML VARCHAR(50)=NULL,
@PDoc_SUNATTicket_ID VARCHAR(50)=NULL,
@PSUNAT_Respuesta_Codigo CHAR(4)=NULL,
@PEstEnv_Codigo CHAR(3)
)
AS
BEGIN
IF (SELECT COUNT(*) FROM dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO 
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_FechaGeneracion = @PDoc_FechaGeneracion 
	AND Doc_Resumen_Correlativo = @PDoc_Resumen_Correlativo AND Doc_CondicionItem='3')>0
	BEGIN
	UPDATE dbo.FACTE_RESUMEN_DIARIO 
	SET EstEnv_Codigo = @PEstEnv_Codigo, 
	Doc_NombreArchivoXML = @PDoc_NombreArchivoXML, Doc_SUNATTicket_ID = @PDoc_SUNATTicket_ID, 
	SUNAT_Respuesta_Codigo = @PSUNAT_Respuesta_Codigo
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_FechaGeneracion = @PDoc_FechaGeneracion 
	AND Doc_Resumen_Correlativo = @PDoc_Resumen_Correlativo ;

	UPDATE dex
	SET dex.EstEnv_Codigo=@PEstEnv_Codigo
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de 
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
    rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
	AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
	WHERE de.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND dex.TipXML_Codigo='003' 
	--AND dex.EstEnv_Codigo='003'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND rescab.Doc_FechaGeneracion=@PDoc_FechaGeneracion AND rescab.Doc_Resumen_Correlativo=@PDoc_Resumen_Correlativo;

	IF @PEstEnv_Codigo='006'
	BEGIN
	UPDATE dex
	SET dex.XML_Enviado_FechaHora=GETDATE()
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
    rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
	AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
	WHERE de.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND dex.TipXML_Codigo='003' 
	--AND dex.EstEnv_Codigo='003'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND rescab.Doc_FechaGeneracion=@PDoc_FechaGeneracion AND rescab.Doc_Resumen_Correlativo=@PDoc_Resumen_Correlativo;
	END

	IF @PEstEnv_Codigo='007'
	BEGIN
	UPDATE dex
	SET dex.XML_Aceptado_CDR_FechaHora=GETDATE()
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
    rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
	AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
	WHERE de.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND dex.TipXML_Codigo='003' 
	--AND dex.EstEnv_Codigo='003'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND rescab.Doc_FechaGeneracion=@PDoc_FechaGeneracion AND rescab.Doc_Resumen_Correlativo=@PDoc_Resumen_Correlativo;
	END
	END
	ELSE
	BEGIN
	UPDATE dbo.FACTE_RESUMEN_DIARIO 
	SET EstEnv_Codigo = @PEstEnv_Codigo, 
	Doc_NombreArchivoXML = @PDoc_NombreArchivoXML, Doc_SUNATTicket_ID = @PDoc_SUNATTicket_ID, 
	SUNAT_Respuesta_Codigo = @PSUNAT_Respuesta_Codigo
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_FechaGeneracion = @PDoc_FechaGeneracion 
	AND Doc_Resumen_Correlativo = @PDoc_Resumen_Correlativo ;

	UPDATE dex
	SET dex.EstEnv_Codigo=@PEstEnv_Codigo
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res  WITH(NOLOCK)ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
    rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
	AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
	WHERE de.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND dex.TipXML_Codigo='001' 
	--AND dex.EstEnv_Codigo='003'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND rescab.Doc_FechaGeneracion=@PDoc_FechaGeneracion AND rescab.Doc_Resumen_Correlativo=@PDoc_Resumen_Correlativo;

	IF @PEstEnv_Codigo='006'
	BEGIN
	UPDATE dex
	SET dex.XML_Enviado_FechaHora=GETDATE()
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
    rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
	AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
	WHERE de.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND dex.TipXML_Codigo='001' 
	--AND dex.EstEnv_Codigo='003'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND rescab.Doc_FechaGeneracion=@PDoc_FechaGeneracion AND rescab.Doc_Resumen_Correlativo=@PDoc_Resumen_Correlativo;
	END

	IF @PEstEnv_Codigo='007'
	BEGIN
	UPDATE dex
	SET dex.XML_Aceptado_CDR_FechaHora=GETDATE()
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
    rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
	AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
	WHERE de.Doc_Emisor_RUC=@PDoc_Emisor_RUC AND dex.TipXML_Codigo='001' 
	--AND dex.EstEnv_Codigo='003'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND rescab.Doc_FechaGeneracion=@PDoc_FechaGeneracion AND rescab.Doc_Resumen_Correlativo=@PDoc_Resumen_Correlativo;
	END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_DIARIO_ESPERA_RESPUESTA_TICKET]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_RESUMEN_DIARIO_ESPERA_RESPUESTA_TICKET]
AS
BEGIN
	--ENVIADOS - PENDIENTES DE RESPUESTA
	SELECT Diar.Doc_Emisor_RUC, Diar.Doc_FechaGeneracion, Diar.Doc_Resumen_Correlativo,
	Diar.Doc_Fecha_Emision, Diar.Doc_NombreArchivoXML, Diar.Doc_SUNATTicket_ID, Emp.Path_Root_App + '\' + Doc_Emisor_RUC, Emp.Etapa_Codigo
	FROM FACTE_RESUMEN_DIARIO Diar WITH(NOLOCK)
	INNER JOIN dbo.FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Diar.Doc_Emisor_RUC
	WHERE Diar.EstEnv_Codigo='006' 
	AND (Diar.Doc_SUNATTicket_ID IS NOT NULL AND LTRIM(RTRIM(Diar.Doc_SUNATTicket_ID))<>'')
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_DIARIO_GENERAR_TASK]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DELETE  FROM [dbo].[FACTE_RESUMEN_DIARIO_DOCUMENTO]
--DELETE   FROM [dbo].[FACTE_RESUMEN_DIARIO]

CREATE PROC [dbo].[UP_FACTE_RESUMEN_DIARIO_GENERAR_TASK]
AS
BEGIN
SET NOCOUNT ON
DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
	DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

DECLARE @Count INT=(SELECT COUNT(*) FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK)
INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
	AND ProgX.Progra_Estado='001' AND ProgX.Progra_Appli_TipoDoc='03'
	AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora);
--PRINT CAST(@Count AS VARCHAR(2))

DECLARE @RUC CHAR(11)=''
WHILE @Count>0
BEGIN
SELECT TOP (@Count) @RUC=ProgX.Emp_RUC FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK)
INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
	AND ProgX.Progra_Estado='001' AND ProgX.Progra_Appli_TipoDoc='03'
	AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora
	ORDER BY ProgX.Emp_RUC DESC
PRINT @RUC
PRINT '---------------'
--DECLARE @RUC CHAR(11)='20542134926'
DECLARE @FechaResumen DATE=GETDATE()
--DECLARE @FechaHastaResumen DATE=DATEADD(DAY,-1,@FechaResumen)
DECLARE @FechaHastaResumen DATE=@FechaResumen
DECLARE @FechaDesdeResumen DATE=DATEADD(DAY,-7,@FechaResumen)
DECLARE @CantidadIncluir INT =500
print  CONVERT(VARCHAR, @FechaHastaResumen, 120)

WHILE @FechaDesdeResumen<=@FechaHastaResumen
BEGIN
--SELECT*
-- FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
--	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
--	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
--	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
--	LEFT JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
--	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
--	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
--	WHERE de.Doc_Emisor_RUC='20447370272' AND dex.TipXML_Codigo='001' AND dex.EstEnv_Codigo='001'
--	AND de.Doc_Tipo='03' AND de.Doc_Moneda='PEN'
--	--AND de.Doc_FechaEmision=@FechaDesdeResumen 
--	AND res.Doc_Emisor_RUC IS NULL
PRINT   CONVERT(VARCHAR, @FechaDesdeResumen, 120)
	--RESUMEN DE BOLETAS
	WHILE ISNULL((SELECT COUNT(*) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	LEFT JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	WHERE de.Doc_Emisor_RUC=@RUC AND dex.TipXML_Codigo='001' AND dex.EstEnv_Codigo='001'
	AND de.Doc_Tipo='03' AND de.Doc_Moneda='PEN'
	AND de.Doc_FechaEmision=@FechaDesdeResumen AND res.Doc_Emisor_RUC IS NULL
	),0)>0
	BEGIN
		PRINT 'INSERTAR RESUMEN BOLETAS'
		DECLARE @Correlativo_Actual INT
			SET @Correlativo_Actual = ISNULL((SELECT MAX(OrgDoc.Doc_Resumen_Correlativo) 
			FROM FACTE_RESUMEN_DIARIO OrgDoc 
			WHERE OrgDoc.Doc_Emisor_RUC = @RUC AND OrgDoc.Doc_FechaGeneracion = @FechaResumen),0) + 1;
			INSERT INTO dbo.FACTE_RESUMEN_DIARIO
					( Doc_Emisor_RUC ,
					  Doc_FechaGeneracion ,
					  Doc_Resumen_Correlativo ,
					  Doc_Fecha_Emision ,
					  Doc_NombreArchivoXML ,
					  Doc_SUNATTicket_ID ,
					  SUNAT_Respuesta_Codigo ,
					  EstEnv_Codigo ,
					  Doc_Estado
					)
			VALUES  ( @RUC , -- Doc_Emisor_RUC - char(11)
					  @FechaResumen , -- Doc_FechaGeneracion - date
					  @Correlativo_Actual , -- Doc_Resumen_Correlativo - int
					  @FechaDesdeResumen , -- Doc_Fecha_Emision - date
					  NULL , -- Doc_NombreArchivoXML - varchar(50)
					  NULL , -- Doc_SUNATTicket_ID - varchar(50)
					  NULL , -- SUNAT_Respuesta_Codigo - char(4)
					  '001' , -- EstEnv_Codigo - char(3)
					  '0001'  -- Doc_Estado - char(4)
					);

			INSERT INTO dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO
			        ( Doc_Emisor_RUC ,
			          Doc_FechaGeneracion ,
			          Doc_Resumen_Correlativo ,
			          Doc_Item ,
			          Doc_Tipo ,
			          Doc_Serie ,
			          Doc_Numero ,
			          Doc_Estado ,
			          Doc_Aud_Registro_FechaHora ,
			          Doc_Aud_Registro_Equipo ,
			          Doc_Aud_Aceptacion_FechaHoraCheck ,
			          Doc_Aud_Aceptacion_Equipo,Doc_CondicionItem
			        )
			SELECT TOP (@CantidadIncluir) 
			 @RUC , @FechaResumen ,@Correlativo_Actual ,
			 ROW_NUMBER() OVER (ORDER BY de.Doc_Tipo, de.Doc_Serie,de.Doc_Numero),
				de.Doc_Tipo, de.Doc_Serie, de.Doc_Numero,
				'0001',GETDATE(), HOST_NAME(),NULL,NULL,'1'
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		LEFT JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero 
		--AND (Res.Doc_CondicionItem IS NULL OR Res.Doc_CondicionItem='1')
		WHERE de.Doc_Emisor_RUC=@RUC AND dex.TipXML_Codigo='001' AND dex.EstEnv_Codigo='001'
		AND de.Doc_Tipo='03' AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND res.Doc_Emisor_RUC IS NULL
		ORDER BY de.Doc_Tipo ASC, de.Doc_Serie ASC , de.Doc_Numero ASC

		--ACTUALIZAR ESTADO DE XML DEL DOCUMENTO ELECTRONICO
		UPDATE dex
		SET dex.EstEnv_Codigo='005'
				FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
		rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
		AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
		WHERE Res.Doc_Emisor_RUC=@RUC
		AND Res.Doc_FechaGeneracion=@FechaResumen
		AND Res.Doc_Resumen_Correlativo=@Correlativo_Actual
		AND dex.TipXML_Codigo='001' 
		--AND dex.EstEnv_Codigo='003'
		AND de.Doc_Tipo='03' AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND rescab.EstEnv_Codigo='001'
		--ACTUALIZAR FECHA Y CORRELATIVO DEL DOCUMENTO DE BAJA
		UPDATE de
		SET de.Doc_Resumen_FechaGeneracion=@FechaResumen, de.Doc_Resumen_Correlativo=@Correlativo_Actual
				FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
		rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
		AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
		WHERE Res.Doc_Emisor_RUC=@RUC
		AND Res.Doc_FechaGeneracion=@FechaResumen
		AND Res.Doc_Resumen_Correlativo=@Correlativo_Actual
		AND dex.TipXML_Codigo='001' 
		--AND dex.EstEnv_Codigo='003'
		AND de.Doc_Tipo='03' AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND rescab.EstEnv_Codigo='001'
		END;

		--RESUMEN NOTA DE CREDITO DEBITO
		WHILE ISNULL((SELECT COUNT(*) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero		
		LEFT JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		WHERE de.Doc_Emisor_RUC=@RUC AND dex.TipXML_Codigo='001' AND dex.EstEnv_Codigo='001'
		AND de.Doc_Tipo IN ('07','08') 
		AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND res.Doc_Emisor_RUC IS NULL
		AND (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI CONSIDERA  NOTAS DE CREDITO Y DEBITO DE LAS BOLETAS
				INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
				ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
				WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
				AND ProgX.Progra_Estado='001' 
				AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
				AND ProgX.Emp_RUC=de.Doc_Emisor_RUC)
				THEN (CASE WHEN (de.Doc_Tipo ='03' OR de.Doc_Tipo ='07' OR de.Doc_Tipo ='08') 
				THEN (CASE WHEN de.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
				ELSE 0 END) 
				ELSE 0 END)=1 ),0)>0
	BEGIN
	PRINT 'INSERTAR NOTAS '
		DECLARE @Correlativo_2 INT
			SET @Correlativo_2 = ISNULL((SELECT MAX(OrgDoc.Doc_Resumen_Correlativo) 
			FROM FACTE_RESUMEN_DIARIO OrgDoc  WITH(NOLOCK)
			WHERE OrgDoc.Doc_Emisor_RUC = @RUC AND OrgDoc.Doc_FechaGeneracion = @FechaResumen),0) + 1;
			INSERT INTO dbo.FACTE_RESUMEN_DIARIO 
					( Doc_Emisor_RUC ,
					  Doc_FechaGeneracion ,
					  Doc_Resumen_Correlativo ,
					  Doc_Fecha_Emision ,
					  Doc_NombreArchivoXML ,
					  Doc_SUNATTicket_ID ,
					  SUNAT_Respuesta_Codigo ,
					  EstEnv_Codigo ,
					  Doc_Estado
					)
			VALUES  ( @RUC , -- Doc_Emisor_RUC - char(11)
					  @FechaResumen , -- Doc_FechaGeneracion - date
					  @Correlativo_2 , -- Doc_Resumen_Correlativo - int
					  @FechaDesdeResumen , -- Doc_Fecha_Emision - date
					  NULL , -- Doc_NombreArchivoXML - varchar(50)
					  NULL , -- Doc_SUNATTicket_ID - varchar(50)
					  NULL , -- SUNAT_Respuesta_Codigo - char(4)
					  '001' , -- EstEnv_Codigo - char(3)
					  '0001'  -- Doc_Estado - char(4)
					);

			INSERT INTO dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO
			        ( Doc_Emisor_RUC ,
			          Doc_FechaGeneracion ,
			          Doc_Resumen_Correlativo ,
			          Doc_Item ,
			          Doc_Tipo ,
			          Doc_Serie ,
			          Doc_Numero ,
			          Doc_Estado ,
			          Doc_Aud_Registro_FechaHora ,
			          Doc_Aud_Registro_Equipo ,
			          Doc_Aud_Aceptacion_FechaHoraCheck ,
			          Doc_Aud_Aceptacion_Equipo,Doc_CondicionItem
			        )
			SELECT TOP (@CantidadIncluir) 
			 @RUC , @FechaResumen ,@Correlativo_2 ,
			 ROW_NUMBER() OVER (ORDER BY de.Doc_Tipo, de.Doc_Serie,de.Doc_Numero),
				de.Doc_Tipo, de.Doc_Serie, de.Doc_Numero,
				'0001',GETDATE(), HOST_NAME(),NULL,NULL,'1'
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
			INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
			dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
			AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
			--INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CC ON  --VERIFICA SI EL XML DE LA BOLETA YA FUE ENVIADA Y ACEPTADA, SI ES ASI LA NOTA DE CREDITO ES INCLUIDA
			--	CC.Doc_Emisor_RUC = dex.Doc_Emisor_RUC AND CC.Doc_Tipo = dex.Doc_Tipo
			--	AND CC.Doc_Serie = dex.Doc_Serie AND CC.Doc_Numero = dex.Doc_Numero 
			--	AND  dex.TipXML_Codigo='001' AND CC.Doc_Tipo='03' --RELACIONAR XML DE NOTA DE CREDITO O DEBITO, CON XML DE BOLETA DE VENTA
			--	AND (CC.EstEnv_Codigo='003' OR CC.EstEnv_Codigo='007') -- VERIFICAR SI EL XML DE BOLETA ESTA ACEPTADO 		
			LEFT JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
			Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
			Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
			WHERE de.Doc_Emisor_RUC=@RUC AND dex.TipXML_Codigo='001' AND dex.EstEnv_Codigo='001'
			AND de.Doc_Tipo IN ('07','08') 
			AND de.Doc_Moneda='PEN'
			AND de.Doc_FechaEmision=@FechaDesdeResumen AND res.Doc_Emisor_RUC IS NULL
			AND (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI CONSIDERA  NOTAS DE CREDITO Y DEBITO DE LAS BOLETAS
					INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc ON 
					ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
					WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
					AND ProgX.Progra_Estado='001' 
					AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
					AND ProgX.Emp_RUC=de.Doc_Emisor_RUC)
					THEN (CASE WHEN (de.Doc_Tipo ='03' OR de.Doc_Tipo ='07' OR de.Doc_Tipo ='08') 
					THEN (CASE WHEN de.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
					ELSE 0 END) 
					ELSE 0 END)=1
			ORDER BY de.Doc_Tipo ASC, de.Doc_Serie ASC , de.Doc_Numero ASC

		--ACTUALIZAR ESTADO DE XML DEL DOCUMENTO ELECTRONICO
		UPDATE dex
		SET dex.EstEnv_Codigo='005'
				FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
		rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
		AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
		WHERE Res.Doc_Emisor_RUC=@RUC
		AND Res.Doc_FechaGeneracion=@FechaResumen
		AND Res.Doc_Resumen_Correlativo=@Correlativo_2
		AND dex.TipXML_Codigo='001' 
		--AND dex.EstEnv_Codigo='003'
		AND de.Doc_Tipo IN ('07','08') AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND rescab.EstEnv_Codigo='001'
		--ACTUALIZAR FECHA Y CORRELATIVO DEL DOCUMENTO ELECTRONICO
		UPDATE de
		SET de.Doc_Resumen_FechaGeneracion=@FechaResumen, de.Doc_Resumen_Correlativo=@Correlativo_2
				FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab ON 
		rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
		AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
		WHERE Res.Doc_Emisor_RUC=@RUC
		AND Res.Doc_FechaGeneracion=@FechaResumen
		AND Res.Doc_Resumen_Correlativo=@Correlativo_2
		AND dex.TipXML_Codigo='001' 
		--AND dex.EstEnv_Codigo='003'
		AND de.Doc_Tipo IN ('07','08') AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND rescab.EstEnv_Codigo='001'
	END

	--RESUMEN DE BAJAS
	WHILE ISNULL((SELECT COUNT(*) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	 INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CC WITH(NOLOCK) ON  --VERIFICAR SI EL XML A SIDO INFORMADO Y ACEPTADO CORRECTAMENTE, SI ES ASI SE GENERA EL XML DE BAJA
		CC.Doc_Emisor_RUC = dex.Doc_Emisor_RUC AND CC.Doc_Tipo = dex.Doc_Tipo
		AND CC.Doc_Serie = dex.Doc_Serie AND CC.Doc_Numero = dex.Doc_Numero 
		AND  dex.TipXML_Codigo='003' AND CC.TipXML_Codigo<>'003' --RELACIONAR XML DE ENVIO, CON XML DE BAJA
		AND (CC.EstEnv_Codigo='003' OR CC.EstEnv_Codigo='007') -- VERIFICAR SI EL XML DE ENVIO ESTA ACEPTADO 
		AND CAST(ISNULL((CC.XML_Aceptado_CDR_FechaHora),de.Doc_FechaEmision) AS DATE)>=DATEADD(DAY,-3,CAST(@AhoraFechaHora AS DATE)) --VERIFICAR EL DIA QUE ACEPTO EL CDR, Y SOLO DEBE SER 3 DIAS ANTES DE LA FECHA ACTUAL.			
	LEFT JOIN (SELECT * FROM dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO resfac 
WHERE resfac.Doc_Emisor_RUC='' AND resfac.Doc_CondicionItem='3') Res ON 
	Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
	Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
	WHERE de.Doc_Emisor_RUC=@RUC AND dex.TipXML_Codigo='003' AND dex.EstEnv_Codigo='001'
	--AND de.Doc_Tipo='03' 
	AND de.Doc_Moneda='PEN'
	AND de.Doc_FechaEmision=@FechaDesdeResumen AND res.Doc_Emisor_RUC IS NULL
	AND (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI NO CONSIDERA BOLETAS NI SUS NOTAS DE CREDITO Y DEBITO
			INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
			ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
			WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
			AND ProgX.Progra_Estado='001' 
			AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
			AND ProgX.Emp_RUC=de.Doc_Emisor_RUC)
			THEN (CASE WHEN (de.Doc_Tipo ='03' OR de.Doc_Tipo ='07' OR de.Doc_Tipo ='08') 
			THEN (CASE WHEN de.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
			ELSE 0 END) 
			ELSE 0 END)=1
	),0)>0
	BEGIN
		PRINT 'INSERTAR BAJAS '
		DECLARE @Correlativo_3 INT
			SET @Correlativo_3 = ISNULL((SELECT MAX(OrgDoc.Doc_Resumen_Correlativo) 
			FROM FACTE_RESUMEN_DIARIO OrgDoc WITH(NOLOCK) 
			WHERE OrgDoc.Doc_Emisor_RUC = @RUC AND OrgDoc.Doc_FechaGeneracion = @FechaResumen),0) + 1;
			INSERT INTO dbo.FACTE_RESUMEN_DIARIO
					( Doc_Emisor_RUC ,
					  Doc_FechaGeneracion ,
					  Doc_Resumen_Correlativo ,
					  Doc_Fecha_Emision ,
					  Doc_NombreArchivoXML ,
					  Doc_SUNATTicket_ID ,
					  SUNAT_Respuesta_Codigo ,
					  EstEnv_Codigo ,
					  Doc_Estado
					)
			VALUES  ( @RUC , -- Doc_Emisor_RUC - char(11)
					  @FechaResumen , -- Doc_FechaGeneracion - date
					  @Correlativo_3 , -- Doc_Resumen_Correlativo - int
					  @FechaDesdeResumen , -- Doc_Fecha_Emision - date
					  NULL , -- Doc_NombreArchivoXML - varchar(50)
					  NULL , -- Doc_SUNATTicket_ID - varchar(50)
					  NULL , -- SUNAT_Respuesta_Codigo - char(4)
					  '001' , -- EstEnv_Codigo - char(3)
					  '0001'  -- Doc_Estado - char(4)
					);

			INSERT INTO dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO
			        ( Doc_Emisor_RUC ,
			          Doc_FechaGeneracion ,
			          Doc_Resumen_Correlativo ,
			          Doc_Item ,
			          Doc_Tipo ,
			          Doc_Serie ,
			          Doc_Numero ,
			          Doc_Estado ,
			          Doc_Aud_Registro_FechaHora ,
			          Doc_Aud_Registro_Equipo ,
			          Doc_Aud_Aceptacion_FechaHoraCheck ,
			          Doc_Aud_Aceptacion_Equipo,Doc_CondicionItem
			        )
			SELECT TOP (@CantidadIncluir) 
			 @RUC , @FechaResumen ,@Correlativo_3 ,
			 ROW_NUMBER() OVER (ORDER BY de.Doc_Tipo, de.Doc_Serie,de.Doc_Numero),
				de.Doc_Tipo, de.Doc_Serie, de.Doc_Numero,
				'0001',GETDATE(), HOST_NAME(),NULL,NULL,'3'
			FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
	dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo
	AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
	 INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE CC WITH(NOLOCK) ON  --VERIFICAR SI EL XML A SIDO INFORMADO Y ACEPTADO CORRECTAMENTE, SI ES ASI SE GENERA EL XML DE BAJA
		CC.Doc_Emisor_RUC = dex.Doc_Emisor_RUC AND CC.Doc_Tipo = dex.Doc_Tipo
		AND CC.Doc_Serie = dex.Doc_Serie AND CC.Doc_Numero = dex.Doc_Numero 
		AND  dex.TipXML_Codigo='003' AND CC.TipXML_Codigo<>'003' --RELACIONAR XML DE ENVIO, CON XML DE BAJA
		AND (CC.EstEnv_Codigo='003' OR CC.EstEnv_Codigo='007') -- VERIFICAR SI EL XML DE ENVIO ESTA ACEPTADO 
		AND CAST(ISNULL((CC.XML_Aceptado_CDR_FechaHora),de.Doc_FechaEmision) AS DATE)>=DATEADD(DAY,-3,CAST(@AhoraFechaHora AS DATE)) --VERIFICAR EL DIA QUE ACEPTO EL CDR, Y SOLO DEBE SER 3 DIAS ANTES DE LA FECHA ACTUAL.			
		LEFT JOIN (SELECT * FROM dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO resfac WITH(NOLOCK) 
		WHERE resfac.Doc_Emisor_RUC='' AND resfac.Doc_CondicionItem='3') Res ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		WHERE de.Doc_Emisor_RUC=@RUC AND dex.TipXML_Codigo='003' AND dex.EstEnv_Codigo='001'
		--AND de.Doc_Tipo='03' 
		AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen AND res.Doc_Emisor_RUC IS NULL
		AND (CASE WHEN EXISTS (SELECT * FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX WITH(NOLOCK) --VERIFICA SI ESTA HABILITADO EL RESUMEN DIARIO, SI ES ASI NO CONSIDERA BOLETAS NI SUS NOTAS DE CREDITO Y DEBITO
				INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipdoc WITH(NOLOCK) ON 
				ProgX.Progra_Appli_TipoDoc=Tipdoc.TipoDoc_Codigo
				WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
				AND ProgX.Progra_Estado='001' 
				AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
				AND ProgX.Emp_RUC=de.Doc_Emisor_RUC)
				THEN (CASE WHEN (de.Doc_Tipo ='03' OR de.Doc_Tipo ='07' OR de.Doc_Tipo ='08') 
				THEN (CASE WHEN de.Doc_Serie LIKE 'B%' THEN 1 ELSE 0 END)
				ELSE 0 END) 
				ELSE 0 END)=1
		ORDER BY de.Doc_Tipo ASC, de.Doc_Serie ASC , de.Doc_Numero ASC

		--ACTUALIZAR ESTADO DE XML DEL DOCUMENTO ELECTRONICO
		UPDATE dex
		SET dex.EstEnv_Codigo='005'
				FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de WITH(NOLOCK)
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
		rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
		AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
		WHERE Res.Doc_Emisor_RUC=@RUC
		AND Res.Doc_FechaGeneracion=@FechaResumen
		AND Res.Doc_Resumen_Correlativo=@Correlativo_3
		AND dex.TipXML_Codigo='003' 
		--AND dex.EstEnv_Codigo='003'
		--AND de.Doc_Tipo='03' 
		AND de.Doc_Moneda='PEN'
		AND de.Doc_FechaEmision=@FechaDesdeResumen 
		AND rescab.EstEnv_Codigo='001'
		--ACTUALIZAR FECHA Y CORRELATIVO DEL DOCUMENTO DE BAJA
		UPDATE de
		SET de.Doc_Baja_GrpTipo_Codigo = tpdc.GrpTipo_Codigo, de.Doc_Baja_FechaGeneracion = @FechaResumen, de.Doc_Baja_Correlativo = @Correlativo_3
		FROM dbo.FACTE_DOCUMENTO_ELECTRONICO de
		INNER JOIN dbo.FACTE_DOCUMENTO_TIPO tpdc WITH(NOLOCK) ON 
        tpdc.TipoDoc_Codigo = de.Doc_Tipo
		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE dex WITH(NOLOCK) ON 
		dex.Doc_Emisor_RUC = de.Doc_Emisor_RUC AND dex.Doc_Tipo = de.Doc_Tipo 
		AND dex.Doc_Serie = de.Doc_Serie AND dex.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Res WITH(NOLOCK) ON 
		Res.Doc_Emisor_RUC=de.Doc_Emisor_RUC AND res.Doc_Tipo=de.Doc_Tipo AND 
		Res.Doc_Serie = de.Doc_Serie AND Res.Doc_Numero = de.Doc_Numero
		INNER JOIN dbo.FACTE_RESUMEN_DIARIO rescab WITH(NOLOCK) ON 
		rescab.Doc_Emisor_RUC = Res.Doc_Emisor_RUC AND rescab.Doc_FechaGeneracion = Res.Doc_FechaGeneracion 
		AND rescab.Doc_Resumen_Correlativo = Res.Doc_Resumen_Correlativo
		WHERE Res.Doc_Emisor_RUC=@RUC
		AND Res.Doc_FechaGeneracion=@FechaResumen
		AND Res.Doc_Resumen_Correlativo=@Correlativo_3
		AND dex.TipXML_Codigo='003' 
		--AND dex.EstEnv_Codigo='003'
		--AND de.Doc_Tipo='03'
		AND de.Doc_Moneda='PEN'
		--AND de.Doc_FechaEmision=@FechaDesdeResumen 
		AND rescab.EstEnv_Codigo='001'
		END;

	SET @FechaDesdeResumen=DATEADD(DAY,1,@FechaDesdeResumen)
END
PRINT '---------------'
SET @Count=@Count-1
END
SET NOCOUNT OFF
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_DIARIO_PENDIENTES_CABECERA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_RESUMEN_DIARIO_PENDIENTES_CABECERA]
AS
BEGIN
	----Habilitados y listos para enviar
	--DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
	--DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

	--UPDATE FACTE_PROGRAMACIONES_ENVIO SET Progra_Ejecucion_Ultimo = GETDATE() WHERE Progra_Ejecucion_Ultimo IS NULL;

	----SOLO DOCUMENTOS ACTIVOS
	----CARGAR TEMPORAL
	--DECLARE @Tar_Emisor_RUC CHAR(11), @Tar_TipoDoc_Codigo CHAR(2), @Tar_TipoDoc_Serie CHAR(4);
	--DECLARE @Num_Emisor CHAR(11), @Num_TipDoc CHAR(2), @Num_Serie CHAR(4), @Num_Inicial VARCHAR(8), @Num_Final VARCHAR(8), @Num_Final_Correlativo VARCHAR(8), @Num_Fecha_Emision DATE;
	--CREATE TABLE #Temp_Prog_Resumenes
	--( Emisor_RUC CHAR(11), TipoDoc_Codigo CHAR(2), TipoDoc_Serie CHAR(4) );
	--INSERT INTO #Temp_Prog_Resumenes
	--( Emisor_RUC , TipoDoc_Codigo , TipoDoc_Serie )
	--SELECT ProgX.Emp_RUC, ProgX.Progra_Appli_TipoDoc,
	--(CASE WHEN ProgX.Progra_Appli_Filtra_Serie = 1 THEN ProgX.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie'
	--FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX
	--WHERE ProgX.Progra_Habilitado = 1 AND ProgX.Progra_EsResumenBoletas = 1
	--AND ProgX.Progra_Estado='001'
	--AND @AhoraFechaHora >= ProgX.Progra_Fecha_Desde 
	--AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	--AND DATEADD(HOUR,ProgX.Progra_FrecuEnvio_Hora_XCadaHora, ProgX.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora
	--AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora;

	----SELECT * FROM #Temp_Prog_Resumenes;

	--WHILE EXISTS(SELECT Emisor_RUC FROM #Temp_Prog_Resumenes)
	--BEGIN
	--	--Item programado -- seleccionado
	--	SELECT TOP 1 @Tar_Emisor_RUC = Emisor_RUC , 
	--	@Tar_TipoDoc_Codigo = TipoDoc_Codigo , @Tar_TipoDoc_Serie = TipoDoc_Serie 
	--	FROM #Temp_Prog_Resumenes;
	--	--Item programado -- eliminado
	--	DELETE #Temp_Prog_Resumenes 
	--	WHERE Emisor_RUC = @Tar_Emisor_RUC AND TipoDoc_Codigo = @Tar_TipoDoc_Codigo
	--	AND (@Tar_TipoDoc_Serie IS NULL OR TipoDoc_Serie= @Tar_TipoDoc_Serie);
		
	--	--CONSULTA SUNAT: Se puede enviar resumenes pero con un rango no consecutivo?

	--	--Tabla correlativos de series
	--	CREATE TABLE #fe_grupo_series_correlativos
	--	(
	--		fe_grp_emisor CHAR(11),
	--		fe_grp_tipodoc CHAR(2),
	--		fe_grp_fecha_emision DATE,
	--		fe_grp_serie CHAR(4),
	--		fe_grp_num_inicial VARCHAR(8),
	--		fe_grp_num_final VARCHAR(8),
	--		fe_grp_num_final_correlativo VARCHAR(8),
	--		fe_grp_habilitado BIT,
	--		fe_grp_ejecutado BIT
	--	);

	--	INSERT INTO #fe_grupo_series_correlativos
	--	( fe_grp_emisor , fe_grp_tipodoc , fe_grp_fecha_emision , fe_grp_serie , fe_grp_num_inicial , fe_grp_num_final , fe_grp_num_final_correlativo ,
	--	fe_grp_habilitado , fe_grp_ejecutado )
	--	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo , Doc.Doc_FechaEmision , Doc.Doc_Serie, MIN(Doc.Doc_Numero), MAX(Doc.Doc_Numero), NULL,0,0
	--	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc 
	--	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1
	--	WHERE Doc.Doc_Emisor_RUC = @Tar_Emisor_RUC AND Doc.Doc_Tipo = @Tar_TipoDoc_Codigo
	--	AND (@Tar_TipoDoc_Serie IS NULL OR Doc.Doc_Serie = @Tar_TipoDoc_Serie)
	--	AND DocXML.EstEnv_Codigo='001'
	--	GROUP BY Doc.Doc_Emisor_RUC , Doc.Doc_FechaEmision , Doc.Doc_Tipo , Doc.Doc_Serie , Doc.Doc_Moneda
	--	ORDER BY Doc.Doc_Tipo ASC, Doc.Doc_FechaEmision ASC, Doc.Doc_Serie ASC ;
		
	--	--Tabla para buscar huecos s-1
	--	CREATE TABLE #fe_correlativo( fe_correlativo INT );

	--	DECLARE @Correlativo_Actual INT, @Fecha_Resumen DATE;

	--	SET @Fecha_Resumen = GETDATE();
	--	SET @Correlativo_Actual = ISNULL((SELECT MAX(OrgDoc.Doc_Resumen_Correlativo) FROM FACTE_RESUMEN_DIARIO OrgDoc WHERE OrgDoc.Doc_Emisor_RUC = @Tar_Emisor_RUC AND OrgDoc.Doc_FechaGeneracion = @Fecha_Resumen),0) + 1;
	--	INSERT INTO dbo.FACTE_RESUMEN_DIARIO
	--			( Doc_Emisor_RUC , Doc_FechaGeneracion , Doc_Resumen_Correlativo ,
	--				Doc_NombreArchivoXML , Doc_SUNATTicket_ID , SUNAT_Respuesta_Codigo ,
	--				EstEnv_Codigo , Doc_Estado )
	--	VALUES  ( @Tar_Emisor_RUC , @Fecha_Resumen , @Correlativo_Actual , -- Doc_Resumen_Correlativo - int
	--				NULL , NULL , NULL ,
	--				'001' , '0001' );

	--	---ENVIAMOS SOLO AQUELLOS QUE SON CORRELATIVOS
	--	WHILE EXISTS(SELECT fe_grp_emisor FROM #fe_grupo_series_correlativos WHERE fe_grp_ejecutado=0)
	--	BEGIN
	--		SET @Num_Final_Correlativo = NULL;
	--		SELECT TOP 1 @Num_Emisor = fe_grp_emisor, @Num_TipDoc =fe_grp_tipodoc, 
	--		@Num_Serie = fe_grp_serie, @Num_Inicial = fe_grp_num_inicial, @Num_Final = fe_grp_num_final ,
	--		@Num_Fecha_Emision = fe_grp_fecha_emision
	--		FROM #fe_grupo_series_correlativos WHERE fe_grp_ejecutado = 0;
	
	--		DECLARE @Temp_Correlativo INT=CAST(@Num_Inicial AS INT);
			
	--		WHILE (@Temp_Correlativo<=CAST(@Num_Final AS INT))
	--		BEGIN
	--			INSERT INTO #fe_correlativo ( fe_correlativo ) VALUES  ( @Temp_Correlativo );
	--			SET @Temp_Correlativo = @Temp_Correlativo + 1;
	--		END

	--		---VERIFICAMOS QUE SEAN CONSECUTIVOS - BUSCAR HUECOS
	--		SELECT TOP 1 @Num_Final_Correlativo = ( Corr.fe_correlativo - 1) --,ISNULL(DOC.Doc_Numero,'') AS PrimerHueco 
	--		FROM #fe_correlativo Corr 
	--		LEFT JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc ON Doc.Doc_Emisor_RUC = @Num_Emisor AND Doc.Doc_Tipo = @Num_TipDoc AND Doc.Doc_Serie = @Num_Serie AND CAST(Doc.Doc_Numero AS INT) = Corr.fe_correlativo
	--		WHERE ISNULL(DOC.Doc_Numero,'') = ''
	--		ORDER BY  Corr.fe_correlativo ASC;

	--		SET @Num_Final_Correlativo =RIGHT('00000000' + ISNULL(@Num_Final_Correlativo, @Num_Final ),8);

	--		UPDATE #fe_grupo_series_correlativos 
	--		SET fe_grp_num_final_correlativo = @Num_Final_Correlativo, fe_grp_ejecutado = 1
	--		WHERE fe_grp_emisor = @Num_Emisor AND fe_grp_tipodoc = @Num_TipDoc AND fe_grp_serie = @Num_Serie 
	--		AND fe_grp_num_inicial = @Num_Inicial AND fe_grp_num_final = @Num_Final  ;

	--		--INSERTAMOS EL RESUMEN --EstEnv_Codigo

	--		INSERT INTO dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO
	--		( Doc_Emisor_RUC , Doc_FechaGeneracion , Doc_Resumen_Correlativo , Doc_Item,
	--		  Doc_Tipo , Doc_Serie , Doc_Numero,
	--		  Doc_Estado ,
	--		  Doc_Aud_Registro_FechaHora ,  Doc_Aud_Registro_Equipo )
	--		SELECT Doc.Doc_Emisor_RUC, @Fecha_Resumen,@Correlativo_Actual, ROW_NUMBER() OVER (ORDER BY Doc.Doc_Numero),
	--		Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero,
	--		'0001',
	--		GETDATE(), HOST_NAME()
	--		FROM FACTE_DOCUMENTO_ELECTRONICO Doc 
	--		WHERE Doc.Doc_Emisor_RUC = @Num_Emisor AND Doc.Doc_Tipo = @Num_TipDoc
	--		AND Doc.Doc_Serie = @Num_Serie AND CAST(DOC.Doc_Numero AS INT) BETWEEN CAST(@Num_Inicial AS INT) AND CAST(ISNULL(@Num_Final_Correlativo, @Num_Final ) AS INT)
	--		ORDER BY Doc.Doc_Tipo ASC, Doc.Doc_Serie ASC , Doc.Doc_Numero ASC ;

	--		--actualizando el XML enviado
	--		UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET EstEnv_Codigo='005'
	--		FROM FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
	--		INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO Doc ON Doc.Doc_Emisor_RUC = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Emisor_RUC AND Doc.Doc_Tipo = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Tipo AND Doc.Doc_Serie = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Serie AND Doc.Doc_Numero = FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.Doc_Numero AND FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE.XML_Reciente = 1
	--		WHERE Doc.Doc_Emisor_RUC = @Num_Emisor AND Doc.Doc_Tipo = @Num_TipDoc
	--		AND Doc.Doc_Serie = @Num_Serie AND CAST(DOC.Doc_Numero AS INT) BETWEEN CAST(@Num_Inicial AS INT) AND CAST(ISNULL(@Num_Final_Correlativo, @Num_Final ) AS INT) ;

	--		--actualizando la cabecera con los datos del resumen
	--		UPDATE FACTE_DOCUMENTO_ELECTRONICO SET Doc_Resumen_FechaGeneracion=@Fecha_Resumen, Doc_Resumen_Correlativo = @Correlativo_Actual
	--		WHERE Doc_Emisor_RUC = @Num_Emisor AND Doc_Tipo = @Num_TipDoc
	--		AND Doc_Serie = @Num_Serie AND CAST(Doc_Numero AS INT) BETWEEN CAST(@Num_Inicial AS INT) AND CAST(ISNULL(@Num_Final_Correlativo, @Num_Final ) AS INT) ;
			
	--	END

	--	IF NOT EXISTS(SELECT det.Doc_Emisor_RUC FROM dbo.FACTE_RESUMEN_DIARIO_DOCUMENTO Det 
	--	WHERE Det.Doc_Emisor_RUC = @Tar_Emisor_RUC AND Det.Doc_FechaGeneracion = @Fecha_Resumen AND Det.Doc_Resumen_Correlativo =@Correlativo_Actual)
	--	BEGIN
	--		DELETE FACTE_RESUMEN_DIARIO 
	--		WHERE Doc_Emisor_RUC = @Tar_Emisor_RUC AND Doc_FechaGeneracion = @Fecha_Resumen AND Doc_Resumen_Correlativo =@Correlativo_Actual;
	--	END
	--	ELSE
 --       BEGIN
	--		UPDATE dbo.FACTE_RESUMEN_DIARIO SET Doc_Fecha_Emision = @Num_Fecha_Emision
	--		WHERE Doc_Emisor_RUC = @Tar_Emisor_RUC AND Doc_FechaGeneracion = @Fecha_Resumen AND Doc_Resumen_Correlativo =@Correlativo_Actual;
 --       END

	--	DROP TABLE #fe_correlativo;

	--	--SELECT * FROM #fe_grupo_series_correlativos;
		
	--	DROP TABLE #fe_grupo_series_correlativos;
	--END

	--DROP TABLE #Temp_Prog_Resumenes;

	--SOLO MOSTRAMOS LAS CABECERAS
	SELECT Doc_Emisor_RUC,Emp.Emp_NombreRazonSocial, Doc_FechaGeneracion, Doc_Resumen_Correlativo, 
	Doc_Fecha_Emision, Emp.Path_Root_App + '\' + Doc_Emisor_RUC,Emp.Etapa_Codigo
	FROM FACTE_RESUMEN_DIARIO Diar WITH(NOLOCK)
	INNER JOIN dbo.FACTE_EMPRESA Emp WITH(NOLOCK) ON Emp.Emp_RUC = Diar.Doc_Emisor_RUC
	WHERE Diar.EstEnv_Codigo='001'
	ORDER BY Diar.Doc_Emisor_RUC,Diar.Doc_FechaGeneracion ,Diar.Doc_Resumen_Correlativo ASC
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_RESUMEN_DIARIO_PENDIENTES_DETALLE]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UP_FACTE_RESUMEN_DIARIO_PENDIENTES_DETALLE]
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_FechaGeneracion DATE,
@PDoc_Resumen_Correlativo INT
)
AS
BEGIN
	SELECT
	RsmDoc.Doc_Item, DocOrig.Doc_Moneda, 
	RsmDoc.Doc_Tipo, RsmDoc.Doc_Serie, RsmDoc.Doc_Numero,
	(CASE WHEN LTRIM(RTRIM(DocOrig.Doc_Cliente_TipoDocIdent_Numero))='-' THEN '00000000' ELSE DocOrig.Doc_Cliente_TipoDocIdent_Numero END) AS 'Doc_Cliente_TipoDocIdent_Numero', 
	DocOrig.Doc_Cliente_TipoDocIdent_Codigo, 
	ISNULL(DocOrig.Doc_Modif_Doc_Serie + '-' + DocOrig.Doc_Modif_Doc_Numero,'') AS 'SerieNumero_Boleta_Modif', 
	ISNULL(DocOrig.Doc_Modif_Doc_Tipo,'') AS 'Tipo_Boleta_Modif',
	ISNULL(RsmDoc.Doc_CondicionItem,'1') AS 'Estado_Item', ---Cat 19: 1-Adicionar 2-Modificar 3-Anulado 4-Anulado-en-el-dia(Anuladoantes de informar comprobante) Transporte publico
	--DocOrig.Doc_Oper_Gravadas, DocOrig.Doc_Oper_Exoneradas, DocOrig.Doc_Oper_Inafectas, DocOrig.Doc_Oper_Gratuitas, 
	--DocOrig.Doc_ISC, DocOrig.Doc_IGV, DocOrig.Doc_Otros_Tributos, DocOrig.Doc_Importe, DocOrig.Doc_Otros_Conceptos
	ISNULL(DocOrig.Doc_Oper_Gravadas,0.00), ISNULL(DocOrig.Doc_Oper_Exoneradas,0.00), 
	ISNULL(DocOrig.Doc_Oper_Inafectas,0.00), ISNULL(DocOrig.Doc_Oper_Gratuitas,0.00), 
	ISNULL(DocOrig.Doc_ISC,0.00), DocOrig.Doc_IGV, ISNULL(DocOrig.Doc_Otros_Tributos,0.00), 
	DocOrig.Doc_Importe, ISNULL(DocOrig.Doc_Otros_Conceptos,0.00),
	ISNULL((SELECT TOP 1 xm.XML_Archivo FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE xm WITH(NOLOCK) WHERE xm.Doc_Emisor_RUC=DocOrig.Doc_Emisor_RUC AND xm.Doc_Tipo=DocOrig.Doc_Tipo
	AND xm.Doc_Serie=DocOrig.Doc_Serie AND xm.Doc_Numero=DocOrig.Doc_Numero AND xm.TipXML_Codigo<>'003' ORDER BY xm.XML_Correlativo DESC),'') AS 'xml_Gen'
	FROM FACTE_RESUMEN_DIARIO_DOCUMENTO RsmDoc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO DocOrig  WITH(NOLOCK) ON 
	DocOrig.Doc_Emisor_RUC = RsmDoc.Doc_Emisor_RUC AND DocOrig.Doc_Tipo = RsmDoc.Doc_Tipo 
	AND DocOrig.Doc_Serie = RsmDoc.Doc_Serie AND DocOrig.Doc_Numero = RsmDoc.Doc_Numero
	WHERE RsmDoc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND RsmDoc.Doc_FechaGeneracion = @PDoc_FechaGeneracion 
	AND RsmDoc.Doc_Resumen_Correlativo = @PDoc_Resumen_Correlativo ;
END



GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_SOCIO_NEGOCIO_BUSQUEDA]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_SOCIO_NEGOCIO_BUSQUEDA]
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6', --Por defecto es RUC
@PReceptor_TipoDocIdent_Numero VARCHAR(11)=NULL
)
AS
BEGIN
	SELECT SocNeg.TipoDocIdent_Codigo,Tip.TipoDocIdent_Corto,SocNeg.SocNeg_TipoDocIdent_Numero,SocNeg.SocNeg_RazonSocialNombres
	FROM FACTE_SOCIO_NEGOCIO SocNeg WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO Tip WITH(NOLOCK) ON Tip.TipoDocIdent_Codigo = SocNeg.TipoDocIdent_Codigo
END


GO
/****** Object:  StoredProcedure [dbo].[UP_FACTE_TABLA_OBSERVAC_ERRORES_INSERT]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_FACTE_TABLA_OBSERVAC_ERRORES_INSERT]
(
	@PObsErr_Maestro CHAR(3),
	@PObsErr_Codigo CHAR(4),
	@PObsErr_Descripcion VARCHAR(150)
)
AS
BEGIN
	IF NOT EXISTS(SELECT TE.ObsErr_Codigo FROM dbo.FACTE_TABLA_OBSERVAC_ERRORES TE WITH(NOLOCK) WHERE TE.ObsErr_Maestro = @PObsErr_Maestro AND TE.ObsErr_Codigo = @PObsErr_Codigo)
	BEGIN
		INSERT INTO dbo.FACTE_TABLA_OBSERVAC_ERRORES ( ObsErr_Maestro , ObsErr_Codigo , ObsErr_Descripcion )
		VALUES ( @PObsErr_Maestro , @PObsErr_Codigo , @PObsErr_Descripcion );
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UP_JOB_ACTUALIZAR_ESTADO_FE_SG]    Script Date: 22/12/2020 21:08:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 CREATE PROC [dbo].[UP_JOB_ACTUALIZAR_ESTADO_FE_SG]
 AS
 SET XACT_ABORT ON

UPDATE VEN_SG
SET VEN_SG.Vnt_FactElectronica_Estado=EST_FE.EstEnv_Codigo_Cliente
	,VEN_SG.XML_DigestValue=ISNULL(XML_FE.XML_DigestValue,VEN_SG.XML_DigestValue)
	,VEN_SG.XML_SignatureValue=ISNULL(XML_FE.XML_SignatureValue,VEN_SG.XML_SignatureValue)
FROM [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] XML_FE
INNER JOIN FACTE_ESTADO_ENVIO EST_FE ON EST_FE.EstEnv_Codigo=XML_FE.EstEnv_Codigo
INNER JOIN SG_LAU.dbo.EMPRESA EMP_SG ON EMP_SG.Emp_cNumRuc=XML_FE.Doc_Emisor_RUC
INNER JOIN SG_LAU.dbo.GNL_TIPO_DOCUMENTO DOC_SG ON DOC_SG.TpDc_Sunat=XML_FE.Doc_Tipo
INNER JOIN SG_LAU.dbo.VTS_MOVIMIENTO_VENTAS_CAB VEN_SG  ON VEN_SG.Emp_cCodigo=EMP_SG.Emp_cCodigo 
AND DOC_SG.TpDc_Sunat= XML_FE.Doc_Tipo AND VEN_SG.Vnt_NPSerie=XML_FE.Doc_Serie AND VEN_SG.Vnt_NPNumer=XML_FE.Doc_Numero 
WHERE XML_FE.XML_Reciente=1 --AND Isnull(XML_FE.Estado_FE_SG,0)=0


--UPDATE XML_FE
--SET XML_FE.Estado_FE_SG=1
--FROM [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] XML_FE
--INNER JOIN FACTE_ESTADO_ENVIO EST_FE ON EST_FE.EstEnv_Codigo=XML_FE.EstEnv_Codigo
--INNER JOIN SG_LAU.dbo.EMPRESA EMP_SG ON EMP_SG.Emp_cNumRuc=XML_FE.Doc_Emisor_RUC
--INNER JOIN SG_LAU.dbo.GNL_TIPO_DOCUMENTO DOC_SG ON DOC_SG.TpDc_Sunat=XML_FE.Doc_Tipo
--INNER JOIN SG_LAU.dbo.VTS_MOVIMIENTO_VENTAS_CAB VEN_SG  ON VEN_SG.Emp_cCodigo=EMP_SG.Emp_cCodigo 
--AND DOC_SG.TpDc_Sunat= XML_FE.Doc_Tipo AND VEN_SG.Vnt_NPSerie=XML_FE.Doc_Serie AND VEN_SG.Vnt_NPNumer=XML_FE.Doc_Numero 
--WHERE XML_FE.XML_Reciente=1 AND Isnull(XML_FE.Estado_FE_SG,0)=0


--UPDATE VEN_SG
--SET VEN_SG.Vnt_FactElectronica_Estado=EST_FE.EstEnv_Codigo_Cliente
--	,VEN_SG.XML_DigestValue=ISNULL(XML_FE.XML_DigestValue,VEN_SG.XML_DigestValue)
--	,VEN_SG.XML_SignatureValue=ISNULL(XML_FE.XML_SignatureValue,VEN_SG.XML_SignatureValue)
--FROM [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] XML_FE
--INNER JOIN FACTE_ESTADO_ENVIO EST_FE ON EST_FE.EstEnv_Codigo=XML_FE.EstEnv_Codigo
--INNER JOIN CLIENT_01.SG_LAU.dbo.EMPRESA EMP_SG ON EMP_SG.Emp_cNumRuc=XML_FE.Doc_Emisor_RUC
--INNER JOIN CLIENT_01.SG_LAU.dbo.GNL_TIPO_DOCUMENTO DOC_SG ON DOC_SG.TpDc_Sunat=XML_FE.Doc_Tipo
--INNER JOIN CLIENT_01.SG_LAU.dbo.VTS_MOVIMIENTO_VENTAS_CAB VEN_SG  ON VEN_SG.Emp_cCodigo=EMP_SG.Emp_cCodigo 
--AND DOC_SG.TpDc_Sunat= XML_FE.Doc_Tipo AND VEN_SG.Vnt_NPSerie=XML_FE.Doc_Serie AND VEN_SG.Vnt_NPNumer=XML_FE.Doc_Numero 
--WHERE XML_FE.XML_Reciente=1 AND Isnull(XML_FE.Estado_FE_SG,0)=0

--UPDATE XML_FE
--SET XML_FE.Estado_FE_SG=1
--FROM [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] XML_FE
--INNER JOIN FACTE_ESTADO_ENVIO EST_FE ON EST_FE.EstEnv_Codigo=XML_FE.EstEnv_Codigo
--INNER JOIN CLIENT_01.SG_LAU.dbo.EMPRESA EMP_SG ON EMP_SG.Emp_cNumRuc=XML_FE.Doc_Emisor_RUC
--INNER JOIN CLIENT_01.SG_LAU.dbo.GNL_TIPO_DOCUMENTO DOC_SG ON DOC_SG.TpDc_Sunat=XML_FE.Doc_Tipo
--INNER JOIN CLIENT_01.SG_LAU.dbo.VTS_MOVIMIENTO_VENTAS_CAB VEN_SG  ON VEN_SG.Emp_cCodigo=EMP_SG.Emp_cCodigo 
--AND DOC_SG.TpDc_Sunat= XML_FE.Doc_Tipo AND VEN_SG.Vnt_NPSerie=XML_FE.Doc_Serie AND VEN_SG.Vnt_NPNumer=XML_FE.Doc_Numero 
--WHERE XML_FE.XML_Reciente=1 AND Isnull(XML_FE.Estado_FE_SG,0)=0

--UPDATE VEN_SG
--SET VEN_SG.Vnt_FactElectronica_Estado=EST_FE.EstEnv_Codigo_Cliente
--	,VEN_SG.XML_DigestValue=ISNULL(XML_FE.XML_DigestValue,VEN_SG.XML_DigestValue)
--	,VEN_SG.XML_SignatureValue=ISNULL(XML_FE.XML_SignatureValue,VEN_SG.XML_SignatureValue)
--FROM [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] XML_FE
--INNER JOIN FACTE_ESTADO_ENVIO EST_FE ON EST_FE.EstEnv_Codigo=XML_FE.EstEnv_Codigo
--INNER JOIN CLIENT_02.SG_LAU.dbo.EMPRESA EMP_SG ON EMP_SG.Emp_cNumRuc=XML_FE.Doc_Emisor_RUC
--INNER JOIN CLIENT_02.SG_LAU.dbo.GNL_TIPO_DOCUMENTO DOC_SG ON DOC_SG.TpDc_Sunat=XML_FE.Doc_Tipo
--INNER JOIN CLIENT_02.SG_LAU.dbo.VTS_MOVIMIENTO_VENTAS_CAB VEN_SG  ON VEN_SG.Emp_cCodigo=EMP_SG.Emp_cCodigo 
--AND DOC_SG.TpDc_Sunat= XML_FE.Doc_Tipo AND VEN_SG.Vnt_NPSerie=XML_FE.Doc_Serie AND VEN_SG.Vnt_NPNumer=XML_FE.Doc_Numero 
--WHERE XML_FE.XML_Reciente=1 AND Isnull(XML_FE.Estado_FE_SG,0)=0

--UPDATE XML_FE
--SET XML_FE.Estado_FE_SG=1
--FROM [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] XML_FE
--INNER JOIN FACTE_ESTADO_ENVIO EST_FE ON EST_FE.EstEnv_Codigo=XML_FE.EstEnv_Codigo
--INNER JOIN CLIENT_02.SG_LAU.dbo.EMPRESA EMP_SG ON EMP_SG.Emp_cNumRuc=XML_FE.Doc_Emisor_RUC
--INNER JOIN CLIENT_02.SG_LAU.dbo.GNL_TIPO_DOCUMENTO DOC_SG ON DOC_SG.TpDc_Sunat=XML_FE.Doc_Tipo
--INNER JOIN CLIENT_02.SG_LAU.dbo.VTS_MOVIMIENTO_VENTAS_CAB VEN_SG  ON VEN_SG.Emp_cCodigo=EMP_SG.Emp_cCodigo 
--AND DOC_SG.TpDc_Sunat= XML_FE.Doc_Tipo AND VEN_SG.Vnt_NPSerie=XML_FE.Doc_Serie AND VEN_SG.Vnt_NPNumer=XML_FE.Doc_Numero 
--WHERE XML_FE.XML_Reciente=1 AND Isnull(XML_FE.Estado_FE_SG,0)=0







GO
