--SELECT * FROM dbo.VEN_MOVIMIENTO_CAJA 
--SELECT * FROM dbo.VEN_MOVIMIENTO_CAJA_DET

--SELECT * FROM dbo.VEN_APERTURA_CIERRE_CAJA_CAB 
--SELECT * FROM dbo.VEN_APERTURA_CIERRE_CAJA_DET

--SELECT * FROM dbo.GNL_GENERAL_DET WHERE Id_General_Det='0050'

alter PROC pa_OTROS_INGRESOS_CORTE_BEFORE
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL

)
AS
BEGIN
SELECT --ISNULL(Cja_Motivo,'') AS Motivo ,	
	  ISNULL(SUM(ISNULL(Cja_Monto,0)),0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Aper_Cierre_Codigo_Item IS NULL and
Cja_Tipo='0049' --INGRESOS

END
GO

ALTER PROC pa_OTROS_INGRESOS_CORTE_TODO
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL

)
AS
BEGIN
SELECT --ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(SUM(ISNULL(Cja_Monto,0)),0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Tipo='0049' --INGRESOS

END
GO

ALTER PROC pa_OTROS_INGRESOS_CORTE
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL

)
AS
BEGIN
SELECT ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(Cja_Monto,0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Aper_Cierre_Codigo_Item=@Cja_Aper_Cierre_Codigo_Item and
Cja_Tipo='0049' --INGRESOS

END
GO

 
ALTER PROC pa_OTROS_EGRESOS_CORTE
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL
)
AS
BEGIN
SELECT ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(Cja_Monto,0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Aper_Cierre_Codigo_Item=@Cja_Aper_Cierre_Codigo_Item AND
Cja_Tipo='0050' --EGRESOS

END
GO

ALTER PROC pa_OTROS_EGRESOS_CORTE_BEFORE
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL
)
AS
BEGIN
SELECT --ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(SUM(ISNULL(Cja_Monto,0)),0)AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Aper_Cierre_Codigo_Item IS NULL AND
Cja_Tipo='0050' --EGRESOS

END
GO

ALTER PROC pa_OTROS_EGRESOS_CORTE_TODO
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL
)
AS
BEGIN
SELECT --ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(SUM(ISNULL(Cja_Monto,0)),0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Tipo='0050' --EGRESOS

END
GO



ALTER PROC pa_VEN_APERTURA_CIERRE_CAJA_DET_INSERTAR
(
@Id_Empresa CHAR(3), 
@Pdv_Codigo CHAR(2), 
@Cja_Codigo CHAR(3), 
@Cja_Aper_Cierre_Codigo INT, 
@Cja_Total_Efectivo DECIMAL(18,4), 
@Cja_Total_Credito DECIMAL(18,4), 
@Cja_Total_Tarjeta DECIMAL(18,4), 
@Autor VARCHAR(50), 
@Maquina VARCHAR(50)
)
AS
BEGIN

DECLARE @Cja_Aper_Cierre_Codigo_Item INT;

SET @Cja_Aper_Cierre_Codigo_Item = (SELECT ISNULL(MAX(Cja_Aper_Cierre_Codigo_Item),0)+1 FROM dbo.VEN_APERTURA_CIERRE_CAJA_DET
								 WHERE Id_Empresa = @Id_Empresa AND 
								 Pdv_Codigo = @Pdv_Codigo AND 
								 Cja_Codigo = @Cja_Codigo AND 
								 Cja_Aper_Cierre_Codigo = @Cja_Aper_Cierre_Codigo)

INSERT INTO dbo.VEN_APERTURA_CIERRE_CAJA_DET
        ( Id_Empresa ,   Pdv_Codigo ,
          Cja_Codigo , Cja_Aper_Cierre_Codigo ,
          Cja_Aper_Cierre_Codigo_Item ,
          Cja_Total_Efectivo ,
          Cja_Total_Credito ,
          Cja_Total_Tarjeta ,
          Cja_Fecha_Corte ,
          Cja_Autor_Corte ,
          Cja_Maquina_Corte
        )
VALUES  ( @Id_Empresa ,   @Pdv_Codigo ,
          @Cja_Codigo , @Cja_Aper_Cierre_Codigo ,
          @Cja_Aper_Cierre_Codigo_Item ,
          @Cja_Total_Efectivo ,
          @Cja_Total_Credito ,
          @Cja_Total_Tarjeta ,
          GETDATE() ,
          @Autor,
          @Maquina
        )


		UPDATE dbo.CTB_MOVIMIENTO_CONTABLE_CAB 
		SET	cja_Estado='0059',--	cerrado
			Cja_Aper_Cierre_Codigo_Item=@Cja_Aper_Cierre_Codigo_Item
		WHERE Id_Empresa=@Id_Empresa AND
			  ctb_pdv_codigo=@Pdv_Codigo AND	
			  Cja_Aper_Cierre_Codigo=@Cja_Aper_Cierre_Codigo AND
              Cja_Aper_Cierre_Codigo_Item=0


		UPDATE dbo.CTB_MOVIMIENTO_INVENTARIO_CAB
		SET cja_Estado='0059',
			Cja_Aper_Cierre_Codigo_Item=@Cja_Aper_Cierre_Codigo_Item
		WHERE Id_Empresa=@Id_Empresa AND
			  Inv_pdv_codigo=@Pdv_Codigo AND
              Cja_Aper_Cierre_Codigo=@Cja_Aper_Cierre_Codigo AND
              Cja_Aper_Cierre_Codigo_Item=0
					  
		UPDATE dbo.VEN_MOVIMIENTO_CAB
			SET cja_Estado='0059',
			Cja_Aper_Cierre_Codigo_Item=@Cja_Aper_Cierre_Codigo_Item
		WHERE Id_Empresa=@Id_Empresa AND
			  Id_Pdv_Codigo=@Pdv_Codigo AND
              Cja_Aper_Cierre_Codigo=@Cja_Aper_Cierre_Codigo AND
              Cja_Aper_Cierre_Codigo_Item=0

		UPDATE dbo.VEN_MOVIMIENTO_CAJA
		SET Cja_Aper_Cierre_Codigo_Item = @Cja_Aper_Cierre_Codigo_Item
		WHERE Id_Empresa=@Id_Empresa AND
			  Pdv_Codigo=@Pdv_Codigo AND
              Cja_Aper_Cierre_Codigo=@Cja_Aper_Cierre_Codigo AND
              Cja_Aper_Cierre_Codigo_Item IS NULL 

END


GO


 