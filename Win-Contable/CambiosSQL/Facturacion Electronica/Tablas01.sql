 
CREATE TABLE [dbo].[CFG_APPLICATION](
	[App_Replicacion] [bit] NULL,
	[App_Replicacion_Servidor] [varchar](100) NULL,
	[App_Replicacion_BD] [varchar](50) NULL,
	[App_Replicacion_Usuario] [varchar](50) NULL,
	[App_Replicacion_Contrasenia] [varchar](50) NULL,
	[App_Replicacion_Prog_Hora_Establecido] [bit] NULL,
	[App_Replicacion_Prog_Hora_CadaRangoHoras] [bit] NULL,
	[App_Replicacion_Prog_Hora] [int] NULL,
	[App_Replicacion_Prog_Hora_Desde] [time](7) NULL,
	[App_Replicacion_Prog_Hora_Hasta] [time](7) NULL,
	[App_Replicacion_RutaPrincipalArchivos] [varchar](250) NULL,
	[Aud_Actualizacion_FechaHora] [datetime] NULL,
	[Aud_Actualizacion_Equipo] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_CERTIFICADO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_CERTIFICADO](
	[Emp_RUC] [char](11) NOT NULL,
	[Cert_Correlativo] [int] NOT NULL,
	[Cert_Codigo] [varchar](15) NULL,
	[Cert_EsPropio] [bit] NULL,
	[Cert_Ext_RUC] [varchar](15) NULL,
	[Cert_Ext_RazonSocial] [varchar](150) NULL,
	[Cert_Vigencia_Desde] [date] NULL,
	[Cert_Vigencia_Hasta] [date] NULL,
	[Cert_Contrasena] [varchar](300) NULL,
	[Cert_Ruta] [varchar](300) NULL,
	[Aud_Registro_Fecha] [datetime] NULL,
	[Aud_Registro_Autor] [varchar](50) NULL,
	[Aud_Registro_Equipo] [varchar](50) NULL,
	[Aud_Actualizacion_Fecha] [datetime] NULL,
	[Aud_Actualizacion_Autor] [varchar](50) NULL,
	[Aud_Actualizacion_Equipo] [varchar](50) NULL,
 CONSTRAINT [PK_FACTE_CERTIFICADO] PRIMARY KEY CLUSTERED 
(
	[Emp_RUC] ASC,
	[Cert_Correlativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_CLASIFICACION_ESTADOS_EXEPCION]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_CLASIFICACION_ESTADOS_EXEPCION](
	[Codigo] [char](4) NULL,
	[Descripcion] [varchar](500) NULL,
	[Cod_Estado] [char](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_COMUNICACION_BAJA]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_COMUNICACION_BAJA](
	[RsmBj_Emisor_RUC] [char](11) NOT NULL,
	[RsmBj_GrpTipo_Codigo] [char](3) NOT NULL,
	[RsmBj_FechaGeneracion] [date] NOT NULL,
	[RsmBj_Resumen_Correlativo] [int] NOT NULL,
	[RsmBj_Fecha_Generacion_Doc_Baja] [date] NULL,
	[RsmBj_NombreArchivoXML] [varchar](50) NULL,
	[RsmBj_SUNATTicket_ID] [varchar](50) NULL,
	[SUNAT_Respuesta_Codigo] [char](4) NULL,
	[EstEnv_Codigo] [char](3) NULL,
	[RsmBj_Estado] [char](4) NULL,
	[Doc_Aud_Registro_FechaHora] [datetime] NULL,
	[Doc_Aud_Registro_Equipo] [varchar](50) NULL,
 CONSTRAINT [PK_FACTE_COMUNICACION_BAJA] PRIMARY KEY CLUSTERED 
(
	[RsmBj_Emisor_RUC] ASC,
	[RsmBj_GrpTipo_Codigo] ASC,
	[RsmBj_FechaGeneracion] ASC,
	[RsmBj_Resumen_Correlativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_COMUNICACION_BAJA_DOCUMENTO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_COMUNICACION_BAJA_DOCUMENTO](
	[RsmBj_Emisor_RUC] [char](11) NOT NULL,
	[RsmBj_GrpTipo_Codigo] [char](3) NOT NULL,
	[RsmBj_FechaGeneracion] [date] NOT NULL,
	[RsmBj_Resumen_Correlativo] [int] NOT NULL,
	[RsmBj_Item] [int] NOT NULL,
	[Doc_Tipo] [char](2) NULL,
	[Doc_Serie] [char](4) NULL,
	[Doc_Numero] [char](8) NULL,
 CONSTRAINT [PK_FACTE_COMUNICACION_BAJA_DOCUMENTO] PRIMARY KEY CLUSTERED 
(
	[RsmBj_Emisor_RUC] ASC,
	[RsmBj_GrpTipo_Codigo] ASC,
	[RsmBj_FechaGeneracion] ASC,
	[RsmBj_Resumen_Correlativo] ASC,
	[RsmBj_Item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_ELECTRONICO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO](
	[Doc_Emisor_RUC] [char](11) NOT NULL,
	[Doc_Tipo] [char](2) NOT NULL,
	[Doc_Serie] [char](4) NOT NULL,
	[Doc_Numero] [char](8) NOT NULL,
	[Doc_FechaEmision] [date] NULL,
	[Doc_Moneda] [char](3) NULL,
	[Doc_Oper_Gravadas] [decimal](15, 2) NULL,
	[Doc_Oper_Exoneradas] [decimal](15, 2) NULL,
	[Doc_Oper_Inafectas] [decimal](15, 2) NULL,
	[Doc_Oper_Gratuitas] [decimal](15, 2) NULL,
	[Doc_ISC] [decimal](15, 2) NULL,
	[Doc_Otros_Tributos] [decimal](15, 2) NULL,
	[Doc_Otros_Conceptos] [decimal](15, 2) NULL,
	[Doc_IGV] [decimal](15, 2) NULL DEFAULT ((0)),
	[Doc_Importe] [decimal](15, 2) NULL,
	[Doc_Certificado_Correlativo] [int] NULL,
	[Doc_Cliente_TipoDocIdent_Codigo] [char](1) NULL,
	[Doc_Cliente_TipoDocIdent_Numero] [varchar](15) NULL,
	[Doc_Modif_Doc_Tipo] [char](2) NULL,
	[Doc_Modif_Doc_Serie] [char](4) NULL,
	[Doc_Modif_Doc_Numero] [varchar](8) NULL,
	[Doc_NombreArchivoXML] [varchar](50) NULL,
	[Doc_NombreArchivoPDF] [varchar](50) NULL,
	[Doc_NombreArchivoWeb] [varchar](50) NULL,
	[Doc_Resumen_FechaGeneracion] [date] NULL,
	[Doc_Resumen_Correlativo] [int] NULL,
	[Doc_Baja_GrpTipo_Codigo] [char](3) NULL,
	[Doc_Baja_FechaGeneracion] [date] NULL,
	[Doc_Baja_Correlativo] [int] NULL,
	[Doc_Correo_Enviado] [bit] NULL,
	[Doc_Correo_EstaLeido] [bit] NULL,
	[Doc_Correo_ContadorLectura] [int] NULL,
	[Doc_Correo_LecturaFechaPrimera] [datetime] NULL,
	[Doc_Correo_LecturaFechaUltima] [datetime] NULL,
	[EstDoc_Codigo] [char](3) NULL,
	[Replicacion_Estado] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_FACTE_DOCUMENTO_ELECTRONICO] PRIMARY KEY CLUSTERED 
(
	[Doc_Emisor_RUC] ASC,
	[Doc_Tipo] ASC,
	[Doc_Serie] ASC,
	[Doc_Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO](
	[Doc_Emisor_RUC] [char](11) NOT NULL,
	[Doc_Tipo] [char](2) NOT NULL,
	[Doc_Serie] [char](4) NOT NULL,
	[Doc_Numero] [char](8) NOT NULL,
	[XML_Archivo_Item] [int] NOT NULL,
	[XML_Archivo_NombreLocal] [varchar](30) NULL,
	[XML_Archivo_Nombre] [varchar](150) NULL,
	[Aud_Registro_FechaHora] [datetime] NULL,
	[Aud_Registro_Equipo] [varchar](50) NULL,
	[Aud_Registro_Usuario] [varchar](50) NULL,
	[Replicacion_Estado] [bit] NULL,
 CONSTRAINT [PK_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO] PRIMARY KEY CLUSTERED 
(
	[Doc_Emisor_RUC] ASC,
	[Doc_Tipo] ASC,
	[Doc_Serie] ASC,
	[Doc_Numero] ASC,
	[XML_Archivo_Item] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE](
	[Doc_Emisor_RUC] [char](11) NOT NULL,
	[Doc_Tipo] [char](2) NOT NULL,
	[Doc_Serie] [char](4) NOT NULL,
	[Doc_Numero] [char](8) NOT NULL,
	[XML_Correlativo] [int] NOT NULL,
	[XML_Reciente] [bit] NULL,
	[XML_Archivo] [varchar](150) NULL,
	[TipXML_Codigo] [char](3) NULL,
	[EstEnv_Codigo] [char](3) NULL,
	[Rsm_EstSUNATItem_Codigo] [char](2) NULL,
	[XML_DigestValue] [varchar](50) NULL,
	[XML_SignatureValue] [varchar](500) NULL,
	[SUNAT_Respuesta_Codigo] [char](4) NULL,
	[XML_MotivoBaja] [varchar](100) NULL,
	[XML_FilaActiva] [bit] NULL,
	[XML_EnviadoFTP] [bit] NULL DEFAULT ((0)),
	[XML_Revercion_Anulacion] [bit] NULL DEFAULT ((0)),
	[XML_Revercion_Anulacion_FechaHora] [datetime] NULL,
	[XML_Revercion_Anulacion_Razon] [varchar](150) NULL,
	[XML_Creado_FechaHora] [datetime] NULL,
	[XML_Enviado_FechaHora] [datetime] NULL,
	[Aud_Registro_FechaHora] [datetime] NULL,
	[Aud_Registro_Equipo] [varchar](50) NULL,
	[Aud_Registro_Usuario] [varchar](50) NULL,
	[Replicacion_Estado] [bit] NULL DEFAULT ((0)),
	[XML_Aceptado_CDR_FechaHora] [datetime] NULL,
 CONSTRAINT [PK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] PRIMARY KEY CLUSTERED 
(
	[Doc_Emisor_RUC] ASC,
	[Doc_Tipo] ASC,
	[Doc_Serie] ASC,
	[Doc_Numero] ASC,
	[XML_Correlativo] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_IDENTIFICACION_TIPO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_IDENTIFICACION_TIPO](
	[TipoDocIdent_Codigo] [char](1) NOT NULL,
	[TipoDocIdent_Descripcion] [varchar](100) NULL,
	[TipoDocIdent_Corto] [varchar](10) NULL,
 CONSTRAINT [PK_FACTE_DOCUMENTO_IDENTIFICACION_TIPO] PRIMARY KEY CLUSTERED 
(
	[TipoDocIdent_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_TIPO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_TIPO](
	[TipoDoc_Codigo] [char](2) NOT NULL,
	[TipoDoc_Descripcion] [varchar](100) NULL,
	[GrpTipo_Codigo] [char](3) NULL,
 CONSTRAINT [PK_FACTE_DOCUMENTO_TIPO] PRIMARY KEY CLUSTERED 
(
	[TipoDoc_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_TIPO_GRUPO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_TIPO_GRUPO](
	[GrpTipo_Codigo] [char](3) NOT NULL,
	[GrpTipo_Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_FACTE_DOCUMENTO_TIPO_GRUPO] PRIMARY KEY CLUSTERED 
(
	[GrpTipo_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_DOCUMENTO_TIPO_GRUPO_URL]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_DOCUMENTO_TIPO_GRUPO_URL](
	[GrpTipo_Codigo] [char](3) NOT NULL,
	[Etapa_Codigo] [char](3) NOT NULL,
	[GrpTipo_URL] [varchar](150) NULL,
	[GrpTipo_URL_Validez] [varchar](150) NULL,
	[GrpTipo_URL_Estado_Envio] [varchar](150) NULL,
 CONSTRAINT [PK_FACTE_DOCUMENTO_TIPO_GRUPO_URL] PRIMARY KEY CLUSTERED 
(
	[GrpTipo_Codigo] ASC,
	[Etapa_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_EMPRESA]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_EMPRESA](
	[Emp_RUC] [char](11) NOT NULL,
	[Emp_NombreRazonSocial] [varchar](150) NULL,
	[Sunat_Usuario] [varchar](50) NULL,
	[Sunat_Contrasena] [varchar](50) NULL,
	[Sunat_Autorizacion] [varchar](50) NULL,
	[Emisor_Web_Visualizacion] [varchar](50) NULL,
	[Correo_Usuario] [varchar](50) NULL,
	[Correo_Contrasena] [varchar](50) NULL,
	[Correo_ServidorSMTP] [varchar](50) NULL,
	[Correo_ServidorPuerto] [varchar](6) NULL,
	[Correo_ServidorSSL] [bit] NULL,
	[Etapa_Codigo] [char](3) NULL,
	[Path_Root_App] [varchar](150) NULL,
	[FTP_Direccion] [varchar](50) NULL,
	[FTP_Carpeta] [varchar](200) NULL,
	[FTP_Usuario] [varchar](50) NULL,
	[FTP_Contrasena] [varchar](50) NULL,
	[ActualizarClientes] [bit] NULL,
	[Aud_Actualizacion_Fecha] [datetime] NULL,
	[Aud_Actualizacion_Autor] [varchar](50) NULL,
	[Aud_Actualizacion_Equipo] [varchar](50) NULL,
 CONSTRAINT [PK_FACTE_EMPRESA] PRIMARY KEY CLUSTERED 
(
	[Emp_RUC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_ESTADO_DOCUMENTO_ELECTRONICO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_ESTADO_DOCUMENTO_ELECTRONICO](
	[EstDoc_Codigo] [char](3) NOT NULL,
	[EstDoc_Descripcion] [varchar](60) NULL,
	[EstDoc_Codigo_Cliente] [varchar](4) NULL,
 CONSTRAINT [PK_FACTE_ESTADO_DOCUMENTO_ELECTRONICO] PRIMARY KEY CLUSTERED 
(
	[EstDoc_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_ESTADO_ENVIO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_ESTADO_ENVIO](
	[EstEnv_Codigo] [char](3) NOT NULL,
	[EstEnv_Descripcion] [varchar](60) NULL,
	[EstEnv_Codigo_Cliente] [varchar](4) NULL,
 CONSTRAINT [PK_FACTE_ESTADO_ENVIO] PRIMARY KEY CLUSTERED 
(
	[EstEnv_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_ESTADO_EXEPCION]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_ESTADO_EXEPCION](
	[Cod_Estado] [char](4) NULL,
	[Descripcion] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_ETAPA]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_ETAPA](
	[Etapa_Codigo] [char](3) NOT NULL,
	[Etapa_Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_FACTE_ETAPA] PRIMARY KEY CLUSTERED 
(
	[Etapa_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_MONEDA]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_MONEDA](
	[Mon_Codigo] [char](3) NOT NULL,
	[Mon_Descripcion] [varchar](50) NULL,
	[Mon_Simbolo] [varchar](4) NULL,
 CONSTRAINT [PK_FACTE_MONEDA] PRIMARY KEY CLUSTERED 
(
	[Mon_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_PROGRAMACIONES_ENVIO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_PROGRAMACIONES_ENVIO](
	[Emp_RUC] [char](11) NOT NULL,
	[Progra_Correlativo] [int] NOT NULL,
	[Progra_Descripcion] [varchar](100) NULL,
	[Progra_Habilitado] [bit] NULL,
	[Progra_Appli_TipoDoc] [char](2) NULL,
	[Progra_Appli_Filtra_Serie] [bit] NULL,
	[Progra_Appli_Serie] [char](4) NULL,
	[Progra_EsResumenBoletas] [bit] NULL,
	[Progra_EsComunicacionBaja] [bit] NULL,
	[Progra_FrecuEnvio_Hora_Es_Establecido] [bit] NULL,
	[Progra_FrecuEnvio_Hora_Es_XCadaHora] [bit] NULL,
	[Progra_FrecuEnvio_Hora_Es_DespuesEmitido] [bit] NULL,
	[Progra_FrecuEnvio_Hora_Es_DespuesConfirmado] [bit] NULL,
	[Progra_FrecuEnvio_Hora_Desde] [time](7) NULL,
	[Progra_FrecuEnvio_Hora_Hasta] [time](7) NULL,
	[Progra_FrecuEnvio_Hora_XCadaHora] [int] NULL,
	[Progra_Fecha_Desde] [date] NULL,
	[Progra_Fecha_SinFin] [bit] NULL,
	[Progra_Fecha_Hasta] [date] NULL,
	[Progra_Ejecucion_Ultimo] [datetime] NULL,
	[Progra_Estado] [char](3) NULL,
	[Aud_Registro_FechaHora] [datetime] NULL,
	[Aud_Registro_Equipo] [varchar](50) NULL,
	[Aud_Actualizacion_FechaHora] [datetime] NULL,
	[Aud_Actualizacion_Equipo] [varchar](50) NULL,
 CONSTRAINT [PK_FACTE_PROGRAMACIONES_ENVIO] PRIMARY KEY CLUSTERED 
(
	[Emp_RUC] ASC,
	[Progra_Correlativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_RECEPTORES_CORREO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_RECEPTORES_CORREO](
	[Receptor_TipoDocIdent_Codigo] [char](1) NULL,
	[Receptor_TipoDocIdent_Numero] [varchar](11) NOT NULL,
	[Receptor_Registro] [int] NOT NULL,
	[Receptor_Email] [varchar](50) NULL,
	[Receptor_Validado] [bit] NULL,
	[Receptor_CopiadoACliente] [bit] NULL,
	[Aud_Registro_Fecha] [datetime] NULL,
 CONSTRAINT [PK_FACTE_RECEPTORES_CORREO] PRIMARY KEY CLUSTERED 
(
	[Receptor_TipoDocIdent_Numero] ASC,
	[Receptor_Registro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_RESUMEN_DIARIO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_RESUMEN_DIARIO](
	[Doc_Emisor_RUC] [char](11) NOT NULL,
	[Doc_FechaGeneracion] [date] NOT NULL,
	[Doc_Resumen_Correlativo] [int] NOT NULL,
	[Doc_Fecha_Emision] [date] NULL,
	[Doc_NombreArchivoXML] [varchar](50) NULL,
	[Doc_SUNATTicket_ID] [varchar](50) NULL,
	[SUNAT_Respuesta_Codigo] [char](4) NULL,
	[EstEnv_Codigo] [char](3) NULL,
	[Doc_Estado] [char](4) NULL,
 CONSTRAINT [PK_FACTE_RESUMEN_DIARIO] PRIMARY KEY CLUSTERED 
(
	[Doc_Emisor_RUC] ASC,
	[Doc_FechaGeneracion] ASC,
	[Doc_Resumen_Correlativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_RESUMEN_DIARIO_DOCUMENTO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_RESUMEN_DIARIO_DOCUMENTO](
	[Doc_Emisor_RUC] [char](11) NOT NULL,
	[Doc_FechaGeneracion] [date] NOT NULL,
	[Doc_Resumen_Correlativo] [int] NOT NULL,
	[Doc_Item] [int] NOT NULL,
	[Doc_Tipo] [char](2) NULL,
	[Doc_Serie] [char](4) NULL,
	[Doc_Numero] [char](8) NULL,
	[Doc_Estado] [char](4) NULL,
	[Doc_Aud_Registro_FechaHora] [datetime] NULL,
	[Doc_Aud_Registro_Equipo] [varchar](50) NULL,
	[Doc_Aud_Aceptacion_FechaHoraCheck] [datetime] NULL,
	[Doc_Aud_Aceptacion_Equipo] [varchar](50) NULL,
	[Doc_CondicionItem] [char](1) NULL,
 CONSTRAINT [PK_FACTE_RESUMEN_DOCUMENTOS] PRIMARY KEY CLUSTERED 
(
	[Doc_Emisor_RUC] ASC,
	[Doc_FechaGeneracion] ASC,
	[Doc_Resumen_Correlativo] ASC,
	[Doc_Item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_SOCIO_NEGOCIO]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_SOCIO_NEGOCIO](
	[TipoDocIdent_Codigo] [char](1) NOT NULL,
	[SocNeg_TipoDocIdent_Numero] [varchar](15) NOT NULL,
	[SocNeg_RazonSocialNombres] [varchar](150) NULL,
	[SocNeg_Correo_Electronico] [varchar](150) NULL,
	[Replicacion_Estado] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_FACTE_SOCIO_NEGOCIO] PRIMARY KEY CLUSTERED 
(
	[TipoDocIdent_Codigo] ASC,
	[SocNeg_TipoDocIdent_Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_TABLA_OBSERVAC_ERRORES]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_TABLA_OBSERVAC_ERRORES](
	[ObsErr_Maestro] [char](3) NOT NULL,
	[ObsErr_Codigo] [char](4) NOT NULL,
	[ObsErr_Descripcion] [varchar](150) NULL,
 CONSTRAINT [PK_FACTE_TABLA_OBSERVAC_ERRORES] PRIMARY KEY CLUSTERED 
(
	[ObsErr_Maestro] ASC,
	[ObsErr_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACTE_TIPO_XML]    Script Date: 22/12/2020 21:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FACTE_TIPO_XML](
	[TipXML_Codigo] [char](3) NOT NULL,
	[TipXML_Descripcion] [varchar](60) NULL,
 CONSTRAINT [PK_FACTE_TIPO_XML] PRIMARY KEY CLUSTERED 
(
	[TipXML_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO] ADD  DEFAULT ((0)) FOR [Replicacion_Estado]
GO
ALTER TABLE [dbo].[FACTE_COMUNICACION_BAJA]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_COMUNICACION_BAJA_FACTE_EMPRESA] FOREIGN KEY([RsmBj_Emisor_RUC])
REFERENCES [dbo].[FACTE_EMPRESA] ([Emp_RUC])
GO
ALTER TABLE [dbo].[FACTE_COMUNICACION_BAJA] CHECK CONSTRAINT [FK_FACTE_COMUNICACION_BAJA_FACTE_EMPRESA]
GO
ALTER TABLE [dbo].[FACTE_COMUNICACION_BAJA_DOCUMENTO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_COMUNICACION_BAJA_DOCUMENTO_FACTE_RESUMEN_BAJA] FOREIGN KEY([RsmBj_Emisor_RUC], [RsmBj_GrpTipo_Codigo], [RsmBj_FechaGeneracion], [RsmBj_Resumen_Correlativo])
REFERENCES [dbo].[FACTE_COMUNICACION_BAJA] ([RsmBj_Emisor_RUC], [RsmBj_GrpTipo_Codigo], [RsmBj_FechaGeneracion], [RsmBj_Resumen_Correlativo])
GO
ALTER TABLE [dbo].[FACTE_COMUNICACION_BAJA_DOCUMENTO] CHECK CONSTRAINT [FK_FACTE_COMUNICACION_BAJA_DOCUMENTO_FACTE_RESUMEN_BAJA]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_DOCUMENTO_TIPO] FOREIGN KEY([Doc_Tipo])
REFERENCES [dbo].[FACTE_DOCUMENTO_TIPO] ([TipoDoc_Codigo])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_DOCUMENTO_TIPO]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_EMPRESA] FOREIGN KEY([Doc_Emisor_RUC])
REFERENCES [dbo].[FACTE_EMPRESA] ([Emp_RUC])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_EMPRESA]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_ESTADO_DOCUMENTO_ELECTRONICO] FOREIGN KEY([EstDoc_Codigo])
REFERENCES [dbo].[FACTE_ESTADO_DOCUMENTO_ELECTRONICO] ([EstDoc_Codigo])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_ESTADO_DOCUMENTO_ELECTRONICO]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_MONEDA] FOREIGN KEY([Doc_Moneda])
REFERENCES [dbo].[FACTE_MONEDA] ([Mon_Codigo])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_MONEDA]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_SOCIO_NEGOCIO] FOREIGN KEY([Doc_Cliente_TipoDocIdent_Codigo], [Doc_Cliente_TipoDocIdent_Numero])
REFERENCES [dbo].[FACTE_SOCIO_NEGOCIO] ([TipoDocIdent_Codigo], [SocNeg_TipoDocIdent_Numero])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_SOCIO_NEGOCIO]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_FACTE_DOCUMENTO_ELECTRONICO] FOREIGN KEY([Doc_Emisor_RUC], [Doc_Tipo], [Doc_Serie], [Doc_Numero])
REFERENCES [dbo].[FACTE_DOCUMENTO_ELECTRONICO] ([Doc_Emisor_RUC], [Doc_Tipo], [Doc_Serie], [Doc_Numero])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_FACTE_DOCUMENTO_ELECTRONICO]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]  WITH CHECK ADD  CONSTRAINT [FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_TIPO_XML] FOREIGN KEY([TipXML_Codigo])
REFERENCES [dbo].[FACTE_TIPO_XML] ([TipXML_Codigo])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] CHECK CONSTRAINT [FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_TIPO_XML]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_DOCUMENTO_ELECTRONICO] FOREIGN KEY([Doc_Emisor_RUC], [Doc_Tipo], [Doc_Serie], [Doc_Numero])
REFERENCES [dbo].[FACTE_DOCUMENTO_ELECTRONICO] ([Doc_Emisor_RUC], [Doc_Tipo], [Doc_Serie], [Doc_Numero])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_DOCUMENTO_ELECTRONICO]
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_ESTADO_ENVIO] FOREIGN KEY([EstEnv_Codigo])
REFERENCES [dbo].[FACTE_ESTADO_ENVIO] ([EstEnv_Codigo])
GO
ALTER TABLE [dbo].[FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE] CHECK CONSTRAINT [FK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_ESTADO_ENVIO]
GO
ALTER TABLE [dbo].[FACTE_EMPRESA]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_EMPRESA_FACTE_ETAPA] FOREIGN KEY([Etapa_Codigo])
REFERENCES [dbo].[FACTE_ETAPA] ([Etapa_Codigo])
GO
ALTER TABLE [dbo].[FACTE_EMPRESA] CHECK CONSTRAINT [FK_FACTE_EMPRESA_FACTE_ETAPA]
GO
ALTER TABLE [dbo].[FACTE_RESUMEN_DIARIO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_RESUMEN_DIARIO_FACTE_EMPRESA] FOREIGN KEY([Doc_Emisor_RUC])
REFERENCES [dbo].[FACTE_EMPRESA] ([Emp_RUC])
GO
ALTER TABLE [dbo].[FACTE_RESUMEN_DIARIO] CHECK CONSTRAINT [FK_FACTE_RESUMEN_DIARIO_FACTE_EMPRESA]
GO
ALTER TABLE [dbo].[FACTE_RESUMEN_DIARIO_DOCUMENTO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_RESUMEN_DIARIO_DOCUMENTO_FACTE_RESUMEN_DIARIO] FOREIGN KEY([Doc_Emisor_RUC], [Doc_FechaGeneracion], [Doc_Resumen_Correlativo])
REFERENCES [dbo].[FACTE_RESUMEN_DIARIO] ([Doc_Emisor_RUC], [Doc_FechaGeneracion], [Doc_Resumen_Correlativo])
GO
ALTER TABLE [dbo].[FACTE_RESUMEN_DIARIO_DOCUMENTO] CHECK CONSTRAINT [FK_FACTE_RESUMEN_DIARIO_DOCUMENTO_FACTE_RESUMEN_DIARIO]
GO
ALTER TABLE [dbo].[FACTE_SOCIO_NEGOCIO]  WITH CHECK ADD  CONSTRAINT [FK_FACTE_SOCIO_NEGOCIO_FACTE_DOCUMENTO_IDENTIFICACION_TIPO] FOREIGN KEY([TipoDocIdent_Codigo])
REFERENCES [dbo].[FACTE_DOCUMENTO_IDENTIFICACION_TIPO] ([TipoDocIdent_Codigo])
GO
ALTER TABLE [dbo].[FACTE_SOCIO_NEGOCIO] CHECK CONSTRAINT [FK_FACTE_SOCIO_NEGOCIO_FACTE_DOCUMENTO_IDENTIFICACION_TIPO]
GO
