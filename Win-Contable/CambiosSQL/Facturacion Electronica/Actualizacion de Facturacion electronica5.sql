 
SELECT * FROM CTB_CATALOGO_OPERACION_VENTA WHERE Id_Tipo='0032'
ALTER TABLE CTB_CATALOGO_OPERACION_VENTA
DROP CONSTRAINT PK_CTB_CATALOGO_OPERACION_VENTA;


ALTER TABLE CTB_CATALOGO_OPERACION_VENTA ALTER COLUMN Id_Tipo VARCHAR(4) NOT NULL

ALTER TABLE CTB_CATALOGO_OPERACION_VENTA
ADD CONSTRAINT PK_CTB_CATALOGO_OPERACION_VENTA PRIMARY KEY (Id_Empresa,Id_Tipo,Id_Catalogo);

-- para que es esot ?
ALTER PROC pa_CONSULTAR_SERVICIOS_PDV
(
@Id_Empresa CHAR(3),
@Invd_TipoBSA CHAR(4),
@Invd_Catalogo CHAR(10)=NULL,
@Codigo_Barra CHAR(200)=NULL
)
AS
BEGIN
	SELECT cata.Id_Catalogo,
			cata.Cat_Descripcion,
			ISNULL(cata.Cat_Serie,'') AS Cat_Serie,
			CAST( 0 AS DECIMAL)  AS Stock,
			ISNULL(cata.Acepta_Lote,0) AS Acepta_Lote,
			cata.Id_Tipo,
			cata.Id_Unidad_Medida ,
			uni.Und_Descripcion,
			ISNULL(uni.Und_Abreviado,'') AS Und_Abreviado,
			ISNULL(uni.Und_Codigo_FE,'') AS Und_Codigo_FE,
			ISNULL(mar.Mar_Descripcion,'') AS Mar_Descripcion
	FROM dbo.CTB_CATALOGO cata
		INNER JOIN dbo.CTB_UNIDAD_MEDIDA uni ON cata.Id_Unidad_Medida=uni.Id_Unidad_Medida
		LEFT JOIN dbo.CTB_MARCA mar ON mar.Id_Marca=cata.Id_Marca
	WHERE cata.Id_Empresa=@Id_Empresa AND
 		  cata.Id_Tipo=@Invd_TipoBSA AND
		  cata.Cat_Estado='0001' AND 
		(@Invd_Catalogo IS NULL OR cata.Id_Catalogo=@Invd_Catalogo)


END
GO

 

ALTER PROC VEN_MOVIMIENTO_DET_LISTAR_SP
(
@Id_Empresa CHAR(3),
@Id_Anio CHAR(4) ,
@Id_Periodo CHAR(2) = null,
@Id_Pdv_Codigo CHAR(2) = null,
@Id_Movimiento CHAR(10) = null
)
AS
BEGIN
 
 SELECT  
 DET.Id_Empresa, DET.Id_Anio, DET.Id_Periodo, DET.Id_Pdv_Codigo, DET.Id_Movimiento, DET.Adm_Item,
 DET.Adm_Tipo_BSA,tipbs.Gen_Codigo_Interno Tipo_BSA_Cod, tipbs.Gen_Descripcion_Det Tipo_BSA_Desc,
 DET.Adm_Catalogo, cat.Cat_Descripcion,
 ISNULL(DET.Adm_Almacen,'') Adm_Almacen,
 ISNULL(alm.Alm_Descrcipcion,'') Alm_Descrcipcion, DET.Adm_Cantidad, DET.Adm_Valor_Unit, DET.Adm_Total, 
 DET.Adm_Unm_Id,unm.Und_Descripcion,ISNULL(unm.Und_Codigo_FE,'') Und_Codigo_FE, 
 DET.Adm_Tiene_Lote,ISNULL(DET.Adm_Lote,'') AS Adm_Lote, DET.Adm_FechaFabricacion, DET.Adm_FechaVencimiento, 
 DET.Adm_CantidadAtendidaEnBaseAlCoefciente,
 ISNULL(DET.Ven_Tipo_Operacion ,'') AS Ven_Tipo_Operacion,
 ISNULL(tipop.Gen_Codigo_Interno,'') AS Ven_Tipo_Operacion_Cod,
 ISNULL(tipop.Gen_Descripcion_Det,'')  AS Ven_Tipo_Operacion_Desc,
 ISNULL(det.Ven_Afecto_IGV_FE,'') AS Ven_Afecto_IGV_FE,
 ISNULL(fe.AfecIGV_Descripcion,'') AS Ven_Afecto_IGV_FE_Desc,
 ISNULL(det.Adm_IgvTasa,0) AS Adm_IgvTasa,
 ISNULL(det.Adm_Valor_Venta,0) AS Adm_Valor_Venta
 
  FROM dbo.VEN_MOVIMIENTO_DET  DET

  INNER JOIN dbo.GNL_GENERAL_DET tipbs ON tipbs.Id_General_Det=det.Adm_Tipo_BSA
  INNER JOIN dbo.CTB_CATALOGO cat ON cat.Id_Empresa = DET.Id_Empresa AND cat.Id_Tipo=det.Adm_Tipo_BSA AND cat.Id_Catalogo=det.Adm_Catalogo
  left JOIN dbo.CTB_ALMACEN alm ON alm.Id_Empresa=det.Id_Empresa AND alm.Alm_TipoBSA = det.Adm_Tipo_BSA AND alm.Id_Almacen = det.Adm_Almacen
  INNER JOIN dbo.CTB_UNIDAD_MEDIDA unm ON unm.Id_Unidad_Medida=det.Adm_Unm_Id
  LEFT JOIN dbo.GNL_GENERAL_DET tipop ON tipop.Id_General_Det=det.Ven_Tipo_Operacion
  LEFT JOIN dbo.FACTE_CFG_TIPO_AFECTACION_IGV fe ON fe.AfecIGV_Codigo=det.Ven_Afecto_IGV_FE

WHERE  
	DET.Id_Empresa = @Id_Empresa  AND
	DET.Id_Anio = @Id_Anio AND
	(@Id_Periodo IS NULL OR  DET.Id_Periodo = @Id_Periodo) AND
	(@Id_Pdv_Codigo IS NULL OR DET.Id_Pdv_Codigo = @Id_Pdv_Codigo) AND
	(@Id_Movimiento IS NULL OR DET.Id_Movimiento = @Id_Movimiento )

END

GO


ALTER PROC pa_CTB_CATALOGO_ELIMINAR
(
@Id_Empresa CHAR(3), 
@Id_Grupo CHAR(2), 
@Id_Familia CHAR(2), 
@Id_Tipo CHAR(4), 
@Id_Catalogo CHAR(10),
@Usuario VARCHAR(50), 
@Maquina VARCHAR(50)
)
AS
BEGIN
 DECLARE @Mensaje varchar(400)  
 DECLARE @Cant int  

 SET @Cant = (SELECT COUNT(*) FROM dbo.CTB_MOVIMIENTO_INVENTARIO_DET det WITH(NOLOCK)  
				INNER JOIN dbo.CTB_MOVIMIENTO_INVENTARIO_CAB cab ON cab.Id_Empresa = det.Id_Empresa AND cab.Id_Anio = det.Id_Anio AND cab.Id_Periodo = det.Id_Periodo AND cab.Id_Tipo_Mov = det.Id_Tipo_Mov AND cab.Id_Almacen = det.Id_Almacen AND cab.Id_Movimiento = det.Id_Movimiento
 WHERE det.Id_Empresa=@Id_empresa AND det.Invd_TipoBSA=@Id_Tipo AND DET.Invd_Catalogo=@Id_Catalogo  AND cab.Inv_Estado='0001');   

 --IF ISNULL(@Cant, 0) > 0 BEGIN  
	--	SET @Mensaje = 'No se puede ELIMINAR ' + char(13) + 'Existe(n) ' + LTRIM(STR(@Cant)) + ' registros en inventario.'  
	--	GOTO tagError   
	--	tagError:  
	--	RAISERROR (@Mensaje, 11, 1 ) WITH NOWAIT  
 --END  	
   --IF ISNULL(@Cant, 0) = 0 BEGIN 

	UPDATE dbo.CTB_CATALOGO
	SET	Cat_Estado='0003'	
	WHERE Id_Empresa=@Id_Empresa AND	
		  Id_Tipo=@Id_Tipo AND	
		  Id_Catalogo=@Id_Catalogo
	--END	
END



GO


alter PROC pa_VEN_LISTAR_PRODUCTO_CON_PRECIO_DET
(
@Id_Empresa CHAR(3), 
@Id_Catalogo CHAR(10)=NULL,
@Est_Codigo varchar(2)=null
)
AS
BEGIN

	SELECT 
		DET.Id_Empresa, DET.Est_Codigo, DET.Id_Tipo, DET.Id_Catalogo, DET.Pre_Correlativo, 
		DET.Pre_Id_Unm, unm.Und_Descripcion,
		DET.Pre_Id_Mod_Venta, Tip.Gen_Codigo_Interno,Tip.Gen_Descripcion_Det,
		DET.Pre_Moneda_cod,mo.Id_Sunat,mo.Nombre_Moneda,
		DET.Pre_Fecha_Ini, DET.Pre_Fecha_Fin, DET.Pres_Es_Variable, 
		ISNULL(DET.Pre_Minimo,0) AS Pre_Minimo , ISNULL(DET.Pre_Maximo,0) AS Pre_Maximo,
		ISNULL(det.Pre_Requiere_Codigo_Autoriza,0) AS Pre_Requiere_Codigo_Autoriza,
		ISNULL(det.Pre_Codigo_Autoriza,'') AS Pre_Codigo_Autoriza
	FROM [dbo].[VEN_LISTA_PRECIO_DET] DET 
	INNER JOIN dbo.CTB_UNIDAD_MEDIDA unm ON det.Pre_Id_Unm=unm.Id_Unidad_Medida
	INNER JOIN dbo.GNL_GENERAL_DET Tip ON tip.Id_General_Det=det.Pre_Id_Mod_Venta
	INNER JOIN dbo.CTB_MONEDA mo ON mo.Id_Moneda=det.Pre_Moneda_cod

	WHERE DET.Id_Empresa=@Id_Empresa AND (@Id_Catalogo IS NULL OR det.Id_Catalogo=@Id_Catalogo)
	and (@Est_Codigo is null or det.Est_Codigo=@Est_Codigo)
END
go
