 ALTER TABLE dbo.VEN_MOVIMIENTO_CAB
ADD Amortizado DECIMAL(18,4);


 ALTER TABLE dbo.CTB_MOVIMIENTO_INVENTARIO_CAB
ADD  Amortizado DECIMAL(18,4);

 ALTER TABLE dbo.CTB_MOVIMIENTO_CONTABLE_CAB
ADD Amortizado DECIMAL(18,4);




ALTER PROC pa_PENDIENTES_POR_COBRAR
(
@Id_Empresa CHAR(3) 
)
AS
BEGIN
	SELECT cab.Id_Empresa, cab.Id_Anio, cab.Id_Periodo, 
		   cab.Id_Tipo_Mov, 
		   cab.Id_Almacen, 
	       cab.Id_Movimiento, 
 		   ISNULL(cab.Inv_pdv_codigo,'') AS Inv_pdv_codigo,
		   ISNULL(LTRIM(RTRIM(pdv.Pdv_Nombre)),'') AS Pdv_Nombre,
		   ISNULL(cab.Inv_Condicion,'') AS Inv_Condicion
		   ,ISNULL(condi.Gen_Codigo_Interno,'') Inv_Condicion_Interno
		   ,ISNULL(Condi.Gen_Descripcion_Det,'') AS Inv_Condicion_Desc   ,
		   ISNULL(cab.Inv_Ruc_Dni,'') AS Inv_Ruc_Dni, ISNULL(Enti.Ent_Razon_Social_Nombre,'') +' ' + ISNULL(Enti.Ent_Ape_Paterno,'') +' '+ ISNULL(Enti.Ent_Ape_Materno,'') AS Entidad, 
		   cab.Inv_Tipo_Doc,TipDoc.Nombre_Comprobante, cab.Inv_Serie, Inv_Numero, 
		   cab.Inv_Fecha, 
 		   ISNULL(cab.Inv_Es_Venta_Sin_Ticket,0) AS Inv_Es_Venta_Sin_Ticket,
		   ISNULL(cab.Inv_Base,0) AS Inv_Base,
		   ISNULL(cab.Inv_Igv,0) AS Inv_Igv,
		   ISNULL(cab.Inv_Total,0) AS Inv_Total,
		   ISNULL(cab.Cja_Codigo,'') AS Cja_Codigo,
		   ISNULL(cab.Cja_Aper_Cierre_Codigo,0) AS Cja_Aper_Cierre_Codigo,
		   ISNULL(cab.Cja_Aper_Cierre_Codigo_Item,0) AS Cja_Aper_Cierre_Codigo_Item,
		   ISNULL(cab.cja_Estado,'') AS cja_Estado ,
		   '',''
		   ,ISNULL(cab.Amortizado,0) AS Amortizado

	FROM dbo.CTB_MOVIMIENTO_INVENTARIO_CAB cab WITH(NOLOCK)  
 
	INNER JOIN dbo.GNL_ENTIDAD Enti WITH(NOLOCK) ON enti.Ent_RUC_DNI=cab.Inv_Ruc_Dni
	INNER JOIN dbo.CTB_COMPROBANTE TipDoc WITH(NOLOCK) ON TipDoc.Id_Comprobante=cab.Inv_Tipo_Doc
 	LEFT JOIN dbo.GNL_ENTIDAD EntiRes WITH(NOLOCK) ON EntiRes.Ent_RUC_DNI=cab.Inv_Responsable_Ruc_Dni
	 
	LEFT JOIN dbo.VEN_PUNTO_VENTA pdv WITH(NOLOCK) ON pdv.Id_Empresa=cab.Id_Empresa AND pdv.Pdv_Codigo=cab.Inv_pdv_codigo
	INNER JOIN dbo.GNL_GENERAL_DET est WITH(NOLOCK) ON est.Id_General_Det=cab.Inv_Estado
	--INNER JOIN dbo.GNL_PLANTILLA_PERIODO per WITH(NOLOCK) ON per.Id_Periodo = cab.Id_Periodo
	LEFT JOIN dbo.GNL_GENERAL_DET Condi WITH(NOLOCK) ON Condi.Id_General_Det=cab.Inv_Condicion
	WHERE cab.Id_Empresa=@Id_Empresa AND	 
		cab.Inv_Estado = '0001' AND
		ISNULL(cab.Inv_Es_Venta_Sin_Ticket,0)=1 
		AND Id_Tipo_Operacion ='01' -- Solo ventas
		AND cab.Id_Tipo_Mov='0035'
		AND cab.Inv_Condicion='0007' --credito
		AND cab.Inv_Est_Facturacion IN ('0585','0586')
 
	UNION ALL 
SELECT  
		cab.Id_Empresa, cab.Id_Anio, cab.Id_Periodo, 
		'' Id_Tipo_Mov,
		'' Id_Almacen,
		cab.Id_Movimiento,
		cab.Id_Pdv_Codigo,pdv.Pdv_Nombre,  
		cab.Ven_Condicion_Cod ,condi.Gen_Codigo_Interno,condi.Gen_Descripcion_Det,
		cab.Ven_Ruc_dni , ISNULL(enti.Ent_Razon_Social_Nombre,'') +' '+ ISNULL(enti.Ent_Ape_Paterno,'') +' '+ ISNULL(enti.Ent_Ape_Materno,'') AS Entidad,
		cab.Ven_Tipo_Doc, doc.Nombre_Comprobante, cab.Ven_Serie, cab.Ven_Numero,
		cab.Ven_Fecha_Movimiento, 
		CAST( 0  AS BIT ) ,
		cab.Ven_Base_Imponible, cab.Ven_Igv, 
		cab.Ven_Importe_Total, 
		cab.Cja_Codigo, cab.Cja_Aper_Cierre_Codigo, 
		cab.Cja_Aper_Cierre_Codigo_Item, cab.Cja_Estado,
		cab.Id_Libro, cab.Id_Voucher
		  ,ISNULL(cab.Amortizado,0) AS Amortizado
 FROM dbo.VEN_MOVIMIENTO_CAB cab
 
INNER JOIN dbo.CTB_COMPROBANTE doc ON doc.Id_Comprobante=cab.Ven_Tipo_Doc
INNER JOIN dbo.VEN_PUNTO_VENTA pdv ON pdv.Id_Empresa=cab.Id_Empresa AND pdv.Pdv_Codigo=cab.Id_Pdv_Codigo
--INNER JOIN dbo.CTB_MONEDA mon ON mon.Id_Moneda=cab.Ven_Moneda_Cod
INNER JOIN dbo.GNL_ENTIDAD enti ON enti.Ent_RUC_DNI=cab.Ven_Ruc_dni
--INNER JOIN dbo.GNL_GENERAL_DET TipoPerDoc ON enti.Ent_Tipo_Doc=TipoPerDoc.Id_General_Det
INNER JOIN dbo.GNL_GENERAL_DET condi ON condi.Id_General_Det=cab.Ven_Condicion_Cod
--INNER JOIN dbo.CTB_ANALISIS_CONTABLE_CAB ana ON ana.Id_Empresa=cab.Id_Empresa AND ana.Id_Anio=cab.Id_Anio AND ana.Id_Analisis=cab.Ven_Analisis
--INNER JOIN dbo.GNL_PLANTILLA_PERIODO per ON per.Id_Periodo = cab.Id_Periodo
--INNER JOIN dbo.GNL_GENERAL_DET est ON est.Id_General_Det=cab.Ven_Estado
INNER JOIN dbo.GNL_GENERAL_DET caja ON caja.Id_General_Det=cab.Cja_Estado
 

WHERE cab.Ven_Estado<>'0003' AND
cab.Id_Empresa = @Id_Empresa   
 AND cab.Ven_Condicion_Cod='0007'
 AND cab.Est_Facturacion IN ('0585','0586')
ORDER BY cab.Id_Periodo DESC,cab.Id_Movimiento DESC 


END
GO



ALTER PROC pa_VEN_MOVIMIENTO_CAJA_DET_INSERTAR
(
@Id_Empresa CHAR(3), 
@Pdv_Codigo CHAR(2), 
@Cja_Codigo CHAR(3), 
@Cja_Aper_Cierre_Codigo INT, 
@Cja_Movimiento_Item INT, 
@Item INT, 
@Anio_Ref CHAR(4)	, 
@Periodo_Ref CHAR(2), 
@Id_Tipo_Mov_Ref CHAR(4), 
@Id_Almacen_Ref CHAR(2), 
@Id_Movimiento_Ref CHAR(10), 
@Es_Venta_Sin_Ticket BIT,
@Ruc_DNI VARCHAR(15) NULL,
@Total DECIMAL(18,4) NULL,
@TipoDoc VARCHAR(3) NULL,
@Serie VARCHAR(4) NULL,
@Numero VARCHAR(8) NULL,
@Maquina VARCHAR(50),
@Usuario VARCHAR(50)
)
AS
BEGIN
	
 

	SET @Cja_Movimiento_Item= (SELECT TOP 1 Cja_Movimiento_Item  FROM dbo.VEN_MOVIMIENTO_CAJA m 
							  WHERE m.Id_Empresa=@Id_Empresa AND 
									m.Pdv_Codigo=@Pdv_Codigo AND 
									m.Cja_Codigo=@Cja_Codigo AND
									m.Cja_Aper_Cierre_Codigo=@Cja_Aper_Cierre_Codigo 
									ORDER BY Cja_Movimiento_Item desc)


 


INSERT INTO dbo.VEN_MOVIMIENTO_CAJA_DET (
Id_Empresa ,Pdv_Codigo ,  Cja_Codigo ,  Cja_Aper_Cierre_Codigo ,  Cja_Movimiento_Item ,
Item ,Anio_Ref ,Periodo_Ref ,Id_Tipo_Mov_Ref , Id_Almacen_Ref ,Id_Movimiento_Ref ,  Es_Venta_Sin_Ticket,
Ruc_DNI ,Total  ,TipoDoc ,Serie  ,Numero 
 )
VALUES ( @Id_Empresa ,@Pdv_Codigo ,@Cja_Codigo ,@Cja_Aper_Cierre_Codigo ,@Cja_Movimiento_Item ,
@Item ,@Anio_Ref ,@Periodo_Ref ,@Id_Tipo_Mov_Ref ,@Id_Almacen_Ref ,@Id_Movimiento_Ref ,@Es_Venta_Sin_Ticket,
@Ruc_DNI ,@Total,@TipoDoc ,@Serie  ,@Numero  
    
)


IF (@Es_Venta_Sin_Ticket = 0) BEGIN

	UPDATE dbo.VEN_MOVIMIENTO_CAB
	SET  Amortizado= ISNULL(Amortizado,0) + @Total
	WHERE  
	Id_Empresa = @Id_Empresa  AND
	Id_Anio = @Anio_Ref AND
	Id_Periodo = @Periodo_Ref  AND
	Id_Pdv_Codigo = @Pdv_Codigo  AND
	Id_Movimiento = @Id_Movimiento_Ref  

	UPDATE dbo.VEN_MOVIMIENTO_CAB
	SET Est_Facturacion= CASE WHEN Ven_Importe_Total = Amortizado THEN  '0587' ELSE '0586' END 
	    
	WHERE  
	Id_Empresa = @Id_Empresa  AND
	Id_Anio = @Anio_Ref AND
	Id_Periodo = @Periodo_Ref  AND
	Id_Pdv_Codigo = @Pdv_Codigo  AND
	Id_Movimiento = @Id_Movimiento_Ref 


	DECLARE	@Id_Libro CHAR(3), @Id_Voucher CHAR(10)

	SELECT @Id_Libro=Id_Libro ,@Id_Voucher=Id_Voucher from dbo.VEN_MOVIMIENTO_CAB
	WHERE  
		Id_Empresa = @Id_Empresa  AND
		Id_Anio = @Anio_Ref AND
		Id_Periodo = @Periodo_Ref AND
		Id_Pdv_Codigo = @Pdv_Codigo AND
		Id_Movimiento = @Id_Movimiento_Ref

	UPDATE dbo.CTB_MOVIMIENTO_CONTABLE_CAB 
	SET	  Amortizado= ISNULL(Amortizado,0) + @Total
	WHERE Id_Empresa=@Id_Empresa AND
	      Id_Anio=@Anio_Ref AND
		  Id_Periodo=@Periodo_Ref AND	
		  Id_Libro=@Id_Libro AND
		  Id_Voucher=@Id_Voucher

	UPDATE dbo.CTB_MOVIMIENTO_CONTABLE_CAB 
	SET	Est_Facturacion= CASE WHEN Ctb_Importe_Total = Amortizado THEN  '0587' ELSE '0586' END 
	WHERE Id_Empresa=@Id_Empresa AND
	      Id_Anio=@Anio_Ref AND
		  Id_Periodo=@Periodo_Ref AND	
		  Id_Libro=@Id_Libro AND
		  Id_Voucher=@Id_Voucher

		  --ACTUALIZAR EL ESTADO DE FACTURACION EN INVENTARIO TAMBIEN
   DECLARE @Inv_Empresa CHAR(4),@Inv_Anio CHAR(4),@Inv_Periodo CHAR(2), @Inv_Tipo_mov CHAR(4),@Id_Almacen CHAR(2),@Inv_Id_Movimiento CHAR(10);

	SELECT @Inv_Empresa=DocRf.Id_Empresa ,
	       @Inv_Anio=DocRf.Id_Anio,
		   @Inv_Periodo=DocRf.Id_Periodo,
		   @Inv_Tipo_mov=DocRf.Id_Tipo_Mov,
		   @Id_Almacen=DocRf.Id_Almacen,
		   @Inv_Id_Movimiento=DocRf.Id_Movimiento
	FROM dbo.CTB_MOVIMIENTO_INVENTARIO_CAB DocRf WITH(NOLOCK)
	WHERE DocRf.Id_Empresa=@Id_Empresa AND DocRf.Id_Anio=@Anio_Ref AND 
		  DocRf.Id_Periodo=@Periodo_Ref AND DocRf.Inv_Voucher=@Id_Voucher 

 

	UPDATE dbo.CTB_MOVIMIENTO_INVENTARIO_CAB 
		SET   Amortizado=  ISNULL(Amortizado,0) + @Total
		    ,Fecha_Modi=GETDATE()
	WHERE Id_Empresa=@Inv_Empresa AND 
	      Id_Anio=@Inv_Anio AND
		  Id_Periodo= @Inv_Periodo AND 
		  Id_Tipo_Mov=@Inv_Tipo_mov AND 
		  Id_Almacen=@Id_Almacen AND 
	      Id_Movimiento=@Inv_Id_Movimiento ;


	UPDATE dbo.CTB_MOVIMIENTO_INVENTARIO_CAB 
		SET Inv_Est_Facturacion=CASE WHEN Inv_Total = Amortizado THEN  '0587' ELSE '0586' END 
	WHERE Id_Empresa=@Inv_Empresa AND 
	      Id_Anio=@Inv_Anio AND
		  Id_Periodo= @Inv_Periodo AND 
		  Id_Tipo_Mov=@Inv_Tipo_mov AND 
		  Id_Almacen=@Id_Almacen AND 
	      Id_Movimiento=@Inv_Id_Movimiento ;




END
ELSE BEGIN

		UPDATE dbo.CTB_MOVIMIENTO_INVENTARIO_CAB 
	SET 
	   Amortizado= ISNULL(Amortizado,0) + @Total,
		Maquina_Modi=@Maquina,
		Usuario_Modi=@Usuario,
		Fecha_Modi=GETDATE()
		
	WHERE Id_Empresa=@Id_Empresa AND
	      Id_Anio=@Anio_Ref AND
		  Id_Periodo=@Periodo_Ref AND	
		  Id_Tipo_Mov=@Id_Tipo_Mov_Ref AND
		  Id_Almacen=@Id_Almacen_Ref AND
		  Id_Movimiento=@Id_Movimiento_Ref

	UPDATE dbo.CTB_MOVIMIENTO_INVENTARIO_CAB 
	SET	Inv_Est_Facturacion=CASE WHEN Inv_Total = Amortizado THEN  '0587' ELSE '0586' END 
	WHERE Id_Empresa=@Id_Empresa AND
	      Id_Anio=@Anio_Ref AND
		  Id_Periodo=@Periodo_Ref AND	
		  Id_Tipo_Mov=@Id_Tipo_Mov_Ref AND
		  Id_Almacen=@Id_Almacen_Ref AND
		  Id_Movimiento=@Id_Movimiento_Ref


END

END

GO






