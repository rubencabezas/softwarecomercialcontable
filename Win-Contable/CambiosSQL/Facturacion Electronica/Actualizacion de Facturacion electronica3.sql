--SELECT * FROM VEN_MOVIMIENTO_CAJA;
--SELECT * FROM dbo.VEN_MOVIMIENTO_CAJA_DET

--EXEC pa_OTROS_INGRESOS_CORTE '001','01','001',264 ,1


ALTER PROC pa_OTROS_INGRESOS_CORTE
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL

)
AS
BEGIN
SELECT ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(Cja_Monto,0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Movimiento_Item=@Cja_Aper_Cierre_Codigo_Item and
Cja_Tipo='0049' --INGRESOS

END
GO


ALTER PROC pa_OTROS_EGRESOS_CORTE
(
@EmpresaCodigo CHAR(3),
@PdvCodigo CHAR(2),
@Cja_Codigo CHAR(3),
@Cja_Codigo_Apertura INT ,
@Cja_Aper_Cierre_Codigo_Item INT =NULL
)
AS
BEGIN
SELECT ISNULL(Cja_Motivo,'') AS Motivo ,	
	   ISNULL(Cja_Monto,0) AS Monto

FROM dbo.VEN_MOVIMIENTO_CAJA WHERE 
Id_Empresa=@EmpresaCodigo AND
Pdv_Codigo=@PdvCodigo AND
Cja_Codigo=@Cja_Codigo AND
Cja_Aper_Cierre_Codigo=@Cja_Codigo_Apertura AND
Cja_Movimiento_Item=@Cja_Aper_Cierre_Codigo_Item and
Cja_Tipo='0050' --EGRESOS

END

GO

ALTER PROC CTB_MOVIMIENTO_INVENTARIO_CAB_LISTAR_VENTAS_SIN_TICKET
(
@Id_Empresa CHAR(3), 
@Id_Anio CHAR(4), 
@Id_Periodo CHAR(2)=NULL, 
@Id_Tipo_Mov CHAR(4)=NULL, 
@Id_Almacen CHAR(2)=NULL, 
@Id_Movimiento CHAR(10)=NULL
--,@Inv_Es_Venta_Sin_Ticket BIT =NULL,
--@Id_Tipo_Operacion CHAR(2)=NULL
)
AS
BEGIN
	SELECT cab.Id_Empresa, cab.Id_Anio, cab.Id_Periodo, 
		   cab.Id_Tipo_Mov,tipmov.Gen_Codigo_Interno,TipMov.Gen_Descripcion_Det, 
		   cab.Id_Almacen,al.Alm_Descrcipcion, 
	       cab.Id_Movimiento, cab.Id_Tipo_Operacion,oper.Top_Descripcion, 
		   ISNULL(cab.Inv_Tipo_Entidad,''),ISNULL(TipEnt.Ent_Descripcion,'') AS Ent_Descripcion, 
		   ISNULL(cab.Inv_Ruc_Dni,''), ISNULL(Enti.Ent_Razon_Social_Nombre,'') +' ' + ISNULL(Enti.Ent_Ape_Paterno,'') +' '+ ISNULL(Enti.Ent_Ape_Materno,'') AS Entidad, 
		   cab.Inv_Tipo_Doc,TipDoc.Nombre_Comprobante, cab.Inv_Serie, Inv_Numero, 
		   cab.Inv_Fecha, 
		   ISNULL(cab.Inv_Almacen_O_D,'') AS Inv_Almacen_O_D,ISNULL(aldo.Alm_Descrcipcion,'') AS Alm_Descrcipcion, 
		  ISNULL(cab.Inv_Responsable_Ruc_Dni,'') AS Inv_Responsable_Ruc_Dni,
		   ISNULL(EntiRes.Ent_Razon_Social_Nombre +' '+ EntiRes.Ent_Ape_Paterno +' '+ EntiRes.Ent_Ape_Materno,'')  AS Responsable, 
		   ISNULL(cab.Inv_Libro,'') AS Inv_Libro,ISNULL(cab.Inv_Voucher,'') AS Inv_Voucher,
		   ISNULL(cab.Inv_Glosa,'') AS Inv_Glosa,
		   ISNULL(cab.Inv_Es_Venta_Sin_Ticket,0) AS Inv_Es_Venta_Sin_Ticket,
		   ISNULL(cab.Inv_Base,0) AS Inv_Base,
		   ISNULL(cab.Inv_Igv,0) AS Inv_Igv,
		   ISNULL(cab.Inv_Total,0) AS Inv_Total,
		   ISNULL(cab.Cja_Codigo,'') AS Cja_Codigo,
		   ISNULL(cab.Cja_Aper_Cierre_Codigo,0) AS Cja_Aper_Cierre_Codigo,
		   ISNULL(cab.Cja_Aper_Cierre_Codigo_Item,0) AS Cja_Aper_Cierre_Codigo_Item,
		   ISNULL(cab.cja_Estado,'') AS cja_Estado,
		   ISNULL(LTRIM(RTRIM(pdv.Pdv_Nombre)),'') AS Pdv_Nombre,
		   ISNULL(est.Gen_Descripcion_Det,'') AS Estado_Fila,
		   ISNULL(cab.Inv_pdv_codigo,'') AS Inv_pdv_codigo ,
		   ISNULL(cab.Es_Atendido_Traspaso,0) AS Es_Atendido_Traspaso,
		   ISNULL(cab.Inv_Generado_En_Compra,0) AS Inv_Generado_En_Compra,
		   ISNULL(per.Descripcion_Periodo,'') AS Descripcion_Periodo,
		   ISNULL(cab.Inv_Condicion,'') AS Inv_Condicion
		   ,ISNULL(condi.Gen_Codigo_Interno,'') Inv_Condicion_Interno
		   ,ISNULL(Condi.Gen_Descripcion_Det,'') AS Inv_Condicion_Desc
		   ,ISNULL(cab.Inv_Est_Facturacion,'') AS Inv_Est_Facturacion
		   ,ISNULL(EstFac.Gen_Codigo_Interno,'') AS Inv_Est_Facturacion_Interno
		   ,ISNULL(EstFac.Gen_Descripcion_Det,'') AS Inv_Est_Facturacion_Desc,
		   ISNULL(cab.Usuario_Crea,'') AS Vendedor
		   ,ISNULL(cab.Fecha_Crea,'01/01/1900') AS fechaOperacion
	FROM dbo.CTB_MOVIMIENTO_INVENTARIO_CAB cab WITH(NOLOCK)  
	INNER JOIN dbo.GNL_GENERAL_DET TipMov WITH(NOLOCK) ON TipMov.Id_General_Det=cab.Id_Tipo_Mov 
	INNER JOIN dbo.CTB_ALMACEN al WITH(NOLOCK) ON al.Id_Empresa=cab.Id_Empresa AND al.Id_Almacen=cab.Id_Almacen
	INNER JOIN dbo.CTB_TIPO_OPERACION oper WITH(NOLOCK) ON oper.Id_Operacion=cab.Id_Tipo_Operacion 
	INNER JOIN dbo.GNL_ENTIDAD Enti WITH(NOLOCK) ON enti.Ent_RUC_DNI=cab.Inv_Ruc_Dni
	INNER JOIN dbo.CTB_COMPROBANTE TipDoc WITH(NOLOCK) ON TipDoc.Id_Comprobante=cab.Inv_Tipo_Doc
	LEFT JOIN dbo.CTB_ALMACEN AlDO WITH(NOLOCK) ON aldo.Id_Empresa=cab.Id_Empresa AND aldo.Id_Almacen=cab.Inv_Almacen_O_D
	LEFT JOIN dbo.GNL_ENTIDAD EntiRes WITH(NOLOCK) ON EntiRes.Ent_RUC_DNI=cab.Inv_Responsable_Ruc_Dni
	LEFT JOIN dbo.GNL_TIPO_ENTIDAD TipEnt WITH(NOLOCK) ON TipEnt.Id_Tipo_Ent=cab.Inv_Tipo_Entidad
	LEFT JOIN dbo.VEN_PUNTO_VENTA pdv WITH(NOLOCK) ON pdv.Id_Empresa=cab.Id_Empresa AND pdv.Pdv_Codigo=cab.Inv_pdv_codigo
	INNER JOIN dbo.GNL_GENERAL_DET est WITH(NOLOCK) ON est.Id_General_Det=cab.Inv_Estado
	INNER JOIN dbo.GNL_PLANTILLA_PERIODO per WITH(NOLOCK) ON per.Id_Periodo = cab.Id_Periodo
	LEFT JOIN dbo.GNL_GENERAL_DET Condi WITH(NOLOCK) ON Condi.Id_General_Det=cab.Inv_Condicion
	LEFT JOIN dbo.GNL_GENERAL_DET EstFac WITH(NOLOCK) ON EstFac.Id_General_Det=cab.Inv_Est_Facturacion
	WHERE cab.Id_Empresa=@Id_Empresa AND	
	      cab.Id_Anio=@Id_Anio AND
         (@Id_Periodo IS NULL OR cab.Id_Periodo=@Id_Periodo) AND
         (@Id_Tipo_Mov IS NULL OR cab.Id_Tipo_Mov=@Id_Tipo_Mov )AND
         (@Id_Almacen IS NULL OR cab.Id_Almacen=@Id_Almacen) AND
         (@Id_Movimiento IS NULL OR cab.Id_Movimiento=@Id_Movimiento) AND	
		  cab.Inv_Estado <> '0003' AND
          ISNULL(cab.Inv_Es_Venta_Sin_Ticket,0)=1 
		  AND Id_Tipo_Operacion ='01' -- Solo ventas
		  --AND (@Id_Tipo_Operacion IS NULL OR Id_Tipo_Operacion=@Id_Tipo_Operacion)
		  
	ORDER BY cab.Inv_Numero DESC	
END



GO


