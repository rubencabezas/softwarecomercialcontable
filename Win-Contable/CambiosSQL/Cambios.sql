


create PROC pa_CTB_MOVIMIENTO_CONTABLE_DET_INSERTAR
(
@Id_Empresa CHAR(3), 
@Id_Anio CHAR(4), 
@Id_Periodo CHAR(2), 
@Id_Libro CHAR(3), 
@Id_Voucher CHAR(10), 
@Id_Item INT, 
@Ctb_Cuenta CHAR(10), 
@Ctb_Operacion_Cod CHAR(4)=NULL, 
@Ctb_Igv_Cod CHAR(4)=NULL, 
@Ctb_Centro_CG CHAR(3)=NULL, 
@Ctb_Tipo_DH CHAR(4), 
@Ctb_Importe_Debe DECIMAL (18,4),  
@Ctb_Importe_Haber DECIMAL (18,4), 
@Ctb_Tipo_BSA CHAR(4)=NULL,
@Ctb_Catalogo CHAR(10)=NULL,
@Ctb_Valor_unit DECIMAL(18,4) = NULL,
@Ctb_Cantidad DECIMAL(18,4) = NULL,
@Ctb_Importe_Debe_Extr DECIMAL(18,4) = NULL,
@Ctb_Importe_Haber_Extr DECIMAL(18,4) =NULL,
@Ctb_Almacen CHAR(2)=NULL,
@Ctb_Tipo_Doc_det CHAR(3)=NULL,
@Ctb_Serie_det CHAR(4)=NULL,
@Ctb_Numero_det CHAR(8)=NULL,
@Ctb_Fecha_Mov_det DATE=NULL,
@Ctb_Tipo_Ent_det CHAR(1)=NULL,
@Ctb_Ruc_dni_det CHAR(11)=NULL,
@Ctb_moneda_cod_det CHAR(3)=NULL,
@Ctb_Tipo_Cambio_Cod_Det CHAR(3)=NULL,
@Ctb_Tipo_Cambio_Desc_Det VARCHAR(50)=NULL,
@Ctb_Tipo_Cambio_Valor_Det DECIMAL(10,4)=NULL,
@Usuario VARCHAR(50), 
@Maquina VARCHAR(50),
@Ctb_Cta_Destino bit =NULL,
@Ctb_Patri_Neto_Cod CHAR(3)=NULL,
@Ctb_Estado_Flujo_Efecctivo_Cod_Det CHAR(6)=NULL
 
)
AS
BEGIN


		INSERT INTO dbo.CTB_MOVIMIENTO_CONTABLE_DET
		        ( Id_Empresa ,
		          Id_Anio ,
		          Id_Periodo ,
		          Id_Libro ,
		          Id_Voucher ,
		          Id_Item ,
		          Ctb_Cuenta ,
		          Ctb_Operacion_Cod ,
		          Ctb_Igv_Cod ,
		          Ctb_Centro_CG ,
		          Ctb_Tipo_DH ,
		          Ctb_Importe_Debe ,
		          Ctb_Importe_Haber ,
				  Ctb_Tipo_BSA,
				  Ctb_Catalogo,
				  Ctb_Valor_unit ,
				  Ctb_Cantidad,
                  Ctb_Importe_Debe_Extr ,
                  Ctb_Importe_Haber_Extr ,
				Ctb_Almacen,
				Ctb_Tipo_Doc_det,
				Ctb_Serie_det,
				Ctb_Numero_det,
				Ctb_Fecha_Mov_det,
				Ctb_Tipo_Ent_det,
				Ctb_Ruc_dni_det,
				Ctb_moneda_cod_det,
				Ctb_Tipo_Cambio_Cod_Det,
				Ctb_Tipo_Cambio_Desc_Det,
				Ctb_Tipo_Cambio_Valor_Det,
		          Ctb_Estado ,
		          Usuario_Crea ,
		          Fecha_Crea ,
		          Maquina_Crea ,
				  Ctb_Cta_Destino,
				  Ctb_Patri_Neto_Cod,
				  Ctb_Estado_Flujo_Efecctivo_Cod_Det
		        )
		VALUES  ( 
					@Id_Empresa ,
					@Id_Anio ,
					@Id_Periodo ,
					@Id_Libro ,
					@Id_Voucher ,
					@Id_Item ,
					@Ctb_Cuenta ,
					@Ctb_Operacion_Cod ,
					@Ctb_Igv_Cod ,
					@Ctb_Centro_CG ,
					@Ctb_Tipo_DH ,
					@Ctb_Importe_Debe,--@Ctb_Importe_Debe
					@Ctb_Importe_Haber,--@Ctb_Importe_Haber
					@Ctb_Tipo_BSA ,
					@Ctb_Catalogo ,
					@Ctb_Valor_unit ,
					@Ctb_Cantidad,
					@Ctb_Importe_Debe_Extr ,
					@Ctb_Importe_Haber_Extr ,
					@Ctb_Almacen,
					@Ctb_Tipo_Doc_det,
					@Ctb_Serie_det,
					@Ctb_Numero_det,
					@Ctb_Fecha_Mov_det,
					@Ctb_Tipo_Ent_det,
					@Ctb_Ruc_dni_det,
					@Ctb_moneda_cod_det,
					@Ctb_Tipo_Cambio_Cod_Det,
					@Ctb_Tipo_Cambio_Desc_Det,
					@Ctb_Tipo_Cambio_Valor_Det,
					'0001' ,
					@Usuario ,
					GETDATE() ,
					@Maquina,
					@Ctb_Cta_Destino,
					@Ctb_Patri_Neto_Cod,
					@Ctb_Estado_Flujo_Efecctivo_Cod_Det
		        )
END


go

 
ALTER PROC paRpt_INV_MovimientoAlmacen(
	 @Id_Empresa char(3)=NULL      
	,@Id_Anio char(4)=NULL              
	,@Id_Almacen char(2)=NULL      
	,@Invd_Catalogo char(10)=NULL      
	,@Fecha_inicio date=NULL      
	,@Fecha_Fin date=NULL      
)AS
BEGIN
SET NOCOUNT ON

DECLARE @Empresa CHAR(3),
	    @Anio CHAR(4),
		@Almacen CHAR(5),
		@Producto CHAR(10),
		@FechaInicio DATE,
		@FechaFin DATE

	SET @Empresa=@Id_Empresa;
	SET @anio=@Id_Anio;
	SET @Almacen=@Id_Almacen;
	SET @Producto=@Invd_Catalogo;
	SET @FechaInicio=@Fecha_inicio;
	SET @FechaFin=@Fecha_Fin;

       SELECT stck.Id_Empresa,
					emp.Emp_Nombre,
					emp.Emp_RUC,
					stck.Id_Anio,
					stck.Id_Periodo,
					per.Descripcion_Periodo,
					stck.Id_Movimiento,stck.Id_Almacen,
					lga.Alm_Descrcipcion,
					stck.Id_Tipo_Mov,stck.Gen_Descripcion_Det,
					stck.Invd_Catalogo,
					cata.Cat_Descripcion,
					med.Id_Unidad_Medida ,
					ISNULL(med.Und_SunaT,'') AS Und_SunaT,
					med.Und_Descripcion,
					stck.Inv_Tipo_Doc,stck.Id_Sunat,stck.Nombre_Comprobante,    
					stck.Inv_Serie,stck.Inv_Numero,      
					stck.Inv_Fecha,
					stck.CantidadEntrada,
					stck.CostoUnitarioEntrada,
					stck.CostoTotalEntrada,

					stck.CantidadSalida,
					stck.CostoUnitarioSalida,
					stck.CostoTotalSalida,
					stck.Top_Descripcion ,
					stck.Invd_CantidadAtendidaEnBaseAlCoefciente
			FROM dbo.CTB_CATALOGO  cata
			LEFT JOIN dbo.CTB_MARCA mar ON mar.Id_Marca=cata.Id_Marca
			INNER JOIN (
			SELECT  MovDet.Id_Empresa,
					MovDet.Id_Anio,
					MovDet.Id_Periodo,
					MovDet.Id_Movimiento,MovDet.Id_Almacen,
					MovDet.Id_Tipo_Mov,IE.Gen_Descripcion_Det, 
					MovDet.Invd_Catalogo ,
					MovDet.Invd_Unm,
					MovCab.Inv_Tipo_Doc,tds.Id_Sunat,tds.Nombre_Comprobante,   --20   
					MovCab.Inv_Serie,MovCab.Inv_Numero,      
					MovCab.Inv_Fecha,

					 case MovCab.Id_Tipo_Mov when '0034' THEN MovDet.Invd_Cantidad else 0.00  end   AS CantidadEntrada,
					 CASE MovDet.Id_Tipo_Mov when '0034' then ISNULL(MovDet.Invd_Valor_Unit,0.00) ELSE 0.00 END  AS CostoUnitarioEntrada,      
				    (CASE MovDet.Id_Tipo_Mov when '0034' then ISNULL(MovDet.Invd_Cantidad * MovDet.Invd_Valor_Unit,0.00)ELSE 0.00 END) CostoTotalEntrada, 
		     
					 --case MovCab.Id_Tipo_Mov when '0035' then (MovDet.Invd_Cantidad/cunm.Unm_Coef_Base)  end  AS Stock 
					(case MovDet.Id_Tipo_Mov when '0035' then MovDet.INVD_Cantidad else 0 end)as CantidadSalida,      
					(case MovDet.Id_Tipo_Mov when '0035' then ISNULL(MovDet.Invd_Valor_Unit,0.00) else 0.00 end)as CostoUnitarioSalida,      
					(case MovDet.Id_Tipo_Mov when '0035' then ISNULL(MovDet.INVD_Cantidad * MovDet.Invd_Valor_Unit,0.00) else 0.00 end)AS CostoTotalSalida 
					,ito.Top_Descripcion ,
					(case MovDet.Id_Tipo_Mov when '0035' THEN MovDet.Invd_CantidadAtendidaEnBaseAlCoefciente ELSE 0 END ) AS Invd_CantidadAtendidaEnBaseAlCoefciente
			FROM dbo.CTB_MOVIMIENTO_INVENTARIO_DET MovDet WITH(NOLOCK)
			INNER JOIN dbo.CTB_MOVIMIENTO_INVENTARIO_CAB MovCab WITH(NOLOCK) ON 
				MovCab.Id_Empresa = MovDet.Id_Empresa AND 
				MovCab.Id_Anio = MovDet.Id_Anio AND 
				MovCab.Id_Periodo = MovDet.Id_Periodo AND 
				MovCab.Id_Tipo_Mov = MovDet.Id_Tipo_Mov AND 
				MovCab.Id_Almacen = MovDet.Id_Almacen AND 
				MovCab.Id_Movimiento = MovDet.Id_Movimiento
			INNER JOIN dbo.CTB_CATALOGO_UNM cunm ON cunm.Id_Empresa=MovDet.Id_Empresa 
				AND cunm.Id_Tipo=MovDet.Invd_TipoBSA 
				AND cunm.Id_Catalogo=MovDet.Invd_Catalogo 
				AND cunm.Id_Unidad_Medida=MovDet.Invd_Unm
			inner join dbo.GNL_GENERAL_DET ie WITH(NOLOCK) ON      
				IE.Id_General_Det=MovDet.Id_Tipo_Mov 
			left join dbo.CTB_COMPROBANTE tds WITH(NOLOCK) on               
				MovCab.Inv_Tipo_Doc=tds.Id_Comprobante 
			left  join dbo.CTB_TIPO_OPERACION ito WITH(NOLOCK) on                
				MovCab.Id_Tipo_Operacion=ito.Id_Operacion   
			WHERE           
			(@Empresa IS NULL OR MovCab.Id_Empresa=@Empresa) AND      
			(@Anio IS NULL OR MovCab.Id_Anio=@Anio) AND      
			(@Almacen IS NULL OR MovCab.Id_Almacen=@Almacen) AND    
			(@Producto IS NULL OR MovDet.Invd_Catalogo=@Producto) AND 
			(@FechaInicio IS NULL OR MovCab.Inv_Fecha>=@FechaInicio) AND      
			(@FechaFin IS NULL OR MovCab.Inv_Fecha<=@FechaFin) AND      
			MovCab.Inv_Estado='0001' and MovDet.Invd_TipoBSA='0031'    

			) AS stck
				ON cata.Id_Empresa=stck.Id_Empresa AND cata.Id_Catalogo=stck.Invd_Catalogo
		INNER JOIN dbo.CTB_UNIDAD_MEDIDA med ON med.Id_Unidad_Medida=stck.Invd_Unm 
		INNER join dbo.GNL_EMPRESA emp WITH(NOLOCK) on   emp.Id_Empresa = stck.Id_Empresa  
		INNER join dbo.EMPRESA_EJERCICIO per WITH(NOLOCK) on per.Id_Empresa=stck.Id_Empresa and per.Id_Anio=stck.Id_Anio and per.Id_Periodo=stck.Id_Periodo 
		INNER join dbo.CTB_ALMACEN lga WITH(NOLOCK) on stck.Id_Empresa=lga.Id_Empresa and stck.Id_Almacen=lga.Id_Almacen 
    ORDER BY  stck.Invd_Catalogo,stck.Inv_Fecha, stck.Id_Tipo_Mov,--stck.Id_Operacion asc ,
			  stck.Inv_Tipo_Doc,stck.Inv_Serie,stck.Inv_Numero

SET NOCOUNT OFF
END
GO


 create  PROC pa_EXPORTAR_GNL_ENTIDAD_COMPRAS
(
@empresa CHAR(3),
--@anio CHAR(4),
@desde date,
@hasta date

)
AS
BEGIN


		SELECT  
			ENTI.Ent_RUC_DNI, Ent_Tipo_Persona, Ent_Tipo_Doc, 
	
			LTRIM(dbo.fnReemplazar_caracteres_entidad(ENTI.Ent_Razon_Social_Nombre)) AS Ent_Razon_Social_Nombre, 
			Ent_Ape_Paterno, 
			Ent_Ape_Materno,dbo.fnReemplazar_caracteres_entidad(ENTI.Ent_Telefono) AS Ent_Telefono, 
			LTRIM(dbo.fnReemplazar_caracteres_entidad(ENTI.Ent_Repre_Legal)) AS Ent_Repre_Legal, 
			LTRIM(dbo.fnReemplazar_caracteres_entidad(ENTI.Ent_Telefono_movil)) AS Ent_Telefono_movil, 
			LTRIM(dbo.fnReemplazar_caracteres_entidad(ENTI.Ent_Domicilio_Fiscal)) AS Ent_Domicilio_Fiscal, 
			ENTI.Ent_Correo, ENTI.Ent_Pagina_Web, ENTI.Ent_Estado, ENTI.Usuario_Crea, 
			ENTI.Fecha_Crea, ENTI.Maquina_Crea, 
			ISNULL(ENTI.Usuario_Modi,'') AS Usuario_Modi, 
			ISNULL(ENTI.Fecha_Modi,'') AS Fecha_Modi,
			ISNULL(ENTI.Maquina_Modi,'') AS Maquina_Modi
		
		FROM CTB_MOVIMIENTO_CONTABLE_CAB cab 
		INNER JOIN dbo.GNL_ENTIDAD ENTI ON cab.Ctb_Ruc_dni=ENTI.Ent_RUC_DNI
		WHERE 
		cab.Id_Empresa=@empresa AND -- cab.Id_Anio=@anio AND 
		(cab.Ctb_Fecha_Movimiento BETWEEN   @desde  AND @hasta ) AND  
		cab.Ctb_Es_Compra_Comercial=1 AND CAB.Id_Libro='007'


	--SELECT 
	--Ent_RUC_DNI, Ent_Tipo_Persona, Ent_Tipo_Doc, 
	
	--LTRIM(dbo.fnReemplazar_caracteres_entidad(Ent_Razon_Social_Nombre)) AS Ent_Razon_Social_Nombre, 
	--Ent_Ape_Paterno, 
	--Ent_Ape_Materno,dbo.fnReemplazar_caracteres_entidad(Ent_Telefono) AS Ent_Telefono, 
	--LTRIM(dbo.fnReemplazar_caracteres_entidad(Ent_Repre_Legal)) AS Ent_Repre_Legal, 
	--LTRIM(dbo.fnReemplazar_caracteres_entidad(Ent_Telefono_movil)) AS Ent_Telefono_movil, 
	--LTRIM(dbo.fnReemplazar_caracteres_entidad(Ent_Domicilio_Fiscal)) AS Ent_Domicilio_Fiscal, Ent_Correo, Ent_Pagina_Web, Ent_Estado, Usuario_Crea, 
	--Fecha_Crea, Maquina_Crea, 
	--ISNULL(Usuario_Modi,'') AS Usuario_Modi, 
	--ISNULL(Fecha_Modi,'') AS Fecha_Modi,
	--ISNULL(Maquina_Modi,'') AS Maquina_Modi
	-- FROM dbo.GNL_ENTIDAD;


END

GO

alter FUNCTION fnReemplazar_caracteres_entidad(  
--DECLARE
 @glosa VARCHAR(100)  ='VARIOS'
)returns VARCHAR(100)  
AS  
BEGIN  
DECLARE @glosa_correcta VARCHAR(100)  
DECLARE @aux VARCHAR(2)  
SET @glosa_correcta = @glosa   
DECLARE @i INT  
 SET @i = 0  
	declare @caracteres table (idT INT,colC VARCHAR(300))   
	
	insert @caracteres (idT, colC) 
	values (0,'%'), (1,'�'),
			(2,'$'),(3,'�?'),
			(4,'^')
			--, (5,'�')
			,(5,'~'),(6,'�'),
			(7,'�'),
			(8,'�') ,(9,'.'),
			(10,'-'),(11,'�')
			,(12,'|'),
			(13,'�'),
			(14,'/'),
			(15,'*'),(16,'+'),(17,'"'),(18,'='),
			(19,'</TD>'),
			(20,'</TD>'),
			(21,'<TR>'),		
			(22,'<'),(23,'>'),
			(24,'</TD>
				 </TR>

				 <TR>
				'),(25,'TD'),(26,'TR'),
				(27,CHAR(13)),(28,CHAR(10)) ,(29,'"')

DECLARE @Cont INT
SET @Cont=0
SET @Cont=(SELECT COUNT (*) from @caracteres)

WHILE @i < @Cont  
BEGIN  
 SET @aux = (SELECT colC FROM @caracteres WHERE idT = @i)  
 SET @glosa_correcta = REPLACE(@glosa_correcta,  @aux, ' ')  
 SET @i = @i + 1  
END  
RETURN @glosa_correcta  
END
GO


ALTER PROC pa_CTB_MOVIMIENTO_INVENTARIO_DET_INSERTAR
(
@Id_Empresa CHAR(3), 
@Id_Anio CHAR(4), 
@Id_Periodo CHAR(2), 
@Id_Tipo_Mov CHAR(4), 
@Id_Almacen CHAR(2), 
@Id_Movimiento CHAR(10), 
@Id_Item INT, 
@Invd_TipoBSA CHAR(4)=NULL, 
@Invd_Catalogo CHAR(10)=NULL, 
@Invd_Cantidad DECIMAL(10,4)=NULL, 
@Invd_Valor_Unit DECIMAL(10,4)=NULL , 
@Invd_Total DECIMAL(10,4)=NULL,  
@Invd_Centro_CG CHAR(3)=NULL,
@Invd_Unm  CHAR(3)=NULL,
@Invd_Tiene_Lote BIT = NULL,
@Invd_Lote VARCHAR(30) = NULL,
@Invd_FechaFabricacion DATE = NULL,
@Invd_FechaVencimiento DATE = NULL,
@Invd_CantidadAtendidaEnBaseAlCoefciente DECIMAL(10,4)=NULL   
)
AS
BEGIN

SET @Invd_CantidadAtendidaEnBaseAlCoefciente =(
SELECT (Unm_Coef_Base * @Invd_Cantidad) FROM dbo.CTB_CATALOGO_UNM WHERE Id_Empresa=@Id_Empresa AND Id_Tipo=@Invd_TipoBSA AND Id_Catalogo=@Invd_Catalogo
AND Id_Unidad_Medida=@Invd_Unm)



INSERT INTO dbo.CTB_MOVIMIENTO_INVENTARIO_DET
        ( Id_Empresa ,
          Id_Anio ,
          Id_Periodo ,
          Id_Tipo_Mov ,
          Id_Almacen ,
          Id_Movimiento ,
          Id_Item ,
          Invd_TipoBSA ,
          Invd_Catalogo ,
          Invd_Cantidad ,
          Invd_Valor_Unit ,
          Invd_Total ,
          Invd_Centro_CG,
		  Invd_Unm,
		  Invd_Tiene_Lote,
		Invd_Lote  ,
		Invd_FechaFabricacion ,
		Invd_FechaVencimiento  ,
		Invd_CantidadAtendidaEnBaseAlCoefciente
		  )
VALUES  ( @Id_Empresa, 
			@Id_Anio, 
			@Id_Periodo, 
			@Id_Tipo_Mov, 
			@Id_Almacen, 
			@Id_Movimiento, 
			@Id_Item, 
			@Invd_TipoBSA, 
			@Invd_Catalogo, 
			@Invd_Cantidad, 
			@Invd_Valor_Unit, 
			@Invd_Total, 
			@Invd_Centro_CG,
			@Invd_Unm,
			@Invd_Tiene_Lote,
			@Invd_Lote  ,
			@Invd_FechaFabricacion ,
			@Invd_FechaVencimiento  ,
			@Invd_CantidadAtendidaEnBaseAlCoefciente
        )

END

GO





CREATE PROC pa_CONSULTAR_STOCK_LOTE
(
@Id_Empresa CHAR(3),
@Id_Anio CHAR(4),
@Id_Almacen CHAR(2),
@Invd_TipoBSA CHAR(4),
@Invd_Catalogo CHAR(10)=NULL,
@Codigo_Barra CHAR(200)=NULL
)
AS
BEGIN
SET NOCOUNT ON
			SELECT cata.Id_Catalogo,
				   cata.Cat_Descripcion,
					ISNULL(cata.Cat_Serie,'') AS Cat_Serie,
					ISNULL(stck.Stock,0) AS Stock,
					ISNULL(cata.Acepta_Lote,0) AS Acepta_Lote,
					cata.Id_Tipo,
					cata.Id_Unidad_Medida ,
					uni.Und_Descripcion,
					ISNULL(uni.Und_Abreviado,'') AS Und_Abreviado,
					ISNULL(uni.Und_Codigo_FE,'') AS Und_Codigo_FE,
					ISNULL(mar.Mar_Descripcion,'') AS Mar_Descripcion
			FROM dbo.CTB_CATALOGO  cata
			INNER JOIN dbo.CTB_UNIDAD_MEDIDA uni ON cata.Id_Unidad_Medida=uni.Id_Unidad_Medida
			LEFT JOIN dbo.CTB_MARCA mar ON mar.Id_Marca=cata.Id_Marca
			INNER JOIN (
			SELECT  MovDet.Id_Empresa,
					MovDet.Invd_Catalogo ,
					ISNULL(SUM(case MovCab.Id_Tipo_Mov when '0034' then(MovDet.Invd_Cantidad) end),0) -        
					ISNULL(SUM(case MovCab.Id_Tipo_Mov when '0035' then (MovDet.Invd_Cantidad*cunm.Unm_Coef_Base)  end),0) AS Stock 
		 
			FROM dbo.CTB_MOVIMIENTO_INVENTARIO_DET MovDet WITH(NOLOCK)
			INNER JOIN dbo.CTB_MOVIMIENTO_INVENTARIO_CAB MovCab WITH(NOLOCK) ON 
				MovCab.Id_Empresa = MovDet.Id_Empresa AND 
				MovCab.Id_Anio = MovDet.Id_Anio AND 
				MovCab.Id_Periodo = MovDet.Id_Periodo AND 
				MovCab.Id_Tipo_Mov = MovDet.Id_Tipo_Mov AND 
				MovCab.Id_Almacen = MovDet.Id_Almacen AND 
				MovCab.Id_Movimiento = MovDet.Id_Movimiento
			INNER JOIN dbo.CTB_CATALOGO_UNM cunm ON cunm.Id_Empresa=MovDet.Id_Empresa 
				AND cunm.Id_Tipo=MovDet.Invd_TipoBSA 
				AND cunm.Id_Catalogo=MovDet.Invd_Catalogo 
				AND cunm.Id_Unidad_Medida=MovDet.Invd_Unm
			WHERE MovCab.Id_Empresa=@Id_Empresa AND
				MovCab.Id_Anio=@Id_Anio AND
				MovDet.Invd_TipoBSA=@Invd_TipoBSA AND
				MovDet.Id_Almacen=@Id_Almacen AND
				MovCab.Inv_Estado='0001' AND 
				(@Invd_Catalogo IS NULL OR MovDet.Invd_Catalogo=@Invd_Catalogo)
			 GROUP BY MovDet.Id_Empresa,MovDet.Invd_Catalogo
			) AS stck
				ON cata.Id_Empresa=stck.Id_Empresa AND cata.Id_Catalogo=stck.Invd_Catalogo


SET NOCOUNT OFF
END
