﻿//using DevExpress.XtraEditors.xtr
namespace Contable
{
    partial class frm_fuente :DevExpress.XtraEditors.XtraForm
    {
  

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //public Accion.Estados_ Est = Accion.Estados_.Ninguno;
        private Estados Est = Estados.Ninguno;
        private Estados EstD = Estados.Ninguno;
        private Estados EstD2 = Estados.Ninguno;
        //public Accion.Estados Estado
        //{
        //    get { return Est; }
        //    set
        //    {
        //        Est = value;
        //        //CambiarEstado();
        //        //CambiandoEstado();
        //    }
        //}

        public bool cambio;
        public Estados Estado
        {
            get { return Est; }

            set { Est = value; }
        }

        public Estados EstadoDetalle
        {
            get { return EstD; }
            set
            {
                EstD = value;
                CambiandoEstadoDetalle();
            }
        }
        public Estados EstadoDetalle2
        {
            get { return EstD2; }
            set
            {
                EstD2 = value;
                CambiandoEstadoDetalle2();
            }
        }

        public virtual void CambiandoEstado()
        {
        }

        public virtual void CambiandoEstadoDetalle()
        {
        }
        public virtual void CambiandoEstadoDetalle2()
        {
        }

        public virtual void ActualizarBusquedaPorAnio()
        {
        }
     public virtual void ActualizarBusquedaPorPeriodo()
        {
           
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formAssistant1 = new DevExpress.XtraBars.FormAssistant();
            this.SuspendLayout();
            // 
            // frm_fuente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 361);
            this.Name = "frm_fuente";
            this.Text = "frm_fuente";
            this.Load += new System.EventHandler(this.frm_fuente_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.FormAssistant formAssistant1;
    }
}