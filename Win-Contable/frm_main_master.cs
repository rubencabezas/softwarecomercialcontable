﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Comercial;
using Comercial.Configuracion;
using Comercial.Consultas;
using Contable;
using Contable._0_Configuracion.Estados_Financieros;
using Contable.Procesos.Inventario.Catalogo;
using Contable.Reportes.Libros_y_registros;

namespace Contable
{
    public partial class frm_main_master : frm_fuente
    {
        public frm_main_master()
        {
            InitializeComponent();
        }

        public string EmpresaNombre;
        public string CodigoEmpresa;
        public string RucEmpresa;

        public string AnioSelect;
        public string PeriodoSelect;

        //FACTURACION ELECTRONICA
        public string Emp_DireccionCorta;
        public string Emp_Urbanizacion;
        public string Emp_Direccion_Departamento;
        public string Emp_Direccion_Provincia;
        public string Emp_Direccion_Distrito;


        public string UserID;
        public string UserName = "user";
        public string UserNameEmployee;

        private void frm_main_master_Load(object sender, EventArgs e)
        {
            Actual_Conexion.EmpresaNombre = EmpresaNombre;
            Actual_Conexion.CodigoEmpresa = CodigoEmpresa;
            Actual_Conexion.RucEmpresa = RucEmpresa;

            Actual_Conexion.UserID = UserID;
            Actual_Conexion.UserName = UserName;
            Actual_Conexion.UserNameEmployee = UserNameEmployee;

            //FACTURACION ELECTRONICA
            Actual_Conexion.Emp_DireccionCorta = Emp_DireccionCorta;
            Actual_Conexion.Emp_Urbanizacion = Emp_Urbanizacion;
            Actual_Conexion.Emp_Direccion_Departamento = Emp_Direccion_Departamento;
            Actual_Conexion.Emp_Direccion_Provincia = Emp_Direccion_Provincia;
            Actual_Conexion.Emp_Direccion_Distrito = Emp_Direccion_Distrito;



            IniciarBarra();

           // timerTipodeCambio.Enabled = true;
        }

        List<Entidad_Ejercicio> Anios = new List<Entidad_Ejercicio>();
        List<Entidad_Ejercicio> Periodos = new List<Entidad_Ejercicio>();
        List<Entidad_Ejercicio> Anios_Aux = new List<Entidad_Ejercicio>();
        public void IniciarBarra()
        {
            try
            {

                txtempresa.Caption = Actual_Conexion.EmpresaNombre;
                Logica_Ejercicio LogAnio = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

                Anios = LogAnio.Listar(Ent);

                var PrivView = from item in Anios
                               orderby item.Id_Anio descending
                               select item;
                Anios_Aux = PrivView.ToList();


                comboanio.ValueMember = "Id_Anio";
                comboanio.DisplayMember = "Id_Anio";
                comboanio.DataSource = Anios_Aux;

                ListarPeriodos();

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        public void ListarPeriodos()
        {
            try
            {

                Logica_Ejercicio LogPer = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;

                Periodos = LogPer.Listar_Periodo(Ent);


                comboperiodo.ValueMember = "Id_Periodo";
                comboperiodo.DisplayMember = "Descripcion_Periodo";
                comboperiodo.DataSource = Periodos;


                DateTime time = DateTime.Now;
                string format = "00";
                comboperiodo.SelectedValue = time.Month.ToString(format);

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void comboanio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboanio.Items.Count > 0)
            {

                Actual_Conexion.AnioSelect = comboanio.SelectedValue.ToString();
                ListarPeriodos();
                Actual_Conexion.PeriodoSelect = comboperiodo.SelectedValue.ToString();
                Accion.RefreshDateDefault();
                ActualizarBusquedaPorAnio();
            }
        }

        private void comboperiodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboperiodo.Items.Count > 0)
            {
                Actual_Conexion.PeriodoSelect = comboperiodo.SelectedValue.ToString();
                Accion.RefreshDateDefault();
                ActualizarBusquedaPorPeriodo();
                //Cambio = true;
                frm_fuente FrmChildFather = new frm_fuente();
                FrmChildFather.cambio = true;
            }
        }

        private void btn_establecimiento_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0001 = establecimiento
            if (Accesos(Actual_Conexion.UserID, "0001"))
            {
                if (MdiNoDuplicate(new frm_establecimiento().Name) == false)
                {
                   frm_establecimiento f = new frm_establecimiento();
                    AbreHijo(f);
                }


            }

        }

        public bool MdiNoDuplicate(string VcNombre)
        {
            FormCollection loColFormularios = default(FormCollection);

            loColFormularios = Application.OpenForms;
            foreach (Form Form in loColFormularios)
            {
                if (Form.Name == VcNombre)
                {
                    Form.Activate();
                    return true;
                }
            }
            return false;
        }

        public void AbreHijo(Form Frm)
        {
            Frm.MdiParent = this;
            if (Frm.MdiChildren.Count() <= 0)
            {
                //Par que se maximize en caso no encontrase ningun hijo de su c.. padre
                //Frm.WindowState = FormWindowState.Maximized;
            }

            Frm.Show();
        }

        private void btnComprobantes_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos.frm_tipo_documentos().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos.frm_tipo_documentos f = new _0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos.frm_tipo_documentos();
                AbreHijo(f);
            }
        }

        private void btnMonedas_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda.frm_moneda().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda.frm_moneda f = new _0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda.frm_moneda();
                AbreHijo(f);
            }
        }

        private void btnlibroscontables_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Tablas_Sunat.Libros.frm_libros_contables().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Tablas_Sunat.Libros.frm_libros_contables f = new _0_Configuracion.Mantenimiento.Tablas_Sunat.Libros.frm_libros_contables();
                AbreHijo(f);
            }
        }

        private void btnimpuesto_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto f = new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto();
 
            if (MdiNoDuplicate(f.Name) == false)
            {
                _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto ff = new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto();
                AbreHijo(ff);
            }
        }

        private void btnasignacionimporte_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Impuesto.frm_asignacion_impuesto().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Impuesto.frm_asignacion_impuesto f = new _0_Configuracion.Mantenimiento.Impuesto.frm_asignacion_impuesto();

                AbreHijo(f);
            }
        }

        private void btntipocambio_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_cambio().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_cambio f = new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_cambio();
                AbreHijo(f);
            }
        }

        private void btncuentacorriente_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_cuenta_corriente().Name) == false)
            {
                Procesos.Tesoreria.frm_cuenta_corriente f = new Procesos.Tesoreria.frm_cuenta_corriente();
                AbreHijo(f);
            }
        }

        private void btnmediopago_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_medio_pago().Name) == false)
            {
                Procesos.Tesoreria.frm_medio_pago f = new Procesos.Tesoreria.frm_medio_pago();
                AbreHijo(f);
            }
        }

        private void btnentidadfinanciera_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_entidad_financiera().Name) == false)
            {
                Procesos.Tesoreria.frm_entidad_financiera f = new Procesos.Tesoreria.frm_entidad_financiera();
                AbreHijo(f);
            }
        }

        private void btndiario_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Diario.frm_diario().Name) == false)
            {
                Procesos.Diario.frm_diario f = new Procesos.Diario.frm_diario();
                AbreHijo(f);
            }
        }

        private void btncomrpas_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new frm_compras().Name) == false)
            {
                frm_compras f = new frm_compras();
                AbreHijo(f);
            }
        }

        private void btnventas_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Ventas.frm_ventas().Name) == false)
            {
                Procesos.Ventas.frm_ventas f = new Procesos.Ventas.frm_ventas();
                AbreHijo(f);
            }
        }

        private void btntesoreria_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_ingreso().Name) == false)
            {
                Procesos.Tesoreria.frm_ingreso f = new Procesos.Tesoreria.frm_ingreso();
                AbreHijo(f);
            }
        }

        private void btnempresas_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_empresa().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_empresa f = new _0_Configuracion.Mantenimiento.Generales.frm_empresa();

                AbreHijo(f);
            }
        }

        private void btnejercicios_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_ejercicios().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_ejercicios f = new _0_Configuracion.Mantenimiento.Generales.frm_ejercicios();
                AbreHijo(f);
            }
        }

        private void btnreplicacion_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new frm_replicacion().Name) == false)
            {
                frm_replicacion f = new frm_replicacion();
                AbreHijo(f);
            }
        }

        private void btninicio_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {

            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Inicio.frm_inicio().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Inicio.frm_inicio f = new _0_Configuracion.Mantenimiento.Inicio.frm_inicio();
                AbreHijo(f);
            }
        }

        private void btncajachica_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_caja_chica().Name) == false)
            {
                Procesos.Tesoreria.frm_caja_chica f = new Procesos.Tesoreria.frm_caja_chica();
                AbreHijo(f);
            }
        }

        private void btnseriesdoccajachica_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_series_documentos().Name) == false)
            {
                Procesos.Tesoreria.frm_series_documentos f = new Procesos.Tesoreria.frm_series_documentos();
                AbreHijo(f);
            }
        }

        private void btnaperturacajachica_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_apertura_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_apertura_cajachica f = new Procesos.Tesoreria.frm_apertura_cajachica();
                AbreHijo(f);
            }
        }

        private void btnrendicion_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_rendicion_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_rendicion_cajachica f = new Procesos.Tesoreria.frm_rendicion_cajachica();
                AbreHijo(f);
            }
        }

        private void btnreembolso_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_reembolso_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_reembolso_cajachica f = new Procesos.Tesoreria.frm_reembolso_cajachica();
                AbreHijo(f);
            }
        }

        private void btncierre_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_cierre_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_cierre_cajachica f = new Procesos.Tesoreria.frm_cierre_cajachica();
                AbreHijo(f);
            }
        }


        List<Entidad_Accesos> Lista_Acceso = new List<Entidad_Accesos>();
        string Cod_Opcion_General = "";
        bool Accesos(string dni_user, string Cod_Opcion)
        {
            //try
            //{
            Entidad_Accesos ent = new Entidad_Accesos();
            Logica_Accesos log = new Logica_Accesos();

            ent.Usuario_dni = dni_user;// Actual_Conexion.UserID;
            ent.Id_Opcion = Cod_Opcion;
            ent.Modulo_Cod = "01"; //vebtas

            Lista_Acceso = log.Listar(ent);

            if ((Actual_Conexion.UserID != "admin"))
            {

                if ((Lista_Acceso.Count > 0))
                {
                    Cod_Opcion_General = Lista_Acceso[0].Id_Opcion;

                    if (Cod_Opcion_General == Cod_Opcion)
                    {
                        return true;
                    }
                    else
                    {
                        Accion.Advertencia("Usted no tiene permiso para ingresar a esta ventana");
                        return false;
                    }
                }
                else
                {
                    Accion.Advertencia("No existe accesos asignados");
                    return false;
                }

            }
            else
            {
                return true;
            }

            //}
            //catch (Exception ex)
            //{
            //    Accion.ErrorSistema(ex.Message);
            //}
        }

        private void btnpuntoventa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0002=punto de venta
            if (Accesos(Actual_Conexion.UserID, "0002"))
            {

                if (MdiNoDuplicate(new frm_punto_venta().Name) == false)
                {
                    frm_punto_venta f = new frm_punto_venta();
                    AbreHijo(f);
                }
            }
        }

        private void bntpuntocompra_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0003=punto de compra
            if (Accesos(Actual_Conexion.UserID, "0003"))
            {

                if (MdiNoDuplicate(new frm_punto_compra().Name) == false)
                {
                    frm_punto_compra f = new frm_punto_compra();
                    AbreHijo(f);
                }
            }
        }

        private void btnalmacen_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0004=almacen
            if (Accesos(Actual_Conexion.UserID, "0004"))
            {

                if (MdiNoDuplicate(new frm_almacen().Name) == false)
                {
                    frm_almacen f = new frm_almacen();
                    AbreHijo(f);
                }
            }
        }

        private void btndocumentospdv_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0005=documentos por punto
            if (Accesos(Actual_Conexion.UserID, "0005"))
            {

                if (MdiNoDuplicate(new frm_documentos_pdv().Name) == false)
                {
                    frm_documentos_pdv f = new frm_documentos_pdv();
                    AbreHijo(f);
                }
            }
        }

        private void btnseriecomprobante_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0006=series
            if (Accesos(Actual_Conexion.UserID, "0006"))
            {

                if (MdiNoDuplicate(new frm_series_comprobantes().Name) == false)
                {
                    frm_series_comprobantes f = new frm_series_comprobantes();
                    AbreHijo(f);
                }
            }
        }

        private void btnprecios_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0007=precios
            if (Accesos(Actual_Conexion.UserID, "0007"))
            {

                if (MdiNoDuplicate(new frm_precio().Name) == false)
                {
                    frm_precio f = new frm_precio();
                    AbreHijo(f);
                }
            }
        }

        private void btncaja_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0008=caja
            if (Accesos(Actual_Conexion.UserID, "0008"))
            {

                if (MdiNoDuplicate(new frm_caja_punto_venta().Name) == false)
                {
                    frm_caja_punto_venta f = new frm_caja_punto_venta();
                    AbreHijo(f);
                }
            }
        }

        private void btngrupo_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0009=grupo
            if (Accesos(Actual_Conexion.UserID, "0009"))
            {

                if (MdiNoDuplicate(new frm_grupo().Name) == false)
                {
                    frm_grupo f = new frm_grupo();
                    AbreHijo(f);
                }
            }
        }

        private void btnfamilia_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0010=familia
            if (Accesos(Actual_Conexion.UserID, "0010"))
            {

                if (MdiNoDuplicate(new frm_familia().Name) == false)
                {
                    frm_familia f = new frm_familia();
                    AbreHijo(f);
                }
            }
        }

        private void btncatalogo_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0011=catalogo
            if (Accesos(Actual_Conexion.UserID, "0011"))
            {

                if (MdiNoDuplicate(new frm_catalogo().Name) == false)
                {
                    frm_catalogo f = new frm_catalogo();
                    AbreHijo(f);
                }
            }
        }

        private void btncompracomercial_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0012=compra
            if (Accesos(Actual_Conexion.UserID, "0012"))
            {

                if (MdiNoDuplicate(new frm_compras_comercial().Name) == false)
                {
                    frm_compras_comercial f = new frm_compras_comercial();
                    AbreHijo(f);
               
                }
            }
        }

        private void btningresocomercial_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0013=ingresos
            if (Accesos(Actual_Conexion.UserID, "0013"))
            {

                if (MdiNoDuplicate(new frm_inventario_comercial().Name) == false)
                {

                    frm_inventario_comercial f = new frm_inventario_comercial();
                    AbreHijo(f);
 
                }
            }
        }

        private void btntraspasoscomercial_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new frm_traspasos_comercial().Name) == false)
            {
                frm_traspasos_comercial f = new frm_traspasos_comercial();
                AbreHijo(f);
            }
        }

        private void btnpos_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0014=punto de venta
            if (Accesos(Actual_Conexion.UserID, "0014"))
            {

                if (MdiNoDuplicate(new frm_pos_edicion().Name) == false)
                {
                    frm_pos_edicion f = new frm_pos_edicion();
                    AbreHijoPOS(f);
                }
            }
        }

        public void AbreHijoPOS(Form Frm)
        {
            //Frm.MdiParent = this;
            if (Frm.MdiChildren.Count() <= 0)
            {
                //Par que se maximize en caso no encontrase ningun hijo de su c.. padre
                Frm.WindowState = FormWindowState.Maximized;
            }

            Frm.Show();
        }

        private void btnconsultabolfac_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //0015=ticket boleta factura
            if (Accesos(Actual_Conexion.UserID, "0015"))
            {

                if (MdiNoDuplicate(new frm_consulta_ventas_contado().Name) == false)
                {

                    frm_consulta_ventas_contado f = new frm_consulta_ventas_contado();
                    AbreHijo(f);
    

                }
            }
        }

        private void btnconsultasinticket_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (Accesos(Actual_Conexion.UserID, "0016"))
            {

                if (MdiNoDuplicate(new frm_consulta_ventas_sin_ticket().Name) == false)
                {

                    frm_consulta_ventas_sin_ticket f = new frm_consulta_ventas_sin_ticket();
                    AbreHijo(f);
             
                }
            }
        }

        private void btnConsultaCajas_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (Accesos(Actual_Conexion.UserID, "0018"))
            {

                if (MdiNoDuplicate(new frm_consulta_cajas().Name) == false)
                {

                    frm_consulta_cajas f = new frm_consulta_cajas();
                    AbreHijo(f);
   
                }
            }
        }

        private void btnconsultastock_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (Accesos(Actual_Conexion.UserID, "0019"))
            {

                if (MdiNoDuplicate(new frm_consulta_stock_kardex().Name) == false)
                {

                    frm_consulta_stock_kardex f = new frm_consulta_stock_kardex();
                    AbreHijo(f);
    
                }
            }
        }

        private void btnusuario_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (Accesos(Actual_Conexion.UserID, "0020"))
            {
                if (MdiNoDuplicate(new frm_usuarios().Name) == false)
                {
                    frm_usuarios f = new frm_usuarios();
                    AbreHijo(f);
                }
            }
        }

        private void btnaccesos_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (Accesos(Actual_Conexion.UserID, "0020"))
            {
                if (MdiNoDuplicate(new frm_roles_01().Name) == false)
                {
                    frm_roles_01 f = new frm_roles_01();
                    AbreHijo(f);
                }
            }
        }

        private void btnelemento_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_elemento().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_elemento f = new _0_Configuracion.Mantenimiento.Generales.frm_elemento();
                AbreHijo(f);
            }
        }

        private void btnestructura_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_estructura().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_estructura f = new _0_Configuracion.Mantenimiento.Generales.frm_estructura();
                AbreHijo(f);
            }
        }

        private void btnplangeneral_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {

            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_plan_general().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_plan_general f = new _0_Configuracion.Mantenimiento.Generales.frm_plan_general();
                AbreHijo(f);
            }
        }

        private void btnplanempresarial_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Plan_Empresarial.frm_plan_empresarial().Name) == false)
            {
                _0_Configuracion.Plan_Empresarial.frm_plan_empresarial f = new _0_Configuracion.Plan_Empresarial.frm_plan_empresarial();
                AbreHijo(f);
            }
        }

        private void btnanalisiscontable(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {

            if (MdiNoDuplicate(new _0_Configuracion.Plan_Empresarial.frm_analisis_operacion().Name) == false)
            {
                _0_Configuracion.Plan_Empresarial.frm_analisis_operacion f = new _0_Configuracion.Plan_Empresarial.frm_analisis_operacion();
                AbreHijo(f);
            }
        }

        private void btnbalancecomprobacion_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Balance_Comprobacion.frm_balance_comprobacion().Name) == false)
            {
                Reportes.Balance_Comprobacion.frm_balance_comprobacion f = new Reportes.Balance_Comprobacion.frm_balance_comprobacion();
                AbreHijo(f);
            }
        }

        private void btnregistrocompras_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_registro_compras().Name) == false)
            {
                Reportes.Libros_y_registros.frm_registro_compras f = new Reportes.Libros_y_registros.frm_registro_compras();
                AbreHijo(f);
            }
        }

        private void btnregistroventa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_registo_ventas().Name) == false)
            {
                Reportes.Libros_y_registros.frm_registo_ventas f = new Reportes.Libros_y_registros.frm_registo_ventas();
                AbreHijo(f);
            }
        }

        private void btnlibrodiario_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_libro_diario().Name) == false)
            {
                Reportes.Libros_y_registros.frm_libro_diario f = new Reportes.Libros_y_registros.frm_libro_diario();
                AbreHijo(f);
            }
        }

        private void btnlibromayor_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_libro_mayor().Name) == false)
            {
                Reportes.Libros_y_registros.frm_libro_mayor f = new Reportes.Libros_y_registros.frm_libro_mayor();
                AbreHijo(f);
            }
        }

        private void btnigv_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_determinacion_igv().Name))
            {
                frm_determinacion_igv frm = new frm_determinacion_igv();
                this.AbreHijo(frm);
            }
        }

        private void btnLibroSimplificado_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_libro_diario_simplificado().Name) == false)
            {
                Reportes.Libros_y_registros.frm_libro_diario_simplificado f = new Reportes.Libros_y_registros.frm_libro_diario_simplificado();
                AbreHijo(f);
            }
        }

        private void btnesatdosituacionfinanciera_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_situacion_financiera().Name))
            {
                frm_estado_situacion_financiera frm = new frm_estado_situacion_financiera();
                this.AbreHijo(frm);
            }
        }

        private void btnestadodflujoefectivo_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_flujo_efectivo().Name))
            {
                frm_estado_flujo_efectivo frm = new frm_estado_flujo_efectivo();
                this.AbreHijo(frm);
            }
        }

        private void btnestadocambiopatrimonioneto_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_cambio_patrimonio_neto().Name))
            {
                frm_estado_cambio_patrimonio_neto frm = new frm_estado_cambio_patrimonio_neto();
                this.AbreHijo(frm);
            }
        }

        private void btnesatdofinancierosunat_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_financiero_tributario().Name))
            {
                frm_estado_financiero_tributario frm = new frm_estado_financiero_tributario();
                this.AbreHijo(frm);
            }
        }

        private void btncambiospatrimonioneto_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_estado_cambios_patrimonio_neto_smv().Name))
            {
                frm_formato_estado_cambios_patrimonio_neto_smv frm = new frm_formato_estado_cambios_patrimonio_neto_smv();
                this.AbreHijo(frm);
            }
        }

        private void btnestadosfianacierostributarios_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_balance_tributario().Name))
            {
                frm_formato_balance_tributario frm = new frm_formato_balance_tributario();
                this.AbreHijo(frm);
            }
        }

        private void btnestadofinansmv_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_estado_situacion_finan_est_resultado().Name))
            {
                frm_formato_estado_situacion_finan_est_resultado frm = new frm_formato_estado_situacion_finan_est_resultado();
                this.AbreHijo(frm);
            }
        }

        private void btnflujoefectivo_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_flujo_efectivo().Name))
            {
                frm_formato_flujo_efectivo frm = new frm_formato_flujo_efectivo();
                this.AbreHijo(frm);
            }
        }


        public override void ActualizarBusquedaPorPeriodo()
        {

            try
            {
                FormCollection loColFormularios;
                string name = typeof(frm_fuente).AssemblyQualifiedName;
                Type type = Type.GetType(name);

                loColFormularios = Application.OpenForms;
                foreach (Form loForm in loColFormularios)
                {

                    if ((loForm.GetType() == loForm.GetType()))
                    {
                        frm_fuente FrmChildFather = new frm_fuente();
                        FrmChildFather.ActualizarBusquedaPorPerio();
                    }

                }


            }
            catch (Exception ex)
            {
            }

        }



    }
}
