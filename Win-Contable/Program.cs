﻿using Contable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Win_Contable
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

             frm_login_master m = new   frm_login_master();
            m.Show();
            Application.Run();
        }
    }
}
