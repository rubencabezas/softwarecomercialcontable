﻿namespace Contable.Procesos.Diario
{
    partial class frm_diario_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtflujoefectivodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtflujoefectivocod = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.TxtDMN = new System.Windows.Forms.TextBox();
            this.TxtHME = new System.Windows.Forms.TextBox();
            this.TxtHMN = new System.Windows.Forms.TextBox();
            this.TxtDME = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtpatrimonionetodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpatrimonionetocod = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtimporte = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtccgdesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtccgcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpatrimonionetodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpatrimonionetocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtccgdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtccgcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(886, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 607);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(886, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 575);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(886, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 575);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.txtentidad);
            this.groupControl1.Controls.Add(this.txttipocambiodesc);
            this.groupControl1.Controls.Add(this.txttipocambiovalor);
            this.groupControl1.Controls.Add(this.txttipocambiocod);
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.txtcondiciondesc);
            this.groupControl1.Controls.Add(this.txtcondicioncod);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txtrucdni);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(881, 172);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(102, 69);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(192, 20);
            this.txttipodocdesc.TabIndex = 6;
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(614, 69);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(93, 20);
            this.txtfechadoc.TabIndex = 10;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(559, 72);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 13);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "Fecha Doc:";
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(342, 69);
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(211, 20);
            this.txtserie.TabIndex = 8;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(146, 90);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtentidad.Size = new System.Drawing.Size(561, 20);
            this.txtentidad.TabIndex = 13;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(539, 112);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 22;
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(646, 112);
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Properties.MaxLength = 4;
            this.txttipocambiovalor.Size = new System.Drawing.Size(62, 20);
            this.txttipocambiovalor.TabIndex = 23;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(503, 112);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 21;
            this.txttipocambiocod.TextChanged += new System.EventHandler(this.txttipocambiocod_TextChanged);
            this.txttipocambiocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocod_KeyDown);
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(318, 112);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedadesc.Size = new System.Drawing.Size(149, 20);
            this.txtmonedadesc.TabIndex = 19;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(278, 112);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedacod.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacod.TabIndex = 18;
            this.txtmonedacod.TextChanged += new System.EventHandler(this.txtmonedacod_TextChanged);
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(102, 112);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondiciondesc.Size = new System.Drawing.Size(115, 20);
            this.txtcondiciondesc.TabIndex = 16;
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(62, 112);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(38, 20);
            this.txtcondicioncod.TabIndex = 15;
            this.txtcondicioncod.TextChanged += new System.EventHandler(this.txtcondicioncod_TextChanged);
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(472, 115);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(25, 13);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "T.C.:";
            // 
            // txtrucdni
            // 
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(62, 90);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 12;
            this.txtrucdni.TextChanged += new System.EventHandler(this.txtrucdni_TextChanged);
            this.txtrucdni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdni_KeyDown);
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(221, 115);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 17;
            this.labelControl9.Text = "Moneda:";
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(62, 69);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 5;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(5, 115);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "Condicion:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(300, 72);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Serie:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(30, 93);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(25, 13);
            this.labelControl7.TabIndex = 11;
            this.labelControl7.Text = "RUC:";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(62, 47);
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(645, 20);
            this.txtglosa.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 72);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "T. Doc.:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(62, 25);
            this.txtlibro.MenuManager = this.barManager1;
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlibro.Size = new System.Drawing.Size(645, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(25, 50);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Glosa:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(28, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // txtflujoefectivodesc
            // 
            this.txtflujoefectivodesc.Enabled = false;
            this.txtflujoefectivodesc.EnterMoveNextControl = true;
            this.txtflujoefectivodesc.Location = new System.Drawing.Point(220, 86);
            this.txtflujoefectivodesc.MenuManager = this.barManager1;
            this.txtflujoefectivodesc.Name = "txtflujoefectivodesc";
            this.txtflujoefectivodesc.Size = new System.Drawing.Size(488, 20);
            this.txtflujoefectivodesc.TabIndex = 27;
            // 
            // txtflujoefectivocod
            // 
            this.txtflujoefectivocod.EnterMoveNextControl = true;
            this.txtflujoefectivocod.Location = new System.Drawing.Point(121, 86);
            this.txtflujoefectivocod.MenuManager = this.barManager1;
            this.txtflujoefectivocod.Name = "txtflujoefectivocod";
            this.txtflujoefectivocod.Size = new System.Drawing.Size(97, 20);
            this.txtflujoefectivocod.TabIndex = 26;
            this.txtflujoefectivocod.TextChanged += new System.EventHandler(this.txtflujoefectivocod_TextChanged);
            this.txtflujoefectivocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtflujoefectivocod_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Flujo Efectivo Cod.";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.txtflujoefectivodesc);
            this.groupControl2.Controls.Add(this.txtflujoefectivocod);
            this.groupControl2.Controls.Add(this.TxtDMN);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Controls.Add(this.TxtHME);
            this.groupControl2.Controls.Add(this.TxtHMN);
            this.groupControl2.Controls.Add(this.TxtDME);
            this.groupControl2.Controls.Add(this.label3);
            this.groupControl2.Controls.Add(this.txtpatrimonionetodesc);
            this.groupControl2.Controls.Add(this.txtpatrimonionetocod);
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.btnanadirdet);
            this.groupControl2.Controls.Add(this.btneditardet);
            this.groupControl2.Controls.Add(this.btnquitardet);
            this.groupControl2.Controls.Add(this.btnnuevodet);
            this.groupControl2.Controls.Add(this.dgvdatos);
            this.groupControl2.Controls.Add(this.txtimporte);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.txttipodesc);
            this.groupControl2.Controls.Add(this.txtccgdesc);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.txttipo);
            this.groupControl2.Controls.Add(this.txtcuentadesc);
            this.groupControl2.Controls.Add(this.txtccgcod);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.txtcuenta);
            this.groupControl2.Location = new System.Drawing.Point(1, 216);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(885, 390);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Detalles";
            // 
            // TxtDMN
            // 
            this.TxtDMN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtDMN.BackColor = System.Drawing.Color.Khaki;
            this.TxtDMN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtDMN.Location = new System.Drawing.Point(413, 364);
            this.TxtDMN.Name = "TxtDMN";
            this.TxtDMN.ReadOnly = true;
            this.TxtDMN.Size = new System.Drawing.Size(122, 21);
            this.TxtDMN.TabIndex = 69;
            this.TxtDMN.TabStop = false;
            this.TxtDMN.Text = "0";
            this.TxtDMN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtHME
            // 
            this.TxtHME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtHME.BackColor = System.Drawing.Color.PapayaWhip;
            this.TxtHME.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHME.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtHME.Location = new System.Drawing.Point(739, 364);
            this.TxtHME.Name = "TxtHME";
            this.TxtHME.ReadOnly = true;
            this.TxtHME.Size = new System.Drawing.Size(88, 21);
            this.TxtHME.TabIndex = 68;
            this.TxtHME.TabStop = false;
            this.TxtHME.Text = "0";
            this.TxtHME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtHMN
            // 
            this.TxtHMN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtHMN.BackColor = System.Drawing.Color.Khaki;
            this.TxtHMN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtHMN.Location = new System.Drawing.Point(538, 364);
            this.TxtHMN.Name = "TxtHMN";
            this.TxtHMN.ReadOnly = true;
            this.TxtHMN.Size = new System.Drawing.Size(102, 21);
            this.TxtHMN.TabIndex = 67;
            this.TxtHMN.TabStop = false;
            this.TxtHMN.Text = "0";
            this.TxtHMN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtDME
            // 
            this.TxtDME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtDME.BackColor = System.Drawing.Color.PapayaWhip;
            this.TxtDME.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDME.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtDME.Location = new System.Drawing.Point(642, 364);
            this.TxtDME.Name = "TxtDME";
            this.TxtDME.ReadOnly = true;
            this.TxtDME.Size = new System.Drawing.Size(95, 21);
            this.TxtDME.TabIndex = 66;
            this.TxtDME.TabStop = false;
            this.TxtDME.Text = "0";
            this.TxtDME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(361, 365);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 70;
            this.label3.Text = "Totales";
            // 
            // txtpatrimonionetodesc
            // 
            this.txtpatrimonionetodesc.Enabled = false;
            this.txtpatrimonionetodesc.EnterMoveNextControl = true;
            this.txtpatrimonionetodesc.Location = new System.Drawing.Point(381, 65);
            this.txtpatrimonionetodesc.Name = "txtpatrimonionetodesc";
            this.txtpatrimonionetodesc.Size = new System.Drawing.Size(327, 20);
            this.txtpatrimonionetodesc.TabIndex = 24;
            // 
            // txtpatrimonionetocod
            // 
            this.txtpatrimonionetocod.EnterMoveNextControl = true;
            this.txtpatrimonionetocod.Location = new System.Drawing.Point(341, 65);
            this.txtpatrimonionetocod.MenuManager = this.barManager1;
            this.txtpatrimonionetocod.Name = "txtpatrimonionetocod";
            this.txtpatrimonionetocod.Size = new System.Drawing.Size(38, 20);
            this.txtpatrimonionetocod.TabIndex = 23;
            this.txtpatrimonionetocod.TextChanged += new System.EventHandler(this.txtpatrimonionetocod_TextChanged);
            this.txtpatrimonionetocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpatrimonionetocod_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(227, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Cod. Patrimonio Neto";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(391, 48);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(24, 13);
            this.labelControl18.TabIndex = 49;
            this.labelControl18.Text = "Tipo:";
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Location = new System.Drawing.Point(804, 55);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(75, 28);
            this.btnanadirdet.TabIndex = 28;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.Location = new System.Drawing.Point(724, 55);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(75, 28);
            this.btneditardet.TabIndex = 60;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.Location = new System.Drawing.Point(804, 26);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(75, 28);
            this.btnquitardet.TabIndex = 56;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Location = new System.Drawing.Point(724, 26);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(75, 28);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(4, 115);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(876, 226);
            this.dgvdatos.TabIndex = 45;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Cuenta";
            this.gridColumn1.FieldName = "Ctb_Cuenta";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 104;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Cuenta descripcion";
            this.gridColumn2.FieldName = "Ctb_Cuenta_Desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 206;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Centro Costo/Gasto";
            this.gridColumn3.FieldName = "Ctb_Centro_CG_Desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 110;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "Debe";
            this.gridColumn4.DisplayFormat.FormatString = "n2";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn4.FieldName = "Ctb_Importe_Debe";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe", "{0:n2}")});
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 125;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Haber";
            this.gridColumn5.DisplayFormat.FormatString = "n2";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "Ctb_Importe_Haber";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber", "{0:n2}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 101;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.Caption = "Debe Extr.";
            this.gridColumn6.DisplayFormat.FormatString = "n2";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "Ctb_Importe_Debe_Extr";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe_Extr", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.Caption = "Haber Extr.";
            this.gridColumn7.DisplayFormat.FormatString = "n2";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "Ctb_Importe_Haber_Extr";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber_Extr", "{0:n2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.Caption = "Patrimonio neto";
            this.gridColumn8.FieldName = "Patri_Neto_Desc";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            this.gridColumn8.Width = 169;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn9.Caption = "Cta_Destino";
            this.gridColumn9.FieldName = "Cta_Destino";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Flujo de efectivo";
            this.gridColumn10.FieldName = "Estado_Flujo_Efecctivo_Desc_Det";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            this.gridColumn10.Width = 196;
            // 
            // txtimporte
            // 
            this.txtimporte.EnterMoveNextControl = true;
            this.txtimporte.Location = new System.Drawing.Point(121, 64);
            this.txtimporte.MenuManager = this.barManager1;
            this.txtimporte.Name = "txtimporte";
            this.txtimporte.Properties.Mask.EditMask = "n2";
            this.txtimporte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporte.Size = new System.Drawing.Size(100, 20);
            this.txtimporte.TabIndex = 21;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(73, 70);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(42, 13);
            this.labelControl16.TabIndex = 20;
            this.labelControl16.Text = "Importe:";
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(466, 44);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodesc.Size = new System.Drawing.Size(242, 20);
            this.txttipodesc.TabIndex = 15;
            // 
            // txtccgdesc
            // 
            this.txtccgdesc.Enabled = false;
            this.txtccgdesc.EnterMoveNextControl = true;
            this.txtccgdesc.Location = new System.Drawing.Point(166, 44);
            this.txtccgdesc.Name = "txtccgdesc";
            this.txtccgdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtccgdesc.Size = new System.Drawing.Size(213, 20);
            this.txtccgdesc.TabIndex = 6;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(15, 47);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(100, 13);
            this.labelControl12.TabIndex = 4;
            this.labelControl12.Text = "Centro Costo/Gasto:";
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(421, 44);
            this.txttipo.Name = "txttipo";
            this.txttipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipo.Size = new System.Drawing.Size(43, 20);
            this.txttipo.TabIndex = 14;
            this.txttipo.TextChanged += new System.EventHandler(this.txttipo_TextChanged);
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipo_KeyDown);
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(205, 23);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuentadesc.Size = new System.Drawing.Size(503, 20);
            this.txtcuentadesc.TabIndex = 3;
            // 
            // txtccgcod
            // 
            this.txtccgcod.EnterMoveNextControl = true;
            this.txtccgcod.Location = new System.Drawing.Point(121, 44);
            this.txtccgcod.Name = "txtccgcod";
            this.txtccgcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtccgcod.Size = new System.Drawing.Size(43, 20);
            this.txtccgcod.TabIndex = 5;
            this.txtccgcod.TextChanged += new System.EventHandler(this.txtccgcod_TextChanged);
            this.txtccgcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtccgcod_KeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(75, 26);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(39, 13);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "Cuenta:";
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(121, 23);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuenta.Size = new System.Drawing.Size(82, 20);
            this.txtcuenta.TabIndex = 2;
            this.txtcuenta.TabStopChanged += new System.EventHandler(this.txtcuenta_TabStopChanged);
            this.txtcuenta.TextChanged += new System.EventHandler(this.txtcuenta_TextChanged);
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl8.Location = new System.Drawing.Point(886, 32);
            this.barDockControl8.Manager = null;
            this.barDockControl8.Size = new System.Drawing.Size(0, 575);
            // 
            // frm_diario_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(886, 607);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControl8);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_diario_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Diario";
            this.Load += new System.EventHandler(this.frm_diario_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpatrimonionetodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpatrimonionetocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtccgdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtccgcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtentidad;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtcuenta;
        private DevExpress.XtraEditors.TextEdit txtccgdesc;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtccgcod;
        private DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.TextEdit txttipo;
        private DevExpress.XtraEditors.TextEdit txtimporte;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnanadirdet;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.SimpleButton btnquitardet;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.TextEdit txtpatrimonionetodesc;
        private DevExpress.XtraEditors.TextEdit txtpatrimonionetocod;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraEditors.TextEdit txtflujoefectivodesc;
        private DevExpress.XtraEditors.TextEdit txtflujoefectivocod;
        internal System.Windows.Forms.TextBox TxtDMN;
        internal System.Windows.Forms.TextBox TxtHME;
        internal System.Windows.Forms.TextBox TxtHMN;
        internal System.Windows.Forms.TextBox TxtDME;
        internal System.Windows.Forms.Label label3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
    }
}
