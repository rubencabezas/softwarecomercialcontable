﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Diario
{
    public partial class frm_diario : Contable.frm_fuente
    {
        public frm_diario()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


            //if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            //{
                           try
                                {
             using (frm_diario_edicion f = new frm_diario_edicion())
                        {


                            Estado = Estados.Nuevo;
                            f.Estado_Ven_Boton = "1";
                           f.Id_Periodo = Actual_Conexion.PeriodoSelect;

                            if (f.ShowDialog() == DialogResult.OK)
                            {

                                    Listar();

                                }
                                else
                                {
                                    Listar();
                                }

                        }
                                }
                                catch (Exception ex)
                                {
                                   Accion.ErrorSistema(ex.Message);
                                }

            //}
 


        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;
            try
            {
                Lista = log.Listar(Ent);

                dgvdatos.DataSource = null;

                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        string Id_Libro;
        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Diario(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_Diario;

                }
                else
                {
                    MessageBox.Show("");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_diario_Load(object sender, EventArgs e)
        {
            TraerLibro();
            Listar();
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            //{
                            try
                                {
              using (frm_diario_edicion f = new frm_diario_edicion())
                        {


                            Estado = Estados.Modificar;
                            f.Estado_Ven_Boton = "2";

                            f.Id_Empresa = Entidad.Id_Empresa;
                            f.Id_Anio = Entidad.Id_Anio;
                            f.Id_Periodo = Entidad.Id_Periodo;
                            f.Id_Libro = Entidad.Id_Libro;
                            f.Voucher = Entidad.Id_Voucher;

                            if (f.ShowDialog() == DialogResult.OK)
                            {


           


                            }
                            else
                            {
                                Listar();
                            }
                        }
                                }
                                catch (Exception ex)
                                {
                                    Accion.ErrorSistema(ex.Message);
                                }

            //}





        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Anio = Entidad.Id_Anio,
                        Id_Periodo = Entidad.Id_Periodo,
                        Id_Voucher = Entidad.Id_Voucher,
                        Id_Libro = Entidad.Id_Libro
                    };

                    Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Diario" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TraerLibro();
            Listar();
        }
    }
}
