﻿using Contable._1_Busquedas_Generales;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Diario
{
    public partial class frm_diario_edicion : Contable.frm_fuente
    {
        public frm_diario_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;

        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();
        List<Entidad_Movimiento_Cab> DetallesDocRef = new List<Entidad_Movimiento_Cab>();
        List<Entidad_Movimiento_Cab> Detalles_ADM= new List<Entidad_Movimiento_Cab>();
 List<Entidad_Movimiento_Cab> Detalles_Orden_Compra = new List<Entidad_Movimiento_Cab>();

        public void LimpiarDet()
        {
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();
            //txttipoopecod.ResetText();
            //txttipoopedesc.ResetText();

            txtccgcod.ResetText();
            txtccgdesc.ResetText();
            txttipo.ResetText();
            //txttipodoc.ResetText();
            //txttipodesc.ResetText();
            txtimporte.ResetText();

            txtpatrimonionetocod.ResetText();
            txtpatrimonionetodesc.ResetText();

            txtflujoefectivocod.ResetText();
            txtflujoefectivodesc.ResetText();

            //Detalles.Clear();

        }

        public void LimpiarCab()
        {
            txtlibro.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();
          
            txtfechadoc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();

            //txtflujoefectivocod.ResetText();
            //txtflujoefectivodesc.ResetText();


        }


        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                LimpiarDet();
                Detalles.Clear();
                dgvdatos.DataSource = null;
                BloquearDetalles();
                TraerLibro();
                txtglosa.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                txtcuenta.Tag = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }



        public void HabilitarDetalles()
        {
            txtcuenta.Enabled = true;
           

            txtccgcod.Enabled = true;
            txttipo.Enabled = true;
            txtimporte.Enabled = true;
            txtpatrimonionetocod.Enabled = true;
            txtflujoefectivocod.Enabled = true;

            txtcuenta.Focus();
        }


        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtcuenta.Focus();
                return false;
            }


            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

            if ((Convert.ToDouble(gridView1.Columns["Ctb_Importe_Debe"].SummaryItem.SummaryValue)) != (Convert.ToDouble(gridView1.Columns["Ctb_Importe_Haber"].SummaryItem.SummaryValue)))
            {
                Accion.Advertencia("Los Montos Totales del Debe y el Haber no son iguales");
                return false;
            }


                return true;
        }



        public bool VerificarDetalle()
        {
            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una cuenta contable");
                txtcuenta.Focus();
                return false;
            }
       
  
            if (string.IsNullOrEmpty(txttipodesc.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un tipo (Debe / Haber)");
                txttipo.Focus();
                return false;
            }

            if (Convert.ToDouble(txtimporte.Text) <= 0)
            {
                //tools.ShowWarnig("VALOR INCORRECTO EN CANTIDAD", "Debe de ingresar un valor numérico mayor a CERO en el campo cantidad antes de grabar");
                Accion.Advertencia("Debe ingresar un importe mayor a cero");
                txtimporte.Focus();
                return false;
            }



            return true;
        }

        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1.000";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocod.Text))
                    {
                        txttipocambiocod.Enabled = true;
                        txttipocambiocod.ResetText();
                        txttipocambiodesc.ResetText();
                        txttipocambiovalor.ResetText();
                        txttipocambiovalor.Enabled = true;
                    }

                }
            }
        }


        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdni.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdni.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdni.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdni.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdni.EnterMoveNextControl = false;
                                txtrucdni.ResetText();
                                txtrucdni.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcondicioncod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtcondicioncod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcondicioncod.Text.Substring(txtcondicioncod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0003";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcondicioncod.Tag = Entidad.Id_General_Det;
                                txtcondicioncod.Text = Entidad.Gen_Codigo_Interno;
                                txtcondiciondesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcondicioncod.Text) & string.IsNullOrEmpty(txtcondiciondesc.Text))
                    {
                        BuscarCondicion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarCondicion()
        {
            try
            {
                txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = txtcondicioncod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtcondicioncod.Text.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool Es_moneda_nac;
        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Tag = Entidad.Id_Moneda;

                                txtmonedacod.Text = Entidad.Id_Sunat;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                                VerificarMoneda();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Diario(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    txtlibro.Tag = Generales[0].Ini_Diario;
                    txtlibro.Text = Generales[0].Ini_Diario_Desc;
                }
                else
                {
                    MessageBox.Show("");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BloquearDetalles()
        {
            txtcuenta.Enabled = false;
     
            txtccgcod.Enabled = false;
            txttipo.Enabled = false;
            txtimporte.Enabled = false;
            txtpatrimonionetocod.Enabled = false;
            txtflujoefectivocod.Enabled = false;
        }

        private void frm_diario_edicion_Load(object sender, EventArgs e)
        {
            TraerLibro();
            BloquearDetalles();
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();
            }
        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = txtlibro.Tag.ToString();
                    Voucher = Enti.Id_Voucher;

                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;


                    txtserie.Text = Enti.Ctb_Serie +"-" + Enti.Ctb_Numero;
                    //txtnumero.Text = Enti.Ctb_Numero;



                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento); 
                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtcondicioncod.Tag = Enti.Ctb_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ctb_Condicion_Interno;
                    txtcondiciondesc.Text = Enti.Ctb_Condicion_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;

                
                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    //txtflujoefectivocod.Text = Enti.Flujo_Efectivo_Cod;
                    //txtflujoefectivodesc.Text = Enti.Flujo_Efectivo_Desc;

                    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();
                    dgvdatos.DataSource = null;
                    Detalles = log_det.Listar_Det(Enti);

                    if (Detalles.Count > 0)
                    {
                        decimal SumaDebeNac = 0, SumaDebeExt = 0, SumaHaberNac = 0, SumaHaberExt = 0;

                        foreach (Entidad_Movimiento_Cab Asi in Detalles)
                        {
                            if ((Asi.Cta_Destino != true))
                            {
                                SumaDebeNac += Asi.Ctb_Importe_Debe;
                                SumaDebeExt += Asi.Ctb_Importe_Debe_Extr;
                                SumaHaberNac += Asi.Ctb_Importe_Haber;
                                SumaHaberExt += Asi.Ctb_Importe_Haber_Extr;
                            }


                        }

                        dgvdatos.DataSource = Detalles;

                        TxtDMN.Text = String.Format("{0:0,0.00}", SumaDebeNac);// Format(SumaDebeNac, );
                        TxtDME.Text = String.Format("{0:0,0.00}", SumaDebeExt);
                        TxtHMN.Text = String.Format("{0:0,0.00}", SumaHaberNac);
                        TxtHME.Text = String.Format("{0:0,0.00}", SumaHaberExt);

                    }

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }


        private void txtserie_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtserie.Text))
                {
                    string Serie_Numero = txtserie.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        //txtserie.Text = Accion.Formato(txtserie.Text, 4);
                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtserie.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtnumero.Text))
            //{
            //    txtnumero.Text = Accion.Formato(txtnumero.Text, 8);
            //}
        }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {

            if (VerificarNuevoDet())
            {
                EstadoDetalle = Estados.Nuevo;

                HabilitarDetalles();
            }
         
        }


        public bool VerificarNuevoDet()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }



            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

    

            return true;
        }


        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }
        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {
  if (VerificarDetalle())
            {
                Entidad_Movimiento_Cab ItemDetalle = new Entidad_Movimiento_Cab();

                ItemDetalle.Id_Item = Convert.ToInt32(txtcuenta.Tag.ToString());

                ItemDetalle.Ctb_Cuenta = txtcuenta.Text.ToString();
                ItemDetalle.Ctb_Cuenta_Desc = txtcuentadesc.Text.Trim().ToString();



                ItemDetalle.Ctb_Centro_CG = txtccgcod.Text.Trim().ToString();
                ItemDetalle.Ctb_Centro_CG_Desc = txtccgdesc.Text.Trim().ToString();

                ItemDetalle.Ctb_Tipo_DH = IIf(txttipo.Tag == null, "", txttipo.Tag).ToString();// txttipo.Tag.ToString();
                ItemDetalle.CCtb_Tipo_DH_Interno = txttipo.Text.ToString();
                ItemDetalle.Ctb_Tipo_DH_Desc = txttipodesc.Text.Trim().ToString();

                ItemDetalle.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadoc.Text);
                ItemDetalle.Ctb_Ruc_dni_det = txtrucdni.Text.ToString().Trim();
                ItemDetalle.Ctb_Tipo_Doc_det = txttipodoc.Tag.ToString();
                ItemDetalle.Ctb_moneda_cod_det = txtmonedacod.Tag.ToString();
                ItemDetalle.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocod.Text;
                ItemDetalle.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodesc.Text;
                ItemDetalle.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text);
                //ItemDetalle.Ctb_Tipo_Doc_det = txttipodoc.Text;

                string SerNum = txtserie.Text;
                string[] Datos = SerNum.Split(Convert.ToChar("-"));

                ItemDetalle.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                ItemDetalle.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();



                ItemDetalle.Patri_Neto_Cod = txtpatrimonionetocod.Text;
                ItemDetalle.Patri_Neto_Desc = txtpatrimonionetodesc.Text;

                ItemDetalle.Estado_Flujo_Efecctivo_Cod_Det = txtflujoefectivocod.Text;
                ItemDetalle.Estado_Flujo_Efecctivo_Desc_Det = txtflujoefectivodesc.Text;


                    if (ItemDetalle.Ctb_Tipo_DH == "0004")
                    {
                        if (Es_moneda_nac == true)
                        {
                            //txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");

                            ItemDetalle.Ctb_Importe_Debe = Convert.ToDecimal(txtimporte.Text);
                        }
                        else
                        {
                            ItemDetalle.Ctb_Importe_Debe = Convert.ToDecimal(txtimporte.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                            ItemDetalle.Ctb_Importe_Debe_Extr = Convert.ToDecimal(txtimporte.Text);

                            //ItemDetalle.Ctb_Importe_Debe = Convert.ToDecimal(txtimportecontable.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                            //ItemDetalle.Ctb_Importe_Debe_Extr = Convert.ToDecimal(txtimportecontable.Text.ToString());

                        }


                    }
                    else if (ItemDetalle.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            ItemDetalle.Ctb_Importe_Haber =Convert.ToDecimal(txtimporte.Text);
                        }
                        else
                        {
                            ItemDetalle.Ctb_Importe_Haber = Convert.ToDecimal(txtimporte.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                            ItemDetalle.Ctb_Importe_Haber_Extr =Convert.ToDecimal(txtimporte.Text);

                            //ItemDetalle.Ctb_Importe_Debe = Convert.ToDecimal(txtimportecontable.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                            //ItemDetalle.Ctb_Importe_Debe_Extr = Convert.ToDecimal(txtimportecontable.Text.ToString());

                        }


                    }




                    if (EstadoDetalle == Estados.Nuevo)
                {
                    Detalles.Add(ItemDetalle);
                    UpdateGrilla();
                    EstadoDetalle = Estados.Guardado;
                }
                else if (EstadoDetalle == Estados.Modificar)
                {

                    Detalles[Convert.ToInt32(txtcuenta.Tag) - 1] = ItemDetalle;
                    UpdateGrilla();
                    EstadoDetalle = Estados.Guardado;

                }

                    btnnuevodet.Focus();
            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

          
        }

        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdatos.DataSource = Detalles;



                decimal SumaDebeNac = 0, SumaDebeExt = 0, SumaHaberNac = 0, SumaHaberExt = 0;

                foreach (Entidad_Movimiento_Cab Asi in Detalles)
                {
                    if ((Asi.Cta_Destino != true))
                    {
                        SumaDebeNac += Asi.Ctb_Importe_Debe;
                        SumaDebeExt += Asi.Ctb_Importe_Debe_Extr;
                        SumaHaberNac += Asi.Ctb_Importe_Haber;
                        SumaHaberExt += Asi.Ctb_Importe_Haber_Extr;
                    }


                }

                dgvdatos.DataSource = Detalles;

                TxtDMN.Text = String.Format("{0:0,0.00}", SumaDebeNac);// Format(SumaDebeNac, );
                TxtDME.Text = String.Format("{0:0,0.00}", SumaDebeExt);
                TxtHMN.Text = String.Format("{0:0,0.00}", SumaHaberNac);
                TxtHME.Text = String.Format("{0:0,0.00}", SumaHaberExt);


            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta.Trim();
                                txtcuentadesc.Text = Entidad.Cta_Descripcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Empresa=Actual_Conexion.CodigoEmpresa,
                    Id_Anio=Actual_Conexion.AnioSelect,
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion.Trim();
                            txtcuenta.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    txtcuenta.EnterMoveNextControl = false;
                    Accion.Advertencia("No se encontró la cuenta contable");
                    txtcuenta.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtccgcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtccgcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtccgcod.Text.Substring(txtccgcod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_centro_costo_gasto_busqueda f = new _1_Busquedas_Generales.frm_centro_costo_gasto_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Centro_Costo_Gasto Entidad = new Entidad_Centro_Costo_Gasto();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtccgcod.Text = Entidad.Id_Centro_cg;
                                txtccgdesc.Text = Entidad.Ccg_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtccgcod.Text) & string.IsNullOrEmpty(txtccgdesc.Text))
                    {
                        BuscarCentroCostogasto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarCentroCostogasto()
        {
            try
            {
                txtccgcod.Text = Accion.Formato(txtccgcod.Text, 3);

                Logica_Centro_Costo_Gasto log = new Logica_Centro_Costo_Gasto();

                List<Entidad_Centro_Costo_Gasto> Generales = new List<Entidad_Centro_Costo_Gasto>();

                Generales = log.Listar(new Entidad_Centro_Costo_Gasto
                {
                    Id_Centro_cg = txtccgcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Centro_Costo_Gasto T in Generales)
                    {
                        if ((T.Id_Centro_cg).ToString().Trim().ToUpper() == txtccgcod.Text.Trim().ToUpper())
                        {
                            txtccgcod.Text = (T.Id_Centro_cg).ToString().Trim();
                            txtccgdesc.Text = T.Ccg_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

   

        private void txttipo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipo.Text.Substring(txttipo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0002";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipo.Tag = Entidad.Id_General_Det;
                                txttipo.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipo.Text) & string.IsNullOrEmpty(txttipodesc.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTipo()
        {
            try
            {
                txttipo.Text = Accion.Formato(txttipo.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0002",
                    Gen_Codigo_Interno = txttipo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipo.Text.Trim().ToUpper())
                        {
                            txttipo.Tag = T.Id_General_Det;
                            txttipo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesc.Text = T.Gen_Descripcion_Det;
                            txttipo.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipo.EnterMoveNextControl = false;
                    txttipo.ResetText();
                    txttipo.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            if (txtcuenta.Focus() == false)
            {
                txtcuentadesc.ResetText();
            }
        }

        private void txtccgcod_TextChanged(object sender, EventArgs e)
        {
            if (txtccgcod.Focus() == false)
            {
                txtccgdesc.ResetText();
            }
        }

        private void txttipo_TextChanged(object sender, EventArgs e)
        {
            if (txttipo.Focus() == false)
            {
                txttipodesc.ResetText();
            }
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    txtcuenta.Tag = Entidad.Id_Item;

                    txtcuenta.Text = Entidad.Ctb_Cuenta;
                    txtcuentadesc.Text = Entidad.Ctb_Cuenta_Desc;



                    txtccgcod.Text = Entidad.Ctb_Centro_CG;
                    txtccgdesc.Text = Entidad.Ctb_Centro_CG_Desc;

                    txttipo.Tag = Entidad.Ctb_Tipo_DH;
                    txttipo.Text = Entidad.CCtb_Tipo_DH_Interno;
                    txttipodesc.Text = Entidad.Ctb_Tipo_DH_Desc;

                    //txttipobsacod.Tag = Entidad.Ctb_Tipo_BSA;
                    //txttipobsacod.Text = Entidad.Ctb_Tipo_BSA_Interno;
                    //txttipobsadesc.Text = Entidad.Ctb_Tipo_BSA_Desc;

                    //txtproductocod.Text = Entidad.Ctb_Catalogo;
                    //txtproductodesc.Text = Entidad.Ctb_Catalogo_Desc;

                    //txtcantidad.Text = Convert.ToString( Entidad.Ctb_Cantidad);
                    //txtvalorunit.Text = Convert.ToString( Entidad.Ctb_Valor_Unit);

                    txtpatrimonionetocod.Text = Entidad.Patri_Neto_Cod;
                    txtpatrimonionetodesc.Text = Entidad.Patri_Neto_Desc;

                    txtflujoefectivocod.Text = Entidad.Estado_Flujo_Efecctivo_Cod_Det;
                    txtflujoefectivodesc.Text = Entidad.Estado_Flujo_Efecctivo_Desc_Det;


                    if (Entidad.Ctb_Tipo_DH == "0004")
                    {

                        if (Es_moneda_nac == true)
                        {
                            txtimporte.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe).ToString();
                        }
                        else
                        {
                            txtimporte.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe_Extr).ToString();
                        }

                    }
                    else if (Entidad.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            txtimporte.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber).ToString();
                        }
                        else
                        {
                            txtimporte.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber_Extr).ToString();
                        }

                    }



                    if (Estado_Ven_Boton == "1")
                    {
                        //EstadoDetalle = Estados.Consulta;
                        EstadoDetalle = Estados.SoloLectura;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle = Estados.SoloLectura;


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {
                Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;
                Ent.Id_Periodo = Id_Periodo;
                Ent.Id_Libro = txtlibro.Tag.ToString();
                Ent.Id_Voucher = Voucher;

                Ent.Ctb_Glosa = txtglosa.Text;
                Ent.Ctb_Tipo_Doc = txttipodoc.Tag.ToString();

                //Ent.Ctb_Serie = txtserie.Text;
                //Ent.Ctb_Numero = txtnumero.Text;

                string SerNum = txtserie.Text;
                string[] Datos = SerNum.Split(Convert.ToChar("-"));

                Ent.Ctb_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                Ent.Ctb_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);
                Ent.Ctb_Ruc_dni = txtrucdni.Text;
                    if (txtcondicioncod.Tag == null)
                    {
                        Ent.Ctb_Condicion_Cod = "";
                    }
                    else
                    {
                        Ent.Ctb_Condicion_Cod = txtcondicioncod.Tag.ToString();
                    }

                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;

                Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                Ent.Ctb_Base_Imponible = 0;
                Ent.Ctb_Igv = 0;
                Ent.Ctb_Importe_Total = 0;
                Ent.Ctb_Isc = false;
                Ent.Ctb_Isc_Importe = 0;
                Ent.Ctb_Otros_Tributos = false;
                Ent.Ctb_Otros_Tributos_Importe = 0;
                Ent.Ctb_No_Gravadas = 0;

               // Ent.Flujo_Efectivo_Cod = txtflujoefectivocod.Text;

                Ent.DetalleAsiento = Detalles;
                Ent.DetalleDoc_Ref = DetallesDocRef;
                Ent.DetalleADM = Detalles_ADM;
                    Ent.DetalleDoc_Orden_CS = Detalles_Orden_Compra;

                    if (Estado == Estados.Nuevo)
                {
                    if (Log.Insertar(Ent))
                    {
                        //Listar();
                        //frm_compras f = new frm_compras();
                        //f.dgvdatos.DataSource = Lista;
                        //f.dgvdatos.RefreshDataSource();
                        Accion.ExitoGuardar();
                        Estado = Estados.Nuevo;
                        LimpiarCab();
                        LimpiarDet();
                        Detalles.Clear();
                        dgvdatos.DataSource = null;
                        BloquearDetalles();
                        TraerLibro();
                        txtglosa.Focus();


                    }

                }
                else if (Estado == Estados.Modificar)
                {
                    if (Log.Modificar(Ent))
                    {
                        Accion.ExitoModificar();
                        this.Close();
                    }
                }
                }

        
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
            if (Detalles.Count > 0)
            {
               
                Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                dgvdatos.DataSource = null;
                if (Detalles.Count > 0)
                {
                    dgvdatos.DataSource = Detalles;
                    RefreshNumeral();
                }

                EstadoDetalle = Estados.Ninguno;
            }
            }
            catch (Exception ex) {
                Accion.ErrorSistema(ex.Message);
            }

       
        }

        public void RefreshNumeral()
        {
            int NumOrden = 1;
            foreach (Entidad_Movimiento_Cab Det in Detalles)
            {
                Det.Id_Item = NumOrden;
                NumOrden += 1;
            }
        }

        private void txtcuenta_TabStopChanged(object sender, EventArgs e)
        {

        }



        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocod.Text) == false)
            {
                if (IsDate(txtfechadoc.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocod.Text.Substring(txttipocambiocod.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadoc.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocod.Text = Entidad.Codigo;
                                    txttipocambiodesc.Text = Entidad.Nombre;
                                    txttipocambiovalor.Text = Convert.ToDouble(Entidad.Valor).ToString();// Convert.ToDouble(Entidad.Valor).ToString();
                                    //if (txttipocambiocod.Text == "OTR")
                                    //{
                                        txttipocambiovalor.Enabled = true;
                                    //}
                                    //else
                                    //{
                                    //    txttipocambiovalor.Enabled = false;
                                    //}

                                    txttipocambiocod.EnterMoveNextControl = true;
                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocod.Text) & string.IsNullOrEmpty(txttipocambiodesc.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadoc.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocod.Text = T.Codigo;
                                            txttipocambiodesc.Text = T.Nombre;
                                            txttipocambiovalor.Text =  Convert.ToDouble(T.Valor).ToString();
                                            //if (txttipocambiocod.Text == "OTR")
                                            //{
                                                txttipocambiovalor.Enabled = true;
                                            //}
                                            //else
                                            //{
                                            //    txttipocambiovalor.Enabled = false;
                                            //}
                                            txttipocambiocod.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadoc.Focus();

                }

            }
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocod.Focus() == false)
            {
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
            }
        }

        private void txtvalorunit_KeyDown(object sender, KeyEventArgs e)
        {
         
        }

        private void txtvalorunit_TextChanged(object sender, EventArgs e)
        {
            //if (txtvalorunit.Focus() == false)
            //{
            //    txtvalorunit.ResetText();
            //    //txtimporte.ResetText();
            //    txtvalorunit.Focus();
            //}
        }



        private void txtcantidad_TextChanged(object sender, EventArgs e)
        {
            //if (txtcantidad.Focus() == false)
            //{
            //    txtcantidad.ResetText();
            //    //txtimporte.ResetText();
            //    txtcantidad.Focus();
            //}
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txtpatrimonionetocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpatrimonionetocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpatrimonionetocod.Text.Substring(txtpatrimonionetocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_patrimonio_neto_busqueda f = new _1_Busquedas_Generales.frm_patrimonio_neto_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Flujo_Efectivo Entidad = new Entidad_Flujo_Efectivo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpatrimonionetocod.Text = Entidad.Codigo;
                                txtpatrimonionetodesc.Text = Entidad.Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpatrimonionetocod.Text) & string.IsNullOrEmpty(txtpatrimonionetodesc.Text))
                    {
                        BuscarPatrimonioNeto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarPatrimonioNeto()
        {
            try
            {
                txtpatrimonionetocod.Text = Accion.Formato(txtpatrimonionetocod.Text, 3);

                Logica_Flujo_Efectivo log = new Logica_Flujo_Efectivo();

                List<Entidad_Flujo_Efectivo> Generales = new List<Entidad_Flujo_Efectivo>();

                Generales = log.Patrimonio_Neto_Listar(new Entidad_Flujo_Efectivo
                {
                    Codigo = txtpatrimonionetocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Flujo_Efectivo T in Generales)
                    {
                        if ((T.Codigo).ToString().Trim().ToUpper() == txtpatrimonionetocod.Text.Trim().ToUpper())
                        {
                            txtpatrimonionetocod.Text = (T.Codigo).ToString().Trim();
                            txtpatrimonionetodesc.Text = T.Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtpatrimonionetocod_TextChanged(object sender, EventArgs e)
        {
            if (txtpatrimonionetocod.Focus() == false)
            {
                txtpatrimonionetodesc.ResetText();
            }
        }

        private void txtflujoefectivocod_TextChanged(object sender, EventArgs e)
        {
            if (txtflujoefectivocod.Focus() == false)
            {
                txtflujoefectivodesc.ResetText();
            }
        }

        private void txtflujoefectivocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtflujoefectivocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtflujoefectivocod.Text.Substring(txtflujoefectivocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_flujo_efectivo_busqueda f = new frm_flujo_efectivo_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Flujo_Efectivo Entidad = new Entidad_Flujo_Efectivo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtflujoefectivocod.Text = Entidad.Flujo_Efectivo_Cod;
                                txtflujoefectivodesc.Text = Entidad.Flujo_Efectivo_Desc;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtflujoefectivocod.Text) & string.IsNullOrEmpty(txtflujoefectivodesc.Text))
                    {
                        BuscarFlujoEfectivo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarFlujoEfectivo()
        {
            try
            {
                txtflujoefectivocod.Text = Accion.Formato(txtflujoefectivocod.Text, 6);

                Logica_Flujo_Efectivo log = new Logica_Flujo_Efectivo();

                List<Entidad_Flujo_Efectivo> Generales = new List<Entidad_Flujo_Efectivo>();

                Generales = log.Patrimonio_Neto_Listar(new Entidad_Flujo_Efectivo
                {
                    Codigo = txtflujoefectivocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Flujo_Efectivo T in Generales)
                    {
                        if ((T.Codigo).ToString().Trim().ToUpper() == txtflujoefectivocod.Text.Trim().ToUpper())
                        {
                            txtflujoefectivocod.Text = (T.Flujo_Efectivo_Cod).ToString().Trim();
                            txtflujoefectivodesc.Text = T.Flujo_Efectivo_Desc;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

 
        private void btnbuscarprovision_Click(object sender, EventArgs e)
        {

        }

        private void gridView2_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {

        }

        private void btnAgregarCtaPrincipal_Click(object sender, EventArgs e)
        {

        }

        private void btnnuevodet_Click_1(object sender, EventArgs e)
        {

        }

        private void btnbuscarprovisioncab_Click(object sender, EventArgs e)
        {

        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;

            if ((e.RowHandle >= 0))
            {
                //bool Diferencia = Convert.ToBoolean(View.GetRowCellDisplayText(e.RowHandle, View.Columns["Cta_Destino"]));
                bool iCheckSelection = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, "Cta_Destino"));
                if ((iCheckSelection == true))
                {
                    e.Appearance.BackColor = Color.FromArgb(128, 153, 195);
                }
            }
        }

        private void txtrucdni_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdni.Focus() == false)
            {
                txtentidad.ResetText();
            }
        }

        private void txtcondicioncod_TextChanged(object sender, EventArgs e)
        {
            if (txtcondicioncod.Focus() == false)
            {
                txtcondiciondesc.ResetText();

            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            //if (txtmonedacod.Focus() == false)
            //{
            //    txtmonedadesc.ResetText();
            //    txttipocambiocod.ResetText();
            //    txttipocambiodesc.ResetText();
            //    txttipocambiovalor.Text = "0.000";

            //}

            if (txtmonedacod.Focus() == false)
            {
                txtmonedadesc.ResetText();
                txttipocambiocod.ResetText();
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
                txttipocambiovalor.Enabled = false;
                txtmonedacod.Focus();

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
