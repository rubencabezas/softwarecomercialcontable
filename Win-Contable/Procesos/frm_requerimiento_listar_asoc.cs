﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos
{
    public partial class frm_requerimiento_listar_asoc : Contable.frm_fuente
    {
        public frm_requerimiento_listar_asoc()
        {
            InitializeComponent();
        }

        public string Empresa;
        public string Anio;
     public string Periodo;
        public string Tipo_orden;
        public string Folio;
        public string Proceso;
   

        List<Entidad_Requerimiento> Detalles_Orden_Compra = new List<Entidad_Requerimiento>();
        public List<Entidad_Requerimiento> Detalles_Orden_Compra_l = new List<Entidad_Requerimiento>();
        public void buscar2(List<Entidad_Orden_C_S> Ent)
        {
            try
            {
                dgvdatos.DataSource = null;
                foreach (Entidad_Orden_C_S x in Ent)
                {
                    Empresa = x.Id_Empresa;
                    Anio = x.Doc_Anio;
                    Periodo = x.Doc_Periodo;
                    Tipo_orden = x.Id_Tipo_Orden;
                    Folio = x.Doc_Folio;
                    Proceso = x.Doc_Proceso;


                    Logica_Requerimiento log = new Logica_Requerimiento();
                    Entidad_Requerimiento Bqda = new Entidad_Requerimiento();
                    // With...
                    Bqda.Id_Empresa = Empresa;
                    Bqda.Id_Anio = Anio;
                    Bqda.Id_Periodo = Periodo;
                     Bqda.Id_Movimiento = Folio;
   

                    Detalles_Orden_Compra_l = log.Listar(Bqda);
                    Entidad_Requerimiento Ent_orden = new Entidad_Requerimiento();

                    Ent_orden = Detalles_Orden_Compra_l[0];
                    Detalles_Orden_Compra.Add(Ent_orden);

                    if ((Detalles_Orden_Compra.Count > 0))
                    {
                        dgvdatos.DataSource = Detalles_Orden_Compra;
                        dgvdatos.Focus();
                    }
                    else
                    {
                        Accion.Advertencia("No se encontro ningun documento");
                        this.Close();
                    }


                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
