﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Inventario
{
    public partial class frm_entrada_provision : Contable.frm_fuente
    {
        public frm_entrada_provision()
        {
            InitializeComponent();
        }

        //public string Id_Empresa, Id_Anio, Id_Periodo, Id_Tipo_Mov, Id_Almacen;

        public string Empresa;
        public string Anio;
        public string Tipo_mov;
        public string Almacen;
        public string Folio;
        public string Proceso;
        public string Periodo;

        List<Entidad_Movimiento_Inventario> Detalles_Orden_Compra = new List<Entidad_Movimiento_Inventario>();
        public List<Entidad_Movimiento_Inventario> Detalles_Orden_Compra_l = new List<Entidad_Movimiento_Inventario>();
        public void buscar2(List<Entidad_Movimiento_Inventario> Ent)
        {
            try
            {
                dgvdatos.DataSource = null;
                foreach (Entidad_Movimiento_Inventario x in Ent)
                {
                    Empresa = x.Id_Empresa;
                    Anio = x.Doc_Anio;
                    Periodo = x.Doc_Periodo;
                    Tipo_mov = x.Id_Tipo_Mov;
                    Almacen = x.Id_Almacen;
                    Folio = x.Doc_Folio;
                    Proceso = x.Doc_Proceso;
          

                    Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();
                    Entidad_Movimiento_Inventario Bqda = new Entidad_Movimiento_Inventario();
                    // With...
                    Bqda.Id_Empresa = Empresa;
                    Bqda.Id_Anio = Anio;
                    Bqda.Id_Periodo = Periodo;
                    Bqda.Id_Tipo_Mov = Tipo_mov;
                    Bqda.Id_Almacen = Almacen;
                    Bqda.Doc_Folio = Folio;
                    Bqda.Proceso_Cod = Proceso;


                    Detalles_Orden_Compra_l = log.Buscar_Documentos_Referenciados(Bqda);
                    Entidad_Movimiento_Inventario Ent_orden = new Entidad_Movimiento_Inventario();

                    Ent_orden = Detalles_Orden_Compra_l[0];
                    Detalles_Orden_Compra.Add(Ent_orden);

                    if ((Detalles_Orden_Compra.Count > 0))
                    {
                        dgvdatos.DataSource = Detalles_Orden_Compra;
                        dgvdatos.Focus();
                    }
                    else
                    {
                        Accion.Advertencia("No se encontro ningun documento");
                        this.Close();
                    }


                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


        private void frm_entrada_provision_Load(object sender, EventArgs e)
        {
            
        }




        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }








    }
}
