﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Inventario
{
    public partial class frm_entrada_provision_buscar : Contable.frm_fuente
    {
        public frm_entrada_provision_buscar()
        {
            InitializeComponent();
        }

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Tipo_Mov, Id_Almacen;

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Lista.Count > 0)
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\EntradaProvision" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void frm_entrada_provision_buscar_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Movimiento_Inventario> Lista = new List<Entidad_Movimiento_Inventario>();
        public void Listar()
        {
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
            Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Tipo_Mov = Id_Tipo_Mov;
            Ent.Id_Almacen = Id_Almacen;
  

            try
            {
                Lista = log.Buscar_Doc_Dar_Ingreso(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
