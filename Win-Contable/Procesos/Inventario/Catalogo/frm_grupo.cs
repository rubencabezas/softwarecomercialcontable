﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_grupo : frm_fuente
    {
        public frm_grupo()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_grupo_edicion f = new frm_grupo_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Grupo Ent = new Entidad_Grupo();
                    Logica_Grupo Log = new Logica_Grupo();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Tipo = f.txttipobsacod.Tag.ToString() ;
                    Ent.Gru_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_grupo_edicion f = new frm_grupo_edicion())
            {
                f.txttipobsacod.Enabled = false;
                f.txttipobsacod.Tag = Entidad.Id_Tipo;
                f.txttipobsacod.Text = Entidad.Id_TipoCod;
                f.txttipobsadesc.Text = Entidad.Id_Tipo_Descripcion;

                f.txtdescripcion.Tag = Entidad.Id_Grupo;
                f.txtdescripcion.Text = Entidad.Gru_Descripcion;


                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Grupo Ent = new Entidad_Grupo();
                    Logica_Grupo Log = new Logica_Grupo();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Tipo = f.txttipobsacod.Tag.ToString(); //"0031";
                    Ent.Id_Grupo = f.txtdescripcion.Tag.ToString();
                    Ent.Gru_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }


        Entidad_Grupo Entidad = new Entidad_Grupo();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }


        public List<Entidad_Grupo> Lista = new List<Entidad_Grupo>();
        public void Listar()
        {
            Entidad_Grupo Ent = new Entidad_Grupo();
            Logica_Grupo log = new Logica_Grupo();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //Ent.Id_Tipo = "0031";
            Ent.Id_Grupo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_grupo_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La grupo a eliminar es el siguiente :" + Entidad.Gru_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Grupo Ent = new Entidad_Grupo
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Grupo=Entidad.Id_Grupo,
                        Id_Tipo=Entidad.Id_Tipo
                    };

                    Logica_Grupo log = new Logica_Grupo();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Grupo" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
