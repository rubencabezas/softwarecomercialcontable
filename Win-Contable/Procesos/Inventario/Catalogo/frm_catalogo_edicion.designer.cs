﻿namespace Comercial
{
    partial class frm_catalogo_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.txtfamiliadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtgrupodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.txtmarcadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.txtmarcacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtfamiliacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtgrupocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.Gbpresentacion = new System.Windows.Forms.GroupBox();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtunmdescDetFactorConver = new DevExpress.XtraEditors.TextEdit();
            this.txtunmdescDet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtunmcodFactorConver = new DevExpress.XtraEditors.TextEdit();
            this.txtunidadbase = new DevExpress.XtraEditors.TextEdit();
            this.txtunmcodDet = new DevExpress.XtraEditors.TextEdit();
            this.chkunmdefecto = new System.Windows.Forms.CheckBox();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnagregarDet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitarDet = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditarDet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevoDet = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatosunm = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnAgregarcuenta = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitarcuenta = new DevExpress.XtraEditors.SimpleButton();
            this.txtcuentaventa = new DevExpress.XtraEditors.TextEdit();
            this.dgvcuenta = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtcuentaventadesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuentacompradesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcuentacompra = new DevExpress.XtraEditors.TextEdit();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtoperacionVentaDesc = new DevExpress.XtraEditors.TextEdit();
            this.txtoperacionVentaCod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.chkfacturarsinexistencia = new DevExpress.XtraEditors.CheckEdit();
            this.chkLotes = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtlaboratorio = new DevExpress.XtraEditors.TextEdit();
            this.txtlaboratorioDesc = new DevExpress.XtraEditors.TextEdit();
            this.txtbarra = new DevExpress.XtraEditors.TextEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rchcomentarios = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnagregar = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitar = new DevExpress.XtraEditors.SimpleButton();
            this.btneditar = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevo = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gbespecificaciones = new System.Windows.Forms.GroupBox();
            this.txttipoespecifidesc = new DevExpress.XtraEditors.TextEdit();
            this.txtespecificacionDesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipoespecificod = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfamiliadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmarcadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmarcacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfamiliacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupocod.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.Gbpresentacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescDetFactorConver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodFactorConver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunidadbase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatosunm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentaventa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentaventadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentacompradesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentacompra.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtoperacionVentaDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtoperacionVentaCod.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkfacturarsinexistencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlaboratorio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlaboratorioDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbarra.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.gbespecificaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoespecifidesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtespecificacionDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoespecificod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(948, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 484);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(948, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(948, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // txtfamiliadesc
            // 
            this.txtfamiliadesc.Enabled = false;
            this.txtfamiliadesc.EnterMoveNextControl = true;
            this.txtfamiliadesc.Location = new System.Drawing.Point(139, 58);
            this.txtfamiliadesc.Name = "txtfamiliadesc";
            this.txtfamiliadesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfamiliadesc.Properties.Appearance.Options.UseFont = true;
            this.txtfamiliadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfamiliadesc.Size = new System.Drawing.Size(810, 22);
            this.txtfamiliadesc.TabIndex = 8;
            // 
            // txtgrupodesc
            // 
            this.txtgrupodesc.Enabled = false;
            this.txtgrupodesc.EnterMoveNextControl = true;
            this.txtgrupodesc.Location = new System.Drawing.Point(139, 35);
            this.txtgrupodesc.Name = "txtgrupodesc";
            this.txtgrupodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrupodesc.Properties.Appearance.Options.UseFont = true;
            this.txtgrupodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtgrupodesc.Size = new System.Drawing.Size(810, 22);
            this.txtgrupodesc.TabIndex = 5;
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.EnterMoveNextControl = true;
            this.txtdescripcion.Location = new System.Drawing.Point(99, 131);
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescripcion.Properties.Appearance.Options.UseFont = true;
            this.txtdescripcion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Size = new System.Drawing.Size(850, 22);
            this.txtdescripcion.TabIndex = 20;
            this.txtdescripcion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdescripcion_KeyPress);
            // 
            // txtmarcadesc
            // 
            this.txtmarcadesc.Enabled = false;
            this.txtmarcadesc.EnterMoveNextControl = true;
            this.txtmarcadesc.Location = new System.Drawing.Point(553, 83);
            this.txtmarcadesc.Name = "txtmarcadesc";
            this.txtmarcadesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmarcadesc.Properties.Appearance.Options.UseFont = true;
            this.txtmarcadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmarcadesc.Size = new System.Drawing.Size(396, 22);
            this.txtmarcadesc.TabIndex = 13;
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(99, 83);
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtserie.Properties.Appearance.Options.UseFont = true;
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(364, 22);
            this.txtserie.TabIndex = 10;
            // 
            // txtmarcacod
            // 
            this.txtmarcacod.EnterMoveNextControl = true;
            this.txtmarcacod.Location = new System.Drawing.Point(512, 83);
            this.txtmarcacod.Name = "txtmarcacod";
            this.txtmarcacod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmarcacod.Properties.Appearance.Options.UseFont = true;
            this.txtmarcacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmarcacod.Size = new System.Drawing.Size(39, 22);
            this.txtmarcacod.TabIndex = 12;
            this.txtmarcacod.EditValueChanged += new System.EventHandler(this.textEdit6_EditValueChanged);
            this.txtmarcacod.TextChanged += new System.EventHandler(this.txtmarcacod_TextChanged);
            this.txtmarcacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmarcacod_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(61, 86);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(35, 16);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "Serie:";
            // 
            // txtfamiliacod
            // 
            this.txtfamiliacod.EnterMoveNextControl = true;
            this.txtfamiliacod.Location = new System.Drawing.Point(99, 58);
            this.txtfamiliacod.Name = "txtfamiliacod";
            this.txtfamiliacod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfamiliacod.Properties.Appearance.Options.UseFont = true;
            this.txtfamiliacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfamiliacod.Size = new System.Drawing.Size(39, 22);
            this.txtfamiliacod.TabIndex = 7;
            this.txtfamiliacod.TextChanged += new System.EventHandler(this.txtfamiliacod_TextChanged);
            this.txtfamiliacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfamiliacod_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(469, 85);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(40, 16);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Marca:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(26, 135);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 16);
            this.labelControl3.TabIndex = 19;
            this.labelControl3.Text = "Descripcion:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(50, 61);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(46, 16);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Familia:";
            // 
            // txtgrupocod
            // 
            this.txtgrupocod.Enabled = false;
            this.txtgrupocod.EnterMoveNextControl = true;
            this.txtgrupocod.Location = new System.Drawing.Point(99, 35);
            this.txtgrupocod.MenuManager = this.barManager1;
            this.txtgrupocod.Name = "txtgrupocod";
            this.txtgrupocod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrupocod.Properties.Appearance.Options.UseFont = true;
            this.txtgrupocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtgrupocod.Size = new System.Drawing.Size(39, 22);
            this.txtgrupocod.TabIndex = 4;
            this.txtgrupocod.TextChanged += new System.EventHandler(this.txtgrupocod_TextChanged);
            this.txtgrupocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgrupocod_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(57, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 16);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Grupo:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 32);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(948, 452);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(940, 426);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Datos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Location = new System.Drawing.Point(3, 191);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(953, 242);
            this.tabControl2.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Gbpresentacion);
            this.tabPage4.Controls.Add(this.btnagregarDet);
            this.tabPage4.Controls.Add(this.btnquitarDet);
            this.tabPage4.Controls.Add(this.btnEditarDet);
            this.tabPage4.Controls.Add(this.btnnuevoDet);
            this.tabPage4.Controls.Add(this.dgvdatosunm);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(945, 216);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Unidad de medida";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // Gbpresentacion
            // 
            this.Gbpresentacion.Controls.Add(this.labelControl7);
            this.Gbpresentacion.Controls.Add(this.txtunmdescDetFactorConver);
            this.Gbpresentacion.Controls.Add(this.txtunmdescDet);
            this.Gbpresentacion.Controls.Add(this.labelControl11);
            this.Gbpresentacion.Controls.Add(this.txtunmcodFactorConver);
            this.Gbpresentacion.Controls.Add(this.txtunidadbase);
            this.Gbpresentacion.Controls.Add(this.txtunmcodDet);
            this.Gbpresentacion.Controls.Add(this.chkunmdefecto);
            this.Gbpresentacion.Controls.Add(this.labelControl9);
            this.Gbpresentacion.Location = new System.Drawing.Point(3, 3);
            this.Gbpresentacion.Name = "Gbpresentacion";
            this.Gbpresentacion.Size = new System.Drawing.Size(812, 58);
            this.Gbpresentacion.TabIndex = 1;
            this.Gbpresentacion.TabStop = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(506, 17);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(124, 13);
            this.labelControl7.TabIndex = 5;
            this.labelControl7.Text = "Unidad Factor Conversión";
            this.labelControl7.Visible = false;
            // 
            // txtunmdescDetFactorConver
            // 
            this.txtunmdescDetFactorConver.Enabled = false;
            this.txtunmdescDetFactorConver.EnterMoveNextControl = true;
            this.txtunmdescDetFactorConver.Location = new System.Drawing.Point(672, 12);
            this.txtunmdescDetFactorConver.Name = "txtunmdescDetFactorConver";
            this.txtunmdescDetFactorConver.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmdescDetFactorConver.Properties.Appearance.Options.UseFont = true;
            this.txtunmdescDetFactorConver.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmdescDetFactorConver.Size = new System.Drawing.Size(151, 22);
            this.txtunmdescDetFactorConver.TabIndex = 7;
            this.txtunmdescDetFactorConver.Visible = false;
            // 
            // txtunmdescDet
            // 
            this.txtunmdescDet.Enabled = false;
            this.txtunmdescDet.EnterMoveNextControl = true;
            this.txtunmdescDet.Location = new System.Drawing.Point(120, 12);
            this.txtunmdescDet.Name = "txtunmdescDet";
            this.txtunmdescDet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmdescDet.Properties.Appearance.Options.UseFont = true;
            this.txtunmdescDet.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmdescDet.Size = new System.Drawing.Size(191, 22);
            this.txtunmdescDet.TabIndex = 2;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(317, 15);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(103, 16);
            this.labelControl11.TabIndex = 3;
            this.labelControl11.Text = "Factor Conversion";
            // 
            // txtunmcodFactorConver
            // 
            this.txtunmcodFactorConver.EnterMoveNextControl = true;
            this.txtunmcodFactorConver.Location = new System.Drawing.Point(636, 12);
            this.txtunmcodFactorConver.Name = "txtunmcodFactorConver";
            this.txtunmcodFactorConver.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmcodFactorConver.Properties.Appearance.Options.UseFont = true;
            this.txtunmcodFactorConver.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmcodFactorConver.Size = new System.Drawing.Size(34, 22);
            this.txtunmcodFactorConver.TabIndex = 6;
            this.txtunmcodFactorConver.Visible = false;
            this.txtunmcodFactorConver.TextChanged += new System.EventHandler(this.txtunmcodFactorConver_TextChanged);
            this.txtunmcodFactorConver.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtunmcodFactorConver_KeyDown);
            // 
            // txtunidadbase
            // 
            this.txtunidadbase.EnterMoveNextControl = true;
            this.txtunidadbase.Location = new System.Drawing.Point(428, 11);
            this.txtunidadbase.Name = "txtunidadbase";
            this.txtunidadbase.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunidadbase.Properties.Appearance.Options.UseFont = true;
            this.txtunidadbase.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunidadbase.Properties.Mask.EditMask = "n4";
            this.txtunidadbase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtunidadbase.Size = new System.Drawing.Size(72, 22);
            this.txtunidadbase.TabIndex = 4;
            // 
            // txtunmcodDet
            // 
            this.txtunmcodDet.EnterMoveNextControl = true;
            this.txtunmcodDet.Location = new System.Drawing.Point(84, 12);
            this.txtunmcodDet.Name = "txtunmcodDet";
            this.txtunmcodDet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmcodDet.Properties.Appearance.Options.UseFont = true;
            this.txtunmcodDet.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmcodDet.Size = new System.Drawing.Size(34, 22);
            this.txtunmcodDet.TabIndex = 1;
            this.txtunmcodDet.TextChanged += new System.EventHandler(this.txtunmcod_TextChanged);
            this.txtunmcodDet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtunmcodDet_KeyDown);
            // 
            // chkunmdefecto
            // 
            this.chkunmdefecto.AutoSize = true;
            this.chkunmdefecto.Location = new System.Drawing.Point(84, 37);
            this.chkunmdefecto.Name = "chkunmdefecto";
            this.chkunmdefecto.Size = new System.Drawing.Size(85, 17);
            this.chkunmdefecto.TabIndex = 8;
            this.chkunmdefecto.Text = "Unidad base";
            this.chkunmdefecto.UseVisualStyleBackColor = true;
            this.chkunmdefecto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkunmdefecto_KeyPress);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(7, 15);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(71, 16);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "Unidad Med.";
            // 
            // btnagregarDet
            // 
            this.btnagregarDet.Location = new System.Drawing.Point(874, 35);
            this.btnagregarDet.Name = "btnagregarDet";
            this.btnagregarDet.Size = new System.Drawing.Size(47, 27);
            this.btnagregarDet.TabIndex = 2;
            this.btnagregarDet.Text = "Añadir";
            this.btnagregarDet.Click += new System.EventHandler(this.btnagregarDet_Click);
            // 
            // btnquitarDet
            // 
            this.btnquitarDet.Location = new System.Drawing.Point(874, 6);
            this.btnquitarDet.Name = "btnquitarDet";
            this.btnquitarDet.Size = new System.Drawing.Size(47, 27);
            this.btnquitarDet.TabIndex = 22;
            this.btnquitarDet.Text = "Quitar";
            this.btnquitarDet.Click += new System.EventHandler(this.btnquitarDet_Click);
            // 
            // btnEditarDet
            // 
            this.btnEditarDet.Location = new System.Drawing.Point(821, 35);
            this.btnEditarDet.Name = "btnEditarDet";
            this.btnEditarDet.Size = new System.Drawing.Size(47, 27);
            this.btnEditarDet.TabIndex = 22;
            this.btnEditarDet.Text = "Editar";
            this.btnEditarDet.Click += new System.EventHandler(this.btnEditarDet_Click);
            // 
            // btnnuevoDet
            // 
            this.btnnuevoDet.Location = new System.Drawing.Point(821, 6);
            this.btnnuevoDet.Name = "btnnuevoDet";
            this.btnnuevoDet.Size = new System.Drawing.Size(47, 27);
            this.btnnuevoDet.TabIndex = 0;
            this.btnnuevoDet.Text = "Nuevo";
            this.btnnuevoDet.Click += new System.EventHandler(this.btnnuevoDet_Click);
            // 
            // dgvdatosunm
            // 
            this.dgvdatosunm.Location = new System.Drawing.Point(3, 63);
            this.dgvdatosunm.MainView = this.gridView2;
            this.dgvdatosunm.MenuManager = this.barManager1;
            this.dgvdatosunm.Name = "dgvdatosunm";
            this.dgvdatosunm.Size = new System.Drawing.Size(936, 147);
            this.dgvdatosunm.TabIndex = 21;
            this.dgvdatosunm.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn7});
            this.gridView2.GridControl = this.dgvdatosunm;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Item";
            this.gridColumn4.FieldName = "Id_Item_Det";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 52;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Unidad de medida";
            this.gridColumn5.FieldName = "Unm_Desc_Det";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Base - Coef. - Fraccion";
            this.gridColumn6.FieldName = "Unm_Base";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 166;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Base - Coef Unm";
            this.gridColumn8.FieldName = "Unm_Coef_Base_Unidad_Medida_Descripcion";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowMove = false;
            this.gridColumn8.Width = 118;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Presentacion del producto";
            this.gridColumn9.FieldName = "Presentacion_Descripcion";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowMove = false;
            this.gridColumn9.Width = 156;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Unm x defecto";
            this.gridColumn7.FieldName = "Unm_Defecto";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 150;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox5);
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(945, 216);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Contable";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnAgregarcuenta);
            this.groupBox5.Controls.Add(this.btnquitarcuenta);
            this.groupBox5.Controls.Add(this.txtcuentaventa);
            this.groupBox5.Controls.Add(this.dgvcuenta);
            this.groupBox5.Controls.Add(this.txtcuentaventadesc);
            this.groupBox5.Controls.Add(this.labelControl13);
            this.groupBox5.Controls.Add(this.labelControl12);
            this.groupBox5.Controls.Add(this.txtcuentacompradesc);
            this.groupBox5.Controls.Add(this.txtcuentacompra);
            this.groupBox5.Location = new System.Drawing.Point(3, 51);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(930, 159);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            // 
            // btnAgregarcuenta
            // 
            this.btnAgregarcuenta.Location = new System.Drawing.Point(424, 60);
            this.btnAgregarcuenta.Name = "btnAgregarcuenta";
            this.btnAgregarcuenta.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarcuenta.TabIndex = 31;
            this.btnAgregarcuenta.Text = "Añadir";
            this.btnAgregarcuenta.Click += new System.EventHandler(this.btnAgregarcuenta_Click);
            // 
            // btnquitarcuenta
            // 
            this.btnquitarcuenta.Location = new System.Drawing.Point(343, 60);
            this.btnquitarcuenta.Name = "btnquitarcuenta";
            this.btnquitarcuenta.Size = new System.Drawing.Size(75, 23);
            this.btnquitarcuenta.TabIndex = 30;
            this.btnquitarcuenta.Text = "Quitar";
            // 
            // txtcuentaventa
            // 
            this.txtcuentaventa.EnterMoveNextControl = true;
            this.txtcuentaventa.Location = new System.Drawing.Point(119, 13);
            this.txtcuentaventa.MenuManager = this.barManager1;
            this.txtcuentaventa.Name = "txtcuentaventa";
            this.txtcuentaventa.Size = new System.Drawing.Size(81, 20);
            this.txtcuentaventa.TabIndex = 28;
            this.txtcuentaventa.TextChanged += new System.EventHandler(this.txtidanalisis_TextChanged);
            this.txtcuentaventa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtidanalisis_KeyDown);
            // 
            // dgvcuenta
            // 
            this.dgvcuenta.Location = new System.Drawing.Point(505, 14);
            this.dgvcuenta.MainView = this.gridView3;
            this.dgvcuenta.MenuManager = this.barManager1;
            this.dgvcuenta.Name = "dgvcuenta";
            this.dgvcuenta.Size = new System.Drawing.Size(419, 139);
            this.dgvcuenta.TabIndex = 1;
            this.dgvcuenta.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView3.GridControl = this.dgvcuenta;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Año";
            this.gridColumn10.FieldName = "Id_Anio";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 69;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Cuenta Venta";
            this.gridColumn11.FieldName = "Id_Cuenta_Venta";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 99;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Cuenta Compra";
            this.gridColumn12.FieldName = "Id_Cuenta_Compra";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 893;
            // 
            // txtcuentaventadesc
            // 
            this.txtcuentaventadesc.Enabled = false;
            this.txtcuentaventadesc.Location = new System.Drawing.Point(206, 13);
            this.txtcuentaventadesc.MenuManager = this.barManager1;
            this.txtcuentaventadesc.Name = "txtcuentaventadesc";
            this.txtcuentaventadesc.Size = new System.Drawing.Size(291, 20);
            this.txtcuentaventadesc.TabIndex = 29;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(6, 38);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(107, 16);
            this.labelControl13.TabIndex = 18;
            this.labelControl13.Text = "Cuenta de Compra";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(18, 14);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(95, 16);
            this.labelControl12.TabIndex = 18;
            this.labelControl12.Text = "Cuenta de Venta";
            // 
            // txtcuentacompradesc
            // 
            this.txtcuentacompradesc.Enabled = false;
            this.txtcuentacompradesc.EnterMoveNextControl = true;
            this.txtcuentacompradesc.Location = new System.Drawing.Point(206, 34);
            this.txtcuentacompradesc.MenuManager = this.barManager1;
            this.txtcuentacompradesc.Name = "txtcuentacompradesc";
            this.txtcuentacompradesc.Size = new System.Drawing.Size(291, 20);
            this.txtcuentacompradesc.TabIndex = 29;
            // 
            // txtcuentacompra
            // 
            this.txtcuentacompra.EnterMoveNextControl = true;
            this.txtcuentacompra.Location = new System.Drawing.Point(119, 34);
            this.txtcuentacompra.MenuManager = this.barManager1;
            this.txtcuentacompra.Name = "txtcuentacompra";
            this.txtcuentacompra.Size = new System.Drawing.Size(81, 20);
            this.txtcuentacompra.TabIndex = 28;
            this.txtcuentacompra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtidanalisiscompra_KeyDown);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtoperacionVentaDesc);
            this.groupBox4.Controls.Add(this.txtoperacionVentaCod);
            this.groupBox4.Controls.Add(this.labelControl14);
            this.groupBox4.Location = new System.Drawing.Point(3, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(461, 45);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ventas (Tener en cuenta el año actual)";
            // 
            // txtoperacionVentaDesc
            // 
            this.txtoperacionVentaDesc.Enabled = false;
            this.txtoperacionVentaDesc.EnterMoveNextControl = true;
            this.txtoperacionVentaDesc.Location = new System.Drawing.Point(163, 17);
            this.txtoperacionVentaDesc.Name = "txtoperacionVentaDesc";
            this.txtoperacionVentaDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoperacionVentaDesc.Properties.Appearance.Options.UseFont = true;
            this.txtoperacionVentaDesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtoperacionVentaDesc.Size = new System.Drawing.Size(292, 22);
            this.txtoperacionVentaDesc.TabIndex = 20;
            // 
            // txtoperacionVentaCod
            // 
            this.txtoperacionVentaCod.EnterMoveNextControl = true;
            this.txtoperacionVentaCod.Location = new System.Drawing.Point(127, 17);
            this.txtoperacionVentaCod.Name = "txtoperacionVentaCod";
            this.txtoperacionVentaCod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoperacionVentaCod.Properties.Appearance.Options.UseFont = true;
            this.txtoperacionVentaCod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtoperacionVentaCod.Size = new System.Drawing.Size(34, 22);
            this.txtoperacionVentaCod.TabIndex = 19;
            this.txtoperacionVentaCod.EditValueChanged += new System.EventHandler(this.txtoperacionVentaCod_EditValueChanged);
            this.txtoperacionVentaCod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtoperacionVentaCod_KeyDown);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(27, 20);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(94, 16);
            this.labelControl14.TabIndex = 18;
            this.labelControl14.Text = "Operacion I.G.V.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txttipobsadesc);
            this.groupBox1.Controls.Add(this.txttipobsacod);
            this.groupBox1.Controls.Add(this.labelControl15);
            this.groupBox1.Controls.Add(this.chkfacturarsinexistencia);
            this.groupBox1.Controls.Add(this.chkLotes);
            this.groupBox1.Controls.Add(this.txtgrupodesc);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Controls.Add(this.txtgrupocod);
            this.groupBox1.Controls.Add(this.txtlaboratorio);
            this.groupBox1.Controls.Add(this.txtlaboratorioDesc);
            this.groupBox1.Controls.Add(this.txtbarra);
            this.groupBox1.Controls.Add(this.txtfamiliadesc);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.txtdescripcion);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.txtfamiliacod);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.txtmarcadesc);
            this.groupBox1.Controls.Add(this.txtmarcacod);
            this.groupBox1.Controls.Add(this.txtserie);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(959, 430);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(154, 12);
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(130, 20);
            this.txttipobsadesc.TabIndex = 2;
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(98, 12);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Size = new System.Drawing.Size(53, 20);
            this.txttipobsacod.TabIndex = 1;
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(49, 15);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(43, 13);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Tipo B/S:";
            // 
            // chkfacturarsinexistencia
            // 
            this.chkfacturarsinexistencia.EnterMoveNextControl = true;
            this.chkfacturarsinexistencia.Location = new System.Drawing.Point(214, 159);
            this.chkfacturarsinexistencia.Name = "chkfacturarsinexistencia";
            this.chkfacturarsinexistencia.Properties.Caption = "Facturar sin existencias";
            this.chkfacturarsinexistencia.Size = new System.Drawing.Size(145, 19);
            this.chkfacturarsinexistencia.TabIndex = 22;
            this.chkfacturarsinexistencia.Visible = false;
            this.chkfacturarsinexistencia.CheckedChanged += new System.EventHandler(this.chkfacturarsinexistencia_CheckedChanged);
            // 
            // chkLotes
            // 
            this.chkLotes.EnterMoveNextControl = true;
            this.chkLotes.Location = new System.Drawing.Point(99, 159);
            this.chkLotes.MenuManager = this.barManager1;
            this.chkLotes.Name = "chkLotes";
            this.chkLotes.Properties.Caption = "Acepta Lotes?";
            this.chkLotes.Size = new System.Drawing.Size(100, 19);
            this.chkLotes.TabIndex = 21;
            this.chkLotes.CheckedChanged += new System.EventHandler(this.chkLotes_CheckedChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(469, 110);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(54, 16);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "Lab./Fab.";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(21, 112);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(72, 16);
            this.labelControl10.TabIndex = 14;
            this.labelControl10.Text = "Cod. Barras:";
            // 
            // txtlaboratorio
            // 
            this.txtlaboratorio.EnterMoveNextControl = true;
            this.txtlaboratorio.Location = new System.Drawing.Point(529, 107);
            this.txtlaboratorio.Name = "txtlaboratorio";
            this.txtlaboratorio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlaboratorio.Properties.Appearance.Options.UseFont = true;
            this.txtlaboratorio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlaboratorio.Size = new System.Drawing.Size(66, 22);
            this.txtlaboratorio.TabIndex = 17;
            this.txtlaboratorio.TextChanged += new System.EventHandler(this.txtlaboratorio_TextChanged);
            this.txtlaboratorio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlaboratorio_KeyDown);
            // 
            // txtlaboratorioDesc
            // 
            this.txtlaboratorioDesc.Enabled = false;
            this.txtlaboratorioDesc.EnterMoveNextControl = true;
            this.txtlaboratorioDesc.Location = new System.Drawing.Point(596, 107);
            this.txtlaboratorioDesc.Name = "txtlaboratorioDesc";
            this.txtlaboratorioDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlaboratorioDesc.Properties.Appearance.Options.UseFont = true;
            this.txtlaboratorioDesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlaboratorioDesc.Size = new System.Drawing.Size(353, 22);
            this.txtlaboratorioDesc.TabIndex = 18;
            // 
            // txtbarra
            // 
            this.txtbarra.EnterMoveNextControl = true;
            this.txtbarra.Location = new System.Drawing.Point(99, 108);
            this.txtbarra.Name = "txtbarra";
            this.txtbarra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbarra.Properties.Appearance.Options.UseFont = true;
            this.txtbarra.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtbarra.Size = new System.Drawing.Size(364, 22);
            this.txtbarra.TabIndex = 15;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(954, 432);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Caracteristicas";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rchcomentarios);
            this.groupBox2.Location = new System.Drawing.Point(3, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(734, 395);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // rchcomentarios
            // 
            this.rchcomentarios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rchcomentarios.Location = new System.Drawing.Point(6, 24);
            this.rchcomentarios.Name = "rchcomentarios";
            this.rchcomentarios.Size = new System.Drawing.Size(728, 365);
            this.rchcomentarios.TabIndex = 0;
            this.rchcomentarios.Text = "";
            this.rchcomentarios.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rchcomentarios_KeyPress);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnagregar);
            this.tabPage3.Controls.Add(this.btnquitar);
            this.tabPage3.Controls.Add(this.btneditar);
            this.tabPage3.Controls.Add(this.btnnuevo);
            this.tabPage3.Controls.Add(this.dgvdatos);
            this.tabPage3.Controls.Add(this.gbespecificaciones);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(954, 432);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Otras especificaciones";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnagregar
            // 
            this.btnagregar.Location = new System.Drawing.Point(246, 76);
            this.btnagregar.Name = "btnagregar";
            this.btnagregar.Size = new System.Drawing.Size(75, 23);
            this.btnagregar.TabIndex = 2;
            this.btnagregar.Text = "Agregar";
            this.btnagregar.Click += new System.EventHandler(this.btnagregar_Click_1);
            // 
            // btnquitar
            // 
            this.btnquitar.Location = new System.Drawing.Point(165, 76);
            this.btnquitar.Name = "btnquitar";
            this.btnquitar.Size = new System.Drawing.Size(75, 23);
            this.btnquitar.TabIndex = 4;
            this.btnquitar.Text = "Quitar";
            this.btnquitar.Click += new System.EventHandler(this.btnquitar_Click);
            // 
            // btneditar
            // 
            this.btneditar.Location = new System.Drawing.Point(84, 76);
            this.btneditar.Name = "btneditar";
            this.btneditar.Size = new System.Drawing.Size(75, 23);
            this.btneditar.TabIndex = 3;
            this.btneditar.Text = "Editar";
            this.btneditar.Click += new System.EventHandler(this.btneditar_Click);
            // 
            // btnnuevo
            // 
            this.btnnuevo.Location = new System.Drawing.Point(3, 76);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.Size = new System.Drawing.Size(75, 23);
            this.btnnuevo.TabIndex = 0;
            this.btnnuevo.Text = "Nuevo";
            this.btnnuevo.Click += new System.EventHandler(this.btnnuevo_Click);
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(6, 101);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(726, 298);
            this.dgvdatos.TabIndex = 45;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "N";
            this.gridColumn1.FieldName = "Id_Item";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 49;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tipo";
            this.gridColumn2.FieldName = "Esp_Descripcion";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 225;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Descripcion";
            this.gridColumn3.FieldName = "Esp_Desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 389;
            // 
            // gbespecificaciones
            // 
            this.gbespecificaciones.Controls.Add(this.txttipoespecifidesc);
            this.gbespecificaciones.Controls.Add(this.txtespecificacionDesc);
            this.gbespecificaciones.Controls.Add(this.txttipoespecificod);
            this.gbespecificaciones.Controls.Add(this.label2);
            this.gbespecificaciones.Controls.Add(this.label1);
            this.gbespecificaciones.Location = new System.Drawing.Point(3, 3);
            this.gbespecificaciones.Name = "gbespecificaciones";
            this.gbespecificaciones.Size = new System.Drawing.Size(729, 67);
            this.gbespecificaciones.TabIndex = 1;
            this.gbespecificaciones.TabStop = false;
            // 
            // txttipoespecifidesc
            // 
            this.txttipoespecifidesc.Enabled = false;
            this.txttipoespecifidesc.EnterMoveNextControl = true;
            this.txttipoespecifidesc.Location = new System.Drawing.Point(189, 15);
            this.txttipoespecifidesc.MenuManager = this.barManager1;
            this.txttipoespecifidesc.Name = "txttipoespecifidesc";
            this.txttipoespecifidesc.Size = new System.Drawing.Size(531, 20);
            this.txttipoespecifidesc.TabIndex = 2;
            // 
            // txtespecificacionDesc
            // 
            this.txtespecificacionDesc.EnterMoveNextControl = true;
            this.txtespecificacionDesc.Location = new System.Drawing.Point(119, 37);
            this.txtespecificacionDesc.Name = "txtespecificacionDesc";
            this.txtespecificacionDesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtespecificacionDesc.Size = new System.Drawing.Size(601, 20);
            this.txtespecificacionDesc.TabIndex = 4;
            // 
            // txttipoespecificod
            // 
            this.txttipoespecificod.EnterMoveNextControl = true;
            this.txttipoespecificod.Location = new System.Drawing.Point(119, 15);
            this.txttipoespecificod.MenuManager = this.barManager1;
            this.txttipoespecificod.Name = "txttipoespecificod";
            this.txttipoespecificod.Size = new System.Drawing.Size(64, 20);
            this.txttipoespecificod.TabIndex = 1;
            this.txttipoespecificod.TextChanged += new System.EventHandler(this.txttipoespecificod_TextChanged);
            this.txttipoespecificod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoespecificod_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Descripcion:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de especificacion:";
            // 
            // frm_catalogo_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(948, 484);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_catalogo_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catalogo";
            this.Load += new System.EventHandler(this.frm_catalogo_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfamiliadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmarcadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmarcacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfamiliacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupocod.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.Gbpresentacion.ResumeLayout(false);
            this.Gbpresentacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescDetFactorConver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodFactorConver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunidadbase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatosunm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentaventa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentaventadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentacompradesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentacompra.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtoperacionVentaDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtoperacionVentaCod.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkfacturarsinexistencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlaboratorio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlaboratorioDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbarra.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.gbespecificaciones.ResumeLayout(false);
            this.gbespecificaciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoespecifidesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtespecificacionDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoespecificod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.TextEdit txtfamiliadesc;
        public DevExpress.XtraEditors.TextEdit txtgrupodesc;
        public DevExpress.XtraEditors.TextEdit txtdescripcion;
        public DevExpress.XtraEditors.TextEdit txtmarcadesc;
        public DevExpress.XtraEditors.TextEdit txtserie;
        public DevExpress.XtraEditors.TextEdit txtmarcacod;
        public DevExpress.XtraEditors.TextEdit txtfamiliacod;
        public DevExpress.XtraEditors.TextEdit txtgrupocod;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.RichTextBox rchcomentarios;
        private System.Windows.Forms.GroupBox gbespecificaciones;
        private DevExpress.XtraEditors.TextEdit txttipoespecifidesc;
        private DevExpress.XtraEditors.TextEdit txtespecificacionDesc;
        private DevExpress.XtraEditors.TextEdit txttipoespecificod;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnagregar;
        private DevExpress.XtraEditors.SimpleButton btnquitar;
        private DevExpress.XtraEditors.SimpleButton btneditar;
        private DevExpress.XtraEditors.SimpleButton btnnuevo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        public DevExpress.XtraGrid.GridControl dgvdatos;
        public DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckBox chkunmdefecto;
        public DevExpress.XtraEditors.TextEdit txtunmdescDet;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        public DevExpress.XtraEditors.TextEdit txtunmcodDet;
        public DevExpress.XtraEditors.TextEdit txtbarra;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        public DevExpress.XtraEditors.TextEdit txtunidadbase;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SimpleButton btnagregarDet;
        private DevExpress.XtraEditors.SimpleButton btnquitarDet;
        private DevExpress.XtraEditors.SimpleButton btnEditarDet;
        private DevExpress.XtraEditors.SimpleButton btnnuevoDet;
        private DevExpress.XtraGrid.GridControl dgvdatosunm;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.GroupBox Gbpresentacion;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.TextEdit txtlaboratorioDesc;
        public DevExpress.XtraEditors.TextEdit txtlaboratorio;
        private DevExpress.XtraEditors.CheckEdit chkLotes;
        private DevExpress.XtraEditors.CheckEdit chkfacturarsinexistencia;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        public DevExpress.XtraEditors.TextEdit txtunmdescDetFactorConver;
        public DevExpress.XtraEditors.TextEdit txtunmcodFactorConver;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox4;
        public DevExpress.XtraEditors.TextEdit txtoperacionVentaDesc;
        public DevExpress.XtraEditors.TextEdit txtoperacionVentaCod;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtcuentaventadesc;
        private DevExpress.XtraEditors.TextEdit txtcuentaventa;
        private DevExpress.XtraEditors.TextEdit txtcuentacompradesc;
        private DevExpress.XtraEditors.TextEdit txtcuentacompra;
        public DevExpress.XtraEditors.TextEdit txttipobsadesc;
        public DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private System.Windows.Forms.GroupBox groupBox5;
        private DevExpress.XtraEditors.SimpleButton btnAgregarcuenta;
        private DevExpress.XtraEditors.SimpleButton btnquitarcuenta;
        private DevExpress.XtraGrid.GridControl dgvcuenta;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
    }
}
