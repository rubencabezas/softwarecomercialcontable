﻿using Comercial.Busquedas_Generales;
using Contable;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_catalogo_edicion : frm_fuente
    {
        public frm_catalogo_edicion()
        {
            InitializeComponent();
        }
        public string Estado_Ven_Boton;
        bool estado;
        bool estado_unm;
        public string Id_Empresa, Id_tipo, Id_Catalogo;
        private void textEdit6_EditValueChanged(object sender, EventArgs e)
        {

        }

        public string Tipo;
        public int Id_Item;
        public int Id_Item_unm;

        public string Desde_Catalogo ="";

         string Desde_Catalogo_var = "";
        void Limpiar()
        {
            txtgrupocod.ResetText();
            txtgrupodesc.ResetText();
            txtfamiliacod.ResetText();
            txtfamiliadesc.ResetText();
            txtdescripcion.ResetText();
            txtserie.ResetText();
         
            txtmarcacod.ResetText();
            txtmarcadesc.ResetText();

            rchcomentarios.ResetText();
            txttipoespecificod.ResetText();
            txttipoespecifidesc.ResetText();
            txtespecificacionDesc.ResetText();
            chkLotes.Checked = false;
        }
        void Limpiar_UNM()
        {
            txtunmcodDet.ResetText();
            txtunmdescDet.ResetText();
            txtunidadbase.ResetText();
            txtunmcodFactorConver.ResetText();
            txtunmdescDetFactorConver.ResetText();
            chkunmdefecto.Checked = false;
        }
        private void txtgrupocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtgrupocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtgrupocod.Text.Substring(txtgrupocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_grupo_busqueda f = new frm_grupo_busqueda())
                        {
                            f.Tipo = "0031";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Grupo Entidad = new Entidad_Grupo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtgrupocod.Text = Entidad.Id_Grupo;
                                txtgrupodesc.Text = Entidad.Gru_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtgrupocod.Text) & string.IsNullOrEmpty(txtgrupodesc.Text))
                    {
                        BuscarGrupo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarGrupo()
        {
            try
            {
                txtgrupocod.Text = Accion.Formato(txtgrupocod.Text, 2);
                Logica_Grupo log = new Logica_Grupo();

                List<Entidad_Grupo> Generales = new List<Entidad_Grupo>();
                Generales = log.Listar(new Entidad_Grupo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = "0031",
                    Id_Grupo = txtgrupocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Grupo T in Generales)
                    {
                        if ((T.Id_Grupo).ToString().Trim().ToUpper() == txtgrupocod.Text.Trim().ToUpper())
                        {
                            txtgrupocod.Text = (T.Id_Grupo).ToString().Trim();
                            txtgrupodesc.Text = T.Gru_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtgrupocod_TextChanged(object sender, EventArgs e)
        {
            if (txtgrupocod.Focus() == false)
            {
                txtgrupodesc.ResetText();
            }
        }

        private void txtfamiliacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtfamiliacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtfamiliacod.Text.Substring(txtfamiliacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_familia_busqueda f = new frm_familia_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString(); //"0031";
                            //f.Grupo_Id = txtgrupocod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Familia Entidad = new Entidad_Familia();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtgrupocod.Text = Entidad.Id_Grupo;
                                txtgrupodesc.Text = Entidad.Gru_Descripcion;

                                txtfamiliacod.Text = Entidad.Id_Familia;
                                txtfamiliadesc.Text = Entidad.Fam_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtfamiliacod.Text) & string.IsNullOrEmpty(txtfamiliadesc.Text))
                    {
                        BuscarFamilia();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarFamilia()
        {
            try
            {
                txtfamiliacod.Text = Accion.Formato(txtfamiliacod.Text, 2);
                Logica_Familia log = new Logica_Familia();

                List<Entidad_Familia> Generales = new List<Entidad_Familia>();
                Generales = log.Listar(new Entidad_Familia
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Grupo = txtgrupocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Familia T in Generales)
                    {
                        if ((T.Id_Familia).ToString().Trim().ToUpper() == txtfamiliacod.Text.Trim().ToUpper())
                        {
                            txtgrupocod.Text = T.Id_Grupo.ToString().Trim();
                            txtgrupodesc.Text = T.Gru_Descripcion.ToString().Trim();

                            txtfamiliacod.Text = (T.Id_Familia).ToString().Trim();
                            txtfamiliadesc.Text = T.Fam_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmarcacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmarcacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmarcacod.Text.Substring(txtmarcacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_marca_busqueda f = new frm_marca_busqueda())
                        {
                            //f.marca = txtmarcacod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Marca Entidad = new Entidad_Marca();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmarcacod.Text = Entidad.Id_Marca;
                                txtmarcadesc.Text = Entidad.Mar_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmarcacod.Text) & string.IsNullOrEmpty(txtmarcadesc.Text))
                    {
                        BuscarMarca();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMarca()
        {
            try
            {
                txtmarcacod.Text = Accion.Formato(txtmarcacod.Text,3);
                Logica_Marca log = new Logica_Marca();

                List<Entidad_Marca> Generales = new List<Entidad_Marca>();
                Generales = log.Listar(new Entidad_Marca
                {
                   Id_Marca=txtmarcacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Marca T in Generales)
                    {
                        if ((T.Id_Marca).ToString().Trim().ToUpper() == txtmarcacod.Text.Trim().ToUpper())
                        {
                            txtmarcacod.Text = (T.Id_Marca).ToString().Trim();
                            txtmarcadesc.Text = T.Mar_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtunmcodDet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtunmcodDet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunmcodDet.Text.Substring(txtunmcodDet.Text.Length - 1, 1) == "*")
                    {
                        using (frm_unidad_medida_busqueda f = new frm_unidad_medida_busqueda())
                        {
                            //f.unmcod = txtunmcod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunmcodDet.Text = Entidad.Id_Unidad_Medida;
                                txtunmdescDet.Text = Entidad.Und_Descripcion;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtunmcodDet.Text) & string.IsNullOrEmpty(txtunmdescDet.Text))
                    {
                        BuscarUnm();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarUnm()
        {
            try
            {
                txtunmcodDet.Text = Accion.Formato(txtunmcodDet.Text, 3);
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar(new Entidad_Unidad_Medida
                {
                    Id_Unidad_Medida = txtunmcodDet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Id_Unidad_Medida).ToString().Trim().ToUpper() == txtunmcodDet.Text.Trim().ToUpper())
                        {
                            txtunmcodDet.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdescDet.Text = T.Und_Descripcion;


                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtfamiliacod_TextChanged(object sender, EventArgs e)
        {
            if (txtfamiliacod.Focus() == false)
            {
                txtfamiliadesc.ResetText();
            }
        }

        private void txtmarcacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmarcacod.Focus() == false)
            {
                txtmarcadesc.ResetText();
            }
        }

        private void txtunmcod_TextChanged(object sender, EventArgs e)
        {
            if (txtunmcodDet.Focus() == false)
            {
                txtunmdescDet.ResetText();
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VerificarCabecera())
            {
                //DialogResult = DialogResult.OK;
                Entidad_Catalogo Ent = new Entidad_Catalogo();
                Logica_Catalogo Log = new Logica_Catalogo();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Grupo = txtgrupocod.Text;
                Ent.Id_Familia = txtfamiliacod.Text;
                Ent.Id_Tipo = txttipobsacod.Tag.ToString();
                Ent.Id_Catalogo = Id_Catalogo;
                Ent.Cat_Descripcion = txtdescripcion.Text;

                Ent.Cat_Serie = txtserie.Text.Trim();
                Ent.Cat_Barra = txtbarra.Text.Trim();
                Ent.Id_Marca = txtmarcacod.Text;
                Ent.Id_Fabricante = txtlaboratorio.Text.Trim();

                Ent.Cat_Comentarios = rchcomentarios.Text;
                Ent.Acepta_Lote = chkLotes.Checked;
                Ent.Facturar_Sin_Existencia = chkfacturarsinexistencia.Checked;

                Ent.Detalle_especificaciones = ListEspeci;
                Ent.Detalle_UNM = ListUnm_Det;

                Ent.Id_Afecto_Venta = txtoperacionVentaCod.Text;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;
                Ent.Id_Analisis_Compra = txtcuentacompra.Text;
                Ent.Id_Analisis = txtcuentaventa.Text;

                Ent.Detalle_Cuenta = ListaCuenta;

                try
                {
                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {
                            //if (Desde_Catalogo_var == "1"){

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();
                            Limpiar_UNM();
                            ListUnm_Det.Clear();
                            dgvdatosunm.DataSource = null;
                            txtgrupocod.Select();
                            this.Close();
                            //}
                            //else
                            //{
                            //    Accion.ExitoGuardar();
                            //    this.Close();
                            //}

                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
            
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        public bool VerificarCabecera()
        {

            if (string.IsNullOrEmpty(txtgrupodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un grupo");
                txtgrupocod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfamiliadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una famila");
                txtfamiliacod.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtdescripcion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion del producto");
                txtdescripcion.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmarcadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una marca");
                txtmarcacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtoperacionVentaDesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de operacion en la pestaña Contable");
                tabControl2.SelectedIndex = 1;
                txtoperacionVentaCod.Focus();
                return false;
            }


            //if (string.IsNullOrEmpty(txtanalisisdesc.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar un analisis contable en la pestaña Contable");
            //    tabControl2.SelectedIndex = 1;
            //    txtidanalisis.Focus();
            //    return false;
            //}

            //if (string.IsNullOrEmpty(txtoperacionCompraDesc.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar un tipo de operacion en la pestaña Contable");
            //    tabControl2.SelectedIndex = 1;
            //    txtoperacionCompraCod.Focus();
            //    return false;
            //}


            //if (string.IsNullOrEmpty(txtanalisisdesccompra.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar un analisis contable en la pestaña Contable");
            //    tabControl2.SelectedIndex = 1;
            //    txtidanalisiscompra.Focus();
            //    return false;
            //}


  
            if (ListUnm_Det.Count <= 0)
            {
                Accion.Advertencia("Debe ingresar una unidad de medida en el detalle");
                txtunmcodDet.Focus();
                return false;
            }

            int cantidad = 0;
            foreach (Entidad_Catalogo Unm in ListUnm_Det)
            {

                if (Unm.Unm_Defecto == true)
                {
                    cantidad += 1;
    
                }
            }

            if (cantidad>=2)
            {
                Accion.Advertencia("Debe asignar una unidad de medida de compra");
                return false;
            }
            return true;
        }

        private void txttipoespecificod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txttipoespecificod.Text.Trim()))
            {
                try
                {
                    if (((e.KeyCode == Keys.Enter) && (txttipoespecificod.Text.Substring((txttipoespecificod.Text.Trim().Length - 1), 1) == "*")))
                    {
                        using (frm_tipo_especificacion f =new frm_tipo_especificacion())
                        {
                       if (f.ShowDialog() == DialogResult.OK)
                            {
                                Entidad_Tipo_Especificacion ent = new Entidad_Tipo_Especificacion();
                                ent = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];
                                txttipoespecificod.Text = ent.Esp_Codigo.Trim();
                                txttipoespecifidesc.Text = ent.Esp_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoespecificod.Text) & string.IsNullOrEmpty(txttipoespecifidesc.Text))
                    {
                        //BuscarFamilia();
                    }

                    //(Keys.Enter & ((txttipoespecificod.Text.Trim() != "")   & txttipoespecifidesc.Text.Trim())) = "";
                    //e.KeyCode = "";
                    //txttipoespecificod.Text = CodeFormat(txtTipoEspecifCod.Text, 2);
                    //buscarTipoEspecif();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }

        }

        private void frm_catalogo_edicion_Load(object sender, EventArgs e)
        {
            Desde_Catalogo_var = Desde_Catalogo;

            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                gbespecificaciones.Enabled = false;
                Gbpresentacion.Enabled = false;
                txtgrupocod.Select();
                Limpiar();

                txttipobsacod.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                Gbpresentacion.Enabled = false;
                ListarModificar();
        
            }

        }


        public List<Entidad_Catalogo> Lista_Modificar = new List<Entidad_Catalogo>();

        List<Entidad_Catalogo> Cuenta_Con_Ingresos_Egresos_Inventario= new List<Entidad_Catalogo>();
        public void ListarModificar()
        {
            Entidad_Catalogo Ent = new Entidad_Catalogo();
            Logica_Catalogo log = new Logica_Catalogo();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Tipo = Id_tipo;
            Ent.Id_Catalogo = Id_Catalogo;

            try
            {
                Lista_Modificar = log.Listar(Ent);


                //Cuenta_Con_Ingresos_Egresos_Inventario = log.Cuenta_Con_Ingresos_Egresos_Inventario(Ent);



                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Catalogo  Enti = new Entidad_Catalogo();
                    Enti = Lista_Modificar[0];
                    txttipobsacod.Tag = Enti.Id_Tipo;
                    txttipobsacod.Text = Enti.Id_TipoCod;
                    txttipobsadesc.Text = Enti.Id_TipoDescripcion;
                    txttipobsacod.Enabled = false;

                    Id_Empresa = Enti.Id_Empresa;
                    txtgrupocod.Text = Enti.Id_Grupo;
                    txtgrupodesc.Text = Enti.Gru_Descripcion;
                    txtfamiliacod.Text = Enti.Id_Familia;
                    txtfamiliadesc.Text = Enti.Fam_Descripcion;
                    Id_tipo = Enti.Id_Tipo;
                    txtdescripcion.Tag = Enti.Id_Catalogo.Trim();
                    txtdescripcion.Text = Enti.Cat_Descripcion.Trim();

                    txtbarra.Text = Enti.Cat_Barra.Trim();

                    txtserie.Text = Enti.Cat_Serie.Trim();
      
                    txtmarcacod.Text = Enti.Id_Marca.Trim();
                    txtmarcadesc.Text = Enti.Mar_Descripcion.Trim();

                    txtlaboratorio.Text = Enti.Id_Fabricante;
                    txtlaboratorioDesc.Text = Enti.Fab_Descripcion;
                    rchcomentarios.Text = Enti.Cat_Comentarios;

                    //Entidad_Catalogo Det = new Entidad_Catalogo;
                    chkLotes.Checked = Enti.Acepta_Lote;
                    chkfacturarsinexistencia.Checked = Enti.Facturar_Sin_Existencia;

                    Logica_Catalogo logi = new Logica_Catalogo();

                    ListEspeci = logi.Listar_especificaciones(Enti);
                    ListUnm_Det = logi.Listar_Cat_Unm(Enti);

                    ListaOperacion_Venta = logi.Listar_Operacion_venta(Enti);

                    if (ListEspeci.Count > 0)
                    {
                    dgvdatos.DataSource = ListEspeci;
                    }

                    if (ListUnm_Det.Count > 0)
                    {
                        dgvdatosunm.DataSource = ListUnm_Det;
                    }

                    if (ListaOperacion_Venta.Count > 0)
                    {
                    txtoperacionVentaCod.Text = ListaOperacion_Venta[0].Id_Afecto_Venta.Trim();
                    txtoperacionVentaDesc.Text = ListaOperacion_Venta[0].Afecto_Venta_Descripcion.Trim();
                    }


                    Enti.Id_Anio = Actual_Conexion.AnioSelect;
                    ListaAnalisisContable = logi.Listar_Cuenta_Catalogo(Enti);

                    if (ListaAnalisisContable.Count > 0)
                    {
                        dgvcuenta.DataSource = ListaAnalisisContable;
                    }

 



                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            Id_Item = ListEspeci.Count+1;
            txttipoespecificod.ResetText();
            txttipoespecifidesc.ResetText();
            txtespecificacionDesc.ResetText();

            gbespecificaciones.Enabled = true;
            txttipoespecificod.Select();
            estado = true;
        }

      public  List<Entidad_Catalogo> ListEspeci = new List<Entidad_Catalogo>();

      public List<Entidad_Catalogo> ListaOperacion_Venta = new List<Entidad_Catalogo>();

      public List<Entidad_Catalogo> ListaAnalisisContable= new List<Entidad_Catalogo>();
  
        private void btnagregar_Click_1(object sender, EventArgs e)
        {
            try
            {
                    if (VerificarCabecera_Especificaciones())
                    {
                            Entidad_Catalogo especificaciones = new Entidad_Catalogo();

                            especificaciones.Id_Item = Id_Item;
                            especificaciones.Esp_Codigo = txttipoespecificod.Text;
                            especificaciones.Esp_Descripcion = txttipoespecifidesc.Text;
                            especificaciones.Esp_Desc = txtespecificacionDesc.Text;

                            if (estado == true)
                            {
                             ListEspeci.Add(especificaciones);
                            }
                            else
                            {
                                ListEspeci[especificaciones.Id_Item - 1] = especificaciones;
                            }
                            dgvdatos.DataSource = null;
                            dgvdatos.DataSource = ListEspeci;
                    }
                btnnuevo.Select();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public bool VerificarCabecera_Especificaciones()
        {
            if (string.IsNullOrEmpty(txttipoespecifidesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de especificacion");
                txttipoespecificod.Focus();
                return false;
            }

            return true;
        }

        public bool Verificar_Unm()
        {
            if (string.IsNullOrEmpty(txtunmdescDet.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una unidad de medida");
                txtunmcodDet.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtunidadbase.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una unidad base");
                txtunmcodDet.Focus();
                return false;
            }

            //if (1 ==ListUnm_Det.Count )
            //{
            //    Accion.Advertencia("Solo se permite una unidad de medida");
            //    //txtunmcodDet.Focus();
            //    return false;
            //}

            //if (chkunmdefecto.Checked == false)
            //{
            //    Accion.Advertencia("Debe poner el check de compra por defecto");
            //    chkunmdefecto.Focus();
            //    return false;
            //}

            //foreach (Entidad_Catalogo Unm in ListUnm_Det)
            //{

            //    if (Unm.Unm_Defecto == true)
            //    {
            //        Accion.Advertencia("Solo Debe tener una Unidad de Medida por Defecto");
            //        //break;
            //        chkunmdefecto.Checked = false;
            //        return false;
            //    }
            //}

            return true;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (ListEspeci.Count > 0)
            {
                Entidad_Catalogo esp = new Entidad_Catalogo();
                esp = ListEspeci[gridView1.GetFocusedDataSourceRowIndex()];
                Id_Item = esp.Id_Item;
                txttipoespecificod.Text = esp.Esp_Codigo;
                txttipoespecifidesc.Text = esp.Esp_Descripcion;
                txtespecificacionDesc.Text = esp.Esp_Desc;

                gbespecificaciones.Enabled = false;

            }
        }

        private void btneditar_Click(object sender, EventArgs e)
        {
            btnnuevo.Enabled = true;
            btneditar.Enabled = false;
            btnquitar.Enabled = true;
            btnagregar.Enabled = true;
            gbespecificaciones.Enabled = true;
            estado = false;
        }

        private void rchcomentarios_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        public List<Entidad_Catalogo> ListUnm_Det = new List<Entidad_Catalogo>();
        private void btnnuevoDet_Click(object sender, EventArgs e)
        {
            Id_Item_unm = ListUnm_Det.Count + 1;

          
            txtunidadbase.ResetText();
            txtunmcodDet.ResetText();
            txtunmdescDet.ResetText();
            txtunmcodFactorConver.ResetText();
            txtunmdescDetFactorConver.ResetText();
            chkunmdefecto.Checked = false;
            Gbpresentacion.Enabled = true;
            txtunmcodDet.Focus();
            estado_unm = true;
        }

        private void btnEditarDet_Click(object sender, EventArgs e)
        {
            btnnuevoDet.Enabled = true;
            btnEditarDet.Enabled = false;
            btnquitarDet.Enabled = true;
            btnagregarDet.Enabled = true;
            Gbpresentacion.Enabled = true;
            estado_unm = false;
        }

        private void btnquitarDet_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListUnm_Det.Count > 0)
                {
                    ListUnm_Det.RemoveAt(gridView2.GetFocusedDataSourceRowIndex());

                    dgvdatosunm.DataSource = null;
                    if (ListUnm_Det.Count > 0)
                    {
                        dgvdatosunm.DataSource = ListUnm_Det;
                        RefreshNumeralUnm();
                    }
                    Limpiar_UNM();
                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnquitar_Click(object sender, EventArgs e)
        {
         try
            {
                if (ListEspeci.Count > 0)
                {
                    ListEspeci.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());

                    dgvdatos.DataSource = null;
                    if (ListEspeci.Count > 0)
                    {
                        dgvdatos.DataSource = ListEspeci;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnagregarDet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Verificar_Unm())
                {
                    Entidad_Catalogo unm_det = new Entidad_Catalogo();

                    unm_det.Id_Item_Det = Id_Item_unm;
                    unm_det.Unm_Cod_Det = txtunmcodDet.Text;
                    unm_det.Unm_Desc_Det = txtunmdescDet.Text;
                    unm_det.Unm_Base =Convert.ToDecimal(txtunidadbase.Text);
                    unm_det.Unm_Defecto = chkunmdefecto.Checked;
                    unm_det.Unm_Coef_Base_Unidad_Medida = txtunmcodFactorConver.Text.Trim();
                    unm_det.Unm_Coef_Base_Unidad_Medida_Descripcion = txtunmdescDetFactorConver.Text.Trim();
                    

                    if (estado_unm == true)
                    {
                        ListUnm_Det.Add(unm_det);
                    }
                    else
                    {
                        ListUnm_Det[unm_det.Id_Item_Det - 1] = unm_det;
                    }

                    dgvdatosunm.DataSource = null;
                    dgvdatosunm.DataSource = ListUnm_Det;
                    Limpiar_UNM();
                    Gbpresentacion.Enabled = false;
                 }
                btnnuevoDet.Select();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (ListUnm_Det.Count > 0)
            {
                Entidad_Catalogo esp = new Entidad_Catalogo();
                esp = ListUnm_Det[gridView2.GetFocusedDataSourceRowIndex()];
                Id_Item_unm = esp.Id_Item_Det;

                txtunmcodDet.Text = esp.Unm_Cod_Det;
                txtunmdescDet.Text = esp.Unm_Desc_Det;
                txtunidadbase.Text = Convert.ToString(esp.Unm_Base);

                txtunmcodFactorConver.Text = esp.Unm_Coef_Base_Unidad_Medida;
                txtunmdescDetFactorConver.Text = esp.Unm_Coef_Base_Unidad_Medida_Descripcion;

                chkunmdefecto.Checked = esp.Unm_Defecto;
                Gbpresentacion.Enabled = false;

                btnEditarDet.Enabled = true;
            }
        }

        private void txtlaboratorio_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlaboratorio.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlaboratorio.Text.Substring(txtlaboratorio.Text.Length - 1, 1) == "*")
                    {
                        using (frm_laboratorio_busqueda f = new frm_laboratorio_busqueda())
                        {
                            //f.marca = txtmarcacod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Laboratorio Entidad = new Entidad_Laboratorio();
                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];
                                txtlaboratorio.Text = Entidad.Id_Fabricante;
                                txtlaboratorioDesc.Text = Entidad.Fab_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlaboratorio.Text) & string.IsNullOrEmpty(txtlaboratorioDesc.Text))
                    {
                        BuscarLaboratorio();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarLaboratorio()
        {
            try
            {
                txtlaboratorio.Text = Accion.Formato(txtlaboratorio.Text, 6);
                Logica_Laboratorio log = new Logica_Laboratorio();

                List<Entidad_Laboratorio> Generales = new List<Entidad_Laboratorio>();
                Generales = log.Listar(new Entidad_Laboratorio
                {
                    Id_Fabricante = txtlaboratorio.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Laboratorio T in Generales)
                    {
                        if ((T.Id_Fabricante).ToString().Trim().ToUpper() == txtlaboratorio.Text.Trim().ToUpper())
                        {
                            txtlaboratorio.Text = (T.Id_Fabricante).ToString().Trim();
                            txtlaboratorioDesc.Text = T.Fab_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {
            
        }


        private void txtdescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnnuevoDet.Select();
            }
        }

        private void chkunmdefecto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnagregarDet.Select();
            }
        }

        private void txtlaboratorio_TextChanged(object sender, EventArgs e)
        {
            if (txtlaboratorio.Focus() == false)
            {
                txtlaboratorioDesc.ResetText();
            }
        }

        private void txttipoespecificod_TextChanged(object sender, EventArgs e)
        {
            if (txttipoespecificod.Focus() == false)
            {
                txttipoespecifidesc.ResetText();
            }
        }

        private void txtunmcodFactorConver_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtunmcodFactorConver.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunmcodFactorConver.Text.Substring(txtunmcodFactorConver.Text.Length - 1, 1) == "*")
                    {
                        using (frm_unidad_medida_busqueda f = new frm_unidad_medida_busqueda())
                        {
                            //f.unmcod = txtunmcod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunmcodFactorConver.Text = Entidad.Id_Unidad_Medida;
                                txtunmdescDetFactorConver.Text = Entidad.Und_Descripcion;
                                
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtunmcodFactorConver.Text) & string.IsNullOrEmpty(txtunmdescDetFactorConver.Text))
                    {
                        BuscarUnmFactorConversion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarUnmFactorConversion()
        {
            try
            {
                txtunmcodFactorConver.Text = Accion.Formato(txtunmcodFactorConver.Text, 3);
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar(new Entidad_Unidad_Medida
                {
                    Id_Unidad_Medida = txtunmcodFactorConver.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Id_Unidad_Medida).ToString().Trim().ToUpper() == txtunmcodFactorConver.Text.Trim().ToUpper())
                        {
                            txtunmcodFactorConver.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdescDetFactorConver.Text = T.Und_Descripcion;


                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtunmcodFactorConver_TextChanged(object sender, EventArgs e)
        {
            if (txtunmcodFactorConver.Focus() == false)
            {
                txtunmdescDetFactorConver.ResetText();
            }
        }

        private void chkfacturarsinexistencia_CheckedChanged(object sender, EventArgs e)
        {
            if (chkfacturarsinexistencia.Checked )
            {
                chkLotes.Checked = false;
            }
        }

        private void chkLotes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLotes.Checked)
            {
                chkfacturarsinexistencia.Checked = false;
            }
        }

        private void txtoperacionVentaCod_EditValueChanged(object sender, EventArgs e)
        {
            if (txtoperacionVentaCod.Focused == false)
            {
                txtoperacionVentaDesc.ResetText();
            }
        }

        private void txtoperacionVentaCod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtoperacionVentaCod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtoperacionVentaCod.Text.Substring(txtoperacionVentaCod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_busqueda_operacion_venta f = new frm_busqueda_operacion_venta())
                        {
                            //f.unmcod = txtunmcod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Operaciones Entidad = new Entidad_Operaciones();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtoperacionVentaCod.Text = Entidad.AfecIGV_Codigo;
                                txtoperacionVentaDesc.Text = Entidad.AfecIGV_Descripcion;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtoperacionVentaCod.Text) & string.IsNullOrEmpty(txtoperacionVentaDesc.Text))
                    {
                        BuscarOperacionVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarOperacionVenta()
        {
            try
            {
                txtoperacionVentaCod.Text = Accion.Formato(txtoperacionVentaCod.Text, 2);
                Logica_Operaciones log = new Logica_Operaciones();

                List<Entidad_Operaciones> Generales = new List<Entidad_Operaciones>();
                Generales = log.Listar_Operaciones_Venta(new Entidad_Operaciones
                {
                    AfecIGV_Codigo = txtoperacionVentaCod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Operaciones T in Generales)
                    {
                        if ((T.AfecIGV_Codigo).ToString().Trim().ToUpper() == txtoperacionVentaCod.Text.Trim().ToUpper())
                        {
                            txtoperacionVentaCod.Text = (T.AfecIGV_Codigo).ToString().Trim();
                            txtoperacionVentaDesc.Text = T.AfecIGV_Descripcion;


                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtidanalisis_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuentaventa.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuentaventa.Text.Substring(txtcuentaventa.Text.Length - 1, 1) == "*")
                    {
                        using (frm_plan_empresarial_busqueda f = new frm_plan_empresarial_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuentaventa.Text = Entidad.Id_Cuenta;
                                txtcuentaventadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuentaventa.Text) & string.IsNullOrEmpty(txtcuentaventadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {

                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Empresa=Actual_Conexion.CodigoEmpresa,
                    Id_Anio=Actual_Conexion.AnioSelect,
                    Id_Cuenta = txtcuentaventa.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuentaventa.Text.Trim().ToUpper())
                        {
                            txtcuentaventa.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentaventadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuentaventa.EnterMoveNextControl = false;
                    txtcuentaventa.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }





        private void txtidanalisis_TextChanged(object sender, EventArgs e)
        {
            if (txtcuentaventa.Focus() == false)
            {
                txtcuentaventadesc.ResetText();
            }
        }

        private void txtidanalisiscompra_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuentacompra.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuentacompra.Text.Substring(txtcuentacompra.Text.Length - 1, 1) == "*")
                    {
                        using (frm_plan_empresarial_busqueda f = new frm_plan_empresarial_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuentacompra.Text = Entidad.Id_Cuenta;
                                txtcuentacompradesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuentacompra.Text) & string.IsNullOrEmpty(txtcuentacompradesc.Text))
                    {
                        CuentaCompra();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaCompra()
        {
            try
            {

                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Cuenta = txtcuentacompra.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuentacompra.Text.Trim().ToUpper())
                        {
                            txtcuentacompra.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentacompradesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuentacompra.EnterMoveNextControl = false;
                    txtcuentacompra.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }





        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipobsacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;
                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.ResetText();
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        List<Entidad_Catalogo> ListaCuenta = new List<Entidad_Catalogo>();
        private void btnAgregarcuenta_Click(object sender, EventArgs e)
        {
            try
            {
                if (Verificar_Cuenta())
                {
                    Entidad_Catalogo cta= new Entidad_Catalogo();

                    cta.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    cta.Id_Anio = Actual_Conexion.AnioSelect;
                   
                    cta.Id_Cuenta_Venta = txtcuentaventa.Text;
                    cta.Id_Cuenta_Venta_desc = txtcuentaventadesc.Text ;
                    cta.Id_Cuenta_Compra = txtcuentacompra.Text;
                    cta.Id_Cuenta_Compra_desc = txtcuentacompra.Text;
            

                   // if (estado_unm == true)
                   // {
                     //   ListaCuenta.Add(cta);
                   // }
                    //else
                   // {
                     //   ListaCuenta[cta.Id_Item_Det - 1] = cta;
                    //}

                    if (ExistenCuenta(cta))
                    {
                        Accion.Advertencia("Existe una cuenta asiganada,para este año.");
                    }
                    else
                    {
                        ListaCuenta.Add(cta);
                    }


                    dgvcuenta.DataSource = null;
                    dgvcuenta.DataSource = ListaCuenta;
                    LimpiarCuenta();
                   // Gbpresentacion.Enabled = false;
                }
               // btnnuevoDet.Select();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool Verificar_Cuenta()
        {
            if (string.IsNullOrEmpty(txtcuentaventadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una cuenta para ventas para este periodo");
                txtcuentaventa.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcuentacompradesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una cuenta para compras para este periodo");
                txtcuentacompra.Focus();
                return false;
            }
 

            return true;
        }

        bool ExistenCuenta(Entidad_Catalogo CuentaProducto )
        {
               // i_con = 0;
                foreach (Entidad_Catalogo T in ListaCuenta)
                {
                   if (T.Id_Catalogo == CuentaProducto.Id_Catalogo &  T.Id_Empresa == CuentaProducto.Id_Empresa & T.Id_Anio == CuentaProducto.Id_Anio &   T.Id_Cuenta_Venta == CuentaProducto.Id_Cuenta_Venta)
                   {
                         return true;
                   }

                if (T.Id_Catalogo == CuentaProducto.Id_Catalogo & T.Id_Empresa == CuentaProducto.Id_Empresa & T.Id_Anio == CuentaProducto.Id_Anio &  T.Id_Cuenta_Compra == CuentaProducto.Id_Cuenta_Compra)
                {
                    return true;
                }

              //  i_con += 1;
               }
                return false;
        }
        void LimpiarCuenta()
        {
            txtcuentaventa.ResetText();
            txtcuentaventadesc.ResetText();
            txtcuentacompra.ResetText();
            txtcuentacompradesc.ResetText();
        }


        void RefreshNumeral()
        {

            int NumOrden = 1;
            foreach (Entidad_Catalogo Det in ListEspeci)
            {
                Det.Id_Item = NumOrden;
                NumOrden += 1;
            }
        }

        void RefreshNumeralUnm()
        {

            int NumOrden = 1;
            foreach (Entidad_Catalogo Det in ListUnm_Det)
            {
                Det.Id_Item_Det = NumOrden;
                NumOrden += 1;
            }
        }


    }
}
