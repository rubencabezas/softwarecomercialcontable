﻿namespace Comercial
{
    partial class frm_familia_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnnuevo = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtgrupodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtgrupocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupocod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnnuevo,
            this.btnmodificar});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnnuevo)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnnuevo
            // 
            this.btnnuevo.Caption = "Guardar [F4]";
            this.btnnuevo.Id = 0;
            this.btnnuevo.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnnuevo.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevo.ItemAppearance.Normal.Options.UseFont = true;
            this.btnnuevo.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnnuevo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnnuevo_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(645, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 140);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(645, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 106);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(645, 34);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 106);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar";
            this.btnmodificar.Id = 1;
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txttipobsadesc);
            this.groupControl1.Controls.Add(this.txttipobsacod);
            this.groupControl1.Controls.Add(this.labelControl15);
            this.groupControl1.Controls.Add(this.txtgrupodesc);
            this.groupControl1.Controls.Add(this.txtdescripcion);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtgrupocod);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(645, 99);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos";
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(134, 29);
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(130, 20);
            this.txttipobsadesc.TabIndex = 2;
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(78, 29);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Size = new System.Drawing.Size(53, 20);
            this.txttipobsacod.TabIndex = 1;
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(29, 32);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(43, 13);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Tipo B/S:";
            // 
            // txtgrupodesc
            // 
            this.txtgrupodesc.Enabled = false;
            this.txtgrupodesc.EnterMoveNextControl = true;
            this.txtgrupodesc.Location = new System.Drawing.Point(134, 52);
            this.txtgrupodesc.Name = "txtgrupodesc";
            this.txtgrupodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtgrupodesc.Size = new System.Drawing.Size(506, 20);
            this.txtgrupodesc.TabIndex = 5;
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.EnterMoveNextControl = true;
            this.txtdescripcion.Location = new System.Drawing.Point(77, 74);
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Size = new System.Drawing.Size(563, 20);
            this.txtdescripcion.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 77);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(58, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Descripcion:";
            // 
            // txtgrupocod
            // 
            this.txtgrupocod.EnterMoveNextControl = true;
            this.txtgrupocod.Location = new System.Drawing.Point(77, 52);
            this.txtgrupocod.MenuManager = this.barManager1;
            this.txtgrupocod.Name = "txtgrupocod";
            this.txtgrupocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtgrupocod.Size = new System.Drawing.Size(54, 20);
            this.txtgrupocod.TabIndex = 4;
            this.txtgrupocod.EditValueChanged += new System.EventHandler(this.txtgrupocod_EditValueChanged);
            this.txtgrupocod.TextChanged += new System.EventHandler(this.txtgrupocod_TextChanged);
            this.txtgrupocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgrupocod_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(37, 55);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(33, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Grupo:";
            // 
            // frm_familia_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(645, 140);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_familia_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Familia";
            this.Load += new System.EventHandler(this.frm_familia_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgrupocod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnnuevo;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.TextEdit txtgrupodesc;
        public DevExpress.XtraEditors.TextEdit txtdescripcion;
        public DevExpress.XtraEditors.TextEdit txtgrupocod;
        public DevExpress.XtraEditors.TextEdit txttipobsadesc;
        public DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.LabelControl labelControl15;
    }
}
