﻿using Contable;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_familia_edicion :  frm_fuente
    {
        public frm_familia_edicion()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public string Tipo;

        private void txtgrupocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtgrupocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtgrupocod.Text.Substring(txtgrupocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_grupo_busqueda f = new frm_grupo_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Grupo Entidad = new Entidad_Grupo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtgrupocod.Text = Entidad.Id_Grupo;
                                txtgrupodesc.Text = Entidad.Gru_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtgrupocod.Text) & string.IsNullOrEmpty(txtgrupodesc.Text))
                    {
                        BuscarGrupo();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }


        public void BuscarGrupo()
        {
            try
            {
                txtgrupocod.Text = Accion.Formato(txtgrupocod.Text, 2);
                Logica_Grupo log = new Logica_Grupo();

                List<Entidad_Grupo> Generales = new List<Entidad_Grupo>();
                Generales = log.Listar(new Entidad_Grupo
                { Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Grupo = txtgrupocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Grupo T in Generales)
                    {
                        if ((T.Id_Grupo).ToString().Trim().ToUpper() == txtgrupocod.Text.Trim().ToUpper())
                        {
                            txtgrupocod.Text = (T.Id_Grupo).ToString().Trim();
                            txtgrupodesc.Text = T.Gru_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtgrupocod_TextChanged(object sender, EventArgs e)
        {
            if (txtgrupocod.Focus() == false)
            {
                txtgrupodesc.ResetText();
            }
        }

        private void txtgrupocod_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void frm_familia_edicion_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipobsacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;
                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.ResetText();
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



    }
}
