﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_tipo_especificacion : Contable.frm_fuente
    {
        public frm_tipo_especificacion()
        {
            InitializeComponent();
        }

        public  string Esp_Codigo;

        void Limpiar()
        {
            txtdescripcion.ResetText();
            txtdescripcion.Focus();
        }
        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtdescripcion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtdescripcion.Focus();
                return false;
            }

            return true;
        }

        public List<Entidad_Tipo_Especificacion> Lista = new List<Entidad_Tipo_Especificacion>();
        public void Listar()
        {
            Entidad_Tipo_Especificacion Ent = new Entidad_Tipo_Especificacion();
            Logica_Tipo_Especificacion log = new Logica_Tipo_Especificacion();

            Ent.Esp_Codigo = Esp_Codigo;
     
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Tipo_Especificacion Ent = new Entidad_Tipo_Especificacion();
                    Logica_Tipo_Especificacion Log = new Logica_Tipo_Especificacion();
                  
                    Ent.Esp_Codigo = Esp_Codigo;
                
                    Ent.Esp_Descripcion = txtdescripcion.Text;


                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();

                            Listar();
                            txtdescripcion.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            Listar();
                           
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        Entidad_Tipo_Especificacion Entidad = new Entidad_Tipo_Especificacion();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                    Esp_Codigo = Entidad.Esp_Codigo;
                    txtdescripcion.Text = Entidad.Esp_Descripcion;
                    gbdatos.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void frm_tipo_especificacion_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Nuevo;
            gbdatos.Enabled = true;
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;
            gbdatos.Enabled = true;
            txtdescripcion.Focus();
        }

        private void btnseleccionar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccionar.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccionar.PerformClick();
                e.SuppressKeyPress = true;
            }
        }
    }
}
