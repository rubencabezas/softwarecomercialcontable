﻿using Contable;
using Contable._2_Alertas;
using DevExpress.Compression;
using DevExpress.DataProcessing;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_catalogo : frm_fuente
    {
        public frm_catalogo()
        {
            InitializeComponent();
        }

        public string Tipo;
        public string Desde_Catalogo;

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
       
            using (frm_catalogo_edicion f = new frm_catalogo_edicion())
            {

                //f.Desde_Catalogo = Desde_Catalogo;
                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }

        }


        Entidad_Catalogo Entidad = new Entidad_Catalogo();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }


        public List<Entidad_Catalogo> Lista = new List<Entidad_Catalogo>();
        public void Listar()
        {
            Entidad_Catalogo Ent = new Entidad_Catalogo();
            Logica_Catalogo log = new Logica_Catalogo();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //Ent.Id_Tipo = "0031";
            Ent.Id_Grupo = null;
            Ent.Id_Familia = null;
            Ent.Id_Catalogo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Estado = Estados.Modificar;

            //using (frm_catalogo_edicion f = new frm_catalogo_edicion())
            //{

            //    //f.txtdescripcion.Tag = Entidad.Id_Grupo;
            //    //f.txtdescripcion.Text = Entidad.Gru_Descripcion;

            //            //Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //            f.txtgrupocod.Text = Entidad.Id_Grupo ;
            //            f.txtgrupodesc.Text = Entidad.Gru_Descripcion;
            //            f.txtfamiliacod.Text = Entidad.Id_Familia;
            //            f.txtfamiliadesc.Text = Entidad.Fam_Descripcion;
            //    //Ent.Id_Tipo = "0031";
            //    f.txtdescripcion.Tag = Entidad.Id_Catalogo.Trim();
            //            f.txtdescripcion.Text  = Entidad.Cat_Descripcion.Trim();

            //            f.txtserie.Text  = Entidad.Cat_Serie.Trim();
            //            f.txtbarra.Text   = Entidad.Cat_Barra.Trim();
            //            f.txtmarcacod.Text = Entidad.Id_Marca.Trim();
            //            f.txtmarcadesc.Text = Entidad.Mar_Descripcion.Trim();
            //            f.txtunmcod.Text = Entidad.Id_Unidad_Medida.Trim();
            //            f.txtunmdesc.Text = Entidad.Und_Descripcion.Trim();
            //            f.txtexistenciacod.Text = Entidad.Id_Tipo_Existencia.Trim();
            //            f.txtexistenciadesc.Text = Entidad.Exs_Nombre;
            //            f.txtundpresentacion.Text = Entidad.Cat_Unidades_Presentacion.ToString() ;
            //            f.rchcomentarios.Text = Entidad.Cat_Comentarios ;

            //             //Entidad_Catalogo Det = new Entidad_Catalogo;

            //         Logica_Catalogo log = new Logica_Catalogo();

            //           f.ListEspeci =  log.Listar_especificaciones(Entidad);
            //            f.dgvdatos.DataSource = f.ListEspeci;

            //    if (f.ShowDialog() == DialogResult.OK)
            //    {

            //        Entidad_Catalogo Ent = new Entidad_Catalogo();
            //        Logica_Catalogo Log = new Logica_Catalogo();

            //        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //        Ent.Id_Tipo = "0031";
            //        Ent.Id_Grupo = f.txtgrupocod.Text.ToString();
            //        Ent.Gru_Descripcion = f.txtgrupodesc.Text;

            //        Ent.Id_Familia  =  f.txtfamiliacod.Text;
            //        Ent.Fam_Descripcion  =  f.txtfamiliadesc.Text;

            //        Ent.Id_Catalogo = f.txtdescripcion.Tag.ToString();
            //        Ent.Cat_Descripcion  = f.txtdescripcion.Text;

            //        Ent.Cat_Serie  =  f.txtserie.Text.Trim();
            //        Ent.Cat_Barra  = f.txtbarra.Text.Trim();
            //        Ent.Id_Marca  = f.txtmarcacod.Text;
            //        Ent.Mar_Descripcion  = f.txtmarcadesc.Text;
            //        Ent.Id_Unidad_Medida  =f.txtunmcod.Text;
            //        Ent.Und_Descripcion  =f.txtunmdesc.Text;
            //        Ent.Id_Tipo_Existencia =  f.txtexistenciacod.Text.Trim() ;
            //        Ent.Exs_Nombre  =  f.txtexistenciadesc.Text;
            //        Ent.Cat_Unidades_Presentacion = Convert.ToDecimal(f.txtundpresentacion.Text) ;
            //        Ent.Cat_Comentarios =   f.rchcomentarios.Text  ;
            //        Ent.Detalle_especificaciones = f.ListEspeci;
            //        try
            //        {
            //            if (Estado == Estados.Modificar)
            //            {

            //                if (Log.Modificar(Ent))
            //                {
            //                    Accion.ExitoModificar();
            //                    Listar();
            //                }
            //                else
            //                {
            //                    MessageBox.Show("No se puedo insertar");
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception(ex.Message);
            //        }



            //    }
            //}

            using (frm_catalogo_edicion f = new frm_catalogo_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_tipo = Entidad.Id_Tipo;
                f.Id_Catalogo = Entidad.Id_Catalogo;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }

        }

 
        private void frm_catalogo_Load(object sender, EventArgs e)
        {

            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "El producto a eliminar es el siguiente :" + Entidad.Cat_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Catalogo Ent = new Entidad_Catalogo
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Grupo = Entidad.Id_Grupo,
                        Id_Tipo = Entidad.Id_Tipo,
                        Id_Familia = Entidad.Id_Familia,
                        Id_Catalogo=Entidad.Id_Catalogo
                    };

                    Logica_Catalogo log = new Logica_Catalogo();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Catalogo" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnseleccionar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void btnrevisarPrecios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using(frm_precio f = new frm_precio())
            {
                if (f.ShowDialog()== DialogResult.OK)
                {

                }
            }
        }

 

        private void btnexportardatos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


      // System.Threading.Thread p = new System.Threading.Thread((this.Progreso));
            try
            {

               // p.Start();


                Logica_Exportar_Importar log = new Logica_Exportar_Importar();
                    Entidad_Exportar entidad = new Entidad_Exportar();
                   entidad.Empresa = Actual_Conexion.CodigoEmpresa;
                    //CATALOGO
                    DataSet CTB_MARCA = new DataSet();
                    DataSet LOG_FABRICANTE = new DataSet();
                    DataSet CTB_UNIDAD_MEDIDA = new DataSet();
                    DataSet CTB_CATALOGO_GRUPO = new DataSet();
                    DataSet CTB_CATALOGO_FAMILIA = new DataSet();
                    DataSet CTB_CATALOGO = new DataSet();
                    DataSet CTB_CATALOGO_UNM = new DataSet();
                    DataSet CTB_CATALOGO_OPERACION_VENTA = new DataSet();

                    CTB_MARCA = log.EXPORTAR_MARCA_XML(entidad);
                    LOG_FABRICANTE = log.EXPORTAR_FABRICANTE_XML(entidad);
                    CTB_UNIDAD_MEDIDA = log.EXPORTAR_UNIDAD_MEDIDA_XML(entidad);
                    CTB_CATALOGO_GRUPO = log.EXPORTAR_CATALOGO_GRUPO_XML(entidad);
                    CTB_CATALOGO_FAMILIA = log.EXPORTAR_CATALOGO_FAMILIA_XML(entidad);
                    CTB_CATALOGO = log.EXPORTAR_CATALOGO_XML(entidad);
                    CTB_CATALOGO_UNM = log.EXPORTAR_CATALOGO_UNM_XML(entidad);
                    CTB_CATALOGO_OPERACION_VENTA = log.EXPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(entidad);


                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    string Nombre = "CATALOGO";// + EstCod.ToUpper.Trim + " " + FechaServer();
                    string Ruta;
                    Ruta = desktopPath + @"\" + Nombre;

                    File.Delete(Ruta + ".rar");
                    if (!Directory.Exists(Ruta))
                    {
                        Directory.CreateDirectory(Ruta);
                    }
                    else
                    {
                        foreach (var f in Directory.GetFiles(Ruta))
                            File.Delete(Conversions.ToString(f));
                    }

                    string marca = "CTB_MARCA";
                    string fabricante = "LOG_FABRICANTE";
                    string unidad_medida = "CTB_UNIDAD_MEDIDA";
                    string catalogo_grupo= "CTB_CATALOGO_GRUPO";
                    string catalogo_familia = "CTB_CATALOGO_FAMILIA";
                    string catalogo = "CTB_CATALOGO";
                    string catalogo_unm = "CTB_CATALOGO_UNM";
                    string catalogo_operventa = "CTB_CATALOGO_OPERACION_VENTA";



                File.WriteAllText(Ruta + @"\" + marca + ".csv", ConvertToCSV(CTB_MARCA.Tables[0]));
                File.WriteAllText(Ruta + @"\" + fabricante + ".csv", ConvertToCSV(LOG_FABRICANTE.Tables[0]));
                File.WriteAllText(Ruta + @"\" + unidad_medida + ".csv", ConvertToCSV(CTB_UNIDAD_MEDIDA.Tables[0]));

                File.WriteAllText(Ruta + @"\" + catalogo_grupo + ".csv", ConvertToCSV(CTB_CATALOGO_GRUPO.Tables[0]));
                File.WriteAllText(Ruta + @"\" + catalogo_familia + ".csv", ConvertToCSV(CTB_CATALOGO_FAMILIA.Tables[0]));
                File.WriteAllText(Ruta + @"\" + catalogo + ".csv", ConvertToCSV(CTB_CATALOGO.Tables[0]));
                File.WriteAllText(Ruta + @"\" + catalogo_unm + ".csv", ConvertToCSV(CTB_CATALOGO_UNM.Tables[0]));
                File.WriteAllText(Ruta + @"\" + catalogo_operventa + ".csv", ConvertToCSV(CTB_CATALOGO_OPERACION_VENTA.Tables[0]));



                var files = Directory.GetFiles(Ruta);
                string Password = Actual_Conexion.RucEmpresa.Trim();
                Comprimir(files, Ruta, Accion.Encriptar(Password));

 

              //  p.Abort();
            }
            catch(Exception ex)
            {
              //  p.Abort();
                Accion.ErrorSistema(ex.Message);
            }

   


        }

        //public void Progreso()
        //{
        //    WaitForm1 dlg = new WaitForm1();
        //    dlg.ShowDialog();
        //}

        public static void Comprimir(string[] Archivos, string Ruta, string password)
        {
            using (var archive = new ZipArchive())
            {
                foreach (string file in Archivos)
                {
                    ZipFileItem zipFI = archive.AddFile(file, "/");
                    zipFI.EncryptionType = EncryptionType.Aes128;
                    zipFI.Password = password;
                }

                archive.Save(Ruta + ".rar");
               // File.Move(Ruta + ".rar", Ruta + ".xml");
                archive.Dispose();
            }
        }


        private static string ConvertToCSV(DataTable oDt)
        {
            var sb = new StringBuilder();
            var columnNames = oDt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
            sb.AppendLine(string.Join("|", columnNames));
            foreach (DataRow row in oDt.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                sb.AppendLine(string.Join("|", fields));
            }

            return sb.ToString();
        }

        string RutaArchivo = "";

        private void btnimportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

         /////   System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {
        
    
                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "ZIP Folder (.rar)|*.rar";
                openFileDialog1.ShowDialog();
                openFileDialog1.Multiselect = true;
                RutaArchivo = openFileDialog1.FileName;

                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Nombre = Path.GetFileNameWithoutExtension(RutaArchivo);
                string Destino;
                Destino = desktopPath + @"\" + Nombre;

              //  p.Start();

                if (!Directory.Exists(Destino))
                {
                    Directory.CreateDirectory(Destino);
                }

                if (Directory.Exists(Destino))
                {
                    foreach (var f in Directory.GetFiles(Destino))

                     File.Delete(Conversions.ToString(f));
                     string Password = Actual_Conexion.RucEmpresa.Trim();
                    if (!Descomprimir(RutaArchivo, Destino, Accion.Encriptar(Password)))
                    {
                        Directory.Delete(Destino, true);
                    }
                }


                if (Directory.Exists(Destino))
                {
                    // Aki empesamos a importar los archivos
                    Importar_productos(Destino);
 
                }

              //  p.Abort();
            }
            catch (Exception ex)
            {
              //  p.Abort();
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void Importar_productos(string Destino)
        {
            if (File.Exists(Destino + @"\" + "CTB_MARCA.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MARCA.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_MARCA_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "LOG_FABRICANTE.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "LOG_FABRICANTE.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_FABRICANTE_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_UNIDAD_MEDIDA.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_UNIDAD_MEDIDA.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_UNIDAD_MEDIDA_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_CATALOGO_GRUPO.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_CATALOGO_GRUPO.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CATALOGO_GRUPO_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_CATALOGO_FAMILIA.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_CATALOGO_FAMILIA.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CATALOGO_FAMILIA_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_CATALOGO.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_CATALOGO.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CATALOGO_XML(Dt);
            }
            if (File.Exists(Destino + @"\" + "CTB_CATALOGO_UNM.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_CATALOGO_UNM.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CATALOGO_UNM_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_CATALOGO_OPERACION_VENTA.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_CATALOGO_OPERACION_VENTA.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(Dt);
            }

        }


        private static DataTable GetDataTableFromCsv(string path__1, bool isFirstRowHeader)
        {
            var csvData = new DataTable();
            using (var csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(path__1))
            {
                csvReader.SetDelimiters(new string[] { "|" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                var colFields = csvReader.ReadFields();
                foreach (var Column in colFields)
                {
                    var datecolumn = new DataColumn(Column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }

                int Cont = 2;
                while (!csvReader.EndOfData)
                {
                    var fieldData = csvReader.ReadFields();
                    // Making empty value as null
                    if (fieldData.Count() != csvData.Columns.Count)
                    {
                        Accion.Advertencia("ARCHIVO CON FALLAS,El archivo " + Path.GetFileName(path__1) + " En la linea " + Cont + " la cantidad de columnas de cabecera es " + csvData.Columns.Count + " la fila presenta " + fieldData.Count());
                    }

                    for (int i = 0, loopTo = fieldData.Length - 1; i <= loopTo; i++)
                    {
                        if (string.IsNullOrEmpty(fieldData[i]))
                        {
                            fieldData[i] = null;
                        }
                    }

                    csvData.Rows.Add(fieldData);
                    Cont = Cont + 1;
                }
            }

            return csvData;
        }


        public static bool Descomprimir(string ArchivoRar, string RutaDescomprimir, string password)
        {
            using (ZipArchive archive = ZipArchive.Read(ArchivoRar))
            {
                foreach (ZipItem item in archive)
                {
                    try
                    {
                        item.Password = password;
                        item.Extract(RutaDescomprimir);
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccionar.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccionar.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

    }
}
