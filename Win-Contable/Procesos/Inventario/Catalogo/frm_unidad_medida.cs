﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Inventario.Catalogo
{
    public partial class frm_unidad_medida : Contable.frm_fuente
    {
        public frm_unidad_medida()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_unidad_medida_edicion f = new frm_unidad_medida_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
                    Logica_Unidad_Medida Log = new Logica_Unidad_Medida();

                    Ent.Und_Descripcion = f.txtdescripcion.Text;
                    Ent.Und_Abreviado = f.txtabreviado.Text;
                    Ent.Und_SunaT = f.txtsunat.Text;


                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }


        Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_unidad_medida_edicion f = new frm_unidad_medida_edicion())
            {

                f.txtdescripcion.Tag = Entidad.Id_Unidad_Medida;
                f.txtdescripcion.Text = Entidad.Und_Descripcion;
                f.txtabreviado.Text = Entidad.Und_Abreviado;
                f.txtsunat.Text = Entidad.Und_SunaT;


                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
                    Logica_Unidad_Medida Log = new Logica_Unidad_Medida();


                    Ent.Id_Unidad_Medida = f.txtdescripcion.Tag.ToString();
                    Ent.Und_Descripcion = f.txtdescripcion.Text;
                    Ent.Und_Abreviado = f.txtabreviado.Text;
                    Ent.Und_SunaT = f.txtsunat.Text;


                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }

        }


        public List<Entidad_Unidad_Medida> Lista = new List<Entidad_Unidad_Medida>();
        public void Listar()
        {
            Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
            Logica_Unidad_Medida log = new Logica_Unidad_Medida();
            Ent.Id_Unidad_Medida = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_unidad_medida_Load(object sender, EventArgs e)
        {
            Listar();

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\UnidadMedida" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
