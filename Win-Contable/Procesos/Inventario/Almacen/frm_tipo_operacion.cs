﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Inventario.Almacen
{
    public partial class frm_tipo_operacion : Contable.frm_fuente
    {
        public frm_tipo_operacion()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            using (frm_tipo_operacion_edicion f = new frm_tipo_operacion_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Tipo_Operacion Ent = new Entidad_Tipo_Operacion();
                    Logica_Tipo_Operacion Log = new Logica_Tipo_Operacion();

                    Ent.Top_Entrada = f.chkentrada.Checked;
                    Ent.Top_Salida = f.chksalida.Checked;
                    Ent.Top_Descripcion = f.txtdescripcion.Text;
                    Ent.Top_Sunat = f.txtsunat.Text;
                    Ent.Top_Genera_Asiento = f.chkgeneraasiento.Checked;
                    Ent.Top_Necesita_Doc = f.chkrequieredoc.Checked;
                    Ent.Top_NecesitaCC = f.chkcentrocosto.Checked;
                    Ent.Top_NecesitaCG = f.chkcentrogasto.Checked;
                    Ent.Top_Tipo_Doc_Defecto = f.txttipodoc.Text;
                    Ent.Top_Es_Saldo_Inicial = f.chksaldoinicial.Checked;
                    Ent.Top_Es_Valorizado = f.chkvalorizado.Checked;
                    Ent.Top_Es_Salida_Produccion = f.chksalidaproduccion.Checked;
                    Ent.Top_Requiere_Transferecia_Almacen= f.chktransferencia.Checked;

                    Ent.Top_Requiere_Tipo_Entidad = f.chkRequiereEntidad.Checked;
                    Ent.Top_Tipo_Entidad_Cod = f.txttipoentcod.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
        }


        public List<Entidad_Tipo_Operacion> Lista = new List<Entidad_Tipo_Operacion>();
        public void Listar()
        {
            Entidad_Tipo_Operacion Ent = new Entidad_Tipo_Operacion();
            Logica_Tipo_Operacion log = new Logica_Tipo_Operacion();

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_tipo_operacion_edicion f = new frm_tipo_operacion_edicion())
            {

                //f.txtnombre.Tag = Entidad.Id_Moneda;
                //f.txtnombre.Text = Entidad.Nombre_Moneda;
                //f.txtsunat.Text = Entidad.Id_Sunat;
                //f.chmonedanac.Checked = Entidad.Es_nacional;

                f.Id_Operacion = Entidad.Id_Operacion;
                                f.chkentrada.Checked = Entidad.Top_Entrada  ;
                                f.chksalida.Checked = Entidad.Top_Salida ;
                                f.txtdescripcion.Text = Entidad.Top_Descripcion ;
                                f.txtsunat.Text = Entidad.Top_Sunat;
                                f.chkgeneraasiento.Checked= Entidad.Top_Genera_Asiento ;
                                f.chkrequieredoc.Checked = Entidad.Top_Necesita_Doc ;
                                f.chkcentrocosto.Checked  = Entidad.Top_NecesitaCC;
                                f.chkcentrogasto.Checked= Entidad.Top_NecesitaCG ;
                                f.txttipodoc.Text = Entidad.Top_Tipo_Doc_Defecto;
                                f.txttipodocdesc.Text = Entidad.Top_Tipo_Doc_Defecto_Desc;
                                f.chksaldoinicial.Checked  = Entidad.Top_Es_Saldo_Inicial;
                                f.chkvalorizado.Checked = Entidad.Top_Es_Valorizado;
                                f.chksalidaproduccion.Checked  = Entidad.Top_Es_Salida_Produccion;
              
                                f.chktransferencia.Checked=    Entidad.Top_Requiere_Transferecia_Almacen;

              f.chkRequiereEntidad.Checked  = Entidad.Top_Requiere_Tipo_Entidad ;
              f.txttipoentcod.Text  = Entidad.Top_Tipo_Entidad_Cod ;
                f.txttipoentdesc.Text = Entidad.Ent_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Tipo_Operacion Ent = new Entidad_Tipo_Operacion();
                    Logica_Tipo_Operacion Log = new Logica_Tipo_Operacion();

                    //Ent.Id_Moneda = f.txtnombre.Tag.ToString();
                    //Ent.Nombre_Moneda = f.txtnombre.Text;
                    //Ent.Id_Sunat = f.txtsunat.Text;
                    //Ent.Es_nacional = f.chmonedanac.Checked;
                    Ent.Id_Operacion = f.Id_Operacion;
                    Ent.Top_Entrada = f.chkentrada.Checked;
                    Ent.Top_Salida = f.chksalida.Checked;
                    Ent.Top_Descripcion = f.txtdescripcion.Text;
                    Ent.Top_Sunat = f.txtsunat.Text;
                    Ent.Top_Genera_Asiento = f.chkgeneraasiento.Checked;
                    Ent.Top_Necesita_Doc = f.chkrequieredoc.Checked;
                    Ent.Top_NecesitaCC = f.chkcentrocosto.Checked;
                    Ent.Top_NecesitaCG = f.chkcentrogasto.Checked;
                    Ent.Top_Tipo_Doc_Defecto = f.txttipodoc.Text;
                    Ent.Top_Tipo_Doc_Defecto_Desc = f.txttipodocdesc.Text;
                    Ent.Top_Es_Saldo_Inicial = f.chksaldoinicial.Checked;
                    Ent.Top_Es_Valorizado = f.chkvalorizado.Checked;
                    Ent.Top_Es_Salida_Produccion = f.chksalidaproduccion.Checked;

                    Ent.Top_Requiere_Transferecia_Almacen = f.chktransferencia.Checked;

                    Ent.Top_Requiere_Tipo_Entidad  =   f.chkRequiereEntidad.Checked ;
                    Ent.Top_Tipo_Entidad_Cod   =  f.txttipoentcod.Text;
                    Ent.Ent_Descripcion  = f.txttipoentdesc.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        Entidad_Tipo_Operacion Entidad = new Entidad_Tipo_Operacion();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_tipo_operacion_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);


        }
    }
}
