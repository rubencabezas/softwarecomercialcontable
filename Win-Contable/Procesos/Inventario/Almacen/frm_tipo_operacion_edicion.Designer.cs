﻿namespace Contable.Procesos.Inventario.Almacen
{
    partial class frm_tipo_operacion_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoentdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipoentcod = new DevExpress.XtraEditors.TextEdit();
            this.chkRequiereEntidad = new DevExpress.XtraEditors.CheckEdit();
            this.chktransferencia = new DevExpress.XtraEditors.CheckEdit();
            this.chkcentrogasto = new DevExpress.XtraEditors.CheckEdit();
            this.chkcentrocosto = new DevExpress.XtraEditors.CheckEdit();
            this.txtsunat = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chksalidaproduccion = new DevExpress.XtraEditors.CheckEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.chkvalorizado = new DevExpress.XtraEditors.CheckEdit();
            this.chksaldoinicial = new DevExpress.XtraEditors.CheckEdit();
            this.chkgeneraasiento = new DevExpress.XtraEditors.CheckEdit();
            this.chkrequieredoc = new DevExpress.XtraEditors.CheckEdit();
            this.chksalida = new DevExpress.XtraEditors.CheckEdit();
            this.chkentrada = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequiereEntidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chktransferencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcentrogasto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcentrocosto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsunat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksalidaproduccion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkvalorizado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksaldoinicial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkgeneraasiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkrequieredoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksalida.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkentrada.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(840, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 151);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(840, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 117);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(840, 34);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 117);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txttipoentdesc);
            this.groupControl1.Controls.Add(this.txttipoentcod);
            this.groupControl1.Controls.Add(this.chkRequiereEntidad);
            this.groupControl1.Controls.Add(this.chktransferencia);
            this.groupControl1.Controls.Add(this.chkcentrogasto);
            this.groupControl1.Controls.Add(this.chkcentrocosto);
            this.groupControl1.Controls.Add(this.txtsunat);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.chksalidaproduccion);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txtdescripcion);
            this.groupControl1.Controls.Add(this.chkvalorizado);
            this.groupControl1.Controls.Add(this.chksaldoinicial);
            this.groupControl1.Controls.Add(this.chkgeneraasiento);
            this.groupControl1.Controls.Add(this.chkrequieredoc);
            this.groupControl1.Controls.Add(this.chksalida);
            this.groupControl1.Controls.Add(this.chkentrada);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Location = new System.Drawing.Point(0, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(828, 109);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Datos generales";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(581, 76);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 13);
            this.labelControl4.TabIndex = 18;
            this.labelControl4.Text = "Tipo Ent.";
            // 
            // txttipoentdesc
            // 
            this.txttipoentdesc.Enabled = false;
            this.txttipoentdesc.Location = new System.Drawing.Point(678, 73);
            this.txttipoentdesc.MenuManager = this.barManager1;
            this.txttipoentdesc.Name = "txttipoentdesc";
            this.txttipoentdesc.Size = new System.Drawing.Size(137, 20);
            this.txttipoentdesc.TabIndex = 20;
            // 
            // txttipoentcod
            // 
            this.txttipoentcod.Enabled = false;
            this.txttipoentcod.Location = new System.Drawing.Point(630, 73);
            this.txttipoentcod.MenuManager = this.barManager1;
            this.txttipoentcod.Name = "txttipoentcod";
            this.txttipoentcod.Size = new System.Drawing.Size(45, 20);
            this.txttipoentcod.TabIndex = 19;
            this.txttipoentcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoentcod_KeyDown);
            // 
            // chkRequiereEntidad
            // 
            this.chkRequiereEntidad.Location = new System.Drawing.Point(471, 73);
            this.chkRequiereEntidad.MenuManager = this.barManager1;
            this.chkRequiereEntidad.Name = "chkRequiereEntidad";
            this.chkRequiereEntidad.Properties.Caption = "Requiere Entidad";
            this.chkRequiereEntidad.Size = new System.Drawing.Size(104, 19);
            this.chkRequiereEntidad.TabIndex = 17;
            this.chkRequiereEntidad.CheckedChanged += new System.EventHandler(this.chkRequiereEntidad_CheckedChanged);
            // 
            // chktransferencia
            // 
            this.chktransferencia.Location = new System.Drawing.Point(301, 73);
            this.chktransferencia.MenuManager = this.barManager1;
            this.chktransferencia.Name = "chktransferencia";
            this.chktransferencia.Properties.Caption = "Transferencia entre almacenes";
            this.chktransferencia.Size = new System.Drawing.Size(181, 19);
            this.chktransferencia.TabIndex = 16;
            // 
            // chkcentrogasto
            // 
            this.chkcentrogasto.Location = new System.Drawing.Point(184, 53);
            this.chkcentrogasto.MenuManager = this.barManager1;
            this.chkcentrogasto.Name = "chkcentrogasto";
            this.chkcentrogasto.Properties.Caption = "C. Gasto";
            this.chkcentrogasto.Size = new System.Drawing.Size(75, 19);
            this.chkcentrogasto.TabIndex = 8;
            // 
            // chkcentrocosto
            // 
            this.chkcentrocosto.Location = new System.Drawing.Point(108, 53);
            this.chkcentrocosto.MenuManager = this.barManager1;
            this.chkcentrocosto.Name = "chkcentrocosto";
            this.chkcentrocosto.Properties.Caption = "C. Costo";
            this.chkcentrocosto.Size = new System.Drawing.Size(75, 19);
            this.chkcentrocosto.TabIndex = 7;
            // 
            // txtsunat
            // 
            this.txtsunat.EnterMoveNextControl = true;
            this.txtsunat.Location = new System.Drawing.Point(754, 28);
            this.txtsunat.MenuManager = this.barManager1;
            this.txtsunat.Name = "txtsunat";
            this.txtsunat.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtsunat.Size = new System.Drawing.Size(61, 20);
            this.txtsunat.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(694, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Cod. Sunat";
            // 
            // chksalidaproduccion
            // 
            this.chksalidaproduccion.Location = new System.Drawing.Point(184, 73);
            this.chksalidaproduccion.MenuManager = this.barManager1;
            this.chksalidaproduccion.Name = "chksalidaproduccion";
            this.chksalidaproduccion.Properties.Caption = "Salida a produccion";
            this.chksalidaproduccion.Size = new System.Drawing.Size(123, 19);
            this.chksalidaproduccion.TabIndex = 15;
            // 
            // txttipodoc
            // 
            this.txttipodoc.Enabled = false;
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(453, 50);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(29, 20);
            this.txttipodoc.TabIndex = 11;
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(483, 50);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(332, 20);
            this.txttipodocdesc.TabIndex = 12;
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.EnterMoveNextControl = true;
            this.txtdescripcion.Location = new System.Drawing.Point(200, 28);
            this.txtdescripcion.MenuManager = this.barManager1;
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Size = new System.Drawing.Size(488, 20);
            this.txtdescripcion.TabIndex = 3;
            // 
            // chkvalorizado
            // 
            this.chkvalorizado.Location = new System.Drawing.Point(107, 73);
            this.chkvalorizado.MenuManager = this.barManager1;
            this.chkvalorizado.Name = "chkvalorizado";
            this.chkvalorizado.Properties.Caption = "Valorizado";
            this.chkvalorizado.Size = new System.Drawing.Size(92, 19);
            this.chkvalorizado.TabIndex = 14;
            // 
            // chksaldoinicial
            // 
            this.chksaldoinicial.Location = new System.Drawing.Point(12, 73);
            this.chksaldoinicial.MenuManager = this.barManager1;
            this.chksaldoinicial.Name = "chksaldoinicial";
            this.chksaldoinicial.Properties.Caption = "Saldo Inicial";
            this.chksaldoinicial.Size = new System.Drawing.Size(75, 19);
            this.chksaldoinicial.TabIndex = 13;
            // 
            // chkgeneraasiento
            // 
            this.chkgeneraasiento.Location = new System.Drawing.Point(12, 53);
            this.chkgeneraasiento.MenuManager = this.barManager1;
            this.chkgeneraasiento.Name = "chkgeneraasiento";
            this.chkgeneraasiento.Properties.Caption = "Genera asiento";
            this.chkgeneraasiento.Size = new System.Drawing.Size(97, 19);
            this.chkgeneraasiento.TabIndex = 6;
            // 
            // chkrequieredoc
            // 
            this.chkrequieredoc.Location = new System.Drawing.Point(259, 54);
            this.chkrequieredoc.MenuManager = this.barManager1;
            this.chkrequieredoc.Name = "chkrequieredoc";
            this.chkrequieredoc.Properties.Caption = "Requiere Doc.";
            this.chkrequieredoc.Size = new System.Drawing.Size(101, 19);
            this.chkrequieredoc.TabIndex = 9;
            this.chkrequieredoc.CheckedChanged += new System.EventHandler(this.chkrequieredoc_CheckedChanged);
            // 
            // chksalida
            // 
            this.chksalida.Location = new System.Drawing.Point(80, 32);
            this.chksalida.MenuManager = this.barManager1;
            this.chksalida.Name = "chksalida";
            this.chksalida.Properties.Caption = "Salida";
            this.chksalida.Size = new System.Drawing.Size(54, 19);
            this.chksalida.TabIndex = 1;
            // 
            // chkentrada
            // 
            this.chkentrada.EnterMoveNextControl = true;
            this.chkentrada.Location = new System.Drawing.Point(12, 32);
            this.chkentrada.MenuManager = this.barManager1;
            this.chkentrada.Name = "chkentrada";
            this.chkentrada.Properties.Caption = "Entrada";
            this.chkentrada.Size = new System.Drawing.Size(62, 19);
            this.chkentrada.TabIndex = 0;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(140, 35);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Descripcion";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(366, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(81, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Doc. por defecto";
            // 
            // frm_tipo_operacion_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(840, 151);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_tipo_operacion_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tipo de operacion";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequiereEntidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chktransferencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcentrogasto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcentrocosto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsunat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksalidaproduccion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkvalorizado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksaldoinicial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkgeneraasiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkrequieredoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chksalida.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkentrada.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.CheckEdit chksalidaproduccion;
        public DevExpress.XtraEditors.TextEdit txttipodoc;
        public DevExpress.XtraEditors.TextEdit txttipodocdesc;
        public DevExpress.XtraEditors.TextEdit txtdescripcion;
        public DevExpress.XtraEditors.CheckEdit chkvalorizado;
        public DevExpress.XtraEditors.CheckEdit chksaldoinicial;
        public DevExpress.XtraEditors.CheckEdit chkgeneraasiento;
        public DevExpress.XtraEditors.CheckEdit chkrequieredoc;
        public DevExpress.XtraEditors.CheckEdit chksalida;
        public DevExpress.XtraEditors.CheckEdit chkentrada;
        public DevExpress.XtraEditors.CheckEdit chkcentrogasto;
        public DevExpress.XtraEditors.CheckEdit chkcentrocosto;
        public DevExpress.XtraEditors.TextEdit txtsunat;
        public DevExpress.XtraEditors.CheckEdit chktransferencia;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraEditors.CheckEdit chkRequiereEntidad;
        public DevExpress.XtraEditors.TextEdit txttipoentdesc;
        public DevExpress.XtraEditors.TextEdit txttipoentcod;
    }
}
