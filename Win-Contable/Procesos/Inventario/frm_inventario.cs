﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Inventario
{
    public partial class frm_inventario : Contable.frm_fuente
    {
        public frm_inventario()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_inventario_edicion f = new frm_inventario_edicion())
            {


              Estado = Estados.Nuevo;
              f.Estado_Ven_Boton = "1";

               if (f.ShowDialog() == DialogResult.OK)
               {


                 try
                   {
                       Listar();
                    }
                    catch (Exception ex)
                   {
                      throw new Exception(ex.Message);
                  }
               }else
               {
                   Listar();
               }

           }
        }


        Entidad_Movimiento_Inventario Entidad = new Entidad_Movimiento_Inventario();
        public List<Entidad_Movimiento_Inventario> Lista = new List<Entidad_Movimiento_Inventario>();
        public void Listar()
        {
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
            Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Tipo_Mov = null;
            Ent.Id_Almacen = null;
            Ent.Id_Movimiento = null;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_inventario_Load(object sender, EventArgs e)
        {
            Listar();
        }


        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //using (frm_inventario_edicion f = new frm_inventario_edicion())
            //{


            //    Estado = Estados.Modificar;
            //    f.Estado_Ven_Boton = "2";

            //    f.Id_Empresa = Entidad.Id_Empresa;
            //    f.Id_Anio = Entidad.Id_Anio;
            //    f.Id_Periodo = Entidad.Id_Periodo;
            //    f.Id_Tipo_Mov = Entidad.Id_Tipo_Mov;
            //    f.Id_Almacen = Entidad.Id_Almacen;
            //    f.Id_Movimiento=Entidad.Id_Movimiento;
            //    f.txtmotivocod.Enabled = false;
            //    f.txtalmacencod.Enabled = false;

            //    if (f.ShowDialog() == DialogResult.OK)
            //    {


            //        try
            //        {
            //            Listar();
            //        }
            //        catch (Exception ex)
            //        {
            //            Accion.ErrorSistema(ex.Message);
            //        }



            //    }
            //    else
            //    {
            //        Listar();
            //    }
            //}
        }

        private void btnimprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
