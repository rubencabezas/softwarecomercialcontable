﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Ventas
{
    public partial class frm_ventas_regimen : Contable.frm_fuente
    {
        public frm_ventas_regimen()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;

        public void Habilitar_Regimen()
        {
            txttiporegimencod.Enabled = true;
            txtregtipodoccod.Enabled = true;
            txtregimenserie.Enabled = true;
            txtregimennumero.Enabled = true;
            txtporcentaje.Enabled = true;
            txtmonto.Enabled = true;
            txtfechadetrac.Enabled = true;

        }
        public void Desabilitar_Regimen()
        {
            txttiporegimencod.Enabled = false;
            txtregtipodoccod.Enabled = false;
            txtregimenserie.Enabled = false;
            txtregimennumero.Enabled = false;
            txtporcentaje.Enabled = false;
            txtmonto.Enabled = false;
            txtfechadetrac.Enabled = false;


        }

        public void Limpiar_Regimen()
        {
            txttiporegimencod.Tag = "";
            txttiporegimencod.ResetText();
            txttiporegimendesc.ResetText();
            txtregtipodoccod.ResetText();
            txtregtipodocdesc.ResetText();
            txtregimenserie.ResetText();
            txtregimennumero.ResetText();
            txtporcentaje.ResetText();
            txtmonto.ResetText();
        }

        private void txttiporegimencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttiporegimencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttiporegimencod.Text.Substring(txttiporegimencod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0013";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttiporegimencod.Tag = Entidad.Id_General_Det;
                                txttiporegimencod.Text = Entidad.Gen_Codigo_Interno;
                                txttiporegimendesc.Text = Entidad.Gen_Descripcion_Det;
                                txttiporegimencod.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttiporegimencod.Text) & string.IsNullOrEmpty(txttiporegimendesc.Text))
                    {
                        BuscarTiporEGIMEN();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTiporEGIMEN()
        {
            try
            {
                txttiporegimencod.Text = Accion.Formato(txttiporegimencod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0013",
                    Gen_Codigo_Interno = txttiporegimencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Id_General).ToString().Trim().ToUpper() == txttiporegimencod.Text.Trim().ToUpper())
                        {
                            txttiporegimencod.Tag = T.Id_General_Det;
                            txttiporegimencod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttiporegimendesc.Text = T.Gen_Descripcion_Det;
                            txttiporegimencod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttiporegimencod.EnterMoveNextControl = false;
                    txttiporegimencod.ResetText();
                    txttiporegimencod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtregtipodoccod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtregtipodoccod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtregtipodoccod.Text.Substring(txtregtipodoccod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtregtipodoccod.Text = Entidad.Id_SUnat;
                                txtregtipodoccod.Tag = Entidad.Id_Comprobante;
                                txtregtipodocdesc.Text = Entidad.Nombre_Comprobante;
            

                                txtregtipodoccod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtregtipodoccod.Text) & string.IsNullOrEmpty(txtregtipodocdesc.Text))
                    {
                        BuscarTipoDocumentoRegimen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarTipoDocumentoRegimen()
        {
            try
            {
                txtregtipodoccod.Text = Accion.Formato(txtregtipodoccod.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txtregtipodoccod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txtregtipodoccod.Text.Trim().ToUpper())
                        {
                            txtregtipodoccod.Text = (T.Id_SUnat).ToString().Trim();
                            txtregtipodoccod.Tag = (T.Id_Comprobante).ToString().Trim();
                            txtregtipodocdesc.Text = T.Nombre_Comprobante;
                            //txttipodoc.Text = Entidad.Id_SUnat;
                            //txttipodoc.Tag = Entidad.Id_Comprobante;
                            //txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                            txtregtipodoccod.EnterMoveNextControl = true;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtregtipodoccod.EnterMoveNextControl = false;
                    txtregtipodoccod.ResetText();
                    txtregtipodoccod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Limpiar_Regimen();
            Desabilitar_Regimen();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void txtregtipodoccod_TextChanged(object sender, EventArgs e)
        {
            if (txtregtipodoccod.Focus() == false)
            {
                txtregtipodocdesc.ResetText();
            }
        }

        private void txttiporegimencod_TextChanged(object sender, EventArgs e)
        {
            if (txttiporegimencod.Focus() == false)
            {
                txttiporegimendesc.ResetText();
            }
        }

        private void chkAfecto_CheckedChanged(object sender, EventArgs e)
        {

            if (chkAfecto.Checked)
            {
                Habilitar_Regimen();
                txttiporegimencod.Focus();
            }
            else
            {
                Desabilitar_Regimen();
                Limpiar_Regimen();
                txtfechadetrac.Text = "01/01/1900";
                txtmonto.Text = "0.00";
                txtporcentaje.Text = "0.00";
            }
        }

        private void frm_ventas_regimen_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Desabilitar_Regimen();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();
            }
        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];

                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = Enti.Id_Libro;
                    Voucher = Enti.Id_Voucher;


                    chkAfecto.Checked = Enti.Ctb_Afecto_RE;
                    txttiporegimencod.Tag = Enti.Ctb_Afecto_Tipo;
                    txttiporegimencod.Text = Enti.Ctb_Afecto_Tipo_cod_Interno;
                    txttiporegimendesc.Text = Enti.Ctb_Afecto_Tipo_Descripcion;
                    txtregtipodoccod.Text = Enti.Ctb_Afecto_Tipo_Doc;
                    txtregtipodocdesc.Text = Enti.Ctb_Afecto_Tipo_Doc_DEsc;
                    txtregimenserie.Text = Enti.Ctb_Afecto_Serie + "-" + Enti.Ctb_Afecto_Numero;

                    txtporcentaje.Text = Convert.ToString(Enti.Ctb_Afecto_Porcentaje);
                    txtmonto.Text = Convert.ToString(Enti.Ctb_Afecto_Monto);


                    if (String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Afecto_Fecha) == "01/01/0001")
                    {
                        txtfechadetrac.Text = "";
                    }
                    else
                    {
                        txtfechadetrac.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Afecto_Fecha);
                    }

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }


    }
}
