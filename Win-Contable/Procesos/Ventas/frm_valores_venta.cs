﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Ventas
{
    public partial class frm_valores_venta : Contable.frm_fuente
    {
        public frm_valores_venta()
        {
            InitializeComponent();
        }

        public List<Entidad_Movimiento_Cab> Detalles_CONT_Val;
        public DateTime Fecha;



         decimal Tasa;
         decimal Porcentaje;
        private void ActualizaIGV()
        {
            try
            {
                if (IsDate(Convert.ToString(this.Fecha)) && (DateTime.Parse(Convert.ToString(this.Fecha)).Year > 0x7d0))
                {
                    Entidad_Impuesto_Det det = new Entidad_Impuesto_Det
                    {
                        Imd_Fecha_Inicio = this.Fecha
                    };
                    List<Entidad_Impuesto_Det> list = new List<Entidad_Impuesto_Det>();
                    list = new Logica_Impuesto_Det().Igv_Actual(det);
                    if (list.Count > 0)
                    {
                        this.Tasa = list[0].Imd_Tasa;
                        this.Porcentaje = Math.Round((decimal)(list[0].Imd_Tasa * 100M), 2);
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }


        private void Bloquear()
        {
            this.txtbase01.Enabled = false;
            this.txtigv01.Enabled = false;
            this.txtexportacion.Enabled = false;
            this.txtisc.Enabled = false;
            this.txtotc.Enabled = false;
            this.txttotal.Enabled = false;
            this.txtexonerado.Enabled = false;
            this.txtinafecto.Enabled = false;
        }

        private void Desbloqear()
        {
            this.txtbase01.Enabled = true;
            this.txtigv01.Enabled = true;
            this.txtexportacion.Enabled = true;
            this.txtisc.Enabled = true;
            this.txtotc.Enabled = true;
            this.txttotal.Enabled = true;
            this.txtexonerado.Enabled = true;
            this.txtinafecto.Enabled = true;
        }




        private void Calcular_Importe_Total()
        {
            decimal num = (((((Convert.ToDecimal(this.txtbase01.Text) + Convert.ToDecimal(this.txtigv01.Text)) + Convert.ToDecimal(this.txtexportacion.Text)) + Convert.ToDecimal(this.txtotc.Text)) + Convert.ToDecimal(this.txtisc.Text)) + Convert.ToDecimal(this.txtinafecto.Text)) + Convert.ToDecimal(this.txtexonerado.Text);
            this.txttotal.Text = Convert.ToString(num);
        }




        private void Calcular_Cuando_son_Base()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab cab in this.Detalles_CONT_Val)
                {
                    if (cab.Ctb_Operacion_Cod == "0016")
                    {
                        decimal num = Math.Round((decimal)(Convert.ToDecimal(this.txtbase01.Text) * this.Tasa), 2);
                        this.txtigv01.Text = Convert.ToString(num);
                        break;
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }





        public static bool IsDate(string date)
        {
            try
            {
                DateTime time = DateTime.Parse(date);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        private void txtbase02_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtbase02_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                base.DialogResult = DialogResult.OK;
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }


        private void Calcular_Base_01()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab cab in this.Detalles_CONT_Val)
                {
                    if (cab.Ctb_Operacion_Cod == "0016")
                    {
                        decimal num = Math.Round((decimal)(Convert.ToDecimal(this.txtbase01.Text) * this.Tasa), 2);
                        this.txtigv01.Text = Convert.ToString(num);
                        break;
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }


     

        private void txtotc_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtotc.Text.Trim()))
            {
                try
                {
                    this.Calcular_Importe_Total();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtisc_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtisc.Text.Trim()))
            {
                try
                {
                    this.Calcular_Importe_Total();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtinafecto_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtinafecto.Text.Trim()))
            {
                try
                {
                    this.Calcular_Importe_Total();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtexportacion_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtexportacion.Text.Trim()))
            {
                try
                {
                    this.Calcular_Importe_Total();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtexonerado_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtexonerado.Text.Trim()))
            {
                try
                {
                    this.Calcular_Importe_Total();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }
        }

        private void txtbase01_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtbase01.Text.Trim()))
            {
                try
                {
                    this.Calcular_Base_01();
                    this.Calcular_Importe_Total();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }



        private void Habilitar_Orden()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab cab in this.Detalles_CONT_Val)
                {
                    Entidad_Movimiento_Cab cab2 = new Entidad_Movimiento_Cab();
                    if (cab.Ctb_Operacion_Cod == "0016")
                    {
                        this.txtbase01.Enabled = true;
                        this.txtbase01.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0017")
                    {
                        this.txtigv01.Enabled = true;
                        this.txtigv01.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0018")
                    {
                        this.txtexportacion.Enabled = true;
                        this.txtexportacion.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0019")
                    {
                        this.txtexonerado.Enabled = true;
                        this.txtexonerado.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0041")
                    {
                        this.txtisc.Enabled = true;
                        this.txtisc.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0042")
                    {
                        this.txtotc.Enabled = true;
                        this.txtotc.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0043")
                    {
                        this.txttotal.Enabled = true;
                        this.txttotal.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Debe);
                        continue;
                    }
                    if (cab.Ctb_Operacion_Cod == "0058")
                    {
                        this.txtinafecto.Enabled = true;
                        this.txtinafecto.Text = string.Format("{0:0,0.00}", cab.Ctb_Importe_Haber);
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void frm_valores_venta_Load(object sender, EventArgs e)
        {
            this.Bloquear();
            this.Habilitar_Orden();
            this.ActualizaIGV();

        }

        private void frm_valores_venta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }
    }
}
