﻿namespace Contable.Procesos.Ventas
{
    partial class frm_ventas_regimen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.txtmonto = new DevExpress.XtraEditors.TextEdit();
            this.txtporcentaje = new DevExpress.XtraEditors.TextEdit();
            this.txtregimennumero = new DevExpress.XtraEditors.TextEdit();
            this.txtregimenserie = new DevExpress.XtraEditors.TextEdit();
            this.txtfechadetrac = new DevExpress.XtraEditors.TextEdit();
            this.txtregtipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtregtipodoccod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.chkAfecto = new DevExpress.XtraEditors.CheckEdit();
            this.txttiporegimendesc = new DevExpress.XtraEditors.TextEdit();
            this.txttiporegimencod = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtporcentaje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregimennumero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregimenserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadetrac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregtipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregtipodoccod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfecto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimencod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2,
            this.btnlimpiar});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarAppearance.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bar2.BarAppearance.Normal.Options.UseBackColor = true;
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Añadir [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 2;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlimpiar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(704, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 131);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(704, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 99);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(704, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 99);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl33);
            this.groupControl2.Controls.Add(this.txtmonto);
            this.groupControl2.Controls.Add(this.txtporcentaje);
            this.groupControl2.Controls.Add(this.txtregimennumero);
            this.groupControl2.Controls.Add(this.txtregimenserie);
            this.groupControl2.Controls.Add(this.txtfechadetrac);
            this.groupControl2.Controls.Add(this.txtregtipodocdesc);
            this.groupControl2.Controls.Add(this.txtregtipodoccod);
            this.groupControl2.Controls.Add(this.labelControl32);
            this.groupControl2.Controls.Add(this.labelControl31);
            this.groupControl2.Controls.Add(this.labelControl30);
            this.groupControl2.Controls.Add(this.labelControl29);
            this.groupControl2.Controls.Add(this.labelControl26);
            this.groupControl2.Controls.Add(this.chkAfecto);
            this.groupControl2.Controls.Add(this.txttiporegimendesc);
            this.groupControl2.Controls.Add(this.txttiporegimencod);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 32);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(704, 99);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "Regimen IGV";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(110, 74);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(34, 13);
            this.labelControl33.TabIndex = 14;
            this.labelControl33.Text = "Monto:";
            // 
            // txtmonto
            // 
            this.txtmonto.EditValue = "0.00";
            this.txtmonto.EnterMoveNextControl = true;
            this.txtmonto.Location = new System.Drawing.Point(150, 71);
            this.txtmonto.MenuManager = this.barManager1;
            this.txtmonto.Name = "txtmonto";
            this.txtmonto.Properties.Mask.EditMask = "n2";
            this.txtmonto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtmonto.Size = new System.Drawing.Size(100, 20);
            this.txtmonto.TabIndex = 15;
            // 
            // txtporcentaje
            // 
            this.txtporcentaje.EditValue = "0.00";
            this.txtporcentaje.EnterMoveNextControl = true;
            this.txtporcentaje.Location = new System.Drawing.Point(642, 49);
            this.txtporcentaje.MenuManager = this.barManager1;
            this.txtporcentaje.Name = "txtporcentaje";
            this.txtporcentaje.Properties.Mask.EditMask = "n2";
            this.txtporcentaje.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtporcentaje.Size = new System.Drawing.Size(65, 20);
            this.txtporcentaje.TabIndex = 13;
            // 
            // txtregimennumero
            // 
            this.txtregimennumero.EnterMoveNextControl = true;
            this.txtregimennumero.Location = new System.Drawing.Point(452, 49);
            this.txtregimennumero.MenuManager = this.barManager1;
            this.txtregimennumero.Name = "txtregimennumero";
            this.txtregimennumero.Size = new System.Drawing.Size(100, 20);
            this.txtregimennumero.TabIndex = 11;
            // 
            // txtregimenserie
            // 
            this.txtregimenserie.EnterMoveNextControl = true;
            this.txtregimenserie.Location = new System.Drawing.Point(286, 50);
            this.txtregimenserie.MenuManager = this.barManager1;
            this.txtregimenserie.Name = "txtregimenserie";
            this.txtregimenserie.Size = new System.Drawing.Size(94, 20);
            this.txtregimenserie.TabIndex = 9;
            // 
            // txtfechadetrac
            // 
            this.txtfechadetrac.EnterMoveNextControl = true;
            this.txtfechadetrac.Location = new System.Drawing.Point(150, 49);
            this.txtfechadetrac.MenuManager = this.barManager1;
            this.txtfechadetrac.Name = "txtfechadetrac";
            this.txtfechadetrac.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadetrac.Properties.Mask.BeepOnError = true;
            this.txtfechadetrac.Properties.Mask.EditMask = "d";
            this.txtfechadetrac.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadetrac.Properties.Mask.SaveLiteral = false;
            this.txtfechadetrac.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadetrac.Properties.MaxLength = 10;
            this.txtfechadetrac.Size = new System.Drawing.Size(100, 20);
            this.txtfechadetrac.TabIndex = 7;
            // 
            // txtregtipodocdesc
            // 
            this.txtregtipodocdesc.Enabled = false;
            this.txtregtipodocdesc.EnterMoveNextControl = true;
            this.txtregtipodocdesc.Location = new System.Drawing.Point(506, 27);
            this.txtregtipodocdesc.MenuManager = this.barManager1;
            this.txtregtipodocdesc.Name = "txtregtipodocdesc";
            this.txtregtipodocdesc.Size = new System.Drawing.Size(201, 20);
            this.txtregtipodocdesc.TabIndex = 5;
            // 
            // txtregtipodoccod
            // 
            this.txtregtipodoccod.EnterMoveNextControl = true;
            this.txtregtipodoccod.Location = new System.Drawing.Point(452, 27);
            this.txtregtipodoccod.MenuManager = this.barManager1;
            this.txtregtipodoccod.Name = "txtregtipodoccod";
            this.txtregtipodoccod.Size = new System.Drawing.Size(51, 20);
            this.txtregtipodoccod.TabIndex = 4;
            this.txtregtipodoccod.TextChanged += new System.EventHandler(this.txtregtipodoccod_TextChanged);
            this.txtregtipodoccod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtregtipodoccod_KeyDown);
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(558, 52);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(74, 13);
            this.labelControl32.TabIndex = 12;
            this.labelControl32.Text = "Porcentaje (%)";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(399, 52);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(37, 13);
            this.labelControl31.TabIndex = 10;
            this.labelControl31.Text = "Numero";
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(256, 52);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(24, 13);
            this.labelControl30.TabIndex = 8;
            this.labelControl30.Text = "Serie";
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(111, 52);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(33, 13);
            this.labelControl29.TabIndex = 6;
            this.labelControl29.Text = "Fecha:";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(386, 30);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(49, 13);
            this.labelControl26.TabIndex = 3;
            this.labelControl26.Text = "Tipo Doc.:";
            // 
            // chkAfecto
            // 
            this.chkAfecto.EnterMoveNextControl = true;
            this.chkAfecto.Location = new System.Drawing.Point(5, 27);
            this.chkAfecto.MenuManager = this.barManager1;
            this.chkAfecto.Name = "chkAfecto";
            this.chkAfecto.Properties.Caption = "Afecto regimen especial";
            this.chkAfecto.Size = new System.Drawing.Size(139, 19);
            this.chkAfecto.TabIndex = 0;
            this.chkAfecto.CheckedChanged += new System.EventHandler(this.chkAfecto_CheckedChanged);
            // 
            // txttiporegimendesc
            // 
            this.txttiporegimendesc.Enabled = false;
            this.txttiporegimendesc.EnterMoveNextControl = true;
            this.txttiporegimendesc.Location = new System.Drawing.Point(204, 27);
            this.txttiporegimendesc.MenuManager = this.barManager1;
            this.txttiporegimendesc.Name = "txttiporegimendesc";
            this.txttiporegimendesc.Size = new System.Drawing.Size(176, 20);
            this.txttiporegimendesc.TabIndex = 2;
            // 
            // txttiporegimencod
            // 
            this.txttiporegimencod.EnterMoveNextControl = true;
            this.txttiporegimencod.Location = new System.Drawing.Point(150, 27);
            this.txttiporegimencod.MenuManager = this.barManager1;
            this.txttiporegimencod.Name = "txttiporegimencod";
            this.txttiporegimencod.Size = new System.Drawing.Size(51, 20);
            this.txttiporegimencod.TabIndex = 1;
            this.txttiporegimencod.TextChanged += new System.EventHandler(this.txttiporegimencod_TextChanged);
            this.txttiporegimencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttiporegimencod_KeyDown);
            // 
            // frm_ventas_regimen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(704, 131);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_ventas_regimen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Regimen";
            this.Load += new System.EventHandler(this.frm_ventas_regimen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtporcentaje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregimennumero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregimenserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadetrac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregtipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtregtipodoccod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfecto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimencod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        public DevExpress.XtraEditors.TextEdit txtmonto;
        public DevExpress.XtraEditors.TextEdit txtporcentaje;
        public DevExpress.XtraEditors.TextEdit txtregimennumero;
        public DevExpress.XtraEditors.TextEdit txtregimenserie;
        public DevExpress.XtraEditors.TextEdit txtfechadetrac;
        public DevExpress.XtraEditors.TextEdit txtregtipodocdesc;
        public DevExpress.XtraEditors.TextEdit txtregtipodoccod;
        public DevExpress.XtraEditors.TextEdit txttiporegimendesc;
        public DevExpress.XtraEditors.TextEdit txttiporegimencod;
        public DevExpress.XtraEditors.CheckEdit chkAfecto;
    }
}
