﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Ventas
{
    public partial class frm_ventas_administrativo : Contable.frm_fuente
    {
        public frm_ventas_administrativo()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;

        public List<Entidad_Movimiento_Cab> Detalles_ADM = new List<Entidad_Movimiento_Cab>();

        int Adm_Item;

        public void LimpiarDet()
        {
            txtccgcod.ResetText();
            txtccgdesc.ResetText();
            txtimporte.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtalmacencod.ResetText();
            txtalmacendesc.ResetText();
            txtvalorunit.ResetText();
            txtcantidad.ResetText();
        }

        public void BloquearDetalles()
        {
            txtccgcod.Enabled = false;
            //txttipo.Enabled = false;
            txtimporte.Enabled = false;
            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtalmacencod.Enabled = false;
            txtvalorunit.Enabled = false;
            txtcantidad.Enabled = false;

        }
        public void HabilitarDetalles()
        {

            txtccgcod.Enabled = true;
            //txttipo.Enabled = true;
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtimporte.Enabled = true;
            txtalmacencod.Enabled = true;
            txtvalorunit.Enabled = true;
            txtcantidad.Enabled = true;

            txtccgcod.Focus();
        }

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                Adm_Item = Detalles_ADM.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }
        private void txtccgcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtccgcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtccgcod.Text.Substring(txtccgcod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_centro_costo_gasto_busqueda f = new _1_Busquedas_Generales.frm_centro_costo_gasto_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Centro_Costo_Gasto Entidad = new Entidad_Centro_Costo_Gasto();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtccgcod.Text = Entidad.Id_Centro_cg;
                                txtccgdesc.Text = Entidad.Ccg_Descripcion;
                                txtccgcod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtccgcod.Text) & string.IsNullOrEmpty(txtccgdesc.Text))
                    {
                        BuscarCentroCostogasto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCentroCostogasto()
        {
            try
            {
                txtccgcod.Text = Accion.Formato(txtccgcod.Text, 3);

                Logica_Centro_Costo_Gasto log = new Logica_Centro_Costo_Gasto();

                List<Entidad_Centro_Costo_Gasto> Generales = new List<Entidad_Centro_Costo_Gasto>();

                Generales = log.Listar(new Entidad_Centro_Costo_Gasto
                {
                    Id_Empresa = Actual_Conexion.EmpresaNombre,
                    Id_Centro_cg = txtccgcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Centro_Costo_Gasto T in Generales)
                    {
                        if ((T.Id_Centro_cg).ToString().Trim().ToUpper() == txtccgcod.Text.Trim().ToUpper())
                        {
                            txtccgcod.Text = (T.Id_Centro_cg).ToString().Trim();
                            txtccgdesc.Text = T.Ccg_Descripcion;
                            txtccgcod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ninguna descripcion con este codigo");
                    txtccgcod.EnterMoveNextControl = false;
                    txtccgcod.ResetText();
                    txtccgcod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipobsacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = ""
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;
                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ninguna descripcion con este codigo");
                    txttipobsacod.ResetText();
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.Focus();

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        using ( frm_catalogo_busqueda f = new  frm_catalogo_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;
                                txtproductocod.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {

                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;
                            txtproductocod.EnterMoveNextControl = true;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun producto con este codigo");
                    txtproductocod.ResetText();
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            if (VerificarNuevoDet())
            {
                EstadoDetalle = Estados.Nuevo;

                HabilitarDetalles();

                txtvalorunit.Text = "0.00";
                txtimporte.Text = "0.00";
                txtcantidad.Text = "0.00";

            }
        }

        public bool VerificarNuevoDet()
        {
            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}


            return true;
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles_ADM.Count > 0)
                {

                    Detalles_ADM.RemoveAt(gridView2.GetFocusedDataSourceRowIndex());

                    Dgvdetalles.DataSource = null;
                    if (Detalles_ADM.Count > 0)
                    {
                        Dgvdetalles.DataSource = Detalles_ADM;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Movimiento_Cab Det in Detalles_ADM)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {


            try
            {
                if (VerificarDetalle())
                {
                    Entidad_Movimiento_Cab ItemDetalle = new Entidad_Movimiento_Cab();

                    ItemDetalle.Adm_Item = Adm_Item;


                    ItemDetalle.Adm_Centro_CG = txtccgcod.Text.Trim().ToString();
                    ItemDetalle.Adm_Centro_CG_Desc = txtccgdesc.Text.Trim().ToString();

                    if (txttipobsacod.Tag.ToString() == null)
                    {
                        ItemDetalle.Adm_Tipo_BSA = "";
                    }
                    else
                    {
                        ItemDetalle.Adm_Tipo_BSA = txttipobsacod.Tag.ToString();
                    }

                    ItemDetalle.Adm_Tipo_BSA_Interno = txttipobsacod.Text;
                    ItemDetalle.Adm_Tipo_BSA_Desc = txttipobsadesc.Text;

                    ItemDetalle.Adm_Catalogo = txtproductocod.Text.Trim();
                    ItemDetalle.Adm_Catalogo_Desc = txtproductodesc.Text;


                    ItemDetalle.Adm_Almacen = txtalmacencod.Text;
                    ItemDetalle.Adm_Almacen_desc = txtalmacendesc.Text;

                    ItemDetalle.Adm_Cantidad = Convert.ToDecimal(txtcantidad.Text);
                    ItemDetalle.Adm_Valor_Unit = Convert.ToDecimal(txtvalorunit.Text);

                    ItemDetalle.Adm_Total = Convert.ToDecimal(txtimporte.Text);



                    if (EstadoDetalle == Estados.Nuevo)
                    {
                        Detalles_ADM.Add(ItemDetalle);
                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;
                    }
                    else if (EstadoDetalle == Estados.Modificar)
                    {

                        Detalles_ADM[Convert.ToInt32(Adm_Item) - 1] = ItemDetalle;
                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;

                    }

                    btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void UpdateGrilla()
        {
            Dgvdetalles.DataSource = null;
            if (Detalles_ADM.Count > 0)
            {
                Dgvdetalles.DataSource = Detalles_ADM;
            }
        }

        public bool VerificarDetalle()
        {
            if (Convert.ToDouble(txtimporte.Text) <= 0)
            {
                Accion.Advertencia("Debe ingresar un importe mayor a cero");
                txtimporte.Focus();
                return false;
            }
            return true;
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void frm_ventas_administrativo_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                BloquearDetalles();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();
            }
        }


        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];

                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = Enti.Id_Libro;
                    Voucher = Enti.Id_Voucher;

                    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();
                    Dgvdetalles.DataSource = null;
                    Detalles_ADM = log_det.Listar_Det_Administrativo(Enti);

                    if (Detalles_ADM.Count > 0)
                    {
                        Dgvdetalles.DataSource = Detalles_ADM;
                    }



                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txtalmacencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacencod.Text.Substring(txtalmacencod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacencod.Text = Entidad.Id_Almacen;
                                txtalmacendesc.Text = Entidad.Alm_Descrcipcion;
                                txtalmacencod.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacencod.Text) & string.IsNullOrEmpty(txtalmacendesc.Text))
                    {
                        BuscarAlmacen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarAlmacen()
        {
            try
            {
                txtalmacencod.Text = Accion.Formato(txtalmacencod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen = txtalmacencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacencod.Text.Trim().ToUpper())
                        {
                            txtalmacencod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion;
                            txtalmacencod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se eoncontro ningun almacen con este codigo");
                    txtalmacencod.EnterMoveNextControl = false;
                    txtalmacencod.ResetText();
                    txtalmacencod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtvalorunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtvalorunit.Text.Trim()))
            {
                try
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        decimal Importe;
                        Importe = Convert.ToDecimal(txtvalorunit.Text) * Convert.ToDecimal(txtcantidad.Text);
                        txtimporte.Text = Convert.ToString(Importe);
                    }

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
