﻿namespace Contable.Procesos.Ventas
{
    partial class frm_ventas_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl6 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl7 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl9 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl10 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl11 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl12 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl13 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl14 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl15 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl16 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl17 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl18 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl19 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl20 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl21 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl22 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl23 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl24 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl25 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl26 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl27 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl28 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl29 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl30 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl31 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl32 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl33 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl34 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl35 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl36 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl37 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl38 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl39 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl40 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl41 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl42 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl43 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl44 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl45 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl46 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl47 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl48 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl49 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl50 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl51 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl52 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl53 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl54 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl55 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl56 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl57 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl58 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl59 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl60 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl61 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl62 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl63 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl64 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl65 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl66 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl67 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl68 = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtfechavencimiento = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtanalisisdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtidanalisis = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtigvporcentaje = new DevExpress.XtraEditors.TextEdit();
            this.chkotrostri = new DevExpress.XtraEditors.CheckEdit();
            this.chkisc = new DevExpress.XtraEditors.CheckEdit();
            this.txtimportetotal = new DevExpress.XtraEditors.TextEdit();
            this.txtotrostribuimporte = new DevExpress.XtraEditors.TextEdit();
            this.txtigv = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteisc = new DevExpress.XtraEditors.TextEdit();
            this.txtinafecta = new DevExpress.XtraEditors.TextEdit();
            this.txtexonerada = new DevExpress.XtraEditors.TextEdit();
            this.txtvalorfactexport = new DevExpress.XtraEditors.TextEdit();
            this.txtbaseimponible = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.btnbuscardocref = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDMN = new System.Windows.Forms.TextBox();
            this.btnnuevodetcon = new DevExpress.XtraEditors.SimpleButton();
            this.TxtHME = new System.Windows.Forms.TextBox();
            this.TxtHMN = new System.Windows.Forms.TextBox();
            this.btneditardetcon = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDME = new System.Windows.Forms.TextBox();
            this.btnquitardetcon = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnanadirdetcon = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatosContable = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtimportecontable = new DevExpress.XtraEditors.TextEdit();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.txttipoopedesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoopecod = new DevExpress.XtraEditors.TextEdit();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.btnregimen = new DevExpress.XtraEditors.SimpleButton();
            this.btndetadm = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtinafecta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexonerada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorfactexport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatosContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportecontable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopedesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopecod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1023, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 556);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1023, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 524);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 32);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(0, 556);
            this.barDockControl3.Manager = this.barManager1;
            this.barDockControl3.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 32);
            this.barDockControl4.Manager = this.barManager1;
            this.barDockControl4.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl5.Location = new System.Drawing.Point(0, 32);
            this.barDockControl5.Manager = this.barManager1;
            this.barDockControl5.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl6
            // 
            this.barDockControl6.CausesValidation = false;
            this.barDockControl6.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl6.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl6.Manager = this.barManager1;
            this.barDockControl6.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl7
            // 
            this.barDockControl7.CausesValidation = false;
            this.barDockControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl7.Location = new System.Drawing.Point(0, 556);
            this.barDockControl7.Manager = this.barManager1;
            this.barDockControl7.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl8.Location = new System.Drawing.Point(0, 32);
            this.barDockControl8.Manager = this.barManager1;
            this.barDockControl8.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl9
            // 
            this.barDockControl9.CausesValidation = false;
            this.barDockControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl9.Location = new System.Drawing.Point(0, 32);
            this.barDockControl9.Manager = this.barManager1;
            this.barDockControl9.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl10
            // 
            this.barDockControl10.CausesValidation = false;
            this.barDockControl10.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl10.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl10.Manager = this.barManager1;
            this.barDockControl10.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl11
            // 
            this.barDockControl11.CausesValidation = false;
            this.barDockControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl11.Location = new System.Drawing.Point(0, 556);
            this.barDockControl11.Manager = this.barManager1;
            this.barDockControl11.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl12
            // 
            this.barDockControl12.CausesValidation = false;
            this.barDockControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl12.Location = new System.Drawing.Point(0, 32);
            this.barDockControl12.Manager = this.barManager1;
            this.barDockControl12.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl13
            // 
            this.barDockControl13.CausesValidation = false;
            this.barDockControl13.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl13.Location = new System.Drawing.Point(0, 32);
            this.barDockControl13.Manager = this.barManager1;
            this.barDockControl13.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl14
            // 
            this.barDockControl14.CausesValidation = false;
            this.barDockControl14.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl14.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl14.Manager = this.barManager1;
            this.barDockControl14.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl15
            // 
            this.barDockControl15.CausesValidation = false;
            this.barDockControl15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl15.Location = new System.Drawing.Point(0, 556);
            this.barDockControl15.Manager = this.barManager1;
            this.barDockControl15.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl16
            // 
            this.barDockControl16.CausesValidation = false;
            this.barDockControl16.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl16.Location = new System.Drawing.Point(0, 32);
            this.barDockControl16.Manager = this.barManager1;
            this.barDockControl16.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl17
            // 
            this.barDockControl17.CausesValidation = false;
            this.barDockControl17.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl17.Location = new System.Drawing.Point(0, 32);
            this.barDockControl17.Manager = this.barManager1;
            this.barDockControl17.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl18
            // 
            this.barDockControl18.CausesValidation = false;
            this.barDockControl18.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl18.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl18.Manager = this.barManager1;
            this.barDockControl18.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl19
            // 
            this.barDockControl19.CausesValidation = false;
            this.barDockControl19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl19.Location = new System.Drawing.Point(0, 556);
            this.barDockControl19.Manager = this.barManager1;
            this.barDockControl19.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl20
            // 
            this.barDockControl20.CausesValidation = false;
            this.barDockControl20.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl20.Location = new System.Drawing.Point(0, 32);
            this.barDockControl20.Manager = this.barManager1;
            this.barDockControl20.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl21
            // 
            this.barDockControl21.CausesValidation = false;
            this.barDockControl21.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl21.Location = new System.Drawing.Point(0, 32);
            this.barDockControl21.Manager = this.barManager1;
            this.barDockControl21.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl22
            // 
            this.barDockControl22.CausesValidation = false;
            this.barDockControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl22.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl22.Manager = this.barManager1;
            this.barDockControl22.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl23
            // 
            this.barDockControl23.CausesValidation = false;
            this.barDockControl23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl23.Location = new System.Drawing.Point(0, 556);
            this.barDockControl23.Manager = this.barManager1;
            this.barDockControl23.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl24
            // 
            this.barDockControl24.CausesValidation = false;
            this.barDockControl24.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl24.Location = new System.Drawing.Point(0, 32);
            this.barDockControl24.Manager = this.barManager1;
            this.barDockControl24.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl25
            // 
            this.barDockControl25.CausesValidation = false;
            this.barDockControl25.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl25.Location = new System.Drawing.Point(0, 32);
            this.barDockControl25.Manager = this.barManager1;
            this.barDockControl25.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl26
            // 
            this.barDockControl26.CausesValidation = false;
            this.barDockControl26.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl26.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl26.Manager = this.barManager1;
            this.barDockControl26.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl27
            // 
            this.barDockControl27.CausesValidation = false;
            this.barDockControl27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl27.Location = new System.Drawing.Point(0, 556);
            this.barDockControl27.Manager = this.barManager1;
            this.barDockControl27.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl28
            // 
            this.barDockControl28.CausesValidation = false;
            this.barDockControl28.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl28.Location = new System.Drawing.Point(0, 32);
            this.barDockControl28.Manager = this.barManager1;
            this.barDockControl28.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl29
            // 
            this.barDockControl29.CausesValidation = false;
            this.barDockControl29.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl29.Location = new System.Drawing.Point(0, 32);
            this.barDockControl29.Manager = this.barManager1;
            this.barDockControl29.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl30
            // 
            this.barDockControl30.CausesValidation = false;
            this.barDockControl30.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl30.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl30.Manager = this.barManager1;
            this.barDockControl30.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl31
            // 
            this.barDockControl31.CausesValidation = false;
            this.barDockControl31.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl31.Location = new System.Drawing.Point(0, 556);
            this.barDockControl31.Manager = this.barManager1;
            this.barDockControl31.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl32
            // 
            this.barDockControl32.CausesValidation = false;
            this.barDockControl32.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl32.Location = new System.Drawing.Point(0, 32);
            this.barDockControl32.Manager = this.barManager1;
            this.barDockControl32.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl33
            // 
            this.barDockControl33.CausesValidation = false;
            this.barDockControl33.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl33.Location = new System.Drawing.Point(0, 32);
            this.barDockControl33.Manager = this.barManager1;
            this.barDockControl33.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl34
            // 
            this.barDockControl34.CausesValidation = false;
            this.barDockControl34.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl34.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl34.Manager = this.barManager1;
            this.barDockControl34.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl35
            // 
            this.barDockControl35.CausesValidation = false;
            this.barDockControl35.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl35.Location = new System.Drawing.Point(0, 556);
            this.barDockControl35.Manager = this.barManager1;
            this.barDockControl35.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl36
            // 
            this.barDockControl36.CausesValidation = false;
            this.barDockControl36.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl36.Location = new System.Drawing.Point(0, 32);
            this.barDockControl36.Manager = this.barManager1;
            this.barDockControl36.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl37
            // 
            this.barDockControl37.CausesValidation = false;
            this.barDockControl37.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl37.Location = new System.Drawing.Point(0, 32);
            this.barDockControl37.Manager = this.barManager1;
            this.barDockControl37.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl38
            // 
            this.barDockControl38.CausesValidation = false;
            this.barDockControl38.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl38.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl38.Manager = this.barManager1;
            this.barDockControl38.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl39
            // 
            this.barDockControl39.CausesValidation = false;
            this.barDockControl39.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl39.Location = new System.Drawing.Point(0, 556);
            this.barDockControl39.Manager = this.barManager1;
            this.barDockControl39.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl40
            // 
            this.barDockControl40.CausesValidation = false;
            this.barDockControl40.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl40.Location = new System.Drawing.Point(0, 32);
            this.barDockControl40.Manager = this.barManager1;
            this.barDockControl40.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl41
            // 
            this.barDockControl41.CausesValidation = false;
            this.barDockControl41.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl41.Location = new System.Drawing.Point(0, 32);
            this.barDockControl41.Manager = this.barManager1;
            this.barDockControl41.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl42
            // 
            this.barDockControl42.CausesValidation = false;
            this.barDockControl42.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl42.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl42.Manager = this.barManager1;
            this.barDockControl42.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl43
            // 
            this.barDockControl43.CausesValidation = false;
            this.barDockControl43.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl43.Location = new System.Drawing.Point(0, 556);
            this.barDockControl43.Manager = this.barManager1;
            this.barDockControl43.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl44
            // 
            this.barDockControl44.CausesValidation = false;
            this.barDockControl44.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl44.Location = new System.Drawing.Point(0, 32);
            this.barDockControl44.Manager = this.barManager1;
            this.barDockControl44.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl45
            // 
            this.barDockControl45.CausesValidation = false;
            this.barDockControl45.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl45.Location = new System.Drawing.Point(0, 32);
            this.barDockControl45.Manager = this.barManager1;
            this.barDockControl45.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl46
            // 
            this.barDockControl46.CausesValidation = false;
            this.barDockControl46.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl46.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl46.Manager = this.barManager1;
            this.barDockControl46.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl47
            // 
            this.barDockControl47.CausesValidation = false;
            this.barDockControl47.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl47.Location = new System.Drawing.Point(0, 556);
            this.barDockControl47.Manager = this.barManager1;
            this.barDockControl47.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl48
            // 
            this.barDockControl48.CausesValidation = false;
            this.barDockControl48.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl48.Location = new System.Drawing.Point(0, 32);
            this.barDockControl48.Manager = this.barManager1;
            this.barDockControl48.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl49
            // 
            this.barDockControl49.CausesValidation = false;
            this.barDockControl49.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl49.Location = new System.Drawing.Point(0, 32);
            this.barDockControl49.Manager = this.barManager1;
            this.barDockControl49.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl50
            // 
            this.barDockControl50.CausesValidation = false;
            this.barDockControl50.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl50.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl50.Manager = this.barManager1;
            this.barDockControl50.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl51
            // 
            this.barDockControl51.CausesValidation = false;
            this.barDockControl51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl51.Location = new System.Drawing.Point(0, 556);
            this.barDockControl51.Manager = this.barManager1;
            this.barDockControl51.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl52
            // 
            this.barDockControl52.CausesValidation = false;
            this.barDockControl52.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl52.Location = new System.Drawing.Point(0, 32);
            this.barDockControl52.Manager = this.barManager1;
            this.barDockControl52.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl53
            // 
            this.barDockControl53.CausesValidation = false;
            this.barDockControl53.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl53.Location = new System.Drawing.Point(0, 32);
            this.barDockControl53.Manager = this.barManager1;
            this.barDockControl53.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl54
            // 
            this.barDockControl54.CausesValidation = false;
            this.barDockControl54.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl54.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl54.Manager = this.barManager1;
            this.barDockControl54.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl55
            // 
            this.barDockControl55.CausesValidation = false;
            this.barDockControl55.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl55.Location = new System.Drawing.Point(0, 556);
            this.barDockControl55.Manager = this.barManager1;
            this.barDockControl55.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl56
            // 
            this.barDockControl56.CausesValidation = false;
            this.barDockControl56.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl56.Location = new System.Drawing.Point(0, 32);
            this.barDockControl56.Manager = this.barManager1;
            this.barDockControl56.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl57
            // 
            this.barDockControl57.CausesValidation = false;
            this.barDockControl57.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl57.Location = new System.Drawing.Point(0, 32);
            this.barDockControl57.Manager = this.barManager1;
            this.barDockControl57.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl58
            // 
            this.barDockControl58.CausesValidation = false;
            this.barDockControl58.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl58.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl58.Manager = this.barManager1;
            this.barDockControl58.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl59
            // 
            this.barDockControl59.CausesValidation = false;
            this.barDockControl59.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl59.Location = new System.Drawing.Point(0, 556);
            this.barDockControl59.Manager = this.barManager1;
            this.barDockControl59.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl60
            // 
            this.barDockControl60.CausesValidation = false;
            this.barDockControl60.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl60.Location = new System.Drawing.Point(0, 32);
            this.barDockControl60.Manager = this.barManager1;
            this.barDockControl60.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl61
            // 
            this.barDockControl61.CausesValidation = false;
            this.barDockControl61.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl61.Location = new System.Drawing.Point(0, 32);
            this.barDockControl61.Manager = this.barManager1;
            this.barDockControl61.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl62
            // 
            this.barDockControl62.CausesValidation = false;
            this.barDockControl62.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl62.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl62.Manager = this.barManager1;
            this.barDockControl62.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl63
            // 
            this.barDockControl63.CausesValidation = false;
            this.barDockControl63.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl63.Location = new System.Drawing.Point(0, 556);
            this.barDockControl63.Manager = this.barManager1;
            this.barDockControl63.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl64
            // 
            this.barDockControl64.CausesValidation = false;
            this.barDockControl64.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl64.Location = new System.Drawing.Point(0, 32);
            this.barDockControl64.Manager = this.barManager1;
            this.barDockControl64.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl65
            // 
            this.barDockControl65.CausesValidation = false;
            this.barDockControl65.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl65.Location = new System.Drawing.Point(0, 32);
            this.barDockControl65.Manager = this.barManager1;
            this.barDockControl65.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl66
            // 
            this.barDockControl66.CausesValidation = false;
            this.barDockControl66.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl66.Location = new System.Drawing.Point(1023, 32);
            this.barDockControl66.Manager = this.barManager1;
            this.barDockControl66.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControl67
            // 
            this.barDockControl67.CausesValidation = false;
            this.barDockControl67.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl67.Location = new System.Drawing.Point(0, 556);
            this.barDockControl67.Manager = this.barManager1;
            this.barDockControl67.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControl68
            // 
            this.barDockControl68.CausesValidation = false;
            this.barDockControl68.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl68.Location = new System.Drawing.Point(0, 32);
            this.barDockControl68.Manager = this.barManager1;
            this.barDockControl68.Size = new System.Drawing.Size(1023, 0);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtfechavencimiento);
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.txtanalisisdesc);
            this.groupControl1.Controls.Add(this.txtidanalisis);
            this.groupControl1.Controls.Add(this.labelControl25);
            this.groupControl1.Controls.Add(this.txttipocambiovalor);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtentidad);
            this.groupControl1.Controls.Add(this.txttipocambiodesc);
            this.groupControl1.Controls.Add(this.txttipocambiocod);
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.txtcondiciondesc);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txtcondicioncod);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtrucdni);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(717, 165);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // txtfechavencimiento
            // 
            this.txtfechavencimiento.EnterMoveNextControl = true;
            this.txtfechavencimiento.Location = new System.Drawing.Point(106, 95);
            this.txtfechavencimiento.MenuManager = this.barManager1;
            this.txtfechavencimiento.Name = "txtfechavencimiento";
            this.txtfechavencimiento.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechavencimiento.Properties.Mask.BeepOnError = true;
            this.txtfechavencimiento.Properties.Mask.EditMask = "d";
            this.txtfechavencimiento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechavencimiento.Properties.Mask.SaveLiteral = false;
            this.txtfechavencimiento.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechavencimiento.Properties.MaxLength = 10;
            this.txtfechavencimiento.Size = new System.Drawing.Size(108, 20);
            this.txtfechavencimiento.TabIndex = 14;
            this.txtfechavencimiento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfechavencimiento_KeyDown);
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(7, 99);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(93, 13);
            this.labelControl35.TabIndex = 13;
            this.labelControl35.Text = "Fecha Vencimiento:";
            // 
            // txtanalisisdesc
            // 
            this.txtanalisisdesc.Enabled = false;
            this.txtanalisisdesc.Location = new System.Drawing.Point(171, 138);
            this.txtanalisisdesc.MenuManager = this.barManager1;
            this.txtanalisisdesc.Name = "txtanalisisdesc";
            this.txtanalisisdesc.Size = new System.Drawing.Size(308, 20);
            this.txtanalisisdesc.TabIndex = 27;
            // 
            // txtidanalisis
            // 
            this.txtidanalisis.Location = new System.Drawing.Point(124, 138);
            this.txtidanalisis.MenuManager = this.barManager1;
            this.txtidanalisis.Name = "txtidanalisis";
            this.txtidanalisis.Size = new System.Drawing.Size(44, 20);
            this.txtidanalisis.TabIndex = 26;
            this.txtidanalisis.EditValueChanged += new System.EventHandler(this.txtidanalisis_EditValueChanged);
            this.txtidanalisis.TextChanged += new System.EventHandler(this.txtidanalisis_TextChanged);
            this.txtidanalisis.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtidanalisis_KeyDown);
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(7, 141);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(111, 13);
            this.labelControl25.TabIndex = 25;
            this.labelControl25.Text = "Analisis de la operacion";
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(632, 95);
            this.txttipocambiovalor.MenuManager = this.barManager1;
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Size = new System.Drawing.Size(65, 20);
            this.txttipocambiovalor.TabIndex = 21;
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(101, 73);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(192, 20);
            this.txttipodocdesc.TabIndex = 6;
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(608, 73);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(89, 20);
            this.txtfechadoc.TabIndex = 12;
            this.txtfechadoc.TextChanged += new System.EventHandler(this.txtfechadoc_TextChanged);
            this.txtfechadoc.Leave += new System.EventHandler(this.txtfechadoc_Leave);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(545, 76);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(58, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Fecha Doc.:";
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(338, 73);
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(201, 20);
            this.txtserie.TabIndex = 8;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(298, 76);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Serie:";
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(61, 73);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 5;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(21, 76);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "T.Doc.:";
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(146, 116);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtentidad.Size = new System.Drawing.Size(550, 20);
            this.txtentidad.TabIndex = 24;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(523, 95);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 20;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(487, 95);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 19;
            this.txttipocambiocod.TextChanged += new System.EventHandler(this.txttipocambiocod_TextChanged);
            this.txttipocambiocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocod_KeyDown);
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(306, 95);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedadesc.Size = new System.Drawing.Size(149, 20);
            this.txtmonedadesc.TabIndex = 17;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(266, 95);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedacod.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacod.TabIndex = 16;
            this.txtmonedacod.TextChanged += new System.EventHandler(this.txtmonedacod_TextChanged);
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(581, 137);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondiciondesc.Size = new System.Drawing.Size(115, 20);
            this.txtcondiciondesc.TabIndex = 30;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(462, 96);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(21, 13);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "T.C.";
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.Enabled = false;
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(542, 137);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(38, 20);
            this.txtcondicioncod.TabIndex = 29;
            this.txtcondicioncod.TextChanged += new System.EventHandler(this.txtcondicioncod_TextChanged);
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(220, 98);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "Moneda:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(485, 141);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "Condicion:";
            // 
            // txtrucdni
            // 
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(61, 116);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 23;
            this.txtrucdni.TextChanged += new System.EventHandler(this.txtrucdni_TextChanged);
            this.txtrucdni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdni_KeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(21, 119);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(37, 13);
            this.labelControl7.TabIndex = 22;
            this.labelControl7.Text = "R.U.C.:";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(61, 50);
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(636, 20);
            this.txtglosa.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(21, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Glosa:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(61, 27);
            this.txtlibro.MenuManager = this.barManager1;
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlibro.Size = new System.Drawing.Size(636, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(24, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtigvporcentaje);
            this.groupBox1.Controls.Add(this.chkotrostri);
            this.groupBox1.Controls.Add(this.chkisc);
            this.groupBox1.Controls.Add(this.txtimportetotal);
            this.groupBox1.Controls.Add(this.txtotrostribuimporte);
            this.groupBox1.Controls.Add(this.txtigv);
            this.groupBox1.Controls.Add(this.txtimporteisc);
            this.groupBox1.Controls.Add(this.txtinafecta);
            this.groupBox1.Controls.Add(this.txtexonerada);
            this.groupBox1.Controls.Add(this.txtvalorfactexport);
            this.groupBox1.Controls.Add(this.txtbaseimponible);
            this.groupBox1.Controls.Add(this.labelControl28);
            this.groupBox1.Controls.Add(this.labelControl27);
            this.groupBox1.Controls.Add(this.labelControl24);
            this.groupBox1.Controls.Add(this.labelControl23);
            this.groupBox1.Controls.Add(this.labelControl22);
            this.groupBox1.Controls.Add(this.labelControl21);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(721, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 185);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resumen";
            // 
            // txtigvporcentaje
            // 
            this.txtigvporcentaje.EditValue = "0.00";
            this.txtigvporcentaje.Location = new System.Drawing.Point(55, 119);
            this.txtigvporcentaje.MenuManager = this.barManager1;
            this.txtigvporcentaje.Name = "txtigvporcentaje";
            this.txtigvporcentaje.Size = new System.Drawing.Size(39, 20);
            this.txtigvporcentaje.TabIndex = 18;
            // 
            // chkotrostri
            // 
            this.chkotrostri.Location = new System.Drawing.Point(9, 141);
            this.chkotrostri.MenuManager = this.barManager1;
            this.chkotrostri.Name = "chkotrostri";
            this.chkotrostri.Properties.Caption = "Otros Tributos";
            this.chkotrostri.Size = new System.Drawing.Size(92, 20);
            this.chkotrostri.TabIndex = 17;
            // 
            // chkisc
            // 
            this.chkisc.Location = new System.Drawing.Point(9, 99);
            this.chkisc.MenuManager = this.barManager1;
            this.chkisc.Name = "chkisc";
            this.chkisc.Properties.Caption = "I.S.C.";
            this.chkisc.Size = new System.Drawing.Size(75, 20);
            this.chkisc.TabIndex = 16;
            // 
            // txtimportetotal
            // 
            this.txtimportetotal.EditValue = "0.00";
            this.txtimportetotal.Location = new System.Drawing.Point(100, 161);
            this.txtimportetotal.MenuManager = this.barManager1;
            this.txtimportetotal.Name = "txtimportetotal";
            this.txtimportetotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimportetotal.Properties.Appearance.Options.UseFont = true;
            this.txtimportetotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimportetotal.Size = new System.Drawing.Size(87, 20);
            this.txtimportetotal.TabIndex = 15;
            // 
            // txtotrostribuimporte
            // 
            this.txtotrostribuimporte.EditValue = "0.00";
            this.txtotrostribuimporte.Location = new System.Drawing.Point(100, 140);
            this.txtotrostribuimporte.MenuManager = this.barManager1;
            this.txtotrostribuimporte.Name = "txtotrostribuimporte";
            this.txtotrostribuimporte.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtotrostribuimporte.Properties.Appearance.Options.UseFont = true;
            this.txtotrostribuimporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtotrostribuimporte.Size = new System.Drawing.Size(87, 20);
            this.txtotrostribuimporte.TabIndex = 14;
            // 
            // txtigv
            // 
            this.txtigv.EditValue = "0.00";
            this.txtigv.Location = new System.Drawing.Point(100, 119);
            this.txtigv.MenuManager = this.barManager1;
            this.txtigv.Name = "txtigv";
            this.txtigv.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtigv.Properties.Appearance.Options.UseFont = true;
            this.txtigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtigv.Size = new System.Drawing.Size(87, 20);
            this.txtigv.TabIndex = 13;
            // 
            // txtimporteisc
            // 
            this.txtimporteisc.EditValue = "0.00";
            this.txtimporteisc.Location = new System.Drawing.Point(100, 97);
            this.txtimporteisc.MenuManager = this.barManager1;
            this.txtimporteisc.Name = "txtimporteisc";
            this.txtimporteisc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimporteisc.Properties.Appearance.Options.UseFont = true;
            this.txtimporteisc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimporteisc.Size = new System.Drawing.Size(87, 20);
            this.txtimporteisc.TabIndex = 12;
            // 
            // txtinafecta
            // 
            this.txtinafecta.EditValue = "0.00";
            this.txtinafecta.Location = new System.Drawing.Point(100, 76);
            this.txtinafecta.MenuManager = this.barManager1;
            this.txtinafecta.Name = "txtinafecta";
            this.txtinafecta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtinafecta.Properties.Appearance.Options.UseFont = true;
            this.txtinafecta.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtinafecta.Size = new System.Drawing.Size(87, 20);
            this.txtinafecta.TabIndex = 11;
            // 
            // txtexonerada
            // 
            this.txtexonerada.EditValue = "0.00";
            this.txtexonerada.Location = new System.Drawing.Point(100, 55);
            this.txtexonerada.MenuManager = this.barManager1;
            this.txtexonerada.Name = "txtexonerada";
            this.txtexonerada.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtexonerada.Properties.Appearance.Options.UseFont = true;
            this.txtexonerada.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtexonerada.Size = new System.Drawing.Size(87, 20);
            this.txtexonerada.TabIndex = 10;
            // 
            // txtvalorfactexport
            // 
            this.txtvalorfactexport.EditValue = "0.00";
            this.txtvalorfactexport.Location = new System.Drawing.Point(100, 34);
            this.txtvalorfactexport.MenuManager = this.barManager1;
            this.txtvalorfactexport.Name = "txtvalorfactexport";
            this.txtvalorfactexport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtvalorfactexport.Properties.Appearance.Options.UseFont = true;
            this.txtvalorfactexport.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtvalorfactexport.Size = new System.Drawing.Size(87, 20);
            this.txtvalorfactexport.TabIndex = 9;
            // 
            // txtbaseimponible
            // 
            this.txtbaseimponible.EditValue = "0.00";
            this.txtbaseimponible.Location = new System.Drawing.Point(100, 13);
            this.txtbaseimponible.MenuManager = this.barManager1;
            this.txtbaseimponible.Name = "txtbaseimponible";
            this.txtbaseimponible.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtbaseimponible.Properties.Appearance.Options.UseFont = true;
            this.txtbaseimponible.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtbaseimponible.Size = new System.Drawing.Size(87, 20);
            this.txtbaseimponible.TabIndex = 8;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(9, 122);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(29, 13);
            this.labelControl28.TabIndex = 7;
            this.labelControl28.Text = "I.G.V.";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(9, 164);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(69, 13);
            this.labelControl27.TabIndex = 6;
            this.labelControl27.Text = "Importe Total:";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(9, 78);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(41, 13);
            this.labelControl24.TabIndex = 3;
            this.labelControl24.Text = "Inafecta";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(9, 56);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(52, 13);
            this.labelControl23.TabIndex = 2;
            this.labelControl23.Text = "Exonerada";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(9, 37);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(85, 13);
            this.labelControl22.TabIndex = 1;
            this.labelControl22.Text = "Val. Fact. Export.";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(9, 16);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(72, 13);
            this.labelControl21.TabIndex = 0;
            this.labelControl21.Text = "Base Imponible";
            // 
            // btnbuscardocref
            // 
            this.btnbuscardocref.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbuscardocref.Appearance.Options.UseFont = true;
            this.btnbuscardocref.Location = new System.Drawing.Point(919, 66);
            this.btnbuscardocref.Name = "btnbuscardocref";
            this.btnbuscardocref.Size = new System.Drawing.Size(100, 37);
            this.btnbuscardocref.TabIndex = 75;
            this.btnbuscardocref.Text = "Vincular Doc. Ref.";
            this.btnbuscardocref.Click += new System.EventHandler(this.btnbuscardocref_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 38);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(719, 190);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(717, 165);
            this.xtraTabPage1.Text = "Datos Generales";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Location = new System.Drawing.Point(1, 234);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage4;
            this.xtraTabControl2.Size = new System.Drawing.Size(1004, 325);
            this.xtraTabControl2.TabIndex = 1;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage4});
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.groupControl3);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1002, 300);
            this.xtraTabPage4.Text = "Contable";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.simpleButton1);
            this.groupControl3.Controls.Add(this.TxtDMN);
            this.groupControl3.Controls.Add(this.btnnuevodetcon);
            this.groupControl3.Controls.Add(this.TxtHME);
            this.groupControl3.Controls.Add(this.TxtHMN);
            this.groupControl3.Controls.Add(this.btneditardetcon);
            this.groupControl3.Controls.Add(this.TxtDME);
            this.groupControl3.Controls.Add(this.btnquitardetcon);
            this.groupControl3.Controls.Add(this.label1);
            this.groupControl3.Controls.Add(this.btnanadirdetcon);
            this.groupControl3.Controls.Add(this.dgvdatosContable);
            this.groupControl3.Controls.Add(this.labelControl15);
            this.groupControl3.Controls.Add(this.txtimportecontable);
            this.groupControl3.Controls.Add(this.txttipodesc);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.txttipo);
            this.groupControl3.Controls.Add(this.txttipoopedesc);
            this.groupControl3.Controls.Add(this.labelControl12);
            this.groupControl3.Controls.Add(this.txttipoopecod);
            this.groupControl3.Controls.Add(this.txtcuentadesc);
            this.groupControl3.Controls.Add(this.txtcuenta);
            this.groupControl3.Controls.Add(this.labelControl11);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1002, 300);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "Detalles Contables";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = global::Contable.Properties.Resources.Enter_Pin_26px;
            this.simpleButton1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.simpleButton1.Location = new System.Drawing.Point(935, 23);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(58, 80);
            this.simpleButton1.TabIndex = 71;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // TxtDMN
            // 
            this.TxtDMN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtDMN.BackColor = System.Drawing.Color.Khaki;
            this.TxtDMN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtDMN.Location = new System.Drawing.Point(423, 276);
            this.TxtDMN.Name = "TxtDMN";
            this.TxtDMN.ReadOnly = true;
            this.TxtDMN.Size = new System.Drawing.Size(111, 21);
            this.TxtDMN.TabIndex = 69;
            this.TxtDMN.TabStop = false;
            this.TxtDMN.Text = "0";
            this.TxtDMN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnnuevodetcon
            // 
            this.btnnuevodetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevodetcon.Appearance.Options.UseFont = true;
            this.btnnuevodetcon.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.btnnuevodetcon.Location = new System.Drawing.Point(751, 25);
            this.btnnuevodetcon.Name = "btnnuevodetcon";
            this.btnnuevodetcon.Size = new System.Drawing.Size(87, 37);
            this.btnnuevodetcon.TabIndex = 1;
            this.btnnuevodetcon.Text = "Nuevo";
            this.btnnuevodetcon.Click += new System.EventHandler(this.btnnuevodetcon_Click);
            // 
            // TxtHME
            // 
            this.TxtHME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtHME.BackColor = System.Drawing.Color.PapayaWhip;
            this.TxtHME.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHME.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtHME.Location = new System.Drawing.Point(746, 276);
            this.TxtHME.Name = "TxtHME";
            this.TxtHME.ReadOnly = true;
            this.TxtHME.Size = new System.Drawing.Size(97, 21);
            this.TxtHME.TabIndex = 68;
            this.TxtHME.TabStop = false;
            this.TxtHME.Text = "0";
            this.TxtHME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtHMN
            // 
            this.TxtHMN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtHMN.BackColor = System.Drawing.Color.Khaki;
            this.TxtHMN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtHMN.Location = new System.Drawing.Point(535, 276);
            this.TxtHMN.Name = "TxtHMN";
            this.TxtHMN.ReadOnly = true;
            this.TxtHMN.Size = new System.Drawing.Size(102, 21);
            this.TxtHMN.TabIndex = 67;
            this.TxtHMN.TabStop = false;
            this.TxtHMN.Text = "0";
            this.TxtHMN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btneditardetcon
            // 
            this.btneditardetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneditardetcon.Appearance.Options.UseFont = true;
            this.btneditardetcon.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.btneditardetcon.Location = new System.Drawing.Point(751, 65);
            this.btneditardetcon.Name = "btneditardetcon";
            this.btneditardetcon.Size = new System.Drawing.Size(87, 37);
            this.btneditardetcon.TabIndex = 30;
            this.btneditardetcon.Text = "Editar";
            this.btneditardetcon.Click += new System.EventHandler(this.btneditardetcon_Click);
            // 
            // TxtDME
            // 
            this.TxtDME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtDME.BackColor = System.Drawing.Color.PapayaWhip;
            this.TxtDME.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDME.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtDME.Location = new System.Drawing.Point(639, 276);
            this.TxtDME.Name = "TxtDME";
            this.TxtDME.ReadOnly = true;
            this.TxtDME.Size = new System.Drawing.Size(104, 21);
            this.TxtDME.TabIndex = 66;
            this.TxtDME.TabStop = false;
            this.TxtDME.Text = "0";
            this.TxtDME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnquitardetcon
            // 
            this.btnquitardetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnquitardetcon.Appearance.Options.UseFont = true;
            this.btnquitardetcon.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.btnquitardetcon.Location = new System.Drawing.Point(844, 24);
            this.btnquitardetcon.Name = "btnquitardetcon";
            this.btnquitardetcon.Size = new System.Drawing.Size(87, 37);
            this.btnquitardetcon.TabIndex = 31;
            this.btnquitardetcon.Text = "Quitar";
            this.btnquitardetcon.Click += new System.EventHandler(this.btnquitardetcon_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(371, 277);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 70;
            this.label1.Text = "Totales";
            // 
            // btnanadirdetcon
            // 
            this.btnanadirdetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnanadirdetcon.Appearance.Options.UseFont = true;
            this.btnanadirdetcon.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.btnanadirdetcon.Location = new System.Drawing.Point(844, 65);
            this.btnanadirdetcon.Name = "btnanadirdetcon";
            this.btnanadirdetcon.Size = new System.Drawing.Size(87, 37);
            this.btnanadirdetcon.TabIndex = 13;
            this.btnanadirdetcon.Text = "Añadir";
            this.btnanadirdetcon.Click += new System.EventHandler(this.btnanadirdetcon_Click);
            // 
            // dgvdatosContable
            // 
            this.dgvdatosContable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvdatosContable.Location = new System.Drawing.Point(5, 109);
            this.dgvdatosContable.MainView = this.gridView1;
            this.dgvdatosContable.MenuManager = this.barManager1;
            this.dgvdatosContable.Name = "dgvdatosContable";
            this.dgvdatosContable.Size = new System.Drawing.Size(988, 161);
            this.dgvdatosContable.TabIndex = 28;
            this.dgvdatosContable.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridView1.GridControl = this.dgvdatosContable;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.Caption = "Cuenta";
            this.gridColumn8.FieldName = "Ctb_Cuenta";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 110;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn9.Caption = "Cuenta Descripcion";
            this.gridColumn9.FieldName = "Ctb_Cuenta_Desc";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 296;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn10.Caption = "Debe Nac.";
            this.gridColumn10.DisplayFormat.FormatString = "n2";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "Ctb_Importe_Debe";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe", "{0:n2}")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 102;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn11.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn11.Caption = "Haber Nac.";
            this.gridColumn11.DisplayFormat.FormatString = "n2";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn11.FieldName = "Ctb_Importe_Haber";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber", "{0:n2}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 98;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn12.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn12.Caption = "Debe Extr.";
            this.gridColumn12.DisplayFormat.FormatString = "n2";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn12.FieldName = "Ctb_Importe_Debe_Extr";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe_Extr", "{0:n2}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            this.gridColumn12.Width = 108;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn13.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn13.Caption = "Haber Extr.";
            this.gridColumn13.DisplayFormat.FormatString = "n2";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "Ctb_Importe_Haber_Extr";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber_Extr", "{0:n2}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            this.gridColumn13.Width = 105;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(615, 49);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(42, 13);
            this.labelControl15.TabIndex = 11;
            this.labelControl15.Text = "Importe:";
            // 
            // txtimportecontable
            // 
            this.txtimportecontable.EnterMoveNextControl = true;
            this.txtimportecontable.Location = new System.Drawing.Point(665, 46);
            this.txtimportecontable.Name = "txtimportecontable";
            this.txtimportecontable.Properties.Mask.EditMask = "n3";
            this.txtimportecontable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimportecontable.Size = new System.Drawing.Size(85, 20);
            this.txtimportecontable.TabIndex = 12;
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(423, 46);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodesc.Size = new System.Drawing.Size(184, 20);
            this.txttipodesc.TabIndex = 10;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(347, 49);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(24, 13);
            this.labelControl14.TabIndex = 8;
            this.labelControl14.Text = "Tipo:";
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(378, 46);
            this.txttipo.Name = "txttipo";
            this.txttipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipo.Size = new System.Drawing.Size(44, 20);
            this.txttipo.TabIndex = 9;
            this.txttipo.TextChanged += new System.EventHandler(this.txttipo_TextChanged_1);
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipo_KeyDown);
            // 
            // txttipoopedesc
            // 
            this.txttipoopedesc.Enabled = false;
            this.txttipoopedesc.EnterMoveNextControl = true;
            this.txttipoopedesc.Location = new System.Drawing.Point(123, 46);
            this.txttipoopedesc.Name = "txttipoopedesc";
            this.txttipoopedesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipoopedesc.Size = new System.Drawing.Size(219, 20);
            this.txttipoopedesc.TabIndex = 7;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(20, 49);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(53, 13);
            this.labelControl12.TabIndex = 5;
            this.labelControl12.Text = "Operacion:";
            // 
            // txttipoopecod
            // 
            this.txttipoopecod.EnterMoveNextControl = true;
            this.txttipoopecod.Location = new System.Drawing.Point(78, 46);
            this.txttipoopecod.Name = "txttipoopecod";
            this.txttipoopecod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipoopecod.Size = new System.Drawing.Size(44, 20);
            this.txttipoopecod.TabIndex = 6;
            this.txttipoopecod.TextChanged += new System.EventHandler(this.txttipoopecod_TextChanged);
            this.txttipoopecod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoopecod_KeyDown);
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(123, 23);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuentadesc.Size = new System.Drawing.Size(627, 20);
            this.txtcuentadesc.TabIndex = 4;
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(61, 23);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuenta.Size = new System.Drawing.Size(60, 20);
            this.txtcuenta.TabIndex = 3;
            this.txtcuenta.TextChanged += new System.EventHandler(this.txtcuenta_TextChanged_1);
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(15, 26);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(39, 13);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "Cuenta:";
            // 
            // btnregimen
            // 
            this.btnregimen.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregimen.Appearance.Options.UseFont = true;
            this.btnregimen.ImageOptions.Image = global::Contable.Properties.Resources.Aplicado;
            this.btnregimen.ImageOptions.ImageToTextIndent = 20;
            this.btnregimen.Location = new System.Drawing.Point(919, 112);
            this.btnregimen.Name = "btnregimen";
            this.btnregimen.Size = new System.Drawing.Size(100, 37);
            this.btnregimen.TabIndex = 222;
            this.btnregimen.Text = "Regimen";
            this.btnregimen.Click += new System.EventHandler(this.btnregimen_Click);
            // 
            // btndetadm
            // 
            this.btndetadm.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndetadm.Appearance.Options.UseFont = true;
            this.btndetadm.ImageOptions.Image = global::Contable.Properties.Resources.Administrativo;
            this.btndetadm.Location = new System.Drawing.Point(919, 155);
            this.btndetadm.Name = "btndetadm";
            this.btndetadm.Size = new System.Drawing.Size(100, 37);
            this.btndetadm.TabIndex = 223;
            this.btndetadm.Text = "Det. Adm";
            this.btndetadm.Click += new System.EventHandler(this.btndetadm_Click);
            // 
            // frm_ventas_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 556);
            this.Controls.Add(this.btndetadm);
            this.Controls.Add(this.btnregimen);
            this.Controls.Add(this.xtraTabControl2);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.btnbuscardocref);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl5);
            this.Controls.Add(this.barDockControl6);
            this.Controls.Add(this.barDockControl7);
            this.Controls.Add(this.barDockControl8);
            this.Controls.Add(this.barDockControl9);
            this.Controls.Add(this.barDockControl10);
            this.Controls.Add(this.barDockControl11);
            this.Controls.Add(this.barDockControl12);
            this.Controls.Add(this.barDockControl13);
            this.Controls.Add(this.barDockControl14);
            this.Controls.Add(this.barDockControl15);
            this.Controls.Add(this.barDockControl16);
            this.Controls.Add(this.barDockControl17);
            this.Controls.Add(this.barDockControl18);
            this.Controls.Add(this.barDockControl19);
            this.Controls.Add(this.barDockControl20);
            this.Controls.Add(this.barDockControl21);
            this.Controls.Add(this.barDockControl22);
            this.Controls.Add(this.barDockControl23);
            this.Controls.Add(this.barDockControl24);
            this.Controls.Add(this.barDockControl25);
            this.Controls.Add(this.barDockControl26);
            this.Controls.Add(this.barDockControl27);
            this.Controls.Add(this.barDockControl28);
            this.Controls.Add(this.barDockControl29);
            this.Controls.Add(this.barDockControl30);
            this.Controls.Add(this.barDockControl31);
            this.Controls.Add(this.barDockControl32);
            this.Controls.Add(this.barDockControl33);
            this.Controls.Add(this.barDockControl34);
            this.Controls.Add(this.barDockControl35);
            this.Controls.Add(this.barDockControl36);
            this.Controls.Add(this.barDockControl37);
            this.Controls.Add(this.barDockControl38);
            this.Controls.Add(this.barDockControl39);
            this.Controls.Add(this.barDockControl40);
            this.Controls.Add(this.barDockControl41);
            this.Controls.Add(this.barDockControl42);
            this.Controls.Add(this.barDockControl43);
            this.Controls.Add(this.barDockControl44);
            this.Controls.Add(this.barDockControl45);
            this.Controls.Add(this.barDockControl46);
            this.Controls.Add(this.barDockControl47);
            this.Controls.Add(this.barDockControl48);
            this.Controls.Add(this.barDockControl49);
            this.Controls.Add(this.barDockControl50);
            this.Controls.Add(this.barDockControl51);
            this.Controls.Add(this.barDockControl52);
            this.Controls.Add(this.barDockControl53);
            this.Controls.Add(this.barDockControl54);
            this.Controls.Add(this.barDockControl55);
            this.Controls.Add(this.barDockControl56);
            this.Controls.Add(this.barDockControl57);
            this.Controls.Add(this.barDockControl58);
            this.Controls.Add(this.barDockControl59);
            this.Controls.Add(this.barDockControl60);
            this.Controls.Add(this.barDockControl61);
            this.Controls.Add(this.barDockControl62);
            this.Controls.Add(this.barDockControl63);
            this.Controls.Add(this.barDockControl64);
            this.Controls.Add(this.barDockControl65);
            this.Controls.Add(this.barDockControl66);
            this.Controls.Add(this.barDockControl67);
            this.Controls.Add(this.barDockControl68);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_ventas_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ventas";
            this.Load += new System.EventHandler(this.frm_ventas_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtinafecta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexonerada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorfactexport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatosContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportecontable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopedesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopecod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarDockControl barDockControl6;
        private DevExpress.XtraBars.BarDockControl barDockControl7;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraBars.BarDockControl barDockControl9;
        private DevExpress.XtraBars.BarDockControl barDockControl10;
        private DevExpress.XtraBars.BarDockControl barDockControl11;
        private DevExpress.XtraBars.BarDockControl barDockControl12;
        private DevExpress.XtraBars.BarDockControl barDockControl13;
        private DevExpress.XtraBars.BarDockControl barDockControl14;
        private DevExpress.XtraBars.BarDockControl barDockControl15;
        private DevExpress.XtraBars.BarDockControl barDockControl16;
        private DevExpress.XtraBars.BarDockControl barDockControl17;
        private DevExpress.XtraBars.BarDockControl barDockControl18;
        private DevExpress.XtraBars.BarDockControl barDockControl19;
        private DevExpress.XtraBars.BarDockControl barDockControl20;
        private DevExpress.XtraBars.BarDockControl barDockControl21;
        private DevExpress.XtraBars.BarDockControl barDockControl22;
        private DevExpress.XtraBars.BarDockControl barDockControl23;
        private DevExpress.XtraBars.BarDockControl barDockControl24;
        private DevExpress.XtraBars.BarDockControl barDockControl25;
        private DevExpress.XtraBars.BarDockControl barDockControl26;
        private DevExpress.XtraBars.BarDockControl barDockControl27;
        private DevExpress.XtraBars.BarDockControl barDockControl28;
        private DevExpress.XtraBars.BarDockControl barDockControl29;
        private DevExpress.XtraBars.BarDockControl barDockControl30;
        private DevExpress.XtraBars.BarDockControl barDockControl31;
        private DevExpress.XtraBars.BarDockControl barDockControl32;
        private DevExpress.XtraBars.BarDockControl barDockControl33;
        private DevExpress.XtraBars.BarDockControl barDockControl34;
        private DevExpress.XtraBars.BarDockControl barDockControl35;
        private DevExpress.XtraBars.BarDockControl barDockControl36;
        private DevExpress.XtraBars.BarDockControl barDockControl37;
        private DevExpress.XtraBars.BarDockControl barDockControl38;
        private DevExpress.XtraBars.BarDockControl barDockControl39;
        private DevExpress.XtraBars.BarDockControl barDockControl40;
        private DevExpress.XtraBars.BarDockControl barDockControl41;
        private DevExpress.XtraBars.BarDockControl barDockControl42;
        private DevExpress.XtraBars.BarDockControl barDockControl43;
        private DevExpress.XtraBars.BarDockControl barDockControl44;
        private DevExpress.XtraBars.BarDockControl barDockControl45;
        private DevExpress.XtraBars.BarDockControl barDockControl46;
        private DevExpress.XtraBars.BarDockControl barDockControl47;
        private DevExpress.XtraBars.BarDockControl barDockControl48;
        private DevExpress.XtraBars.BarDockControl barDockControl49;
        private DevExpress.XtraBars.BarDockControl barDockControl50;
        private DevExpress.XtraBars.BarDockControl barDockControl51;
        private DevExpress.XtraBars.BarDockControl barDockControl52;
        private DevExpress.XtraBars.BarDockControl barDockControl53;
        private DevExpress.XtraBars.BarDockControl barDockControl54;
        private DevExpress.XtraBars.BarDockControl barDockControl55;
        private DevExpress.XtraBars.BarDockControl barDockControl56;
        private DevExpress.XtraBars.BarDockControl barDockControl57;
        private DevExpress.XtraBars.BarDockControl barDockControl58;
        private DevExpress.XtraBars.BarDockControl barDockControl59;
        private DevExpress.XtraBars.BarDockControl barDockControl60;
        private DevExpress.XtraBars.BarDockControl barDockControl61;
        private DevExpress.XtraBars.BarDockControl barDockControl62;
        private DevExpress.XtraBars.BarDockControl barDockControl63;
        private DevExpress.XtraBars.BarDockControl barDockControl64;
        private DevExpress.XtraBars.BarDockControl barDockControl65;
        private DevExpress.XtraBars.BarDockControl barDockControl66;
        private DevExpress.XtraBars.BarDockControl barDockControl67;
        private DevExpress.XtraBars.BarDockControl barDockControl68;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtentidad;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;

        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtvalorfactexport;
        private DevExpress.XtraEditors.TextEdit txtbaseimponible;
        private DevExpress.XtraEditors.TextEdit txtimportetotal;
        private DevExpress.XtraEditors.TextEdit txtotrostribuimporte;
        private DevExpress.XtraEditors.TextEdit txtigv;
        private DevExpress.XtraEditors.TextEdit txtimporteisc;
        private DevExpress.XtraEditors.TextEdit txtinafecta;
        private DevExpress.XtraEditors.TextEdit txtexonerada;
        private DevExpress.XtraEditors.CheckEdit chkisc;
        private DevExpress.XtraEditors.CheckEdit chkotrostri;
        private DevExpress.XtraEditors.SimpleButton btnbuscardocref;
        private DevExpress.XtraEditors.TextEdit txtigvporcentaje;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtanalisisdesc;
        private DevExpress.XtraEditors.TextEdit txtidanalisis;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.TextEdit txtcuenta;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txttipoopedesc;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txttipoopecod;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtimportecontable;
        private DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txttipo;
        private DevExpress.XtraGrid.GridControl dgvdatosContable;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnnuevodetcon;
        private DevExpress.XtraEditors.SimpleButton btneditardetcon;
        private DevExpress.XtraEditors.SimpleButton btnquitardetcon;
        private DevExpress.XtraEditors.SimpleButton btnanadirdetcon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.SimpleButton btndetadm;
        private DevExpress.XtraEditors.SimpleButton btnregimen;
        private DevExpress.XtraEditors.TextEdit txtfechavencimiento;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private System.Windows.Forms.TextBox TxtDMN;
        private System.Windows.Forms.TextBox TxtHME;
        private System.Windows.Forms.TextBox TxtHMN;
        private System.Windows.Forms.TextBox TxtDME;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}