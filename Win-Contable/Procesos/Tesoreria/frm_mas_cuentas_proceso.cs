﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_mas_cuentas_proceso : Form
    {
        public frm_mas_cuentas_proceso()
        {
            InitializeComponent();
        }

        public string empresa;
        public string anio;
        public string tag_proceso;

        public List<Entidad_Plan_Empresarial> Lista = new List<Entidad_Plan_Empresarial>();
        public void Listar()
        {
            Entidad_Plan_Empresarial Ent = new Entidad_Plan_Empresarial();
            Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

            Ent.Id_Empresa = empresa;
            Ent.Id_Anio = anio;
            Ent.Tag_Proceso = tag_proceso;

            try
            {
                Lista = log.Busqueda_Cta_Por_Proceso(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void frm_mas_cuentas_proceso_Load(object sender, EventArgs e)
        {
            Listar();
        }
    }
}
