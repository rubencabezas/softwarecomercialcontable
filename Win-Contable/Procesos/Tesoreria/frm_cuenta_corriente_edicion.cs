﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_cuenta_corriente_edicion : Contable.frm_fuente
    {
        public frm_cuenta_corriente_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        public string Id_Empresa,Id_Anio,Id_Cuenta_Corriente;


        void LimpiarCab()
        {
            txtctacorriente.ResetText();
            txtentfinancieracod.ResetText();
            txtentfinancieradesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();
        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();

                txtctacorriente.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuenta.EnterMoveNextControl = false;
                    txtcuenta.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                   
                                txtmonedacod.Text = Entidad.Id_Moneda;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                       
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                  
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 3);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Moneda = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Moneda).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                       
                            txtmonedacod.Text = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {



                    Entidad_Cuenta_Corriente Ent = new Entidad_Cuenta_Corriente();
                    Logica_Cuenta_Corriente Log = new Logica_Cuenta_Corriente();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Cuenta_Corriente = txtctacorriente.Text.Trim();
                    Ent.Cor_Ent_Financiera = txtentfinancieracod.Text.Trim();
                    Ent.Cor_Moneda_cod = txtmonedacod.Text.Trim();
                    Ent.Cor_Cuenta_Contable = txtcuenta.Text.Trim();

                    
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Estado = Estados.Nuevo;
                                LimpiarCab();
                                txtctacorriente.Focus();
                                this.Close();

                            }

                        }
                        else if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                this.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtctacorriente.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una cuena corriente");
                txtctacorriente.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtentfinancieradesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una entidad financiera");
                txtentfinancieracod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }
      if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una cuenta");
                txtcuenta.Focus();
                return false;
            }
            return true;
        }

        private void frm_cuenta_corriente_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;

                txtctacorriente.Focus();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                txtctacorriente.Enabled = false;
                ListarModificar();

            }
        }

        public List<Entidad_Cuenta_Corriente> Lista_Modificar = new List<Entidad_Cuenta_Corriente>();

        public void ListarModificar()
        {
            Entidad_Cuenta_Corriente Ent = new Entidad_Cuenta_Corriente();
            Logica_Cuenta_Corriente log = new Logica_Cuenta_Corriente();

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Cuenta_Corriente = Id_Cuenta_Corriente;
            try
            {
                Lista_Modificar = log.Listar(Ent);

                Entidad_Cuenta_Corriente Entidad = new Entidad_Cuenta_Corriente();
                Entidad = Lista_Modificar[0];


                txtctacorriente.Text = Entidad.Id_Cuenta_Corriente;
                txtentfinancieracod.Text = Entidad.Cor_Ent_Financiera;
                txtentfinancieradesc.Text = Entidad.Fin_Descripcion;
                txtmonedacod.Text = Entidad.Cor_Moneda_cod;
                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                txtcuenta.Text = Entidad.Cor_Cuenta_Contable;
                txtcuentadesc.Text = Entidad.Cta_Descripcion;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void txtentfinancieracod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtentfinancieracod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtentfinancieracod.Text.Substring(txtentfinancieracod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_entidad_financiera_busqueda f = new _1_Busquedas_Generales.frm_entidad_financiera_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad_Financiera Entidad = new Entidad_Entidad_Financiera();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtentfinancieracod.Text = Entidad.Id_SUNAT;
                                txtentfinancieradesc.Text = Entidad.Fin_Descripcion;

                                txtentfinancieracod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtentfinancieracod.Text) & string.IsNullOrEmpty(txtentfinancieradesc.Text))
                    {
                        BuscarEntFinanciera();

                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEntFinanciera()
        {
            try
            {
                txtentfinancieracod.Text = Accion.Formato(txtentfinancieracod.Text, 2);
                Logica_Entidad_Financiera log = new Logica_Entidad_Financiera();

                List<Entidad_Entidad_Financiera> Generales = new List<Entidad_Entidad_Financiera>();
                Generales = log.Listar(new Entidad_Entidad_Financiera
                {
                    Id_SUNAT = txtentfinancieracod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad_Financiera T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtentfinancieracod.Text.Trim().ToUpper())
                        {

                            txtentfinancieracod.Text = (T.Id_SUNAT).ToString().Trim();
                            txtentfinancieradesc.Text = T.Fin_Descripcion;
                            txtentfinancieracod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtentfinancieracod.EnterMoveNextControl = false;
                    txtentfinancieracod.ResetText();
                    txtentfinancieracod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
