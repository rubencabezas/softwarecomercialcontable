﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_caja_chica_edicion : Contable.frm_fuente
    {
        public frm_caja_chica_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        public string Id_Empresa,Id_Anio,Id_Caja;


        void LimpiarCab()
        {
            txtdescripcion.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();
            chkcajacentral.Checked=false;
            chkcajachica.Checked=false;
        }

        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                txtdescripcion.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;


            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }
        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using ( frm_entidades_busqueda f = new  frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdni.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdni.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdni.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdni.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdni.EnterMoveNextControl = false;
                                txtrucdni.ResetText();
                                txtrucdni.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuenta.EnterMoveNextControl = false;
                    txtcuenta.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_caja_chica_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();

            }

        }

        public List<Entidad_Caja_Chica_Config> Lista_Modificar = new List<Entidad_Caja_Chica_Config>();
        public void ListarModificar()
        {
            Entidad_Caja_Chica_Config Ent = new Entidad_Caja_Chica_Config();
            Logica_Caja_Chica_Config log = new Logica_Caja_Chica_Config();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Caja = Id_Caja;


            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Caja_Chica_Config Enti = new Entidad_Caja_Chica_Config();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Caja = Enti.Id_Caja;

                    txtdescripcion.Text = Enti.Cja_Descripcion;
                    txtrucdni.Text = Enti.Cja_Responsable;
                    txtentidad.Text = Enti.Cja_Responsable_Desc;

                    txtcuenta.Text = Enti.Cja_Cuenta;
                    txtcuentadesc.Text = Enti.Cja_Cuenta_Desc;

                                chkcajachica.Checked = Enti.Cja_Es_Caja_Chica;
                    chkcajacentral.Checked = Enti.Cja_Es_Caja_Central;

                 
                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }



        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Caja_Chica_Config Ent = new Entidad_Caja_Chica_Config();
                    Logica_Caja_Chica_Config Log = new Logica_Caja_Chica_Config();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Caja = Id_Caja;
                    Ent.Cja_Descripcion = txtdescripcion.Text.ToString().Trim();
                    Ent.Cja_Responsable =txtrucdni.Text;
                   

                    Ent.Cja_Cuenta = txtcuenta.Text.ToString().Trim();
                    Ent.Cja_Es_Caja_Chica = chkcajachica.Checked;

                    Ent.Cja_Es_Caja_Central = chkcajacentral.Checked;
                    

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                             txtdescripcion.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtdescripcion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtdescripcion.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un responsable");
                txtrucdni.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una cuenta contable");
                txtcuenta.Focus();
                return false;
            }

            if (chkcajacentral.Checked==false &&  chkcajachica.Checked==false)
            {
                Accion.Advertencia("Debe seleccionar un tipo (selecionar un chek)");
                
                return false;
            }

         

            return true;
        }




    }
}
