﻿namespace Contable.Procesos.Tesoreria
{
    partial class frm_cierre_cajachica_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.bntcerrar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtDiferencias = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.TxtDifRedondeo = new DevExpress.XtraEditors.TextEdit();
            this.TxtDifCambio = new DevExpress.XtraEditors.TextEdit();
            this.ChkDifRedondeo = new System.Windows.Forms.CheckBox();
            this.ChkDifCambio = new System.Windows.Forms.CheckBox();
            this.txtcierre = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteRendicion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtcajadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcaja = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txttipomovimientodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipomovimientocod = new DevExpress.XtraEditors.TextEdit();
            this.txttipoEgresodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipoEgreso = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtimporte = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtnumero = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txtTotalImporteMNDet = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteMNDet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.txtentidaddet = new DevExpress.XtraEditors.TextEdit();
            this.txtrucdnidet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoentdescdet = new DevExpress.XtraEditors.TextEdit();
            this.txttipoentcoddet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtimporteMEDet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalordet = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodescdet = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocoddet = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtmonedadescdet = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacoddet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechadocdet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtseriedet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodocdescdet = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiferencias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifRedondeo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifCambio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcierre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteRendicion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcajadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcaja.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgresodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgreso.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalImporteMNDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMNDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidaddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdnidet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentdescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentcoddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMEDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalordet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocoddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacoddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadocdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtseriedet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.bntcerrar});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.bntcerrar)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // bntcerrar
            // 
            this.bntcerrar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bntcerrar.Caption = "Cancelar";
            this.bntcerrar.Id = 1;
            this.bntcerrar.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.bntcerrar.Name = "bntcerrar";
            this.bntcerrar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(975, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 645);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(975, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 613);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(975, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 613);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.txtcierre);
            this.groupControl1.Controls.Add(this.txtimporteRendicion);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl27);
            this.groupControl1.Controls.Add(this.labelControl25);
            this.groupControl1.Controls.Add(this.txtcajadesc);
            this.groupControl1.Controls.Add(this.txtcaja);
            this.groupControl1.Controls.Add(this.labelControl24);
            this.groupControl1.Controls.Add(this.txttipomovimientodesc);
            this.groupControl1.Controls.Add(this.txttipomovimientocod);
            this.groupControl1.Controls.Add(this.txttipoEgresodesc);
            this.groupControl1.Controls.Add(this.txttipoEgreso);
            this.groupControl1.Controls.Add(this.labelControl23);
            this.groupControl1.Controls.Add(this.labelControl22);
            this.groupControl1.Controls.Add(this.labelControl21);
            this.groupControl1.Controls.Add(this.labelControl20);
            this.groupControl1.Controls.Add(this.txtimporte);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txttipocambiovalor);
            this.groupControl1.Controls.Add(this.txttipocambiodesc);
            this.groupControl1.Controls.Add(this.txttipocambiocod);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtentidad);
            this.groupControl1.Controls.Add(this.txtrucdni);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtnumero);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(975, 184);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Datos";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtDiferencias);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Label25);
            this.groupBox1.Controls.Add(this.TxtDifRedondeo);
            this.groupBox1.Controls.Add(this.TxtDifCambio);
            this.groupBox1.Controls.Add(this.ChkDifRedondeo);
            this.groupBox1.Controls.Add(this.ChkDifCambio);
            this.groupBox1.Location = new System.Drawing.Point(755, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 91);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Diferencias";
            // 
            // TxtDiferencias
            // 
            this.TxtDiferencias.Enabled = false;
            this.TxtDiferencias.Location = new System.Drawing.Point(78, 67);
            this.TxtDiferencias.MenuManager = this.barManager1;
            this.TxtDiferencias.Name = "TxtDiferencias";
            this.TxtDiferencias.Properties.Mask.EditMask = "n3";
            this.TxtDiferencias.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtDiferencias.Size = new System.Drawing.Size(100, 20);
            this.TxtDiferencias.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Diferencias";
            // 
            // Label25
            // 
            this.Label25.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Label25.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.Label25.Location = new System.Drawing.Point(17, 62);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(160, 1);
            this.Label25.TabIndex = 5;
            this.Label25.Text = "Label25";
            // 
            // TxtDifRedondeo
            // 
            this.TxtDifRedondeo.Location = new System.Drawing.Point(87, 39);
            this.TxtDifRedondeo.Name = "TxtDifRedondeo";
            this.TxtDifRedondeo.Properties.Mask.EditMask = "n3";
            this.TxtDifRedondeo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtDifRedondeo.Size = new System.Drawing.Size(91, 20);
            this.TxtDifRedondeo.TabIndex = 2;
            // 
            // TxtDifCambio
            // 
            this.TxtDifCambio.Location = new System.Drawing.Point(87, 18);
            this.TxtDifCambio.MenuManager = this.barManager1;
            this.TxtDifCambio.Name = "TxtDifCambio";
            this.TxtDifCambio.Properties.Mask.EditMask = "n3";
            this.TxtDifCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtDifCambio.Size = new System.Drawing.Size(91, 20);
            this.TxtDifCambio.TabIndex = 2;
            // 
            // ChkDifRedondeo
            // 
            this.ChkDifRedondeo.AutoSize = true;
            this.ChkDifRedondeo.Location = new System.Drawing.Point(6, 42);
            this.ChkDifRedondeo.Name = "ChkDifRedondeo";
            this.ChkDifRedondeo.Size = new System.Drawing.Size(87, 17);
            this.ChkDifRedondeo.TabIndex = 1;
            this.ChkDifRedondeo.Text = "de redondeo";
            this.ChkDifRedondeo.UseVisualStyleBackColor = true;
            this.ChkDifRedondeo.CheckedChanged += new System.EventHandler(this.ChkDifRedondeo_CheckedChanged);
            // 
            // ChkDifCambio
            // 
            this.ChkDifCambio.AutoSize = true;
            this.ChkDifCambio.Location = new System.Drawing.Point(6, 20);
            this.ChkDifCambio.Name = "ChkDifCambio";
            this.ChkDifCambio.Size = new System.Drawing.Size(74, 17);
            this.ChkDifCambio.TabIndex = 0;
            this.ChkDifCambio.Text = "de cambio";
            this.ChkDifCambio.UseVisualStyleBackColor = true;
            this.ChkDifCambio.CheckedChanged += new System.EventHandler(this.ChkDifCambio_CheckedChanged);
            // 
            // txtcierre
            // 
            this.txtcierre.Enabled = false;
            this.txtcierre.Location = new System.Drawing.Point(861, 137);
            this.txtcierre.Name = "txtcierre";
            this.txtcierre.Size = new System.Drawing.Size(75, 20);
            this.txtcierre.TabIndex = 35;
            // 
            // txtimporteRendicion
            // 
            this.txtimporteRendicion.Enabled = false;
            this.txtimporteRendicion.Location = new System.Drawing.Point(746, 137);
            this.txtimporteRendicion.Name = "txtimporteRendicion";
            this.txtimporteRendicion.Size = new System.Drawing.Size(75, 20);
            this.txtimporteRendicion.TabIndex = 33;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(825, 141);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(29, 13);
            this.labelControl6.TabIndex = 34;
            this.labelControl6.Text = "Cierre";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(694, 141);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(46, 13);
            this.labelControl27.TabIndex = 32;
            this.labelControl27.Text = "Rendicion";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(443, 72);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(0, 13);
            this.labelControl25.TabIndex = 11;
            // 
            // txtcajadesc
            // 
            this.txtcajadesc.Enabled = false;
            this.txtcajadesc.EnterMoveNextControl = true;
            this.txtcajadesc.Location = new System.Drawing.Point(119, 70);
            this.txtcajadesc.Name = "txtcajadesc";
            this.txtcajadesc.Size = new System.Drawing.Size(627, 20);
            this.txtcajadesc.TabIndex = 10;
            // 
            // txtcaja
            // 
            this.txtcaja.EnterMoveNextControl = true;
            this.txtcaja.Location = new System.Drawing.Point(75, 71);
            this.txtcaja.Name = "txtcaja";
            this.txtcaja.Size = new System.Drawing.Size(40, 20);
            this.txtcaja.TabIndex = 9;
            this.txtcaja.TextChanged += new System.EventHandler(this.txtcaja_TextChanged);
            this.txtcaja.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcaja_KeyDown);
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(16, 74);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(53, 13);
            this.labelControl24.TabIndex = 8;
            this.labelControl24.Text = "Caja chica:";
            // 
            // txttipomovimientodesc
            // 
            this.txttipomovimientodesc.Enabled = false;
            this.txttipomovimientodesc.EnterMoveNextControl = true;
            this.txttipomovimientodesc.Location = new System.Drawing.Point(426, 48);
            this.txttipomovimientodesc.Name = "txttipomovimientodesc";
            this.txttipomovimientodesc.Size = new System.Drawing.Size(320, 20);
            this.txttipomovimientodesc.TabIndex = 7;
            // 
            // txttipomovimientocod
            // 
            this.txttipomovimientocod.Enabled = false;
            this.txttipomovimientocod.EnterMoveNextControl = true;
            this.txttipomovimientocod.Location = new System.Drawing.Point(377, 48);
            this.txttipomovimientocod.Name = "txttipomovimientocod";
            this.txttipomovimientocod.Size = new System.Drawing.Size(47, 20);
            this.txttipomovimientocod.TabIndex = 6;
            // 
            // txttipoEgresodesc
            // 
            this.txttipoEgresodesc.Enabled = false;
            this.txttipoEgresodesc.EnterMoveNextControl = true;
            this.txttipoEgresodesc.Location = new System.Drawing.Point(117, 49);
            this.txttipoEgresodesc.Name = "txttipoEgresodesc";
            this.txttipoEgresodesc.Size = new System.Drawing.Size(196, 20);
            this.txttipoEgresodesc.TabIndex = 4;
            // 
            // txttipoEgreso
            // 
            this.txttipoEgreso.Enabled = false;
            this.txttipoEgreso.EnterMoveNextControl = true;
            this.txttipoEgreso.Location = new System.Drawing.Point(75, 49);
            this.txttipoEgreso.Name = "txttipoEgreso";
            this.txttipoEgreso.Size = new System.Drawing.Size(39, 20);
            this.txttipoEgreso.TabIndex = 3;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(317, 51);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(58, 13);
            this.labelControl23.TabIndex = 5;
            this.labelControl23.Text = "Movimiento:";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(46, 51);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(24, 13);
            this.labelControl22.TabIndex = 2;
            this.labelControl22.Text = "Tipo:";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(347, 97);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(0, 13);
            this.labelControl21.TabIndex = 15;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(7, 96);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(0, 13);
            this.labelControl20.TabIndex = 13;
            // 
            // txtimporte
            // 
            this.txtimporte.EnterMoveNextControl = true;
            this.txtimporte.Location = new System.Drawing.Point(607, 137);
            this.txtimporte.Name = "txtimporte";
            this.txtimporte.Properties.Mask.EditMask = "n";
            this.txtimporte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporte.Size = new System.Drawing.Size(84, 20);
            this.txtimporte.TabIndex = 31;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(559, 140);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(44, 13);
            this.labelControl10.TabIndex = 30;
            this.labelControl10.Text = "Asignado";
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.Enabled = false;
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(494, 137);
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Size = new System.Drawing.Size(62, 20);
            this.txttipocambiovalor.TabIndex = 29;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(387, 137);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 28;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.Enabled = false;
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(351, 137);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 27;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(324, 140);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(21, 13);
            this.labelControl9.TabIndex = 26;
            this.labelControl9.Text = "T.C.";
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(118, 137);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(193, 20);
            this.txtmonedadesc.TabIndex = 25;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.Enabled = false;
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(74, 137);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Size = new System.Drawing.Size(42, 20);
            this.txtmonedacod.TabIndex = 24;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(27, 140);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(42, 13);
            this.labelControl8.TabIndex = 23;
            this.labelControl8.Text = "Moneda:";
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(160, 93);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Size = new System.Drawing.Size(586, 20);
            this.txtentidad.TabIndex = 13;
            // 
            // txtrucdni
            // 
            this.txtrucdni.Enabled = false;
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(75, 93);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 12;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(5, 96);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(65, 13);
            this.labelControl7.TabIndex = 11;
            this.labelControl7.Text = "Responsable:";
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(674, 115);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(72, 20);
            this.txtfechadoc.TabIndex = 22;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(585, 118);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(79, 13);
            this.labelControl5.TabIndex = 21;
            this.labelControl5.Text = "Fecha rendicion:";
            // 
            // txtnumero
            // 
            this.txtnumero.EnterMoveNextControl = true;
            this.txtnumero.Location = new System.Drawing.Point(477, 115);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(102, 20);
            this.txtnumero.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(432, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Numero:";
            // 
            // txtserie
            // 
            this.txtserie.Enabled = false;
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(351, 115);
            this.txtserie.Name = "txtserie";
            this.txtserie.Size = new System.Drawing.Size(73, 20);
            this.txtserie.TabIndex = 18;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(317, 118);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "Serie:";
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(118, 115);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(192, 20);
            this.txttipodocdesc.TabIndex = 16;
            // 
            // txttipodoc
            // 
            this.txttipodoc.Enabled = false;
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(75, 115);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Size = new System.Drawing.Size(41, 20);
            this.txttipodoc.TabIndex = 15;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(30, 118);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "T. Doc.:";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(75, 159);
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(863, 20);
            this.txtglosa.TabIndex = 37;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(39, 162);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 36;
            this.labelControl2.Text = "Glosa:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(74, 26);
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Size = new System.Drawing.Size(672, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(42, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl28);
            this.groupControl2.Controls.Add(this.txtTotalImporteMNDet);
            this.groupControl2.Controls.Add(this.txtimporteMNDet);
            this.groupControl2.Controls.Add(this.labelControl26);
            this.groupControl2.Controls.Add(this.dgvdatos);
            this.groupControl2.Controls.Add(this.btnanadirdet);
            this.groupControl2.Controls.Add(this.btneditardet);
            this.groupControl2.Controls.Add(this.btnquitardet);
            this.groupControl2.Controls.Add(this.btnnuevodet);
            this.groupControl2.Controls.Add(this.txtentidaddet);
            this.groupControl2.Controls.Add(this.txtrucdnidet);
            this.groupControl2.Controls.Add(this.labelControl19);
            this.groupControl2.Controls.Add(this.txttipoentdescdet);
            this.groupControl2.Controls.Add(this.txttipoentcoddet);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.txtimporteMEDet);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.txttipodesc);
            this.groupControl2.Controls.Add(this.txttipo);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.txttipocambiovalordet);
            this.groupControl2.Controls.Add(this.txttipocambiodescdet);
            this.groupControl2.Controls.Add(this.txttipocambiocoddet);
            this.groupControl2.Controls.Add(this.label3);
            this.groupControl2.Controls.Add(this.txtmonedadescdet);
            this.groupControl2.Controls.Add(this.txtmonedacoddet);
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.txtfechadocdet);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.txtseriedet);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.txttipodocdescdet);
            this.groupControl2.Controls.Add(this.txttipodocdet);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.txtcuentadesc);
            this.groupControl2.Controls.Add(this.txtcuenta);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Location = new System.Drawing.Point(0, 227);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(975, 418);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Detalles";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(777, 121);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(68, 13);
            this.labelControl28.TabIndex = 48;
            this.labelControl28.Text = "Total detalles:";
            // 
            // txtTotalImporteMNDet
            // 
            this.txtTotalImporteMNDet.Location = new System.Drawing.Point(851, 115);
            this.txtTotalImporteMNDet.MenuManager = this.barManager1;
            this.txtTotalImporteMNDet.Name = "txtTotalImporteMNDet";
            this.txtTotalImporteMNDet.Properties.ReadOnly = true;
            this.txtTotalImporteMNDet.Size = new System.Drawing.Size(100, 20);
            this.txtTotalImporteMNDet.TabIndex = 47;
            // 
            // txtimporteMNDet
            // 
            this.txtimporteMNDet.EnterMoveNextControl = true;
            this.txtimporteMNDet.Location = new System.Drawing.Point(265, 116);
            this.txtimporteMNDet.Name = "txtimporteMNDet";
            this.txtimporteMNDet.Properties.Mask.EditMask = "n";
            this.txtimporteMNDet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporteMNDet.Size = new System.Drawing.Size(100, 20);
            this.txtimporteMNDet.TabIndex = 30;
            this.txtimporteMNDet.TextChanged += new System.EventHandler(this.txtimporteMNDet_TextChanged);
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(221, 118);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(38, 13);
            this.labelControl26.TabIndex = 29;
            this.labelControl26.Text = "Importe";
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(0, 142);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(970, 271);
            this.dgvdatos.TabIndex = 34;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn15,
            this.gridColumn14,
            this.gridColumn16});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Cuenta";
            this.gridColumn1.FieldName = "Ctb_Cuenta";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 102;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Cuenta Desc.";
            this.gridColumn2.FieldName = "Ctb_Cuenta_Desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 181;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn10.Caption = "RUC";
            this.gridColumn10.FieldName = "Ctb_Ruc_dni_det";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 90;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn11.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn11.Caption = "Entidad";
            this.gridColumn11.FieldName = "Entidad_det";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 236;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Tipo Doc.";
            this.gridColumn3.FieldName = "Ctb_Tipo_Doc_det_desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 85;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "Serie";
            this.gridColumn4.FieldName = "Ctb_Serie_det";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Numero";
            this.gridColumn5.FieldName = "Ctb_Numero_det";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.Caption = "Fecha";
            this.gridColumn6.FieldName = "Ctb_Fecha_Mov_det";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            this.gridColumn6.Width = 93;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.Caption = "Moneda";
            this.gridColumn7.FieldName = "Ctb_moneda_det_desc";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.Caption = "T.C.";
            this.gridColumn8.FieldName = "Ctb_Tipo_Cambio_Desc_Det";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 9;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn9.Caption = "T.C. Valor";
            this.gridColumn9.FieldName = "Ctb_Tipo_Cambio_Valor_Det";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 10;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn12.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn12.Caption = "Tipo";
            this.gridColumn12.FieldName = "Ctb_Tipo_DH_Desc";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 11;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn13.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn13.Caption = "Importe Debe M.N";
            this.gridColumn13.DisplayFormat.FormatString = "n2";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "Ctb_Importe_Debe";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe", "{0:0.##}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 12;
            this.gridColumn13.Width = 112;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn15.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn15.Caption = "Importe Haber M.N.";
            this.gridColumn15.DisplayFormat.FormatString = "n2";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn15.FieldName = "Ctb_Importe_Haber";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber", "{0:0.##}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 13;
            this.gridColumn15.Width = 111;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn14.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn14.Caption = "Importe Debe M.E.";
            this.gridColumn14.DisplayFormat.FormatString = "n2";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn14.FieldName = "Ctb_Importe_Debe_Extr";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe_Extr", "{0:0.##}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 14;
            this.gridColumn14.Width = 109;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn16.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn16.Caption = "Importe Haber M.E.";
            this.gridColumn16.DisplayFormat.FormatString = "n2";
            this.gridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn16.FieldName = "Ctb_Importe_Haber_Extr";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber_Extr", "{0:0.##}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 15;
            this.gridColumn16.Width = 105;
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.btnanadirdet.Location = new System.Drawing.Point(858, 64);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(93, 28);
            this.btnanadirdet.TabIndex = 31;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.btneditardet.Location = new System.Drawing.Point(756, 65);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(93, 28);
            this.btneditardet.TabIndex = 42;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.btnquitardet.Location = new System.Drawing.Point(858, 35);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(93, 28);
            this.btnquitardet.TabIndex = 40;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.btnnuevodet.Location = new System.Drawing.Point(756, 35);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(93, 28);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // txtentidaddet
            // 
            this.txtentidaddet.Enabled = false;
            this.txtentidaddet.EnterMoveNextControl = true;
            this.txtentidaddet.Location = new System.Drawing.Point(140, 93);
            this.txtentidaddet.Name = "txtentidaddet";
            this.txtentidaddet.Size = new System.Drawing.Size(579, 20);
            this.txtentidaddet.TabIndex = 25;
            // 
            // txtrucdnidet
            // 
            this.txtrucdnidet.EnterMoveNextControl = true;
            this.txtrucdnidet.Location = new System.Drawing.Point(56, 93);
            this.txtrucdnidet.Name = "txtrucdnidet";
            this.txtrucdnidet.Size = new System.Drawing.Size(82, 20);
            this.txtrucdnidet.TabIndex = 24;
            this.txtrucdnidet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdnidet_KeyDown);
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(15, 95);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(40, 13);
            this.labelControl19.TabIndex = 23;
            this.labelControl19.Text = "Entidad:";
            // 
            // txttipoentdescdet
            // 
            this.txttipoentdescdet.Enabled = false;
            this.txttipoentdescdet.EnterMoveNextControl = true;
            this.txttipoentdescdet.Location = new System.Drawing.Point(608, 72);
            this.txttipoentdescdet.Name = "txttipoentdescdet";
            this.txttipoentdescdet.Size = new System.Drawing.Size(111, 20);
            this.txttipoentdescdet.TabIndex = 22;
            // 
            // txttipoentcoddet
            // 
            this.txttipoentcoddet.EnterMoveNextControl = true;
            this.txttipoentcoddet.Location = new System.Drawing.Point(569, 72);
            this.txttipoentcoddet.Name = "txttipoentcoddet";
            this.txttipoentcoddet.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipoentcoddet.Size = new System.Drawing.Size(37, 20);
            this.txttipoentcoddet.TabIndex = 21;
            this.txttipoentcoddet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoentcoddet_KeyDown);
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(531, 75);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(35, 13);
            this.labelControl18.TabIndex = 20;
            this.labelControl18.Text = "Tip. E.:";
            // 
            // txtimporteMEDet
            // 
            this.txtimporteMEDet.EnterMoveNextControl = true;
            this.txtimporteMEDet.Location = new System.Drawing.Point(443, 116);
            this.txtimporteMEDet.Name = "txtimporteMEDet";
            this.txtimporteMEDet.Properties.Mask.EditMask = "n";
            this.txtimporteMEDet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporteMEDet.Properties.ReadOnly = true;
            this.txtimporteMEDet.Size = new System.Drawing.Size(100, 20);
            this.txtimporteMEDet.TabIndex = 32;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(375, 119);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(64, 13);
            this.labelControl17.TabIndex = 31;
            this.labelControl17.Text = "Importe M.N.";
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(101, 115);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Size = new System.Drawing.Size(91, 20);
            this.txttipodesc.TabIndex = 28;
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(56, 115);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(43, 20);
            this.txttipo.TabIndex = 27;
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipo_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(30, 118);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(24, 13);
            this.labelControl16.TabIndex = 26;
            this.labelControl16.Text = "Tipo:";
            // 
            // txttipocambiovalordet
            // 
            this.txttipocambiovalordet.EnterMoveNextControl = true;
            this.txttipocambiovalordet.Location = new System.Drawing.Point(463, 71);
            this.txttipocambiovalordet.Name = "txttipocambiovalordet";
            this.txttipocambiovalordet.Size = new System.Drawing.Size(62, 20);
            this.txttipocambiovalordet.TabIndex = 19;
            // 
            // txttipocambiodescdet
            // 
            this.txttipocambiodescdet.Enabled = false;
            this.txttipocambiodescdet.EnterMoveNextControl = true;
            this.txttipocambiodescdet.Location = new System.Drawing.Point(319, 71);
            this.txttipocambiodescdet.Name = "txttipocambiodescdet";
            this.txttipocambiodescdet.Size = new System.Drawing.Size(140, 20);
            this.txttipocambiodescdet.TabIndex = 18;
            // 
            // txttipocambiocoddet
            // 
            this.txttipocambiocoddet.EnterMoveNextControl = true;
            this.txttipocambiocoddet.Location = new System.Drawing.Point(283, 71);
            this.txttipocambiocoddet.Name = "txttipocambiocoddet";
            this.txttipocambiocoddet.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocoddet.TabIndex = 17;
            this.txttipocambiocoddet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocoddet_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(249, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "T.C.";
            // 
            // txtmonedadescdet
            // 
            this.txtmonedadescdet.Enabled = false;
            this.txtmonedadescdet.EnterMoveNextControl = true;
            this.txtmonedadescdet.Location = new System.Drawing.Point(96, 70);
            this.txtmonedadescdet.Name = "txtmonedadescdet";
            this.txtmonedadescdet.Size = new System.Drawing.Size(147, 20);
            this.txtmonedadescdet.TabIndex = 15;
            // 
            // txtmonedacoddet
            // 
            this.txtmonedacoddet.EnterMoveNextControl = true;
            this.txtmonedacoddet.Location = new System.Drawing.Point(56, 71);
            this.txtmonedacoddet.Name = "txtmonedacoddet";
            this.txtmonedacoddet.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacoddet.TabIndex = 14;
            this.txtmonedacoddet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacoddet_KeyDown);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(12, 72);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(42, 13);
            this.labelControl15.TabIndex = 13;
            this.labelControl15.Text = "Moneda:";
            // 
            // txtfechadocdet
            // 
            this.txtfechadocdet.EnterMoveNextControl = true;
            this.txtfechadocdet.Location = new System.Drawing.Point(626, 49);
            this.txtfechadocdet.Name = "txtfechadocdet";
            this.txtfechadocdet.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadocdet.Properties.Mask.BeepOnError = true;
            this.txtfechadocdet.Properties.Mask.EditMask = "d";
            this.txtfechadocdet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadocdet.Properties.Mask.SaveLiteral = false;
            this.txtfechadocdet.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadocdet.Properties.MaxLength = 10;
            this.txtfechadocdet.Size = new System.Drawing.Size(92, 20);
            this.txtfechadocdet.TabIndex = 12;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(562, 52);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(58, 13);
            this.labelControl14.TabIndex = 11;
            this.labelControl14.Text = "Fecha Doc.:";
            // 
            // txtseriedet
            // 
            this.txtseriedet.EnterMoveNextControl = true;
            this.txtseriedet.Location = new System.Drawing.Point(319, 49);
            this.txtseriedet.Name = "txtseriedet";
            this.txtseriedet.Size = new System.Drawing.Size(206, 20);
            this.txtseriedet.TabIndex = 8;
            this.txtseriedet.Leave += new System.EventHandler(this.txtseriedet_Leave);
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(287, 54);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(28, 13);
            this.labelControl12.TabIndex = 7;
            this.labelControl12.Text = "Serie:";
            // 
            // txttipodocdescdet
            // 
            this.txttipodocdescdet.Enabled = false;
            this.txttipodocdescdet.EnterMoveNextControl = true;
            this.txttipodocdescdet.Location = new System.Drawing.Point(96, 49);
            this.txttipodocdescdet.Name = "txttipodocdescdet";
            this.txttipodocdescdet.Size = new System.Drawing.Size(185, 20);
            this.txttipodocdescdet.TabIndex = 6;
            // 
            // txttipodocdet
            // 
            this.txttipodocdet.EnterMoveNextControl = true;
            this.txttipodocdet.Location = new System.Drawing.Point(56, 49);
            this.txttipodocdet.Name = "txttipodocdet";
            this.txttipodocdet.Size = new System.Drawing.Size(39, 20);
            this.txttipodocdet.TabIndex = 5;
            this.txttipodocdet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodocdet_KeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(19, 52);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(36, 13);
            this.labelControl11.TabIndex = 4;
            this.labelControl11.Text = "T.Doc.:";
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(128, 26);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Size = new System.Drawing.Size(591, 20);
            this.txtcuentadesc.TabIndex = 3;
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(56, 26);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Size = new System.Drawing.Size(70, 20);
            this.txtcuenta.TabIndex = 2;
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cuenta:";
            // 
            // frm_cierre_cajachica_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(975, 645);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_cierre_cajachica_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cierre";
            this.Load += new System.EventHandler(this.frm_cierre_cajachica_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiferencias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifRedondeo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifCambio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcierre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteRendicion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcajadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcaja.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgresodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgreso.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalImporteMNDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMNDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidaddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdnidet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentdescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentcoddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMEDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalordet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocoddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacoddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadocdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtseriedet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem bntcerrar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit TxtDiferencias;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label Label25;
        private DevExpress.XtraEditors.TextEdit TxtDifRedondeo;
        private DevExpress.XtraEditors.TextEdit TxtDifCambio;
        private System.Windows.Forms.CheckBox ChkDifRedondeo;
        private System.Windows.Forms.CheckBox ChkDifCambio;
        private DevExpress.XtraEditors.TextEdit txtcierre;
        private DevExpress.XtraEditors.TextEdit txtimporteRendicion;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtcajadesc;
        private DevExpress.XtraEditors.TextEdit txtcaja;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txttipomovimientodesc;
        private DevExpress.XtraEditors.TextEdit txttipomovimientocod;
        private DevExpress.XtraEditors.TextEdit txttipoEgresodesc;
        private DevExpress.XtraEditors.TextEdit txttipoEgreso;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtimporte;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtentidad;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtnumero;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit txtTotalImporteMNDet;
        private DevExpress.XtraEditors.TextEdit txtimporteMNDet;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.SimpleButton btnanadirdet;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.SimpleButton btnquitardet;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraEditors.TextEdit txtentidaddet;
        private DevExpress.XtraEditors.TextEdit txtrucdnidet;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txttipoentdescdet;
        private DevExpress.XtraEditors.TextEdit txttipoentcoddet;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtimporteMEDet;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.TextEdit txttipo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalordet;
        private DevExpress.XtraEditors.TextEdit txttipocambiodescdet;
        private DevExpress.XtraEditors.TextEdit txttipocambiocoddet;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtmonedadescdet;
        private DevExpress.XtraEditors.TextEdit txtmonedacoddet;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtfechadocdet;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtseriedet;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txttipodocdescdet;
        private DevExpress.XtraEditors.TextEdit txttipodocdet;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.TextEdit txtcuenta;
        private System.Windows.Forms.Label label2;
    }
}
