﻿namespace Contable.Procesos.Tesoreria
{
    partial class frm_series_documentos_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtcajadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcaja = new DevExpress.XtraEditors.TextEdit();
            this.chkCajachica = new DevExpress.XtraEditors.CheckEdit();
            this.tctctacorriente = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.chkCuentaCorriente = new DevExpress.XtraEditors.CheckEdit();
            this.txtnumerofin = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtnumeroinicio = new DevExpress.XtraEditors.TextEdit();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txttipomovimientodesc = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txttipomovimientocod = new DevExpress.XtraEditors.TextEdit();
            this.txttipoEgresodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipoEgreso = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkrendicion = new DevExpress.XtraEditors.CheckEdit();
            this.chkreembolso = new DevExpress.XtraEditors.CheckEdit();
            this.chkcierre = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcajadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcaja.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCajachica.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tctctacorriente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCuentaCorriente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumerofin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumeroinicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgresodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgreso.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkrendicion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkreembolso.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcierre.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(641, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 235);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(641, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 203);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(641, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 203);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtcajadesc);
            this.groupControl1.Controls.Add(this.txtcaja);
            this.groupControl1.Controls.Add(this.chkcierre);
            this.groupControl1.Controls.Add(this.chkreembolso);
            this.groupControl1.Controls.Add(this.chkrendicion);
            this.groupControl1.Controls.Add(this.chkCajachica);
            this.groupControl1.Controls.Add(this.tctctacorriente);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.chkCuentaCorriente);
            this.groupControl1.Controls.Add(this.txtnumerofin);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtnumeroinicio);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txttipomovimientodesc);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.txttipomovimientocod);
            this.groupControl1.Controls.Add(this.txttipoEgresodesc);
            this.groupControl1.Controls.Add(this.txttipoEgreso);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(5, 36);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(624, 187);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Datos";
            // 
            // txtcajadesc
            // 
            this.txtcajadesc.Enabled = false;
            this.txtcajadesc.Location = new System.Drawing.Point(206, 126);
            this.txtcajadesc.MenuManager = this.barManager1;
            this.txtcajadesc.Name = "txtcajadesc";
            this.txtcajadesc.Size = new System.Drawing.Size(407, 20);
            this.txtcajadesc.TabIndex = 20;
            // 
            // txtcaja
            // 
            this.txtcaja.Enabled = false;
            this.txtcaja.Location = new System.Drawing.Point(153, 126);
            this.txtcaja.MenuManager = this.barManager1;
            this.txtcaja.Name = "txtcaja";
            this.txtcaja.Size = new System.Drawing.Size(51, 20);
            this.txtcaja.TabIndex = 19;
            this.txtcaja.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcaja_KeyDown);
            // 
            // chkCajachica
            // 
            this.chkCajachica.Location = new System.Drawing.Point(72, 124);
            this.chkCajachica.MenuManager = this.barManager1;
            this.chkCajachica.Name = "chkCajachica";
            this.chkCajachica.Properties.Caption = "Caja chica";
            this.chkCajachica.Size = new System.Drawing.Size(75, 19);
            this.chkCajachica.TabIndex = 18;
            this.chkCajachica.CheckedChanged += new System.EventHandler(this.chkCajachica_CheckedChanged);
            // 
            // tctctacorriente
            // 
            this.tctctacorriente.Enabled = false;
            this.tctctacorriente.Location = new System.Drawing.Point(267, 100);
            this.tctctacorriente.MenuManager = this.barManager1;
            this.tctctacorriente.Name = "tctctacorriente";
            this.tctctacorriente.Size = new System.Drawing.Size(346, 20);
            this.tctctacorriente.TabIndex = 17;
            this.tctctacorriente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tctctacorriente_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(178, 107);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(83, 13);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Cuenta Corriente";
            // 
            // chkCuentaCorriente
            // 
            this.chkCuentaCorriente.Location = new System.Drawing.Point(72, 104);
            this.chkCuentaCorriente.MenuManager = this.barManager1;
            this.chkCuentaCorriente.Name = "chkCuentaCorriente";
            this.chkCuentaCorriente.Properties.Caption = "Cuenta corriente";
            this.chkCuentaCorriente.Size = new System.Drawing.Size(100, 19);
            this.chkCuentaCorriente.TabIndex = 15;
            this.chkCuentaCorriente.CheckedChanged += new System.EventHandler(this.chkCuentaCorriente_CheckedChanged);
            // 
            // txtnumerofin
            // 
            this.txtnumerofin.EnterMoveNextControl = true;
            this.txtnumerofin.Location = new System.Drawing.Point(383, 78);
            this.txtnumerofin.MenuManager = this.barManager1;
            this.txtnumerofin.Name = "txtnumerofin";
            this.txtnumerofin.Size = new System.Drawing.Size(100, 20);
            this.txtnumerofin.TabIndex = 14;
            this.txtnumerofin.Leave += new System.EventHandler(this.txtnumerofin_Leave);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(323, 81);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(54, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Numero Fin";
            // 
            // txtnumeroinicio
            // 
            this.txtnumeroinicio.EnterMoveNextControl = true;
            this.txtnumeroinicio.Location = new System.Drawing.Point(231, 77);
            this.txtnumeroinicio.MenuManager = this.barManager1;
            this.txtnumeroinicio.Name = "txtnumeroinicio";
            this.txtnumeroinicio.Size = new System.Drawing.Size(86, 20);
            this.txtnumeroinicio.TabIndex = 12;
            this.txtnumeroinicio.Leave += new System.EventHandler(this.txtnumero_Leave);
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(72, 78);
            this.txtserie.MenuManager = this.barManager1;
            this.txtserie.Name = "txtserie";
            this.txtserie.Size = new System.Drawing.Size(84, 20);
            this.txtserie.TabIndex = 10;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(160, 81);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(65, 13);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Numero Inicio";
            // 
            // txttipomovimientodesc
            // 
            this.txttipomovimientodesc.Enabled = false;
            this.txttipomovimientodesc.EnterMoveNextControl = true;
            this.txttipomovimientodesc.Location = new System.Drawing.Point(382, 55);
            this.txttipomovimientodesc.MenuManager = this.barManager1;
            this.txttipomovimientodesc.Name = "txttipomovimientodesc";
            this.txttipomovimientodesc.Size = new System.Drawing.Size(231, 20);
            this.txttipomovimientodesc.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Serie";
            // 
            // txttipomovimientocod
            // 
            this.txttipomovimientocod.EnterMoveNextControl = true;
            this.txttipomovimientocod.Location = new System.Drawing.Point(333, 55);
            this.txttipomovimientocod.MenuManager = this.barManager1;
            this.txttipomovimientocod.Name = "txttipomovimientocod";
            this.txttipomovimientocod.Size = new System.Drawing.Size(46, 20);
            this.txttipomovimientocod.TabIndex = 7;
            this.txttipomovimientocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipomovimientocod_KeyDown);
            // 
            // txttipoEgresodesc
            // 
            this.txttipoEgresodesc.Enabled = false;
            this.txttipoEgresodesc.EnterMoveNextControl = true;
            this.txttipoEgresodesc.Location = new System.Drawing.Point(114, 55);
            this.txttipoEgresodesc.MenuManager = this.barManager1;
            this.txttipoEgresodesc.Name = "txttipoEgresodesc";
            this.txttipoEgresodesc.Size = new System.Drawing.Size(153, 20);
            this.txttipoEgresodesc.TabIndex = 5;
            // 
            // txttipoEgreso
            // 
            this.txttipoEgreso.EnterMoveNextControl = true;
            this.txttipoEgreso.Location = new System.Drawing.Point(72, 55);
            this.txttipoEgreso.MenuManager = this.barManager1;
            this.txttipoEgreso.Name = "txttipoEgreso";
            this.txttipoEgreso.Size = new System.Drawing.Size(39, 20);
            this.txttipoEgreso.TabIndex = 4;
            this.txttipoEgreso.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoEgreso_KeyDown);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(113, 33);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(500, 20);
            this.txttipodocdesc.TabIndex = 2;
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(72, 33);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 1;
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(273, 58);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Movimiento";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(46, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(20, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Tipo";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(45, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo Doc.";
            // 
            // chkrendicion
            // 
            this.chkrendicion.Location = new System.Drawing.Point(72, 149);
            this.chkrendicion.Name = "chkrendicion";
            this.chkrendicion.Properties.Caption = "Rendicion de caja chica";
            this.chkrendicion.Size = new System.Drawing.Size(132, 19);
            this.chkrendicion.TabIndex = 18;
            this.chkrendicion.CheckedChanged += new System.EventHandler(this.chkCajachica_CheckedChanged);
            // 
            // chkreembolso
            // 
            this.chkreembolso.Location = new System.Drawing.Point(206, 149);
            this.chkreembolso.Name = "chkreembolso";
            this.chkreembolso.Properties.Caption = "Reembolso de caja chica";
            this.chkreembolso.Size = new System.Drawing.Size(143, 19);
            this.chkreembolso.TabIndex = 18;
            this.chkreembolso.CheckedChanged += new System.EventHandler(this.chkCajachica_CheckedChanged);
            // 
            // chkcierre
            // 
            this.chkcierre.Location = new System.Drawing.Point(355, 149);
            this.chkcierre.Name = "chkcierre";
            this.chkcierre.Properties.Caption = "Cierre de caja chica";
            this.chkcierre.Size = new System.Drawing.Size(122, 19);
            this.chkcierre.TabIndex = 18;
            this.chkcierre.CheckedChanged += new System.EventHandler(this.chkCajachica_CheckedChanged);
            // 
            // frm_series_documentos_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(641, 235);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_series_documentos_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Series de documentos";
            this.Load += new System.EventHandler(this.frm_series_documentos_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcajadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcaja.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCajachica.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tctctacorriente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCuentaCorriente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumerofin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumeroinicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgresodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoEgreso.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkrendicion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkreembolso.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcierre.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtnumeroinicio;
        public DevExpress.XtraEditors.TextEdit txttipomovimientodesc;
        public DevExpress.XtraEditors.TextEdit txttipomovimientocod;
        public DevExpress.XtraEditors.TextEdit txttipoEgresodesc;
        public DevExpress.XtraEditors.TextEdit txttipoEgreso;
        public DevExpress.XtraEditors.TextEdit txttipodocdesc;
        public DevExpress.XtraEditors.TextEdit txttipodoc;
        public DevExpress.XtraEditors.TextEdit txtserie;
        public DevExpress.XtraEditors.TextEdit txtnumerofin;
        private DevExpress.XtraEditors.LabelControl labelControl5;

        private DevExpress.XtraEditors.TextEdit tctctacorriente;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit chkCuentaCorriente;
        private DevExpress.XtraEditors.TextEdit txtcajadesc;
        private DevExpress.XtraEditors.TextEdit txtcaja;
        private DevExpress.XtraEditors.CheckEdit chkCajachica;
        private DevExpress.XtraEditors.CheckEdit chkrendicion;
        private DevExpress.XtraEditors.CheckEdit chkcierre;
        private DevExpress.XtraEditors.CheckEdit chkreembolso;
    }
}
