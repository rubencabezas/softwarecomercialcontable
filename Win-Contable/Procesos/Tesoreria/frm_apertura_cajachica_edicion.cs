﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_apertura_cajachica_edicion : Contable.frm_fuente
    {
        public frm_apertura_cajachica_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher, Tipo_IE;

        public string Ctb_Caja_CHica_Estado;
        public string Ctb_Caja_CHica_Estado_Descripcion;

        void LimpiarCab()
        {
            txttipomovimientocod.ResetText();
            txttipomovimientodesc.ResetText();
            txtglosa.ResetText();
             txtfechadoc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();
            txttipocambiovalor.ResetText();
            txtimporte.ResetText();
            txtimporteMN.ResetText();

            ChkDifCambio.Checked = false;
            ChkDifRedondeo.Checked = false;
            TxtDifCambio.Text = "0.00";
            TxtDifRedondeo.Text = "0.00";
            TxtDiferencias.Text = "0.00";

            cuenta_caja_chica = "";
            cuenta_caja_chica_Desc = "";

        }

        void LimpiarDet()
        {
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();

            txttipodocdet.ResetText();
            txttipodocdescdet.ResetText();

            txtseriedet.ResetText();
            //txtnumerodet.ResetText();

            txtfechadocdet.ResetText();


            txtmonedacoddet.ResetText();
            txtmonedadescdet.ResetText();

            txttipocambiocoddet.ResetText();
            txttipocambiodescdet.ResetText();
            txttipocambiovalordet.ResetText();

            txttipoentcoddet.ResetText();
            txttipoentdescdet.ResetText();
            txtrucdnidet.ResetText();
            txtentidaddet.ResetText();


            txttipo.ResetText();
            txttipodesc.ResetText();
            txtimporteMNDet.ResetText();
            txtimporteMEDet.ResetText();

            Es_Cuenta_Principal = false;
        }


        void BloquearDetalles()
        {
            txtcuenta.Enabled = false;

            txttipodocdet.Enabled = false;

            txtseriedet.Enabled = false;
            //txtnumerodet.Enabled = false;

            txtfechadocdet.Enabled = false;


            txtmonedacoddet.Enabled = false;

            txttipocambiocoddet.Enabled = false;
            txttipocambiovalordet.Enabled = false;

            txttipoentcoddet.Enabled = false;
            txtrucdnidet.Enabled = false;


            txttipo.Enabled = false;
            txtimporteMNDet.Enabled = false;
            txtimporteMEDet.Enabled = false;

        }

        void BloquearDiferencias_Inicio()
        {

            TxtDifCambio.Enabled = false;
            TxtDifRedondeo.Enabled = false;
        }

        void HabilitarDetalles()
        {
            txtcuenta.Enabled = true;

            txttipodocdet.Enabled = true;

            txtseriedet.Enabled = true;
            //txtnumerodet.Enabled = true;

            txtfechadocdet.Enabled = true;


            txtmonedacoddet.Enabled = true;

            txttipocambiocoddet.Enabled = true;
            txttipocambiovalordet.Enabled = true;

            //txttipoentcoddet.Enabled = true;
            txtrucdnidet.Enabled = true;


            txttipo.Enabled = true;
            txtimporteMNDet.Enabled = true;
            txtimporteMEDet.Enabled = true;

        }

        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                LimpiarDet();
                Detalles.Clear();

                dgvdatos.DataSource = null;
                BloquearDetalles();
                TraerLibro();

                txtglosa.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;


            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                Id_Item = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }


        private void txttipomovimientocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipomovimientocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipomovimientocod.Text.Substring(txttipomovimientocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0016";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipomovimientocod.Tag = Entidad.Id_General_Det;
                                txttipomovimientocod.Text = Entidad.Gen_Codigo_Interno;
                                txttipomovimientodesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipomovimientocod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipomovimientocod.Text) & string.IsNullOrEmpty(txttipomovimientodesc.Text))
                    {
                        BuscarMovimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using ( frm_entidades_busqueda f = new  frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdni.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdni.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdni.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdni.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdni.EnterMoveNextControl = false;
                                txtrucdni.ResetText();
                                txtrucdni.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Tag = Entidad.Id_Moneda;

                                txtmonedacod.Text = Entidad.Id_Sunat;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                                VerificarMoneda();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        Boolean Es_moneda_nac;
        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1.000";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocod.Text))
                    {
                        txttipocambiocod.Enabled = true;
                        txttipocambiocod.ResetText();
                        txttipocambiodesc.ResetText();
                        txttipocambiovalor.ResetText();
                    }

                }
            }
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocod.Text) == false)
            {
                if (IsDate(txtfechadoc.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocod.Text.Substring(txttipocambiocod.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadoc.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocod.Text = Entidad.Codigo;
                                    txttipocambiodesc.Text = Entidad.Nombre;
                                    txttipocambiovalor.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocod.Text == "OTR")
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }

                                    txttipocambiocod.EnterMoveNextControl = true;

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocod.Text) & string.IsNullOrEmpty(txttipocambiodesc.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadoc.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocod.Text = T.Codigo;
                                            txttipocambiodesc.Text = T.Nombre;
                                            txttipocambiovalor.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocod.Text == "OTR")
                                            {
                                                txttipocambiovalor.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalor.Enabled = false;
                                            }
                                            txttipocambiocod.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadoc.Focus();

                }

            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuenta.EnterMoveNextControl = false;
                    txtcuenta.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodocdet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodocdet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodocdet.Text.Substring(txttipodocdet.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodocdet.Text = Entidad.Id_SUnat;
                                txttipodocdet.Tag = Entidad.Id_Comprobante;
                                txttipodocdescdet.Text = Entidad.Nombre_Comprobante;
                                txttipodocdet.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodocdet.Text) & string.IsNullOrEmpty(txttipodocdescdet.Text))
                    {
                        BuscarTipoDocumentoDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumentoDet()
        {
            try
            {
                txttipodocdet.Text = Accion.Formato(txttipodocdet.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodocdet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodocdet.Text.Trim().ToUpper())
                        {
                            txttipodocdet.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodocdet.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdescdet.Text = T.Nombre_Comprobante;
                            txttipodocdet.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodocdet.EnterMoveNextControl = false;
                    txttipodocdet.ResetText();
                    txttipodocdet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacoddet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacoddet.Text.Substring(txtmonedacoddet.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nacDet = Entidad.Es_nacional;
                                txtmonedacoddet.Tag = Entidad.Id_Sunat;
                                txtmonedacoddet.Text = Entidad.Id_Moneda;
                                txtmonedadescdet.Text = Entidad.Nombre_Moneda;
                                VerificarMonedaDet();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacoddet.Text) & string.IsNullOrEmpty(txtmonedadescdet.Text))
                    {
                        BuscarMonedaDet();
                        VerificarMonedaDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        Boolean Es_moneda_nacDet;
        public void BuscarMonedaDet()
        {
            try
            {
                txtmonedacoddet.Text = Accion.Formato(txtmonedacoddet.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacoddet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacoddet.Text.Trim().ToUpper())
                        {
                            Es_moneda_nacDet = T.Es_nacional;
                            txtmonedacoddet.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacoddet.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadescdet.Text = T.Nombre_Moneda;
                            txtmonedacoddet.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacoddet.EnterMoveNextControl = false;
                    txtmonedacoddet.ResetText();
                    txtmonedacoddet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void VerificarMonedaDet()
        {
            if (!string.IsNullOrEmpty(txtmonedacoddet.Text.Trim()))
            {

                if (Es_moneda_nacDet == true)
                {
                    txttipocambiocoddet.Enabled = false;
                    txttipocambiocoddet.Text = "SCV";
                    txttipocambiodescdet.Text = "SIN CONVERSION";
                    txttipocambiovalordet.Enabled = false;
                    txttipocambiovalordet.Text = "1.000";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocoddet.Text))
                    {
                        txttipocambiocoddet.Enabled = true;
                        txttipocambiocoddet.ResetText();
                        txttipocambiodescdet.ResetText();
                        txttipocambiovalordet.ResetText();
                    }

                }
            }
        }

        private void txttipocambiocoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocoddet.Text) == false)
            {
                if (IsDate(txtfechadocdet.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocoddet.Text.Substring(txttipocambiocoddet.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadocdet.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocoddet.Text = Entidad.Codigo;
                                    txttipocambiodescdet.Text = Entidad.Nombre;
                                    txttipocambiovalordet.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocoddet.Text == "OTR")
                                    {
                                        txttipocambiovalordet.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalordet.Enabled = true;
                                    }

                                    txttipocambiocoddet.EnterMoveNextControl = true;

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocoddet.Text) & string.IsNullOrEmpty(txttipocambiodescdet.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadocdet.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocoddet.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocoddet.Text = T.Codigo;
                                            txttipocambiodescdet.Text = T.Nombre;
                                            txttipocambiovalordet.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocoddet.Text == "OTR")
                                            {
                                                txttipocambiovalordet.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalordet.Enabled = false;
                                            }
                                            txttipocambiocoddet.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadocdet.Focus();

                }

            }
        }

        private void txttipoentcoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipoentcoddet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoentcoddet.Text.Substring(txttipoentcoddet.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_entidad_busqueda f = new _1_Busquedas_Generales.frm_tipo_entidad_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoentcoddet.Text = Entidad.Id_Tipo_Ent;
                                txttipoentdescdet.Text = Entidad.Ent_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoentcoddet.Text) & string.IsNullOrEmpty(txttipoentdescdet.Text))
                    {
                        BuscarTipoEntidadDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTipoEntidadDet()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar_tipo_entidad(new Entidad_Entidad
                {
                    Id_Tipo_Ent = "T"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Id_Tipo_Ent).ToString().Trim().ToUpper() == "T")
                        {
                            txttipoentcoddet.Text = (T.Id_Tipo_Ent).ToString().Trim();
                            txttipoentdescdet.Text = T.Ent_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtrucdnidet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdnidet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdnidet.Text.Substring(txtrucdnidet.Text.Length - 1, 1) == "*")
                    {
                        using ( frm_entidades_busqueda f = new  frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdnidet.Text = Entidad.Ent_RUC_DNI;
                                txtentidaddet.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdnidet.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdnidet.Text) & string.IsNullOrEmpty(txtentidaddet.Text))
                    {
                        BuscarEntidadDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEntidadDet()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdnidet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdnidet.Text.Trim().ToUpper())
                        {
                            txtrucdnidet.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidaddet.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdnidet.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdnidet.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdnidet.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdnidet.Text.Trim().ToUpper())
                                    {
                                        txtrucdnidet.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidaddet.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdnidet.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdnidet.EnterMoveNextControl = false;
                                txtrucdnidet.ResetText();
                                txtrucdnidet.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipo_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipo.Text.Substring(txttipo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0002";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipo.Tag = Entidad.Id_General_Det;
                                txttipo.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipo.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipo.Text) & string.IsNullOrEmpty(txttipodesc.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipo()
        {
            try
            {
                txttipo.Text = Accion.Formato(txttipo.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0002",
                    Gen_Codigo_Interno = txttipo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipo.Text.Trim().ToUpper())
                        {
                            txttipo.Tag = T.Id_General_Det;
                            txttipo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesc.Text = T.Gen_Descripcion_Det;
                            txttipo.EnterMoveNextControl = true;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipo.EnterMoveNextControl = false;
                    txttipo.ResetText();
                    txttipo.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();
        int Id_Item;
        decimal ImporteNoMay;

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            if (VerificarNuevoDet())
            {
                EstadoDetalle = Estados.Nuevo;

                BuscarTipoEntidadDet();

                HabilitarDetalles();

                Armar_Detalle_Primer_Item();
                MostrarDiferencias();

            }
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            //if (Es_Cuenta_Principal == true)
            //{
            //    Accion.Advertencia("No puede editar esta cuenta,debe Quitar este registro y modificar la cabecera para qeu agrege al detalle.");
            //    BloquearDetalles();
            //}
            //else {
            //    EstadoDetalle = Estados.Modificar;
            //}

            EstadoDetalle = Estados.Modificar;
            if (Es_Cuenta_Principal == true)
            {
                //btnquitardet.Enabled = true;
                //btnnuevodet.Enabled = false;
                //btnanadirdet.Enabled = false;
                Accion.Advertencia("No puede editar esta cuenta,debe Quitar este registro y modificar la cabecera para qeu agrege al detalle.");
                BloquearDetalles();
            }

        }

        Boolean Es_Cuenta_Principal=false;
        public void Armar_Detalle_Primer_Item()
        {
            try
            {
                //PRIMERO VERIFICAMOS SI EXISTE UNA CUENTA PRINCIPAL

                foreach (Entidad_Movimiento_Cab T in Detalles)
                {
                    if ((T.Ctb_Es_Cuenta_Principal) == true)
                    {
                        Es_Cuenta_Principal = true;
                       
                        break;
                    }
                   
                }

                if (Es_Cuenta_Principal == false)
                {

                    if (VerificarDetalle01())
                    {
                        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                        Entidad.Id_Item = Id_Item;


                        Entidad.Ctb_Cuenta = cuenta_caja_chica;
                        Entidad.Ctb_Cuenta_Desc = cuenta_caja_chica_Desc;

                        Entidad.Ctb_Ruc_dni_det = txtrucdni.Text;
                        Entidad.Entidad_det = txtentidad.Text;

                        Entidad.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadoc.Text);//String.Format("{0:yyyy-MM-dd}",);

                        Entidad.Ctb_moneda_cod_det_Sunat = txtmonedacod.Text;
                        Entidad.Ctb_moneda_cod_det = txtmonedacod.Tag.ToString();
                        Entidad.Ctb_moneda_det_desc = txtmonedadesc.Text;

                        Entidad.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocod.Text;
                        Entidad.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodesc.Text;
                        Entidad.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text);// Convert.ToDecimal().ToString();

                        Entidad.Ctb_Es_Cuenta_Principal = true;


                        Entidad.Ctb_Tipo_DH = "0004";
                        Entidad.CCtb_Tipo_DH_Interno = "01";
                        Entidad.Ctb_Tipo_DH_Desc = "DEBE";


                        if ((txtimporte.Text == "") || (txtimporteMN.Text == ""))
                        {
                            txtimporte.Text = IIf(txtimporte.Text == "", "0.00", txtimporte.Text).ToString();
                            txtimporteMN.Text = IIf(txtimporteMN.Text == "", "0.00", txtimporteMN.Text).ToString();
                        }

                        Entidad.Ctb_Importe_Debe = Convert.ToDecimal(txtimporte.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                        Entidad.Ctb_Importe_Debe_Extr = 0;



                        Detalles.Add(Entidad);
                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;
                     
                        btnnuevodet.Focus();
                    }
                }
                Es_Cuenta_Principal = false;
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
        private void btnanadirdet_Click(object sender, EventArgs e)
        {

            try
            {
                if (VerificarDetalle())
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    Entidad.Id_Item = Id_Item;


                    Entidad.Ctb_Cuenta = txtcuenta.Text;
                    Entidad.Ctb_Cuenta_Desc = txtcuentadesc.Text;

                    Entidad.Ctb_Tipo_Doc_det_Sunat = txttipodocdet.Text;
                    Entidad.Ctb_Tipo_Doc_det = txttipodocdet.Tag.ToString();
                    Entidad.Ctb_Tipo_Doc_det_desc = txttipodocdescdet.Text;

                    //Entidad.Ctb_Serie_det = txtseriedet.Text;
                    //Entidad.Ctb_Numero_det = txtnumerodet.Text;


                    string SerNum = txtseriedet.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Entidad.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Entidad.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();




                    Entidad.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadocdet.Text);//String.Format("{0:yyyy-MM-dd}",);

                    Entidad.Ctb_moneda_cod_det_Sunat = txtmonedacoddet.Text;
                    Entidad.Ctb_moneda_cod_det = txtmonedacoddet.Tag.ToString();
                    Entidad.Ctb_moneda_det_desc = txtmonedadescdet.Text;
                    Entidad.Ctb_Es_moneda_nac_det = Es_moneda_nacDet;

                    Entidad.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocoddet.Text;
                    Entidad.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodescdet.Text;
                    Entidad.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalordet.Text);// Convert.ToDecimal().ToString();

                    Entidad.Ctb_Tipo_Ent_det = txttipoentcoddet.Text;
                    Entidad.Ctb_Tipo_Ent_det_Desc = txttipoentdescdet.Text;
                    Entidad.Ctb_Ruc_dni_det = txtrucdnidet.Text;
                    Entidad.Entidad_det = txtentidaddet.Text;


                    Entidad.Ctb_Tipo_DH = txttipo.Tag.ToString();
                    Entidad.CCtb_Tipo_DH_Interno = txttipo.Text;
                    Entidad.Ctb_Tipo_DH_Desc = txttipodesc.Text;

                    Entidad.Ctb_Es_Cuenta_Principal = Es_Cuenta_Principal;


                    //Entidad.Id_Anio_Ref = Anio_Ref;
                    //Entidad.Id_Periodo_Ref = Periodo_Ref;
                    //Entidad.Id_Libro_Ref = Libro_Ref;
                    //Entidad.Id_Voucher_Ref = Voucher_Ref;
                    //Entidad.Id_Item_Ref = Item_Ref;

                    if (Entidad.Ctb_Tipo_DH == "0004")
                    {
                        if ((txtimporteMEDet.Text == "") || (txtimporteMNDet.Text == ""))
                        {
                            txtimporteMEDet.Text = IIf(txtimporteMEDet.Text == "", "0.00", txtimporteMEDet.Text).ToString();
                            txtimporteMNDet.Text = IIf(txtimporteMNDet.Text == "", "0.00", txtimporteMNDet.Text).ToString();
                        }
                        Entidad.Ctb_Importe_Debe = Convert.ToDecimal(txtimporteMNDet.Text) * Convert.ToDecimal(txttipocambiovalordet.Text);
                        Entidad.Ctb_Importe_Debe_Extr = !Es_moneda_nacDet ? Convert.ToDecimal(txtimporteMEDet.Text.ToString()) : Convert.ToDecimal("0.000");// Convert.ToDecimal(txtimporteMEDet.Text.ToString());
                    }
                    else if (Entidad.Ctb_Tipo_DH == "0005")
                    {
                        if ((txtimporteMEDet.Text == "") || (txtimporteMNDet.Text == ""))
                        {
                            txtimporteMEDet.Text = IIf(txtimporteMEDet.Text == "", "0.00", txtimporteMEDet.Text).ToString();
                            txtimporteMNDet.Text = IIf(txtimporteMNDet.Text == "", "0.00", txtimporteMNDet.Text).ToString();
                        }
                        Entidad.Ctb_Importe_Haber = Convert.ToDecimal(txtimporteMNDet.Text) * Convert.ToDecimal(txttipocambiovalordet.Text);
                        Entidad.Ctb_Importe_Haber_Extr = !Es_moneda_nacDet ? Convert.ToDecimal(txtimporteMNDet.Text.ToString()) : Convert.ToDecimal("0.000"); // Convert.ToDecimal(txtimporteMEDet.Text.ToString());
                    }

                    if (EstadoDetalle == Estados.Nuevo)
                    {
                        Detalles.Add(Entidad);


                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;
                    }
                    else if (EstadoDetalle == Estados.Modificar)
                    {

                        Detalles[Id_Item - 1] = Entidad;

                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;

                    }

                    MostrarDiferencias();

                    btnnuevodet.Focus();
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdatos.DataSource = Detalles;
            }
        }



        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        public bool VerificarDetalle()
        {
            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una cuenta contable");
                txtcuenta.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipodocdescdet.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodocdet.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipodesc.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un tipo (Debe/Haber)");
                txttipo.Focus();
                return false;
            }

            if (Convert.ToDouble(txtimporteMNDet.Text) <= 0)
            {
                //tools.ShowWarnig("VALOR INCORRECTO EN CANTIDAD", "Debe de ingresar un valor numérico mayor a CERO en el campo cantidad antes de grabar");
                Accion.Advertencia("Debe ingresar un importe");
                txtimporteMNDet.Focus();
                return false;
            }

            //if (Convert.ToDecimal(txtimporteMNDet.Text) > ImporteNoMay)
            //{
            //    Accion.Advertencia("No puede modificar el importe del documento por otro mayor a " + " " + ImporteNoMay);
            //    txtimporteMNDet.Focus();
            //    return false;
            //}


            return true;
        }


        public bool VerificarDetalle01()
        {
            if (string.IsNullOrEmpty(txtcajadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe seleccionar una caja chica");
                txtcaja.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtentidad.Text))
            {
           
                Accion.Advertencia("Debe ingresar un responsable");
                txtrucdni.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text))
            {
                Accion.Advertencia("Debe seleccionar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (Convert.ToDouble(txtimporte.Text) <= 0)
            {
                Accion.Advertencia("Debe ingresar un importe");
                txtimporte.Focus();
                return false;
            }

  

            return true;
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {


                    Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());

                    dgvdatos.DataSource = null;
                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Movimiento_Cab Det in Detalles)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtnumerodet_Leave(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtnumerodet.Text))
            //{
            //    txtnumerodet.Text = Accion.Formato(txtnumerodet.Text, 8);
            //}
        }

        private void txtseriedet_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtseriedet.Text))
                {
                    string Serie_Numero = txtseriedet.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtseriedet.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtseriedet.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        //txtserie.Text = Accion.Formato(txtserie.Text, 4);
                        txtseriedet.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtseriedet.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    Id_Item = Entidad.Id_Item;


                    txtcuenta.Text = Entidad.Ctb_Cuenta;
                    txtcuentadesc.Text = Entidad.Ctb_Cuenta_Desc;

                    txttipodocdet.Text = Entidad.Ctb_Tipo_Doc_det_Sunat;
                    txttipodocdet.Tag = Entidad.Ctb_Tipo_Doc_det;
                    txttipodocdescdet.Text = Entidad.Ctb_Tipo_Doc_det_desc;

                    txtseriedet.Text = Entidad.Ctb_Serie_det +"-"+ Entidad.Ctb_Numero_det;
                    //txtnumerodet.Text = Entidad.Ctb_Numero_det;


                    txtfechadocdet.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Ctb_Fecha_Mov_det);


                    txtmonedacoddet.Text = Entidad.Ctb_moneda_cod_det_Sunat;
                    txtmonedacoddet.Tag = Entidad.Ctb_moneda_cod_det;
                    txtmonedadescdet.Text = Entidad.Ctb_moneda_det_desc;

                    txttipocambiocoddet.Text = Entidad.Ctb_Tipo_Cambio_Cod_Det;
                    txttipocambiodescdet.Text = Entidad.Ctb_Tipo_Cambio_Desc_Det;
                    txttipocambiovalordet.Text = Convert.ToDecimal(Entidad.Ctb_Tipo_Cambio_Valor_Det).ToString();

                    txttipoentcoddet.Text = Entidad.Ctb_Tipo_Ent_det;
                    txttipoentdescdet.Text = Entidad.Ctb_Tipo_Ent_det_Desc;
                    txtrucdnidet.Text = Entidad.Ctb_Ruc_dni_det;
                    txtentidaddet.Text = Entidad.Entidad_det;


                    txttipo.Tag = Entidad.Ctb_Tipo_DH;
                    txttipo.Text = Entidad.CCtb_Tipo_DH_Interno;
                    txttipodesc.Text = Entidad.Ctb_Tipo_DH_Desc;

                    Es_Cuenta_Principal = Entidad.Ctb_Es_Cuenta_Principal;

                    if (Entidad.Ctb_Tipo_DH == "0004")
                    {
                        txtimporteMNDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe).ToString();
                        txtimporteMEDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe_Extr).ToString();
                        ImporteNoMay = Entidad.Ctb_Importe_Debe;
                    }
                    else if (Entidad.Ctb_Tipo_DH == "0005")
                    {
                        txtimporteMNDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber).ToString();
                        txtimporteMEDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber_Extr).ToString();
                        ImporteNoMay = Entidad.Ctb_Importe_Debe;
                    }

                    //Anio_Ref = Entidad.Id_Anio_Ref;
                    //Periodo_Ref = Entidad.Id_Periodo_Ref;
                    //Libro_Ref = Entidad.Id_Libro_Ref;
                    //Voucher_Ref = Entidad.Id_Voucher_Ref;
                    //Item_Ref = Entidad.Id_Item_Ref;

                   

                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle = Estados.Consulta;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle = Estados.SoloLectura;


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        string cuenta_caja_chica, cuenta_caja_chica_Desc;
        private void txtcaja_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtcaja.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcaja.Text.Substring(txtcaja.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_caja_chica_busqueda f = new _1_Busquedas_Generales.frm_caja_chica_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Caja_Chica_Config Entidad = new Entidad_Caja_Chica_Config();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcaja.Text = Entidad.Id_Caja;
                                txtcajadesc.Text = Entidad.Cja_Descripcion;

                                cuenta_caja_chica = Entidad.Cja_Cuenta;
                                cuenta_caja_chica_Desc = Entidad.Cja_Cuenta_Desc;

                                txtrucdni.Text = Entidad.Cja_Responsable;
                                txtentidad.Text = Entidad.Cja_Responsable_Desc;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcaja.Text) & string.IsNullOrEmpty(txtcajadesc.Text))
                    {
                        Caja();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void txtimporte_TextChanged(object sender, EventArgs e)
        {
            MostrarImporteMN();
        }

        void MostrarImporteMN()
        {
            try
            {
                decimal importe = Convert.ToDecimal(txtimporte.Text);
                decimal Tc = Convert.ToDecimal(txttipocambiovalor.Text);
                decimal Resultado;
                Resultado = importe * Tc;//(double.Parse(txtimporte.Text).ToString() * double.Parse(txttipocambiovalor.Text).ToString());
                txtimporteMN.Text = Resultado.ToString();
            }
            catch (Exception ex)
            {
                txtimporteMN.Text = "0.00";
            }

        }
        public void Caja()
        {
            try
            {
                Logica_Caja_Chica_Config log = new Logica_Caja_Chica_Config();

                List<Entidad_Caja_Chica_Config> Generales = new List<Entidad_Caja_Chica_Config>();

                txtcaja.Text = Accion.Formato(txtcaja.Text, 3);

                Generales = log.Listar(new Entidad_Caja_Chica_Config
                {
                    Id_Empresa=Actual_Conexion.CodigoEmpresa,
                    Id_Anio=Actual_Conexion.AnioSelect,
                    Id_Caja = txtcaja.Text,
                    Cja_Es_Caja_Chica=true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Caja_Chica_Config T in Generales)
                    {
                        if ((T.Id_Caja).ToString().Trim().ToUpper() == txtcaja.Text.Trim().ToUpper())
                        {
                            txtcaja.Text = (T.Id_Caja).ToString().Trim();
                            txtcajadesc.Text = T.Cja_Descripcion;

                            cuenta_caja_chica = T.Cja_Cuenta;
                            cuenta_caja_chica_Desc = T.Cja_Cuenta_Desc;

                            txtrucdni.Text = T.Cja_Responsable;
                            txtentidad.Text = T.Cja_Responsable_Desc;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcaja.EnterMoveNextControl = false;
                    txtcajadesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool VerificarCabecera()
        {

            if (string.IsNullOrEmpty(txtcajadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe seleccionar una caja chica");
                txtcaja.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtentidad.Text))
            {

                Accion.Advertencia("Debe ingresar un responsable");
                txtrucdni.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text))
            {
                Accion.Advertencia("Debe seleccionar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (Convert.ToDouble(txtimporte.Text) <= 0)
            {
                Accion.Advertencia("Debe ingresar un importe");
                txtimporte.Focus();
                return false;
            }

            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

             if (Convert.ToDecimal(TxtDiferencias.Text) != 0)
            {
                Accion.Advertencia("Verificacionn de Datos,No debe existir diferencias entre los documentos");
                txtimporte.Focus();
                return  false;
            }

            if (Math.Round(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Debe"].SummaryItem.SummaryValue), 2) != Math.Round(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Haber"].SummaryItem.SummaryValue), 2))
            {
                Accion.Advertencia("Los Montos Totales del Debe y el Haber no son iguales");
                return false;
            }

            return true;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Id_Periodo;
                    Ent.Id_Libro = txtlibro.Tag.ToString();
                    Ent.Id_Voucher = Voucher;

                    Ent.Ctb_Tipo_IE = txttipoEgreso.Tag.ToString();
                    Ent.Ctb_Tipo_Movimiento = txttipomovimientocod.Tag.ToString();

                    Ent.Ctb_Codigo_Caja_Chica = txtcaja.Text.ToString();
                    Ent.Ctb_Tipo_Ent = "T";
                    Ent.Ctb_Ruc_dni = txtrucdni.Text;
                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;
                    Ent.Ctb_Es_moneda_nac = Es_moneda_nac;
                    Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                    Ent.Ctb_Glosa = txtglosa.Text;
            

                    Ent.Ctb_Importe = Convert.ToDecimal(txtimporte.Text);

                    Ent.Ctb_Caja_CHica_Estado = "0053";
                    Ent.Ctb_Es_Caja_Chica = true;

                    Ent.Ctb_Es_Dif_Cambio = ChkDifCambio.Checked;
                    Ent.Ctb_Importe_Dif_Cambio = Convert.ToDecimal(IIf(TxtDifCambio.Text == "", "0.00", TxtDifCambio.Text).ToString()); 
                    Ent.Ctb_Es_Redondeo = ChkDifRedondeo.Checked;
                    Ent.Ctb_Importe_Redondeo = Convert.ToDecimal(IIf(TxtDifRedondeo.Text == "", "0.00", TxtDifRedondeo.Text).ToString()); 
                    Ent.Ctb_Importe_Diferencias = Convert.ToDecimal(IIf(TxtDiferencias.Text == "", "0.00", TxtDiferencias.Text).ToString());

                    

                    //detalle Contable
                    Ent.DetalleAsiento = Detalles;



                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar_Caja_Chica(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();

                            Detalles.Clear();

                            dgvdatos.DataSource = null;

                            BloquearDetalles();
                            TraerLibro();
                            txtglosa.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar_Caja_Chica(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public bool VerificarNuevoDet()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            return true;
        }

        public void BuscarMovimiento()
        {
            try
            {
                txttipomovimientocod.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0016",
                    Gen_Codigo_Interno = txttipomovimientocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipomovimientocod.Text.Trim().ToUpper())
                        {
                            txttipomovimientocod.Tag = T.Id_General_Det;
                            txttipomovimientocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipomovimientodesc.Text = T.Gen_Descripcion_Det;
                            txttipomovimientocod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipomovimientocod.EnterMoveNextControl = false;
                    txttipomovimientocod.ResetText();
                    txttipomovimientocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void ChkDifCambio_CheckedChanged(object sender, EventArgs e)
        {
            BloquearDiferencias();
        }
        void BloquearDiferencias()
        {
            if (ChkDifCambio.Checked == true)
            {
                TxtDifCambio.Enabled = true;
                TxtDifCambio.Focus();
            }
            else
            {
                TxtDifCambio.Enabled = false;
                TxtDifCambio.Text = "0.00";
            }
            if (ChkDifRedondeo.Checked == true)
            {
                TxtDifRedondeo.Enabled = true;
                TxtDifRedondeo.Focus();
            }
            else
            {
                TxtDifRedondeo.Enabled = false;
                TxtDifRedondeo.Text = "0.00";
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void ChkDifRedondeo_CheckedChanged(object sender, EventArgs e)
        {
            BloquearDiferencias();
        }

        private void TxtDifCambio_TextChanged(object sender, EventArgs e)
        {
            MostrarDiferencias();
        }
    //    Sub MostrarDiferencias()
    //    Try
    //        'Mostramos diferencias entre el reembolso y el documento sin provision
    //        Dim TotalDoc, TotalDetalle As Double
    //        TotalDoc = txtImporteMN.Text
    //        For Each Det As Ent_Tesoreria_Det In DetallesApertura
    //            TotalDetalle += Det.ImporteDocMN
    //        Next
    //        txtTotalImporteMNDet.Text = TotalDetalle
    //        TxtDiferencias.Text = TotalDoc - (TotalDetalle + CDbl(TxtDifCambio.Text) + CDbl(TxtDifRedondeo.Text))
    //    Catch ex As Exception
    //    End Try
    //End Sub
    void MostrarDiferencias()
        {
            //try
            //{
            //    decimal TotalDoc, TotalDetalle=0;
            //    TotalDoc  = Convert.ToDecimal(IIf(txtimporteMN.Text == "", "0.00", txtimporteMN.Text)); //Convert.ToDecimal(txtimporteMN.Text);

            //    foreach (Entidad_Movimiento_Cab T in Detalles)
            //    {
            //        if (T.Ctb_Es_Cuenta_Principal != true)
            //        {
            //            TotalDetalle += T.Ctb_Importe_Det_MN_Aper_Caja_chica;//T.Ctb_Importe_Debe;
            //        }

            //    }
            //    txtTotalImporteMNDet.Text = Convert.ToString(TotalDetalle);

            //    decimal DifCambio;//IIf(txtimporte.Text == "", "0.00", txtimporte.Text).ToString();
            //    DifCambio =Convert.ToDecimal(IIf(TxtDifCambio.Text == "", "0.00", TxtDifCambio.Text));

            //    decimal DifRedondeo;
            //    DifRedondeo = Convert.ToDecimal(IIf(TxtDifRedondeo.Text == "", "0.00", TxtDifRedondeo.Text));// Convert.ToDecimal(TxtDifRedondeo.Text);
            //    TxtDiferencias.Text = Convert.ToString( TotalDoc - (TotalDetalle + DifCambio + DifRedondeo));

            //}
            //catch (Exception ex)
            //{
            //    Accion.ErrorSistema(ex.Message);
            //}
            try
            {
                decimal num = 0;
                decimal num2 = 0;
                num = Convert.ToDecimal(txtimporteMN.Text);

                if (Detalles.Count > 0)
                {
                    foreach (Entidad_Movimiento_Cab cab in Detalles)
                    {
                        if (!cab.Ctb_Es_Cuenta_Principal & !cab.Cta_Dif)
                        {
                            num2 += cab.Ctb_Importe_Debe + cab.Ctb_Importe_Haber;
                        }
                    }

                }

                txtTotalImporteMNDet.Text = Convert.ToString(num2);
                TxtDiferencias.Text = Convert.ToString((decimal)(num - ((Convert.ToDecimal(IIf(TxtDifCambio.Text == "", 0, TxtDifCambio.Text)) + Convert.ToDecimal(IIf(TxtDifRedondeo.Text == "", 0, TxtDifRedondeo.Text))) + num2)));
            }
            catch
            {
                TxtDiferencias.Text = "0.0";
            }
        }

        private void TxtDifRedondeo_TextChanged(object sender, EventArgs e)
        {
            MostrarDiferencias();
        }

        private void txtcaja_TextChanged(object sender, EventArgs e)
        {
            if (txtcaja.Focus() == false)
            {
                txtcajadesc.ResetText();
                txtrucdni.ResetText();
                txtentidad.ResetText();
            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacod.Focus() == false)
            {
                txtmonedadesc.ResetText();
                txttipocambiocod.ResetText();
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
                txtmonedacod.Focus();

            }
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocod.Focus() == false)
            {
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
            }
        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            if (txtcuenta.Focus() == false)
            {
                txtcuentadesc.ResetText();
            }
        }

        private void txttipodocdet_TextChanged(object sender, EventArgs e)
        {
            if (txttipodocdet.Focus() == false)
            {
                txttipodocdescdet.ResetText();
            }
        }

        private void txtmonedacoddet_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacoddet.Focus() == false)
            {
                txtmonedadescdet.ResetText();
                txttipocambiocoddet.ResetText();
                txttipocambiodescdet.ResetText();
                txttipocambiovalordet.Text = "0.000";
                txtmonedacoddet.Focus();

            }
        }

        private void txttipocambiocoddet_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocoddet.Focus() == false)
            {
                txttipocambiodescdet.ResetText();
                txttipocambiovalordet.Text = "0.000";
            }
        }

        private void txttipoentcoddet_TextChanged(object sender, EventArgs e)
        {
            if (txttipoentcoddet.Focus())
            {
                txttipoentdescdet.ResetText();
            }
        }

        private void txtrucdnidet_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdnidet.Focus() == false)
            {
                txtentidaddet.ResetText();
            }
        }

        private void txttipo_TextChanged(object sender, EventArgs e)
        {
            if (txttipo.Focus() == false)
            {
                txttipodesc.ResetText();
            }
        }

        private void txtimporteMNDet_TextChanged(object sender, EventArgs e)
        {
            MostrarImporteMN_Det();
        }

        void MostrarImporteMN_Det()
        {
            try
            {
                txtimporteMEDet.Text = (Convert.ToDecimal(txtimporteMNDet.Text) * Convert.ToDecimal(txttipocambiovalordet.Text)).ToString();
            }
            catch (Exception)
            {
                txtimporteMEDet.Text = "0.00";
            }
        }

        private void frm_apertura_cajachica_edicion_Load(object sender, EventArgs e)
        {
            TraerLibro();
            BuscarTipoEgre();
            BloquearDetalles();
            BloquearDiferencias_Inicio();
            BuscarTipoCaja_Banco();

            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();

                if (Ctb_Caja_CHica_Estado == "0055")
                {
                    btnguardar.Enabled = false;
                    lblestado.Text = Ctb_Caja_CHica_Estado_Descripcion;
                }

            }
        }


        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            Ent.Ctb_Tipo_IE = Tipo_IE;
            Ent.Ctb_Es_Caja_Chica = true;

            try
            {
                Lista_Modificar = log.Listar_Caja_Chica(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = txtlibro.Tag.ToString();
                    Voucher = Enti.Id_Voucher;

                    txttipoEgreso.Tag = Enti.Ctb_Tipo_IE;
                    txttipoEgreso.Text = Enti.Ctb_Tipo_IE_Interno;
                    txttipoEgresodesc.Text = Enti.Ctb_Tipo_IE_Desc;

                    txttipomovimientocod.Tag = Enti.Ctb_Tipo_Movimiento;
                    txttipomovimientocod.Text = Enti.Ctb_Tipo_Movimiento_Interno;
                    txttipomovimientodesc.Text = Enti.Ctb_Tipo_Movimiento_Desc;

                    txtcaja.Text = Enti.Ctb_Codigo_Caja_Chica;
                    txtcajadesc.Text = Enti.Ctb_Codigo_Caja_Chica_Desc;

                    //Recuperamos la cuenta contable de la caja chica
                    Caja();


                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));
             
                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    txtglosa.Text = Enti.Ctb_Glosa;

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;

                    txtimporte.Text = Convert.ToDecimal(Enti.Ctb_Importe).ToString();



                   ChkDifCambio.Checked  = Enti.Ctb_Es_Dif_Cambio;
                   TxtDifCambio.Text =Convert.ToString(Enti.Ctb_Importe_Dif_Cambio);
                   ChkDifRedondeo.Checked  =Enti.Ctb_Es_Redondeo  ;
                   TxtDifRedondeo.Text   = Convert.ToString(Enti.Ctb_Importe_Redondeo) ;
                   TxtDiferencias.Text  = Convert.ToString(Enti.Ctb_Importe_Diferencias);


                    //listar admnistrativo

                    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();
                    dgvdatos.DataSource = null;
                    Detalles = log_det.Listar_Caja_Chica_Det(Enti);

                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                        MostrarDiferencias();

                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_CajaBanco(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    txtlibro.Tag = Generales[0].Ini_CajaBanco;
                    txtlibro.Text = Generales[0].Ini_CajaBanco_Desc;
                }
                else
                {
                    Accion.Advertencia("Debe configurar un libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarTipoEgre()
        {
            try
            {
                txttipoEgreso.Text = Accion.Formato(txttipoEgreso.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0015",
                    Gen_Codigo_Interno = "02"//txttipoEgreso.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == "02")
                        {
                            txttipoEgreso.Tag = T.Id_General_Det;
                            txttipoEgreso.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipoEgresodesc.Text = T.Gen_Descripcion_Det;
                            txttipoEgreso.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipoEgreso.EnterMoveNextControl = false;
                    txttipoEgreso.ResetText();
                    txttipoEgreso.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void BuscarTipoCaja_Banco()
        {
            try
            {
               
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0016",
                    Gen_Codigo_Interno = "01"//txttipoEgreso.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == "01")
                        {
                 
                            txttipomovimientocod.Tag = T.Id_General_Det;
                            txttipomovimientocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipomovimientodesc.Text = T.Gen_Descripcion_Det;
                            txttipomovimientodesc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipomovimientocod.EnterMoveNextControl = false;
                    txttipomovimientocod.ResetText();
                    txttipomovimientocod.Focus();

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


    }
}
