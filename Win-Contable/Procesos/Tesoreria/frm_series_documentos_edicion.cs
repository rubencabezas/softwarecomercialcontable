﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_series_documentos_edicion : Contable.frm_fuente
    {
        public frm_series_documentos_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        public string Id_Empresa,Ser_Tipo_Doc, Ser_Tip_IE, Ser_Tipo_Mov, Ser_Serie;
        public Boolean Ser_Emitiendose;
        void LimpiarCab()
        {
             txttipodoc.ResetText();
            txttipoEgreso.ResetText();
            txttipomovimientocod.ResetText();
            txtserie.ResetText();
            txtnumeroinicio.ResetText();
            txtnumerofin.ResetText();
        }
        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoEgreso_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipoEgreso.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoEgreso.Text.Substring(txttipoEgreso.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0015";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoEgreso.Tag = Entidad.Id_General_Det;
                                txttipoEgreso.Text = Entidad.Gen_Codigo_Interno;
                                txttipoEgresodesc.Text = Entidad.Gen_Descripcion_Det;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoEgreso.Text) & string.IsNullOrEmpty(txttipoEgresodesc.Text))
                    {
                        BuscarTipoEgre();

                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarTipoEgre()
        {
            try
            {
                txttipoEgreso.Text = Accion.Formato(txttipoEgreso.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0015",
                    Gen_Codigo_Interno = "01"//txttipoEgreso.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == "01")
                        {
                            txttipoEgreso.Tag = T.Id_General_Det;
                            txttipoEgreso.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipoEgresodesc.Text = T.Gen_Descripcion_Det;
                            txttipoEgreso.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipoEgreso.EnterMoveNextControl = false;
                    txttipoEgreso.ResetText();
                    txttipoEgreso.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipomovimientocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipomovimientocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipomovimientocod.Text.Substring(txttipomovimientocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0016";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipomovimientocod.Tag = Entidad.Id_General_Det;
                                txttipomovimientocod.Text = Entidad.Gen_Codigo_Interno;
                                txttipomovimientodesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipomovimientocod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipomovimientocod.Text) & string.IsNullOrEmpty(txttipomovimientodesc.Text))
                    {
                        BuscarMovimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMovimiento()
        {
            try
            {
                txttipomovimientocod.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0016",
                    Gen_Codigo_Interno = txttipomovimientocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipomovimientocod.Text.Trim().ToUpper())
                        {
                            txttipomovimientocod.Tag = T.Id_General_Det;
                            txttipomovimientocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipomovimientodesc.Text = T.Gen_Descripcion_Det;
                            txttipomovimientocod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipomovimientocod.EnterMoveNextControl = false;
                    txttipomovimientocod.ResetText();
                    txttipomovimientocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtserie_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtserie.Text))
            {
                txtserie.Text = Accion.Formato(txtserie.Text, 4);
            }
        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtnumeroinicio.Text))
            {
                txtnumeroinicio.Text = Accion.Formato(txtnumeroinicio.Text, 8);
            }
        }

        private void frm_series_documentos_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;


            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
             
                ListarModificar();
                txttipodoc.Enabled = false;
                txttipoEgreso.Enabled = false;
                txtserie.Enabled = false;


            }
        }

        public List<Entidad_Series_Tesorerira> Lista_Modificar = new List<Entidad_Series_Tesorerira>();

        private void tctctacorriente_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(tctctacorriente.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & tctctacorriente.Text.Substring(tctctacorriente.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_cuenta_corriente_busqueda f = new _1_Busquedas_Generales.frm_cuenta_corriente_busqueda())
                        {
                            f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            f.Id_Anio = Actual_Conexion.AnioSelect;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Cuenta_Corriente Entidad = new Entidad_Cuenta_Corriente();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                tctctacorriente.Text = Entidad.Id_Cuenta_Corriente;
                                tctctacorriente.EnterMoveNextControl = true;
             
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(tctctacorriente.Text) & string.IsNullOrEmpty(tctctacorriente.Text))
                    {
                        BuscarCtaCorriente();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarCtaCorriente()
        {
            try
            {
                //txtmedpago.Text = /*Accion.Formato(txtmedpago.Text, 2);*/
                Logica_Cuenta_Corriente log = new Logica_Cuenta_Corriente();

                List<Entidad_Cuenta_Corriente> Generales = new List<Entidad_Cuenta_Corriente>();
                Generales = log.Listar(new Entidad_Cuenta_Corriente
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Cuenta_Corriente = tctctacorriente.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Cuenta_Corriente T in Generales)
                    {
                        if ((T.Id_Cuenta_Corriente).ToString().Trim().ToUpper() == tctctacorriente.Text.Trim().ToUpper())
                        {
                            tctctacorriente.Text = (T.Id_Cuenta_Corriente).ToString().Trim();
         
                            tctctacorriente.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    tctctacorriente.EnterMoveNextControl = false;
                    tctctacorriente.ResetText();
                    tctctacorriente.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void chkCuentaCorriente_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCuentaCorriente.Checked)
            {
                tctctacorriente.Enabled = true;
            }
            else
            {
                tctctacorriente.Enabled = false;
            }
        }

        public void ListarModificar()
        {
            Entidad_Series_Tesorerira Ent = new Entidad_Series_Tesorerira();
            Logica_Series_Tesoreria log = new Logica_Series_Tesoreria();

         
            try
            {
                Ent.Id_Empresa = Id_Empresa;
                Ent.Ser_Tipo_Doc = Ser_Tipo_Doc;
                Ent.Ser_Tip_IE = Ser_Tip_IE;
                Ent.Ser_Tipo_Mov = Ser_Tipo_Mov;
                Ent.Ser_Serie = Ser_Serie;
                Ent.Ser_Emitiendose = Ser_Emitiendose;

                Lista_Modificar = log.Listar(Ent);

                Entidad_Series_Tesorerira Entidad = new Entidad_Series_Tesorerira();
                Entidad = Lista_Modificar[0];


                txttipodoc.Text = Entidad.Ser_Tipo_Doc_Sunat;
                txttipodoc.Tag = Entidad.Ser_Tipo_Doc;
                txttipodocdesc.Text = Entidad.Ser_Tipo_Doc_Des;
                
                txttipoEgreso.Tag = Entidad.Ser_Tip_IE;
                txttipoEgreso.Text = Entidad.Ser_Tip_IE_Interno;
                txttipoEgresodesc.Text = Entidad.Ser_Tip_IE_Desc;

                txttipomovimientocod.Tag = Entidad.Ser_Tipo_Mov;
                txttipomovimientocod.Text = Entidad.Ser_Tipo_Mov_Interno;
                txttipomovimientodesc.Text = Entidad.Ser_Tipo_Mov_Desc;


                txtserie.Text = Entidad.Ser_Serie;
                txtnumeroinicio.Text = Entidad.Ser_Numero_Inicio;
                txtnumerofin.Text = Entidad.Ser_Numero_Fin;


                chkCuentaCorriente.Checked = Entidad.Ser_Es_CtaCorriente;
                tctctacorriente.Text = Entidad.Ser_Cta_Corriente;

                chkCajachica.Checked = Entidad.Ser_Es_CajaChica;
                txtcaja.Text = Entidad.Ser_Codigo_Caja;
                txtcajadesc.Text = Entidad.Ser_Caja_Desc;

                chkrendicion.Checked = Entidad.Ser_Rendicion_Caja_Chica;
                chkreembolso.Checked = Entidad.Ser_Reembolso_Caja_Chica;
                chkcierre.Checked = Entidad.Ser_Cierre_Caja_Chica;
 



            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtcaja_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtcaja.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcaja.Text.Substring(txtcaja.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_caja_chica_busqueda f = new _1_Busquedas_Generales.frm_caja_chica_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Caja_Chica_Config Entidad = new Entidad_Caja_Chica_Config();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcaja.Text = Entidad.Id_Caja;
                                txtcajadesc.Text = Entidad.Cja_Descripcion;

                                //cuenta_caja_chica = Entidad.Cja_Cuenta;
                                //cuenta_caja_chica_Desc = Entidad.Cja_Cuenta_Desc;

                                //txtrucdni.Text = Entidad.Cja_Responsable;
                                //txtentidad.Text = Entidad.Cja_Responsable_Desc;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcaja.Text) & string.IsNullOrEmpty(txtcajadesc.Text))
                    {
                        Caja();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Caja()
        {
            try
            {
                Logica_Caja_Chica_Config log = new Logica_Caja_Chica_Config();

                List<Entidad_Caja_Chica_Config> Generales = new List<Entidad_Caja_Chica_Config>();
                Generales = log.Listar(new Entidad_Caja_Chica_Config
                {
                    Id_Caja = txtcaja.Text,
                    Cja_Es_Caja_Chica = true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Caja_Chica_Config T in Generales)
                    {
                        if ((T.Id_Caja).ToString().Trim().ToUpper() == txtcaja.Text.Trim().ToUpper())
                        {
                            txtcaja.Text = (T.Id_Caja).ToString().Trim();
                            txtcajadesc.Text = T.Cja_Descripcion;

                            //cuenta_caja_chica = T.Cja_Cuenta;
                            //cuenta_caja_chica_Desc = T.Cja_Cuenta_Desc;

                            //txtrucdni.Text = T.Cja_Responsable;
                            //txtentidad.Text = T.Cja_Responsable_Desc;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcaja.EnterMoveNextControl = false;
                    txtcajadesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void chkCajachica_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCajachica.Checked)
            {
                txtcaja.Enabled = true;
            }
            else
            {
                txtcaja.Enabled = false;
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                if (VerificarCabecera())
                {



                    Entidad_Series_Tesorerira Ent = new Entidad_Series_Tesorerira();
                    Logica_Series_Tesoreria Log = new Logica_Series_Tesoreria();

                    Ent.Id_Empresa = Id_Empresa;
                    Ent.Ser_Tipo_Doc = txttipodoc.Tag.ToString();
                    Ent.Ser_Tip_IE = txttipoEgreso.Tag.ToString();

                   
                    if (txttipomovimientocod.Tag == null)
                    {
                        Ent.Ser_Tipo_Mov = "";
                    }
                    else
                    {
                        Ent.Ser_Tipo_Mov = txttipomovimientocod.Tag.ToString();
                    }

                    Ent.Ser_Serie = txtserie.Text;
                    Ent.Ser_Numero_Inicio = txtnumeroinicio.Text;
                    Ent.Ser_Numero_Fin = txtnumerofin.Text;
                    Ent.Ser_Emitiendose = Ser_Emitiendose;

                    Ent.Ser_Es_CtaCorriente = chkCuentaCorriente.Checked;
                    Ent.Ser_Cta_Corriente = tctctacorriente.Text.Trim().ToString();

                    Ent.Ser_Es_CajaChica = chkCajachica.Checked;
                    Ent.Ser_Codigo_Caja = txtcaja.Text;

                    Ent.Ser_Rendicion_Caja_Chica = chkrendicion.Checked;
                    Ent.Ser_Reembolso_Caja_Chica = chkreembolso.Checked;
                    Ent.Ser_Cierre_Caja_Chica =chkcierre.Checked;


                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Estado = Estados.Nuevo;
                                LimpiarCab();
                                txttipodoc.Focus();
                                this.Close();

                            }

                        }
                        else if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                this.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipoEgresodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo (Ingreso/Egreso)");
                txttipoEgreso.Focus();
                return false;
            }
            //if (string.IsNullOrEmpty(txttipomovimientodesc.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar un tipo de movimiento");
            //    txttipomovimientocod.Focus();
            //    return false;
            //}

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtnumeroinicio.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero de inicio");
                txtnumeroinicio.Focus();
                return false;
            }

        if (string.IsNullOrEmpty(txtnumerofin.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero de fin");
                txtnumerofin.Focus();
                return false;
            }


            return true;
        }

        private void txtnumerofin_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtnumerofin.Text))
            {
                txtnumerofin.Text = Accion.Formato(txtnumerofin.Text, 8);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }



    }
}
