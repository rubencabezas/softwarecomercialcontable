﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_apertura_cajachica : Contable.frm_fuente
    {
        public frm_apertura_cajachica()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            //{
                       using (frm_apertura_cajachica_edicion f = new frm_apertura_cajachica_edicion())
                    {


                        Estado = Estados.Nuevo;
                        f.Estado_Ven_Boton = "1";
                        f.Id_Periodo = Actual_Conexion.PeriodoSelect;

                        if (f.ShowDialog() == DialogResult.OK)
                        {


                            try
                            {
                                Listar();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                        }
                        else
                        {
                            Listar();
                        }

                    }

            //}

 
        }

        private void frm_apertura_cajachica_Load(object sender, EventArgs e)
        {
            TraerLibro();
            Listar();
        }

        string Id_Libro;

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_CajaBanco(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_CajaBanco;

                }
                else
                {
                    Accion.Advertencia("Debe configurar libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;
            Ent.Ctb_Tipo_IE = "0050"; //Egreso
            Ent.Ctb_Es_Caja_Chica = true;
            try
            {
                Lista = log.Listar_Caja_Chica(Ent);
                dgvdatos.DataSource = null;


                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            using (frm_apertura_cajachica_edicion f = new frm_apertura_cajachica_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Ctb_Caja_CHica_Estado = Entidad.Ctb_Caja_CHica_Estado;
                f.Ctb_Caja_CHica_Estado_Descripcion = Entidad.Ctb_Caja_CHica_Estado_Desc;

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Libro = Entidad.Id_Libro;
                f.Voucher = Entidad.Id_Voucher;
                f.Tipo_IE = Entidad.Ctb_Tipo_IE;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
         
        
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
         
        }

        private void btexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\AperturaCajachica" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TraerLibro();
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

                if (Entidad.Ctb_Caja_CHica_Estado== "0053")
                    {
                            string DocDelted = "La apertura de Caja Chica a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
                            if (Accion.ShowDeleted(DocDelted))
                            {
                                try
                                {
                                        Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab
                                        {
                                            Id_Empresa = Entidad.Id_Empresa,
                                            Id_Anio = Entidad.Id_Anio,
                                            Id_Periodo = Entidad.Id_Periodo,
                                            Id_Voucher = Entidad.Id_Voucher,
                                            Id_Libro = Entidad.Id_Libro,
                                            Ctb_Es_Caja_Chica=true,
                                            Id_Anio_Ref = Entidad.Id_Anio_Ref,
                                            Id_Periodo_Ref = Entidad.Id_Periodo_Ref,
                                            Id_Libro_Ref = Entidad.Id_Libro_Ref,
                                            Id_Voucher_Ref = Entidad.Id_Voucher_Ref
                                        };

                                    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();
                                    Ent.DetalleAsiento = log_det.Listar_Caja_Chica_Det(Ent);
                                    Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

                                    log.Eliminar_Tesoreria(Ent);
                                        Accion.ExitoGuardar();
                                        Listar();
                                 }
                                catch (Exception ex)
                                {
                                    Accion.ErrorSistema(ex.Message);
                                }

                            }

            }
            else
            {
                Accion.Advertencia("Solo se puede eliminar si su estado es SIN RENDICION");
            }


        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
