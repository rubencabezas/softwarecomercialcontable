﻿using Contable._0_Configuracion.Entidades;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_caja_banco_edicion : Contable.frm_fuente
    {
        public frm_caja_banco_edicion()
        {
            InitializeComponent();
        }

        public string Voucher;
        public string Tipo_IE;
          List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();

        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();



        public string Id_Periodo;
        public string Id_Libro;
        public string Id_Empresa;
        public string Id_Anio;

        public string Estado_Ven_Boton;

        bool es_cuenta_principal;

        string txttipodoc_;
        string Numero;

        bool Es_moneda_nac;
        bool Es_moneda_nacDet;

        int Id_Item;

        string Anio_Ref;
        int Item_Ref;
        string Periodo_Ref;
        string Libro_Ref;
        string Voucher_Ref;

        DateTime FechaDoc;

        decimal ImporteNoMay;

        string Cuenta_Contable_Cab;
        string Cuenta_Contable_Desc;
        string Cuenta_Cor;
        string Cuenta_Cor_Desc;


        string txtentfinanciera;
        string txtentfinancieradesc;





        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                 
               // LimpiarCab();
                LimpiarDet();
                Detalles.Clear();

                dgvdatos.DataSource = null;
                BloquearDetalles();
                TraerLibro();
                Detalles.Clear();
                txtglosa.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;


            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                Id_Item = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }





        private void Actualizar_Cabezara_Si_Es_Cuenta_Principal(Entidad_Movimiento_Cab Enti)
        {
             Cuenta_Contable_Cab = Enti.Ctb_Cuenta;
             Cuenta_Contable_Desc = Enti.Ctb_Cuenta_Desc;
             txttipoentcod.Text = Enti.Ctb_Tipo_Ent_det;
             txtrucdni.Text = Enti.Ctb_Ruc_dni_det;
             txtentidad.Text = Enti.Entidad_det;
             txttipodoc_ = Enti.Ctb_Tipo_Doc_det;
             txttipodoc.Text = Enti.Ctb_Tipo_Doc_det_Sunat;
             txttipodocdesc.Text = Enti.Ctb_Tipo_Doc_det_desc;
             txtserie.Text = Enti.Ctb_Serie_det + "-" + Enti.Ctb_Numero_det;
             txtfechadoc.Text = string.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Mov_det);
             txtmonedacod.Tag = Enti.Ctb_moneda_cod_det;
             txtmonedacod.Text = Enti.Ctb_moneda_cod_det_Sunat;
             txtmonedadesc.Text = Enti.Ctb_moneda_det_desc;
             txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod_Det;
             txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_Desc_Det;
             txttipocambiovalor.Text = Convert.ToString(Enti.Ctb_Tipo_Cambio_Valor_Det);
             txtimporte.Text =  txtimporteMNDet.Text;
             txtflujoefectivocod.Text = Enti.Flujo_Efectivo_Cod;
             es_cuenta_principal = true;
        }

        private void AumentarNumero()
        {
            string str = Convert.ToString((int)(Convert.ToInt32( Numero) + 1));
            if (! txtserie.Text.Trim().Contains("-"))
            {
                Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                 txtserie.Focus();
            }
            else
            {
                char[] separator = new char[] { Convert.ToChar("-") };
                string[] strArray =  txtserie.Text.Split(separator);
                 txtserie.Text = Accion.Formato(strArray[0].Trim(), 4).ToString() + "-" + Accion.Formato(strArray[1].Trim(), 8).ToString();
                 txtserie.EnterMoveNextControl = true;
            }
        }


        private void BloquearCompensacion()
        {
             txttipodoc.Enabled = false;
             txtserie.Enabled = false;
             txttipoentcod.Enabled = false;
             txtrucdni.Enabled = false;
             btnbuscarprovisioncab.Enabled = false;
        }


        private void BloquearDetalles()
        {
             txtcuenta.Enabled = false;
             txttipodocdet.Enabled = false;
             txtseriedet.Enabled = false;
             txtmonedacoddet.Enabled = false;
             txttipocambiocoddet.Enabled = false;
             txttipocambiovalordet.Enabled = false;
             txtrucdnidet.Enabled = false;
             txtmedpagodet.Enabled = false;
             txttipo.Enabled = false;
             txtimporteMNDet.Enabled = false;
        }


        private void BloquearDiferencias()
        {
            if ( ChkDifCambio.Checked)
            {
                 TxtDifCambio.Enabled = true;
                 TxtDifCambio.Focus();
            }
            else
            {
                 TxtDifCambio.Enabled = false;
                 TxtDifCambio.Text = "0.0";
            }
            if ( ChkDifRedondeo.Checked)
            {
                 TxtDifRedondeo.Enabled = true;
                 TxtDifRedondeo.Focus();
            }
            else
            {
                 TxtDifRedondeo.Enabled = false;
                 TxtDifRedondeo.Text = "0.0";
            }
        }


        private void BloquearTipoCajaBanco()
        {
             tctctacorriente.Enabled = false;
             txtnumetransaccion.Enabled = false;
            // chkITF.Enabled = false;
        }


        public bool VerificarCuentaPrincipal()
        {
            if ( Detalles.Count > 0)
            {
                foreach (Entidad_Movimiento_Cab cab in  Detalles)
                {
                    if (cab.Ctb_Es_Cuenta_Principal)
                    {
                        Accion.Advertencia("Ya existe una cuenta principal");
                         txtcuenta.Focus();
                        return false;
                    }
                }
            }
            return true;
        }


        private void btnAgregarCtaPrincipal_Click(object sender, EventArgs e)
        {
            try
            {
                if ( VerificarCuentaPrincipal())
                {
                    Entidad_Parametro_Inicial inicial = new Entidad_Parametro_Inicial();
                    Logica_Parametro_Inicial inicial2 = new Logica_Parametro_Inicial();
                    List<Entidad_Parametro_Inicial> list = new List<Entidad_Parametro_Inicial>();
                    inicial.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    inicial.Id_Anio = Actual_Conexion.AnioSelect;
                    if ( txttipomovimientocod.Text != "02")
                    {
                        if ( txttipodoc_ == null)
                        {
                             txttipodoc_ = "";
                        }
                         TraerDetalleCta( Cuenta_Contable_Cab,  Cuenta_Contable_Desc,  txttipoentcod.Text.Trim(),  txtrucdni.Text.Trim(),  txtentidad.Text.Trim(),  txttipodoc_.Trim(),  txttipodoc.Text.Trim(),  txttipodocdesc.Text.Trim(),  txtserie.Text.Trim(), Convert.ToDateTime( txtfechadoc.Text),  txtmonedacod.Tag.ToString(),  txtmonedacod.Text.Trim(),  txtmonedadesc.Text.Trim(),  txttipocambiocod.Text.Trim(),  txttipocambiodesc.Text, Convert.ToDecimal( txttipocambiovalor.Text), Convert.ToDecimal( txtimporte.Text),  txtflujoefectivocod.Text.Trim(), true);
                         dgvdatos.DataSource = null;
                         dgvdatos.DataSource =  Detalles;
                         txtcuenta.Focus();
                    }
                    else if ( txttipomovimientocod.Text == "02")
                    {
                         TraerDetalleCta( Cuenta_Cor,  Cuenta_Cor_Desc,  txttipoentcod.Text,  txtrucdni.Text,  txtentidad.Text,  txttipodoc_,  txttipodoc.Text,  txttipodocdesc.Text,  txtserie.Text, Convert.ToDateTime( txtfechadoc.Text),  txtmonedacod.Tag.ToString(),  txtmonedacod.Text,  txtmonedadesc.Text,  txttipocambiocod.Text,  txttipocambiodesc.Text, Convert.ToDecimal( txttipocambiovalor.Text), Convert.ToDecimal( txtimporte.Text),  txtflujoefectivocod.Text.Trim(), true);
                         dgvdatos.DataSource = null;
                         dgvdatos.DataSource =  Detalles;
                         txtcuenta.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }
        }


        private void TraerDetalleCta(string cta, string desccta, string TipoEnt, string ruc_dni, string enti_descrip, string TipoDoc, string TipoDocSunat, string TipoDocDesc, string SerieNumero, DateTime Fecha, string MonCod, string MonCodSunat, string MonDesc, string TC, string TC_Desc, decimal ValTv, decimal Importe, string Flujo_Efectivo_Cod, bool es_cuenta_principal)
        {
            try
            {
                Entidad_Movimiento_Cab item = new Entidad_Movimiento_Cab();
                Logica_Movimiento_Cab cab2 = new Logica_Movimiento_Cab();
                List<Entidad_Movimiento_Cab> list = new List<Entidad_Movimiento_Cab>();
                item.Id_Item =  Detalles.Count + 1;
                item.Ctb_Cuenta = cta;
                item.Ctb_Cuenta_Desc = desccta;
                item.Ctb_Tipo_Ent_det = TipoEnt;
                item.Ctb_Ruc_dni_det = ruc_dni;
                item.Entidad_det = enti_descrip;
                item.Ctb_Tipo_Doc_det = TipoDoc;
                item.Ctb_Tipo_Doc_det_Sunat = TipoDocSunat;
                item.Ctb_Tipo_Doc_det_desc = TipoDocDesc;

                string str = SerieNumero;

                if (SerieNumero != "")
                {
                    char[] separator = new char[] { Convert.ToChar("-") };
                    string[] strArray = str.Split(separator);
                    item.Ctb_Serie_det = Accion.Formato(strArray[0].Trim(), 4).ToString();
                    item.Ctb_Numero_det = Accion.Formato(strArray[1].Trim(), 8).ToString();
                }

                item.Ctb_Fecha_Mov_det = Fecha;
                item.Ctb_moneda_cod_det = MonCod;
                item.Ctb_moneda_cod_det_Sunat = MonCodSunat;
                item.Ctb_moneda_det_desc = MonDesc;
                item.Ctb_Tipo_Cambio_Cod_Det = TC;
                item.Ctb_Tipo_Cambio_Desc_Det = TC_Desc;
                item.Ctb_Tipo_Cambio_Valor_Det = ValTv;
                item.Ctb_Importe = Convert.ToDecimal( txtimporte.Text);

                if ( txttipomovimientocod.Text == "01")
                {
                    if (! Es_moneda_nac)
                    {
                        item.Ctb_Importe_Debe = Convert.ToDecimal( txtimporte.Text) * Convert.ToDecimal( txttipocambiovalor.Text);
                        item.Ctb_Importe_Haber = 0;
                        item.Ctb_Importe_Debe_Extr = Convert.ToDecimal( txtimporte.Text);
                        item.Ctb_Importe_Haber_Extr = 0;
                    }
                    else
                    {
                        item.Ctb_Importe_Debe = Convert.ToDecimal( txtimporte.Text) * Convert.ToDecimal( txttipocambiovalor.Text);
                        item.Ctb_Importe_Haber = 0;
                        item.Ctb_Importe_Debe_Extr = 0;
                        item.Ctb_Importe_Haber_Extr = 0;
                    }
                }
                if ( txttipomovimientocod.Text == "02")
                {
                    if (! Es_moneda_nac)
                    {
                        item.Ctb_Importe_Debe = 0;
                        item.Ctb_Importe_Haber = Convert.ToDecimal( txtimporte.Text) * Convert.ToDecimal( txttipocambiovalor.Text);
                        item.Ctb_Importe_Debe_Extr = 0;
                        item.Ctb_Importe_Haber_Extr = Convert.ToDecimal( txtimporte.Text);
                    }
                    else
                    {
                        item.Ctb_Importe_Debe = 0;
                        item.Ctb_Importe_Haber = Convert.ToDecimal( txtimporte.Text) * Convert.ToDecimal( txttipocambiovalor.Text);
                        item.Ctb_Importe_Debe_Extr = 0;
                        item.Ctb_Importe_Haber_Extr = 0;
                    }
                }
                if (item.Ctb_Importe_Debe > 0)
                {
                    item.Ctb_Tipo_DH = "0004";
                    item.CCtb_Tipo_DH_Interno = "01";
                    item.Ctb_Tipo_DH_Desc = "DEBE";
                }
                else
                {
                    item.Ctb_Tipo_DH = "0005";
                    item.CCtb_Tipo_DH_Interno = "02";
                    item.Ctb_Tipo_DH_Desc = "HABER";
                }
                item.Estado_Flujo_Efecctivo_Cod_Det = Flujo_Efectivo_Cod;
                item.Ctb_Es_Cuenta_Principal = es_cuenta_principal;
                 Detalles.Add(item);
                 Sumar(Detalles);
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }


        private void Sumar(List<Entidad_Movimiento_Cab> Entidad_Movimiento_Cab)
        {
            for (int i = 0; i <= ( Detalles.Count - 1); i++)
            {
                 Detalles[i].Id_Item = i + 1;
            }
        }




         

        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {
                if ( VerificarDetalle())
                {
                    Entidad_Movimiento_Cab enti = new Entidad_Movimiento_Cab
                    {
                        Ctb_Es_Cuenta_Principal =  es_cuenta_principal,
                        Id_Item =  Id_Item,
                        Ctb_Cuenta =  txtcuenta.Text,
                        Ctb_Cuenta_Desc =  txtcuentadesc.Text,
                        Ctb_Medio_Pago_Det =  txtmedpagodet.Text,
                        Ctb_Medio_Pago_Det_Desc =  txtmedpagodescdet.Text,
                        Ctb_Tipo_Doc_det_Sunat =  txttipodocdet.Text,
                        Ctb_Tipo_Doc_det =  txttipodocdet.Tag.ToString(),
                        Ctb_Tipo_Doc_det_desc =  txttipodocdescdet.Text
                    };
                    char[] separator = new char[] { Convert.ToChar("-") };
                    string[] strArray =  txtseriedet.Text.Split(separator);
                    enti.Ctb_Serie_det = Accion.Formato(strArray[0].Trim(), 4).ToString();
                    enti.Ctb_Numero_det = Accion.Formato(strArray[1].Trim(), 8).ToString();
                    enti.Ctb_Fecha_Mov_det =  FechaDoc;
                    enti.Ctb_moneda_cod_det_Sunat =  txtmonedacoddet.Text;
                    enti.Ctb_moneda_cod_det =  txtmonedacoddet.Tag.ToString();
                    enti.Ctb_moneda_det_desc =  txtmonedadescdet.Text;
                    enti.Ctb_Tipo_Cambio_Cod_Det =  txttipocambiocoddet.Text;
                    enti.Ctb_Tipo_Cambio_Desc_Det =  txttipocambiodescdet.Text;
                    enti.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal( txttipocambiovalordet.Text);
                    enti.Ctb_Tipo_Ent_det =  txttipoentcod.Text;
                    enti.Ctb_Ruc_dni_det =  txtrucdnidet.Text;
                    enti.Entidad_det =  txtentidaddet.Text;
                    enti.Ctb_Tipo_DH =  txttipo.Tag.ToString();
                    enti.CCtb_Tipo_DH_Interno =  txttipo.Text;
                    enti.Ctb_Tipo_DH_Desc =  txttipodesc.Text;
                    enti.Id_Anio_Ref =  Anio_Ref;
                    enti.Id_Periodo_Ref =  Periodo_Ref;
                    enti.Id_Libro_Ref =  Libro_Ref;
                    enti.Id_Voucher_Ref =  Voucher_Ref;
                    enti.Id_Item_Ref =  Item_Ref;
                    enti.Ctb_Es_moneda_nac_det =  Es_moneda_nacDet;

                    if (enti.Ctb_Es_Cuenta_Principal)
                    {
                         Actualizar_Cabezara_Si_Es_Cuenta_Principal(enti);
                    }
                    if (enti.Ctb_Tipo_DH == "0004")
                    {
                        if (( txtimporteMEDet.Text == "") || ( txtimporteMNDet.Text == ""))
                        {
                             txtimporteMEDet.Text =  IIf( txtimporteMEDet.Text == "", "0.00",  txtimporteMEDet.Text).ToString();
                             txtimporteMNDet.Text =  IIf( txtimporteMNDet.Text == "", "0.00",  txtimporteMNDet.Text).ToString();
                        }
                        enti.Ctb_Importe_Debe = Convert.ToDecimal( txtimporteMNDet.Text) * Convert.ToDecimal( txttipocambiovalordet.Text);
                        enti.Ctb_Importe_Debe_Extr = ! Es_moneda_nacDet ? Convert.ToDecimal( txtimporteMEDet.Text.ToString()) : Convert.ToDecimal("0.000");
                    }
                    else if (enti.Ctb_Tipo_DH == "0005")
                    {
                        if (( txtimporteMEDet.Text == "") || ( txtimporteMNDet.Text == ""))
                        {
                             txtimporteMEDet.Text =  IIf( txtimporteMEDet.Text == "", "0.00",  txtimporteMEDet.Text).ToString();
                             txtimporteMNDet.Text =  IIf( txtimporteMNDet.Text == "", "0.00",  txtimporteMNDet.Text).ToString();
                        }
                        enti.Ctb_Importe_Haber = Convert.ToDecimal( txtimporteMNDet.Text) * Convert.ToDecimal( txttipocambiovalordet.Text);
                        enti.Ctb_Importe_Haber_Extr = ! Es_moneda_nacDet ? Convert.ToDecimal( txtimporteMNDet.Text.ToString()) : Convert.ToDecimal("0.000");
                    }
                    if (base.EstadoDetalle == Estados.Nuevo)
                    {
                         Detalles.Add(enti);
                         UpdateGrilla();
                        base.EstadoDetalle = Estados.Guardado;
                         LimpiarDet();
                         BloquearDetalles();
                         btnanadirdet.Focus();
                         btnanadirdet.Enabled = false;
                         btneditardet.Enabled = true;
                         btnquitardet.Enabled = true;
                         btnnuevodet.Enabled = true;
                    }
                    else if (base.EstadoDetalle == Estados.Modificar)
                    {
                         Detalles[ Id_Item - 1] = enti;
                         UpdateGrilla();
                        base.EstadoDetalle = Estados.Guardado;
                         LimpiarDet();
                         BloquearDetalles();
                         btnanadirdet.Focus();
                         btnanadirdet.Enabled = false;
                         btneditardet.Enabled = true;
                         btnquitardet.Enabled = true;
                         btnnuevodet.Enabled = true;
                    }
                     MostrarDiferencias();
                     btnnuevodet.Focus();
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }



        public bool VerificarDetalle()
        {
            bool flag2;
            if (string.IsNullOrEmpty( txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una cuenta contable");
                 txtcuenta.Focus();
                flag2 = false;
            }
            else if (string.IsNullOrEmpty( txttipodesc.Text))
            {
                Accion.Advertencia("Debe ingresar un tipo (Debe/Haber)");
                 txttipo.Focus();
                flag2 = false;
            }
            else if (!(Convert.ToDouble( txtimporteMNDet.Text) != 0.0))
            {
                Accion.Advertencia("Debe ingresar un importe");
                 txtimporteMNDet.Focus();
                flag2 = false;
            }
            else if ( es_cuenta_principal || (!(Convert.ToDecimal( ImporteNoMay) != 0) || (Convert.ToDecimal( txtimporteMNDet.Text) <= Convert.ToDecimal( ImporteNoMay))))
            {
                flag2 = true;
            }
            else
            {
                Accion.Advertencia("No puede modificar el importe del documento por otro mayor a  " +  ImporteNoMay.ToString());
                 txtimporteMNDet.Focus();
                flag2 = false;
            }
            return flag2;
        }


        private void LimpiarDet()
        {
             txtcuenta.ResetText();
             txtcuentadesc.ResetText();
             txttipodocdet.ResetText();
             txttipodocdescdet.ResetText();
             txtseriedet.ResetText();
             txtmonedacoddet.ResetText();
             txtmonedadescdet.ResetText();
             txttipocambiocoddet.ResetText();
             txttipocambiodescdet.ResetText();
             txttipocambiovalordet.ResetText();
             txtrucdnidet.ResetText();
             txtentidaddet.ResetText();
             txtmedpagodet.ResetText();
             txtmedpagodescdet.ResetText();
             txttipo.ResetText();
             txttipodesc.ResetText();
             txtimporteMNDet.ResetText();
             txtimporteMEDet.ResetText();
             es_cuenta_principal = false;
        }


        public void UpdateGrilla()
        {
             dgvdatos.DataSource = null;
            if ( Detalles.Count > 0)
            {
                 dgvdatos.DataSource =  Detalles;
            }
        }



        private void btnbuscarprovision_Click(object sender, EventArgs e)
        {
            try
            {
                using (frm_ingreso_provision f = new frm_ingreso_provision())
                {
                    f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    f.Id_Anio = Actual_Conexion.AnioSelect;
                    if (f.ShowDialog(this) == DialogResult.OK)
                    {
                        foreach (int num2 in f.gridView1.GetSelectedRows())
                        {
                            Entidad_Movimiento_Cab item = new Entidad_Movimiento_Cab();
                            item = f.Lista[f.gridView1.GetDataSourceRowIndex(num2)];
                            item.Id_Item =  Detalles.Count + 1;
                            item.Id_Anio_Ref = item.Id_Anio_Ref;
                            item.Id_Periodo_Ref = item.Id_Periodo_Ref;
                            item.Id_Libro_Ref = item.Id_Libro_Ref;
                            item.Id_Voucher_Ref = item.Id_Voucher_Ref;

                            if ((item.Id_Libro == "008") && (item.Ctb_Tipo_DH == "0004"))
                            {
                                item.Ctb_Tipo_DH = "0005";
                                item.CCtb_Tipo_DH_Interno = "01";
                                item.Ctb_Tipo_DH_Desc = "HABER";
                                item.Ctb_Importe_Haber = item.Ctb_Importe;
                                item.Ctb_Importe_Debe = decimal.Zero;
                                item.Ctb_Importe_Haber_Extr = item.Ctb_Importe_Debe_Extr;
                                item.Ctb_Importe_Debe_Extr = decimal.Zero;
                            }

                            if ((item.Id_Libro == "007") && (item.Ctb_Tipo_DH == "0005"))
                            {
                                item.Ctb_Tipo_DH = "0004";
                                item.CCtb_Tipo_DH_Interno = "01";
                                item.Ctb_Tipo_DH_Desc = "DEBE";
                                item.Ctb_Importe_Debe = item.Ctb_Importe;
                                item.Ctb_Importe_Haber = decimal.Zero;
                                item.Ctb_Importe_Debe_Extr = item.Ctb_Importe_Haber_Extr;
                                item.Ctb_Importe_Haber_Extr = decimal.Zero;
                            }

                            if ((item.Id_Libro == "007") && (item.Ctb_Tipo_DH == "0004") && (item.Ctb_Afecto_RE==true))
                            {
                                item.Ctb_Tipo_DH = "0004";
                                item.CCtb_Tipo_DH_Interno = "01";
                                item.Ctb_Tipo_DH_Desc = "DEBE";
                                item.Ctb_Importe_Debe = item.Ctb_Importe;
                                item.Ctb_Importe_Haber = decimal.Zero;
                                item.Ctb_Importe_Debe_Extr = item.Ctb_Importe_Haber_Extr;
                                item.Ctb_Importe_Haber_Extr = decimal.Zero;
                            }

                            if ( ExistsInDetails(item.Ctb_Cuenta, item.Ctb_Tipo_Doc_det, item.Ctb_Serie_det, item.Ctb_Numero_det, item.Ctb_Tipo_Ent_det, item.Ctb_Ruc_dni_det))
                            {
                                Accion.Advertencia("El Documento ya existe en la lista de agregados");
                            }
                            else
                            {
                                 Detalles.Add(item);
                            }
                        }
                         dgvdatos.DataSource = null;
                        if ( Detalles.Count > 0)
                        {
                             dgvdatos.DataSource =  Detalles;
                             MostrarDiferencias();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }

        }


        private bool ExistsInDetails(string CtaCble, string TipDoc, string Serie, string Numero, string TipEnt, string Ent)
        {
            using (List<Entidad_Movimiento_Cab>.Enumerator enumerator =  Detalles.GetEnumerator())
            {
                while (true)
                {
                    if (!enumerator.MoveNext())
                    {
                        break;
                    }
                    Entidad_Movimiento_Cab current = enumerator.Current;
                    if (((current.Ctb_Cuenta == CtaCble) && ((current.Ctb_Tipo_Doc_det == TipDoc) && ((current.Ctb_Serie_det == Serie) && ((current.Ctb_Numero_det == Numero) && (current.Ctb_Tipo_Ent_det == TipEnt))))) && (current.Ctb_Ruc_dni_det == Ent))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void MostrarDiferencias()
        {
            try
            {
                decimal num = 0;
                decimal num2 = 0;
                num = Convert.ToDecimal( txtimporteMN.Text);

                if (Detalles.Count > 0 )
                {
                   foreach (Entidad_Movimiento_Cab cab in  Detalles)
                        {
                            if (!cab.Ctb_Es_Cuenta_Principal & !cab.Cta_Dif)
                            {
                                num2 += cab.Ctb_Importe_Debe + cab.Ctb_Importe_Haber;
                            }
                        }

                }

                 txtTotalImporteMNDet.Text = Convert.ToString(num2);
                 TxtDiferencias.Text = Convert.ToString((decimal)(num - ((Convert.ToDecimal( IIf( TxtDifCambio.Text == "", 0,  TxtDifCambio.Text)) + Convert.ToDecimal( IIf( TxtDifRedondeo.Text == "", 0,  TxtDifRedondeo.Text))) + num2)));
            }
            catch
            {
                 TxtDiferencias.Text = "0.0";
            }
        }


        private object IIf(bool expression, object truePart, object falsePart)
        {
            return (expression ? truePart : falsePart);
        }

        private void frm_caja_banco_edicion_Load(object sender, EventArgs e)
        {
             TraerLibro();
             BloquearDetalles();
             BloquearTipoCajaBanco();
             BloquearCompensacion();
            if ( Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if ( Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                 ListarModificar();
            }
        }


        public void TraerLibro()
        {
            try
            {
                List<Entidad_Parametro_Inicial> list = new List<Entidad_Parametro_Inicial>();
                Entidad_Parametro_Inicial inicial1 = new Entidad_Parametro_Inicial();
                inicial1.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                inicial1.Id_Anio = Actual_Conexion.AnioSelect;
                list = new Logica_Parametro_Inicial().Traer_Libro_CajaBanco(inicial1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("Debe configurar un libro contable para este proceso");
                }
                else
                {
                     txtlibro.Tag = list[0].Ini_CajaBanco;
                     txtlibro.Text = list[0].Ini_CajaBanco_Desc;
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }


        public void ListarModificar()
        {
            Entidad_Movimiento_Cab cab = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab cab2 = new Logica_Movimiento_Cab();
            cab.Id_Empresa =  Id_Empresa;
            cab.Id_Anio =  Id_Anio;
            cab.Id_Periodo =  Id_Periodo;
            cab.Id_Libro =  Id_Libro;
            cab.Id_Voucher =  Voucher;
            cab.Ctb_Tipo_IE =  Tipo_IE;
            try
            {
                 Lista_Modificar = cab2.Listar_Tesoreria(cab);
                if ( Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab cab3 = new Entidad_Movimiento_Cab();
                    cab3 =  Lista_Modificar[0];
                     Id_Empresa = cab3.Id_Empresa;
                     Id_Anio = cab3.Id_Anio;
                     Id_Periodo = cab3.Id_Periodo;
                     Id_Libro =  txtlibro.Tag.ToString();
                     Voucher = cab3.Id_Voucher;
                     txttipomovimientocod.Tag = cab3.Ctb_Tipo_Movimiento;
                     txttipomovimientocod.Text = cab3.Ctb_Tipo_Movimiento_Interno;
                     txttipomovimientodesc.Text = cab3.Ctb_Tipo_Movimiento_Desc;
                     txtglosa.Text = cab3.Ctb_Glosa;
                     txttipodoc.Text = cab3.Ctb_Tipo_Doc_Sunat;
                     txttipodoc.Tag = cab3.Ctb_Tipo_Doc;
                     txttipodocdesc.Text = cab3.Nombre_Comprobante;
                     txtserie.Text = cab3.Ctb_Serie + "-" + cab3.Ctb_Numero;
                     txtfechadoc.Text = string.Format("{0:yyyy-MM-dd}", cab3.Ctb_Fecha_Movimiento);
                     txttipoentcod.Text = cab3.Ctb_Tipo_Ent;
                     txttipoentdesc.Text = cab3.Ctb_Tipo_Ent_Desc;
                     txtrucdni.Text = cab3.Ctb_Ruc_dni;
                     txtentidad.Text = cab3.Entidad;
                     txtmedpago.Text = cab3.Ctb_Medio_Pago;
                     txtmedpagodesc.Text = cab3.Ctb_Medio_Pago_desc;
                     tctctacorriente.Text = cab3.Ctb_Cuenta_Corriente;
                     txtentfinanciera = cab3.Ctb_Entidad_Financiera;
                     txtentfinancieradesc = cab3.Ctb_Entidad_Financiera_Desc;
                     txtmonedacod.Text = cab3.Ctn_Moneda_Sunat;
                     txtmonedacod.Tag = cab3.Ctn_Moneda_Cod;
                     txtmonedadesc.Text = cab3.Ctn_Moneda_Desc;
                     txttipocambiocod.Text = cab3.Ctb_Tipo_Cambio_Cod;
                     txttipocambiodesc.Text = cab3.Ctb_Tipo_Cambio_desc;
                     txttipocambiovalor.Text = Convert.ToDecimal(cab3.Ctb_Tipo_Cambio_Valor).ToString();
                     Es_moneda_nac = cab3.Ctb_Es_moneda_nac;
                     txtimporte.Text = Convert.ToDecimal(cab3.Ctb_Importe).ToString();
                     txtflujoefectivocod.Text = cab3.Flujo_Efectivo_Cod;
                     txtflujoefectivodesc.Text = cab3.Flujo_Efectivo_Desc;
                     Cuenta_Contable_Cab = cab3.Tes_Cuenta;
                     txttipomovimientodesc.Text = cab3.Ctb_Tipo_Movimiento_Desc + " " + cab3.Tes_Cuenta;
                     ChkDifCambio.Checked = cab3.Tes_DifCam;
                     ChkDifRedondeo.Checked = cab3.Tes_DifRed;
                     TxtDifCambio.Text = Convert.ToString(cab3.Tes_DifCambio);
                     TxtDifRedondeo.Text = Convert.ToString(cab3.Tes_DifRedond);
                    Logica_Movimiento_Cab cab4 = new Logica_Movimiento_Cab();
                     dgvdatos.DataSource = null;
                     Detalles = cab4.Listar_Tesoreria_Det(cab3);
                    if ( Detalles.Count > 0)
                    {
                         dgvdatos.DataSource =  Detalles;
                    }
                     MostrarDiferencias();
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipomovimientocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipomovimientocod.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txttipomovimientocod.Text.Substring( txttipomovimientocod.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipomovimientocod.Text)) & string.IsNullOrEmpty( txttipomovimientodesc.Text))
                        {
                             BuscarMovimiento();
                        }
                    }
                    else
                    {
                        using (frm_generales_busqueda _busqueda = new frm_generales_busqueda())
                        {
                            _busqueda.Id_General = "0016";
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General general = new Entidad_General();
                                general = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txttipomovimientocod.Tag = general.Id_General_Det;
                                 txttipomovimientocod.Text = general.Gen_Codigo_Interno;
                                 txttipomovimientodesc.Text = general.Gen_Descripcion_Det;
                                if ( txttipomovimientocod.Text == "02")
                                {
                                     DesbloquearTipoCajaBanco();
                                }
                                else if ( txttipomovimientocod.Text == "03")
                                {
                                     DesbloquearCompensacion();
                                }
                                else
                                {
                                     BloquearTipoCajaBanco();
                                     BloquearCompensacion();
                                }
                                 txttipomovimientocod.EnterMoveNextControl = true;
                            }
                             Buscar_Cuenta_Por_proceso(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect,  txttipomovimientocod.Tag.ToString());
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }


        private void Buscar_Cuenta_Por_proceso(string empresa, string anio, string tag_proceso)
        {
            try
            {
                Entidad_Plan_Empresarial empresarial = new Entidad_Plan_Empresarial();
                List<Entidad_Plan_Empresarial> list = new List<Entidad_Plan_Empresarial>();
                Logica_Plan_Empresarial empresarial2 = new Logica_Plan_Empresarial();
                empresarial.Id_Empresa = empresa;
                empresarial.Id_Anio = anio;
                empresarial.Tag_Proceso = tag_proceso;
                list = empresarial2.Busqueda_Cta_Por_Proceso(empresarial);
                if (list.Count == 1)
                {
                     Cuenta_Contable_Cab = list[0].Id_Cuenta;
                     Cuenta_Contable_Desc = list[0].Cta_Descripcion;
                     txttipomovimientodesc.Text =  txttipomovimientodesc.Text + " " + list[0].Id_Cuenta;
                }
                else if (list.Count <= 1)
                {
                    if (list.Count == 0)
                    {
                        Accion.Advertencia("No existe una cuenta configurada para este proceso");
                         txttipomovimientocod.ResetText();
                         txttipomovimientodesc.ResetText();
                         txttipomovimientocod.EnterMoveNextControl = false;
                         txttipomovimientocod.Focus();
                    }
                }
                else
                {
                    using (frm_mas_cuentas_proceso _proceso = new frm_mas_cuentas_proceso())
                    {
                        _proceso.empresa = empresa;
                        _proceso.anio = anio;
                        _proceso.tag_proceso = tag_proceso;
                        if (_proceso.ShowDialog() == DialogResult.OK)
                        {
                            Entidad_Plan_Empresarial empresarial3 = new Entidad_Plan_Empresarial();
                            empresarial3 = _proceso.Lista[_proceso.gridView1.GetFocusedDataSourceRowIndex()];
                             Cuenta_Contable_Cab = empresarial3.Id_Cuenta;
                             Cuenta_Contable_Desc = empresarial3.Cta_Descripcion;
                             txttipomovimientodesc.Text =  txttipomovimientodesc.Text + " " + empresarial3.Id_Cuenta;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }




        private void DesbloquearTipoCajaBanco()
        {
             tctctacorriente.Enabled = true;
             txtnumetransaccion.Enabled = true;
            // chkITF.Enabled = true;
        }


        private void DesbloquearCompensacion()
        {
             txttipodoc.Enabled = true;
             txtserie.Enabled = true;
             txttipoentcod.Enabled = true;
             txtrucdni.Enabled = true;
             btnbuscarprovisioncab.Enabled = true;
        }




     





        public void BuscarMovimiento()
        {
            try
            {
                 txttipomovimientocod.Text = Accion.Formato( txttipomovimientocod.Text, 2);
                List<Entidad_General> list = new List<Entidad_General>();
                Entidad_General general1 = new Entidad_General();
                general1.Id_General = "0016";
                general1.Gen_Codigo_Interno =  txttipomovimientocod.Text;
                list = new Logica_General().Listar(general1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txttipomovimientocod.EnterMoveNextControl = false;
                     txttipomovimientocod.ResetText();
                     txttipomovimientocod.Focus();
                }
                else
                {
                    foreach (Entidad_General general2 in list)
                    {
                        if (general2.Gen_Codigo_Interno.ToString().Trim().ToUpper() ==  txttipomovimientocod.Text.Trim().ToUpper())
                        {
                             txttipomovimientocod.Tag = general2.Id_General_Det;
                             txttipomovimientocod.Text = general2.Gen_Codigo_Interno.ToString().Trim();
                             txttipomovimientodesc.Text = general2.Gen_Descripcion_Det;
                            if ( txttipomovimientocod.Text == "02")
                            {
                                 DesbloquearTipoCajaBanco();
                            }
                            else if ( txttipomovimientocod.Text == "03")
                            {
                                 DesbloquearCompensacion();
                            }
                            else
                            {
                                 BloquearTipoCajaBanco();
                                 BloquearCompensacion();
                            }
                             txttipomovimientocod.EnterMoveNextControl = true;
                        }
                    }
                     Buscar_Cuenta_Por_proceso(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect,  txttipomovimientocod.Tag.ToString());
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipomovimientocod_TextChanged(object sender, EventArgs e)
        {
            if (! txttipomovimientocod.Focus())
            {
                 txttipomovimientodesc.ResetText();
            }

        }

        private void txtmedpago_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtmedpago.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtmedpago.Text.Substring( txtmedpago.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtmedpago.Text)) & string.IsNullOrEmpty( txtmedpagodesc.Text))
                        {
                             BuscarMedioPago();
                        }
                    }
                    else
                    {
                        using (frm_medio_pago_busqueda _busqueda = new frm_medio_pago_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Medio_Pago pago = new Entidad_Medio_Pago();
                                pago = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txtmedpago.Text = pago.Id_SUNAT;
                                 txtmedpagodesc.Text = pago.Med_Descripcion;
                                 txtmedpago.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }


        public void BuscarMedioPago()
        {
            try
            {
                 txtmedpago.Text = Accion.Formato( txtmedpago.Text, 3);
                List<Entidad_Medio_Pago> list = new List<Entidad_Medio_Pago>();
                Entidad_Medio_Pago pago1 = new Entidad_Medio_Pago();
                pago1.Id_SUNAT =  txtmedpago.Text;
                list = new Logica_Medio_Pago().Listar(pago1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txtmedpago.EnterMoveNextControl = false;
                     txtmedpago.ResetText();
                     txtmedpago.Focus();
                }
                else
                {
                    foreach (Entidad_Medio_Pago pago2 in list)
                    {
                        if (pago2.Id_SUNAT.ToString().Trim().ToUpper() ==  txtmedpago.Text.Trim().ToUpper())
                        {
                             txtmedpago.Text = pago2.Id_SUNAT.ToString().Trim();
                             txtmedpagodesc.Text = pago2.Med_Descripcion;
                             txtmedpago.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txtmedpago_TextChanged(object sender, EventArgs e)
        {
            if (! txtmedpago.Focus())
            {
                 txtmedpagodesc.ResetText();
            }
        }

        private void tctctacorriente_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( tctctacorriente.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( tctctacorriente.Text.Substring( tctctacorriente.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( tctctacorriente.Text)) & string.IsNullOrEmpty( tctctacorriente.Text))
                        {
                             BuscarCtaCorriente();
                        }
                    }
                    else
                    {
                        using (frm_cuenta_corriente_busqueda _busqueda = new frm_cuenta_corriente_busqueda())
                        {
                            _busqueda.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            _busqueda.Id_Anio = Actual_Conexion.AnioSelect;
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Cuenta_Corriente corriente = new Entidad_Cuenta_Corriente();
                                corriente = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 tctctacorriente.Text = corriente.Id_Cuenta_Corriente;
                                 tctctacorriente.EnterMoveNextControl = true;
                                 txtentfinanciera = corriente.Cor_Ent_Financiera;
                                 txtentfinancieradesc = corriente.Fin_Descripcion;
                                 Cuenta_Cor = corriente.Cor_Cuenta_Contable;
                                 Cuenta_Cor_Desc = corriente.Cta_Descripcion;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }


        public void BuscarCtaCorriente()
        {
            try
            {
                List<Entidad_Cuenta_Corriente> list = new List<Entidad_Cuenta_Corriente>();
                Entidad_Cuenta_Corriente corriente1 = new Entidad_Cuenta_Corriente();
                corriente1.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                corriente1.Id_Anio = Actual_Conexion.AnioSelect;
                corriente1.Id_Cuenta_Corriente =  tctctacorriente.Text;
                list = new Logica_Cuenta_Corriente().Listar(corriente1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     tctctacorriente.EnterMoveNextControl = false;
                     tctctacorriente.ResetText();
                     tctctacorriente.Focus();
                }
                else
                {
                    foreach (Entidad_Cuenta_Corriente corriente2 in list)
                    {
                        if (corriente2.Id_Cuenta_Corriente.ToString().Trim().ToUpper() ==  tctctacorriente.Text.Trim().ToUpper())
                        {
                             tctctacorriente.Text = corriente2.Id_Cuenta_Corriente.ToString().Trim();
                             txtentfinanciera = corriente2.Cor_Ent_Financiera.ToString().Trim();
                             txtentfinancieradesc = corriente2.Fin_Descripcion.ToString().Trim();
                             Cuenta_Cor = corriente2.Cor_Cuenta_Contable.Trim();
                             Cuenta_Cor_Desc = corriente2.Cta_Descripcion.Trim();
                             tctctacorriente.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtmonedacod.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtmonedacod.Text.Substring( txtmonedacod.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtmonedacod.Text)) & string.IsNullOrEmpty( txtmonedadesc.Text))
                        {
                             BuscarMoneda();
                             VerificarMoneda();
                        }
                    }
                    else
                    {
                        using (frm_moneda_busqueda _busqueda = new frm_moneda_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda moneda = new Entidad_Moneda();
                                moneda = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 Es_moneda_nac = moneda.Es_nacional;
                                 txtmonedacod.Tag = moneda.Id_Moneda;
                                 txtmonedacod.Text = moneda.Id_Sunat;
                                 txtmonedadesc.Text = moneda.Nombre_Moneda;
                                 VerificarMoneda();
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }


        public void BuscarMoneda()
        {
            try
            {
                 txtmonedacod.Text = Accion.Formato( txtmonedacod.Text, 1);
                List<Entidad_Moneda> list = new List<Entidad_Moneda>();
                Entidad_Moneda moneda1 = new Entidad_Moneda();
                moneda1.Id_Sunat =  txtmonedacod.Text;
                list = new Logica_Moneda().Listar(moneda1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txtmonedacod.EnterMoveNextControl = false;
                     txtmonedacod.ResetText();
                     txtmonedacod.Focus();
                }
                else
                {
                    foreach (Entidad_Moneda moneda2 in list)
                    {
                        if (moneda2.Id_Sunat.ToString().Trim().ToUpper() ==  txtmonedacod.Text.Trim().ToUpper())
                        {
                             Es_moneda_nac = moneda2.Es_nacional;
                             txtmonedacod.Text = moneda2.Id_Sunat.ToString().Trim();
                             txtmonedacod.Tag = moneda2.Id_Moneda.ToString().Trim();
                             txtmonedadesc.Text = moneda2.Nombre_Moneda;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }


        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty( txtmonedacod.Text.Trim()))
            {
                if (! Es_moneda_nac)
                {
                     Traer_Venta_Publicacion();
                }
                else
                {
                     txttipocambiocod.Enabled = false;
                     txttipocambiocod.Text = "SCV";
                     txttipocambiodesc.Text = "SIN CONVERSION";
                     txttipocambiovalor.Enabled = false;
                     txttipocambiovalor.Text = "1.000";
                }
            }
        }


         void Traer_Venta_Publicacion()
        {
            try
            {
                if ( txtmonedacod.Text == "2")
                {
                     txttipocambiovalor.Enabled = true;
                     txttipocambiocod.Enabled = true;
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipocambiocod.Text))
            {
                if (!IsDate( txtfechadoc.Text))
                {
                    Accion.Advertencia("Debe ingresar primero una fecha v\x00e1lida para obtener un Tipo de Cambio con esa fecha");
                     txtfechadoc.Focus();
                }
                else
                {
                    try
                    {
                        if ((e.KeyCode == Keys.Enter) & ( txttipocambiocod.Text.Substring( txttipocambiocod.Text.Length - 1, 1) == "*"))
                        {
                            using (frm_tipo_cambio_busqueda _busqueda = new frm_tipo_cambio_busqueda())
                            {
                                _busqueda.FechaTransac = Convert.ToDateTime( txtfechadoc.Text);
                                if (_busqueda.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio cambio = new Entidad_Tipo_Cambio();
                                    cambio = _busqueda.ListaTipoCambio[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                     txttipocambiocod.Text = cambio.Codigo;
                                     txttipocambiodesc.Text = cambio.Nombre;
                                     txttipocambiovalor.Text = Convert.ToDouble(cambio.Valor).ToString();
                                     txttipocambiovalor.Enabled =  txtmonedacod.Text == "2";
                                     txttipocambiocod.EnterMoveNextControl = true;
                                }
                            }
                        }
                        else if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipocambiocod.Text)) & string.IsNullOrEmpty( txttipocambiodesc.Text))
                        {
                            try
                            {
                                List<Entidad_Tipo_Cambio> list = new List<Entidad_Tipo_Cambio>();
                                Entidad_Tipo_Cambio cambio1 = new Entidad_Tipo_Cambio();
                                cambio1.Tic_Fecha = Convert.ToDateTime( txtfechadoc.Text);
                                list = new Logica_Tipo_Cambio().Buscar_Tipo_Cambio(cambio1);
                                if (list.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio cambio3 in list)
                                    {
                                        if (cambio3.Codigo.ToString().Trim().ToUpper() ==  txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                             txttipocambiocod.Text = cambio3.Codigo;
                                             txttipocambiodesc.Text = cambio3.Nombre;
                                             txttipocambiovalor.Text = Convert.ToDouble(cambio3.Valor).ToString();
                                             txttipocambiovalor.Enabled =  txtmonedacod.Text == "2";
                                             txttipocambiocod.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception exception1)
                            {
                                Accion.ErrorSistema(exception1.Message);
                            }
                        }
                    }
                    catch (Exception exception3)
                    {
                        Accion.ErrorSistema(exception3.Message);
                    }
                }
            }

        }



        public static bool IsDate(string date)
        {
            try
            {
                DateTime time = DateTime.Parse(date);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipodoc.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txttipodoc.Text.Substring( txttipodoc.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipodoc.Text)) & string.IsNullOrEmpty( txttipodocdesc.Text))
                        {
                             BuscarTipoDocumento();
                        }
                    }
                    else
                    {
                        using (frm_series_tesoreria_busqueda _busqueda = new frm_series_tesoreria_busqueda())
                        {
                            _busqueda.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Series_Tesorerira tesorerira = new Entidad_Series_Tesorerira();
                                tesorerira = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txttipodoc.Text = tesorerira.Ser_Tipo_Doc_Sunat;
                                 txttipodoc_ = tesorerira.Ser_Tipo_Doc;
                                 txttipodocdesc.Text = tesorerira.Ser_Tipo_Doc_Des;
                                 txtserie.Text = tesorerira.Ser_Serie;
                                 Numero =  IIf(tesorerira.Ser_Numero_Actual.Trim() == "", 0, tesorerira.Ser_Numero_Actual.Trim()).ToString();
                                 AumentarNumero();
                                 txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        public void BuscarTipoDocumento()
        {
            try
            {
                 txttipodoc.Text = Accion.Formato( txttipodoc.Text, 2);
                List<Entidad_Series_Tesorerira> list = new List<Entidad_Series_Tesorerira>();
                Entidad_Series_Tesorerira tesorerira1 = new Entidad_Series_Tesorerira();
                tesorerira1.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                tesorerira1.Ser_Cta_Corriente =  tctctacorriente.Text;
                tesorerira1.Ser_Tipo_Doc =  txttipodoc.Text;
                tesorerira1.Ser_Serie =  txtserie.Text;
                list = new Logica_Series_Tesoreria().Listar(tesorerira1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txttipodoc.EnterMoveNextControl = false;
                     txttipodoc.ResetText();
                     txttipodoc.Focus();
                }
                else
                {
                    foreach (Entidad_Series_Tesorerira tesorerira in list)
                    {
                        if (tesorerira.Ser_Tipo_Doc_Sunat.ToString().Trim().ToUpper() ==  txttipodoc.Text.Trim().ToUpper())
                        {
                             txttipodoc.Text = tesorerira.Ser_Tipo_Doc_Sunat.ToString().Trim();
                             txttipodoc_ = tesorerira.Ser_Tipo_Doc.ToString().Trim();
                             txttipodocdesc.Text = tesorerira.Ser_Tipo_Doc_Des;
                             txtserie.Text = tesorerira.Ser_Serie;
                             Numero =  IIf(tesorerira.Ser_Numero_Actual.Trim() == "", 0, tesorerira.Ser_Numero_Actual.Trim()).ToString();
                             AumentarNumero();
                             txttipodoc.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (! txttipocambiocod.Focus())
            {
                 txttipocambiodesc.ResetText();
                 txttipocambiovalor.ResetText();
            }

        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (! txttipodoc.Focus())
            {
                 txttipodocdesc.ResetText();
            }

        }

        private void txtserie_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty( txtserie.Text))
                {
                    if (! txtserie.Text.Trim().Contains("-"))
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                         txtserie.Focus();
                    }
                    else
                    {
                        char[] separator = new char[] { Convert.ToChar("-") };
                        string[] strArray =  txtserie.Text.Split(separator);
                         txtserie.Text = Accion.Formato(strArray[0].Trim(), 4).ToString() + "-" + Accion.Formato(strArray[1].Trim(), 8).ToString();
                         txtserie.EnterMoveNextControl = true;
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void txttipoentcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipoentcod.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txttipoentcod.Text.Substring( txttipoentcod.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipoentcod.Text)) & string.IsNullOrEmpty( txttipoentdesc.Text))
                        {
                             BuscarTipoEntidad();
                        }
                    }
                    else
                    {
                        using (frm_tipo_entidad_busqueda _busqueda = new frm_tipo_entidad_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad entidad = new Entidad_Entidad();
                                entidad = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txttipoentcod.Text = entidad.Id_Tipo_Ent;
                                 txttipoentdesc.Text = entidad.Ent_Descripcion;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }


        public void BuscarTipoEntidad()
        {
            try
            {
                List<Entidad_Entidad> list = new List<Entidad_Entidad>();
                Entidad_Entidad entidad1 = new Entidad_Entidad();
                entidad1.Id_Tipo_Ent =  txttipoentcod.Text;
                list = new Logica_Entidad().Listar(entidad1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
                else
                {
                    foreach (Entidad_Entidad entidad2 in list)
                    {
                        if (entidad2.Id_Tipo_Ent.ToString().Trim().ToUpper() ==  txttipoentcod.Text.Trim().ToUpper())
                        {
                             txttipoentcod.Text = entidad2.Ent_RUC_DNI.ToString().Trim();
                             txttipoentdesc.Text = entidad2.Ent_Descripcion;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipoentcod_TextChanged(object sender, EventArgs e)
        {
            if (! txttipoentcod.Focus())
            {
                 txttipoentdesc.ResetText();
            }

        }

        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtrucdni.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtrucdni.Text.Substring( txtrucdni.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtrucdni.Text)) & string.IsNullOrEmpty( txtentidad.Text))
                        {
                             BuscarEntidad();
                        }
                    }
                    else
                    {
                        using (frm_entidades_busqueda _busqueda = new frm_entidades_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad entidad = new Entidad_Entidad();
                                entidad = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txtrucdni.Text = entidad.Ent_RUC_DNI;
                                string[] textArray1 = new string[] { entidad.Ent_Ape_Materno, " ", entidad.Ent_Ape_Materno, " ", entidad.Ent_Razon_Social_Nombre };
                                 txtentidad.Text = string.Concat(textArray1);
                                 txtrucdni.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtrucdni_TextChanged(object sender, EventArgs e)
        {
            if (! txtrucdni.Focus())
            {
                 txtentidad.ResetText();
            }

        }


        public void BuscarEntidad()
        {
            try
            {
                List<Entidad_Entidad> list = new List<Entidad_Entidad>();
                Entidad_Entidad entidad1 = new Entidad_Entidad();
                entidad1.Ent_RUC_DNI =  txtrucdni.Text;
                list = new Logica_Entidad().Listar(entidad1);
                if (list.Count > 0)
                {
                    foreach (Entidad_Entidad entidad2 in list)
                    {
                        if (entidad2.Ent_RUC_DNI.ToString().Trim().ToUpper() ==  txtrucdni.Text.Trim().ToUpper())
                        {
                             txtrucdni.Text = entidad2.Ent_RUC_DNI.ToString().Trim();
                            string[] textArray1 = new string[] { entidad2.Ent_Ape_Paterno, " ", entidad2.Ent_Ape_Materno, " ", entidad2.Ent_Razon_Social_Nombre };
                             txtentidad.Text = string.Concat(textArray1);
                             txtrucdni.EnterMoveNextControl = true;
                        }
                    }
                }
                else
                {
                    using (frm_entidad_edicion _edicion = new frm_entidad_edicion())
                    {
                        _edicion.txtnumero.Text =  txtrucdni.Text;
                        _edicion.Estado_Ven_Boton = "1";
                        if (_edicion.ShowDialog(this) != DialogResult.OK)
                        {
                            List<Entidad_Entidad> list2 = new List<Entidad_Entidad>();
                            Entidad_Entidad entidad5 = new Entidad_Entidad();
                            entidad5.Ent_RUC_DNI =  txtrucdni.Text;
                            list2 = new Logica_Entidad().Listar(entidad5);
                            if (list2.Count <= 0)
                            {
                                 txtrucdni.EnterMoveNextControl = false;
                                 txtrucdni.ResetText();
                                 txtrucdni.Focus();
                            }
                            else
                            {
                                foreach (Entidad_Entidad entidad4 in list2)
                                {
                                    if (entidad4.Ent_RUC_DNI.ToString().Trim().ToUpper() ==  txtrucdni.Text.Trim().ToUpper())
                                    {
                                         txtrucdni.Text = entidad4.Ent_RUC_DNI.ToString().Trim();
                                        string[] textArray2 = new string[] { entidad4.Ent_Ape_Paterno, " ", entidad4.Ent_Ape_Materno, " ", entidad4.Ent_Razon_Social_Nombre };
                                         txtentidad.Text = string.Concat(textArray2);
                                         txtrucdni.EnterMoveNextControl = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txtflujoefectivocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtflujoefectivocod.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtflujoefectivocod.Text.Substring( txtflujoefectivocod.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtflujoefectivocod.Text)) & string.IsNullOrEmpty( txtflujoefectivodesc.Text))
                        {
                             BuscarFlujoEfectivo();
                        }
                    }
                    else
                    {
                        using (frm_flujo_efectivo_busqueda _busqueda = new frm_flujo_efectivo_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Flujo_Efectivo efectivo = new Entidad_Flujo_Efectivo();
                                efectivo = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txtflujoefectivocod.Text = efectivo.Flujo_Efectivo_Cod;
                                 txtflujoefectivodesc.Text = efectivo.Flujo_Efectivo_Desc;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtflujoefectivocod_TextChanged(object sender, EventArgs e)
        {
            if (! txtflujoefectivocod.Focus())
            {
                 txtflujoefectivodesc.ResetText();
            }

        }


        public void BuscarFlujoEfectivo()
        {
            try
            {
                 txtflujoefectivocod.Text = Accion.Formato( txtflujoefectivocod.Text, 6);
                List<Entidad_Flujo_Efectivo> list = new List<Entidad_Flujo_Efectivo>();
                Entidad_Flujo_Efectivo efectivo1 = new Entidad_Flujo_Efectivo();
                efectivo1.Codigo =  txtflujoefectivocod.Text;
                list = new Logica_Flujo_Efectivo().Patrimonio_Neto_Listar(efectivo1);
                if (list.Count > 0)
                {
                    foreach (Entidad_Flujo_Efectivo efectivo2 in list)
                    {
                        if (efectivo2.Codigo.ToString().Trim().ToUpper() ==  txtflujoefectivocod.Text.Trim().ToUpper())
                        {
                             txtflujoefectivocod.Text = efectivo2.Flujo_Efectivo_Cod.ToString().Trim();
                             txtflujoefectivodesc.Text = efectivo2.Flujo_Efectivo_Desc;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtcuenta.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtcuenta.Text.Substring( txtcuenta.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtcuenta.Text)) & string.IsNullOrEmpty( txtcuentadesc.Text))
                        {
                             Cuenta();
                        }
                    }
                    else
                    {
                        using (frm_plan_empresarial_busqueda _busqueda = new frm_plan_empresarial_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial empresarial = new Entidad_Plan_Empresarial();
                                empresarial = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txtcuenta.Text = empresarial.Id_Cuenta;
                                 txtcuentadesc.Text = empresarial.Cta_Descripcion;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            if (! txtcuenta.Focus())
            {
                 txtcuentadesc.ResetText();
            }

        }


        public void Cuenta()
        {
            try
            {
                List<Entidad_Plan_Empresarial> list = new List<Entidad_Plan_Empresarial>();
                Entidad_Plan_Empresarial empresarial1 = new Entidad_Plan_Empresarial();
                empresarial1.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                empresarial1.Id_Cuenta =  txtcuenta.Text;
                list = new Logica_Plan_Empresarial().Busqueda(empresarial1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txtcuenta.EnterMoveNextControl = false;
                     txtcuenta.Focus();
                }
                else
                {
                    foreach (Entidad_Plan_Empresarial empresarial2 in list)
                    {
                        if (empresarial2.Id_Cuenta.ToString().Trim().ToUpper() ==  txtcuenta.Text.Trim().ToUpper())
                        {
                             txtcuenta.Text = empresarial2.Id_Cuenta.ToString().Trim();
                             txtcuentadesc.Text = empresarial2.Cta_Descripcion;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txtrucdnidet_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtrucdnidet.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtrucdnidet.Text.Substring( txtrucdnidet.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtrucdnidet.Text)) & string.IsNullOrEmpty( txtentidaddet.Text))
                        {
                             BuscarEntidadDet();
                        }
                    }
                    else
                    {
                        using (frm_entidades_busqueda _busqueda = new frm_entidades_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad entidad = new Entidad_Entidad();
                                entidad = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txtrucdnidet.Text = entidad.Ent_RUC_DNI;
                                string[] textArray1 = new string[] { entidad.Ent_Ape_Materno, " ", entidad.Ent_Ape_Materno, " ", entidad.Ent_Razon_Social_Nombre };
                                 txtentidaddet.Text = string.Concat(textArray1);
                                 txtrucdnidet.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtrucdnidet_TextChanged(object sender, EventArgs e)
        {
            if (! txtrucdnidet.Focus())
            {
                 txtentidaddet.ResetText();
            }

        }


        public void BuscarEntidadDet()
        {
            try
            {
                List<Entidad_Entidad> list = new List<Entidad_Entidad>();
                Entidad_Entidad entidad1 = new Entidad_Entidad();
                entidad1.Ent_RUC_DNI =  txtrucdnidet.Text;
                list = new Logica_Entidad().Listar(entidad1);
                if (list.Count > 0)
                {
                    foreach (Entidad_Entidad entidad2 in list)
                    {
                        if (entidad2.Ent_RUC_DNI.ToString().Trim().ToUpper() ==  txtrucdnidet.Text.Trim().ToUpper())
                        {
                             txtrucdnidet.Text = entidad2.Ent_RUC_DNI.ToString().Trim();
                            string[] textArray1 = new string[] { entidad2.Ent_Ape_Paterno, " ", entidad2.Ent_Ape_Materno, " ", entidad2.Ent_Razon_Social_Nombre };
                             txtentidaddet.Text = string.Concat(textArray1);
                             txtrucdnidet.EnterMoveNextControl = true;
                        }
                    }
                }
                else
                {
                    using (frm_entidad_edicion _edicion = new frm_entidad_edicion())
                    {
                        _edicion.txtnumero.Text =  txtrucdnidet.Text;
                        _edicion.Estado_Ven_Boton = "1";
                        if (_edicion.ShowDialog(this) != DialogResult.OK)
                        {
                            List<Entidad_Entidad> list2 = new List<Entidad_Entidad>();
                            Entidad_Entidad entidad5 = new Entidad_Entidad();
                            entidad5.Ent_RUC_DNI =  txtrucdnidet.Text;
                            list2 = new Logica_Entidad().Listar(entidad5);
                            if (list2.Count <= 0)
                            {
                                 txtrucdnidet.EnterMoveNextControl = false;
                                 txtrucdnidet.ResetText();
                                 txtrucdnidet.Focus();
                            }
                            else
                            {
                                foreach (Entidad_Entidad entidad4 in list2)
                                {
                                    if (entidad4.Ent_RUC_DNI.ToString().Trim().ToUpper() ==  txtrucdnidet.Text.Trim().ToUpper())
                                    {
                                         txtrucdnidet.Text = entidad4.Ent_RUC_DNI.ToString().Trim();
                                        string[] textArray2 = new string[] { entidad4.Ent_Ape_Paterno, " ", entidad4.Ent_Ape_Materno, " ", entidad4.Ent_Razon_Social_Nombre };
                                         txtentidaddet.Text = string.Concat(textArray2);
                                         txtrucdnidet.EnterMoveNextControl = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipodocdet_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipodocdet.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txttipodocdet.Text.Substring( txttipodocdet.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipodocdet.Text)) & string.IsNullOrEmpty( txttipodocdescdet.Text))
                        {
                             BuscarTipoDocumentoDet();
                        }
                    }
                    else
                    {
                        using (frm_tipo_documento_busqueda _busqueda = new frm_tipo_documento_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes comprobantes = new Entidad_Comprobantes();
                                comprobantes = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txttipodocdet.Text = comprobantes.Id_SUnat;
                                 txttipodocdet.Tag = comprobantes.Id_Comprobante;
                                 txttipodocdescdet.Text = comprobantes.Nombre_Comprobante;
                                 txttipodocdet.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }


        public void BuscarTipoDocumentoDet()
        {
            try
            {
                 txttipodocdet.Text = Accion.Formato( txttipodocdet.Text, 2);
                List<Entidad_Comprobantes> list = new List<Entidad_Comprobantes>();
                Entidad_Comprobantes comprobantes1 = new Entidad_Comprobantes();
                comprobantes1.Id_SUnat =  txttipodocdet.Text;
                list = new Logica_Comprobante().Listar(comprobantes1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txttipodocdet.EnterMoveNextControl = false;
                     txttipodocdet.ResetText();
                     txttipodocdet.Focus();
                }
                else
                {
                    foreach (Entidad_Comprobantes comprobantes in list)
                    {
                        if (comprobantes.Id_SUnat.ToString().Trim().ToUpper() ==  txttipodocdet.Text.Trim().ToUpper())
                        {
                             txttipodocdet.Text = comprobantes.Id_SUnat.ToString().Trim();
                             txttipodocdet.Tag = comprobantes.Id_Comprobante.ToString().Trim();
                             txttipodocdescdet.Text = comprobantes.Nombre_Comprobante;
                             txttipodocdet.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txttipodocdet_TextChanged(object sender, EventArgs e)
        {
            if (! txttipodocdet.Focus())
            {
                 txttipodocdescdet.ResetText();
            }

        }

        private void txtseriedet_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty( txtseriedet.Text))
                {
                    if (! txtseriedet.Text.Trim().Contains("-"))
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                         txtseriedet.Focus();
                    }
                    else
                    {
                        char[] separator = new char[] { Convert.ToChar("-") };
                        string[] strArray =  txtseriedet.Text.Split(separator);
                         txtseriedet.Text = Accion.Formato(strArray[0].Trim(), 4).ToString() + "-" + Accion.Formato(strArray[1].Trim(), 8).ToString();
                         txtseriedet.EnterMoveNextControl = true;
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void txtmedpagodet_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txtmedpagodet.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtmedpagodet.Text.Substring( txtmedpagodet.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtmedpagodet.Text)) & string.IsNullOrEmpty( txtmedpagodescdet.Text))
                        {
                             BuscarMedioPagoDet();
                        }
                    }
                    else
                    {
                        using (frm_medio_pago_busqueda _busqueda = new frm_medio_pago_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Medio_Pago pago = new Entidad_Medio_Pago();
                                pago = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txtmedpagodet.Text = pago.Id_SUNAT;
                                 txtmedpagodescdet.Text = pago.Med_Descripcion;
                                 txtmedpagodet.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtmedpagodet_TextChanged(object sender, EventArgs e)
        {
            if (! txtmedpagodet.Focus())
            {
                 txtmedpagodescdet.ResetText();
            }

        }

        public void BuscarMedioPagoDet()
        {
            try
            {
                 txtmedpagodet.Text = Accion.Formato( txtmedpagodet.Text, 3);
                List<Entidad_Medio_Pago> list = new List<Entidad_Medio_Pago>();
                Entidad_Medio_Pago pago1 = new Entidad_Medio_Pago();
                pago1.Id_SUNAT =  txtmedpagodet.Text;
                list = new Logica_Medio_Pago().Listar(pago1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txtmedpagodet.EnterMoveNextControl = false;
                     txtmedpagodet.ResetText();
                     txtmedpagodet.Focus();
                }
                else
                {
                    foreach (Entidad_Medio_Pago pago2 in list)
                    {
                        if (pago2.Id_SUNAT.ToString().Trim().ToUpper() ==  txtmedpagodet.Text.Trim().ToUpper())
                        {
                             txtmedpagodet.Text = pago2.Id_SUNAT.ToString().Trim();
                             txtmedpagodescdet.Text = pago2.Med_Descripcion;
                             txtmedpagodet.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void txtmonedacoddet_KeyDown(object sender, KeyEventArgs e)
        {

            if (!string.IsNullOrEmpty( txtmonedacoddet.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txtmonedacoddet.Text.Substring( txtmonedacoddet.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txtmonedacoddet.Text)) & string.IsNullOrEmpty( txtmonedadescdet.Text))
                        {
                             BuscarMonedaDet();
                             VerificarMonedaDet();
                        }
                    }
                    else
                    {
                        using (frm_moneda_busqueda _busqueda = new frm_moneda_busqueda())
                        {
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda moneda = new Entidad_Moneda();
                                moneda = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 Es_moneda_nacDet = moneda.Es_nacional;
                                 txtmonedacoddet.Text = moneda.Id_Sunat;
                                 txtmonedacoddet.Tag = moneda.Id_Moneda;
                                 txtmonedadescdet.Text = moneda.Nombre_Moneda;
                                 VerificarMonedaDet();
                                 txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }
        }

        private void txtmonedacoddet_TextChanged(object sender, EventArgs e)
        {
            if (! txtmonedacoddet.Focus())
            {
                 txtmonedadescdet.ResetText();
            }

        }

        public void BuscarMonedaDet()
        {
            try
            {
                 txtmonedacoddet.Text = Accion.Formato( txtmonedacoddet.Text, 3);
                List<Entidad_Moneda> list = new List<Entidad_Moneda>();
                Entidad_Moneda moneda1 = new Entidad_Moneda();
                moneda1.Id_Moneda =  txtmonedacoddet.Text;
                list = new Logica_Moneda().Listar(moneda1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txtmonedacoddet.EnterMoveNextControl = false;
                     txtmonedacoddet.ResetText();
                     txtmonedacoddet.Focus();
                }
                else
                {
                    foreach (Entidad_Moneda moneda2 in list)
                    {
                        if (moneda2.Id_Moneda.ToString().Trim().ToUpper() ==  txtmonedacoddet.Text.Trim().ToUpper())
                        {
                             Es_moneda_nacDet = moneda2.Es_nacional;
                             txtmonedacoddet.Text = moneda2.Id_Sunat;
                             txtmonedacoddet.Tag = moneda2.Id_Moneda.ToString().Trim();
                             txtmonedadescdet.Text = moneda2.Nombre_Moneda;
                             txtmonedacoddet.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }




        public void VerificarMonedaDet()
        {
            if (!string.IsNullOrEmpty( txtmonedacoddet.Text.Trim()))
            {
                if ( Es_moneda_nacDet)
                {
                     txttipocambiocoddet.Enabled = false;
                     txttipocambiocoddet.Text = "SCV";
                     txttipocambiodescdet.Text = "SIN CONVERSION";
                     txttipocambiovalordet.Enabled = false;
                     txttipocambiovalordet.Text = "1.000";
                }
                else if (string.IsNullOrEmpty( txttipocambiocoddet.Text))
                {
                     txttipocambiocoddet.Enabled = true;
                     txttipocambiocoddet.ResetText();
                     txttipocambiodescdet.ResetText();
                     txttipocambiovalordet.ResetText();
                }
            }
        }

        private void txttipocambiocoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipocambiocoddet.Text))
            {
                if (!IsDate( txtfechadoc.Text))
                {
                    Accion.Advertencia("Debe ingresar primero una fecha v\x00e1lida para obtener un Tipo de Cambio con esa fecha");
                }
                else
                {
                    try
                    {
                        if ((e.KeyCode == Keys.Enter) & ( txttipocambiocoddet.Text.Substring( txttipocambiocoddet.Text.Length - 1, 1) == "*"))
                        {
                            using (frm_tipo_cambio_busqueda _busqueda = new frm_tipo_cambio_busqueda())
                            {
                                _busqueda.FechaTransac = Convert.ToDateTime( txtfechadoc.Text);
                                if (_busqueda.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio cambio = new Entidad_Tipo_Cambio();
                                    cambio = _busqueda.ListaTipoCambio[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                     txttipocambiocoddet.Text = cambio.Codigo;
                                     txttipocambiodescdet.Text = cambio.Nombre;
                                     txttipocambiovalordet.Text = Convert.ToDouble(cambio.Valor).ToString();
                                     txttipocambiovalordet.Enabled =  txtmonedacoddet.Text == "2";
                                     txttipocambiocoddet.EnterMoveNextControl = true;
                                }
                            }
                        }
                        else if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipocambiocoddet.Text)) & string.IsNullOrEmpty( txttipocambiodescdet.Text))
                        {
                            try
                            {
                                List<Entidad_Tipo_Cambio> list = new List<Entidad_Tipo_Cambio>();
                                Entidad_Tipo_Cambio cambio1 = new Entidad_Tipo_Cambio();
                                cambio1.Tic_Fecha = Convert.ToDateTime( txtfechadoc.Text);
                                list = new Logica_Tipo_Cambio().Buscar_Tipo_Cambio(cambio1);
                                if (list.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio cambio3 in list)
                                    {
                                        if (cambio3.Codigo.ToString().Trim().ToUpper() ==  txttipocambiocoddet.Text.Trim().ToUpper())
                                        {
                                             txttipocambiocoddet.Text = cambio3.Codigo;
                                             txttipocambiodescdet.Text = cambio3.Nombre;
                                             txttipocambiovalordet.Text = Convert.ToDouble(cambio3.Valor).ToString();
                                             txttipocambiovalordet.Enabled =  txtmonedacoddet.Text == "2";
                                             txttipocambiocoddet.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception exception1)
                            {
                                Accion.ErrorSistema(exception1.Message);
                            }
                        }
                    }
                    catch (Exception exception3)
                    {
                        Accion.ErrorSistema(exception3.Message);
                    }
                }
            }

        }

        private void txttipocambiocoddet_TextChanged(object sender, EventArgs e)
        {
            if (! txttipocambiocoddet.Focus())
            {
                 txttipocambiodescdet.ResetText();
                 txttipocambiovalordet.ResetText();
            }

        }

        private void txttipo_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty( txttipo.Text))
            {
                try
                {
                    if (!((e.KeyCode == Keys.Enter) & ( txttipo.Text.Substring( txttipo.Text.Length - 1, 1) == "*")))
                    {
                        if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty( txttipo.Text)) & string.IsNullOrEmpty( txttipodesc.Text))
                        {
                             BuscarTipo();
                        }
                    }
                    else
                    {
                        using (frm_generales_busqueda _busqueda = new frm_generales_busqueda())
                        {
                            _busqueda.Id_General = "0002";
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General general = new Entidad_General();
                                general = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                 txttipo.Tag = general.Id_General_Det;
                                 txttipo.Text = general.Gen_Codigo_Interno;
                                 txttipodesc.Text = general.Gen_Descripcion_Det;
                                 txttipo.EnterMoveNextControl = true;
                            }
                        }
                    }
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txttipo_TextChanged(object sender, EventArgs e)
        {
            if (! txttipo.Focus())
            {
                 txttipodesc.ResetText();
            }
        }

        public void BuscarTipo()
        {
            try
            {
                 txttipo.Text = Accion.Formato( txttipo.Text, 2);
                List<Entidad_General> list = new List<Entidad_General>();
                Entidad_General general1 = new Entidad_General();
                general1.Id_General = "0002";
                general1.Gen_Codigo_Interno =  txttipo.Text;
                list = new Logica_General().Listar(general1);
                if (list.Count <= 0)
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                     txttipo.EnterMoveNextControl = false;
                     txttipo.ResetText();
                     txttipo.Focus();
                }
                else
                {
                    foreach (Entidad_General general2 in list)
                    {
                        if (general2.Gen_Codigo_Interno.ToString().Trim().ToUpper() ==  txttipo.Text.Trim().ToUpper())
                        {
                             txttipo.Tag = general2.Id_General_Det;
                             txttipo.Text = general2.Gen_Codigo_Interno.ToString().Trim();
                             txttipodesc.Text = general2.Gen_Descripcion_Det;
                             txttipo.EnterMoveNextControl = true;
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }



        public bool VerificarNuevoDet()
        {
            return true;
        }



        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            if ( VerificarNuevoDet())
            {
                base.EstadoDetalle = Estados.Nuevo;
                 LimpiarDet();
                 ImporteNoMay = 0;
                 HabilitarDetalles();
                 Id_Item =  Detalles.Count + 1;
                 btnnuevodet.Enabled = false;
                 btneditardet.Enabled = false;
                 btnquitardet.Enabled = false;
                 btnanadirdet.Enabled = true;
                 txtcuenta.Focus();
            }

        }

        void HabilitarDetalles()
        {
             txtcuenta.Enabled = true;
             txttipodocdet.Enabled = true;
             txtseriedet.Enabled = true;
             txtmonedacoddet.Enabled = true;
             txttipocambiocoddet.Enabled = true;
             txttipocambiovalordet.Enabled = true;
             txtrucdnidet.Enabled = true;
             txtmedpagodet.Enabled = true;
             txttipo.Enabled = true;
             txtimporteMNDet.Enabled = true;
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {

            EstadoDetalle = Estados.Modificar;
            HabilitarDetalles();
            btnanadirdet.Enabled = true;
            btneditardet.Enabled = false;
            btnquitardet.Enabled = true;
            btnnuevodet.Enabled = true;
            txtcuenta.Focus();

            //try
            //{
            //    if ( Detalles.Count > 0)
            //    {
            //         Detalles.RemoveAt( gridView1.GetFocusedDataSourceRowIndex());
            //         dgvdatos.DataSource = null;
            //        if ( Detalles.Count > 0)
            //        {
            //             dgvdatos.DataSource =  Detalles;
            //             RefreshNumeral();
            //        }
            //          EstadoDetalle = Estados.Ninguno;
            //         LimpiarDet();
            //         BloquearDetalles();
            //         btnanadirdet.Enabled = false;
            //         btneditardet.Enabled = false;
            //         btnquitardet.Enabled = false;
            //         btnnuevodet.Enabled = true;
            //    }
            //}
            //catch (Exception exception1)
            //{
            //    Accion.ErrorSistema(exception1.Message);
            //}

        }

        public void RefreshNumeral()
        {
            try
            {
                int num = 1;
                foreach (Entidad_Movimiento_Cab cab in  Detalles)
                {
                    cab.Id_Item = num;
                    num++;
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if ( Detalles.Count > 0)
                {
                     Detalles.RemoveAt( gridView1.GetFocusedDataSourceRowIndex());
                     dgvdatos.DataSource = null;
                    if ( Detalles.Count > 0)
                    {
                         dgvdatos.DataSource =  Detalles;
                         RefreshNumeral();
                    }
                    base.EstadoDetalle = Estados.Ninguno;
                     LimpiarDet();
                     BloquearDetalles();
                     btnanadirdet.Enabled = false;
                     btneditardet.Enabled = false;
                     btnquitardet.Enabled = false;
                     btnnuevodet.Enabled = true;

                    MostrarDiferencias();

                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void txtimporte_TextChanged(object sender, EventArgs e)
        {
             MostrarImporteMN();
             MostrarDiferencias();

        }

         void MostrarImporteMN()
        {
            try
            {
                 txtimporteMN.Text = (Convert.ToDecimal( txtimporte.Text) * Convert.ToDecimal( txttipocambiovalor.Text)).ToString();
            }
            catch (Exception)
            {
                 txtimporteMN.Text = "0.00";
            }
        }

        private void ChkDifCambio_CheckedChanged(object sender, EventArgs e)
        {
             BloquearDiferencias();
        }

        private void ChkDifRedondeo_CheckedChanged(object sender, EventArgs e)
        {
             BloquearDiferencias();

        }

        private void TxtDifCambio_TextChanged(object sender, EventArgs e)
        {
             MostrarDiferencias();

        }

        private void TxtDifRedondeo_TextChanged(object sender, EventArgs e)
        {
             MostrarDiferencias();

        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //try
            //{
            //    if ( VerificarCabecera())
            //    {
            //        Entidad_Movimiento_Cab cab = new Entidad_Movimiento_Cab();
            //        Logica_Movimiento_Cab cab2 = new Logica_Movimiento_Cab();
            //        cab.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //        cab.Id_Anio = Actual_Conexion.AnioSelect;
            //        cab.Id_Periodo =  Id_Periodo;
            //        cab.Id_Libro =  txtlibro.Tag.ToString();
            //        cab.Id_Voucher =  Voucher;
            //        cab.Ctb_Tipo_Movimiento =  txttipomovimientocod.Tag.ToString();
            //        cab.Ctb_Glosa =  txtglosa.Text;
            //        cab.Ctb_Tipo_Doc =  txttipodoc_;
            //        string text =  txtserie.Text;
            //        if ( txtserie.Text != "")
            //        {
            //            char[] separator = new char[] { Convert.ToChar("-") };
            //            string[] strArray = text.Split(separator);
            //            cab.Ctb_Serie = Accion.Formato(strArray[0].Trim(), 4).ToString();
            //            cab.Ctb_Numero = Accion.Formato(strArray[1].Trim(), 8).ToString();
            //        }
            //        cab.Ctb_Fecha_Movimiento = Convert.ToDateTime( txtfechadoc.Text);
            //        cab.Ctb_Tipo_Ent =  txttipoentcod.Text;
            //        cab.Ctb_Ruc_dni =  txtrucdni.Text;
            //        cab.Ctb_Medio_Pago =  txtmedpago.Text;
            //        cab.Ctb_Numero_Transaccion =  txtnumetransaccion.Text;
            //        cab.Ctb_Cuenta_Corriente =  tctctacorriente.Text;
            //        cab.Ctb_Entidad_Financiera =  txtentfinanciera;
            //        cab.Ctn_Moneda_Cod =  txtmonedacod.Tag.ToString();
            //        cab.Ctb_Tipo_Cambio_Cod =  txttipocambiocod.Text;
            //        cab.Ctb_Es_moneda_nac =  Es_moneda_nac;
            //        cab.Ctb_Tipo_Cambio_desc =  txttipocambiodesc.Text;
            //        cab.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal( txttipocambiovalor.Text);
            //        cab.Ctb_Importe = Convert.ToDecimal( txtimporte.Text);
            //        cab.Flujo_Efectivo_Cod =  txtflujoefectivocod.Text;
            //        cab.Tes_Cuenta =  Cuenta_Contable_Cab;
            //        if ( ChkDifCambio.Checked)
            //        {
            //            cab.Tes_DifCam = true;
            //            cab.Tes_DifCambio = Convert.ToDecimal( IIf( TxtDifCambio.Text == "", 0,  TxtDifCambio.Text));
            //        }
            //        else
            //        {
            //            cab.Tes_DifCam = false;
            //            cab.Tes_DifCambio = Convert.ToDecimal( IIf( TxtDifCambio.Text == "", 0,  TxtDifCambio.Text));
            //        }
            //        if ( ChkDifRedondeo.Checked)
            //        {
            //            cab.Tes_DifRed = true;
            //            cab.Tes_DifRedond = Convert.ToDecimal( IIf( TxtDifRedondeo.Text == "", 0,  TxtDifRedondeo.Text));
            //        }
            //        else
            //        {
            //            cab.Tes_DifRed = false;
            //            cab.Tes_DifRedond = Convert.ToDecimal( IIf( TxtDifRedondeo.Text == "", 0,  TxtDifRedondeo.Text));
            //        }
            //        cab.DetalleAsiento =  Detalles;
            //        if (base.Estado != Estados.Nuevo)
            //        {
            //            if ((base.Estado == Estados.Modificar) && cab2.Modificar_Tesoreria(cab))
            //            {
            //                Accion.ExitoModificar();
            //                base.Close();
            //            }
            //        }
            //        else if (cab2.Insertar_Tesoreria(cab))
            //        {
            //            Accion.ExitoGuardar();
            //            base.Estado = Estados.Nuevo;
            //             LimpiarCab();
            //             LimpiarDet();
            //             Detalles.Clear();
            //             dgvdatos.DataSource = null;
            //             BloquearDetalles();
            //             TraerLibro();
            //             txttipomovimientocod.Focus();
            //        }
            //    }
            //}
            //catch (Exception exception1)
            //{
            //    Accion.ErrorSistema(exception1.Message);
            //}
            try
            {
                if ( VerificarCabecera())
                {
                    
                    Entidad_Movimiento_Cab cab = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab cab2 = new Logica_Movimiento_Cab();
                    cab.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    cab.Id_Anio = Actual_Conexion.AnioSelect;
                    cab.Id_Periodo =  Id_Periodo;
                    cab.Id_Libro =  txtlibro.Tag.ToString();
                    cab.Id_Voucher =  Voucher;
                    cab.Ctb_Tipo_Movimiento =  txttipomovimientocod.Tag.ToString();
                    cab.Ctb_Glosa =  txtglosa.Text;
                    cab.Ctb_Tipo_Doc =  txttipodoc_;

                    string text =  txtserie.Text;

                    if ( txtserie.Text != "")
                    {
                        char[] separator = new char[] { Convert.ToChar("-") };
                        string[] strArray = text.Split(separator);
                        cab.Ctb_Serie = Accion.Formato(strArray[0].Trim(), 4).ToString();
                        cab.Ctb_Numero = Accion.Formato(strArray[1].Trim(), 8).ToString();
                    }

                    cab.Ctb_Fecha_Movimiento = Convert.ToDateTime( txtfechadoc.Text);
                    cab.Ctb_Tipo_Ent =  txttipoentcod.Text;
                    cab.Ctb_Ruc_dni =  txtrucdni.Text;
                    cab.Ctb_Medio_Pago =  txtmedpago.Text;
                    cab.Ctb_Numero_Transaccion =  txtnumetransaccion.Text;
                    cab.Ctb_Cuenta_Corriente =  tctctacorriente.Text;
                    cab.Ctb_Entidad_Financiera =  txtentfinanciera;
                    cab.Ctn_Moneda_Cod =  txtmonedacod.Tag.ToString();
                    cab.Ctb_Tipo_Cambio_Cod =  txttipocambiocod.Text;
                    cab.Ctb_Es_moneda_nac =  Es_moneda_nac;
                    cab.Ctb_Tipo_Cambio_desc =  txttipocambiodesc.Text;
                    cab.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal( txttipocambiovalor.Text);
                    cab.Ctb_Importe = Convert.ToDecimal( txtimporte.Text);
                    cab.Flujo_Efectivo_Cod =  txtflujoefectivocod.Text;
                    cab.Tes_Cuenta =  Cuenta_Contable_Cab;

                    if ( ChkDifCambio.Checked)
                    {
                        cab.Tes_DifCam = true;
                        cab.Tes_DifCambio = Convert.ToDecimal( IIf( TxtDifCambio.Text == "", 0,  TxtDifCambio.Text));
                    }
                    else
                    {
                        cab.Tes_DifCam = false;
                        cab.Tes_DifCambio = Convert.ToDecimal( IIf( TxtDifCambio.Text == "", 0,  TxtDifCambio.Text));
                    }

                    if ( ChkDifRedondeo.Checked)
                    {
                        cab.Tes_DifRed = true;
                        cab.Tes_DifRedond = Convert.ToDecimal( IIf( TxtDifRedondeo.Text == "", 0,  TxtDifRedondeo.Text));
                    }
                    else
                    {
                        cab.Tes_DifRed = false;
                        cab.Tes_DifRedond = Convert.ToDecimal( IIf( TxtDifRedondeo.Text == "", 0,  TxtDifRedondeo.Text));
                    }

                    cab.DetalleAsiento =  Detalles;


                    if (Estado == Estados.Nuevo)
                    {
                        if (cab2.Insertar_Tesoreria(cab))
                        {
                            Accion.ExitoGuardar();
                            base.Estado = Estados.Nuevo;
                             LimpiarCab();
                             LimpiarDet();
                             Detalles.Clear();
                             dgvdatos.DataSource = null;
                             BloquearDetalles();
                             TraerLibro();
                             txttipomovimientocod.Focus();
                        }
                    }
                    else if ((Estado == Estados.Modificar) && cab2.Modificar_Tesoreria(cab))
                    {
                        Accion.ExitoModificar();
                        base.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }

        }


        public bool VerificarCabecera()
        {
            bool flag2;
            if (string.IsNullOrEmpty( txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                 txtglosa.Focus();
                flag2 = false;
            }
            else if (string.IsNullOrEmpty( txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                 txtfechadoc.Focus();
                flag2 = false;
            }
            else if (string.IsNullOrEmpty( txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                 txtmonedacod.Focus();
                flag2 = false;
            }
            else if ( Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                flag2 = false;
            }
            else if (!(Convert.ToDecimal( TxtDiferencias.Text) != 0))
            {
                flag2 = true;
            }
            else
            {
                Accion.Advertencia("Verificaci\x00f3n de Datos,No debe existir diferencias entre los documentos");
                 txtimporte.Focus();
                flag2 = false;
            }
            return flag2;
        }



        private void LimpiarCab()
        {
             txttipomovimientocod.ResetText();
             txttipomovimientodesc.ResetText();
             txtglosa.ResetText();
             txttipodoc.ResetText();
             txttipodocdesc.ResetText();
             txtserie.ResetText();
             txtfechadoc.ResetText();
             txttipoentcod.ResetText();
             txttipoentdesc.ResetText();
             txtrucdni.ResetText();
             txtentidad.ResetText();
             txtmedpago.ResetText();
             txtmedpagodesc.ResetText();
             txtnumetransaccion.ResetText();
             tctctacorriente.ResetText();
             txtentfinanciera = "";
             txtentfinancieradesc = "";
             txtmonedacod.ResetText();
             txtmonedadesc.ResetText();
             txttipocambiocod.ResetText();
             txttipocambiodesc.ResetText();
             txttipocambiovalor.ResetText();
             txtimporte.ResetText();
             txtimporteMN.ResetText();
             txtflujoefectivocod.ResetText();
             txtflujoefectivodesc.ResetText();
             Cuenta_Contable_Cab = "";
             ImporteNoMay = 0;
             Anio_Ref = "";
             Periodo_Ref = "";
             Voucher_Ref = "";
             Libro_Ref = "";
             Item_Ref = 0;
             es_cuenta_principal = false;
             ChkDifCambio.Checked = false;
             ChkDifRedondeo.Checked = false;
             TxtDifCambio.Text = "0.000";
             TxtDifRedondeo.Text = "0.000";
             TxtDiferencias.Text = "0.000";
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if ( Detalles.Count > 0)
                {
                    Entidad_Movimiento_Cab cab = new Entidad_Movimiento_Cab();
                    base.EstadoDetalle = Estados.Ninguno;
                    cab =  Detalles[ gridView1.GetFocusedDataSourceRowIndex()];
                     Id_Item = cab.Id_Item;
                     txtcuenta.Text = cab.Ctb_Cuenta;
                     txtcuentadesc.Text = cab.Ctb_Cuenta_Desc;
                     txttipodocdet.Text = cab.Ctb_Tipo_Doc_det_Sunat;
                     txttipodocdet.Tag = cab.Ctb_Tipo_Doc_det;
                     txttipodocdescdet.Text = cab.Ctb_Tipo_Doc_det_desc;
                     txtseriedet.Text = cab.Ctb_Serie_det + "-" + cab.Ctb_Numero_det;
                     txtmedpagodet.Text = "";
                     txtmedpagodescdet.Text = "";
                     FechaDoc = cab.Ctb_Fecha_Mov_det;
                     txtmonedacoddet.Text = cab.Ctb_moneda_cod_det_Sunat;
                     txtmonedacoddet.Tag = cab.Ctb_moneda_cod_det;
                     txtmonedadescdet.Text = cab.Ctb_moneda_det_desc;
                     txttipocambiocoddet.Text = cab.Ctb_Tipo_Cambio_Cod_Det;
                     txttipocambiodescdet.Text = cab.Ctb_Tipo_Cambio_Desc_Det;
                     txttipocambiovalordet.Text = Convert.ToDecimal(cab.Ctb_Tipo_Cambio_Valor_Det).ToString();
                     txtrucdnidet.Text = cab.Ctb_Ruc_dni_det;
                     txtentidaddet.Text = cab.Entidad_det;
                     txttipo.Tag = cab.Ctb_Tipo_DH;
                     txttipo.Text = cab.CCtb_Tipo_DH_Interno;
                     txttipodesc.Text = cab.Ctb_Tipo_DH_Desc;
                    if (cab.Ctb_Tipo_DH == "0004")
                    {
                        // txtimporteMEDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Debe).ToString();
                        // txtimporteMNDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Debe_Extr).ToString();
                        txtimporteMNDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Debe).ToString();
                        txtimporteMEDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Debe_Extr).ToString();

                        ImporteNoMay = cab.Ctb_Importe_Debe;
                    }
                    else if (cab.Ctb_Tipo_DH == "0005")
                    {
                        //  txtimporteMEDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Haber).ToString();
                        //  txtimporteMNDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Haber_Extr).ToString();
                        txtimporteMNDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Haber).ToString();
                        txtimporteMEDet.Text = Convert.ToDecimal(cab.Ctb_Importe_Haber_Extr).ToString();

                        ImporteNoMay = cab.Ctb_Importe_Haber;
                    }
                     Es_moneda_nacDet = cab.Ctb_Es_moneda_nac_det;
                     Anio_Ref = cab.Id_Anio_Ref;
                     Periodo_Ref = cab.Id_Periodo_Ref;
                     Libro_Ref = cab.Id_Libro_Ref;
                     Voucher_Ref = cab.Id_Voucher_Ref;
                     Item_Ref = cab.Id_Item_Ref;
                     es_cuenta_principal = cab.Ctb_Es_Cuenta_Principal;
                    if ( Estado_Ven_Boton != "1")
                    {
                        if ( Estado_Ven_Boton == "2")
                        {
                            base.EstadoDetalle = Estados.SoloLectura;
                        }
                    }
                    else
                    {
                        base.EstadoDetalle = Estados.Consulta;
                         BloquearDetalles();
                         btnanadirdet.Enabled = false;
                         btneditardet.Enabled = true;
                         btnquitardet.Enabled = false;
                         btnnuevodet.Enabled = false;
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void txtimporteMNDet_TextChanged(object sender, EventArgs e)
        {
            MostrarImporteMN_Det();
        }

        void MostrarImporteMN_Det()
        {
            try
            {
                txtimporteMEDet.Text = (Convert.ToDecimal(txtimporteMNDet.Text) * Convert.ToDecimal(txttipocambiovalordet.Text)).ToString();
            }
            catch (Exception)
            {
                txtimporteMEDet.Text = "0.00";
            }
        }

    }
}
