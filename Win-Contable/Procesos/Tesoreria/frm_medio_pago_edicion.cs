﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_medio_pago_edicion : Contable.frm_fuente
    {
        public frm_medio_pago_edicion()
        {
            InitializeComponent();
        }

     public   string Estado_Ven_Boton;
        public string Id_Sunat;
        void LimpiarCab()
        {
            txtidsunat.ResetText();
            txtdescripcion.ResetText();
        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();

                txtidsunat.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }
        private void frm_medio_pago_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;

                txtidsunat.Focus();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                txtidsunat.Enabled = false;
                ListarModificar();

            }
        }

        public List<Entidad_Medio_Pago> Lista_Modificar = new List<Entidad_Medio_Pago>();

        public void ListarModificar()
        {
            Entidad_Medio_Pago Ent = new Entidad_Medio_Pago();
            Logica_Medio_Pago log = new Logica_Medio_Pago();

            Ent.Id_SUNAT = Id_Sunat;
            try
            {
                Lista_Modificar = log.Listar(Ent);

                Entidad_Medio_Pago Entidad = new Entidad_Medio_Pago();
                Entidad = Lista_Modificar[0];


                txtidsunat.Text = Entidad.Id_SUNAT;
                txtdescripcion.Text = Entidad.Med_Descripcion;



            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {



                    Entidad_Medio_Pago Ent = new Entidad_Medio_Pago();
                    Logica_Medio_Pago Log = new Logica_Medio_Pago();

                    Ent.Id_SUNAT = txtidsunat.Text.ToString();
                    Ent.Med_Descripcion = txtdescripcion.Text.ToString();

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Estado = Estados.Nuevo;
                                LimpiarCab();
                                txtidsunat.Focus();
                                this.Close();

                            }

                        }
                        else if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                this.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtidsunat.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un codigo de sunat");
                txtidsunat.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtdescripcion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtdescripcion.Focus();
                return false;
            }
                     return true;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
