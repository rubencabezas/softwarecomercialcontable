﻿using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_egresos : Contable.frm_fuente
    {
        public frm_egresos()
        {
            InitializeComponent();
        }

        string Id_Libro;

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_CajaBanco(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_CajaBanco;

                }
                else
                {
                    Accion.Advertencia("Debe configurar libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;
            Ent.Ctb_Tipo_IE = "0050"; //Egreso

            try
            {
                Lista = log.Listar_Tesoreria(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_egresos_edicion f = new frm_egresos_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        private void frm_egresos_Load(object sender, EventArgs e)
        {
            TraerLibro();
            Listar();
        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_egresos_edicion f = new frm_egresos_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Libro = Entidad.Id_Libro;
                f.Voucher = Entidad.Id_Voucher;
                f.Tipo_IE = Entidad.Ctb_Tipo_IE;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnimprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (Lista.Count > 0)
            //{
            //    Estado = Estados.Ninguno;
            //    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

            //    Rpt_Orden_Pago R = new Rpt_Orden_Pago();
            //    R.lblnombrecomprobante.Text = Entidad.Nombre_Comprobante;
            //    R.lblserie.Text = Entidad.Ctb_Serie;
            //    R.lblnumero.Text = Entidad.Ctb_Numero;
            //    R.lblentidad.Text = Entidad.Entidad;
            //    R.lblcuentacorriente.Text = Entidad.Ctb_Cuenta_Corriente;
            //    R.lblfechamov.Text = Entidad.Ctb_Glosa;

            //    R.lblfechamov.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Ctb_Fecha_Movimiento);
            //    R.lblentidadfinanciera.Text = Entidad.Ctb_Entidad_Financiera_Desc;



            //    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();

            //    List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();

            //    Detalles = log_det.Listar_Tesoreria_Det(Entidad);


            //    R.DataSource = Detalles;

            //    dynamic ribbonPreview = new PrintPreviewFormEx();
            //    R.CreateDocument();

            //    R.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            //    ribbonPreview.PrintingSystem = R.PrintingSystem;
            //    //ribbonPreview.MdiParent = frm_principal;
            //    ribbonPreview.Show();

            //}
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
