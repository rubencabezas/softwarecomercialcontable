﻿namespace Contable.Procesos.Tesoreria
{
    partial class frm_caja_banco_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtDiferencias = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.TxtDifRedondeo = new DevExpress.XtraEditors.TextEdit();
            this.TxtDifCambio = new DevExpress.XtraEditors.TextEdit();
            this.ChkDifRedondeo = new System.Windows.Forms.CheckBox();
            this.ChkDifCambio = new System.Windows.Forms.CheckBox();
            this.txtimporteMN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txtnumetransaccion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtmedpagodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmedpago = new DevExpress.XtraEditors.TextEdit();
            this.btnAgregarCtaPrincipal = new DevExpress.XtraEditors.SimpleButton();
            this.btnbuscarprovisioncab = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txttipomovimientodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipomovimientocod = new DevExpress.XtraEditors.TextEdit();
            this.txtflujoefectivodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtflujoefectivocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.tctctacorriente = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtimporte = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoentdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipoentcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtimporteMNDet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.btnbuscarprovision = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.txtentidaddet = new DevExpress.XtraEditors.TextEdit();
            this.txtrucdnidet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtmedpagodescdet = new DevExpress.XtraEditors.TextEdit();
            this.txtmedpagodet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtTotalImporteMNDet = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteMEDet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalordet = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodescdet = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocoddet = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtmonedadescdet = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacoddet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtseriedet = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdescdet = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiferencias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifRedondeo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifCambio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumetransaccion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpagodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpago.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tctctacorriente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMNDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidaddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdnidet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpagodescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpagodet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalImporteMNDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMEDet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalordet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocoddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacoddet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtseriedet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdescdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.txtimporteMN);
            this.groupControl1.Controls.Add(this.labelControl27);
            this.groupControl1.Controls.Add(this.txtnumetransaccion);
            this.groupControl1.Controls.Add(this.labelControl25);
            this.groupControl1.Controls.Add(this.txtmedpagodesc);
            this.groupControl1.Controls.Add(this.txtmedpago);
            this.groupControl1.Controls.Add(this.btnAgregarCtaPrincipal);
            this.groupControl1.Controls.Add(this.btnbuscarprovisioncab);
            this.groupControl1.Controls.Add(this.labelControl24);
            this.groupControl1.Controls.Add(this.txttipomovimientodesc);
            this.groupControl1.Controls.Add(this.txttipomovimientocod);
            this.groupControl1.Controls.Add(this.txtflujoefectivodesc);
            this.groupControl1.Controls.Add(this.txtflujoefectivocod);
            this.groupControl1.Controls.Add(this.labelControl23);
            this.groupControl1.Controls.Add(this.labelControl22);
            this.groupControl1.Controls.Add(this.tctctacorriente);
            this.groupControl1.Controls.Add(this.labelControl20);
            this.groupControl1.Controls.Add(this.txtimporte);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txttipocambiovalor);
            this.groupControl1.Controls.Add(this.txttipocambiodesc);
            this.groupControl1.Controls.Add(this.txttipocambiocod);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtentidad);
            this.groupControl1.Controls.Add(this.txtrucdni);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txttipoentdesc);
            this.groupControl1.Controls.Add(this.txttipoentcod);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(5, 37);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1089, 217);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtDiferencias);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Label25);
            this.groupBox1.Controls.Add(this.TxtDifRedondeo);
            this.groupBox1.Controls.Add(this.TxtDifCambio);
            this.groupBox1.Controls.Add(this.ChkDifRedondeo);
            this.groupBox1.Controls.Add(this.ChkDifCambio);
            this.groupBox1.Location = new System.Drawing.Point(901, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 91);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Diferencias";
            // 
            // TxtDiferencias
            // 
            this.TxtDiferencias.Enabled = false;
            this.TxtDiferencias.Location = new System.Drawing.Point(78, 67);
            this.TxtDiferencias.Name = "TxtDiferencias";
            this.TxtDiferencias.Properties.Mask.EditMask = "n3";
            this.TxtDiferencias.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtDiferencias.Size = new System.Drawing.Size(100, 20);
            this.TxtDiferencias.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Diferencias";
            // 
            // Label25
            // 
            this.Label25.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Label25.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.Label25.Location = new System.Drawing.Point(17, 62);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(160, 1);
            this.Label25.TabIndex = 5;
            this.Label25.Text = "Label25";
            // 
            // TxtDifRedondeo
            // 
            this.TxtDifRedondeo.Location = new System.Drawing.Point(87, 39);
            this.TxtDifRedondeo.Name = "TxtDifRedondeo";
            this.TxtDifRedondeo.Properties.Mask.EditMask = "n3";
            this.TxtDifRedondeo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtDifRedondeo.Size = new System.Drawing.Size(91, 20);
            this.TxtDifRedondeo.TabIndex = 2;
            this.TxtDifRedondeo.TextChanged += new System.EventHandler(this.TxtDifRedondeo_TextChanged);
            // 
            // TxtDifCambio
            // 
            this.TxtDifCambio.Location = new System.Drawing.Point(87, 18);
            this.TxtDifCambio.Name = "TxtDifCambio";
            this.TxtDifCambio.Properties.Mask.EditMask = "n3";
            this.TxtDifCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtDifCambio.Size = new System.Drawing.Size(91, 20);
            this.TxtDifCambio.TabIndex = 2;
            this.TxtDifCambio.TextChanged += new System.EventHandler(this.TxtDifCambio_TextChanged);
            // 
            // ChkDifRedondeo
            // 
            this.ChkDifRedondeo.AutoSize = true;
            this.ChkDifRedondeo.Location = new System.Drawing.Point(6, 42);
            this.ChkDifRedondeo.Name = "ChkDifRedondeo";
            this.ChkDifRedondeo.Size = new System.Drawing.Size(87, 17);
            this.ChkDifRedondeo.TabIndex = 1;
            this.ChkDifRedondeo.Text = "de redondeo";
            this.ChkDifRedondeo.UseVisualStyleBackColor = true;
            this.ChkDifRedondeo.CheckedChanged += new System.EventHandler(this.ChkDifRedondeo_CheckedChanged);
            // 
            // ChkDifCambio
            // 
            this.ChkDifCambio.AutoSize = true;
            this.ChkDifCambio.Location = new System.Drawing.Point(6, 20);
            this.ChkDifCambio.Name = "ChkDifCambio";
            this.ChkDifCambio.Size = new System.Drawing.Size(74, 17);
            this.ChkDifCambio.TabIndex = 0;
            this.ChkDifCambio.Text = "de cambio";
            this.ChkDifCambio.UseVisualStyleBackColor = true;
            this.ChkDifCambio.CheckedChanged += new System.EventHandler(this.ChkDifCambio_CheckedChanged);
            // 
            // txtimporteMN
            // 
            this.txtimporteMN.Enabled = false;
            this.txtimporteMN.Location = new System.Drawing.Point(784, 90);
            this.txtimporteMN.Name = "txtimporteMN";
            this.txtimporteMN.Size = new System.Drawing.Size(100, 20);
            this.txtimporteMN.TabIndex = 26;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(714, 93);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(64, 13);
            this.labelControl27.TabIndex = 25;
            this.labelControl27.Text = "Importe M.N.";
            // 
            // txtnumetransaccion
            // 
            this.txtnumetransaccion.EnterMoveNextControl = true;
            this.txtnumetransaccion.Location = new System.Drawing.Point(761, 69);
            this.txtnumetransaccion.Name = "txtnumetransaccion";
            this.txtnumetransaccion.Size = new System.Drawing.Size(124, 20);
            this.txtnumetransaccion.TabIndex = 15;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(685, 72);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(71, 13);
            this.labelControl25.TabIndex = 14;
            this.labelControl25.Text = "N. Transaccion";
            // 
            // txtmedpagodesc
            // 
            this.txtmedpagodesc.Enabled = false;
            this.txtmedpagodesc.EnterMoveNextControl = true;
            this.txtmedpagodesc.Location = new System.Drawing.Point(128, 68);
            this.txtmedpagodesc.Name = "txtmedpagodesc";
            this.txtmedpagodesc.Size = new System.Drawing.Size(264, 20);
            this.txtmedpagodesc.TabIndex = 11;
            // 
            // txtmedpago
            // 
            this.txtmedpago.EnterMoveNextControl = true;
            this.txtmedpago.Location = new System.Drawing.Point(84, 69);
            this.txtmedpago.Name = "txtmedpago";
            this.txtmedpago.Size = new System.Drawing.Size(40, 20);
            this.txtmedpago.TabIndex = 10;
            this.txtmedpago.TextChanged += new System.EventHandler(this.txtmedpago_TextChanged);
            this.txtmedpago.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmedpago_KeyDown);
            // 
            // btnAgregarCtaPrincipal
            // 
            this.btnAgregarCtaPrincipal.Location = new System.Drawing.Point(283, 181);
            this.btnAgregarCtaPrincipal.Name = "btnAgregarCtaPrincipal";
            this.btnAgregarCtaPrincipal.Size = new System.Drawing.Size(146, 25);
            this.btnAgregarCtaPrincipal.TabIndex = 40;
            this.btnAgregarCtaPrincipal.Text = "Agregar Cuenta Principal";
            this.btnAgregarCtaPrincipal.Click += new System.EventHandler(this.btnAgregarCtaPrincipal_Click);
            // 
            // btnbuscarprovisioncab
            // 
            this.btnbuscarprovisioncab.Location = new System.Drawing.Point(171, 181);
            this.btnbuscarprovisioncab.Name = "btnbuscarprovisioncab";
            this.btnbuscarprovisioncab.Size = new System.Drawing.Size(106, 25);
            this.btnbuscarprovisioncab.TabIndex = 55;
            this.btnbuscarprovisioncab.Text = "Doc. Provision";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(6, 70);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(74, 13);
            this.labelControl24.TabIndex = 9;
            this.labelControl24.Text = "Medio de pago:";
            // 
            // txttipomovimientodesc
            // 
            this.txttipomovimientodesc.Enabled = false;
            this.txttipomovimientodesc.EnterMoveNextControl = true;
            this.txttipomovimientodesc.Location = new System.Drawing.Point(436, 26);
            this.txttipomovimientodesc.Name = "txttipomovimientodesc";
            this.txttipomovimientodesc.Size = new System.Drawing.Size(243, 20);
            this.txttipomovimientodesc.TabIndex = 4;
            // 
            // txttipomovimientocod
            // 
            this.txttipomovimientocod.EnterMoveNextControl = true;
            this.txttipomovimientocod.Location = new System.Drawing.Point(387, 26);
            this.txttipomovimientocod.Name = "txttipomovimientocod";
            this.txttipomovimientocod.Size = new System.Drawing.Size(47, 20);
            this.txttipomovimientocod.TabIndex = 3;
            this.txttipomovimientocod.TextChanged += new System.EventHandler(this.txttipomovimientocod_TextChanged);
            this.txttipomovimientocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipomovimientocod_KeyDown);
            // 
            // txtflujoefectivodesc
            // 
            this.txtflujoefectivodesc.Enabled = false;
            this.txtflujoefectivodesc.EnterMoveNextControl = true;
            this.txtflujoefectivodesc.Location = new System.Drawing.Point(219, 155);
            this.txtflujoefectivodesc.Name = "txtflujoefectivodesc";
            this.txtflujoefectivodesc.Size = new System.Drawing.Size(417, 20);
            this.txtflujoefectivodesc.TabIndex = 39;
            // 
            // txtflujoefectivocod
            // 
            this.txtflujoefectivocod.Enabled = false;
            this.txtflujoefectivocod.EnterMoveNextControl = true;
            this.txtflujoefectivocod.Location = new System.Drawing.Point(171, 155);
            this.txtflujoefectivocod.Name = "txtflujoefectivocod";
            this.txtflujoefectivocod.Size = new System.Drawing.Size(46, 20);
            this.txtflujoefectivocod.TabIndex = 38;
            this.txtflujoefectivocod.TextChanged += new System.EventHandler(this.txtflujoefectivocod_TextChanged);
            this.txtflujoefectivocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtflujoefectivocod_KeyDown);
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(327, 29);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(58, 13);
            this.labelControl23.TabIndex = 2;
            this.labelControl23.Text = "Movimiento:";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(76, 158);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(89, 13);
            this.labelControl22.TabIndex = 37;
            this.labelControl22.Text = "Flujo efectivo cod.";
            // 
            // tctctacorriente
            // 
            this.tctctacorriente.EnterMoveNextControl = true;
            this.tctctacorriente.Location = new System.Drawing.Point(476, 69);
            this.tctctacorriente.Name = "tctctacorriente";
            this.tctctacorriente.Size = new System.Drawing.Size(203, 20);
            this.tctctacorriente.TabIndex = 13;
            this.tctctacorriente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tctctacorriente_KeyDown);
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(398, 72);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(71, 13);
            this.labelControl20.TabIndex = 12;
            this.labelControl20.Text = "Cta. corriente:";
            // 
            // txtimporte
            // 
            this.txtimporte.EnterMoveNextControl = true;
            this.txtimporte.Location = new System.Drawing.Point(607, 90);
            this.txtimporte.Name = "txtimporte";
            this.txtimporte.Properties.Mask.EditMask = "n";
            this.txtimporte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporte.Size = new System.Drawing.Size(100, 20);
            this.txtimporte.TabIndex = 24;
            this.txtimporte.TextChanged += new System.EventHandler(this.txtimporte_TextChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(559, 93);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(42, 13);
            this.labelControl10.TabIndex = 23;
            this.labelControl10.Text = "Importe:";
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(470, 90);
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Size = new System.Drawing.Size(62, 20);
            this.txttipocambiovalor.TabIndex = 22;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(363, 90);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 21;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(327, 90);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 20;
            this.txttipocambiocod.TextChanged += new System.EventHandler(this.txttipocambiocod_TextChanged);
            this.txttipocambiocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocod_KeyDown);
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(300, 93);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(21, 13);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "T.C.";
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(127, 90);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(167, 20);
            this.txtmonedadesc.TabIndex = 18;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(84, 90);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Size = new System.Drawing.Size(40, 20);
            this.txtmonedacod.TabIndex = 17;
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(30, 89);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(42, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Moneda:";
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(171, 133);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Size = new System.Drawing.Size(713, 20);
            this.txtentidad.TabIndex = 36;
            // 
            // txtrucdni
            // 
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(86, 133);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 35;
            this.txtrucdni.TextChanged += new System.EventHandler(this.txtrucdni_TextChanged);
            this.txtrucdni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdni_KeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(40, 136);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(40, 13);
            this.labelControl7.TabIndex = 34;
            this.labelControl7.Text = "Entidad:";
            // 
            // txttipoentdesc
            // 
            this.txttipoentdesc.Enabled = false;
            this.txttipoentdesc.EnterMoveNextControl = true;
            this.txttipoentdesc.Location = new System.Drawing.Point(781, 111);
            this.txttipoentdesc.Name = "txttipoentdesc";
            this.txttipoentdesc.Size = new System.Drawing.Size(104, 20);
            this.txttipoentdesc.TabIndex = 33;
            // 
            // txttipoentcod
            // 
            this.txttipoentcod.EnterMoveNextControl = true;
            this.txttipoentcod.Location = new System.Drawing.Point(738, 111);
            this.txttipoentcod.Name = "txttipoentcod";
            this.txttipoentcod.Size = new System.Drawing.Size(40, 20);
            this.txttipoentcod.TabIndex = 32;
            this.txttipoentcod.TextChanged += new System.EventHandler(this.txttipoentcod_TextChanged);
            this.txttipoentcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoentcod_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(665, 114);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(61, 13);
            this.labelControl6.TabIndex = 31;
            this.labelControl6.Text = "Tip. Entidad:";
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(792, 26);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(93, 20);
            this.txtfechadoc.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(703, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(83, 13);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "Fecha operacion:";
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(532, 112);
            this.txtserie.Name = "txtserie";
            this.txtserie.Size = new System.Drawing.Size(127, 20);
            this.txtserie.TabIndex = 30;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(127, 112);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(402, 20);
            this.txttipodocdesc.TabIndex = 29;
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(85, 112);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 28;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(36, 111);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "T. Doc.:";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(84, 48);
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(801, 20);
            this.txtglosa.TabIndex = 8;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(42, 51);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Glosa:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(84, 26);
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Size = new System.Drawing.Size(239, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(45, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1094, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 613);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1094, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 581);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1094, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 581);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.txtimporteMNDet);
            this.groupControl2.Controls.Add(this.labelControl26);
            this.groupControl2.Controls.Add(this.btnbuscarprovision);
            this.groupControl2.Controls.Add(this.dgvdatos);
            this.groupControl2.Controls.Add(this.btnanadirdet);
            this.groupControl2.Controls.Add(this.btneditardet);
            this.groupControl2.Controls.Add(this.btnquitardet);
            this.groupControl2.Controls.Add(this.btnnuevodet);
            this.groupControl2.Controls.Add(this.txtentidaddet);
            this.groupControl2.Controls.Add(this.txtrucdnidet);
            this.groupControl2.Controls.Add(this.labelControl19);
            this.groupControl2.Controls.Add(this.txtmedpagodescdet);
            this.groupControl2.Controls.Add(this.txtmedpagodet);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.txtTotalImporteMNDet);
            this.groupControl2.Controls.Add(this.txtimporteMEDet);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.txttipodesc);
            this.groupControl2.Controls.Add(this.txttipo);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.txttipocambiovalordet);
            this.groupControl2.Controls.Add(this.txttipocambiodescdet);
            this.groupControl2.Controls.Add(this.txttipocambiocoddet);
            this.groupControl2.Controls.Add(this.label3);
            this.groupControl2.Controls.Add(this.txtmonedadescdet);
            this.groupControl2.Controls.Add(this.txtmonedacoddet);
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.txtseriedet);
            this.groupControl2.Controls.Add(this.txttipodocdescdet);
            this.groupControl2.Controls.Add(this.txttipodocdet);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.txtcuentadesc);
            this.groupControl2.Controls.Add(this.txtcuenta);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Location = new System.Drawing.Point(5, 262);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1077, 346);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Detalles";
            // 
            // txtimporteMNDet
            // 
            this.txtimporteMNDet.EnterMoveNextControl = true;
            this.txtimporteMNDet.Location = new System.Drawing.Point(492, 113);
            this.txtimporteMNDet.Name = "txtimporteMNDet";
            this.txtimporteMNDet.Properties.Mask.EditMask = "n";
            this.txtimporteMNDet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporteMNDet.Size = new System.Drawing.Size(100, 20);
            this.txtimporteMNDet.TabIndex = 25;
            this.txtimporteMNDet.TextChanged += new System.EventHandler(this.txtimporteMNDet_TextChanged);
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(452, 116);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(38, 13);
            this.labelControl26.TabIndex = 24;
            this.labelControl26.Text = "Importe";
            // 
            // btnbuscarprovision
            // 
            this.btnbuscarprovision.Location = new System.Drawing.Point(687, 80);
            this.btnbuscarprovision.Name = "btnbuscarprovision";
            this.btnbuscarprovision.Size = new System.Drawing.Size(201, 30);
            this.btnbuscarprovision.TabIndex = 35;
            this.btnbuscarprovision.Text = "Doc. Provision";
            this.btnbuscarprovision.Click += new System.EventHandler(this.btnbuscarprovision_Click);
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(7, 144);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(1065, 202);
            this.dgvdatos.TabIndex = 34;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn13,
            this.gridColumn15,
            this.gridColumn14,
            this.gridColumn16});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 10;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Cuenta";
            this.gridColumn1.FieldName = "Ctb_Cuenta";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 94;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "RUC";
            this.gridColumn10.FieldName = "Ctb_Ruc_dni_det";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            this.gridColumn10.Width = 90;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.Caption = "Entidad";
            this.gridColumn11.FieldName = "Entidad_det";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            this.gridColumn11.Width = 236;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Tipo Doc.";
            this.gridColumn3.FieldName = "Ctb_Tipo_Doc_det_desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 85;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Serie";
            this.gridColumn4.FieldName = "Ctb_Serie_det";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "Numero";
            this.gridColumn5.FieldName = "Ctb_Numero_det";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Fecha";
            this.gridColumn6.FieldName = "Ctb_Fecha_Mov_det";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 93;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Moneda";
            this.gridColumn7.FieldName = "Ctb_moneda_det_desc";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "Importe Debe M.N";
            this.gridColumn13.DisplayFormat.FormatString = "n2";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "Ctb_Importe_Debe";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe", "{0:0.##}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 8;
            this.gridColumn13.Width = 135;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn15.Caption = "Importe Haber M.N.";
            this.gridColumn15.DisplayFormat.FormatString = "n2";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn15.FieldName = "Ctb_Importe_Haber";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber", "{0:0.##}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 9;
            this.gridColumn15.Width = 133;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn14.Caption = "Importe Debe M.E.";
            this.gridColumn14.DisplayFormat.FormatString = "n2";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn14.FieldName = "Ctb_Importe_Debe_Extr";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe_Extr", "{0:0.##}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 10;
            this.gridColumn14.Width = 125;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn16.Caption = "Importe Haber M.E.";
            this.gridColumn16.DisplayFormat.FormatString = "n2";
            this.gridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn16.FieldName = "Ctb_Importe_Haber_Extr";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber_Extr", "{0:0.##}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 11;
            this.gridColumn16.Width = 130;
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.btnanadirdet.Location = new System.Drawing.Point(792, 48);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(96, 23);
            this.btnanadirdet.TabIndex = 29;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.btneditardet.Location = new System.Drawing.Point(687, 48);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(96, 23);
            this.btneditardet.TabIndex = 42;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.btnquitardet.Location = new System.Drawing.Point(792, 24);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(96, 23);
            this.btnquitardet.TabIndex = 40;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.btnnuevodet.Location = new System.Drawing.Point(687, 24);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(96, 23);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // txtentidaddet
            // 
            this.txtentidaddet.Enabled = false;
            this.txtentidaddet.EnterMoveNextControl = true;
            this.txtentidaddet.Location = new System.Drawing.Point(140, 48);
            this.txtentidaddet.Name = "txtentidaddet";
            this.txtentidaddet.Size = new System.Drawing.Size(539, 20);
            this.txtentidaddet.TabIndex = 6;
            // 
            // txtrucdnidet
            // 
            this.txtrucdnidet.EnterMoveNextControl = true;
            this.txtrucdnidet.Location = new System.Drawing.Point(56, 48);
            this.txtrucdnidet.Name = "txtrucdnidet";
            this.txtrucdnidet.Size = new System.Drawing.Size(82, 20);
            this.txtrucdnidet.TabIndex = 5;
            this.txtrucdnidet.TextChanged += new System.EventHandler(this.txtrucdnidet_TextChanged);
            this.txtrucdnidet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdnidet_KeyDown);
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(15, 50);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(40, 13);
            this.labelControl19.TabIndex = 4;
            this.labelControl19.Text = "Entidad:";
            // 
            // txtmedpagodescdet
            // 
            this.txtmedpagodescdet.Enabled = false;
            this.txtmedpagodescdet.EnterMoveNextControl = true;
            this.txtmedpagodescdet.Location = new System.Drawing.Point(96, 92);
            this.txtmedpagodescdet.Name = "txtmedpagodescdet";
            this.txtmedpagodescdet.Size = new System.Drawing.Size(333, 20);
            this.txtmedpagodescdet.TabIndex = 13;
            // 
            // txtmedpagodet
            // 
            this.txtmedpagodet.EnterMoveNextControl = true;
            this.txtmedpagodet.Location = new System.Drawing.Point(55, 92);
            this.txtmedpagodet.Name = "txtmedpagodet";
            this.txtmedpagodet.Size = new System.Drawing.Size(40, 20);
            this.txtmedpagodet.TabIndex = 12;
            this.txtmedpagodet.TextChanged += new System.EventHandler(this.txtmedpagodet_TextChanged);
            this.txtmedpagodet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmedpagodet_KeyDown);
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(17, 95);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(28, 13);
            this.labelControl18.TabIndex = 11;
            this.labelControl18.Text = "Medio";
            // 
            // txtTotalImporteMNDet
            // 
            this.txtTotalImporteMNDet.EnterMoveNextControl = true;
            this.txtTotalImporteMNDet.Location = new System.Drawing.Point(881, 113);
            this.txtTotalImporteMNDet.Name = "txtTotalImporteMNDet";
            this.txtTotalImporteMNDet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalImporteMNDet.Properties.Appearance.Options.UseFont = true;
            this.txtTotalImporteMNDet.Properties.Mask.EditMask = "n";
            this.txtTotalImporteMNDet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalImporteMNDet.Properties.ReadOnly = true;
            this.txtTotalImporteMNDet.Size = new System.Drawing.Size(100, 20);
            this.txtTotalImporteMNDet.TabIndex = 29;
            // 
            // txtimporteMEDet
            // 
            this.txtimporteMEDet.EnterMoveNextControl = true;
            this.txtimporteMEDet.Location = new System.Drawing.Point(687, 114);
            this.txtimporteMEDet.Name = "txtimporteMEDet";
            this.txtimporteMEDet.Properties.Mask.EditMask = "n";
            this.txtimporteMEDet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporteMEDet.Properties.ReadOnly = true;
            this.txtimporteMEDet.Size = new System.Drawing.Size(100, 20);
            this.txtimporteMEDet.TabIndex = 27;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(805, 117);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 13);
            this.labelControl4.TabIndex = 28;
            this.labelControl4.Text = "Total Detalle";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(619, 117);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(53, 13);
            this.labelControl17.TabIndex = 26;
            this.labelControl17.Text = "Total M. N.";
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(338, 114);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Size = new System.Drawing.Size(91, 20);
            this.txttipodesc.TabIndex = 23;
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(293, 114);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(43, 20);
            this.txttipo.TabIndex = 22;
            this.txttipo.TextChanged += new System.EventHandler(this.txttipo_TextChanged);
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipo_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(267, 117);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(24, 13);
            this.labelControl16.TabIndex = 21;
            this.labelControl16.Text = "Tipo:";
            // 
            // txttipocambiovalordet
            // 
            this.txttipocambiovalordet.EnterMoveNextControl = true;
            this.txttipocambiovalordet.Location = new System.Drawing.Point(198, 114);
            this.txttipocambiovalordet.Name = "txttipocambiovalordet";
            this.txttipocambiovalordet.Size = new System.Drawing.Size(62, 20);
            this.txttipocambiovalordet.TabIndex = 20;
            // 
            // txttipocambiodescdet
            // 
            this.txttipocambiodescdet.Enabled = false;
            this.txttipocambiodescdet.EnterMoveNextControl = true;
            this.txttipocambiodescdet.Location = new System.Drawing.Point(96, 114);
            this.txttipocambiodescdet.Name = "txttipocambiodescdet";
            this.txttipocambiodescdet.Size = new System.Drawing.Size(100, 20);
            this.txttipocambiodescdet.TabIndex = 19;
            // 
            // txttipocambiocoddet
            // 
            this.txttipocambiocoddet.EnterMoveNextControl = true;
            this.txttipocambiocoddet.Location = new System.Drawing.Point(55, 114);
            this.txttipocambiocoddet.Name = "txttipocambiocoddet";
            this.txttipocambiocoddet.Size = new System.Drawing.Size(40, 20);
            this.txttipocambiocoddet.TabIndex = 18;
            this.txttipocambiocoddet.TextChanged += new System.EventHandler(this.txttipocambiocoddet_TextChanged);
            this.txttipocambiocoddet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocoddet_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "T.C.";
            // 
            // txtmonedadescdet
            // 
            this.txtmonedadescdet.Enabled = false;
            this.txtmonedadescdet.EnterMoveNextControl = true;
            this.txtmonedadescdet.Location = new System.Drawing.Point(532, 91);
            this.txtmonedadescdet.Name = "txtmonedadescdet";
            this.txtmonedadescdet.Size = new System.Drawing.Size(147, 20);
            this.txtmonedadescdet.TabIndex = 16;
            // 
            // txtmonedacoddet
            // 
            this.txtmonedacoddet.EnterMoveNextControl = true;
            this.txtmonedacoddet.Location = new System.Drawing.Point(492, 91);
            this.txtmonedacoddet.Name = "txtmonedacoddet";
            this.txtmonedacoddet.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacoddet.TabIndex = 15;
            this.txtmonedacoddet.TextChanged += new System.EventHandler(this.txtmonedacoddet_TextChanged);
            this.txtmonedacoddet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacoddet_KeyDown);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(448, 93);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(42, 13);
            this.labelControl15.TabIndex = 14;
            this.labelControl15.Text = "Moneda:";
            // 
            // txtseriedet
            // 
            this.txtseriedet.EnterMoveNextControl = true;
            this.txtseriedet.Location = new System.Drawing.Point(492, 70);
            this.txtseriedet.Name = "txtseriedet";
            this.txtseriedet.Size = new System.Drawing.Size(187, 20);
            this.txtseriedet.TabIndex = 10;
            this.txtseriedet.Leave += new System.EventHandler(this.txtseriedet_Leave);
            // 
            // txttipodocdescdet
            // 
            this.txttipodocdescdet.Enabled = false;
            this.txttipodocdescdet.EnterMoveNextControl = true;
            this.txttipodocdescdet.Location = new System.Drawing.Point(96, 70);
            this.txttipodocdescdet.Name = "txttipodocdescdet";
            this.txttipodocdescdet.Size = new System.Drawing.Size(394, 20);
            this.txttipodocdescdet.TabIndex = 9;
            // 
            // txttipodocdet
            // 
            this.txttipodocdet.EnterMoveNextControl = true;
            this.txttipodocdet.Location = new System.Drawing.Point(56, 70);
            this.txttipodocdet.Name = "txttipodocdet";
            this.txttipodocdet.Size = new System.Drawing.Size(39, 20);
            this.txttipodocdet.TabIndex = 8;
            this.txttipodocdet.TextChanged += new System.EventHandler(this.txttipodocdet_TextChanged);
            this.txttipodocdet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodocdet_KeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(19, 73);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(36, 13);
            this.labelControl11.TabIndex = 7;
            this.labelControl11.Text = "T.Doc.:";
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(140, 26);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Size = new System.Drawing.Size(539, 20);
            this.txtcuentadesc.TabIndex = 3;
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(56, 26);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Size = new System.Drawing.Size(82, 20);
            this.txtcuenta.TabIndex = 2;
            this.txtcuenta.TextChanged += new System.EventHandler(this.txtcuenta_TextChanged);
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cuenta:";
            // 
            // frm_caja_banco_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1094, 613);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_caja_banco_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tesoreria";
            this.Load += new System.EventHandler(this.frm_caja_banco_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiferencias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifRedondeo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDifCambio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumetransaccion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpagodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpago.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtflujoefectivocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tctctacorriente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoentcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMNDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidaddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdnidet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpagodescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmedpagodet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalImporteMNDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteMEDet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalordet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocoddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacoddet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtseriedet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdescdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit TxtDiferencias;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label Label25;
        private DevExpress.XtraEditors.TextEdit TxtDifRedondeo;
        private DevExpress.XtraEditors.TextEdit TxtDifCambio;
        private System.Windows.Forms.CheckBox ChkDifRedondeo;
        private System.Windows.Forms.CheckBox ChkDifCambio;
        private DevExpress.XtraEditors.TextEdit txtimporteMN;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit txtnumetransaccion;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtmedpagodesc;
        private DevExpress.XtraEditors.TextEdit txtmedpago;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txttipomovimientodesc;
        private DevExpress.XtraEditors.TextEdit txttipomovimientocod;
        private DevExpress.XtraEditors.TextEdit txtflujoefectivodesc;
        private DevExpress.XtraEditors.TextEdit txtflujoefectivocod;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit tctctacorriente;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtimporte;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtentidad;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txttipoentdesc;
        private DevExpress.XtraEditors.TextEdit txttipoentcod;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtimporteMNDet;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.SimpleButton btnbuscarprovision;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.SimpleButton btnanadirdet;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.SimpleButton btnquitardet;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraEditors.TextEdit txtentidaddet;
        private DevExpress.XtraEditors.TextEdit txtrucdnidet;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtimporteMEDet;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.TextEdit txttipo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalordet;
        private DevExpress.XtraEditors.TextEdit txttipocambiodescdet;
        private DevExpress.XtraEditors.TextEdit txttipocambiocoddet;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtmonedadescdet;
        private DevExpress.XtraEditors.TextEdit txtmonedacoddet;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtseriedet;
        private DevExpress.XtraEditors.TextEdit txttipodocdescdet;
        private DevExpress.XtraEditors.TextEdit txttipodocdet;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.TextEdit txtcuenta;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton btnAgregarCtaPrincipal;
        private DevExpress.XtraEditors.SimpleButton btnbuscarprovisioncab;
        private DevExpress.XtraEditors.TextEdit txtmedpagodescdet;
        private DevExpress.XtraEditors.TextEdit txtmedpagodet;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtTotalImporteMNDet;
        private DevExpress.XtraEditors.LabelControl labelControl4;
    }
}
