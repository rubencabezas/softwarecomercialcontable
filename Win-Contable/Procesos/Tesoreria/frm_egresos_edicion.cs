﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_egresos_edicion : Contable.frm_fuente
    {
        public frm_egresos_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher, Tipo_IE;
        void LimpiarCab()
        {
            txttipomovimientocod.ResetText();
            txttipomovimientodesc.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();

            txtfechadoc.ResetText();
            txttipoentcod.ResetText();
            txttipoentdesc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtmedpago.ResetText();
            txtmedpagodesc.ResetText();
            txtnumetransaccion.ResetText();
            tctctacorriente.ResetText();
            txtentfinanciera.ResetText();
            txtentfinancieradesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();
            txttipocambiovalor.ResetText();
            txtimporte.ResetText();
            txtimporteMN.ResetText();

        }

        void LimpiarDet()
        {
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();

            txttipodocdet.ResetText();
            txttipodocdescdet.ResetText();

            txtseriedet.ResetText();
      

            txtfechadocdet.ResetText();


            txtmonedacoddet.ResetText();
            txtmonedadescdet.ResetText();

            txttipocambiocoddet.ResetText();
            txttipocambiodescdet.ResetText();
            txttipocambiovalordet.ResetText();

            txttipoentcoddet.ResetText();
            txttipoentdescdet.ResetText();
            txtrucdnidet.ResetText();
            txtentidaddet.ResetText();


           txttipo.ResetText();
            txttipodesc.ResetText();
            txtimporteMNDet.ResetText();
            txtimporteMEDet.ResetText();
        }

        void BloquearDetalles()
        {
            txtcuenta.Enabled = false;

            txttipodocdet.Enabled = false;

            txtseriedet.Enabled = false;
      
            txtfechadocdet.Enabled = false;

            txtmonedacoddet.Enabled = false;

            txttipocambiocoddet.Enabled = false;
            txttipocambiovalordet.Enabled = false;

            txttipoentcoddet.Enabled = false;
            txtrucdnidet.Enabled = false;

            txttipo.Enabled = false;
            txtimporteMNDet.Enabled = false;
            txtimporteMEDet.Enabled = false;

        }

        void HabilitarDetalles()
        {
            txtcuenta.Enabled = true;

            txttipodocdet.Enabled = true;

            txtseriedet.Enabled = true;
            txtfechadocdet.Enabled = true;

            txtmonedacoddet.Enabled = true;

            txttipocambiocoddet.Enabled = true;
            txttipocambiovalordet.Enabled = true;

            txttipoentcoddet.Enabled = true;
            txtrucdnidet.Enabled = true;

            txttipo.Enabled = true;
            txtimporteMNDet.Enabled = true;
            txtimporteMEDet.Enabled = true;

        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                LimpiarDet();
                Detalles.Clear();
           
                //dgvdatos.DataSource = null;
                BloquearDetalles();
                TraerLibro();

                txtglosa.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
            

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                Id_Item = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }

        string Numero;
        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_series_tesoreria_busqueda f = new _1_Busquedas_Generales.frm_series_tesoreria_busqueda())
                        {
                            f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            //f.Cta_Corriente = tctctacorriente.Text;
                            //f.Tipo_Doc = txttipodoc.Text;
                            //f.Serie = txtserie.Text;
                            f.Tipo_IE = txttipoEgreso.Tag.ToString();
                            //f.Tipo_mov = txttipomovimientocod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Series_Tesorerira Entidad = new Entidad_Series_Tesorerira();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Ser_Tipo_Doc_Sunat;
                                txttipodoc.Tag = Entidad.Ser_Tipo_Doc;
                                txttipodocdesc.Text = Entidad.Ser_Tipo_Doc_Des;
                                txtserie.Text = Entidad.Ser_Serie;
                                Numero = IIf(Entidad.Ser_Numero_Actual.Trim() == "", 0, Entidad.Ser_Numero_Actual.Trim()).ToString() ;

                                AumentarNumero();

                                txttipodoc.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Series_Tesoreria log = new Logica_Series_Tesoreria();

                List<Entidad_Series_Tesorerira> Generales = new List<Entidad_Series_Tesorerira>();
                Generales = log.Listar(new Entidad_Series_Tesorerira
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Ser_Tip_IE = txttipoEgreso.Tag.ToString(),
                   //Ser_Tipo_Mov = txttipomovimientocod.Tag.ToString(),
                   Ser_Cta_Corriente = tctctacorriente.Text,
                    Ser_Tipo_Doc = txttipodoc.Text,
                    Ser_Serie = txtserie.Text
            });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Series_Tesorerira T in Generales)
                    {
                        if ((T.Ser_Tipo_Doc_Sunat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Ser_Tipo_Doc_Sunat).ToString().Trim();
                            txttipodoc.Tag = (T.Ser_Tipo_Doc).ToString().Trim();
                            txttipodocdesc.Text = T.Ser_Tipo_Doc_Des;

                            txtserie.Text = T.Ser_Serie;// IIf(txtimporteMEDet.Text == "", "0.00", txtimporteMEDet.Text).ToString();
                            Numero = IIf(T.Ser_Numero_Actual.Trim()=="",0,T.Ser_Numero_Actual.Trim()).ToString();
                            AumentarNumero();

                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void AumentarNumero()
        {

            int suma;
            suma = Convert.ToInt32(Numero) + 1;

            string valor;
            valor = Convert.ToString(suma);

            //txtnumero.Text = Accion.Formato(valor, 8);

            string Serie_Numero = txtserie.Text.Trim();

            if (Serie_Numero.Contains("-") == true)
            {
                string SerNum = txtserie.Text;
                string[] Datos = SerNum.Split(Convert.ToChar("-"));
                txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                txtserie.EnterMoveNextControl = true;
            }
            else
            {
                Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                txtserie.Focus();
            }
        }

        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using ( frm_entidades_busqueda f = new  frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdni.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdni.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdni.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdni.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdni.EnterMoveNextControl = false;
                                txtrucdni.ResetText();
                                txtrucdni.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Tag = Entidad.Id_Moneda;

                                txtmonedacod.Text = Entidad.Id_Sunat;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                                VerificarMoneda();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        Boolean Es_moneda_nac;
        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1.000";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocod.Text))
                    {
                        txttipocambiocod.Enabled = true;
                        txttipocambiocod.ResetText();
                        txttipocambiodesc.ResetText();
                        txttipocambiovalor.ResetText();
                    }

                }
            }
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocod.Text) == false)
            {
                if (IsDate(txtfechadoc.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocod.Text.Substring(txttipocambiocod.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadoc.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocod.Text = Entidad.Codigo;
                                    txttipocambiodesc.Text = Entidad.Nombre;
                                    txttipocambiovalor.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocod.Text == "OTR")
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalor.Enabled = false;
                                    }

                                    txttipocambiocod.EnterMoveNextControl = true;

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocod.Text) & string.IsNullOrEmpty(txttipocambiodesc.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadoc.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocod.Text = T.Codigo;
                                            txttipocambiodesc.Text = T.Nombre;
                                            txttipocambiovalor.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocod.Text == "OTR")
                                            {
                                                txttipocambiovalor.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalor.Enabled = false;
                                            }
                                            txttipocambiocod.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadoc.Focus();

                }

            }
        }

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_CajaBanco(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    txtlibro.Tag = Generales[0].Ini_CajaBanco;
                    txtlibro.Text = Generales[0].Ini_CajaBanco_Desc;
                }
                else
                {
                    Accion.Advertencia("Debe configurar un libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_egresos_edicion_Load(object sender, EventArgs e)
        {
            TraerLibro();
            BuscarTipoEgre();
            BloquearDetalles();

            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();

            }

        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            Ent.Ctb_Tipo_IE = Tipo_IE;

            try
            {
                Lista_Modificar = log.Listar_Tesoreria(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = txtlibro.Tag.ToString();
                    Voucher = Enti.Id_Voucher;

                    txttipoEgreso.Tag = Enti.Ctb_Tipo_IE;
                    txttipoEgreso.Text = Enti.Ctb_Tipo_IE_Interno;
                    txttipoEgresodesc.Text = Enti.Ctb_Tipo_IE_Desc;

                    txttipomovimientocod.Tag = Enti.Ctb_Tipo_Movimiento;
                    txttipomovimientocod.Text = Enti.Ctb_Tipo_Movimiento_Interno;
                    txttipomovimientodesc.Text = Enti.Ctb_Tipo_Movimiento_Desc;


                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;

                    txtserie.Text = Enti.Ctb_Serie + "-" + Enti.Ctb_Numero;


                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));

                    txttipoentcod.Text = Enti.Ctb_Tipo_Ent;
                    txttipoentdesc.Text = Enti.Ctb_Tipo_Ent_Desc;

                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;

                    txtmedpago.Text = Enti.Ctb_Medio_Pago;
                    txtmedpagodesc.Text = Enti.Ctb_Medio_Pago_desc;

                    tctctacorriente.Text = Enti.Ctb_Cuenta_Corriente;
                    txtentfinanciera.Text = Enti.Ctb_Entidad_Financiera;
                    txtentfinancieradesc.Text = Enti.Ctb_Entidad_Financiera_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;

                    txtimporte.Text =Convert.ToDecimal(Enti.Ctb_Importe).ToString();

                    //listar admnistrativo

                    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();
                    //dgvdatos.DataSource = null;
                    Detalles = log_det.Listar_Tesoreria_Det(Enti);

                    if (Detalles.Count > 0)
                    {
                        //dgvdatos.DataSource = Detalles;
                    }






                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }



        private void txttipoEgreso_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipoEgreso.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoEgreso.Text.Substring(txttipoEgreso.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0015";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoEgreso.Tag = Entidad.Id_General_Det;
                                txttipoEgreso.Text = Entidad.Gen_Codigo_Interno;
                                txttipoEgresodesc.Text = Entidad.Gen_Descripcion_Det;
                 
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoEgreso.Text) & string.IsNullOrEmpty(txttipoEgresodesc.Text))
                    {
                        BuscarTipoEgre();
                      
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarTipoEgre()
        {
            try
            {
                txttipoEgreso.Text = Accion.Formato(txttipoEgreso.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0015",
                    Gen_Codigo_Interno = "02"//txttipoEgreso.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == "02")
                        {
                            txttipoEgreso.Tag = T.Id_General_Det;
                            txttipoEgreso.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipoEgresodesc.Text = T.Gen_Descripcion_Det;
                            txttipoEgreso.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipoEgreso.EnterMoveNextControl = false;
                    txttipoEgreso.ResetText();
                    txttipoEgreso.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipomovimientocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipomovimientocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipomovimientocod.Text.Substring(txttipomovimientocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0016";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipomovimientocod.Tag = Entidad.Id_General_Det;
                                txttipomovimientocod.Text = Entidad.Gen_Codigo_Interno;
                                txttipomovimientodesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipomovimientocod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipomovimientocod.Text) & string.IsNullOrEmpty(txttipomovimientodesc.Text))
                    {
                        BuscarMovimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMovimiento()
        {
            try
            {
                txttipomovimientocod.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0016",
                    Gen_Codigo_Interno = txttipomovimientocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipomovimientocod.Text.Trim().ToUpper())
                        {
                            txttipomovimientocod.Tag = T.Id_General_Det;
                            txttipomovimientocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipomovimientodesc.Text = T.Gen_Descripcion_Det;
                            txttipomovimientocod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipomovimientocod.EnterMoveNextControl = false;
                    txttipomovimientocod.ResetText();
                    txttipomovimientocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuenta.EnterMoveNextControl = false;
                    txtcuenta.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodocdet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodocdet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodocdet.Text.Substring(txttipodocdet.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodocdet.Text = Entidad.Id_SUnat;
                                txttipodocdet.Tag = Entidad.Id_Comprobante;
                                txttipodocdescdet.Text = Entidad.Nombre_Comprobante;
                                txttipodocdet.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodocdet.Text) & string.IsNullOrEmpty(txttipodocdescdet.Text))
                    {
                        BuscarTipoDocumentoDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumentoDet()
        {
            try
            {
                txttipodocdet.Text = Accion.Formato(txttipodocdet.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodocdet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodocdet.Text.Trim().ToUpper())
                        {
                            txttipodocdet.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodocdet.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdescdet.Text = T.Nombre_Comprobante;
                            txttipodocdet.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodocdet.EnterMoveNextControl = false;
                    txttipodocdet.ResetText();
                    txttipodocdet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacoddet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacoddet.Text.Substring(txtmonedacoddet.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nacDet = Entidad.Es_nacional;
                                txtmonedacoddet.Tag = Entidad.Id_Sunat;
                                txtmonedacoddet.Text = Entidad.Id_Moneda;
                                txtmonedadescdet.Text = Entidad.Nombre_Moneda;
                                VerificarMonedaDet();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacoddet.Text) & string.IsNullOrEmpty(txtmonedadescdet.Text))
                    {
                        BuscarMonedaDet();
                        VerificarMonedaDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        Boolean Es_moneda_nacDet;
        public void BuscarMonedaDet()
        {
            try
            {
                txtmonedacoddet.Text = Accion.Formato(txtmonedacoddet.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacoddet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacoddet.Text.Trim().ToUpper())
                        {
                            Es_moneda_nacDet = T.Es_nacional;
                            txtmonedacoddet.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacoddet.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadescdet.Text = T.Nombre_Moneda;
                            txtmonedacoddet.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacoddet.EnterMoveNextControl = false;
                    txtmonedacoddet.ResetText();
                    txtmonedacoddet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void VerificarMonedaDet()
        {
            if (!string.IsNullOrEmpty(txtmonedacoddet.Text.Trim()))
            {

                if (Es_moneda_nacDet == true)
                {
                    txttipocambiocoddet.Enabled = false;
                    txttipocambiocoddet.Text = "SCV";
                    txttipocambiodescdet.Text = "SIN CONVERSION";
                    txttipocambiovalordet.Enabled = false;
                    txttipocambiovalordet.Text = "1.000";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocoddet.Text))
                    {
                        txttipocambiocoddet.Enabled = true;
                        txttipocambiocoddet.ResetText();
                        txttipocambiodescdet.ResetText();
                        txttipocambiovalordet.ResetText();
                    }

                }
            }
        }

        private void txttipocambiocoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocoddet.Text) == false)
            {
                if (IsDate(txtfechadocdet.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocoddet.Text.Substring(txttipocambiocoddet.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadocdet.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocoddet.Text = Entidad.Codigo;
                                    txttipocambiodescdet.Text = Entidad.Nombre;
                                    txttipocambiovalordet.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocoddet.Text == "OTR")
                                    {
                                        txttipocambiovalordet.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalordet.Enabled = false;
                                    }

                                    txttipocambiocoddet.EnterMoveNextControl = true;

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocoddet.Text) & string.IsNullOrEmpty(txttipocambiodescdet.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadocdet.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocoddet.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocoddet.Text = T.Codigo;
                                            txttipocambiodescdet.Text = T.Nombre;
                                            txttipocambiovalordet.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocoddet.Text == "OTR")
                                            {
                                                txttipocambiovalordet.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalordet.Enabled = false;
                                            }
                                            txttipocambiocoddet.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadocdet.Focus();

                }

            }
        }

        private void txtrucdnidet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdnidet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdnidet.Text.Substring(txtrucdnidet.Text.Length - 1, 1) == "*")
                    {
                        using ( frm_entidades_busqueda f = new  frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdnidet.Text = Entidad.Ent_RUC_DNI;
                                txtentidaddet.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdnidet.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdnidet.Text) & string.IsNullOrEmpty(txtentidaddet.Text))
                    {
                        BuscarEntidadDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEntidadDet()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdnidet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdnidet.Text.Trim().ToUpper())
                        {
                            txtrucdnidet.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidaddet.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdnidet.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdnidet.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdnidet.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdnidet.Text.Trim().ToUpper())
                                    {
                                        txtrucdnidet.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidaddet.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdnidet.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdnidet.EnterMoveNextControl = false;
                                txtrucdnidet.ResetText();
                                txtrucdnidet.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoentcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipoentcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoentcod.Text.Substring(txttipoentcod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_entidad_busqueda f = new _1_Busquedas_Generales.frm_tipo_entidad_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoentcod.Text = Entidad.Id_Tipo_Ent;
                                txttipoentdesc.Text = Entidad.Ent_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoentcod.Text) & string.IsNullOrEmpty(txttipoentdesc.Text))
                    {
                        BuscarTipoEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTipoEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Id_Tipo_Ent = txttipoentcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Id_Tipo_Ent).ToString().Trim().ToUpper() == txttipoentcod.Text.Trim().ToUpper())
                        {
                            txttipoentcod.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txttipoentdesc.Text = T.Ent_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoentcoddet_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipoentcoddet.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoentcoddet.Text.Substring(txttipoentcoddet.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_entidad_busqueda f = new _1_Busquedas_Generales.frm_tipo_entidad_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoentcoddet.Text = Entidad.Id_Tipo_Ent;
                                txttipoentdescdet.Text = Entidad.Ent_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoentcoddet.Text) & string.IsNullOrEmpty(txttipoentdescdet.Text))
                    {
                        BuscarTipoEntidadDet();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTipoEntidadDet()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Id_Tipo_Ent = txttipoentcoddet.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Id_Tipo_Ent).ToString().Trim().ToUpper() == txttipoentcoddet.Text.Trim().ToUpper())
                        {
                            txttipoentcoddet.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txttipoentdescdet.Text = T.Ent_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmedpago_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmedpago.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmedpago.Text.Substring(txtmedpago.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_medio_pago_busqueda f = new _1_Busquedas_Generales.frm_medio_pago_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Medio_Pago Entidad = new Entidad_Medio_Pago();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmedpago.Text = Entidad.Id_SUNAT;
                                txtmedpagodesc.Text = Entidad.Med_Descripcion;
                                txtmedpago.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmedpago.Text) & string.IsNullOrEmpty(txtmedpagodesc.Text))
                    {
                        BuscarMedioPago();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarMedioPago()
        {
            try
            {
                txtmedpago.Text = Accion.Formato(txtmedpago.Text, 3);
                Logica_Medio_Pago log = new Logica_Medio_Pago();

                List<Entidad_Medio_Pago> Generales = new List<Entidad_Medio_Pago>();
                Generales = log.Listar(new Entidad_Medio_Pago
                {
                    Id_SUNAT = txtmedpago.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Medio_Pago T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtmedpago.Text.Trim().ToUpper())
                        {
                            txtmedpago.Text = (T.Id_SUNAT).ToString().Trim();
                            txtmedpagodesc.Text = T.Med_Descripcion;
                            txtmedpago.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmedpago.EnterMoveNextControl = false;
                    txtmedpago.ResetText();
                    txtmedpago.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void tctctacorriente_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(tctctacorriente.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & tctctacorriente.Text.Substring(tctctacorriente.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_cuenta_corriente_busqueda f = new _1_Busquedas_Generales.frm_cuenta_corriente_busqueda())
                        {
                            f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            f.Id_Anio = Actual_Conexion.AnioSelect;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Cuenta_Corriente Entidad = new Entidad_Cuenta_Corriente();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                tctctacorriente.Text = Entidad.Id_Cuenta_Corriente;
                                 tctctacorriente.EnterMoveNextControl = true;
                                txtentfinanciera.Text = Entidad.Cor_Ent_Financiera;
                                txtentfinancieradesc.Text = Entidad.Fin_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(tctctacorriente.Text) & string.IsNullOrEmpty(tctctacorriente.Text))
                    {
                        BuscarCtaCorriente();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarCtaCorriente()
        {
            try
            {
                //txtmedpago.Text = /*Accion.Formato(txtmedpago.Text, 2);*/
                Logica_Cuenta_Corriente log = new Logica_Cuenta_Corriente();

                List<Entidad_Cuenta_Corriente> Generales = new List<Entidad_Cuenta_Corriente>();
                Generales = log.Listar(new Entidad_Cuenta_Corriente
                {
                    Id_Empresa=Actual_Conexion.CodigoEmpresa,
                    Id_Anio=Actual_Conexion.AnioSelect,
                    Id_Cuenta_Corriente = tctctacorriente.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Cuenta_Corriente T in Generales)
                    {
                        if ((T.Id_Cuenta_Corriente).ToString().Trim().ToUpper() == tctctacorriente.Text.Trim().ToUpper())
                        {
                            tctctacorriente.Text = (T.Id_Cuenta_Corriente).ToString().Trim();
                            txtentfinanciera.Text = (T.Cor_Ent_Financiera).ToString().Trim();
                            txtentfinancieradesc.Text = (T.Fin_Descripcion).ToString().Trim();
                            tctctacorriente.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    tctctacorriente.EnterMoveNextControl = false;
                    tctctacorriente.ResetText();
                    tctctacorriente.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtentfinanciera_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtentfinanciera.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtentfinanciera.Text.Substring(txtentfinanciera.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_entidad_financiera_busqueda f = new _1_Busquedas_Generales.frm_entidad_financiera_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad_Financiera Entidad = new Entidad_Entidad_Financiera();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtentfinanciera.Text = Entidad.Id_SUNAT;
                                txtentfinancieradesc.Text = Entidad.Fin_Descripcion;
                                txtentfinanciera.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtentfinanciera.Text) & string.IsNullOrEmpty(txtentfinancieradesc.Text))
                    {
                        BuscarEntidadFinanciera();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarEntidadFinanciera()
        {
            try
            {
                txtentfinanciera.Text = Accion.Formato(txtentfinanciera.Text, 2);
                Logica_Entidad_Financiera log = new Logica_Entidad_Financiera();

                List<Entidad_Entidad_Financiera> Generales = new List<Entidad_Entidad_Financiera>();
                Generales = log.Listar(new Entidad_Entidad_Financiera
                {
                   
                    Id_SUNAT = txtentfinanciera.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad_Financiera T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtentfinanciera.Text.Trim().ToUpper())
                        {
                            txtentfinanciera.Text = (T.Id_SUNAT).ToString().Trim();
                            txtentfinancieradesc.Text = T.Fin_Descripcion;
                            txtentfinanciera.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtentfinanciera.EnterMoveNextControl = false;
                    txtentfinanciera.ResetText();
                    txtentfinanciera.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();
        private void btnbuscarprovision_Click(object sender, EventArgs e)
        {
            try
            {
                using (Tesoreria.frm_egreso_provision f = new Tesoreria.frm_egreso_provision())
                {
                    f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    f.Id_Anio = Actual_Conexion.AnioSelect;
                    //f.Id_Periodo = Actual_Conexion.PeriodoSelect;
                    f.Id_Libro = "007";

                    if (f.ShowDialog(this) == DialogResult.OK)
                    {
                        foreach (int i in f.gridView1.GetSelectedRows())
                        {
                            Entidad_Movimiento_Cab Doc = new Entidad_Movimiento_Cab();
                            Doc = f.Lista[f.gridView1.GetDataSourceRowIndex(i)];
                            //Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                            Doc.Id_Item = (Detalles.Count + 1);
          
                            Doc.Id_Anio_Ref = Doc.Id_Anio_Ref;
                            Doc.Id_Periodo_Ref = Doc.Id_Periodo_Ref;
                            Doc.Id_Libro_Ref = Doc.Id_Libro_Ref;
                            Doc.Id_Voucher_Ref = Doc.Id_Voucher_Ref;

                            //0004    01  DEBE
                            //0005    02  HABER

                            if (Doc.Ctb_Tipo_DH == "0005")
                            {
                                Doc.Ctb_Tipo_DH = "0004";
                                Doc.CCtb_Tipo_DH_Interno = "01";
                                Doc.Ctb_Tipo_DH_Desc = "DEBE";
                                Doc.Ctb_Importe_Debe = Doc.Ctb_Importe_Haber;
                                Doc.Ctb_Importe_Haber = 0;
                                Doc.Ctb_Importe_Debe_Extr = Doc.Ctb_Importe_Haber_Extr;
                                Doc.Ctb_Importe_Haber_Extr = 0;
                            }

            
                            if (ExistsInDetails(Doc.Ctb_Cuenta, Doc.Ctb_Tipo_Doc_det, Doc.Ctb_Serie_det, Doc.Ctb_Numero_det, Doc.Ctb_Tipo_Ent_det, Doc.Entidad_det))
                            {
                                Accion.Advertencia("El Documento ya existe en la lista de agregados");
                            }
                            else
                            {
                                Detalles.Add(Doc);
                            }

                        }


                        //dgvdatos.DataSource = null;
                        if (Detalles.Count>0)
                        {
                            //dgvdatos.DataSource = Detalles;
                        }

                
                    }

                }

                }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        bool ExistsInDetails(string CtaCble, string TipDoc, string Serie, string Numero, string TipEnt, string Ent)
        {
            foreach (Entidad_Movimiento_Cab Item in Detalles)
            {
                if ((Item.Ctb_Cuenta == CtaCble) && (Item.Ctb_Tipo_Doc_det == TipDoc) && (Item.Ctb_Serie_det == Serie) && (Item.Ctb_Numero_det == Numero) && (Item.Ctb_Tipo_Ent_det == TipEnt) && (Item.Ctb_Ruc_dni_det == Ent))
                {
                    return true;
                }

            }

            return false;
        }

        int Id_Item;

        string Anio_Ref, Periodo_Ref, Voucher_Ref, Libro_Ref;
        int Item_Ref;
        decimal ImporteNoMay;
        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    Id_Item = Entidad.Id_Item;


                    txtcuenta.Text = Entidad.Ctb_Cuenta;
                    txtcuentadesc.Text = Entidad.Ctb_Cuenta_Desc;

                    txttipodocdet.Text = Entidad.Ctb_Tipo_Doc_det_Sunat;
                    txttipodocdet.Tag = Entidad.Ctb_Tipo_Doc_det;
                    txttipodocdescdet.Text = Entidad.Ctb_Tipo_Doc_det_desc;

                    txtseriedet.Text = Entidad.Ctb_Serie_det + "-" + Entidad.Ctb_Numero_det;


                    txtfechadocdet.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Ctb_Fecha_Mov_det);


                    txtmonedacoddet.Text = Entidad.Ctb_moneda_cod_det_Sunat;
                    txtmonedacoddet.Tag = Entidad.Ctb_moneda_cod_det;
                    txtmonedadescdet.Text = Entidad.Ctb_moneda_det_desc;

                    txttipocambiocoddet.Text = Entidad.Ctb_Tipo_Cambio_Cod_Det;
                    txttipocambiodescdet.Text = Entidad.Ctb_Tipo_Cambio_Desc_Det;
                    txttipocambiovalordet.Text = Convert.ToDecimal( Entidad.Ctb_Tipo_Cambio_Valor_Det).ToString();

                    txttipoentcoddet.Text = Entidad.Ctb_Tipo_Ent_det;
                    txttipoentdescdet.Text = Entidad.Ctb_Tipo_Ent_det_Desc;
                    txtrucdnidet.Text = Entidad.Ctb_Ruc_dni_det;
                    txtentidaddet.Text = Entidad.Entidad_det;
       

                    txttipo.Tag = Entidad.Ctb_Tipo_DH;
                    txttipo.Text = Entidad.CCtb_Tipo_DH_Interno;
                    txttipodesc.Text = Entidad.Ctb_Tipo_DH_Desc;

                        //txtimporteMNDet.Text = Entidad.Ctb_Importe_Debe.ToString();
                        //txtimporteMEDet.Text = Entidad.Ctb_Importe_Debe_Extr.ToString();

                    if (Entidad.Ctb_Tipo_DH == "0004")
                    {
                            txtimporteMNDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe).ToString();
                            txtimporteMEDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe_Extr).ToString();
                        ImporteNoMay = Entidad.Ctb_Importe_Debe;
                    }
                    else if (Entidad.Ctb_Tipo_DH == "0005")
                    {
                            txtimporteMNDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber).ToString();
                            txtimporteMEDet.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber_Extr).ToString();
                        ImporteNoMay = Entidad.Ctb_Importe_Debe;
                    }

                    Anio_Ref = Entidad.Id_Anio_Ref;
                    Periodo_Ref = Entidad.Id_Periodo_Ref;
                    Libro_Ref=Entidad.Id_Libro_Ref;
                    Voucher_Ref = Entidad.Id_Voucher_Ref;
                    Item_Ref =Entidad.Id_Item_Ref;


                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle = Estados.Consulta;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle = Estados.SoloLectura;


                    }

               
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtimporte_TextChanged(object sender, EventArgs e)
        {
            MostrarImporteMN();
        }
        void MostrarImporteMN() {
                try
                {
                decimal importe = Convert.ToDecimal(txtimporte.Text);
                decimal Tc = Convert.ToDecimal(txttipocambiovalor.Text);
                decimal Resultado;
                Resultado= importe * Tc;//(double.Parse(txtimporte.Text).ToString() * double.Parse(txttipocambiovalor.Text).ToString());
                txtimporteMN.Text = Resultado.ToString();
            }
                catch (Exception ex)
                {
                txtimporteMN.Text ="0.00";
                }

            }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            if (VerificarNuevoDet())
            {
                EstadoDetalle = Estados.Nuevo;

                //BuscarAnalisis_DETALLE();
        
                HabilitarDetalles();

            }
        }

        public bool VerificarNuevoDet()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }

            //if (string.IsNullOrEmpty(txtnumero.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar un numero");
            //    txtnumero.Focus();
            //    return false;
            //}

            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            return true;
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {


                    Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                 
                    //dgvdatos.DataSource = null;
                    if (Detalles.Count > 0)
                    {
                        //dgvdatos.DataSource = Detalles;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Movimiento_Cab Det in Detalles)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {

            try
            {
       if (VerificarDetalle()) {
    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                Entidad.Id_Item  = Id_Item;


                Entidad.Ctb_Cuenta =txtcuenta.Text ;
                Entidad.Ctb_Cuenta_Desc  =txtcuentadesc.Text ;

                    Entidad.Ctb_Tipo_Doc_det_Sunat = txttipodocdet.Text;
                    Entidad.Ctb_Tipo_Doc_det  =txttipodocdet.Tag.ToString() ;
                Entidad.Ctb_Tipo_Doc_det_desc = txttipodocdescdet.Text;

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Entidad.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Entidad.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();

                    Entidad.Ctb_Fecha_Mov_det =Convert.ToDateTime(txtfechadocdet.Text);//String.Format("{0:yyyy-MM-dd}",);

                    Entidad.Ctb_moneda_cod_det_Sunat = txtmonedacoddet.Text;
                    Entidad.Ctb_moneda_cod_det = txtmonedacoddet.Tag.ToString();
                Entidad.Ctb_moneda_det_desc =txtmonedadescdet.Text ;

                Entidad.Ctb_Tipo_Cambio_Cod_Det  = txttipocambiocoddet.Text;
                Entidad.Ctb_Tipo_Cambio_Desc_Det =  txttipocambiodescdet.Text ;
                Entidad.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalordet.Text);// Convert.ToDecimal().ToString();

                Entidad.Ctb_Tipo_Ent_det =  txttipoentcoddet.Text ;
                Entidad.Ctb_Tipo_Ent_det_Desc =  txttipoentdescdet.Text ;
                Entidad.Ctb_Ruc_dni_det  =  txtrucdnidet.Text;
                Entidad.Entidad_det  =  txtentidaddet.Text;


                Entidad.Ctb_Tipo_DH =  txttipo.Tag.ToString();
                Entidad.CCtb_Tipo_DH_Interno  = txttipo.Text;
                Entidad.Ctb_Tipo_DH_Desc  = txttipodesc.Text;



                    //  if (txtimporteMNDet.Text == "")
                    //  {
                    //      Entidad.Ctb_Importe_Debe = 0;
                    //  }
                    //  else
                    //  {
                    //   Entidad.Ctb_Importe_Debe =Convert.ToDecimal(txtimporteMNDet.Text);
                    //  }

                    //  if (txtimporteMEDet.Text == "")
                    //  {
                    //      Entidad.Ctb_Importe_Debe_Extr = 0;
                    //  }
                    //  else
                    //  {
                    //Entidad.Ctb_Importe_Debe_Extr=Convert.ToDecimal(txtimporteMEDet.Text) ;
                    //  }

                    Entidad.Id_Anio_Ref     = Anio_Ref ;
                    Entidad.Id_Periodo_Ref    =  Periodo_Ref;
                    Entidad.Id_Libro_Ref   = Libro_Ref;
                    Entidad.Id_Voucher_Ref = Voucher_Ref;
                    Entidad.Id_Item_Ref =  Item_Ref ;

                    if (Entidad.Ctb_Tipo_DH == "0004")
                    {
                        if ((txtimporteMEDet.Text == "") || (txtimporteMNDet.Text == ""))
                        {
                            txtimporteMEDet.Text = IIf(txtimporteMEDet.Text == "", "0.00", txtimporteMEDet.Text).ToString(); 
                            txtimporteMNDet.Text = IIf(txtimporteMNDet.Text == "", "0.00", txtimporteMNDet.Text).ToString();
                        }
                       
                            Entidad.Ctb_Importe_Debe = Convert.ToDecimal(txtimporteMNDet.Text) * Convert.ToDecimal(txttipocambiovalordet.Text);
                            Entidad.Ctb_Importe_Debe_Extr = Convert.ToDecimal(txtimporteMEDet.Text.ToString());
                        
                
                       

                    }
                    else if (Entidad.Ctb_Tipo_DH == "0005")
                    {

                        if ((txtimporteMEDet.Text == "")||(txtimporteMNDet.Text==""))
                        {
                            txtimporteMEDet.Text = IIf(txtimporteMEDet.Text == "", "0.00", txtimporteMEDet.Text).ToString();
                            txtimporteMNDet.Text = IIf(txtimporteMNDet.Text == "", "0.00", txtimporteMNDet.Text).ToString();
                        }
                    
                        Entidad.Ctb_Importe_Haber = Convert.ToDecimal(txtimporteMNDet.Text) * Convert.ToDecimal(txttipocambiovalordet.Text);
                        Entidad.Ctb_Importe_Haber_Extr = Convert.ToDecimal(txtimporteMEDet.Text.ToString());
                       
                      
                   
                    }

                    if (EstadoDetalle == Estados.Nuevo)
            {
                Detalles.Add(Entidad);


                UpdateGrilla();
                EstadoDetalle = Estados.Guardado;
            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                Detalles[Id_Item - 1] = Entidad;

                UpdateGrilla();
                EstadoDetalle = Estados.Guardado;

            }
         
            btnnuevodet.Focus();
            }
        
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
     

        }

        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }
        public bool VerificarDetalle()
        {
            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una cuenta contable");
                txtcuenta.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipodocdescdet.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodocdet.Focus();
                return false;
            }

      if (string.IsNullOrEmpty(txttipodesc.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un tipo (Debe/Haber)");
                txttipo.Focus();
                return false;
            }

            if (Convert.ToDouble(txtimporteMNDet.Text) <= 0)
            {
                //tools.ShowWarnig("VALOR INCORRECTO EN CANTIDAD", "Debe de ingresar un valor numérico mayor a CERO en el campo cantidad antes de grabar");
                Accion.Advertencia("Debe ingresar un importe");
                txtimporteMNDet.Focus();
                return false;
            }

            if (Convert.ToDecimal(txtimporteMNDet.Text) > ImporteNoMay)
            {
                Accion.Advertencia("No puede modificar el importe del documento por otro mayor a " + " " + ImporteNoMay);
                txtimporteMNDet.Focus();
                return false;
            }


            return true;
        }
        public void UpdateGrilla()
        {
            //dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                //dgvdatos.DataSource = Detalles;
            }
        }

        private void txttipo_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipo.Text.Substring(txttipo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0002";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipo.Tag = Entidad.Id_General_Det;
                                txttipo.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipo.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipo.Text) & string.IsNullOrEmpty(txttipodesc.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipo()
        {
            try
            {
                txttipo.Text = Accion.Formato(txttipo.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0002",
                    Gen_Codigo_Interno = txttipo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipo.Text.Trim().ToUpper())
                        {
                            txttipo.Tag = T.Id_General_Det;
                            txttipo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesc.Text = T.Gen_Descripcion_Det;
                            txttipo.EnterMoveNextControl = true;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipo.EnterMoveNextControl = false;
                    txttipo.ResetText();
                    txttipo.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipomovimientocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipomovimientocod.Focus() == false)
            {
                txttipomovimientodesc.ResetText();
            }
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txttipoentcod_TextChanged(object sender, EventArgs e)
        {
            if (txttipoentcod.Focus() == false)
            {
                txttipoentdesc.ResetText();
            }
        }

        private void txtrucdni_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdni.Focus() == false)
            {
                txtentidad.ResetText();
            }
        }

        private void txtmedpago_TabStopChanged(object sender, EventArgs e)
        {
            if (txtmedpago.Focus() == false)
            {
                txtmedpagodesc.ResetText();
            }
        }

        private void txtentfinanciera_TextChanged(object sender, EventArgs e)
        {
            if (txtentfinanciera.Focus() == false)
            {
                txtentfinancieradesc.ResetText();
            }
        }

        private void txtmonedacod_TabStopChanged(object sender, EventArgs e)
        {
           
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocod.Focus() == false)
            {
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
            }
        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            if (txtcuenta.Focus() == false)
            {
                txtcuentadesc.ResetText();
            }
        }

        private void txttipodocdet_TextChanged(object sender, EventArgs e)
        {
            if (txttipodocdet.Focus() == false)
            {
                txttipodocdescdet.ResetText();
            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacod.Focus() == false)
            {
                txtmonedadesc.ResetText();
                txttipocambiocod.ResetText();
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
                txtmonedacod.Focus();

            }
        }

        private void txtmonedacoddet_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacoddet.Focus() == false)
            {
                txtmonedadescdet.ResetText();
                txttipocambiocoddet.ResetText();
                txttipocambiodescdet.ResetText();
                txttipocambiovalordet.Text = "0.000";
                txtmonedacoddet.Focus();

            }
        }

        private void txttipocambiocoddet_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocoddet.Focus() == false)
            {
                txttipocambiodescdet.ResetText();
                txttipocambiovalordet.Text = "0.000";
            }
        }

        private void txtseriedet_Leave(object sender, EventArgs e)
        {
   
            try
            {
                if (!string.IsNullOrEmpty(txtseriedet.Text))
                {
                    string Serie_Numero = txtseriedet.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtseriedet.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        txtseriedet.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        txtseriedet.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtseriedet.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtnumerodet_Leave(object sender, EventArgs e)
        {
          
        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txttipoentcoddet_TextChanged(object sender, EventArgs e)
        {
            if (txttipoentcoddet.Focus())
            {
                txttipoentdescdet.ResetText();
            }
        }

        private void txtrucdnidet_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdnidet.Focus() == false)
            {
                txtentidaddet.ResetText();
            }
        }

        private void txttipo_TextChanged(object sender, EventArgs e)
        {
            if (txttipo.Focus() == false)
            {
                txttipodesc.ResetText();
            }
        }

        private void txtserie_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtserie.Text))
                {
                    string Serie_Numero = txtserie.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        //txtserie.Text = Accion.Formato(txtserie.Text, 4);
                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtserie.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
       
        }


        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }



            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }



            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }



            return true;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
                    Ent.Id_Libro = txtlibro.Tag.ToString();
                    Ent.Id_Voucher = Voucher;

                    Ent.Ctb_Tipo_IE = txttipoEgreso.Tag.ToString();
                    Ent.Ctb_Tipo_Movimiento = txttipomovimientocod.Tag.ToString();

                    Ent.Ctb_Glosa = txtglosa.Text;
                    Ent.Ctb_Tipo_Doc = txttipodoc.Tag.ToString();

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Ctb_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Ctb_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();

                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);

                    Ent.Ctb_Tipo_Ent = txttipoentcod.Text;
                    Ent.Ctb_Ruc_dni = txtrucdni.Text;

                    Ent.Ctb_Medio_Pago = txtmedpago.Text;
                    Ent.Ctb_Numero_Transaccion = txtnumetransaccion.Text;

                    Ent.Ctb_Cuenta_Corriente = tctctacorriente.Text;
                    Ent.Ctb_Entidad_Financiera = txtentfinanciera.Text;


                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;
                    Ent.Ctb_Es_moneda_nac = Es_moneda_nac;

                    Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                   Ent.Ctb_Importe = Convert.ToDecimal(txtimporte.Text);
                    



                    //detalle Contable
                    Ent.DetalleAsiento = Detalles;
      

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar_Tesoreria(Ent))
                        {
                           
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();
                       
                            Detalles.Clear();
                            
                            //dgvdatos.DataSource = null;

                            BloquearDetalles();
                            TraerLibro();
                            txtglosa.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar_Tesoreria(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
