﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_series_documentos : Contable.frm_fuente
    {
        public frm_series_documentos()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_series_documentos_edicion f = new frm_series_documentos_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";
                f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        public List<Entidad_Series_Tesorerira> Lista = new List<Entidad_Series_Tesorerira>();
        public void Listar()
        {
            Entidad_Series_Tesorerira Ent = new Entidad_Series_Tesorerira();
            Logica_Series_Tesoreria log = new Logica_Series_Tesoreria();

            try
            {
                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;


                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        Entidad_Series_Tesorerira Entidad = new Entidad_Series_Tesorerira();
        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_series_documentos_edicion f = new frm_series_documentos_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";
                f.Id_Empresa = Entidad.Id_Empresa;
               f.Ser_Tipo_Doc = Entidad.Ser_Tipo_Doc;
                f.Ser_Tip_IE   =Entidad.Ser_Tip_IE ;
                f.Ser_Tipo_Mov = Entidad.Ser_Tipo_Mov ;
                f.Ser_Serie  = Entidad.Ser_Serie;
                f.Ser_Emitiendose = Entidad.Ser_Emitiendose;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    if (Lista.Count > 0)
                    {
                        Estado = Estados.Ninguno;
                        Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void frm_series_documentos_Load(object sender, EventArgs e)
        {
            Listar();
        }
    }
}
