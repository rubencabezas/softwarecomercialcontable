﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_medio_pago : Contable.frm_fuente
    {
        public frm_medio_pago()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_medio_pago_edicion f = new frm_medio_pago_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        public List<Entidad_Medio_Pago> Lista = new List<Entidad_Medio_Pago>();
        public void Listar()
        {
            Entidad_Medio_Pago Ent = new Entidad_Medio_Pago();
            Logica_Medio_Pago log = new Logica_Medio_Pago();

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_medio_pago_Load(object sender, EventArgs e)
        {
            Listar();
        }

        Entidad_Medio_Pago Entidad = new Entidad_Medio_Pago();
        private void gridView1_Click(object sender, EventArgs e)
        {
            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_medio_pago_edicion f = new frm_medio_pago_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Sunat = Entidad.Id_SUNAT;
                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\MedioPago" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
