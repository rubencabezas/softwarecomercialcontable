﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Tesoreria
{
    public partial class frm_ingreso : Contable.frm_fuente
    {
        public frm_ingreso()
        {
            InitializeComponent();
        }

        string Id_Libro;

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_CajaBanco(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_CajaBanco;

                }
                else
                {
                    Accion.Advertencia("Debe configurar libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;

            Ent.Ctb_Tipo_IE = "0049"; //Ingreso
            
            dgvdatos.DataSource = null;

            try
            {
                Lista = log.Listar_Tesoreria(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            //{
                 using (frm_caja_banco_edicion _edicion = new frm_caja_banco_edicion())
                {
                    base.Estado = Estados.Nuevo;
                    _edicion.Estado_Ven_Boton = "1";
                    _edicion.Id_Periodo = Actual_Conexion.PeriodoSelect;
                    if (_edicion.ShowDialog() != DialogResult.OK)
                    {
                        this.Listar();
                    }
                    else
                    {
                        try
                        {
                            this.Listar();
                        }
                        catch (Exception exception1)
                        {
                            throw new Exception(exception1.Message);
                        }
                    }
                }
            //}
   

        }

        private void frm_ingreso_Load(object sender, EventArgs e)
        {

            TraerLibro();
            Listar();
        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_caja_banco_edicion _edicion = new frm_caja_banco_edicion())
            {
                base.Estado = Estados.Modificar;
                _edicion.Estado_Ven_Boton = "2";
                _edicion.Id_Empresa = this.Entidad.Id_Empresa;
                _edicion.Id_Anio = this.Entidad.Id_Anio;
                _edicion.Id_Periodo = this.Entidad.Id_Periodo;
                _edicion.Id_Libro = this.Entidad.Id_Libro;
                _edicion.Voucher = this.Entidad.Id_Voucher;
                if (_edicion.ShowDialog() != DialogResult.OK)
                {
                    this.Listar();
                }
                else
                {
                    try
                    {
                        this.Listar();
                    }
                    catch (Exception exception1)
                    {
                        Accion.ErrorSistema(exception1.Message);
                    }
                }
            }

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string[] textArray1 = new string[] { "EL registro a ELIMINAR tiene la caraterisctica:", this.Entidad.Id_Voucher, " ", this.Entidad.Nombre_Comprobante, " ", this.Entidad.Ctb_Serie, " ", this.Entidad.Ctb_Numero };
            if (Accion.ShowDeleted(string.Concat(textArray1)))
            {
                try
                {
                    Entidad_Movimiento_Cab cab1 = new Entidad_Movimiento_Cab();
                    cab1.Id_Empresa = this.Entidad.Id_Empresa;
                    cab1.Id_Anio = this.Entidad.Id_Anio;
                    cab1.Id_Periodo = this.Entidad.Id_Periodo;
                    cab1.Id_Voucher = this.Entidad.Id_Voucher;
                    cab1.Id_Libro = this.Entidad.Id_Libro;



                    Logica_Movimiento_Cab log_det = new Logica_Movimiento_Cab();
                    cab1.DetalleAsiento = log_det.Listar_Tesoreria_Det(cab1);
                    Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

                    log.Eliminar_Ingresos_Egresos(cab1);
                    Accion.ExitoGuardar();
                    this.Listar();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TraerLibro();
            Listar();
        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Caja" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
