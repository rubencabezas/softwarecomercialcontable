﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras.Catalogo_Servicios
{
    public partial class frm_grupo_ser : Contable.frm_fuente
    {
        public frm_grupo_ser()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using ( frm_grupo_edicion f = new  frm_grupo_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Grupo Ent = new Entidad_Grupo();
                    Logica_Grupo Log = new Logica_Grupo();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Tipo = "0032";
                    Ent.Gru_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        Entidad_Grupo Entidad = new Entidad_Grupo();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using ( frm_grupo_edicion f = new  frm_grupo_edicion())
            {

                f.txtdescripcion.Tag = Entidad.Id_Grupo;

                f.txtdescripcion.Text = Entidad.Gru_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Grupo Ent = new Entidad_Grupo();
                    Logica_Grupo Log = new Logica_Grupo();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Tipo = "0032";
                    Ent.Id_Grupo = f.txtdescripcion.Tag.ToString();
                    Ent.Gru_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }



        public List<Entidad_Grupo> Lista = new List<Entidad_Grupo>();
        public void Listar()
        {
            Entidad_Grupo Ent = new Entidad_Grupo();
            Logica_Grupo log = new Logica_Grupo();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Tipo = "0032";
            Ent.Id_Grupo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_grupo_ser_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La grupo a eliminar es el siguiente :" + Entidad.Gru_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Grupo Ent = new Entidad_Grupo
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Grupo = Entidad.Id_Grupo,
                        Id_Tipo = Entidad.Id_Tipo
                    };

                    Logica_Grupo log = new Logica_Grupo();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\SerGrupo" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
