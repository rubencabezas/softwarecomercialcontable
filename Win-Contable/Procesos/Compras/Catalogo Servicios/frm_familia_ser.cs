﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras.Catalogo_Servicios
{
    public partial class frm_familia_ser : Contable.frm_fuente
    {
        public frm_familia_ser()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using ( frm_familia_edicion f = new  frm_familia_edicion())
            {


                Estado = Estados.Nuevo;

                f.Tipo = "0032";

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Familia Ent = new Entidad_Familia();
                    Logica_Familia Log = new Logica_Familia();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Grupo = f.txtgrupocod.Text;
                    Ent.Id_Tipo = "0032";
                    Ent.Fam_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using ( frm_familia_edicion f = new frm_familia_edicion())
            {

                f.txtgrupocod.Text = Entidad.Id_Grupo;
                f.txtgrupodesc.Text = Entidad.Gru_Descripcion;
                f.txtdescripcion.Tag = Entidad.Id_Familia;
                f.txtdescripcion.Text = Entidad.Fam_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Familia Ent = new Entidad_Familia();
                    Logica_Familia Log = new Logica_Familia();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Tipo = "0032";
                    Ent.Id_Grupo = f.txtgrupocod.Text;
                    Ent.Gru_Descripcion = f.txtgrupodesc.Text;
                    Ent.Id_Familia = f.txtdescripcion.Tag.ToString();
                    Ent.Fam_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }


        Entidad_Familia Entidad = new Entidad_Familia();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }


        public List<Entidad_Familia> Lista = new List<Entidad_Familia>();
        public void Listar()
        {
            Entidad_Familia Ent = new Entidad_Familia();
            Logica_Familia log = new Logica_Familia();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Tipo = "0032";
            Ent.Id_Grupo = null;
            Ent.Id_Familia = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_familia_ser_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La familia a eliminar es el siguiente :" + Entidad.Fam_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Familia Ent = new Entidad_Familia
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Grupo = Entidad.Id_Grupo,
                        Id_Tipo = Entidad.Id_Tipo,
                        Id_Familia = Entidad.Id_Familia
                    };

                    Logica_Familia log = new Logica_Familia();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\SerFamilia" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
