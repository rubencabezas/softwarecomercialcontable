﻿using Comercial;
using Contable.Procesos.Inventario.Catalogo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras.Catalogo_Servicios
{
    public partial class frm_catalogo_ser : Contable.frm_fuente
    {
        public frm_catalogo_ser()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //using (Procesos.Inventario.Catalogo.frm_catalogo_edicion f = new Procesos.Inventario.Catalogo.frm_catalogo_edicion())
            //{


            //    Estado = Estados.Nuevo;
            //    f.Tipo = "0032";
            //    if (f.ShowDialog() == DialogResult.OK)
            //    {

            //        Entidad_Catalogo Ent = new Entidad_Catalogo();
            //        Logica_Catalogo Log = new Logica_Catalogo();

            //        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //        Ent.Id_Grupo = f.txtgrupocod.Text;
            //        Ent.Id_Familia = f.txtfamiliacod.Text;
            //        Ent.Id_Tipo = "0032";
            //        Ent.Cat_Descripcion = f.txtdescripcion.Text;

            //        Ent.Cat_Serie = f.txtserie.Text.Trim();
            //        Ent.Cat_Barra = f.txtbarra.Text.Trim();
            //        Ent.Id_Marca = f.txtmarcacod.Text;

            //        Ent.Id_Unidad_Medida = f.txtunmcod.Text;
            //        Ent.Id_Tipo_Existencia = f.txtexistenciacod.Text;

            //        try
            //        {
            //            if (Estado == Estados.Nuevo)
            //            {

            //                if (Log.Insertar(Ent))
            //                {
            //                    Listar();

            //                }
            //                else
            //                {
            //                    MessageBox.Show("No se puedo insertar");
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception(ex.Message);
            //        }



            //    }
            //}
            using (frm_catalogo_edicion f = new frm_catalogo_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Estado = Estados.Modificar;

            //using (Procesos.Inventario.Catalogo.frm_catalogo_edicion f = new Procesos.Inventario.Catalogo.frm_catalogo_edicion())
            //{

            //    f.txtgrupocod.Text = Entidad.Id_Grupo;
            //    f.txtgrupodesc.Text = Entidad.Gru_Descripcion;
            //    f.txtfamiliacod.Text = Entidad.Id_Familia;
            //    f.txtfamiliadesc.Text = Entidad.Fam_Descripcion;
            //    //Ent.Id_Tipo = "0031";
            //    f.txtdescripcion.Tag = Entidad.Id_Catalogo;
            //    f.txtdescripcion.Text = Entidad.Cat_Descripcion;

            //    f.txtserie.Text = Entidad.Cat_Serie;
            //    f.txtbarra.Text = Entidad.Cat_Barra;
            //    f.txtmarcacod.Text = Entidad.Id_Marca;
            //    f.txtmarcadesc.Text = Entidad.Mar_Descripcion;
            //    f.txtunmcod.Text = Entidad.Id_Unidad_Medida;
            //    f.txtunmdesc.Text = Entidad.Und_Descripcion;
            //    f.txtexistenciacod.Text = Entidad.Id_Tipo_Existencia.Trim();
            //    f.txtexistenciadesc.Text = Entidad.Exs_Nombre;


            //    if (f.ShowDialog() == DialogResult.OK)
            //    {

            //        Entidad_Catalogo Ent = new Entidad_Catalogo();
            //        Logica_Catalogo Log = new Logica_Catalogo();

            //        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //        Ent.Id_Tipo = "0032";
            //        Ent.Id_Grupo = f.txtgrupocod.Text.ToString();
            //        Ent.Gru_Descripcion = f.txtgrupodesc.Text;

            //        Ent.Id_Familia = f.txtfamiliacod.Text;
            //        Ent.Fam_Descripcion = f.txtfamiliadesc.Text;

            //        Ent.Id_Catalogo = f.txtdescripcion.Tag.ToString();
            //        Ent.Cat_Descripcion = f.txtdescripcion.Text;

            //        Ent.Cat_Serie = f.txtserie.Text.Trim();
            //        Ent.Cat_Barra = f.txtbarra.Text.Trim();
            //        Ent.Id_Marca = f.txtmarcacod.Text;
            //        Ent.Mar_Descripcion = f.txtmarcadesc.Text;
            //        Ent.Id_Unidad_Medida = f.txtunmcod.Text;
            //        Ent.Und_Descripcion = f.txtunmdesc.Text;
            //        Ent.Id_Tipo_Existencia = f.txtexistenciacod.Text.Trim();
            //        Ent.Exs_Nombre = f.txtexistenciadesc.Text;

            //        try
            //        {
            //            if (Estado == Estados.Modificar)
            //            {

            //                if (Log.Modificar(Ent))
            //                {
            //                    Listar();
            //                }
            //                else
            //                {
            //                    MessageBox.Show("No se puedo insertar");
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception(ex.Message);
            //        }



            //    }
            //}

            using (frm_catalogo_edicion f = new frm_catalogo_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_tipo = Entidad.Id_Tipo;
                f.Id_Catalogo = Entidad.Id_Catalogo;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }


        }
        public List<Entidad_Catalogo> Lista = new List<Entidad_Catalogo>();
        public void Listar()
        {
            Entidad_Catalogo Ent = new Entidad_Catalogo();
            Logica_Catalogo log = new Logica_Catalogo();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Tipo = "0032";
            Ent.Id_Grupo = null;
            Ent.Id_Familia = null;
            Ent.Id_Catalogo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
        private void frm_catalogo_ser_Load(object sender, EventArgs e)
        {
            Listar();
        }

        Entidad_Catalogo Entidad = new Entidad_Catalogo();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "El producto a eliminar es el siguiente :" + Entidad.Cat_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Catalogo Ent = new Entidad_Catalogo
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Grupo = Entidad.Id_Grupo,
                        Id_Tipo = Entidad.Id_Tipo,
                        Id_Familia = Entidad.Id_Familia,
                        Id_Catalogo = Entidad.Id_Catalogo
                    };

                    Logica_Catalogo log = new Logica_Catalogo();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\ServiciosBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
