﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable 
{
    public partial class frm_valores :  frm_fuente
    {
        public frm_valores()
        {
            InitializeComponent();
        }

        public decimal Base01 = 0;

      public  List<Entidad_Movimiento_Cab> Detalles_CONT_Val = new List<Entidad_Movimiento_Cab>();
        public DateTime Fecha;
        void Bloquear()
        {
            txtbase01.Enabled = false;
            txtbase02.Enabled = false;
            txtbase03.Enabled = false;
            txtigv01.Enabled = false;
            txtigv02.Enabled = false;
            txtigv03.Enabled = false;
            txtisc.Enabled = false;
            txtotc.Enabled = false;
            txttotal.Enabled = false;
            txtvaadgrab.Enabled = false;
        }
        void Desbloqear()
        {
            txtbase01.Enabled = true;
            txtbase02.Enabled = true;
            txtbase03.Enabled = true;
            txtigv01.Enabled = true;
            txtigv02.Enabled = true;
            txtigv03.Enabled = true;
            txtisc.Enabled = true;
            txtotc.Enabled = true;
            txttotal.Enabled = true;
            txtvaadgrab.Enabled = true;
        }
        void Habilitar_Orden()
        {
            try
            {
            foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT_Val)
            {
                Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();

                if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                {
                    txtbase01.Enabled = true;
                    txtbase01.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);
                    //txtbase01.Select();
       
                }
                else if ((itenms.Ctb_Operacion_Cod == "0021"))//IGV 01
                {
                    txtigv01.Enabled = true;
                    txtigv01.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);


                }
                else if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                {
                    txtbase02.Enabled = true;
                    txtbase02.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);

                }
                else if ((itenms.Ctb_Operacion_Cod == "0025"))//IGV 02
                {
                    txtigv02.Enabled = true;
                    txtigv02.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);

                }
                else if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                {
                    txtbase03.Enabled = true;
                    txtbase03.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);

                }
                else if ((itenms.Ctb_Operacion_Cod == "0036"))//IGV 03
                {
                    txtigv03.Enabled = true;
                    txtigv03.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);

                }
                else if ((itenms.Ctb_Operacion_Cod == "0037"))//VALOR DE ADQUISICION NO GRABADA
                {
                    txtvaadgrab.Enabled = true;
                    txtvaadgrab.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);
                }
                else if ((itenms.Ctb_Operacion_Cod == "0039"))//OTROS TRIBUTOS Y CARGOS
                {
                    txtotc.Enabled = true;
                    txtotc.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);

                }
                else if ((itenms.Ctb_Operacion_Cod == "0038"))//IMPUESTO SELECTIVO AL CONSUMO
                {
                    txtisc.Enabled = true;
                    txtisc.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Debe);

                }
                else if ((itenms.Ctb_Operacion_Cod == "0040"))//IMPORTE TOTAL
                {
                    txttotal.Enabled = true;
                    txttotal.Text = String.Format("{0:0,0.00}", itenms.Ctb_Importe_Haber);
                    txttotal.Select();

                }
            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void frm_valores_Load(object sender, EventArgs e)
        {
            Bloquear();
            Habilitar_Orden();
            ActualizaIGV();
            txttotal.Select();
        }
         void Calcular_Cuando_son_Base()
        {
            try
            {
               foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT_Val)
                {
                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                    {
                        

                        decimal resultadoBase1 = Math.Round(Convert.ToDecimal(txttotal.Text)/(Tasa+1),2);
                        decimal ResultadoIgv1 = Math.Round(resultadoBase1 * Tasa,2);

                        txtbase01.Text = Convert.ToString(resultadoBase1);
                        txtigv01.Text = Convert.ToString(ResultadoIgv1);


                    }
                   //else if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                   // {
                   //     decimal resultadoBase2 = Math.Round(Convert.ToDecimal(txtbase02.Text) * ((Tasa + 1) / 100), 4);
                   //     decimal ResultadoIgv2 = Math.Round(resultadoBase2 * Tasa, 4);

                   //     txtbase02.Text = Convert.ToString(resultadoBase2);
                   //     txtigv02.Text = Convert.ToString(ResultadoIgv2);

                   // }
                   // else if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                   // {
                   //     decimal resultadoBase3 = Math.Round(Convert.ToDecimal(txttotal.Text) * ((Tasa + 1) / 100), 4);
                   //     decimal ResultadoIgv3 = Math.Round(resultadoBase3 * Tasa, 4);

                   //     txtbase03.Text = Convert.ToString(resultadoBase3);
                   //     txtigv03.Text = Convert.ToString(ResultadoIgv3);

                   // }

                }
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        decimal Tasa;
        decimal Porcentaje;
        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(Convert.ToString(Fecha)) == true))
                {
                    if ((DateTime.Parse(Convert.ToString(Fecha)).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Fecha;

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            Tasa = Lista[0].Imd_Tasa;//0.18
                            Porcentaje = (Math.Round(Lista[0].Imd_Tasa * 100, 2));//18.00

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;

          
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtisc_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                this.SelectNextControl((Control)sender, true, true, true, true);
            }
        }

        private void frm_valores_KeyPress(object sender, KeyPressEventArgs e)
        {
                if (e.KeyChar == (char)(Keys.Enter))
                     {
                         e.Handled = true;
                        SendKeys.Send("{TAB}");
                   }
        }

 
        void Calcular_Total()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT_Val)
                {
                    if ((itenms.Ctb_Operacion_Cod == "0040"))//BASE IMPONIBLE 01
                    {
                        //if (( Convert.ToDecimal(txtbase01.Text)>0) || (Convert.ToDecimal(txttotal.Text) > 0))
                        //{
                            decimal resultadoBase1 = Math.Round(Convert.ToDecimal(txttotal.Text) / (Tasa + 1), 2);
                            decimal ResultadoIgv1 = Math.Round(resultadoBase1 * Tasa, 2);

                            txtbase01.Text = Convert.ToString(resultadoBase1);
                            txtigv01.Text = Convert.ToString(ResultadoIgv1);
                            break;
                    //}

                }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void Calcular_Base_01()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT_Val)
                {
                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                    {
                        decimal ResultadoIgv1 = Math.Round(Convert.ToDecimal(txtbase01.Text) * Tasa, 2);
                        txtigv01.Text = Convert.ToString(ResultadoIgv1);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        void Calcular_Base_02()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT_Val)
                {
                  if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                    {
                        decimal ResultadoIgv2 = Math.Round(Convert.ToDecimal(txtbase02.Text) * Tasa, 2);
                        txtigv02.Text = Convert.ToString(ResultadoIgv2);
                     
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        void Calcular_Base_03()
        {
            try
            {
                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT_Val)
                {
                    if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                    {
                        decimal ResultadoIgv3= Math.Round(Convert.ToDecimal(txtbase03.Text) * Tasa, 2);
                        txtigv03.Text = Convert.ToString(ResultadoIgv3);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    
        void Calcular_Importe_Total()
        {
            decimal Total = Convert.ToDecimal(txtbase01.Text) + Convert.ToDecimal(txtigv01.Text) + Convert.ToDecimal(txtbase02.Text) + Convert.ToDecimal(txtigv02.Text) + Convert.ToDecimal(txtbase03.Text) + Convert.ToDecimal(txtigv03.Text)+Convert.ToDecimal(txtisc.Text)+Convert.ToDecimal(txtvaadgrab.Text)+Convert.ToDecimal(txtotc.Text);
            txttotal.Text = Convert.ToString(Total);

        }

   
    

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
     
            try
            {

                txtbase01.Text = "0.00";
                txtigv01.Text = "0.00";
                txtbase02.Text = "0.00";
                txtigv02.Text = "0.00";
                txtbase03.Text = "0.00";
                txtigv03.Text = "0.00";
                txtisc.Text = "0.00";
                txtvaadgrab.Text = "0.00";
                txtotc.Text = "0.00";
                txttotal.Text = "0.00";

            }
            catch(Exception ex)
            {

            }
        }

        private void txtbase02_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtbase02.Text.Trim()))
            {
                try
                {
                    Calcular_Base_02();
                    Calcular_Importe_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void txtbase03_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtbase03.Text.Trim()))
            {
                try
                {
                    Calcular_Base_03();
                    Calcular_Importe_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void txtisc_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtisc.Text.Trim()))
            {
                try
                {
                    Calcular_Importe_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void txtvaadgrab_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtvaadgrab.Text.Trim()))
            {
                try
                {
                    Calcular_Importe_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void txtotc_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtotc.Text.Trim()))
            {
                try
                {
                    Calcular_Importe_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void txttotal_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txttotal.Text.Trim()))
            {
                try
                {
                    //Calcular_Cuando_son_Base();
                    //Calcular_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void txtbase02_KeyDown(object sender, KeyEventArgs e)
        {

            //if (!string.IsNullOrEmpty(txtbase02.Text.Trim()))
            //{
            //    try
            //    {
            //        decimal base02 = Convert.ToDecimal(txtbase02.Text);
            //        string b;
            //        //txtbase02.Text= String.Format("{0:0,0.00}", txtbase02.Text);
            //        b= String.Format("{0:0,0.00}", base02);
            //        txtbase02.Text = b;

            //    }
            //    catch (Exception ex)
            //    {
            //        Accion.ErrorSistema(ex.Message);
            //    }

            //}
        }

        private void txtbase01_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtbase01.Text.Trim()))
            {
                try
                {
                    Calcular_Base_01();
                    Calcular_Importe_Total();
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

 
    }
}
