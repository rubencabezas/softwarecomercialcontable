﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras
{
    public partial class frm_orden_sc_edicion : Contable.frm_fuente
    {
        public frm_orden_sc_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo,Id_Movimiento,Id_Tipo_Orden;
        List<Entidad_Orden_C_S> Detalles_Requerimiento = new List<Entidad_Orden_C_S>();
        void ActualizaIGV()
        {
            try
            {
           if ((IsDate(txtfechadoc.Text) == true))
            {
                if ((DateTime.Parse(txtfechadoc.Text).Year > 2000))
                {
                    Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                    Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                    ent.Imd_Fecha_Inicio = Convert.ToDateTime(txtfechadoc.Text);

                    List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                    Lista = log.Igv_Actual(ent);
                    if ((Lista.Count > 0))
                    {
                        txtigv.Tag = Lista[0].Imd_Tasa;
                        txtigv.Text =  Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100,2));
                        //String.Format("{0:yyyy-MM-dd}", Enti.Ord_Fecha)
                        //string igv = string.Format("{0:0.##}", txtigv.Text).ToString();
                        //txtigv.Text = igv;
                    }

                }

            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                LimpiarDet();
                Detalles.Clear();
                dgvdatos.DataSource = null;
                BloquearDetalles();
                ResetearImportes();
                
                txtordencod.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        int Ord_Item;
        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                
                Ord_Item = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }

        public void BloquearDetalles()
        {
         
            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtalmacencod.Enabled = false;
            txtdescripcion.Enabled = false;
            txtpreciounitario.Enabled = false;
            txtcantidad.Enabled = false;

        }

        public void HabilitarDetalles()
        {

            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtalmacencod.Enabled = true;
            txtdescripcion.Enabled = true;
            txtpreciounitario.Enabled = true;
            txtcantidad.Enabled = true;

        }
        public void LimpiarCab()
        {
            
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();
          
            txtfechadoc.ResetText();
            txtproveedor.ResetText();
            txtproveedordesc.ResetText();
            txtresponsable.ResetText();
            txtresponsabledesc.ResetText();

            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();

            txtigv.ResetText();
            txtsubtotal.ResetText();
            txtimporteigv.ResetText();
            txtobservacion.ResetText();
            txttotal.ResetText();

        }

        public void LimpiarDet()
        {
          
            txtdescripcion.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtalmacencod.ResetText();
            txtalmacendesc.ResetText();
            txtpreciounitario.ResetText();
            txtcantidad.ResetText();
            //Detalles.Clear();

        }

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 3);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_Comprobante = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_Comprobante).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void txtserie_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtserie.Text))
                {
                    string Serie_Numero = txtserie.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtserie.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtnumero.Text))
            //{
            //    txtnumero.Text = Accion.Formato(txtnumero.Text, 8);
            //}
        }

        private void txtproveedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproveedor.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproveedor.Text.Substring(txtproveedor.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtproveedor.Text = Entidad.Ent_RUC_DNI;
                                txtproveedordesc.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproveedor.Text) & string.IsNullOrEmpty(txtproveedordesc.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtproveedor.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtproveedor.Text.Trim().ToUpper())
                        {
                            txtproveedor.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtproveedordesc.Text = T.Ent_Ape_Materno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproveedor.EnterMoveNextControl = false;
                    txtproveedordesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtresponsable_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtresponsable.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtresponsable.Text.Substring(txtresponsable.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtresponsable.Text = Entidad.Ent_RUC_DNI;
                                txtresponsabledesc.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtresponsable.Text) & string.IsNullOrEmpty(txtresponsabledesc.Text))
                    {
                        BuscarEntidaResponsabled();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidaResponsabled()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtresponsable.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtresponsable.Text.Trim().ToUpper())
                        {
                            txtresponsable.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtresponsabledesc.Text = T.Ent_Ape_Materno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtresponsable.EnterMoveNextControl = false;
                    txtresponsable.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

  void verificarContado()
        {
            if (txtcondicioncod.Text == "02")
            {
                txtdias.Enabled = true;

            }
            else
            {
                txtdias.Enabled = false;
                txtdias.Text = Convert.ToString(0);
                txtfechavencimiento.Text = "01/01/1900";
            }
        }
 private void txtcondicioncod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcondicioncod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcondicioncod.Text.Substring(txtcondicioncod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0003";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcondicioncod.Tag = Entidad.Id_General_Det;
                                txtcondicioncod.Text = Entidad.Gen_Codigo_Interno;
                                txtcondiciondesc.Text = Entidad.Gen_Descripcion_Det;
                                verificarContado();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcondicioncod.Text) & string.IsNullOrEmpty(txtcondiciondesc.Text))
                    {
                        BuscarCondicion();
                        verificarContado();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarCondicion()
        {
            try
            {
                txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = txtcondicioncod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtcondicioncod.Text.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool Es_moneda_nac;
        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Text = Entidad.Id_Moneda;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                                VerificarMoneda();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }
        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 3);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Moneda = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Moneda).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocod.Text))
                    {
                        txttipocambiocod.Enabled = true;
                        txttipocambiocod.ResetText();
                        txttipocambiodesc.ResetText();
                        txttipocambiovalor.ResetText();
                    }

                }
            }
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)


        {
            if (String.IsNullOrEmpty(txttipocambiocod.Text) == false)
            {
                if (IsDate(txtfechadoc.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocod.Text.Substring(txttipocambiocod.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadoc.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocod.Text = Entidad.Codigo;
                                    txttipocambiodesc.Text = Entidad.Nombre;
                                    txttipocambiovalor.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocod.Text == "OTR")
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalor.Enabled = false;
                                    }

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocod.Text) & string.IsNullOrEmpty(txttipocambiodesc.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadoc.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocod.Text = T.Codigo;
                                            txttipocambiodesc.Text = T.Nombre;
                                            txttipocambiovalor.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocod.Text == "OTR")
                                            {
                                                txttipocambiovalor.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalor.Enabled = false;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadoc.Focus();

                }

            }


        }


        public void ResetearImportes()
        {
        txtsubtotal.Text = "0.00";
        txtimporteigv.Text = "0.00";
        txttotal.Text = "0.00";
        txtigv.Text = "0.00";
        }

        private void frm_orden_sc_edicion_Load(object sender, EventArgs e)
        {
          
            BloquearDetalles();

            ResetearImportes();

            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();
            }
        }

        public List<Entidad_Orden_C_S> Lista_Modificar = new List<Entidad_Orden_C_S>();
        public void ListarModificar()
        {
            Entidad_Orden_C_S Ent = new Entidad_Orden_C_S();
            Logica_Orden_C_S log = new Logica_Orden_C_S();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Movimiento = Id_Movimiento;
            Ent.Id_Tipo_Orden = Id_Tipo_Orden;
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Orden_C_S Enti = new Entidad_Orden_C_S();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Movimiento = Enti.Id_Movimiento;
                    Id_Tipo_Orden= Enti.Id_Tipo_Orden;
                    txtordencod.Tag = Enti.Id_Tipo_Orden;
                    txtordencod.Text = Enti.Id_Tipo_Orden_det;
                    txtordendesc.Text = Enti.Id_Tipo_Orden_des;

                 
                    txttipodoc.Text = Enti.Ord_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Ord_Tipo_Doc_desc;

                    txtserie.Text = Enti.Ord_Serie + "-" +Enti.Ord_Numero;
                    //txtnumero.Text = Enti.Ord_Numero;


        
                    
                    txtproveedor.Text = Enti.Ord_Proveedor_Ruc_dni;
                    txtproveedordesc.Text = Enti.Ord_Proveedor_Ruc_dni_desc;
                    txtresponsable.Text = Enti.Ord_Responsable_Ruc_dni;
                    txtresponsabledesc.Text = Enti.Ord_Responsable_Ruc_dni_desc;
                    txtobservacion.Text = Enti.Ord_Observacion;
                    txtcondicioncod.Tag = Enti.Ord_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ord_Condicion_det;
                    txtcondiciondesc.Text = Enti.Ord_Condicion_desc;
                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ord_Fecha);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));

                    txtdias.Text =Convert.ToString(Enti.Ord_Dias);
                    txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ord_Fecha_Venci);

                    txtmonedacod.Text = Enti.Ord_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ord_Moneda_desc;

                    txttipocambiocod.Text = Enti.Ord_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ord_Tipo_Cambio_Desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ord_Tipo_Cambio_Valor).ToString();

                    txtsubtotal.Text = Convert.ToDecimal(Enti.Ord_Sub_Total).ToString("0.00");
                    txtimporteigv.Text = Convert.ToDecimal(Enti.Ord_Igv).ToString("0.00");
                    txttotal.Text = Convert.ToDecimal(Enti.Ord_Importe_Total).ToString("0.00");
                    

                    Logica_Orden_C_S log_det = new Logica_Orden_C_S();
                    dgvdatos.DataSource = null;
                    Detalles = log_det.Listar_Det(Enti);

                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                    }

                    Logica_Orden_C_S log_req = new Logica_Orden_C_S();
                 
                    Detalles_Requerimiento = log_det.Listar_Req(Enti);

   

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }
        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Orden_C_S Ent = new Entidad_Orden_C_S();
                    Logica_Orden_C_S Log = new Logica_Orden_C_S();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
                    Ent.Id_Tipo_Orden = txtordencod.Tag.ToString();
                    Ent.Id_Movimiento = Id_Movimiento;
                    
                    Ent.Ord_Tipo_Doc = txttipodoc.Text;

                    //Ent.Ord_Serie = txtserie.Text;
                    //Ent.Ord_Numero = txtnumero.Text;

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Ord_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Ord_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    Ent.Ord_Proveedor_Ruc_dni = txtproveedor.Text;
                    Ent.Ord_Responsable_Ruc_dni = txtresponsable.Text.ToString();
                    Ent.Ord_Observacion = txtobservacion.Text;

                    Ent.Ord_Condicion_Cod = txtcondicioncod.Tag.ToString();
                    Ent.Ord_Fecha = Convert.ToDateTime(txtfechadoc.Text);

 //                   if (Ent.Ord_Dias == "")
 //                   {

 //                   }
 //                   else
 //                   {
 //Ent.Ord_Dias = Convert.ToInt32(txtdias.Text);
 //                   }
                    Ent.Ord_Dias =Convert.ToInt32( IIf(Ent.Ord_Dias == 0, 0, Convert.ToInt32(txtdias.Text))); 

                    Ent.Ord_Fecha_Venci = Convert.ToDateTime(txtfechavencimiento.Text);

                    Ent.Ord_Moneda_Cod = txtmonedacod.Text.ToString();
                       
                    Ent.Ord_Tipo_Cambio_Cod = txttipocambiocod.Text;

                    Ent.Ord_Tipo_Cambio_Desc = txttipocambiodesc.Text;
                    Ent.Ord_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                    Ent.Ord_Sub_Total =Convert.ToDecimal(txtsubtotal.Text);
                    Ent.Ord_Igv = Convert.ToDecimal(txtimporteigv.Text);
                    Ent.Ord_Importe_Total = Convert.ToDecimal(txttotal.Text);

                    Ent.Ord_Igv_Tasa = Convert.ToDecimal(txtigv.Text);

                    Ent.Detalle = Detalles;
                    Ent.Detalle_Requerimiento = Detalles_Requerimiento;


                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {
                     
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();
                            Detalles.Clear();
                            Detalles_Requerimiento.Clear();
                            Detalles_Req.Clear();
                            dgvdatos.DataSource = null;
                            BloquearDetalles();
                            txtordencod.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtordendesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de orden");
                txtordendesc.Focus();
                return false;
            }


            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }
            //if (string.IsNullOrEmpty(txtnumero.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar un numero");
            //    txtnumero.Focus();
            //    return false;
            //}
            if (string.IsNullOrEmpty(txtproveedordesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtproveedor.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtresponsabledesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un responsable");
                txtresponsable.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcondiciondesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una condicion");
                txtcondicioncod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipocambiodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de cambio");
                txttipocambiocod.Focus();
                return false;
            }

            return true;
        }

        private void txtordencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtordencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtordencod.Text.Substring(txtordencod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0014";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtordencod.Tag = Entidad.Id_General_Det;
                                txtordencod.Text = Entidad.Gen_Codigo_Interno;
                                txtordendesc.Text = Entidad.Gen_Descripcion_Det;
                                BuscarTipoDocumentoOrden_C_S();

                                txtordencod.EnterMoveNextControl = true;
                                //txtserie.Select();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtordencod.Text) & string.IsNullOrEmpty(txtordendesc.Text))
                    {
                        BuscarTipo();
                        BuscarTipoDocumentoOrden_C_S();
                        txtordencod.EnterMoveNextControl = true;
                        //txtserie.Select();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarTipoDocumentoOrden_C_S()
        {
            try
            {
                //0014    0047    01  ORDEN DE COMPRA
                //0014    0048    02  ORDE DE SERVICIO
                string Id_Doc_tipo;
        if (txtordencod.Tag.ToString() == "0047")
                {
                    Id_Doc_tipo = "OC";
                }
                else
                {
                    Id_Doc_tipo = "OS";
                }

                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = Id_Doc_tipo
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == Id_Doc_tipo.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.Enabled = false;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        public void BuscarTipo()
        {
            try
            {
                txtordencod.Text = Accion.Formato(txtordencod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0014",
                    Gen_Codigo_Interno = txtordencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtordencod.Text.ToString().Trim().ToUpper())
                        {
                            txtordencod.Tag = T.Id_General_Det;
                            txtordencod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtordendesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtordencod.EnterMoveNextControl = false;
                    txtordencod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void TraerTipoDoc()
        {

        }

        //void VerifyCheckIGV()
        //{
        //    if (IsNumeric(txtProductoPrecioUnitario.Text))
        //    {
        //        if ((chkafectoigv.Checked == false))
        //        {
        //            txtProductoPrecioUnitario.Text = Format((txtProductoPrecioUnitario.Text / (1 + TxtResumenIGVPorc.Tag)), "##0.0000");
        //            txtProductoPrecioUnitario.Enabled = false;
        //        }
        //        else
        //        {
        //            txtProductoPrecioUnitario.Text = Format((txtProductoPrecioUnitario.Text * (1 + TxtResumenIGVPorc.Tag)), "##0.0000");
        //            txtProductoPrecioUnitario.Enabled = true;
        //        }

        //    }

        //}
        private void chkafectoigv_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_catalogo_busqueda f = new frm_catalogo_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {

                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtalmacencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacencod.Text.Substring(txtalmacencod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacencod.Text = Entidad.Id_Almacen;
                                txtalmacendesc.Text = Entidad.Alm_Descrcipcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacencod.Text) & string.IsNullOrEmpty(txtalmacendesc.Text))
                    {
                        BuscarAlmacen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarAlmacen()
        {
            try
            {
                txtalmacencod.Text = Accion.Formato(txtalmacencod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen = txtalmacencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacencod.Text.Trim().ToUpper())
                        {
                            txtalmacencod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {

                if (VerificarDetalle())
                {
                    Entidad_Orden_C_S ItemDetalle = new Entidad_Orden_C_S();

                    ItemDetalle.Ord_Item = Ord_Item;


                    if (ItemDetalle.Ord_Tipo_BSA == null)
                    {
                        ItemDetalle.Ord_Tipo_BSA = "";
                    }
                    else
                    {
                        ItemDetalle.Ord_Tipo_BSA = txttipobsacod.Tag.ToString();
                    }

                    ItemDetalle.Ord_Tipo_BSA = txttipobsacod.Tag.ToString();
                    ItemDetalle.Ord_Tipo_BSA_Interno = txttipobsacod.Text;
                    ItemDetalle.Ord_Tipo_BSA_Desc = txttipobsadesc.Text;

                    ItemDetalle.Ord_Catalogo = txtproductocod.Text.Trim();
                    ItemDetalle.Ord_Catalogo_desc = txtproductodesc.Text;

                    ItemDetalle.Ord_Descripcion = txtdescripcion.Text;


                    ItemDetalle.Ord_Almacen = txtalmacencod.Text;
                    ItemDetalle.Ord_Almacen_desc = txtalmacendesc.Text;

                    ItemDetalle.Ord_Cantidad = Convert.ToDecimal(txtcantidad.Text);
                    ItemDetalle.Ord_Precio_Unitario = Convert.ToDecimal(txtpreciounitario.Text);


                  
                    ItemDetalle.Ord_Sub_Total_item = Math.Round(ItemDetalle.Ord_Cantidad * ItemDetalle.Ord_Precio_Unitario, 2);
                    ItemDetalle.Ord_Igv_item = Math.Round(ItemDetalle.Ord_Cantidad * ItemDetalle.Ord_Precio_Unitario * Convert.ToDecimal(txtigv.Tag), 2);
                    ItemDetalle.Ord_Importe_Total_item = Math.Round(ItemDetalle.Ord_Cantidad * ItemDetalle.Ord_Precio_Unitario * (1+Convert.ToDecimal(txtigv.Tag)), 2);

                    if (EstadoDetalle == Estados.Nuevo)
                    {
                        Detalles.Add(ItemDetalle);
          

                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;
                    }
                    else if (EstadoDetalle == Estados.Modificar)
                    {

                        Detalles[Ord_Item - 1] = ItemDetalle;
             
                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;

                    }
                    CalculateSummary();
                    btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public bool VerificarDetalle()
        {
            if (string.IsNullOrEmpty(txttipobsadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar un tipo");
                txttipobsacod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtproductodesc.Text))
            {
                Accion.Advertencia("Debe ingresar un producto");
                txtproductocod.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtalmacendesc.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un almacen");
                txtalmacencod.Focus();
                return false;
            }

            if (Convert.ToDouble(txtcantidad.Text) <= 0)
            {
                //tools.ShowWarnig("VALOR INCORRECTO EN CANTIDAD", "Debe de ingresar un valor numérico mayor a CERO en el campo cantidad antes de grabar");
                Accion.Advertencia("Debe ingresar una cantidad");
                txtcantidad.Focus();
                return false;
            }
       if (Convert.ToDouble(txtpreciounitario.Text) <= 0)
            {
                //tools.ShowWarnig("VALOR INCORRECTO EN CANTIDAD", "Debe de ingresar un valor numérico mayor a CERO en el campo cantidad antes de grabar");
                Accion.Advertencia("Debe ingresar una precio unitario");
                txtpreciounitario.Focus();
                return false;
            }


            return true;
        }
        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdatos.DataSource = Detalles;
            }
        }
        void CalculateSummary()
        {
            decimal SubTotal=0;
            decimal ImporteIGV=0;
            decimal Total=0;

            foreach (Entidad_Orden_C_S Item in Detalles)
            {
                SubTotal = (SubTotal + Item.Ord_Sub_Total_item);
                ImporteIGV = (ImporteIGV + Item.Ord_Igv_item);
                Total = (Total + (Item.Ord_Sub_Total_item + Item.Ord_Igv_item));
            }

            txtsubtotal.Text = Convert.ToDecimal(SubTotal).ToString("0.00");
            txtimporteigv.Text = Convert.ToDecimal(ImporteIGV).ToString("0.00");
            txttotal.Text =Convert.ToDecimal(Total).ToString("0.00");
            
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    

                    Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                    CalculateSummary();
                    dgvdatos.DataSource = null;
                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Orden_C_S Det in Detalles)
                {
                    Det.Ord_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtfechadoc_Leave(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txtfechadoc_TextChanged(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txtordencod_TextChanged(object sender, EventArgs e)
        {
            if (txtordencod.Focus() == false)
            {
                txtordendesc.ResetText();
            }
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txtproveedor_TextChanged(object sender, EventArgs e)
        {
            if (txtproveedor.Focus() == false)
            {
                txtproveedordesc.ResetText();
            }
        }

        private void txtresponsable_TextChanged(object sender, EventArgs e)
        {
            if (txtresponsable.Focus() == false)
            {
                txtresponsabledesc.Reset();
            }
        }

        private void txtcondicioncod_TextChanged(object sender, EventArgs e)
        {
            if (txtcondicioncod.Focus() == false)
            {
                txtcondiciondesc.ResetText();
            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacod.Focus() == false)
            {
                txtmonedadesc.ResetText();
                txttipocambiocod.ResetText();
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
                txtmonedacod.Focus();
            }
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocod.Focus() == false)
            {
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
            }
        }

        private void txttipobsacod_TextChanged(object sender, EventArgs e)
        {
            if (txttipobsacod.Focus() == false)
            {
                txttipobsadesc.ResetText();
            }
        }

        private void txtproductocod_TextChanged(object sender, EventArgs e)
        {
            if (txtproductocod.Focus() == false)
            {
                txtproductodesc.ResetText();
            }
        }

        private void txtalmacencod_TextChanged(object sender, EventArgs e)
        {
            if (txtalmacencod.Focus() == false)
            {
                txtalmacendesc.Reset();
            }
        }

        private void txtdias_TextChanged(object sender, EventArgs e)
        {
            if ((IsDate(txtfechadoc.Text)  && (txtdias.Text != "")))
            {
                DateTime fechacancel;
               fechacancel =Convert.ToDateTime(txtfechadoc.Text);
               double dias;
               dias = Convert.ToDouble(txtdias.Text);
                DateTime Fechfin;
                Fechfin = Convert.ToDateTime(fechacancel.AddDays(dias));

               // txtfechavencimiento.Text =String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Fechfin).ToString());
                txtfechavencimiento.Text =String.Format("{0:yyyy-MM-dd}",Fechfin);

            }
        }

        bool ExisteOrden(Entidad_Orden_C_S op)
        {
            foreach (Entidad_Orden_C_S Ent in Detalles_Requerimiento)
            {
                if (((op.Doc_Periodo == Ent.Doc_Periodo) && ((op.Doc_Folio == Ent.Doc_Folio) )))
                {
                    return true;
                }

            }

            return false;
        }

        List<Entidad_Requerimiento> Detalles_Req = new List<Entidad_Requerimiento>();
        private void btnbuscar_Click(object sender, EventArgs e)
        {

            try
            {
                if ((txtordendesc.Text == ""))
                {
                    Accion.Advertencia("Debe seleccionar un tipo de orden");
                    txtordencod.Focus();
                }
                else
                {
                    using (frm_requerimientos_buscar f = new frm_requerimientos_buscar())
                    {
                        if (f.ShowDialog() == DialogResult.OK)
                        {
                            foreach (int Item in f.gridView1.GetSelectedRows())
                            {

                                Entidad_Requerimiento Ord = new Entidad_Requerimiento();
                                Entidad_Orden_C_S Compra_Ord2 = new Entidad_Orden_C_S();
                                Ord = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Compra_Ord2.Id_Empresa = Ord.Id_Empresa;
                                Compra_Ord2.Doc_Anio = Ord.Id_Anio;
                                Compra_Ord2.Doc_Periodo = Ord.Id_Periodo;
                                Compra_Ord2.Doc_Proceso = "";
                                Compra_Ord2.Doc_Folio = Ord.Id_Movimiento;
                                Compra_Ord2.Doc_Item = Detalles_Requerimiento.Count + 1;

                                if (ExisteOrden(Compra_Ord2) == false)
                                {
                                Detalles_Requerimiento.Add(Compra_Ord2);
                                //BUSCAMOS LOS DETALLES


                                Entidad_Requerimiento Det = new Entidad_Requerimiento();
                                    Det.Id_Empresa = Compra_Ord2.Id_Empresa;
                                    Det.Id_Anio = Compra_Ord2.Doc_Anio;
                                    Det.Id_Periodo = Compra_Ord2.Doc_Periodo;
                                    Det.Id_Movimiento = Compra_Ord2.Doc_Folio;

                                    Logica_Requerimiento log = new Logica_Requerimiento();

                                    Detalles_Req = log.Listar_Det(Det);

                                    foreach (Entidad_Requerimiento T in Detalles_Req)
                                    {
                                        Entidad_Orden_C_S Enti = new Entidad_Orden_C_S();


                                        Enti.Ord_Item = Detalles.Count + 1;


                                        Enti.Ord_Tipo_BSA = T.Req_Tipo_BSA;
                                        Enti.Ord_Tipo_BSA_Interno = T.Req_Tipo_BSA_Det;
                                        Enti.Ord_Tipo_BSA_Desc = T.Req_Tipo_BSA_Descripcion;

                                        Enti.Ord_Catalogo = T.Req_Catalogo;
                                        Enti.Ord_Catalogo_desc = T.Req_Catalogo_Desc;

                                        Enti.Ord_Descripcion = T.Req_Detalle;

                                        Enti.Ord_Almacen = "";
                                        Enti.Ord_Almacen_desc = "";

                                        Enti.Ord_Cantidad = T.Req_Cantidad;
                                        Enti.Ord_Precio_Unitario = 0;

                            

                                        Detalles.Add(Enti);

                                    }

                                }
                        }



                        if (Detalles.Count > 0)
                            {
                                dgvdatos.DataSource = null;
                                dgvdatos.DataSource = Detalles;

               
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnlistar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles_Requerimiento.Count > 0)
                {
                    using (frm_requerimiento_listar_asoc f = new frm_requerimiento_listar_asoc())
                    {

                        f.buscar2(Detalles_Requerimiento);
                        if (f.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("Noe existe ninguna orden");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Orden_C_S Entidad = new Entidad_Orden_C_S();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    Ord_Item= Entidad.Ord_Item;

     

                    txttipobsacod.Tag = Entidad.Ord_Tipo_BSA;
                    txttipobsacod.Text = Entidad.Ord_Tipo_BSA_Interno;
                    txttipobsadesc.Text = Entidad.Ord_Tipo_BSA_Desc;

                    txtproductocod.Text = Entidad.Ord_Catalogo;
                    txtproductodesc.Text = Entidad.Ord_Catalogo_desc;

                    txtdescripcion.Text = Entidad.Ord_Descripcion;

                    txtalmacencod.Text = Entidad.Ord_Almacen;
                    txtalmacendesc.Text = Entidad.Ord_Almacen_desc;

                    txtcantidad.Text = Entidad.Ord_Cantidad.ToString();
                    txtpreciounitario.Text = Entidad.Ord_Precio_Unitario.ToString();

                     chkafectoigv.Checked = Entidad.Ord_Incluye_Igv;

                    

                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle = Estados.Consulta;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle = Estados.SoloLectura;


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        List<Entidad_Orden_C_S> Detalles = new List<Entidad_Orden_C_S>();

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Nuevo;

            HabilitarDetalles();
            txttipobsacod.Focus();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
