﻿using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras
{
    public partial class frm_requerimiento : Contable.frm_fuente
    {
        public frm_requerimiento()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_requerimiento_edicion f = new frm_requerimiento_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        //Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    //Listar();
                }

            }
        }

        private void frm_requerimiento_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Requerimiento> Lista = new List<Entidad_Requerimiento>();
        public void Listar()
        {
            Entidad_Requerimiento Ent = new Entidad_Requerimiento();
            Logica_Requerimiento log = new Logica_Requerimiento();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Movimiento = null;
       
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_requerimiento_edicion f = new frm_requerimiento_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Movimiento = Entidad.Id_Movimiento;
    

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        Entidad_Requerimiento Entidad = new Entidad_Requerimiento();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        List<Entidad_Requerimiento> Detalles = new List<Entidad_Requerimiento>();
        private void btnimprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (Lista.Count > 0)
            //{
            //    Estado = Estados.Ninguno;
            //    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

            //    Rpt_Imprimir_Requerimiento R = new Rpt_Imprimir_Requerimiento();
            //    R.lblarea.Text = Entidad.Rec_Id_Area_desc;
            //    R.lblcargo.Text = Entidad.Req_Id_Cargo_desc;
            //    R.lblencargado.Text = Entidad.Req_Encardado_Ruc_Dni_desc;
            //    R.lblsolicitante.Text = Entidad.Req_Solicitante_Ruc_dni_desc;
            //    R.lblservicio.Text = Entidad.Req_Descripcion;
               
            //    R.lblfechaenvio.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Req_Fecha_Envio);

            //    string[] HorasMinutosSegundosHoraInicio = Entidad.Asist_Hora_Envio.Split(':');
            //    string strHora = HorasMinutosSegundosHoraInicio[0] + ":" + HorasMinutosSegundosHoraInicio[1] + ":" + HorasMinutosSegundosHoraInicio[2].Substring(0, 3);
                

            //    R.lblhoraenvio.Text =strHora.ToString();
            //    R.lbloc.Text = Entidad.Req_O_C;
            //    R.lblproyecto.Text = Entidad.Req_Codigo_Proyecto;
            //    R.lblnumrequerimiento.Text = Entidad.Req_Serie + " - " + Entidad.Req_Numero; 


            //    Logica_Requerimiento log_det = new Logica_Requerimiento();
            
            //    Detalles = log_det.Listar_Det(Entidad);

        
            //      R.DataSource = Detalles;
            
            //    dynamic ribbonPreview = new PrintPreviewFormEx();
            //    R.CreateDocument();

            //    R.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            //    ribbonPreview.PrintingSystem = R.PrintingSystem;
            //    //ribbonPreview.MdiParent = frm_principal;
            //    ribbonPreview.Show();

            //}
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
