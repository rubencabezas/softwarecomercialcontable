﻿namespace Contable 
{
    partial class frm_compras_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtfechavencimiento = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txtanalisisdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtidanalisis = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkotrostri = new DevExpress.XtraEditors.CheckEdit();
            this.chkisc = new DevExpress.XtraEditors.CheckEdit();
            this.txtigvporcentaje = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtbaseimponible = new DevExpress.XtraEditors.TextEdit();
            this.txtigv = new DevExpress.XtraEditors.TextEdit();
            this.txtotrostribuimporte = new DevExpress.XtraEditors.TextEdit();
            this.txtimportetotal = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtimporteisc = new DevExpress.XtraEditors.TextEdit();
            this.txtvadquinograba = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.btnbuscardocref = new DevExpress.XtraEditors.SimpleButton();
            this.btnbuscarorden = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnvalores = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDMN = new System.Windows.Forms.TextBox();
            this.TxtHME = new System.Windows.Forms.TextBox();
            this.TxtHMN = new System.Windows.Forms.TextBox();
            this.TxtDME = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.txttipoopedesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoopecod = new DevExpress.XtraEditors.TextEdit();
            this.btnnuevodetcon = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardetcon = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardetcon = new DevExpress.XtraEditors.SimpleButton();
            this.btnanadirdetcon = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatosContable = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtimportecontable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.btnregimen = new DevExpress.XtraEditors.SimpleButton();
            this.btndetadm = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvadquinograba.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopedesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopecod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatosContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportecontable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1011, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 586);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1011, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 554);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1011, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 554);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtfechavencimiento);
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.txttipocambiovalor);
            this.groupControl1.Controls.Add(this.txtanalisisdesc);
            this.groupControl1.Controls.Add(this.txtidanalisis);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.labelControl33);
            this.groupControl1.Controls.Add(this.txtentidad);
            this.groupControl1.Controls.Add(this.txtcondiciondesc);
            this.groupControl1.Controls.Add(this.txttipocambiodesc);
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtrucdni);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.txtcondicioncod);
            this.groupControl1.Controls.Add(this.txttipocambiocod);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(710, 171);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // txtfechavencimiento
            // 
            this.txtfechavencimiento.EnterMoveNextControl = true;
            this.txtfechavencimiento.Location = new System.Drawing.Point(103, 95);
            this.txtfechavencimiento.MenuManager = this.barManager1;
            this.txtfechavencimiento.Name = "txtfechavencimiento";
            this.txtfechavencimiento.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechavencimiento.Properties.Mask.BeepOnError = true;
            this.txtfechavencimiento.Properties.Mask.EditMask = "d";
            this.txtfechavencimiento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechavencimiento.Properties.Mask.SaveLiteral = false;
            this.txtfechavencimiento.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechavencimiento.Properties.MaxLength = 10;
            this.txtfechavencimiento.Size = new System.Drawing.Size(108, 20);
            this.txtfechavencimiento.TabIndex = 14;
            this.txtfechavencimiento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfechavencimiento_KeyDown);
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(4, 99);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(93, 13);
            this.labelControl35.TabIndex = 13;
            this.labelControl35.Text = "Fecha Vencimiento:";
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(635, 96);
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Properties.Mask.EditMask = "n3";
            this.txttipocambiovalor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txttipocambiovalor.Size = new System.Drawing.Size(62, 20);
            this.txttipocambiovalor.TabIndex = 21;
            // 
            // txtanalisisdesc
            // 
            this.txtanalisisdesc.Enabled = false;
            this.txtanalisisdesc.EnterMoveNextControl = true;
            this.txtanalisisdesc.Location = new System.Drawing.Point(173, 140);
            this.txtanalisisdesc.MenuManager = this.barManager1;
            this.txtanalisisdesc.Name = "txtanalisisdesc";
            this.txtanalisisdesc.Size = new System.Drawing.Size(307, 20);
            this.txtanalisisdesc.TabIndex = 27;
            // 
            // txtidanalisis
            // 
            this.txtidanalisis.EnterMoveNextControl = true;
            this.txtidanalisis.Location = new System.Drawing.Point(127, 140);
            this.txtidanalisis.MenuManager = this.barManager1;
            this.txtidanalisis.Name = "txtidanalisis";
            this.txtidanalisis.Size = new System.Drawing.Size(44, 20);
            this.txtidanalisis.TabIndex = 26;
            this.txtidanalisis.TextChanged += new System.EventHandler(this.txtidanalisis_TextChanged);
            this.txtidanalisis.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtidanalisis_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(465, 97);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(21, 13);
            this.labelControl16.TabIndex = 18;
            this.labelControl16.Text = "T.C.";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(6, 144);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(117, 13);
            this.labelControl33.TabIndex = 25;
            this.labelControl33.Text = "Analisis de la Operacion:";
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(146, 118);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtentidad.Size = new System.Drawing.Size(550, 20);
            this.txtentidad.TabIndex = 24;
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(582, 140);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondiciondesc.Size = new System.Drawing.Size(115, 20);
            this.txtcondiciondesc.TabIndex = 30;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(526, 96);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 20;
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(299, 95);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedadesc.Size = new System.Drawing.Size(160, 20);
            this.txtmonedadesc.TabIndex = 17;
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(101, 73);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(192, 20);
            this.txttipodocdesc.TabIndex = 6;
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(341, 73);
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(198, 20);
            this.txtserie.TabIndex = 8;
            this.txtserie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtserie_KeyDown);
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(545, 76);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Fecha Doc:";
            // 
            // txtrucdni
            // 
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(63, 118);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 23;
            this.txtrucdni.TextChanged += new System.EventHandler(this.txtrucdni_TextChanged);
            this.txtrucdni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdni_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(299, 76);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(41, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Numero:";
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(608, 73);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(89, 20);
            this.txtfechadoc.TabIndex = 12;
            this.txtfechadoc.TextChanged += new System.EventHandler(this.txtfechadoc_TextChanged);
            this.txtfechadoc.Leave += new System.EventHandler(this.txtfechadoc_Leave);
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.Enabled = false;
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(543, 140);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(38, 20);
            this.txtcondicioncod.TabIndex = 29;
            this.txtcondicioncod.TextChanged += new System.EventHandler(this.txtcondicioncod_TextChanged);
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(490, 96);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 19;
            this.txttipocambiocod.TextChanged += new System.EventHandler(this.txttipocambiocod_TextChanged);
            this.txttipocambiocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocod_KeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(17, 121);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(37, 13);
            this.labelControl7.TabIndex = 22;
            this.labelControl7.Text = "R.U.C.:";
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(259, 95);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedacod.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacod.TabIndex = 16;
            this.txtmonedacod.TextChanged += new System.EventHandler(this.txtmonedacod_TextChanged);
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(486, 143);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "Condicion:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(213, 98);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "Moneda:";
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(61, 73);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 5;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(15, 76);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "T.Doc.:";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(61, 50);
            this.txtglosa.MenuManager = this.barManager1;
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(636, 20);
            this.txtglosa.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(21, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Glosa:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(61, 27);
            this.txtlibro.MenuManager = this.barManager1;
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlibro.Properties.Appearance.Options.UseFont = true;
            this.txtlibro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlibro.Size = new System.Drawing.Size(636, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(24, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkotrostri);
            this.groupBox1.Controls.Add(this.chkisc);
            this.groupBox1.Controls.Add(this.txtigvporcentaje);
            this.groupBox1.Controls.Add(this.labelControl23);
            this.groupBox1.Controls.Add(this.labelControl18);
            this.groupBox1.Controls.Add(this.txtbaseimponible);
            this.groupBox1.Controls.Add(this.txtigv);
            this.groupBox1.Controls.Add(this.txtotrostribuimporte);
            this.groupBox1.Controls.Add(this.txtimportetotal);
            this.groupBox1.Controls.Add(this.labelControl14);
            this.groupBox1.Controls.Add(this.txtimporteisc);
            this.groupBox1.Controls.Add(this.txtvadquinograba);
            this.groupBox1.Controls.Add(this.labelControl19);
            this.groupBox1.Controls.Add(this.labelControl17);
            this.groupBox1.Location = new System.Drawing.Point(713, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 149);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resumen";
            // 
            // chkotrostri
            // 
            this.chkotrostri.Enabled = false;
            this.chkotrostri.Location = new System.Drawing.Point(6, 104);
            this.chkotrostri.Name = "chkotrostri";
            this.chkotrostri.Properties.Caption = "Otros Tributos";
            this.chkotrostri.Size = new System.Drawing.Size(89, 20);
            this.chkotrostri.TabIndex = 2;
            // 
            // chkisc
            // 
            this.chkisc.Enabled = false;
            this.chkisc.Location = new System.Drawing.Point(6, 79);
            this.chkisc.MenuManager = this.barManager1;
            this.chkisc.Name = "chkisc";
            this.chkisc.Properties.Caption = "I.S.C";
            this.chkisc.Size = new System.Drawing.Size(50, 20);
            this.chkisc.TabIndex = 2;
            // 
            // txtigvporcentaje
            // 
            this.txtigvporcentaje.Enabled = false;
            this.txtigvporcentaje.Location = new System.Drawing.Point(24, 35);
            this.txtigvporcentaje.Name = "txtigvporcentaje";
            this.txtigvporcentaje.Size = new System.Drawing.Size(39, 20);
            this.txtigvporcentaje.TabIndex = 1;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(67, 39);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(11, 13);
            this.labelControl23.TabIndex = 0;
            this.labelControl23.Text = "%";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(2, 39);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(17, 13);
            this.labelControl18.TabIndex = 0;
            this.labelControl18.Text = "IGV";
            // 
            // txtbaseimponible
            // 
            this.txtbaseimponible.EditValue = "0.00";
            this.txtbaseimponible.Enabled = false;
            this.txtbaseimponible.Location = new System.Drawing.Point(97, 11);
            this.txtbaseimponible.Name = "txtbaseimponible";
            this.txtbaseimponible.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtbaseimponible.Properties.Appearance.Options.UseFont = true;
            this.txtbaseimponible.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtbaseimponible.Size = new System.Drawing.Size(74, 20);
            this.txtbaseimponible.TabIndex = 1;
            // 
            // txtigv
            // 
            this.txtigv.EditValue = "0.00";
            this.txtigv.Enabled = false;
            this.txtigv.Location = new System.Drawing.Point(97, 35);
            this.txtigv.Name = "txtigv";
            this.txtigv.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtigv.Properties.Appearance.Options.UseFont = true;
            this.txtigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtigv.Size = new System.Drawing.Size(74, 20);
            this.txtigv.TabIndex = 1;
            // 
            // txtotrostribuimporte
            // 
            this.txtotrostribuimporte.EditValue = "0.00";
            this.txtotrostribuimporte.Enabled = false;
            this.txtotrostribuimporte.Location = new System.Drawing.Point(97, 103);
            this.txtotrostribuimporte.Name = "txtotrostribuimporte";
            this.txtotrostribuimporte.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtotrostribuimporte.Properties.Appearance.Options.UseFont = true;
            this.txtotrostribuimporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtotrostribuimporte.Size = new System.Drawing.Size(74, 20);
            this.txtotrostribuimporte.TabIndex = 1;
            // 
            // txtimportetotal
            // 
            this.txtimportetotal.EditValue = "0.00";
            this.txtimportetotal.Enabled = false;
            this.txtimportetotal.Location = new System.Drawing.Point(97, 126);
            this.txtimportetotal.Name = "txtimportetotal";
            this.txtimportetotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimportetotal.Properties.Appearance.Options.UseFont = true;
            this.txtimportetotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimportetotal.Size = new System.Drawing.Size(74, 20);
            this.txtimportetotal.TabIndex = 3;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(6, 129);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(67, 13);
            this.labelControl14.TabIndex = 2;
            this.labelControl14.Text = "Importe total:";
            // 
            // txtimporteisc
            // 
            this.txtimporteisc.EditValue = "0.00";
            this.txtimporteisc.Enabled = false;
            this.txtimporteisc.Location = new System.Drawing.Point(97, 80);
            this.txtimporteisc.Name = "txtimporteisc";
            this.txtimporteisc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimporteisc.Properties.Appearance.Options.UseFont = true;
            this.txtimporteisc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimporteisc.Size = new System.Drawing.Size(74, 20);
            this.txtimporteisc.TabIndex = 1;
            // 
            // txtvadquinograba
            // 
            this.txtvadquinograba.EditValue = "0.00";
            this.txtvadquinograba.Enabled = false;
            this.txtvadquinograba.Location = new System.Drawing.Point(97, 58);
            this.txtvadquinograba.Name = "txtvadquinograba";
            this.txtvadquinograba.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtvadquinograba.Properties.Appearance.Options.UseFont = true;
            this.txtvadquinograba.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtvadquinograba.Size = new System.Drawing.Size(74, 20);
            this.txtvadquinograba.TabIndex = 1;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(6, 60);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(82, 13);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "V. Adq. No Grab.";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(6, 15);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(72, 13);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "Base Imponible";
            // 
            // btnbuscardocref
            // 
            this.btnbuscardocref.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbuscardocref.Appearance.Options.UseFont = true;
            this.btnbuscardocref.Location = new System.Drawing.Point(895, 69);
            this.btnbuscardocref.Name = "btnbuscardocref";
            this.btnbuscardocref.Size = new System.Drawing.Size(100, 37);
            this.btnbuscardocref.TabIndex = 46;
            this.btnbuscardocref.Text = "Vincular Doc. Ref.";
            this.btnbuscardocref.Click += new System.EventHandler(this.btnbuscardocref_Click);
            // 
            // btnbuscarorden
            // 
            this.btnbuscarorden.Location = new System.Drawing.Point(5, 15);
            this.btnbuscarorden.Name = "btnbuscarorden";
            this.btnbuscarorden.Size = new System.Drawing.Size(49, 24);
            this.btnbuscarorden.TabIndex = 47;
            this.btnbuscarorden.Text = "Buscar";
            this.btnbuscarorden.Click += new System.EventHandler(this.btnbuscarorden_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 34);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(712, 196);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(710, 171);
            this.xtraTabPage1.Text = "Datos generales";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.simpleButton3);
            this.groupBox2.Controls.Add(this.simpleButton1);
            this.groupBox2.Controls.Add(this.btnbuscarorden);
            this.groupBox2.Location = new System.Drawing.Point(715, 199);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(173, 44);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Orden Compra/Servicio";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(115, 15);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(49, 24);
            this.simpleButton3.TabIndex = 47;
            this.simpleButton3.Text = "Ver";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(60, 15);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(49, 24);
            this.simpleButton1.TabIndex = 47;
            this.simpleButton1.Text = "Quitar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.groupControl3);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1007, 321);
            this.xtraTabPage4.Text = "Contable";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.btnvalores);
            this.groupControl3.Controls.Add(this.TxtDMN);
            this.groupControl3.Controls.Add(this.TxtHME);
            this.groupControl3.Controls.Add(this.TxtHMN);
            this.groupControl3.Controls.Add(this.TxtDME);
            this.groupControl3.Controls.Add(this.Label2);
            this.groupControl3.Controls.Add(this.txttipoopedesc);
            this.groupControl3.Controls.Add(this.labelControl11);
            this.groupControl3.Controls.Add(this.txttipoopecod);
            this.groupControl3.Controls.Add(this.btnnuevodetcon);
            this.groupControl3.Controls.Add(this.btnquitardetcon);
            this.groupControl3.Controls.Add(this.btneditardetcon);
            this.groupControl3.Controls.Add(this.btnanadirdetcon);
            this.groupControl3.Controls.Add(this.dgvdatosContable);
            this.groupControl3.Controls.Add(this.txtimportecontable);
            this.groupControl3.Controls.Add(this.labelControl32);
            this.groupControl3.Controls.Add(this.txttipodesc);
            this.groupControl3.Controls.Add(this.labelControl13);
            this.groupControl3.Controls.Add(this.txttipo);
            this.groupControl3.Controls.Add(this.txtcuentadesc);
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.txtcuenta);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1007, 321);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "Detalles contables";
            // 
            // btnvalores
            // 
            this.btnvalores.ImageOptions.Image = global::Contable.Properties.Resources.Enter_Pin_26px;
            this.btnvalores.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.btnvalores.Location = new System.Drawing.Point(940, 22);
            this.btnvalores.Name = "btnvalores";
            this.btnvalores.Size = new System.Drawing.Size(58, 80);
            this.btnvalores.TabIndex = 72;
            this.btnvalores.Click += new System.EventHandler(this.btnvalores_Click);
            // 
            // TxtDMN
            // 
            this.TxtDMN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtDMN.BackColor = System.Drawing.Color.Khaki;
            this.TxtDMN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtDMN.Location = new System.Drawing.Point(349, 295);
            this.TxtDMN.Name = "TxtDMN";
            this.TxtDMN.ReadOnly = true;
            this.TxtDMN.Size = new System.Drawing.Size(122, 21);
            this.TxtDMN.TabIndex = 64;
            this.TxtDMN.TabStop = false;
            this.TxtDMN.Text = "0";
            this.TxtDMN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtHME
            // 
            this.TxtHME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtHME.BackColor = System.Drawing.Color.PapayaWhip;
            this.TxtHME.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHME.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtHME.Location = new System.Drawing.Point(675, 295);
            this.TxtHME.Name = "TxtHME";
            this.TxtHME.ReadOnly = true;
            this.TxtHME.Size = new System.Drawing.Size(88, 21);
            this.TxtHME.TabIndex = 63;
            this.TxtHME.TabStop = false;
            this.TxtHME.Text = "0";
            this.TxtHME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtHMN
            // 
            this.TxtHMN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtHMN.BackColor = System.Drawing.Color.Khaki;
            this.TxtHMN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtHMN.Location = new System.Drawing.Point(474, 295);
            this.TxtHMN.Name = "TxtHMN";
            this.TxtHMN.ReadOnly = true;
            this.TxtHMN.Size = new System.Drawing.Size(102, 21);
            this.TxtHMN.TabIndex = 62;
            this.TxtHMN.TabStop = false;
            this.TxtHMN.Text = "0";
            this.TxtHMN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtDME
            // 
            this.TxtDME.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TxtDME.BackColor = System.Drawing.Color.PapayaWhip;
            this.TxtDME.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDME.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TxtDME.Location = new System.Drawing.Point(578, 295);
            this.TxtDME.Name = "TxtDME";
            this.TxtDME.ReadOnly = true;
            this.TxtDME.Size = new System.Drawing.Size(95, 21);
            this.TxtDME.TabIndex = 61;
            this.TxtDME.TabStop = false;
            this.TxtDME.Text = "0";
            this.TxtDME.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Maroon;
            this.Label2.Location = new System.Drawing.Point(297, 296);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(49, 17);
            this.Label2.TabIndex = 65;
            this.Label2.Text = "Totales";
            // 
            // txttipoopedesc
            // 
            this.txttipoopedesc.Enabled = false;
            this.txttipoopedesc.EnterMoveNextControl = true;
            this.txttipoopedesc.Location = new System.Drawing.Point(108, 60);
            this.txttipoopedesc.Name = "txttipoopedesc";
            this.txttipoopedesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipoopedesc.Size = new System.Drawing.Size(219, 20);
            this.txttipoopedesc.TabIndex = 7;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(5, 63);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(53, 13);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Operacion:";
            // 
            // txttipoopecod
            // 
            this.txttipoopecod.EnterMoveNextControl = true;
            this.txttipoopecod.Location = new System.Drawing.Point(63, 60);
            this.txttipoopecod.Name = "txttipoopecod";
            this.txttipoopecod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipoopecod.Size = new System.Drawing.Size(44, 20);
            this.txttipoopecod.TabIndex = 6;
            this.txttipoopecod.TextChanged += new System.EventHandler(this.txttipoopecod_TextChanged);
            this.txttipoopecod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoopecod_KeyDown);
            // 
            // btnnuevodetcon
            // 
            this.btnnuevodetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevodetcon.Appearance.Options.UseFont = true;
            this.btnnuevodetcon.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.btnnuevodetcon.Location = new System.Drawing.Point(755, 23);
            this.btnnuevodetcon.Name = "btnnuevodetcon";
            this.btnnuevodetcon.Size = new System.Drawing.Size(87, 37);
            this.btnnuevodetcon.TabIndex = 1;
            this.btnnuevodetcon.Text = "Nuevo";
            this.btnnuevodetcon.Click += new System.EventHandler(this.btnnuevodetcon_Click);
            // 
            // btnquitardetcon
            // 
            this.btnquitardetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnquitardetcon.Appearance.Options.UseFont = true;
            this.btnquitardetcon.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.btnquitardetcon.Location = new System.Drawing.Point(848, 23);
            this.btnquitardetcon.Name = "btnquitardetcon";
            this.btnquitardetcon.Size = new System.Drawing.Size(87, 37);
            this.btnquitardetcon.TabIndex = 48;
            this.btnquitardetcon.Text = "Quitar";
            this.btnquitardetcon.Click += new System.EventHandler(this.btnquitardetcon_Click);
            // 
            // btneditardetcon
            // 
            this.btneditardetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneditardetcon.Appearance.Options.UseFont = true;
            this.btneditardetcon.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.btneditardetcon.Location = new System.Drawing.Point(755, 66);
            this.btneditardetcon.Name = "btneditardetcon";
            this.btneditardetcon.Size = new System.Drawing.Size(87, 37);
            this.btneditardetcon.TabIndex = 49;
            this.btneditardetcon.Text = "Editar";
            this.btneditardetcon.Click += new System.EventHandler(this.btneditardetcon_Click);
            // 
            // btnanadirdetcon
            // 
            this.btnanadirdetcon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnanadirdetcon.Appearance.Options.UseFont = true;
            this.btnanadirdetcon.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.btnanadirdetcon.Location = new System.Drawing.Point(848, 66);
            this.btnanadirdetcon.Name = "btnanadirdetcon";
            this.btnanadirdetcon.Size = new System.Drawing.Size(87, 37);
            this.btnanadirdetcon.TabIndex = 13;
            this.btnanadirdetcon.Text = "Añadir";
            this.btnanadirdetcon.Click += new System.EventHandler(this.btnanadirdetcon_Click);
            // 
            // dgvdatosContable
            // 
            this.dgvdatosContable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatosContable.Location = new System.Drawing.Point(4, 105);
            this.dgvdatosContable.MainView = this.gridView1;
            this.dgvdatosContable.MenuManager = this.barManager1;
            this.dgvdatosContable.Name = "dgvdatosContable";
            this.dgvdatosContable.Size = new System.Drawing.Size(1003, 188);
            this.dgvdatosContable.TabIndex = 46;
            this.dgvdatosContable.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn3});
            this.gridView1.GridControl = this.dgvdatosContable;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Cuenta";
            this.gridColumn1.FieldName = "Ctb_Cuenta";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 99;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Cuenta Descripcion";
            this.gridColumn2.FieldName = "Ctb_Cuenta_Desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 231;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Debe Nac.";
            this.gridColumn5.DisplayFormat.FormatString = "n2";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "Ctb_Importe_Debe";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe", "{0:n2}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 122;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.Caption = "Haber Nac.";
            this.gridColumn6.DisplayFormat.FormatString = "n2";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "Ctb_Importe_Haber";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 102;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.Caption = "Debe Extr.";
            this.gridColumn7.DisplayFormat.FormatString = "n2";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "Ctb_Importe_Debe_Extr";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Debe_Extr", "{0:n2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 95;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.Caption = "Haber Extr.";
            this.gridColumn8.DisplayFormat.FormatString = "n2";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "Ctb_Importe_Haber_Extr";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Ctb_Importe_Haber_Extr", "{0:n2}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            this.gridColumn8.Width = 88;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Cta_Destino";
            this.gridColumn3.FieldName = "Cta_Destino";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // txtimportecontable
            // 
            this.txtimportecontable.EnterMoveNextControl = true;
            this.txtimportecontable.Location = new System.Drawing.Point(636, 61);
            this.txtimportecontable.MenuManager = this.barManager1;
            this.txtimportecontable.Name = "txtimportecontable";
            this.txtimportecontable.Properties.Mask.EditMask = "n2";
            this.txtimportecontable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimportecontable.Size = new System.Drawing.Size(100, 20);
            this.txtimportecontable.TabIndex = 12;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(588, 64);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(42, 13);
            this.labelControl32.TabIndex = 11;
            this.labelControl32.Text = "Importe:";
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(417, 60);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodesc.Size = new System.Drawing.Size(164, 20);
            this.txttipodesc.TabIndex = 10;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(329, 62);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(24, 13);
            this.labelControl13.TabIndex = 8;
            this.labelControl13.Text = "Tipo:";
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(356, 60);
            this.txttipo.Name = "txttipo";
            this.txttipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipo.Size = new System.Drawing.Size(59, 20);
            this.txttipo.TabIndex = 9;
            this.txttipo.TextChanged += new System.EventHandler(this.txttipo_TextChanged_1);
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipo_KeyDown);
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(145, 39);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuentadesc.Size = new System.Drawing.Size(590, 20);
            this.txtcuentadesc.TabIndex = 4;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(19, 42);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(39, 13);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "Cuenta:";
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(63, 39);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuenta.Size = new System.Drawing.Size(81, 20);
            this.txtcuenta.TabIndex = 3;
            this.txtcuenta.EditValueChanged += new System.EventHandler(this.txtcuenta_EditValueChanged);
            this.txtcuenta.TextChanged += new System.EventHandler(this.txtcuenta_TextChanged_1);
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl2.Location = new System.Drawing.Point(2, 244);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage4;
            this.xtraTabControl2.Size = new System.Drawing.Size(1009, 346);
            this.xtraTabControl2.TabIndex = 1;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage4});
            // 
            // btnregimen
            // 
            this.btnregimen.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregimen.Appearance.Options.UseFont = true;
            this.btnregimen.ImageOptions.Image = global::Contable.Properties.Resources.Aplicado;
            this.btnregimen.ImageOptions.ImageToTextIndent = 20;
            this.btnregimen.Location = new System.Drawing.Point(895, 113);
            this.btnregimen.Name = "btnregimen";
            this.btnregimen.Size = new System.Drawing.Size(100, 37);
            this.btnregimen.TabIndex = 67;
            this.btnregimen.Text = "Regimen";
            this.btnregimen.Click += new System.EventHandler(this.btnregimen_Click);
            // 
            // btndetadm
            // 
            this.btndetadm.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndetadm.Appearance.Options.UseFont = true;
            this.btndetadm.ImageOptions.Image = global::Contable.Properties.Resources.Administrativo;
            this.btndetadm.Location = new System.Drawing.Point(895, 156);
            this.btndetadm.Name = "btndetadm";
            this.btndetadm.Size = new System.Drawing.Size(100, 37);
            this.btndetadm.TabIndex = 68;
            this.btndetadm.Text = "Det. Adm.";
            this.btndetadm.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // frm_compras_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 586);
            this.Controls.Add(this.btndetadm);
            this.Controls.Add(this.btnregimen);
            this.Controls.Add(this.btnbuscardocref);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.xtraTabControl2);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_compras_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compras";
            this.Load += new System.EventHandler(this.frm_compras_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvadquinograba.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopedesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopecod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatosContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportecontable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        public DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        public DevExpress.XtraEditors.TextEdit txtentidad;
        public DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        public DevExpress.XtraEditors.TextEdit txtmonedadesc;
        public DevExpress.XtraEditors.TextEdit txttipodocdesc;
        public DevExpress.XtraEditors.TextEdit txtserie;
        public DevExpress.XtraEditors.TextEdit txtrucdni;
        public DevExpress.XtraEditors.TextEdit txtcondicioncod;
        public DevExpress.XtraEditors.TextEdit txtmonedacod;
        public DevExpress.XtraEditors.TextEdit txttipodoc;
        public DevExpress.XtraEditors.TextEdit txtglosa;
        public DevExpress.XtraEditors.TextEdit txtimportetotal;
        public DevExpress.XtraEditors.TextEdit txtfechadoc;
        public DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        public DevExpress.XtraEditors.TextEdit txttipocambiocod;
        public DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        public DevExpress.XtraEditors.TextEdit txtigvporcentaje;
        public DevExpress.XtraEditors.TextEdit txtigv;
        public DevExpress.XtraEditors.CheckEdit chkisc;
        public DevExpress.XtraEditors.TextEdit txtimporteisc;
        public DevExpress.XtraEditors.TextEdit txtotrostribuimporte;
        public DevExpress.XtraEditors.TextEdit txtvadquinograba;
        public DevExpress.XtraEditors.GroupControl groupControl1;
        public System.Windows.Forms.GroupBox groupBox1;
        public DevExpress.XtraEditors.CheckEdit chkotrostri;
        public DevExpress.XtraEditors.TextEdit txtbaseimponible;
        private DevExpress.XtraEditors.SimpleButton btnbuscardocref;
        private DevExpress.XtraEditors.SimpleButton btnbuscarorden;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.TextEdit txtanalisisdesc;
        private DevExpress.XtraEditors.TextEdit txtidanalisis;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.TextEdit txttipoopedesc;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txttipoopecod;
        public DevExpress.XtraEditors.SimpleButton btnquitardetcon;
        public DevExpress.XtraEditors.SimpleButton btneditardetcon;
        public DevExpress.XtraEditors.SimpleButton btnanadirdetcon;
        private DevExpress.XtraGrid.GridControl dgvdatosContable;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txtimportecontable;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        public DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        public DevExpress.XtraEditors.TextEdit txttipo;
        public DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        public DevExpress.XtraEditors.TextEdit txtcuenta;
        private DevExpress.XtraEditors.TextEdit txtfechavencimiento;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.SimpleButton btndetadm;
        private DevExpress.XtraEditors.SimpleButton btnregimen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        internal System.Windows.Forms.TextBox TxtDMN;
        internal System.Windows.Forms.TextBox TxtHME;
        internal System.Windows.Forms.TextBox TxtHMN;
        internal System.Windows.Forms.TextBox TxtDME;
        internal System.Windows.Forms.Label Label2;
        private DevExpress.XtraEditors.SimpleButton btnvalores;
        public DevExpress.XtraEditors.SimpleButton btnnuevodetcon;
    }
}