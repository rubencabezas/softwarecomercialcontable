﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable 
{
    public partial class frm_orden_cs_lista :  frm_fuente
    {
        public frm_orden_cs_lista()
        {
            InitializeComponent();
        }
        public string Empresa;
        public string Anio;
        public string Tipo_orden ;
        public string Folio;
        public string Periodo;

        List<Entidad_Orden_C_S> Detalles_Orden_Compra = new List<Entidad_Orden_C_S>();
        public List<Entidad_Orden_C_S> Detalles_Orden_Compra_l = new List<Entidad_Orden_C_S>();
   public     void buscar2(List<Entidad_Movimiento_Cab> Ent)
        {
            try
            {
                dgvdatos.DataSource = null;
                foreach (Entidad_Movimiento_Cab x in Ent)
                {
                    Empresa = x.Id_Empresa;
                    Anio = x.Id_Anio_Ref;
                    Folio = x.Ord_Folio;
                    Tipo_orden = x.Ord_tipo;
                    Periodo = x.Id_Periodo_Ref;
              
                    Logica_Orden_C_S log = new Logica_Orden_C_S();
                    Entidad_Orden_C_S Bqda = new Entidad_Orden_C_S();
                    // With...
                    Bqda.Id_Empresa = Empresa;
                    Bqda.Id_Anio = Anio;
                    Bqda.Id_Periodo = Periodo;
                    Bqda.Id_Movimiento = Folio;
                    Bqda.Id_Tipo_Orden = Tipo_orden;


                    Detalles_Orden_Compra_l = log.Listar(Bqda);
                    Entidad_Orden_C_S Ent_orden = new Entidad_Orden_C_S();
                 
                    Ent_orden = Detalles_Orden_Compra_l[0];
                    Detalles_Orden_Compra.Add(Ent_orden);
                    if ((Detalles_Orden_Compra.Count > 0))
                    {
                        dgvdatos.DataSource = Detalles_Orden_Compra;
                        dgvdatos.Focus();
                    }
                    else
                    {
                        Accion.Advertencia ("No se encontro ningun documento");
                        this.Close();
                    }

                                   
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


    }

        private void frm_orden_cs_lista_Load(object sender, EventArgs e)
        {
           
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
