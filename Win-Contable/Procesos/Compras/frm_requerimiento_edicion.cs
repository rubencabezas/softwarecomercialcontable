﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras
{
    public partial class frm_requerimiento_edicion : Contable.frm_fuente
    {
        public frm_requerimiento_edicion()
        {
            InitializeComponent();
        }

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Movimiento;

        public string Estado_Ven_Boton;
        List<Entidad_Requerimiento> Detalles = new List<Entidad_Requerimiento>();
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                LimpiarDet();
                Detalles.Clear();
                dgvdatos.DataSource = null;
                BloquearDetalles();
           

                txtsolicitanteruc.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        int Id_Item;
        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();

                Id_Item = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }

        public void BloquearDetalles()
        {

            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtdetalle.Enabled = false;
            txtobservacion.Enabled = false;
            txtunidad.Enabled = false;
            txtcantidad.Enabled = false;

        }

        public void HabilitarDetalles()
        {

            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtdetalle.Enabled = true;
         
            txtobservacion.Enabled = true;
    
            txtcantidad.Enabled = true;
       

        }
        public void LimpiarCab()
        {
            txtsolicitanteruc.ResetText();
            txtsolicitantedesc.ResetText();
            txtarea.ResetText();
            txtareadesc.ResetText();
            txtcargo.ResetText();
            txtcargodesc.ResetText();
            txtencargadoruc.ResetText();
            txtencargadodesc.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();
      
            txtfechaenvio.ResetText();
            DtpHora.ResetText();
            txtoc.ResetText();
            txtdescripcion.ResetText();
            txtproyecto.ResetText();

        }

        public void LimpiarDet()
        {

            txtobservacion.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtdetalle.ResetText();
            txtunidad.ResetText();
           
            txtcantidad.ResetText();
            //Detalles.Clear();

        }
        private void frm_requerimiento_edicion_Load(object sender, EventArgs e)
        {
            BloquearDetalles();


            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();
            }
        }

        public List<Entidad_Requerimiento> Lista_Modificar = new List<Entidad_Requerimiento>();
        public void ListarModificar()
        {
            Entidad_Requerimiento Ent = new Entidad_Requerimiento();
            Logica_Requerimiento log = new Logica_Requerimiento();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Movimiento = Id_Movimiento;
      
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Requerimiento Enti = new Entidad_Requerimiento();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Movimiento = Enti.Id_Movimiento;
             
      
                    txtsolicitanteruc.Text = Enti.Req_Solicitante_Ruc_dni;
                    txtsolicitantedesc.Text = Enti.Req_Solicitante_Ruc_dni_desc;


                    txtarea.Text = Enti.Rec_Id_Area;
                    txtareadesc.Text = Enti.Rec_Id_Area_desc;


                    txtcargo.Text = Enti.Req_Id_Cargo;
                    txtcargodesc.Text = Enti.Req_Id_Cargo_desc;


                    txtencargadoruc.Text = Enti.Req_Encardado_Ruc_Dni;
                    txtencargadodesc.Text = Enti.Req_Encardado_Ruc_Dni_desc;

                    txtdescripcion.Text = Enti.Req_Descripcion;

                    txttipodoc.Text = Enti.Req_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Req_Tipo_Doc_desc;

                    txtserie.Text = Enti.Req_Serie +"-"+ Enti.Req_Numero;
                    //txtnumero.Text = Enti.Req_Numero;



                    txtfechaenvio.Text = String.Format("{0:yyyy-MM-dd}", Enti.Req_Fecha_Envio);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));

                    string[] HorasMinutosSegundosHoraInicio = Enti.Asist_Hora_Envio.Split(':');
                    string strHora = HorasMinutosSegundosHoraInicio[0] + ":" + HorasMinutosSegundosHoraInicio[1] + ":" + HorasMinutosSegundosHoraInicio[2].Substring(0, 3);
                

                    DtpHora.Value = Convert.ToDateTime(strHora);
                    txtoc.Text = Enti.Req_O_C;

                    txtproyecto.Text = Enti.Req_Codigo_Proyecto;


                    Logica_Requerimiento log_det = new Logica_Requerimiento();
                    dgvdatos.DataSource = null;
                    Detalles = log_det.Listar_Det(Enti);

                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                    }


                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtsolicitanteruc_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtsolicitanteruc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtsolicitanteruc.Text.Substring(txtsolicitanteruc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtsolicitanteruc.Text = Entidad.Ent_RUC_DNI;
                                txtsolicitantedesc.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtsolicitanteruc.Text) & string.IsNullOrEmpty(txtsolicitantedesc.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtsolicitanteruc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtsolicitanteruc.Text.Trim().ToUpper())
                        {
                            txtsolicitanteruc.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtsolicitantedesc.Text = T.Ent_Ape_Materno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtsolicitanteruc.EnterMoveNextControl = false;
                    txtsolicitantedesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtencargadoruc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtencargadoruc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtencargadoruc.Text.Substring(txtencargadoruc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtencargadoruc.Text = Entidad.Ent_RUC_DNI;
                                txtencargadodesc.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtencargadoruc.Text) & string.IsNullOrEmpty(txtencargadodesc.Text))
                    {
                        BuscarEntidaResponsabled();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidaResponsabled()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtencargadoruc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtencargadoruc.Text.Trim().ToUpper())
                        {
                            txtencargadoruc.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtencargadodesc.Text = T.Ent_Ape_Materno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtencargadoruc.EnterMoveNextControl = false;
                    txtencargadoruc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 3);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_Comprobante = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_Comprobante).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_catalogo_busqueda f = new frm_catalogo_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;
                                txtunidad.Text = Entidad.Und_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {

                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;
                            txtunidad.Text = T.Und_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtserie_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtserie.Text))
            {
                string Serie_Numero = txtserie.Text.Trim();

                if (Serie_Numero.Contains("-") == true)
                {
                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                    txtserie.EnterMoveNextControl = true;
                }
                else
                {
                    Accion.ErrorSistema("Se debe separar por un '-' para poder registrar la serie y numero");
                    //txtserie.EnterMoveNextControl = false;
                }

            }


        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtnumero.Text))
            //{
            //    txtnumero.Text = Accion.Formato(txtnumero.Text, 8);
            //}
        }

        private void txtcargo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcargo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcargo.Text.Substring(txtcargo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_cargo_busqueda f = new _1_Busquedas_Generales.frm_cargo_busqueda())
                        {
                            

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Cargo Entidad = new Entidad_Cargo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtcargo.Text = Entidad.Id_Cargo;
                                txtcargodesc.Text = Entidad.Car_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcargo.Text) & string.IsNullOrEmpty(txtcargodesc.Text))
                    {
                        BuscarCargo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCargo()
        {
            try
            {
                txtcargo.Text = Accion.Formato(txtcargo.Text, 2);

                Logica_Cargo log = new Logica_Cargo();

                List<Entidad_Cargo> Generales = new List<Entidad_Cargo>();
                Generales = log.Listar(new Entidad_Cargo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Cargo = txtcargo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Cargo T in Generales)
                    {
                        if ((T.Id_Cargo).ToString().Trim().ToUpper() == txtcargo.Text.Trim().ToUpper())
                        {

                            txtcargo.Text = (T.Id_Cargo).ToString().Trim();
                            txtcargodesc.Text = T.Car_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcargo.EnterMoveNextControl = false;
                    txtcargodesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtarea_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtarea.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtarea.Text.Substring(txtarea.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_area_busqueda f = new _1_Busquedas_Generales.frm_area_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Area Entidad = new Entidad_Area();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtarea.Text = Entidad.Id_Area;
                                txtareadesc.Text = Entidad.Are_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtarea.Text) & string.IsNullOrEmpty(txtareadesc.Text))
                    {
                        BuscarArea();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {

                if (VerificarDetalle())
                {
                    Entidad_Requerimiento ItemDetalle = new Entidad_Requerimiento();

                    ItemDetalle.Id_Item = Id_Item;


                    if (ItemDetalle.Req_Tipo_BSA == null)
                    {
                        ItemDetalle.Req_Tipo_BSA = "";
                    }
                    else
                    {
                        ItemDetalle.Req_Tipo_BSA = txttipobsacod.Tag.ToString();
                    }

                    ItemDetalle.Req_Tipo_BSA = txttipobsacod.Tag.ToString();
                    ItemDetalle.Req_Tipo_BSA_Det = txttipobsacod.Text;
                    ItemDetalle.Req_Tipo_BSA_Descripcion = txttipobsadesc.Text;

                    ItemDetalle.Req_Catalogo = txtproductocod.Text.Trim();
                    ItemDetalle.Req_Catalogo_Desc = txtproductodesc.Text;

                    ItemDetalle.Req_Detalle = txtdetalle.Text;


                    ItemDetalle.Req_Observacion = txtobservacion.Text;
                    //txtunidad.Text = ItemDetalle.;

                    ItemDetalle.Req_Cantidad =Convert.ToDecimal( txtcantidad.Text);

                    if (EstadoDetalle == Estados.Nuevo)
                    {
                        Detalles.Add(ItemDetalle);


                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;
                    }
                    else if (EstadoDetalle == Estados.Modificar)
                    {

                        Detalles[Id_Item - 1] = ItemDetalle;

                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;

                    }
           
                    btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdatos.DataSource = Detalles;
            }
        }

        public bool VerificarDetalle()
        {
            if (string.IsNullOrEmpty(txttipobsadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar un tipo");
                txttipobsacod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtproductodesc.Text))
            {
                Accion.Advertencia("Debe ingresar un producto");
                txtproductocod.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtcantidad.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar una cantidad");
                txtcantidad.Focus();
                return false;
            }




            return true;
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {


                    Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                                   dgvdatos.DataSource = null;
                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Requerimiento Det in Detalles)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;
        }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Nuevo;

            HabilitarDetalles();
            txttipobsacod.Focus();
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Requerimiento Entidad = new Entidad_Requerimiento();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    Id_Item = Entidad.Id_Item;



                    txttipobsacod.Tag = Entidad.Req_Tipo_BSA;
                    txttipobsacod.Text = Entidad.Req_Tipo_BSA_Det;
                    txttipobsadesc.Text = Entidad.Req_Tipo_BSA_Descripcion;

                    txtproductocod.Text = Entidad.Req_Catalogo;
                    txtproductodesc.Text = Entidad.Req_Catalogo_Desc;


                    txtdetalle.Text = Entidad.Req_Detalle;
                    txtobservacion.Text = Entidad.Req_Observacion;

                    txtcantidad.Text = Entidad.Req_Cantidad.ToString();
       



                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle = Estados.Consulta;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle = Estados.SoloLectura;


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void BuscarArea()
        {
            try
            {
                txtarea.Text = Accion.Formato(txtarea.Text, 2);

                Logica_Area log = new Logica_Area();

                List<Entidad_Area> Generales = new List<Entidad_Area>();
                Generales = log.Listar(new Entidad_Area
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Area = txtarea.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Area T in Generales)
                    {
                        if ((T.Id_Area).ToString().Trim().ToUpper() == txtarea.Text.Trim().ToUpper())
                        {

                            txtarea.Text = (T.Id_Area).ToString().Trim();
                            txtareadesc.Text = T.Are_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcargo.EnterMoveNextControl = false;
                    txtcargodesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Requerimiento Ent = new Entidad_Requerimiento();
                    Logica_Requerimiento Log = new Logica_Requerimiento();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
  
                    Ent.Id_Movimiento = Id_Movimiento;

                    Ent.Req_Solicitante_Ruc_dni = txtsolicitanteruc.Text;
                    Ent.Rec_Id_Area = txtarea.Text;
                    Ent.Req_Id_Cargo = txtcargo.Text;

                    Ent.Req_Encardado_Ruc_Dni = txtencargadoruc.Text;
                    Ent.Req_Descripcion = txtdescripcion.Text.ToString();
                    Ent.Req_Tipo_Doc = txttipodoc.Text;

                    //Ent.Req_Serie = txtserie.Text.ToString();
                    //Ent.Req_Numero = txtnumero.Text.ToString();

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Req_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Req_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();

                    Ent.Req_Fecha_Envio = Convert.ToDateTime(txtfechaenvio.Text);
                    DateTime HoraFin;
                    HoraFin = DtpHora.Value;
                    Ent.Req_Hora_Envio = new TimeSpan(HoraFin.Hour, HoraFin.Minute, HoraFin.Second);

                    Ent.Req_O_C = txtoc.Text.ToString();

                    Ent.Req_Codigo_Proyecto = txtproyecto.Text;

                    Ent.DetalleS = Detalles;



                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();
                            Detalles.Clear();
                            dgvdatos.DataSource = null;
                            BloquearDetalles();
                            txtsolicitanteruc.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtsolicitantedesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un solicitante");
                txtsolicitanteruc.Focus();
                return false;
            }


            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

            if (string.IsNullOrEmpty(txtareadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un area");
                txtarea.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtcargodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un cargo");
                txtcargo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtencargadodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un encargado");
                txtencargadoruc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }


            return true;
        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
