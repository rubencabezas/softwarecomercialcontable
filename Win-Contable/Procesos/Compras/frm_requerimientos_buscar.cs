﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Procesos.Compras
{
    public partial class frm_requerimientos_buscar : Contable.frm_fuente
    {
        public frm_requerimientos_buscar()
        {
            InitializeComponent();
        }
        public List<Entidad_Requerimiento> Lista = new List<Entidad_Requerimiento>();
        public void Listar()
        {
            Entidad_Requerimiento Ent = new Entidad_Requerimiento();
            Logica_Requerimiento log = new Logica_Requerimiento();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Movimiento = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_requerimientos_buscar_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Lista.Count > 0)
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
