﻿namespace Contable.Procesos.Compras
{
    partial class frm_requerimiento_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.txtobservacion = new DevExpress.XtraEditors.TextEdit();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.txtdetalle = new DevExpress.XtraEditors.TextEdit();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.txtcantidad = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txtunidad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.DtpHora = new System.Windows.Forms.DateTimePicker();
            this.txtproyecto = new DevExpress.XtraEditors.TextEdit();
            this.txtoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechaenvio = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.txtencargadodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcargo = new DevExpress.XtraEditors.TextEdit();
            this.txtencargadoruc = new DevExpress.XtraEditors.TextEdit();
            this.txtcargodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtareadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtarea = new DevExpress.XtraEditors.TextEdit();
            this.txtsolicitantedesc = new DevExpress.XtraEditors.TextEdit();
            this.txtsolicitanteruc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtobservacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdetalle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtproyecto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaenvio.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtencargadodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcargo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtencargadoruc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcargodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtareadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtarea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsolicitantedesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsolicitanteruc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.textEdit16);
            this.groupControl1.Controls.Add(this.textEdit15);
            this.groupControl1.Controls.Add(this.labelControl13);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.textEdit14);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.textEdit9);
            this.groupControl1.Controls.Add(this.textEdit8);
            this.groupControl1.Controls.Add(this.textEdit7);
            this.groupControl1.Controls.Add(this.textEdit6);
            this.groupControl1.Controls.Add(this.textEdit5);
            this.groupControl1.Controls.Add(this.textEdit4);
            this.groupControl1.Controls.Add(this.textEdit3);
            this.groupControl1.Controls.Add(this.textEdit2);
            this.groupControl1.Controls.Add(this.textEdit1);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(772, 177);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // textEdit16
            // 
            this.textEdit16.Location = new System.Drawing.Point(571, 153);
            this.textEdit16.MenuManager = this.barManager1;
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Size = new System.Drawing.Size(168, 20);
            this.textEdit16.TabIndex = 29;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(772, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 554);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(772, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 522);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(772, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 522);
            // 
            // textEdit15
            // 
            this.textEdit15.Location = new System.Drawing.Point(352, 153);
            this.textEdit15.MenuManager = this.barManager1;
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Size = new System.Drawing.Size(100, 20);
            this.textEdit15.TabIndex = 28;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(469, 156);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(96, 13);
            this.labelControl13.TabIndex = 27;
            this.labelControl13.Text = "Codido del proyecto";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(302, 156);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(44, 13);
            this.labelControl10.TabIndex = 24;
            this.labelControl10.Text = "N de O/C";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(185, 156);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(23, 13);
            this.labelControl6.TabIndex = 23;
            this.labelControl6.Text = "Hora";
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(104, 153);
            this.textEdit14.MenuManager = this.barManager1;
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(75, 20);
            this.textEdit14.TabIndex = 22;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(59, 156);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 13);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "F. Envio";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textEdit10);
            this.groupBox1.Controls.Add(this.textEdit12);
            this.groupBox1.Controls.Add(this.textEdit13);
            this.groupBox1.Controls.Add(this.textEdit11);
            this.groupBox1.Controls.Add(this.labelControl8);
            this.groupBox1.Controls.Add(this.labelControl7);
            this.groupBox1.Location = new System.Drawing.Point(106, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(633, 40);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Documento";
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(8, 14);
            this.textEdit10.MenuManager = this.barManager1;
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(44, 20);
            this.textEdit10.TabIndex = 14;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(406, 14);
            this.textEdit12.MenuManager = this.barManager1;
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(72, 20);
            this.textEdit12.TabIndex = 20;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(312, 14);
            this.textEdit13.MenuManager = this.barManager1;
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(48, 20);
            this.textEdit13.TabIndex = 21;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(54, 14);
            this.textEdit11.MenuManager = this.barManager1;
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(222, 20);
            this.textEdit11.TabIndex = 15;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(363, 17);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(37, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Numero";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(282, 17);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(24, 13);
            this.labelControl7.TabIndex = 17;
            this.labelControl7.Text = "Serie";
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(106, 88);
            this.textEdit9.MenuManager = this.barManager1;
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(633, 20);
            this.textEdit9.TabIndex = 13;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(194, 66);
            this.textEdit8.MenuManager = this.barManager1;
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(545, 20);
            this.textEdit8.TabIndex = 12;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(406, 44);
            this.textEdit7.MenuManager = this.barManager1;
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(42, 20);
            this.textEdit7.TabIndex = 11;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(106, 66);
            this.textEdit6.MenuManager = this.barManager1;
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(87, 20);
            this.textEdit6.TabIndex = 10;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(449, 44);
            this.textEdit5.MenuManager = this.barManager1;
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(290, 20);
            this.textEdit5.TabIndex = 9;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(114, 44);
            this.textEdit4.MenuManager = this.barManager1;
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(247, 20);
            this.textEdit4.TabIndex = 8;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(71, 44);
            this.textEdit3.MenuManager = this.barManager1;
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(42, 20);
            this.textEdit3.TabIndex = 7;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(160, 23);
            this.textEdit2.MenuManager = this.barManager1;
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(579, 20);
            this.textEdit2.TabIndex = 6;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(71, 23);
            this.textEdit1.MenuManager = this.barManager1;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(87, 20);
            this.textEdit1.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(46, 91);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(54, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Descripcion";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 69);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(95, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Encargado de envio";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(367, 47);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(33, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Cargo:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(42, 47);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Area";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Solicitante:";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(5, 23);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(69, 13);
            this.labelControl11.TabIndex = 25;
            this.labelControl11.Text = "labelControl11";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(160, 23);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(69, 13);
            this.labelControl12.TabIndex = 26;
            this.labelControl12.Text = "labelControl12";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Location = new System.Drawing.Point(0, 221);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(767, 100);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Detalles";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.dgvdatos);
            this.groupControl3.Controls.Add(this.btnanadirdet);
            this.groupControl3.Controls.Add(this.labelControl30);
            this.groupControl3.Controls.Add(this.btneditardet);
            this.groupControl3.Controls.Add(this.txtobservacion);
            this.groupControl3.Controls.Add(this.btnquitardet);
            this.groupControl3.Controls.Add(this.labelControl29);
            this.groupControl3.Controls.Add(this.btnnuevodet);
            this.groupControl3.Controls.Add(this.txtdetalle);
            this.groupControl3.Controls.Add(this.txtproductodesc);
            this.groupControl3.Controls.Add(this.labelControl28);
            this.groupControl3.Controls.Add(this.txttipobsadesc);
            this.groupControl3.Controls.Add(this.txtproductocod);
            this.groupControl3.Controls.Add(this.txtcantidad);
            this.groupControl3.Controls.Add(this.txttipobsacod);
            this.groupControl3.Controls.Add(this.labelControl27);
            this.groupControl3.Controls.Add(this.txtunidad);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.labelControl15);
            this.groupControl3.Location = new System.Drawing.Point(0, 221);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(772, 332);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Detalles";
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(5, 113);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(762, 214);
            this.dgvdatos.TabIndex = 38;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Tipo";
            this.gridColumn1.FieldName = "Req_Tipo_BSA_Descripcion";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Producto";
            this.gridColumn2.FieldName = "Req_Catalogo_Desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 221;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Detalle";
            this.gridColumn3.FieldName = "Req_Detalle";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 148;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "Observacion";
            this.gridColumn4.FieldName = "Req_Observacion";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 208;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Cantidad";
            this.gridColumn5.FieldName = "Req_Cantidad";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Location = new System.Drawing.Point(708, 53);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(56, 23);
            this.btnanadirdet.TabIndex = 14;
            this.btnanadirdet.Text = "Agregar";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(4, 65);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(60, 13);
            this.labelControl30.TabIndex = 8;
            this.labelControl30.Text = "Observacion";
            // 
            // btneditardet
            // 
            this.btneditardet.Location = new System.Drawing.Point(650, 53);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(56, 23);
            this.btneditardet.TabIndex = 18;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // txtobservacion
            // 
            this.txtobservacion.EnterMoveNextControl = true;
            this.txtobservacion.Location = new System.Drawing.Point(64, 65);
            this.txtobservacion.MenuManager = this.barManager1;
            this.txtobservacion.Name = "txtobservacion";
            this.txtobservacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtobservacion.Size = new System.Drawing.Size(567, 20);
            this.txtobservacion.TabIndex = 9;
            // 
            // btnquitardet
            // 
            this.btnquitardet.Location = new System.Drawing.Point(708, 23);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(56, 23);
            this.btnquitardet.TabIndex = 17;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(246, 90);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(43, 13);
            this.labelControl29.TabIndex = 12;
            this.labelControl29.Text = "Cantidad";
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Location = new System.Drawing.Point(650, 23);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(56, 23);
            this.btnnuevodet.TabIndex = 16;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // txtdetalle
            // 
            this.txtdetalle.EnterMoveNextControl = true;
            this.txtdetalle.Location = new System.Drawing.Point(64, 43);
            this.txtdetalle.MenuManager = this.barManager1;
            this.txtdetalle.Name = "txtdetalle";
            this.txtdetalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdetalle.Size = new System.Drawing.Size(567, 20);
            this.txtdetalle.TabIndex = 7;
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(324, 20);
            this.txtproductodesc.MenuManager = this.barManager1;
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproductodesc.Size = new System.Drawing.Size(307, 20);
            this.txtproductodesc.TabIndex = 5;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(5, 89);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(70, 13);
            this.labelControl28.TabIndex = 10;
            this.labelControl28.Text = "Unidad Medida";
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(93, 22);
            this.txttipobsadesc.MenuManager = this.barManager1;
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipobsadesc.Size = new System.Drawing.Size(100, 20);
            this.txttipobsadesc.TabIndex = 2;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(243, 20);
            this.txtproductocod.MenuManager = this.barManager1;
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproductocod.Size = new System.Drawing.Size(79, 20);
            this.txtproductocod.TabIndex = 4;
            this.txtproductocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproductocod_KeyDown);
            // 
            // txtcantidad
            // 
            this.txtcantidad.EnterMoveNextControl = true;
            this.txtcantidad.Location = new System.Drawing.Point(295, 87);
            this.txtcantidad.MenuManager = this.barManager1;
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Size = new System.Drawing.Size(100, 20);
            this.txtcantidad.TabIndex = 13;
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(64, 22);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipobsacod.Size = new System.Drawing.Size(27, 20);
            this.txttipobsacod.TabIndex = 1;
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(25, 46);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(33, 13);
            this.labelControl27.TabIndex = 6;
            this.labelControl27.Text = "Detalle";
            // 
            // txtunidad
            // 
            this.txtunidad.Enabled = false;
            this.txtunidad.EnterMoveNextControl = true;
            this.txtunidad.Location = new System.Drawing.Point(81, 86);
            this.txtunidad.MenuManager = this.barManager1;
            this.txtunidad.Name = "txtunidad";
            this.txtunidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunidad.Size = new System.Drawing.Size(159, 20);
            this.txtunidad.TabIndex = 11;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(197, 26);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(43, 13);
            this.labelControl14.TabIndex = 3;
            this.labelControl14.Text = "Producto";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(18, 25);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(39, 13);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Tipo B/S";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.DtpHora);
            this.groupControl4.Controls.Add(this.txtproyecto);
            this.groupControl4.Controls.Add(this.txtoc);
            this.groupControl4.Controls.Add(this.labelControl16);
            this.groupControl4.Controls.Add(this.labelControl17);
            this.groupControl4.Controls.Add(this.labelControl18);
            this.groupControl4.Controls.Add(this.txtfechaenvio);
            this.groupControl4.Controls.Add(this.labelControl19);
            this.groupControl4.Controls.Add(this.groupBox2);
            this.groupControl4.Controls.Add(this.txtdescripcion);
            this.groupControl4.Controls.Add(this.txtencargadodesc);
            this.groupControl4.Controls.Add(this.txtcargo);
            this.groupControl4.Controls.Add(this.txtencargadoruc);
            this.groupControl4.Controls.Add(this.txtcargodesc);
            this.groupControl4.Controls.Add(this.txtareadesc);
            this.groupControl4.Controls.Add(this.txtarea);
            this.groupControl4.Controls.Add(this.txtsolicitantedesc);
            this.groupControl4.Controls.Add(this.txtsolicitanteruc);
            this.groupControl4.Controls.Add(this.labelControl22);
            this.groupControl4.Controls.Add(this.labelControl23);
            this.groupControl4.Controls.Add(this.labelControl24);
            this.groupControl4.Controls.Add(this.labelControl25);
            this.groupControl4.Controls.Add(this.labelControl26);
            this.groupControl4.Location = new System.Drawing.Point(0, 38);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(772, 177);
            this.groupControl4.TabIndex = 0;
            this.groupControl4.Text = "Datos Generales";
            // 
            // DtpHora
            // 
            this.DtpHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DtpHora.Location = new System.Drawing.Point(209, 152);
            this.DtpHora.Name = "DtpHora";
            this.DtpHora.Size = new System.Drawing.Size(86, 21);
            this.DtpHora.TabIndex = 24;
            this.DtpHora.Value = new System.DateTime(2015, 2, 17, 18, 32, 55, 0);
            // 
            // txtproyecto
            // 
            this.txtproyecto.EnterMoveNextControl = true;
            this.txtproyecto.Location = new System.Drawing.Point(571, 153);
            this.txtproyecto.MenuManager = this.barManager1;
            this.txtproyecto.Name = "txtproyecto";
            this.txtproyecto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproyecto.Size = new System.Drawing.Size(168, 20);
            this.txtproyecto.TabIndex = 28;
            // 
            // txtoc
            // 
            this.txtoc.EnterMoveNextControl = true;
            this.txtoc.Location = new System.Drawing.Point(352, 153);
            this.txtoc.MenuManager = this.barManager1;
            this.txtoc.Name = "txtoc";
            this.txtoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtoc.Size = new System.Drawing.Size(100, 20);
            this.txtoc.TabIndex = 26;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(469, 156);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(96, 13);
            this.labelControl16.TabIndex = 27;
            this.labelControl16.Text = "Codido del proyecto";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(302, 156);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(44, 13);
            this.labelControl17.TabIndex = 25;
            this.labelControl17.Text = "N de O/C";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(181, 156);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(23, 13);
            this.labelControl18.TabIndex = 23;
            this.labelControl18.Text = "Hora";
            // 
            // txtfechaenvio
            // 
            this.txtfechaenvio.EnterMoveNextControl = true;
            this.txtfechaenvio.Location = new System.Drawing.Point(104, 153);
            this.txtfechaenvio.MenuManager = this.barManager1;
            this.txtfechaenvio.Name = "txtfechaenvio";
            this.txtfechaenvio.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechaenvio.Properties.Mask.BeepOnError = true;
            this.txtfechaenvio.Properties.Mask.EditMask = "d";
            this.txtfechaenvio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechaenvio.Properties.Mask.SaveLiteral = false;
            this.txtfechaenvio.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechaenvio.Properties.MaxLength = 10;
            this.txtfechaenvio.Size = new System.Drawing.Size(75, 20);
            this.txtfechaenvio.TabIndex = 22;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(59, 156);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(39, 13);
            this.labelControl19.TabIndex = 21;
            this.labelControl19.Text = "F. Envio";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txttipodoc);
            this.groupBox2.Controls.Add(this.txtserie);
            this.groupBox2.Controls.Add(this.txttipodocdesc);
            this.groupBox2.Controls.Add(this.labelControl21);
            this.groupBox2.Location = new System.Drawing.Point(106, 110);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(633, 40);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Documento";
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(8, 14);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(44, 20);
            this.txttipodoc.TabIndex = 15;
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(312, 14);
            this.txtserie.MenuManager = this.barManager1;
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(315, 20);
            this.txtserie.TabIndex = 18;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(54, 14);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(222, 20);
            this.txttipodocdesc.TabIndex = 16;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(282, 17);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(24, 13);
            this.labelControl21.TabIndex = 17;
            this.labelControl21.Text = "Serie";
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.EnterMoveNextControl = true;
            this.txtdescripcion.Location = new System.Drawing.Point(106, 88);
            this.txtdescripcion.MenuManager = this.barManager1;
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Size = new System.Drawing.Size(633, 20);
            this.txtdescripcion.TabIndex = 13;
            // 
            // txtencargadodesc
            // 
            this.txtencargadodesc.Enabled = false;
            this.txtencargadodesc.EnterMoveNextControl = true;
            this.txtencargadodesc.Location = new System.Drawing.Point(194, 66);
            this.txtencargadodesc.MenuManager = this.barManager1;
            this.txtencargadodesc.Name = "txtencargadodesc";
            this.txtencargadodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtencargadodesc.Size = new System.Drawing.Size(545, 20);
            this.txtencargadodesc.TabIndex = 11;
            // 
            // txtcargo
            // 
            this.txtcargo.EnterMoveNextControl = true;
            this.txtcargo.Location = new System.Drawing.Point(406, 44);
            this.txtcargo.MenuManager = this.barManager1;
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcargo.Size = new System.Drawing.Size(42, 20);
            this.txtcargo.TabIndex = 7;
            this.txtcargo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcargo_KeyDown);
            // 
            // txtencargadoruc
            // 
            this.txtencargadoruc.EnterMoveNextControl = true;
            this.txtencargadoruc.Location = new System.Drawing.Point(106, 66);
            this.txtencargadoruc.MenuManager = this.barManager1;
            this.txtencargadoruc.Name = "txtencargadoruc";
            this.txtencargadoruc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtencargadoruc.Size = new System.Drawing.Size(87, 20);
            this.txtencargadoruc.TabIndex = 10;
            this.txtencargadoruc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtencargadoruc_KeyDown);
            // 
            // txtcargodesc
            // 
            this.txtcargodesc.Enabled = false;
            this.txtcargodesc.EnterMoveNextControl = true;
            this.txtcargodesc.Location = new System.Drawing.Point(449, 44);
            this.txtcargodesc.MenuManager = this.barManager1;
            this.txtcargodesc.Name = "txtcargodesc";
            this.txtcargodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcargodesc.Size = new System.Drawing.Size(290, 20);
            this.txtcargodesc.TabIndex = 8;
            // 
            // txtareadesc
            // 
            this.txtareadesc.Enabled = false;
            this.txtareadesc.EnterMoveNextControl = true;
            this.txtareadesc.Location = new System.Drawing.Point(114, 44);
            this.txtareadesc.MenuManager = this.barManager1;
            this.txtareadesc.Name = "txtareadesc";
            this.txtareadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtareadesc.Size = new System.Drawing.Size(247, 20);
            this.txtareadesc.TabIndex = 5;
            // 
            // txtarea
            // 
            this.txtarea.EnterMoveNextControl = true;
            this.txtarea.Location = new System.Drawing.Point(71, 44);
            this.txtarea.MenuManager = this.barManager1;
            this.txtarea.Name = "txtarea";
            this.txtarea.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtarea.Size = new System.Drawing.Size(42, 20);
            this.txtarea.TabIndex = 4;
            this.txtarea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtarea_KeyDown);
            // 
            // txtsolicitantedesc
            // 
            this.txtsolicitantedesc.Enabled = false;
            this.txtsolicitantedesc.EnterMoveNextControl = true;
            this.txtsolicitantedesc.Location = new System.Drawing.Point(160, 23);
            this.txtsolicitantedesc.MenuManager = this.barManager1;
            this.txtsolicitantedesc.Name = "txtsolicitantedesc";
            this.txtsolicitantedesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtsolicitantedesc.Size = new System.Drawing.Size(579, 20);
            this.txtsolicitantedesc.TabIndex = 2;
            // 
            // txtsolicitanteruc
            // 
            this.txtsolicitanteruc.EnterMoveNextControl = true;
            this.txtsolicitanteruc.Location = new System.Drawing.Point(71, 23);
            this.txtsolicitanteruc.MenuManager = this.barManager1;
            this.txtsolicitanteruc.Name = "txtsolicitanteruc";
            this.txtsolicitanteruc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtsolicitanteruc.Size = new System.Drawing.Size(87, 20);
            this.txtsolicitanteruc.TabIndex = 1;
            this.txtsolicitanteruc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsolicitanteruc_KeyDown);
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(46, 91);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(54, 13);
            this.labelControl22.TabIndex = 12;
            this.labelControl22.Text = "Descripcion";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(5, 69);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(95, 13);
            this.labelControl23.TabIndex = 9;
            this.labelControl23.Text = "Encargado de envio";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(367, 47);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(33, 13);
            this.labelControl24.TabIndex = 6;
            this.labelControl24.Text = "Cargo:";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(42, 47);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(23, 13);
            this.labelControl25.TabIndex = 3;
            this.labelControl25.Text = "Area";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(12, 26);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(53, 13);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Solicitante:";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 32);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Size = new System.Drawing.Size(0, 522);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(772, 32);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Size = new System.Drawing.Size(0, 522);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(0, 554);
            this.barDockControl3.Manager = this.barManager1;
            this.barDockControl3.Size = new System.Drawing.Size(772, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 32);
            this.barDockControl4.Manager = this.barManager1;
            this.barDockControl4.Size = new System.Drawing.Size(772, 0);
            // 
            // frm_requerimiento_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(772, 554);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_requerimiento_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Requerimiento";
            this.Load += new System.EventHandler(this.frm_requerimiento_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtobservacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdetalle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtproyecto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaenvio.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtencargadodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcargo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtencargadoruc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcargodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtareadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtarea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsolicitantedesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsolicitanteruc.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.TextEdit txtproyecto;
        private DevExpress.XtraEditors.TextEdit txtoc;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtfechaenvio;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtdescripcion;
        private DevExpress.XtraEditors.TextEdit txtencargadodesc;
        private DevExpress.XtraEditors.TextEdit txtcargo;
        private DevExpress.XtraEditors.TextEdit txtencargadoruc;
        private DevExpress.XtraEditors.TextEdit txtcargodesc;
        private DevExpress.XtraEditors.TextEdit txtareadesc;
        private DevExpress.XtraEditors.TextEdit txtarea;
        private DevExpress.XtraEditors.TextEdit txtsolicitantedesc;
        private DevExpress.XtraEditors.TextEdit txtsolicitanteruc;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.TextEdit txttipobsadesc;
        private DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnanadirdet;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.TextEdit txtobservacion;
        private DevExpress.XtraEditors.SimpleButton btnquitardet;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraEditors.TextEdit txtdetalle;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit txtcantidad;
        private DevExpress.XtraEditors.TextEdit txtunidad;
        internal System.Windows.Forms.DateTimePicker DtpHora;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
    }
}
