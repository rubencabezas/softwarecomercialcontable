﻿namespace Contable.Procesos.Compras
{
    partial class frm_orden_sc_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtobservacion = new DevExpress.XtraEditors.TextEdit();
            this.txtresponsabledesc = new DevExpress.XtraEditors.TextEdit();
            this.txtresponsable = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtproveedordesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproveedor = new DevExpress.XtraEditors.TextEdit();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.txtordendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtordencod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chkafectoigv = new DevExpress.XtraEditors.CheckEdit();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtpreciounitario = new DevExpress.XtraEditors.TextEdit();
            this.txtcantidad = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacencod = new DevExpress.XtraEditors.TextEdit();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtfechavencimiento = new DevExpress.XtraEditors.TextEdit();
            this.txtdias = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txttotal = new DevExpress.XtraEditors.TextEdit();
            this.txtigv = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteigv = new DevExpress.XtraEditors.TextEdit();
            this.txtsubtotal = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnbuscar = new DevExpress.XtraEditors.SimpleButton();
            this.btnlistar = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtobservacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsabledesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproveedordesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproveedor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtordendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtordencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkafectoigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpreciounitario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdias.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsubtotal.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(841, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 560);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(841, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(841, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 528);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtobservacion);
            this.groupControl1.Controls.Add(this.txtresponsabledesc);
            this.groupControl1.Controls.Add(this.txtresponsable);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtproveedordesc);
            this.groupControl1.Controls.Add(this.txtproveedor);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.txtordendesc);
            this.groupControl1.Controls.Add(this.txtordencod);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 36);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(841, 114);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos generales";
            // 
            // txtobservacion
            // 
            this.txtobservacion.EnterMoveNextControl = true;
            this.txtobservacion.Location = new System.Drawing.Point(76, 89);
            this.txtobservacion.MenuManager = this.barManager1;
            this.txtobservacion.Name = "txtobservacion";
            this.txtobservacion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtobservacion.Size = new System.Drawing.Size(679, 20);
            this.txtobservacion.TabIndex = 17;
            // 
            // txtresponsabledesc
            // 
            this.txtresponsabledesc.Enabled = false;
            this.txtresponsabledesc.EnterMoveNextControl = true;
            this.txtresponsabledesc.Location = new System.Drawing.Point(159, 68);
            this.txtresponsabledesc.MenuManager = this.barManager1;
            this.txtresponsabledesc.Name = "txtresponsabledesc";
            this.txtresponsabledesc.Size = new System.Drawing.Size(596, 20);
            this.txtresponsabledesc.TabIndex = 15;
            // 
            // txtresponsable
            // 
            this.txtresponsable.EnterMoveNextControl = true;
            this.txtresponsable.Location = new System.Drawing.Point(76, 68);
            this.txtresponsable.MenuManager = this.barManager1;
            this.txtresponsable.Name = "txtresponsable";
            this.txtresponsable.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtresponsable.Size = new System.Drawing.Size(82, 20);
            this.txtresponsable.TabIndex = 14;
            this.txtresponsable.TextChanged += new System.EventHandler(this.txtresponsable_TextChanged);
            this.txtresponsable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtresponsable_KeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 90);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Observacion";
            // 
            // txtproveedordesc
            // 
            this.txtproveedordesc.Enabled = false;
            this.txtproveedordesc.EnterMoveNextControl = true;
            this.txtproveedordesc.Location = new System.Drawing.Point(159, 46);
            this.txtproveedordesc.MenuManager = this.barManager1;
            this.txtproveedordesc.Name = "txtproveedordesc";
            this.txtproveedordesc.Size = new System.Drawing.Size(596, 20);
            this.txtproveedordesc.TabIndex = 12;
            // 
            // txtproveedor
            // 
            this.txtproveedor.EnterMoveNextControl = true;
            this.txtproveedor.Location = new System.Drawing.Point(76, 46);
            this.txtproveedor.MenuManager = this.barManager1;
            this.txtproveedor.Name = "txtproveedor";
            this.txtproveedor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproveedor.Size = new System.Drawing.Size(82, 20);
            this.txtproveedor.TabIndex = 11;
            this.txtproveedor.TextChanged += new System.EventHandler(this.txtproveedor_TextChanged);
            this.txtproveedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproveedor_KeyDown);
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(553, 23);
            this.txtserie.MenuManager = this.barManager1;
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(202, 20);
            this.txtserie.TabIndex = 7;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.Location = new System.Drawing.Point(339, 23);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(174, 20);
            this.txttipodocdesc.TabIndex = 5;
            // 
            // txttipodoc
            // 
            this.txttipodoc.Enabled = false;
            this.txttipodoc.Location = new System.Drawing.Point(294, 23);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(42, 20);
            this.txttipodoc.TabIndex = 4;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // txtordendesc
            // 
            this.txtordendesc.Enabled = false;
            this.txtordendesc.EnterMoveNextControl = true;
            this.txtordendesc.Location = new System.Drawing.Point(107, 23);
            this.txtordendesc.Name = "txtordendesc";
            this.txtordendesc.Size = new System.Drawing.Size(145, 20);
            this.txtordendesc.TabIndex = 2;
            // 
            // txtordencod
            // 
            this.txtordencod.Location = new System.Drawing.Point(76, 23);
            this.txtordencod.MenuManager = this.barManager1;
            this.txtordencod.Name = "txtordencod";
            this.txtordencod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtordencod.Size = new System.Drawing.Size(30, 20);
            this.txtordencod.TabIndex = 1;
            this.txtordencod.TextChanged += new System.EventHandler(this.txtordencod_TextChanged);
            this.txtordencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtordencod_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(5, 71);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(65, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Responsable:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(20, 49);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(50, 13);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Proveedor";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(519, 26);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Serie:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(253, 26);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "T. Doc.";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo Orden:";
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(431, 39);
            this.txttipocambiovalor.MenuManager = this.barManager1;
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Size = new System.Drawing.Size(76, 20);
            this.txttipocambiovalor.TabIndex = 15;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(287, 39);
            this.txttipocambiodesc.MenuManager = this.barManager1;
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Size = new System.Drawing.Size(142, 20);
            this.txttipocambiodesc.TabIndex = 14;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(253, 39);
            this.txttipocambiocod.MenuManager = this.barManager1;
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiocod.Size = new System.Drawing.Size(33, 20);
            this.txttipocambiocod.TabIndex = 13;
            this.txttipocambiocod.TextChanged += new System.EventHandler(this.txttipocambiocod_TextChanged);
            this.txttipocambiocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocod_KeyDown);
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(86, 37);
            this.txtmonedadesc.MenuManager = this.barManager1;
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(103, 20);
            this.txtmonedadesc.TabIndex = 11;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(53, 37);
            this.txtmonedacod.MenuManager = this.barManager1;
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedacod.Size = new System.Drawing.Size(32, 20);
            this.txtmonedacod.TabIndex = 10;
            this.txtmonedacod.TextChanged += new System.EventHandler(this.txtmonedacod_TextChanged);
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(235, 15);
            this.txtfechadoc.MenuManager = this.barManager1;
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(74, 20);
            this.txtfechadoc.TabIndex = 4;
            this.txtfechadoc.TextChanged += new System.EventHandler(this.txtfechadoc_TextChanged);
            this.txtfechadoc.Leave += new System.EventHandler(this.txtfechadoc_Leave);
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(86, 15);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Size = new System.Drawing.Size(103, 20);
            this.txtcondiciondesc.TabIndex = 2;
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(52, 15);
            this.txtcondicioncod.MenuManager = this.barManager1;
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(33, 20);
            this.txtcondicioncod.TabIndex = 1;
            this.txtcondicioncod.TextChanged += new System.EventHandler(this.txtcondicioncod_TextChanged);
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(196, 18);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(33, 13);
            this.labelControl12.TabIndex = 3;
            this.labelControl12.Text = "Fecha:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(226, 42);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(21, 13);
            this.labelControl10.TabIndex = 12;
            this.labelControl10.Text = "T.C.";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(8, 40);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 9;
            this.labelControl9.Text = "Moneda:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(5, 19);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(46, 13);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Condicion";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(20, 26);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(43, 13);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "Tipo B/S:";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.chkafectoigv);
            this.groupControl3.Controls.Add(this.btnanadirdet);
            this.groupControl3.Controls.Add(this.btneditardet);
            this.groupControl3.Controls.Add(this.btnquitardet);
            this.groupControl3.Controls.Add(this.btnnuevodet);
            this.groupControl3.Controls.Add(this.txtdescripcion);
            this.groupControl3.Controls.Add(this.labelControl17);
            this.groupControl3.Controls.Add(this.dgvdatos);
            this.groupControl3.Controls.Add(this.txtpreciounitario);
            this.groupControl3.Controls.Add(this.txtcantidad);
            this.groupControl3.Controls.Add(this.txtalmacendesc);
            this.groupControl3.Controls.Add(this.txtalmacencod);
            this.groupControl3.Controls.Add(this.txtproductodesc);
            this.groupControl3.Controls.Add(this.txtproductocod);
            this.groupControl3.Controls.Add(this.labelControl16);
            this.groupControl3.Controls.Add(this.labelControl15);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.labelControl13);
            this.groupControl3.Controls.Add(this.txttipobsadesc);
            this.groupControl3.Controls.Add(this.txttipobsacod);
            this.groupControl3.Controls.Add(this.labelControl11);
            this.groupControl3.Location = new System.Drawing.Point(0, 232);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(841, 323);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Detalles";
            // 
            // chkafectoigv
            // 
            this.chkafectoigv.EnterMoveNextControl = true;
            this.chkafectoigv.Location = new System.Drawing.Point(68, 86);
            this.chkafectoigv.MenuManager = this.barManager1;
            this.chkafectoigv.Name = "chkafectoigv";
            this.chkafectoigv.Properties.Caption = "Afecto a IGV";
            this.chkafectoigv.Size = new System.Drawing.Size(90, 20);
            this.chkafectoigv.TabIndex = 16;
            this.chkafectoigv.CheckedChanged += new System.EventHandler(this.chkafectoigv_CheckedChanged);
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnanadirdet.Appearance.Options.UseFont = true;
            this.btnanadirdet.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.btnanadirdet.Location = new System.Drawing.Point(741, 91);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(87, 21);
            this.btnanadirdet.TabIndex = 17;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btneditardet.Appearance.Options.UseFont = true;
            this.btneditardet.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.btneditardet.Location = new System.Drawing.Point(741, 68);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(87, 21);
            this.btneditardet.TabIndex = 24;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnquitardet.Appearance.Options.UseFont = true;
            this.btnquitardet.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.btnquitardet.Location = new System.Drawing.Point(741, 46);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(87, 21);
            this.btnquitardet.TabIndex = 23;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnnuevodet.Appearance.Options.UseFont = true;
            this.btnnuevodet.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.btnnuevodet.Location = new System.Drawing.Point(741, 24);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(87, 21);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.EnterMoveNextControl = true;
            this.txtdescripcion.Location = new System.Drawing.Point(69, 45);
            this.txtdescripcion.MenuManager = this.barManager1;
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Size = new System.Drawing.Size(666, 20);
            this.txtdescripcion.TabIndex = 8;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(5, 50);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(58, 13);
            this.labelControl17.TabIndex = 7;
            this.labelControl17.Text = "Descripcion:";
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(5, 116);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(824, 202);
            this.dgvdatos.TabIndex = 20;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Tipo";
            this.gridColumn1.FieldName = "Ord_Tipo_BSA_Desc";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Producto";
            this.gridColumn2.FieldName = "Ord_Catalogo_desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 345;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Almacen";
            this.gridColumn3.FieldName = "Ord_Almacen_desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 164;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "Cantidad";
            this.gridColumn4.FieldName = "Ord_Cantidad";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Precio Unit.";
            this.gridColumn5.FieldName = "Ord_Precio_Unitario";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // txtpreciounitario
            // 
            this.txtpreciounitario.EnterMoveNextControl = true;
            this.txtpreciounitario.Location = new System.Drawing.Point(535, 66);
            this.txtpreciounitario.MenuManager = this.barManager1;
            this.txtpreciounitario.Name = "txtpreciounitario";
            this.txtpreciounitario.Properties.Mask.EditMask = "n3";
            this.txtpreciounitario.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtpreciounitario.Size = new System.Drawing.Size(82, 20);
            this.txtpreciounitario.TabIndex = 15;
            // 
            // txtcantidad
            // 
            this.txtcantidad.EnterMoveNextControl = true;
            this.txtcantidad.Location = new System.Drawing.Point(365, 66);
            this.txtcantidad.MenuManager = this.barManager1;
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Properties.Mask.EditMask = "n3";
            this.txtcantidad.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtcantidad.Size = new System.Drawing.Size(100, 20);
            this.txtcantidad.TabIndex = 13;
            // 
            // txtalmacendesc
            // 
            this.txtalmacendesc.Enabled = false;
            this.txtalmacendesc.EnterMoveNextControl = true;
            this.txtalmacendesc.Location = new System.Drawing.Point(104, 66);
            this.txtalmacendesc.MenuManager = this.barManager1;
            this.txtalmacendesc.Name = "txtalmacendesc";
            this.txtalmacendesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtalmacendesc.Size = new System.Drawing.Size(205, 20);
            this.txtalmacendesc.TabIndex = 11;
            // 
            // txtalmacencod
            // 
            this.txtalmacencod.EnterMoveNextControl = true;
            this.txtalmacencod.Location = new System.Drawing.Point(69, 66);
            this.txtalmacencod.MenuManager = this.barManager1;
            this.txtalmacencod.Name = "txtalmacencod";
            this.txtalmacencod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtalmacencod.Size = new System.Drawing.Size(34, 20);
            this.txtalmacencod.TabIndex = 10;
            this.txtalmacencod.TextChanged += new System.EventHandler(this.txtalmacencod_TextChanged);
            this.txtalmacencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtalmacencod_KeyDown);
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(339, 23);
            this.txtproductodesc.MenuManager = this.barManager1;
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Size = new System.Drawing.Size(396, 20);
            this.txtproductodesc.TabIndex = 6;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(255, 23);
            this.txtproductocod.MenuManager = this.barManager1;
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproductocod.Size = new System.Drawing.Size(78, 20);
            this.txtproductocod.TabIndex = 5;
            this.txtproductocod.TextChanged += new System.EventHandler(this.txtproductocod_TextChanged);
            this.txtproductocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproductocod_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(471, 70);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(58, 13);
            this.labelControl16.TabIndex = 14;
            this.labelControl16.Text = "Costo. Unit.";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(312, 69);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(47, 13);
            this.labelControl15.TabIndex = 12;
            this.labelControl15.Text = "Cantidad:";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(19, 69);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(44, 13);
            this.labelControl14.TabIndex = 9;
            this.labelControl14.Text = "Almacen:";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(207, 26);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(47, 13);
            this.labelControl13.TabIndex = 4;
            this.labelControl13.Text = "Producto:";
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(104, 23);
            this.txttipobsadesc.MenuManager = this.barManager1;
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(100, 20);
            this.txttipobsadesc.TabIndex = 3;
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(69, 23);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipobsacod.Size = new System.Drawing.Size(34, 20);
            this.txttipobsacod.TabIndex = 2;
            this.txttipobsacod.TextChanged += new System.EventHandler(this.txttipobsacod_TextChanged);
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtfechavencimiento);
            this.groupBox1.Controls.Add(this.txtdias);
            this.groupBox1.Controls.Add(this.labelControl22);
            this.groupBox1.Controls.Add(this.labelControl21);
            this.groupBox1.Controls.Add(this.txtmonedadesc);
            this.groupBox1.Controls.Add(this.txtmonedacod);
            this.groupBox1.Controls.Add(this.txttipocambiovalor);
            this.groupBox1.Controls.Add(this.labelControl9);
            this.groupBox1.Controls.Add(this.txtfechadoc);
            this.groupBox1.Controls.Add(this.txttipocambiodesc);
            this.groupBox1.Controls.Add(this.labelControl8);
            this.groupBox1.Controls.Add(this.txttipocambiocod);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Controls.Add(this.labelControl12);
            this.groupBox1.Controls.Add(this.txtcondicioncod);
            this.groupBox1.Controls.Add(this.txtcondiciondesc);
            this.groupBox1.Location = new System.Drawing.Point(0, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 75);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // txtfechavencimiento
            // 
            this.txtfechavencimiento.Enabled = false;
            this.txtfechavencimiento.EnterMoveNextControl = true;
            this.txtfechavencimiento.Location = new System.Drawing.Point(429, 16);
            this.txtfechavencimiento.MenuManager = this.barManager1;
            this.txtfechavencimiento.Name = "txtfechavencimiento";
            this.txtfechavencimiento.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechavencimiento.Properties.Mask.BeepOnError = true;
            this.txtfechavencimiento.Properties.Mask.EditMask = "d";
            this.txtfechavencimiento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechavencimiento.Properties.Mask.SaveLiteral = false;
            this.txtfechavencimiento.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechavencimiento.Properties.MaxLength = 10;
            this.txtfechavencimiento.Size = new System.Drawing.Size(78, 20);
            this.txtfechavencimiento.TabIndex = 8;
            // 
            // txtdias
            // 
            this.txtdias.Enabled = false;
            this.txtdias.EnterMoveNextControl = true;
            this.txtdias.Location = new System.Drawing.Point(339, 15);
            this.txtdias.MenuManager = this.barManager1;
            this.txtdias.Name = "txtdias";
            this.txtdias.Properties.Mask.EditMask = "n0";
            this.txtdias.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtdias.Size = new System.Drawing.Size(32, 20);
            this.txtdias.TabIndex = 6;
            this.txtdias.TextChanged += new System.EventHandler(this.txtdias_TextChanged);
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(381, 18);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(42, 13);
            this.labelControl22.TabIndex = 7;
            this.labelControl22.Text = "Fecha V.";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(313, 18);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(24, 13);
            this.labelControl21.TabIndex = 5;
            this.labelControl21.Text = "Dias:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txttotal);
            this.groupBox2.Controls.Add(this.txtigv);
            this.groupBox2.Controls.Add(this.txtimporteigv);
            this.groupBox2.Controls.Add(this.txtsubtotal);
            this.groupBox2.Controls.Add(this.labelControl20);
            this.groupBox2.Controls.Add(this.labelControl19);
            this.groupBox2.Controls.Add(this.labelControl18);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(519, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(207, 77);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // txttotal
            // 
            this.txttotal.EditValue = "0.00";
            this.txttotal.Location = new System.Drawing.Point(101, 54);
            this.txttotal.MenuManager = this.barManager1;
            this.txttotal.Name = "txttotal";
            this.txttotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txttotal.Size = new System.Drawing.Size(100, 20);
            this.txttotal.TabIndex = 6;
            // 
            // txtigv
            // 
            this.txtigv.EditValue = "0.00";
            this.txtigv.Location = new System.Drawing.Point(60, 34);
            this.txtigv.MenuManager = this.barManager1;
            this.txtigv.Name = "txtigv";
            this.txtigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtigv.Size = new System.Drawing.Size(39, 20);
            this.txtigv.TabIndex = 5;
            // 
            // txtimporteigv
            // 
            this.txtimporteigv.EditValue = "0.00";
            this.txtimporteigv.Location = new System.Drawing.Point(101, 33);
            this.txtimporteigv.MenuManager = this.barManager1;
            this.txtimporteigv.Name = "txtimporteigv";
            this.txtimporteigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimporteigv.Size = new System.Drawing.Size(100, 20);
            this.txtimporteigv.TabIndex = 4;
            // 
            // txtsubtotal
            // 
            this.txtsubtotal.EditValue = "0.00";
            this.txtsubtotal.Location = new System.Drawing.Point(101, 12);
            this.txtsubtotal.MenuManager = this.barManager1;
            this.txtsubtotal.Name = "txtsubtotal";
            this.txtsubtotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtsubtotal.Size = new System.Drawing.Size(100, 20);
            this.txtsubtotal.TabIndex = 3;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(26, 56);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(28, 13);
            this.labelControl20.TabIndex = 2;
            this.labelControl20.Text = "Total:";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(34, 37);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(20, 13);
            this.labelControl19.TabIndex = 1;
            this.labelControl19.Text = "Igv:";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(5, 15);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(49, 13);
            this.labelControl18.TabIndex = 0;
            this.labelControl18.Text = "Sub Total:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnbuscar);
            this.groupBox3.Controls.Add(this.btnlistar);
            this.groupBox3.Controls.Add(this.btnquitar);
            this.groupBox3.Location = new System.Drawing.Point(728, 151);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(111, 77);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Requerimientos";
            this.groupBox3.Visible = false;
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(3, 20);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(30, 49);
            this.btnbuscar.TabIndex = 18;
            this.btnbuscar.Click += new System.EventHandler(this.btnbuscar_Click);
            // 
            // btnlistar
            // 
            this.btnlistar.Location = new System.Drawing.Point(75, 20);
            this.btnlistar.Name = "btnlistar";
            this.btnlistar.Size = new System.Drawing.Size(30, 49);
            this.btnlistar.TabIndex = 1;
            this.btnlistar.Click += new System.EventHandler(this.btnlistar_Click);
            // 
            // btnquitar
            // 
            this.btnquitar.Location = new System.Drawing.Point(39, 20);
            this.btnquitar.Name = "btnquitar";
            this.btnquitar.Size = new System.Drawing.Size(30, 49);
            this.btnquitar.TabIndex = 0;
            // 
            // frm_orden_sc_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(841, 560);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_orden_sc_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Orden de Servicio / Compra";
            this.Load += new System.EventHandler(this.frm_orden_sc_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtobservacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsabledesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproveedordesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproveedor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtordendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtordencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkafectoigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpreciounitario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdias.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsubtotal.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtresponsabledesc;
        private DevExpress.XtraEditors.TextEdit txtresponsable;
        private DevExpress.XtraEditors.TextEdit txtproveedordesc;
        private DevExpress.XtraEditors.TextEdit txtproveedor;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.TextEdit txtordendesc;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtobservacion;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.TextEdit txtalmacendesc;
        private DevExpress.XtraEditors.TextEdit txtalmacencod;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txttipobsadesc;
        private DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.TextEdit txtpreciounitario;
        private DevExpress.XtraEditors.TextEdit txtcantidad;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.TextEdit txtdescripcion;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.SimpleButton btnanadirdet;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.SimpleButton btnquitardet;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.TextEdit txttotal;
        private DevExpress.XtraEditors.TextEdit txtigv;
        private DevExpress.XtraEditors.TextEdit txtimporteigv;
        private DevExpress.XtraEditors.TextEdit txtsubtotal;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.CheckEdit chkafectoigv;
        public DevExpress.XtraEditors.TextEdit txtordencod;
        private DevExpress.XtraEditors.TextEdit txtfechavencimiento;
        private DevExpress.XtraEditors.TextEdit txtdias;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.SimpleButton btnbuscar;
        private DevExpress.XtraEditors.SimpleButton btnlistar;
        private DevExpress.XtraEditors.SimpleButton btnquitar;
    }
}
