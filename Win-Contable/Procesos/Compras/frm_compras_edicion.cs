﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable 
{
    public partial class frm_compras_edicion :  frm_fuente
    {
        public frm_compras_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;

       
        public void LimpiarDet2()
        {

            txttipoopecod.ResetText();
            txttipoopedesc.ResetText();

            txtcuenta.ResetText();
            txtcuentadesc.ResetText();
            txttipo.ResetText();
            txttipodesc.ResetText();
            txtimportecontable.ResetText();
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(txtfechadoc.Text) == true))
                {
                    if ((DateTime.Parse(txtfechadoc.Text).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(txtfechadoc.Text);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            txtigvporcentaje.Tag = Lista[0].Imd_Tasa;//0.18
                            txtigvporcentaje.Text = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));//18.00

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        public void ResetearImportes()
        {
            txtbaseimponible.Text = "0.00";
            txtigv.Text = "0.00";
            txtimporteisc.Text = "0.00";
            txtotrostribuimporte.Text = "0.00";
            txtvadquinograba.Text = "0.00";
            txtimportetotal.Text = "0.00";
            chkisc.Checked = false;
            chkotrostri.Checked = false;

        }

        public void LimpiarCab()
        {
            txtlibro.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();
       
            txtfechadoc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();

            txtidanalisis.ResetText();
            txtanalisisdesc.ResetText();
            txtfechavencimiento.ResetText();

      


        }

    
             public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
              
       
                Detalles_CONT.Clear();
                dgvdatosContable.DataSource = null;
            
                TraerLibro();
                BuscarMoneda_Inicial();
                ResetearImportes();
                
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }


        public override void CambiandoEstadoDetalle2()
        {
            if (EstadoDetalle2 == Estados.Nuevo)
            {
                HabilitarDetalles2();
                LimpiarDet2();
                txtcuenta.Tag = Detalles_CONT.Count + 1;

                btnanadirdetcon.Enabled = false;
                btneditardetcon.Enabled = false;
                btnquitardetcon.Enabled = false;
                btnnuevodetcon.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Ninguno)
            {
                LimpiarDet2();
                BloquearDetalles2();

                btnanadirdetcon.Enabled = false;
                btneditardetcon.Enabled = false;
                btnquitardetcon.Enabled = false;
                btnnuevodetcon.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Modificar)
            {

                HabilitarDetalles2();


                btnanadirdetcon.Enabled = true;
                btneditardetcon.Enabled = false;
                btnquitardetcon.Enabled = true;
                btnnuevodetcon.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Guardado)
            {

                LimpiarDet2();
                BloquearDetalles2();
                btnanadirdetcon.Focus();

                btnanadirdetcon.Enabled = false;
                btneditardetcon.Enabled = true;
                btnquitardetcon.Enabled = true;
                btnnuevodetcon.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Consulta)
            {
                HabilitarDetalles2();

                btnanadirdetcon.Enabled = false;
                btneditardetcon.Enabled = true;
                btnquitardetcon.Enabled = false;
                btnnuevodetcon.Enabled = false;

            }
            else if (EstadoDetalle2 == Estados.SoloLectura)
            {
                BloquearDetalles2();

                btnanadirdetcon.Enabled = false;
                btneditardetcon.Enabled = true;
                btnquitardetcon.Enabled = false;
                btnnuevodetcon.Enabled = false;
            }
        }


        public void BloquearDetalles2()
        {
            txttipoopecod.Enabled = false;
            txttipo.Enabled = false;
            txtcuenta.Enabled = false;
           txtimportecontable.Enabled = false;
          
       

        }


        public void HabilitarDetalles2()
        {
                     txttipoopecod.Enabled = true;
            txtcuenta.Enabled = true;
            txttipo.Enabled = true;
            txtimportecontable.Enabled = true;


            txtcuenta.Focus();
        }


        public bool VerificarDetalle2()
        {
            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una cuenta");
                txtcuenta.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodesc.Text))
            {
               
                Accion.Advertencia("Debe ingresar un tipo Debe / Haber");
                txttipo.Focus();
                return false;
            }

   

            return true;
        }

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Compra(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    txtlibro.Tag = Generales[0].Ini_Compra;
                    txtlibro.Text = Generales[0].Ini_Compra_Desc;
                }
                else
                {
                    Accion.Advertencia("Debe configurar un libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
               
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcuenta.EnterMoveNextControl = false;
                    txtcuenta.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void txttipo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipo.Text.Substring(txttipo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0002";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipo.Tag = Entidad.Id_General_Det;
                                txttipo.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesc.Text = Entidad.Gen_Descripcion_Det;
                                txttipo.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipo.Text) & string.IsNullOrEmpty(txttipodesc.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTipo()
        {
            try
            {
                txttipo.Text = Accion.Formato(txttipo.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0002",
                    Gen_Codigo_Interno = txttipo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipo.Text.Trim().ToUpper())
                        {
                            txttipo.Tag = T.Id_General_Det;
                            txttipo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesc.Text = T.Gen_Descripcion_Det;
                            txttipo.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipo.EnterMoveNextControl = false;
                    txttipo.ResetText();
                    txttipo.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        private void frm_compras_edicion_Load(object sender, EventArgs e)
        {
            //TraerLibro();
           
            //BloquearDetalles2();

            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                EstadoDetalle2 = Estados.Ninguno;
                //BuscarMoneda_Inicial();
                //txtlibro.EnterMoveNextControl = true;
               

                LimpiarCab();
                Detalles_CONT.Clear();
                dgvdatosContable.DataSource = null;
                TraerLibro();
                BuscarMoneda_Inicial();
                ResetearImportes();
                BloquearDetalles2();
                txtglosa.Select();

                int cant = Detalles_CONT.Count();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                TraerLibro();
                BloquearDetalles2();
                ListarModificar();
                //BuscarAnalisis_DETALLE_Modificar();
            }


        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = txtlibro.Tag.ToString();
                    Voucher = Enti.Id_Voucher;

                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;

                    
                    txtserie.Text = Enti.Ctb_Serie + "-" + Enti.Ctb_Numero;

                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));

                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtcondicioncod.Tag = Enti.Ctb_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ctb_Condicion_Interno;
                    txtcondiciondesc.Text = Enti.Ctb_Condicion_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;

                    MBase1 = Enti.Ctb_Base_Imponible;
                    MBase2 = Enti.Ctb_Base_Imponible2;
                    MBase3 = Enti.Ctb_Base_Imponible3;
                    MIgv1 = Enti.Ctb_Igv;
                    MIgv2 = Enti.Ctb_Igv2;
                    MIgv3 = Enti.Ctb_Igv3;
                    MValorAdqNoGrav = Enti.Ctb_No_Gravadas;
                    MIsc = Enti.Ctb_Isc_Importe;
                    MOtrosTributos = Enti.Ctb_Otros_Tributos_Importe;
                    MImporteTotal = Enti.Ctb_Importe_Total;

                    txtbaseimponible.Text = Convert.ToDecimal(MBase1 + MBase2 + MBase3).ToString("0.00");
                    txtigv.Text = Convert.ToDecimal(MIgv1 + MIgv2 + MIgv3).ToString("0.00");
                    txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");
                    txtotrostribuimporte.Text = Convert.ToDecimal(MOtrosTributos).ToString("0.00");
                    txtvadquinograba.Text = Convert.ToDecimal(MValorAdqNoGrav).ToString("0.00");
                    txtimportetotal.Text = Convert.ToDecimal(MImporteTotal).ToString("0.00");


                    //chkAfecto.Checked = Enti.Ctb_Afecto_RE;
                    //txttiporegimencod.Tag = Enti.Ctb_Afecto_Tipo;
                    //txttiporegimencod.Text = Enti.Ctb_Afecto_Tipo_cod_Interno;
                    //txttiporegimendesc.Text = Enti.Ctb_Afecto_Tipo_Descripcion;
                    //txtregtipodoccod.Text = Enti.Ctb_Afecto_Tipo_Doc;
                    //txtregtipodocdesc.Text = Enti.Ctb_Afecto_Tipo_Doc_DEsc;
                    //txtregimenserie.Text = Enti.Ctb_Afecto_Serie;
                    //txtregimennumero.Text = Enti.Ctb_Afecto_Numero;
                    //txtporcentaje.Text = Convert.ToString(Enti.Ctb_Afecto_Porcentaje);
                    //txtmonto.Text = Convert.ToString(Enti.Ctb_Afecto_Monto);
                    //txtfechadetrac.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Afecto_Fecha);


                    txtidanalisis.Text = Enti.Ctb_Analisis;
                    txtanalisisdesc.Text = Enti.Ctb_Analisis_Desc;

                    if (String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/0001" || String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/1900")
                    {
                        txtfechavencimiento.Text = "";
                    }
                    else

                    { 
                    

                        txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Vencimiento);
                    }
                 
       

                      //listar cnotable
                    Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                    dgvdatosContable.DataSource = null;
                    Detalles_CONT = log_det_con.Listar_Det(Enti);

                    if (Detalles_CONT.Count > 0)
                    {
                       
                        
                    decimal SumaDebeNac=0,SumaDebeExt=0,SumaHaberNac=0,SumaHaberExt=0;

                        foreach (Entidad_Movimiento_Cab Asi in Detalles_CONT)
                        {
                            if ((Asi.Cta_Destino != true))
                            {
                                SumaDebeNac += Asi.Ctb_Importe_Debe;
                                SumaDebeExt  += Asi.Ctb_Importe_Debe_Extr;
                                SumaHaberNac  += Asi.Ctb_Importe_Haber;
                                SumaHaberExt  += Asi.Ctb_Importe_Haber_Extr;
                            }

                    
                        }

                        dgvdatosContable.DataSource = Detalles_CONT;

                        TxtDMN.Text = String.Format("{0:0,0.00}", SumaDebeNac);// Format(SumaDebeNac, );
                        TxtDME.Text = String.Format("{0:0,0.00}", SumaDebeExt);
                        TxtHMN.Text = String.Format("{0:0,0.00}", SumaHaberNac);
                        TxtHME.Text = String.Format("{0:0,0.00}", SumaHaberExt);

                    }



                    //Nota debito credito
                    Lista_Doc_ref_Lista.Clear();
                    Logica_Movimiento_Cab Log_Doc_ref = new Logica_Movimiento_Cab();
                    Lista_Doc_ref_Lista = Log_Doc_ref.Listar_Doc_Ref(Enti);

                    //Ordenes
                    Detalles_Orden_Compra.Clear();
                     Logica_Movimiento_Cab Log_Doc_Orden = new Logica_Movimiento_Cab();
                    Detalles_Orden_Compra = Log_Doc_ref.Listar_Doc_Ref_Orden(Enti);


                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }


        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public bool Es_moneda_nac;

        public void BuscarMoneda_Inicial()
        {
            try
            {
               
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = "1"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == "1")
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                          
                        }

                    }
                    VerificarMoneda();
                    //txtglosa.Focus();
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Tag = Entidad.Id_Moneda;

                                txtmonedacod.Text = Entidad.Id_Sunat;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                                VerificarMoneda();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

      



        private void txtcondicioncod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcondicioncod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcondicioncod.Text.Substring(txtcondicioncod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0003";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcondicioncod.Tag = Entidad.Id_General_Det;
                                txtcondicioncod.Text = Entidad.Gen_Codigo_Interno;
                                txtcondiciondesc.Text = Entidad.Gen_Descripcion_Det;
                                //verificarContado();
                                txtcondicioncod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcondicioncod.Text) & string.IsNullOrEmpty(txtcondiciondesc.Text))
                    {
                        BuscarCondicion();
                        //verificarContado();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCondicion()
        {
            try
            {
                txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = txtcondicioncod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtcondicioncod.Text.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;
                            txtcondicioncod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }





        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdni.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1.000";

                }
                else
                {
                    //if (string.IsNullOrEmpty(txttipocambiocod.Text))
                    //{
                    //    txttipocambiocod.Enabled = true;
                    //    txttipocambiocod.ResetText();
                    //    txttipocambiodesc.ResetText();
                    //    txttipocambiovalor.ResetText();
                    //}
                    Traer_Venta_Publicacion();

                }
            }
        }


        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion ())
                    {

                      f.txtnumero.Text=txtrucdni.Text ;
                       
                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdni.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        txtrucdni.EnterMoveNextControl = true;
                                    }
                                }

                            }else
                            {
                                txtrucdni.EnterMoveNextControl = false;
                                txtrucdni.ResetText();
                                txtrucdni.Focus();
                            }
                        }
                    }
            
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

         List<Entidad_Movimiento_Cab> Detalles_CONT = new List<Entidad_Movimiento_Cab>();
         List<Entidad_Movimiento_Cab> Detalles_Orden_Compra= new List<Entidad_Movimiento_Cab>();


        public bool VerificarNuevoDet()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }

              if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcondiciondesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una condicion");
                txtcondicioncod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtanalisisdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una tipo de analisis");
                txtidanalisis.Focus();
                return false;
            }
            
            return true;
        }



        decimal Total_Adm, Base_Adm,Igv_Adm;
         public void Calcular_Total(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

           decimal Total=0;

            foreach (Entidad_Movimiento_Cab It in Detalles_CONT)
            {
                //decimal Tot;

              decimal  Tot = It.Adm_Total;
                Total += Tot;
            }
            //if (Es_moneda_nac == true)
            //{
            Total_Adm = Total;
            Base_Adm = Math.Round(Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)),4);
            Igv_Adm = Math.Round(Base_Adm * Convert.ToDecimal(txtigvporcentaje.Tag),4);
   

        }


        public void UpdateGrilla02()
        {
            dgvdatosContable.DataSource = null;
            
            if (Detalles_CONT.Count > 0)
            {
                decimal SumaDebeNac = 0, SumaDebeExt = 0, SumaHaberNac = 0, SumaHaberExt = 0;

                foreach (Entidad_Movimiento_Cab Asi in Detalles_CONT)
                {
                    if ((Asi.Cta_Destino != true))
                    {
                        SumaDebeNac += Asi.Ctb_Importe_Debe;
                        SumaDebeExt += Asi.Ctb_Importe_Debe_Extr;
                        SumaHaberNac += Asi.Ctb_Importe_Haber;
                        SumaHaberExt += Asi.Ctb_Importe_Haber_Extr;
                    }


                }

                dgvdatosContable.DataSource = Detalles_CONT;

                TxtDMN.Text = String.Format("{0:0,0.00}", SumaDebeNac);// Format(SumaDebeNac, );
                TxtDME.Text = String.Format("{0:0,0.00}", SumaDebeExt);
                TxtHMN.Text = String.Format("{0:0,0.00}", SumaHaberNac);
                TxtHME.Text = String.Format("{0:0,0.00}", SumaHaberExt);


            }
        }


        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles_CONT.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    EstadoDetalle2 = Estados.Ninguno;
                    Entidad = Detalles_CONT[gridView1.GetFocusedDataSourceRowIndex()];

                    txtcuenta.Tag = Entidad.Id_Item;

                    txtcuenta.Text = Entidad.Ctb_Cuenta;
                    txtcuentadesc.Text = Entidad.Ctb_Cuenta_Desc;

                    txttipoopecod.Tag = Entidad.Ctb_Operacion_Cod;
                    txttipoopecod.Text = Entidad.Ctb_Operacion_Cod_Interno;
                    txttipoopedesc.Text = Entidad.Ctb_Operacion_Desc;


                    txttipo.Tag = Entidad.Ctb_Tipo_DH;
                    txttipo.Text = Entidad.CCtb_Tipo_DH_Interno;
                    txttipodesc.Text = Entidad.Ctb_Tipo_DH_Desc;



                    txtrucdni.Text = Entidad.Ctb_Ruc_dni_det;

                    if (Entidad.Ctb_Tipo_DH == "0004")
                    {
                        if (Es_moneda_nac == true)
                        {
                            txtimportecontable.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe).ToString();
                        }
                        else
                        {
                            txtimportecontable.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Debe_Extr).ToString();
                        }


                    }
                    else if (Entidad.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            txtimportecontable.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber).ToString();
                        }
                        else
                        {
                            txtimportecontable.Text = Convert.ToDecimal(Entidad.Ctb_Importe_Haber_Extr).ToString();
                        }


                    }



                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle2 = Estados.SoloLectura;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle2 = Estados.SoloLectura;


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;

        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Movimiento_Cab Det in Detalles_CONT)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        public void Calcular_Quitar(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

            try
            {

                decimal Base1 = 0, Base2 = 0, Base3 = 0;
                decimal Igv1 = 0, Igv2 = 0, Igv3 = 0;
                decimal ValorAdqNoGrav = 0, Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;

       
                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();


                    //Ent=Detalles_CONT
                    Ent = Detalles_CONT[itenms.Id_Item - 1];

                    Ent.Id_Item = itenms.Id_Item;

                    Ent.Ctb_Cuenta = itenms.Ctb_Cuenta;
                    Ent.Ctb_Cuenta_Desc = itenms.Ctb_Cuenta_Desc;
                    Ent.Ctb_Operacion_Cod = itenms.Ctb_Operacion_Cod;
                    Ent.Ctb_Operacion_Cod_Interno = itenms.Ctb_Operacion_Cod_Interno;
                    Ent.Ctb_Operacion_Desc = itenms.Ctb_Operacion_Desc;
                    Ent.Ctb_Tipo_DH = itenms.Ctb_Tipo_DH;
                    Ent.CCtb_Tipo_DH_Interno = itenms.CCtb_Tipo_DH_Interno;
                    Ent.Ctb_Tipo_DH_Desc = itenms.Ctb_Tipo_DH_Desc;

                    Ent.Ctb_Fecha_Mov_det = itenms.Ctb_Fecha_Mov_det;//Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent_det = "P";
                    Ent.Ctb_Ruc_dni_det = itenms.Ctb_Ruc_dni_det;//txtrucdni.Text.ToString().Trim();
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;////txttipodoc.Text;
                    Ent.Ctb_moneda_cod_det = itenms.Ctb_moneda_cod_det;//txtmonedacod.Text;
                    Ent.Ctb_Tipo_Cambio_Cod_Det = itenms.Ctb_Tipo_Cambio_Cod_Det;// txttipocambiocod.Text;
                    Ent.Ctb_Tipo_Cambio_Desc_Det = itenms.Ctb_Tipo_Cambio_Desc_Det;// txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor_Det = itenms.Ctb_Tipo_Cambio_Valor_Det;// Convert.ToDecimal(txttipocambiovalor.Text);
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;// txttipodoc.Text;
                    Ent.Ctb_Serie_det = itenms.Ctb_Serie_det;// txtserie.Text;
                    Ent.Ctb_Numero_det = itenms.Ctb_Numero_det;//txtnumero.Text;



                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible -= Math.Abs(Base_Adm);//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)) ;
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible -= Math.Abs(Base_Adm);//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);


                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible -= Math.Abs(Base_Adm);//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)); 
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible -= Math.Abs(Base_Adm);//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0021"))//IGV 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(itenms.Ctb_Importe_Debe);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                            }
                            else
                            {
                                Ent.Ctb_Igv -= Igv_Adm;// Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Debe_Extr);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                        }
                        else
                        {
                            Ent.Ctb_Igv -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber_Extr);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible2 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);


                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible2 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe_Extr);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible2 -= Base_Adm;//Total_Adm /( Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible2 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0025"))//IGV 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv2 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }
                            else
                            {
                                Ent.Ctb_Igv2 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe_Extr);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv2 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }
                        else
                        {
                            Ent.Ctb_Igv2 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber_Extr);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible3 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible3 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe_Extr);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible3 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible3 -= Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber_Extr);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0036"))//IGV 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv3 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }
                            else
                            {
                                Ent.Ctb_Igv3 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe_Extr);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv3 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }
                        else
                        {
                            Ent.Ctb_Igv3 -= Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber_Extr);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0037"))//VALOR DE ADQUISICION NO GRABADA
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_No_Gravadas -= Total_Adm;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe);
                                ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }
                            else
                            {
                                Ent.Ctb_No_Gravadas -= Total_Adm;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe_Extr);
                                ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_No_Gravadas -= Total_Adm;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }
                        else
                        {
                            Ent.Ctb_No_Gravadas -= Total_Adm;// (Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber_Extr);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0039"))//OTROS TRIBUTOS Y CARGOS
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Otros_Tributos_Importe += Total_Adm;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Otros_Tributos_Importe -= Total_Adm;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe_Extr);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Otros_Tributos_Importe -= Total_Adm;//(Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Otros_Tributos_Importe -= Total_Adm;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber_Extr);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0038"))//IMPUESTO SELECTIVO AL CONSUMO
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Isc_Importe -= Total_Adm;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Isc_Importe -= Total_Adm;// (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe_Extr);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Isc_Importe -= Total_Adm;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Isc_Importe -= Total_Adm; //(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber_Extr);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0040"))//IMPORTE TOTAL
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total -= Total_Adm;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total -= Total_Adm;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total -= Total_Adm;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total -= Total_Adm;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }

                    }

                    ///////////////////////
                    //////////////////////
                    //////////////////////
                    //////////////////////
                    ///////PAR EL DEBE Y HABER 

                    if (Ent.Ctb_Tipo_DH == "0004")
                    {
                        //if (Es_moneda_nac == true)
                        //{

                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            Ent.Ctb_Importe_Debe = Base1;
                            Ent.Ctb_Importe_Debe_Extr = Base1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            Ent.Ctb_Importe_Debe = Igv1;
                            Ent.Ctb_Importe_Debe_Extr = Igv1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            Ent.Ctb_Importe_Debe = Base2;
                            Ent.Ctb_Importe_Debe_Extr = Base2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            Ent.Ctb_Importe_Debe = Igv2;
                            Ent.Ctb_Importe_Debe_Extr = Igv2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            Ent.Ctb_Importe_Debe = Base3;
                            Ent.Ctb_Importe_Debe_Extr = Base3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            Ent.Ctb_Importe_Debe = Igv3;
                            Ent.Ctb_Importe_Debe_Extr = Igv3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            Ent.Ctb_Importe_Debe = ValorAdqNoGrav;
                            Ent.Ctb_Importe_Debe_Extr = ValorAdqNoGrav * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            Ent.Ctb_Importe_Debe = Isc;
                            Ent.Ctb_Importe_Debe_Extr = Isc * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            Ent.Ctb_Importe_Debe = OtrosTributos;
                            Ent.Ctb_Importe_Debe_Extr = OtrosTributos * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            Ent.Ctb_Importe_Debe = ImporteTotal;
                            Ent.Ctb_Importe_Debe_Extr = ImporteTotal * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        //}


                    }
                    else if (Ent.Ctb_Tipo_DH == "0005")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            Ent.Ctb_Importe_Haber = Base1;
                            Ent.Ctb_Importe_Haber_Extr = Base1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            Ent.Ctb_Importe_Haber = Igv1;
                            Ent.Ctb_Importe_Haber_Extr = Igv1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            Ent.Ctb_Importe_Haber = Base2;
                            Ent.Ctb_Importe_Haber_Extr = Base2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            Ent.Ctb_Importe_Haber = Igv2;
                            Ent.Ctb_Importe_Haber_Extr = Igv2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            Ent.Ctb_Importe_Haber = Base3;
                            Ent.Ctb_Importe_Haber_Extr = Base3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            Ent.Ctb_Importe_Haber = Igv3;
                            Ent.Ctb_Importe_Haber_Extr = Igv3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            Ent.Ctb_Importe_Haber = ValorAdqNoGrav;
                            Ent.Ctb_Importe_Haber_Extr = ValorAdqNoGrav * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            Ent.Ctb_Importe_Haber = Isc;
                            Ent.Ctb_Importe_Haber_Extr = Isc * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            Ent.Ctb_Importe_Haber = OtrosTributos;
                            Ent.Ctb_Importe_Haber_Extr = OtrosTributos * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            Ent.Ctb_Importe_Haber = ImporteTotal;
                            Ent.Ctb_Importe_Haber_Extr = ImporteTotal * Ent.Ctb_Tipo_Cambio_Valor;
                        }


                    }


                    Detalles_CONT.Add(Ent);
                    //Detalles_CONT[Convert.ToInt32(itenms.Id_Item) - 1] = Ent;

                }

                //UpdateGrilla03();

                txtbaseimponible.Text = Convert.ToDecimal(Base1 + Base2 + Base3).ToString("0.00");
                txtigv.Text = Convert.ToDecimal(Igv1 + Igv2 + Igv3).ToString("0.00");
                txtimporteisc.Text = Convert.ToDecimal(Isc).ToString("0.00");
                txtotrostribuimporte.Text = Convert.ToDecimal(OtrosTributos).ToString("0.00");
                txtvadquinograba.Text = Convert.ToDecimal(ValorAdqNoGrav).ToString("0.00");
                txtimportetotal.Text = Convert.ToDecimal(ImporteTotal).ToString("0.00");

                MBase1 = Base1;
                MBase2 = Base2;
                MBase3 = Base3;
                MIgv1 = Igv1;
                MIgv2 = Igv2;
                MIgv3 = Igv3;
                MValorAdqNoGrav = ValorAdqNoGrav;
                MIsc = Isc;
                MOtrosTributos = OtrosTributos;
                MImporteTotal = ImporteTotal;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Id_Periodo;
                    Ent.Id_Libro = txtlibro.Tag.ToString();
                    Ent.Id_Voucher = Voucher;

                    Ent.Ctb_Glosa = txtglosa.Text;
                    Ent.Ctb_Tipo_Doc = txttipodoc.Tag.ToString();

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));
                   
                    Ent.Ctb_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Ctb_Numero= Accion.Formato(Datos[1].Trim(), 8).ToString();

                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent = "P";
                    Ent.Ctb_Ruc_dni = txtrucdni.Text;
                    Ent.Ctb_Condicion_Cod = txtcondicioncod.Tag.ToString();
                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;

                    Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                    Ent.Ctb_Base_Imponible = MBase1;
                    Ent.Ctb_Base_Imponible2 = MBase2;
                    Ent.Ctb_Base_Imponible3 = MBase3;

                    Ent.Ctb_Igv = MIgv1;
                    Ent.Ctb_Igv2 = MIgv2;
                    Ent.Ctb_Igv3 = MIgv3;

                    Ent.Ctb_Isc = chkisc.Checked;
                    Ent.Ctb_Isc_Importe = MIsc;
                    Ent.Ctb_Otros_Tributos = chkotrostri.Checked;
                    Ent.Ctb_Otros_Tributos_Importe = MOtrosTributos;
                    Ent.Ctb_No_Gravadas = MValorAdqNoGrav;
                    Ent.Ctb_Importe_Total = MImporteTotal;

                    //Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime(txtfechavencimiento.Text);

                    if (txtfechavencimiento.Text == "" || txtfechavencimiento.Text == "01/01/1900")
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime("01/01/1900");
                    }
                    else
                    {
                    Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime(txtfechavencimiento.Text);
                    }


                    Ent.Ctb_Afecto_RE = Ctb_Afecto_RE;
                    Ent.Ctb_Afecto_Tipo = Ctb_Afecto_Tipo;
                    Ent.Ctb_Afecto_Tipo_Doc = Ctb_Afecto_Tipo_Doc;
                    Ent.Ctb_Afecto_Serie = Ctb_Afecto_Serie;
                    Ent.Ctb_Afecto_Numero = Ctb_Afecto_Numero;
                    Ent.Ctb_Afecto_Fecha = Ctb_Afecto_Fecha;
                    Ent.Ctb_Afecto_Porcentaje = Ctb_Afecto_Porcentaje;
                    Ent.Ctb_Afecto_Monto = Ctb_Afecto_Monto;
                

                    Ent.Ctb_Tasa_IGV = Convert.ToDecimal(txtigvporcentaje.Text);
                    Ent.Ctb_Analisis = txtidanalisis.Text.Trim();

                    //detalle Contable
                    Ent.DetalleAsiento = Detalles_CONT;
                    //Detalle Administrativo
                    Ent.DetalleADM = Detalles_ADM;
                    //Detalle Documento Referencia (Nota Debito - Credito)
                    Ent.DetalleDoc_Ref = Lista_Doc_ref_Lista;
                    // Detalle Orden de Compra.
                    Ent.DetalleDoc_Orden_CS = Detalles_Orden_Compra;

                    //0020    01  BASE IMPONIBLE 01
                    //0021    02  IGV 01
                    //0022    03  BASE IMPONIBLE 02
                    //0025    04  IGV 02
                    //0026    05  BASE IMPONIBLE 03
                    //0036    06  IGV 03
                    //0037    07  VALOR DE ADQUISICION NO GRABADA
                    //0038    08  IMPUESTO SELECTIVO AL CONSUMO
                    //0039    09  OTROS TRIBUTOS Y CARGOS
                    //0040    10  IMPORTE TOTAL

                    //Calcular(Detalles, Ent);

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {
                           
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet2();

                            Detalles_ADM.Clear();
                            Detalles_CONT.Clear();
                        
                            Detalles_Orden_Compra.Clear();
                            Lista_Doc_ref_Lista.Clear();

                            dgvdatosContable.DataSource = null;
                            BloquearDetalles2();
                            TraerLibro();
                            BuscarMoneda_Inicial();
                            ResetearImportes();
                            txtglosa.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcondiciondesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una condicion");
                txtcondicioncod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtanalisisdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una tipo de analisis");
                txtidanalisis.Focus();
                return false;
            }

            if (Detalles_CONT.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

            //MessageBox.Show(Convert.ToString(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Debe"].SummaryItem.SummaryValue)));
            if (Math.Round(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Debe"].SummaryItem.SummaryValue),2) != Math.Round(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Haber"].SummaryItem.SummaryValue),2))
            {
                Accion.Advertencia("Los Montos Totales del Debe y el Haber no son iguales");
                return false;
            }


            return true;
        }





        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();
            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            try
            {
                Lista = log.Listar(Ent);
                //if (Lista.Count > 0)
                //{
                //    dgvdatos.DataSource = Lista;
                //}
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        decimal MBase1 = 0, MBase2 = 0, MBase3 = 0;
        decimal MIgv1 = 0, MIgv2 = 0, MIgv3 = 0;
        decimal MValorAdqNoGrav = 0, MIsc = 0;
        decimal MOtrosTributos = 0, MImporteTotal = 0;
        public void Calcular(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

            try
            {
                
                decimal Base1 = 0, Base2 = 0, Base3 = 0;
                decimal Igv1 = 0, Igv2 = 0, Igv3 = 0;
                decimal ValorAdqNoGrav = 0, Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;

   
                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT )
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();


                    //Ent = Detalles_CONT[itenms.Id_Item - 1];

                    Ent.Id_Item = itenms.Id_Item;

                    Ent.Ctb_Cuenta = itenms.Ctb_Cuenta;
                    Ent.Ctb_Cuenta_Desc = itenms.Ctb_Cuenta_Desc;
                    Ent.Ctb_Operacion_Cod = itenms.Ctb_Operacion_Cod;
                    Ent.Ctb_Operacion_Cod_Interno = itenms.Ctb_Operacion_Cod_Interno;
                    Ent.Ctb_Operacion_Desc = itenms.Ctb_Operacion_Desc;
                    Ent.Ctb_Tipo_DH = itenms.Ctb_Tipo_DH;
                    Ent.CCtb_Tipo_DH_Interno = itenms.CCtb_Tipo_DH_Interno;
                    Ent.Ctb_Tipo_DH_Desc = itenms.Ctb_Tipo_DH_Desc;

                    Ent.Ctb_Fecha_Mov_det = itenms.Ctb_Fecha_Mov_det;//Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent_det = "P";
                    Ent.Ctb_Ruc_dni_det = itenms.Ctb_Ruc_dni_det;//txtrucdni.Text.ToString().Trim();
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;////txttipodoc.Text;
                    Ent.Ctb_moneda_cod_det = itenms.Ctb_moneda_cod_det;//txtmonedacod.Text;
                    Ent.Ctb_Tipo_Cambio_Cod_Det = itenms.Ctb_Tipo_Cambio_Cod_Det;// txttipocambiocod.Text;
                    Ent.Ctb_Tipo_Cambio_Desc_Det = itenms.Ctb_Tipo_Cambio_Desc_Det;// txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor_Det = itenms.Ctb_Tipo_Cambio_Valor_Det;// Convert.ToDecimal(txttipocambiovalor.Text);
                 
                    Ent.Ctb_Serie_det = itenms.Ctb_Serie_det;// txtserie.Text;
                    Ent.Ctb_Numero_det = itenms.Ctb_Numero_det;//txtnumero.Text;



                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Debe;
                                Base1 += Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                           
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Debe_Extr;
                                Base1 += Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                  

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Haber;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)); 
                            Base1 += Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                       
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Haber_Extr;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base1 += Convert.ToDecimal(Ent.Ctb_Base_Imponible);
            
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0021"))//IGV 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv += itenms.Ctb_Importe_Debe;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(itenms.Ctb_Importe_Debe);
                                Igv1 += Convert.ToDecimal(Ent.Ctb_Igv);
                    
                            }
                            else
                            {
                                Ent.Ctb_Igv += itenms.Ctb_Importe_Debe_Extr;// Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Debe_Extr);
                                Igv1 += Convert.ToDecimal(Ent.Ctb_Igv);
                              
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv += itenms.Ctb_Importe_Haber;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber);
                            Igv1 += Convert.ToDecimal(Ent.Ctb_Igv);
                    
                        }
                        else
                        {
                            Ent.Ctb_Igv += itenms.Ctb_Importe_Haber_Extr;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber_Extr);
                            Igv1 += Convert.ToDecimal(Ent.Ctb_Igv);
                      
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible2 += itenms.Ctb_Importe_Debe;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe);
                                Base2 += Convert.ToDecimal(Ent.Ctb_Base_Imponible2);
                 

                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible2 += itenms.Ctb_Importe_Debe_Extr;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe_Extr);
                                Base2 += Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible2 += itenms.Ctb_Importe_Haber;//Total_Adm /( Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber);
                            Base2 += Convert.ToDecimal(Ent.Ctb_Base_Imponible2);
            
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible2 += itenms.Ctb_Importe_Haber_Extr;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base2 += Convert.ToDecimal(Ent.Ctb_Base_Imponible2);
                         
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0025"))//IGV 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv2 += itenms.Ctb_Importe_Debe;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe);
                                Igv2 += Convert.ToDecimal(Ent.Ctb_Igv2);
                            }
                            else
                            {
                                Ent.Ctb_Igv2 += itenms.Ctb_Importe_Debe_Extr;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe_Extr);
                                Igv2 += Convert.ToDecimal(Ent.Ctb_Igv2);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv2 += itenms.Ctb_Importe_Haber;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber);
                            Igv2 += Convert.ToDecimal(Ent.Ctb_Igv2);
                        }
                        else
                        {
                            Ent.Ctb_Igv2 += itenms.Ctb_Importe_Haber_Extr;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber_Extr);
                            Igv2 += Convert.ToDecimal(Ent.Ctb_Igv2);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible3 += itenms.Ctb_Importe_Debe;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe);
                                Base3 += Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible3 += itenms.Ctb_Importe_Debe_Extr;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe_Extr);
                                Base3 += Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible3 += itenms.Ctb_Importe_Haber;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber);
                            Base3 += Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible3 += itenms.Ctb_Importe_Haber_Extr;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber_Extr);
                            Base3 += Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0036"))//IGV 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv3 += itenms.Ctb_Importe_Debe;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe);
                                Igv3 += Convert.ToDecimal(Ent.Ctb_Igv3);
                            }
                            else
                            {
                                Ent.Ctb_Igv3 += itenms.Ctb_Importe_Debe_Extr;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe_Extr);
                                Igv3 += Convert.ToDecimal(Ent.Ctb_Igv3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv3 += itenms.Ctb_Importe_Haber;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber);
                            Igv3 += Convert.ToDecimal(Ent.Ctb_Igv3);
                        }
                        else
                        {
                            Ent.Ctb_Igv3 += itenms.Ctb_Importe_Haber_Extr;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber_Extr);
                            Igv3 += Convert.ToDecimal(Ent.Ctb_Igv3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0037"))//VALOR DE ADQUISICION NO GRABADA
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_No_Gravadas += itenms.Ctb_Importe_Debe;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe);
                                ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }
                            else
                            {
                                Ent.Ctb_No_Gravadas += itenms.Ctb_Importe_Debe_Extr;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe_Extr);
                                ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_No_Gravadas += itenms.Ctb_Importe_Haber;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }
                        else
                        {
                            Ent.Ctb_No_Gravadas += itenms.Ctb_Importe_Haber_Extr;// (Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber_Extr);
                            ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0039"))//OTROS TRIBUTOS Y CARGOS
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Otros_Tributos_Importe += itenms.Ctb_Importe_Debe;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe);
                                OtrosTributos += Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Otros_Tributos_Importe += itenms.Ctb_Importe_Debe_Extr;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe_Extr);
                                OtrosTributos += Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Otros_Tributos_Importe += itenms.Ctb_Importe_Haber;//(Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber);
                            OtrosTributos += Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Otros_Tributos_Importe += itenms.Ctb_Importe_Haber_Extr;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber_Extr);
                            OtrosTributos += Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0038"))//IMPUESTO SELECTIVO AL CONSUMO
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Isc_Importe += itenms.Ctb_Importe_Debe;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe);
                                Isc += Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Isc_Importe += itenms.Ctb_Importe_Debe_Extr;// (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe_Extr);
                                Isc += Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Isc_Importe += itenms.Ctb_Importe_Haber;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber);
                            Isc += Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Isc_Importe += itenms.Ctb_Importe_Haber_Extr; //(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber_Extr);
                            Isc += Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0040"))//IMPORTE TOTAL
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Debe;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal += Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Debe_Extr;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal += Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Haber;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal += Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Haber_Extr;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal += Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }

                    }

                    ///////////////////////
                    //////////////////////
                    //////////////////////
                    //////////////////////
                    ///////PAR EL DEBE Y HABER 

                    if (Ent.Ctb_Tipo_DH == "0004")
                    {
                       
                            
                            if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                            {
                                    if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Base1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Base1 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Debe_Extr = Base1 ;
                            }
                              
                            }
                            if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                            {
                                if (Es_moneda_nac == true)
                            {
                              Ent.Ctb_Importe_Debe = Igv1 ;
                            }
                            else
                            {
                            Ent.Ctb_Importe_Debe = Igv1 * Convert.ToDecimal(txttipocambiovalor.Text); 
                            Ent.Ctb_Importe_Debe_Extr = Igv1 ;
                            }
                               
                            }
                            if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                            {

                                if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Base2;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Base2 * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = Base2 ;
                            }
                            
                            }
                            if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                            {
                                if (Es_moneda_nac == true)
                            {
                                    Ent.Ctb_Importe_Debe = Igv2;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Igv2 * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = Igv2 ;
                            }
                                
                            }
                            if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                            {
                                if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Base3;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Base3 * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = Base3 ;
                            }
                               
                            }
                            if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                            {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Igv3;
                            }else
                            {
                                Ent.Ctb_Importe_Debe = Igv3 * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = Igv3 ;
                            }
                    
                            }
                            if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                            {

                                if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = ValorAdqNoGrav;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = ValorAdqNoGrav * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = ValorAdqNoGrav ;

                            }
                                                     }
                            if (itenms.Ctb_Operacion_Cod == "0038")// isc
                            {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Isc;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Isc * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = Isc ;
                            }
           
                            }
                            if (itenms.Ctb_Operacion_Cod == "0039")// isc
                            {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = OtrosTributos;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = OtrosTributos * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = OtrosTributos ;

                            }
                                                       }
                            if (itenms.Ctb_Operacion_Cod == "0040")// isc
                            {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = ImporteTotal;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = ImporteTotal * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Debe_Extr = ImporteTotal ;
                            }

                            }
                        //}


                    }
                    else if (Ent.Ctb_Tipo_DH == "0005")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Base1;
                            }else
                            {
                            Ent.Ctb_Importe_Haber = Base1 * Convert.ToDecimal(txttipocambiovalor.Text); 
                            Ent.Ctb_Importe_Haber_Extr = Base1 ;
                            }
                
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Igv1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Igv1 * Convert.ToDecimal(txttipocambiovalor.Text); 
                                Ent.Ctb_Importe_Haber_Extr = Igv1 ;
                            }
        
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Base2;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Base2 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = Base2 ;
                            }
                      
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Igv2;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Igv2 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = Igv2 ;
                            }
                      
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Base3;
                            }
                            else
                            {
                               Ent.Ctb_Importe_Haber = Base3 * Convert.ToDecimal(txttipocambiovalor.Text) ;
                               Ent.Ctb_Importe_Haber_Extr = Base3 ;
                            }
         
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            if (Es_moneda_nac == true)
                            {
                                 Ent.Ctb_Importe_Haber = Igv3;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Igv3 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = Igv3 ;
                            }
                            
                      
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = ValorAdqNoGrav;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = ValorAdqNoGrav * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = ValorAdqNoGrav ;
                            }
                
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Isc;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Isc * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = Isc ;
                            }
                  
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = OtrosTributos;
                            }
                            else
                            {
                            Ent.Ctb_Importe_Haber = OtrosTributos * Convert.ToDecimal(txttipocambiovalor.Text);
                            Ent.Ctb_Importe_Haber_Extr = OtrosTributos ;
                            }
           
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = ImporteTotal;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = ImporteTotal * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = ImporteTotal ;
                            }
                 
                        }


                    }


                    //Detalles_CONT.Add(Ent);
                    //Detalles_CONT[Convert.ToInt32(itenms.Id_Item) - 1] = Ent;

                }

                UpdateGrilla02();

                txtbaseimponible.Text = Convert.ToDecimal(Base1 + Base2 + Base3).ToString("0.00");
                txtigv.Text = Convert.ToDecimal(Igv1 + Igv2 + Igv3).ToString("0.00");
                txtimporteisc.Text = Convert.ToDecimal(Isc).ToString("0.00");
                txtotrostribuimporte.Text = Convert.ToDecimal(OtrosTributos).ToString("0.00");
                txtvadquinograba.Text = Convert.ToDecimal(ValorAdqNoGrav).ToString("0.00");
                txtimportetotal.Text = Convert.ToDecimal(ImporteTotal).ToString("0.00");

                MBase1 = Base1;
                MBase2 = Base2;
                MBase3 = Base3;
                MIgv1 = Igv1;
                MIgv2 = Igv2;
                MIgv3 = Igv3;
                MValorAdqNoGrav = ValorAdqNoGrav;
                MIsc = Isc;
                MOtrosTributos = OtrosTributos;
                MImporteTotal = ImporteTotal;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        public void Calcular_Modificar(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

            try
            {
             

                decimal Base1 = 0, Base2 = 0, Base3 = 0;
                decimal Igv1 = 0, Igv2 = 0, Igv3 = 0;
                decimal ValorAdqNoGrav = 0, Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;

                //Detalles_CONT_Aux.Clear();

                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();

                    //MessageBox.Show(Convert.ToString(Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)));

                    //Ent=Detalles_CONT
                    Ent = Detalles_CONT[itenms.Id_Item - 1];

                    Ent.Id_Item = itenms.Id_Item;

                    Ent.Ctb_Cuenta = itenms.Ctb_Cuenta;
                    Ent.Ctb_Cuenta_Desc = itenms.Ctb_Cuenta_Desc;
                    Ent.Ctb_Operacion_Cod = itenms.Ctb_Operacion_Cod;
                    Ent.Ctb_Operacion_Cod_Interno = itenms.Ctb_Operacion_Cod_Interno;
                    Ent.Ctb_Operacion_Desc = itenms.Ctb_Operacion_Desc;
                    Ent.Ctb_Tipo_DH = itenms.Ctb_Tipo_DH;
                    Ent.CCtb_Tipo_DH_Interno = itenms.CCtb_Tipo_DH_Interno;
                    Ent.Ctb_Tipo_DH_Desc = itenms.Ctb_Tipo_DH_Desc;

                    Ent.Ctb_Fecha_Mov_det = itenms.Ctb_Fecha_Mov_det;//Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent_det = "P";
                    Ent.Ctb_Ruc_dni_det = itenms.Ctb_Ruc_dni_det;//txtrucdni.Text.ToString().Trim();
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;////txttipodoc.Text;
                    Ent.Ctb_moneda_cod_det = itenms.Ctb_moneda_cod_det;//txtmonedacod.Text;
                    Ent.Ctb_Tipo_Cambio_Cod_Det = itenms.Ctb_Tipo_Cambio_Cod_Det;// txttipocambiocod.Text;
                    Ent.Ctb_Tipo_Cambio_Desc_Det = itenms.Ctb_Tipo_Cambio_Desc_Det;// txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor_Det = itenms.Ctb_Tipo_Cambio_Valor_Det;// Convert.ToDecimal(txttipocambiovalor.Text);
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;// txttipodoc.Text;
                    Ent.Ctb_Serie_det = itenms.Ctb_Serie_det;// txtserie.Text;
                    Ent.Ctb_Numero_det = itenms.Ctb_Numero_det;//txtnumero.Text;



                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)) ;
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);


                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)); 
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0021"))//IGV 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(itenms.Ctb_Importe_Debe);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                            }
                            else
                            {
                                Ent.Ctb_Igv += Igv_Adm;// Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Debe_Extr);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                        }
                        else
                        {
                            Ent.Ctb_Igv += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber_Extr);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible2 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);


                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible2 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe_Extr);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible2 += Base_Adm;//Total_Adm /( Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible2 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0025"))//IGV 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv2 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }
                            else
                            {
                                Ent.Ctb_Igv2 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe_Extr);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv2 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }
                        else
                        {
                            Ent.Ctb_Igv2 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber_Extr);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible3 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible3 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe_Extr);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible3 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible3 += Base_Adm;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber_Extr);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0036"))//IGV 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv3 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }
                            else
                            {
                                Ent.Ctb_Igv3 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe_Extr);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv3 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }
                        else
                        {
                            Ent.Ctb_Igv3 += Igv_Adm;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber_Extr);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0037"))//VALOR DE ADQUISICION NO GRABADA
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_No_Gravadas += Total_Adm;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe);
                                ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }
                            else
                            {
                                Ent.Ctb_No_Gravadas += Total_Adm;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe_Extr);
                                ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_No_Gravadas += Total_Adm;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }
                        else
                        {
                            Ent.Ctb_No_Gravadas += Total_Adm;// (Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber_Extr);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0039"))//OTROS TRIBUTOS Y CARGOS
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Otros_Tributos_Importe += Total_Adm;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Otros_Tributos_Importe += Total_Adm;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe_Extr);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Otros_Tributos_Importe += Total_Adm;//(Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Otros_Tributos_Importe += Total_Adm;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber_Extr);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0038"))//IMPUESTO SELECTIVO AL CONSUMO
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Isc_Importe += Total_Adm;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Isc_Importe += Total_Adm;// (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe_Extr);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Isc_Importe += Total_Adm;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Isc_Importe += Total_Adm; //(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber_Extr);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0040"))//IMPORTE TOTAL
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total += Total_Adm;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total += Total_Adm;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total += Total_Adm;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total += Total_Adm;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }

                    }

                    ///////////////////////
                    //////////////////////
                    //////////////////////
                    //////////////////////
                    ///////PAR EL DEBE Y HABER 

                    if (Ent.Ctb_Tipo_DH == "0004")
                    {
                        //if (Es_moneda_nac == true)
                        //{

                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            Ent.Ctb_Importe_Debe = Base1;
                            Ent.Ctb_Importe_Debe_Extr = Base1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            Ent.Ctb_Importe_Debe = Igv1;
                            Ent.Ctb_Importe_Debe_Extr = Igv1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            Ent.Ctb_Importe_Debe = Base2;
                            Ent.Ctb_Importe_Debe_Extr = Base2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            Ent.Ctb_Importe_Debe = Igv2;
                            Ent.Ctb_Importe_Debe_Extr = Igv2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            Ent.Ctb_Importe_Debe = Base3;
                            Ent.Ctb_Importe_Debe_Extr = Base3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            Ent.Ctb_Importe_Debe = Igv3;
                            Ent.Ctb_Importe_Debe_Extr = Igv3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            Ent.Ctb_Importe_Debe = ValorAdqNoGrav;
                            Ent.Ctb_Importe_Debe_Extr = ValorAdqNoGrav * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            Ent.Ctb_Importe_Debe = Isc;
                            Ent.Ctb_Importe_Debe_Extr = Isc * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            Ent.Ctb_Importe_Debe = OtrosTributos;
                            Ent.Ctb_Importe_Debe_Extr = OtrosTributos * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            Ent.Ctb_Importe_Debe = ImporteTotal;
                            Ent.Ctb_Importe_Debe_Extr = ImporteTotal * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        //}


                    }
                    else if (Ent.Ctb_Tipo_DH == "0005")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            Ent.Ctb_Importe_Haber = Base1;
                            Ent.Ctb_Importe_Haber_Extr = Base1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            Ent.Ctb_Importe_Haber = Igv1;
                            Ent.Ctb_Importe_Haber_Extr = Igv1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            Ent.Ctb_Importe_Haber = Base2;
                            Ent.Ctb_Importe_Haber_Extr = Base2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            Ent.Ctb_Importe_Haber = Igv2;
                            Ent.Ctb_Importe_Haber_Extr = Igv2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            Ent.Ctb_Importe_Haber = Base3;
                            Ent.Ctb_Importe_Haber_Extr = Base3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            Ent.Ctb_Importe_Haber = Igv3;
                            Ent.Ctb_Importe_Haber_Extr = Igv3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            Ent.Ctb_Importe_Haber = ValorAdqNoGrav;
                            Ent.Ctb_Importe_Haber_Extr = ValorAdqNoGrav * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            Ent.Ctb_Importe_Haber = Isc;
                            Ent.Ctb_Importe_Haber_Extr = Isc * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            Ent.Ctb_Importe_Haber = OtrosTributos;
                            Ent.Ctb_Importe_Haber_Extr = OtrosTributos * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            Ent.Ctb_Importe_Haber = ImporteTotal;
                            Ent.Ctb_Importe_Haber_Extr = ImporteTotal * Ent.Ctb_Tipo_Cambio_Valor;
                        }


                    }


                    //Detalles_CONT_Aux.Add(Ent);
                    //Detalles_CONT[Convert.ToInt32(itenms.Id_Item) - 1] = Ent;

                }

                //UpdateGrilla03();

                txtbaseimponible.Text = Convert.ToDecimal(Base1 + Base2 + Base3).ToString("0.00");
                txtigv.Text = Convert.ToDecimal(Igv1 + Igv2 + Igv3).ToString("0.00");
                txtimporteisc.Text = Convert.ToDecimal(Isc).ToString("0.00");
                txtotrostribuimporte.Text = Convert.ToDecimal(OtrosTributos).ToString("0.00");
                txtvadquinograba.Text = Convert.ToDecimal(ValorAdqNoGrav).ToString("0.00");
                txtimportetotal.Text = Convert.ToDecimal(ImporteTotal).ToString("0.00");

                MBase1 = Base1;
                MBase2 = Base2;
                MBase3 = Base3;
                MIgv1 = Igv1;
                MIgv2 = Igv2;
                MIgv3 = Igv3;
                MValorAdqNoGrav = ValorAdqNoGrav;
                MIsc = Isc;
                MOtrosTributos = OtrosTributos;
                MImporteTotal = ImporteTotal;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {

            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txtserie_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtserie.Text))
                {
                    string Serie_Numero = txtserie.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        //txtserie.Text = Accion.Formato(txtserie.Text, 4);
                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtserie.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
   
        }

        private void txtcondicioncod_TextChanged(object sender, EventArgs e)
        {
            if (txtcondicioncod.Focus() == false)
            {
                txtcondiciondesc.ResetText();

            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacod.Focus() == false)
            {
                txtmonedadesc.ResetText();
                txttipocambiocod.ResetText();
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
                txtmonedacod.Focus();

            }
        }

        private void txtrucdni_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdni.Focus() == false)
            {
                txtentidad.ResetText();
            }
        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            if (txtcuenta.Focus() == false)
            {
                txtcuentadesc.ResetText();
            }
        }


        private void txttipoopecod_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {

        }

        private void txttipo_TextChanged(object sender, EventArgs e)
        {
            if (txttipo.Focus() == false)
            {
                txttipodesc.ResetText();
            }
        }

        public List<Entidad_Movimiento_Cab> Lista_Doc_ref_Lista = new List<Entidad_Movimiento_Cab>();

        private void btnbuscardocref_Click(object sender, EventArgs e)
        {
            using (frm_documento_vinculado f = new frm_documento_vinculado())
            {

                f.Libro = txtlibro.Tag.ToString();
                f.Libro_desc = txtlibro.Text.ToString();

                if (Lista_Doc_ref_Lista.Count > 0)
                {
                    f.Lista_Doc_ref_Aux = Lista_Doc_ref_Lista;
                }
                try
                {
                    if (f.ShowDialog() == DialogResult.OK)
                    {

                        Lista_Doc_ref_Lista = f.Lista_Doc_ref;
                    }
                    else
                    {
                        Lista_Doc_ref_Lista = f.Lista_Doc_ref;
                    }

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }



            }
        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }


        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocod.Text) == false)
            {
                if (IsDate(txtfechadoc.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocod.Text.Substring(txttipocambiocod.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadoc.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocod.Text = Entidad.Codigo;
                                    txttipocambiodesc.Text = Entidad.Nombre;
                                    txttipocambiovalor.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocod.Text == "OTR")
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalor.Enabled = false;
                                    }

                                    txttipocambiocod.EnterMoveNextControl = true;

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocod.Text) & string.IsNullOrEmpty(txttipocambiodesc.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadoc.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                       
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocod.Text = T.Codigo;
                                            txttipocambiodesc.Text = T.Nombre;
                                            txttipocambiovalor.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocod.Text == "OTR")
                                            {
                                                txttipocambiovalor.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalor.Enabled = false;
                                            }
                                            txttipocambiocod.EnterMoveNextControl = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadoc.Focus();

                }

            }
        }

        void Traer_Venta_Publicacion()
        {
            try
            {
                if (txtmonedacod.Text == "2")
                {
                    txttipocambiocod.Text = "VEP";
                    txttipocambiodesc.Text = "VENTA PUBLICACION";
                    txttipocambiovalor.Enabled = true;
                    txttipocambiocod.Enabled = false;
                    //txttipocambiovalor.Focus();
                }
                         
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtfechadoc_Leave(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txtfechadoc_TextChanged(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocod.Focus() == false)
            {
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
            }
        }

  
        private void btnnuevodetcon_Click(object sender, EventArgs e)
        {
            if (VerificarNuevoDet())
            {
                if (Detalles_CONT.Count != 0)
                {
                    EstadoDetalle2 = Estados.Nuevo;
                    HabilitarDetalles2();
                    LimpiarDet2();
                    txtcuenta.Tag = Detalles_CONT.Count + 1;

                    btnnuevodetcon.Enabled = false;
                    btneditardetcon.Enabled = false;
                    btnquitardetcon.Enabled = false;
                    btnanadirdetcon.Enabled = true;
                }
                else
                {
                EstadoDetalle2 = Estados.Nuevo;
                BuscarAnalisis_DETALLE();
                //HabilitarDetalles2();
                    BloquearDetalles2();
                    //btnnuevodetcon.Enabled = true;
                    //btneditardetcon.Enabled = true;
                    //btnquitardetcon.Enabled = true;
                    //btnanadirdetcon.Enabled = true;
                }
            }


        }

        public void Recalcular_Totales_Cabecera(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

            try
            {



                decimal Base1 = 0, Base2 = 0, Base3 = 0;
                decimal Igv1 = 0, Igv2 = 0, Igv3 = 0;
                decimal ValorAdqNoGrav = 0, Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;

                foreach (Entidad_Movimiento_Cab itenms in Detalles)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();

                    if ((itenms.Ctb_Operacion_Cod == "0020"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible += (itenms.Ctb_Importe_Debe);
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible += (itenms.Ctb_Importe_Debe_Extr);
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible += (itenms.Ctb_Importe_Haber);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible += (itenms.Ctb_Importe_Haber_Extr);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0021"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv += (itenms.Ctb_Importe_Debe);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                            }
                            else
                            {
                                Ent.Ctb_Igv += (itenms.Ctb_Importe_Debe_Extr);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv += (itenms.Ctb_Importe_Haber);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                        }
                        else
                        {
                            Ent.Ctb_Igv += (itenms.Ctb_Importe_Haber_Extr);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0022"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible2 += (itenms.Ctb_Importe_Debe);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible2 += (itenms.Ctb_Importe_Debe_Extr);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible2 += (itenms.Ctb_Importe_Haber);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible2 += (itenms.Ctb_Importe_Haber_Extr);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0025"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv2 = (Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }
                            else
                            {
                                Ent.Ctb_Igv2 = (Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe_Extr);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv2 = (Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }
                        else
                        {
                            Ent.Ctb_Igv2 = (Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber_Extr);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0026"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible3 = (Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible3 = (Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe_Extr);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible3 = (Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible3 = (Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber_Extr);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0036"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv3 = (Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }
                            else
                            {
                                Ent.Ctb_Igv3 = (Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe_Extr);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv3 = (Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }
                        else
                        {
                            Ent.Ctb_Igv3 = (Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber_Extr);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0037"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_No_Gravadas = (Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe);
                                ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }
                            else
                            {
                                Ent.Ctb_No_Gravadas  += (itenms.Ctb_Importe_Debe_Extr);
                                ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_No_Gravadas += (itenms.Ctb_Importe_Haber);
                            ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }
                        else
                        {
                            Ent.Ctb_No_Gravadas += (itenms.Ctb_Importe_Haber_Extr);
                            ValorAdqNoGrav += Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0039"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Otros_Tributos_Importe = (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Otros_Tributos_Importe = (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe_Extr);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Otros_Tributos_Importe = (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Otros_Tributos_Importe = (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber_Extr);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0038"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Isc_Importe = (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Isc_Importe = (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe_Extr);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Isc_Importe = (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Isc_Importe = (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber_Extr);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0040"))
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total = (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total = (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total = (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total = (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }

                    }

                }


                txtbaseimponible.Text = Convert.ToDecimal(Base1 + Base2 + Base3).ToString("0.00");
                txtigv.Text = Convert.ToDecimal(Igv1 + Igv2 + Igv3).ToString("0.00");
                txtimporteisc.Text = Convert.ToDecimal(Isc).ToString("0.00");
                txtotrostribuimporte.Text = Convert.ToDecimal(OtrosTributos).ToString("0.00");
                txtvadquinograba.Text = Convert.ToDecimal(ValorAdqNoGrav).ToString("0.00");
                txtimportetotal.Text = Convert.ToDecimal(ImporteTotal).ToString("0.00");

                MBase1 = Base1;
                MBase2 = Base2;
                MBase3 = Base3;
                MIgv1 = Igv1;
                MIgv2 = Igv2;
                MIgv3 = Igv3;
                MValorAdqNoGrav = ValorAdqNoGrav;
                MIsc = Isc;
                MOtrosTributos = OtrosTributos;
                MImporteTotal = ImporteTotal;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }


          void ResetarValoresContables()
        {

            try
            {

                decimal Base1 = 0, Base2 = 0, Base3 = 0;
                decimal Igv1 = 0, Igv2 = 0, Igv3 = 0;
                decimal ValorAdqNoGrav = 0, Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;

                //Detalles_CONT_Aux.Clear();


                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();


                    Ent = Detalles_CONT[itenms.Id_Item - 1];

                    Ent.Id_Item = itenms.Id_Item;

                    Ent.Ctb_Cuenta = itenms.Ctb_Cuenta;
                    Ent.Ctb_Cuenta_Desc = itenms.Ctb_Cuenta_Desc;
                    Ent.Ctb_Operacion_Cod = itenms.Ctb_Operacion_Cod;
                    Ent.Ctb_Operacion_Cod_Interno = itenms.Ctb_Operacion_Cod_Interno;
                    Ent.Ctb_Operacion_Desc = itenms.Ctb_Operacion_Desc;
                    Ent.Ctb_Tipo_DH = itenms.Ctb_Tipo_DH;
                    Ent.CCtb_Tipo_DH_Interno = itenms.CCtb_Tipo_DH_Interno;
                    Ent.Ctb_Tipo_DH_Desc = itenms.Ctb_Tipo_DH_Desc;

                    Ent.Ctb_Fecha_Mov_det = itenms.Ctb_Fecha_Mov_det;//Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent_det = "P";
                    Ent.Ctb_Ruc_dni_det = itenms.Ctb_Ruc_dni_det;//txtrucdni.Text.ToString().Trim();
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;////txttipodoc.Text;
                    Ent.Ctb_moneda_cod_det = itenms.Ctb_moneda_cod_det;//txtmonedacod.Text;
                    Ent.Ctb_Tipo_Cambio_Cod_Det = itenms.Ctb_Tipo_Cambio_Cod_Det;// txttipocambiocod.Text;
                    Ent.Ctb_Tipo_Cambio_Desc_Det = itenms.Ctb_Tipo_Cambio_Desc_Det;// txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor_Det = itenms.Ctb_Tipo_Cambio_Valor_Det;// Convert.ToDecimal(txttipocambiovalor.Text);
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;// txttipodoc.Text;
                    Ent.Ctb_Serie_det = itenms.Ctb_Serie_det;// txtserie.Text;
                    Ent.Ctb_Numero_det = itenms.Ctb_Numero_det;//txtnumero.Text;



                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)) ;
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);


                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1)); 
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0021"))//IGV 01
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(itenms.Ctb_Importe_Debe);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                            }
                            else
                            {
                                Ent.Ctb_Igv = 0;// Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Debe_Extr);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                        }
                        else
                        {
                            Ent.Ctb_Igv = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(itenms.Ctb_Importe_Haber_Extr);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible2 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);


                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible2 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Debe_Extr);
                                Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible2 = 0;//Total_Adm /( Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible2 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(itenms.Ctb_Importe_Haber_Extr);
                            Base2 = Convert.ToDecimal(Ent.Ctb_Base_Imponible2);

                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0025"))//IGV 02
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv2 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }
                            else
                            {
                                Ent.Ctb_Igv2 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Debe_Extr);
                                Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv2 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }
                        else
                        {
                            Ent.Ctb_Igv2 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag);//(Ent.Ctb_Igv2 + itenms.Ctb_Importe_Haber_Extr);
                            Igv2 = Convert.ToDecimal(Ent.Ctb_Igv2);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible3 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible3 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Debe_Extr);
                                Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible3 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible3 = 0;//Total_Adm / (Convert.ToDecimal(txtigvporcentaje.Tag) + Convert.ToDecimal(1));//(Ent.Ctb_Base_Imponible3 + itenms.Ctb_Importe_Haber_Extr);
                            Base3 = Convert.ToDecimal(Ent.Ctb_Base_Imponible3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0036"))//IGV 03
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv3 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }
                            else
                            {
                                Ent.Ctb_Igv3 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Debe_Extr);
                                Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv3 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }
                        else
                        {
                            Ent.Ctb_Igv3 = 0;//Total_Adm * Convert.ToDecimal(txtigvporcentaje.Tag); //(Ent.Ctb_Igv3 + itenms.Ctb_Importe_Haber_Extr);
                            Igv3 = Convert.ToDecimal(Ent.Ctb_Igv3);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0037"))//VALOR DE ADQUISICION NO GRABADA
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_No_Gravadas = 0;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe);
                                ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }
                            else
                            {
                                Ent.Ctb_No_Gravadas = 0;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Debe_Extr);
                                ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_No_Gravadas = 0;//(Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }
                        else
                        {
                            Ent.Ctb_No_Gravadas = 0;// (Ent.Ctb_No_Gravadas + itenms.Ctb_Importe_Haber_Extr);
                            ValorAdqNoGrav = Convert.ToDecimal(Ent.Ctb_No_Gravadas);
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0039"))//OTROS TRIBUTOS Y CARGOS
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Otros_Tributos_Importe = 0;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Otros_Tributos_Importe = 0;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Debe_Extr);
                                OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                                chkotrostri.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Otros_Tributos_Importe = 0;//(Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Otros_Tributos_Importe = 0;// (Ent.Ctb_Otros_Tributos_Importe + itenms.Ctb_Importe_Haber_Extr);
                            OtrosTributos = Convert.ToDecimal(Ent.Ctb_Otros_Tributos_Importe);
                            chkotrostri.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0038"))//IMPUESTO SELECTIVO AL CONSUMO
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Isc_Importe = 0;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }
                            else
                            {
                                Ent.Ctb_Isc_Importe = 0;// (Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Debe_Extr);
                                Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                                chkisc.Checked = true;
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Isc_Importe = 0;//(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }
                        else
                        {
                            Ent.Ctb_Isc_Importe = 0; //(Ent.Ctb_Isc_Importe + itenms.Ctb_Importe_Haber_Extr);
                            Isc = Convert.ToDecimal(Ent.Ctb_Isc_Importe);
                            chkisc.Checked = true;
                        }

                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0040"))//IMPORTE TOTAL
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total = 0;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total = 0;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }

                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total = 0;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total = 0;//(Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }

                    }

                    ///////////////////////
                    //////////////////////
                    //////////////////////
                    //////////////////////
                    ///////PAR EL DEBE Y HABER 

                    if (Ent.Ctb_Tipo_DH == "0004")
                    {
                

                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            Ent.Ctb_Importe_Debe = Base1;
                            Ent.Ctb_Importe_Debe_Extr = Base1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            Ent.Ctb_Importe_Debe = Igv1;
                            Ent.Ctb_Importe_Debe_Extr = Igv1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            Ent.Ctb_Importe_Debe = Base2;
                            Ent.Ctb_Importe_Debe_Extr = Base2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            Ent.Ctb_Importe_Debe = Igv2;
                            Ent.Ctb_Importe_Debe_Extr = Igv2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            Ent.Ctb_Importe_Debe = Base3;
                            Ent.Ctb_Importe_Debe_Extr = Base3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            Ent.Ctb_Importe_Debe = Igv3;
                            Ent.Ctb_Importe_Debe_Extr = Igv3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            Ent.Ctb_Importe_Debe = ValorAdqNoGrav;
                            Ent.Ctb_Importe_Debe_Extr = ValorAdqNoGrav * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            Ent.Ctb_Importe_Debe = Isc;
                            Ent.Ctb_Importe_Debe_Extr = Isc * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            Ent.Ctb_Importe_Debe = OtrosTributos;
                            Ent.Ctb_Importe_Debe_Extr = OtrosTributos * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            Ent.Ctb_Importe_Debe = ImporteTotal;
                            Ent.Ctb_Importe_Debe_Extr = ImporteTotal * Ent.Ctb_Tipo_Cambio_Valor;
                        }
           


                    }
                    else if (Ent.Ctb_Tipo_DH == "0005")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            Ent.Ctb_Importe_Haber = Base1;
                            Ent.Ctb_Importe_Haber_Extr = Base1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            Ent.Ctb_Importe_Haber = Igv1;
                            Ent.Ctb_Importe_Haber_Extr = Igv1 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0022")//base 2
                        {
                            Ent.Ctb_Importe_Haber = Base2;
                            Ent.Ctb_Importe_Haber_Extr = Base2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0025")//igv 2
                        {
                            Ent.Ctb_Importe_Haber = Igv2;
                            Ent.Ctb_Importe_Haber_Extr = Igv2 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0026")//base 3
                        {
                            Ent.Ctb_Importe_Haber = Base3;
                            Ent.Ctb_Importe_Haber_Extr = Base3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0036")//igv 3
                        {
                            Ent.Ctb_Importe_Haber = Igv3;
                            Ent.Ctb_Importe_Haber_Extr = Igv3 * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0037")// v a no grabada
                        {
                            Ent.Ctb_Importe_Haber = ValorAdqNoGrav;
                            Ent.Ctb_Importe_Haber_Extr = ValorAdqNoGrav * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0038")// isc
                        {
                            Ent.Ctb_Importe_Haber = Isc;
                            Ent.Ctb_Importe_Haber_Extr = Isc * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0039")// isc
                        {
                            Ent.Ctb_Importe_Haber = OtrosTributos;
                            Ent.Ctb_Importe_Haber_Extr = OtrosTributos * Ent.Ctb_Tipo_Cambio_Valor;
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// isc
                        {
                            Ent.Ctb_Importe_Haber = ImporteTotal;
                            Ent.Ctb_Importe_Haber_Extr = ImporteTotal * Ent.Ctb_Tipo_Cambio_Valor;
                        }


                    }


                }

                //UpdateGrilla03();

                txtbaseimponible.Text = Convert.ToDecimal(Base1 + Base2 + Base3).ToString("0.00");
                txtigv.Text = Convert.ToDecimal(Igv1 + Igv2 + Igv3).ToString("0.00");
                txtimporteisc.Text = Convert.ToDecimal(Isc).ToString("0.00");
                txtotrostribuimporte.Text = Convert.ToDecimal(OtrosTributos).ToString("0.00");
                txtvadquinograba.Text = Convert.ToDecimal(ValorAdqNoGrav).ToString("0.00");
                txtimportetotal.Text = Convert.ToDecimal(ImporteTotal).ToString("0.00");

                MBase1 = Base1;
                MBase2 = Base2;
                MBase3 = Base3;
                MIgv1 = Igv1;
                MIgv2 = Igv2;
                MIgv3 = Igv3;
                MValorAdqNoGrav = ValorAdqNoGrav;
                MIsc = Isc;
                MOtrosTributos = OtrosTributos;
                MImporteTotal = ImporteTotal;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }
        private void btneditardetcon_Click(object sender, EventArgs e)
        {
            EstadoDetalle2 = Estados.Modificar;
            txtimportecontable.Focus();
        }



        bool ExisteOrden(Entidad_Movimiento_Cab op)
        {
            foreach (Entidad_Movimiento_Cab Ent in Detalles_Orden_Compra)
            {
                if (((op.Id_Periodo_Ref == Ent.Id_Periodo_Ref) && ((op.Ord_tipo == Ent.Ord_tipo) && ((op.Ord_Folio == Ent.Ord_Folio) ))))
                {
                    return true;
                }

            }

            return false;
        }


        List<Entidad_Orden_C_S> Detalles_Orden_ = new List<Entidad_Orden_C_S>();
        private void btnbuscarorden_Click(object sender, EventArgs e)
        {

            try
            {
                if ((txtanalisisdesc.Text == ""))
                {
                    Accion.Advertencia("Debe seleccionar un Analisis");
                    txtidanalisis.Focus();
                }
                else
                {
                    using (frm_orden_cs_buscar f = new frm_orden_cs_buscar())
                    {
                        if (f.ShowDialog() == DialogResult.OK)
                        {
                            foreach (int Item in f.gridView1.GetSelectedRows())
                            {
                                
                                Entidad_Orden_C_S Ord = new Entidad_Orden_C_S();
                                Entidad_Movimiento_Cab Compra_Ord2 = new Entidad_Movimiento_Cab();
                                Ord = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex() ];

                                Compra_Ord2.Id_Empresa = Ord.Id_Empresa;
                                Compra_Ord2.Id_Anio_Ref = Ord.Id_Anio;
                                Compra_Ord2.Id_Periodo_Ref = Ord.Id_Periodo;
                                Compra_Ord2.Ord_tipo = Ord.Id_Tipo_Orden;
                                Compra_Ord2.Ord_Folio = Ord.Id_Movimiento;
                                Compra_Ord2.Ord_Origen = "OR";
                                Compra_Ord2.Id_Item = Detalles_Orden_Compra.Count + 1;

                                if (ExisteOrden(Compra_Ord2) == false)
                                {
                                    Detalles_Orden_Compra.Add(Compra_Ord2);
                                    //BUSCAMOS LOS DETALLES
                               

                                    Entidad_Orden_C_S Det = new Entidad_Orden_C_S();
                                    Det.Id_Empresa = Compra_Ord2.Id_Empresa;
                                    Det.Id_Anio = Compra_Ord2.Id_Anio_Ref;
                                    Det.Id_Periodo = Compra_Ord2.Id_Periodo_Ref;
                                    Det.Id_Tipo_Orden = Compra_Ord2.Ord_tipo;
                                    Det.Id_Movimiento = Compra_Ord2.Ord_Folio;

                                    Logica_Orden_C_S log = new Logica_Orden_C_S();

                                    Detalles_Orden_ = log.Listar_Det(Det);

                                    foreach (Entidad_Orden_C_S T in Detalles_Orden_)
                                    {
                                        Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();

                 
                                        Enti.Adm_Centro_CG = "";
                                        Enti.Adm_Centro_CG_Desc = "";

                                  
                                        Enti.Adm_Tipo_BSA = T.Ord_Tipo_BSA;
                                        Enti.Adm_Tipo_BSA_Interno = T.Ord_Tipo_BSA_Interno;
                                        Enti.Adm_Tipo_BSA_Desc = T.Ord_Tipo_BSA_Desc;

                                        Enti.Adm_Catalogo = T.Ord_Catalogo;
                                        Enti.Adm_Catalogo_Desc = T.Ord_Catalogo_desc;

                                        Enti.Adm_Almacen = T.Ord_Almacen;
                                        Enti.Adm_Almacen_desc = T.Ord_Almacen_desc;

                                        Enti.Adm_Cantidad = T.Ord_Cantidad;
                                        Enti.Adm_Valor_Unit =T.Ord_Precio_Unitario;

                                        Enti.Adm_Total = Enti.Adm_Cantidad * Enti.Adm_Valor_Unit;


                                    }

                                }
                            }




                        }
                        }
                    }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        
        }


        private void simpleButton3_Click(object sender, EventArgs e)
        {
            try
            {
            if (Detalles_Orden_Compra.Count > 0)
            {
                    using( frm_orden_cs_lista f = new frm_orden_cs_lista())
                    {
                        
                        f.buscar2(Detalles_Orden_Compra);
                        if (f.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("Noe existe ninguna orden");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void txttipoopecod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipoopecod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoopecod.Text.Substring(txttipoopecod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0007";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoopecod.Tag = Entidad.Id_General_Det;
                                txttipoopecod.Text = Entidad.Gen_Codigo_Interno;
                                txttipoopedesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipoopecod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoopecod.Text) & string.IsNullOrEmpty(txttipoopedesc.Text))
                    {
                        BuscarTipo_OperacionCompra();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipo_OperacionCompra()
        {
            try
            {
                txttipoopecod.Text = Accion.Formato(txttipoopecod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0007",
                    Gen_Codigo_Interno = txttipoopecod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipoopecod.Text.Trim().ToUpper())
                        {
                            txttipoopecod.Tag = T.Id_General_Det;
                            txttipoopecod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipoopedesc.Text = T.Gen_Descripcion_Det;
                            txttipoopecod.EnterMoveNextControl = true;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipoopecod.EnterMoveNextControl = false;
                    txttipoopecod.ResetText();
                    txttipoopecod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnquitardetcon_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles_CONT.Count > 0)
                {

                    Detalles_CONT.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                    dgvdatosContable.DataSource = null;
                    if (Detalles_CONT.Count > 0)
                    {
                        dgvdatosContable.DataSource = Detalles_CONT;
                        RefreshNumeral();
                        UpdateGrilla02();
                        Calcular(Detalles_CONT, Es_moneda_nac);
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtfechavencimiento_KeyDown(object sender, KeyEventArgs e)
        {
            try
             {
              if (e.KeyCode == Keys.Enter)
               {
                    //0006    01  CONTADO
                    //0007    02  CREDITO

                    if (txtfechavencimiento.Text == "01/01/1900" || txtfechavencimiento.Text == "01/01/0001" || txtfechavencimiento.Text !="")
                    {
                        BuscarCondicion_Por_Fecha("02");
                    }
                    else
                    {
                        BuscarCondicion_Por_Fecha("01");
                    }
                }

             }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        List<Entidad_Movimiento_Cab> Detalles_ADM = new List<Entidad_Movimiento_Cab>();
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            using (frm_compras_administrativo f = new frm_compras_administrativo())
            {

                f.Estado_Ven_Boton = Estado_Ven_Boton;

                f.Id_Empresa = Id_Empresa;
                f.Id_Anio = Id_Anio;
                f.Id_Periodo = Id_Periodo;
                f.Id_Libro = Id_Libro;
                f.Voucher = Voucher;



                if (f.ShowDialog() == DialogResult.OK)
                {

                    Detalles_ADM = f.Detalles_ADM;
                }
            }
        }


        Boolean Ctb_Afecto_RE;
        string Ctb_Afecto_Tipo, Ctb_Afecto_Tipo_Doc, Ctb_Afecto_Serie, Ctb_Afecto_Numero;

        private void txtserie_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void btnanadirdetcon_Click(object sender, EventArgs e)
        {
            try
            {

                if (VerificarDetalle2())
                {
                    Entidad_Movimiento_Cab ItemDetalle = new Entidad_Movimiento_Cab();

                    ItemDetalle.Id_Item = Convert.ToInt32(txtcuenta.Tag.ToString());

                    ItemDetalle.Ctb_Cuenta = txtcuenta.Text.ToString();
                    ItemDetalle.Ctb_Cuenta_Desc = txtcuentadesc.Text.Trim().ToString();

                    ItemDetalle.Ctb_Tipo_DH = txttipo.Tag.ToString();
                    ItemDetalle.CCtb_Tipo_DH_Interno = txttipo.Text.ToString();
                    ItemDetalle.Ctb_Tipo_DH_Desc = txttipodesc.Text.Trim().ToString();



                    ItemDetalle.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadoc.Text);
                    ItemDetalle.Ctb_Tipo_Ent_det = "P";
                    ItemDetalle.Ctb_Ruc_dni_det = txtrucdni.Text.ToString().Trim();
                    ItemDetalle.Ctb_Tipo_Doc_det = txttipodoc.Tag.ToString();
                    ItemDetalle.Ctb_moneda_cod_det = txtmonedacod.Tag.ToString();
                    ItemDetalle.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocod.Text;
                    ItemDetalle.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodesc.Text;
                    ItemDetalle.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text);
                    //ItemDetalle.Ctb_Tipo_Doc_det = txttipodoc.Text;



                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    ItemDetalle.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    ItemDetalle.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    ItemDetalle.Ctb_Operacion_Cod = txttipoopecod.Tag.ToString();
                    ItemDetalle.Ctb_Operacion_Cod_Interno = txttipoopecod.Text;
                    ItemDetalle.Ctb_Operacion_Desc = txttipoopedesc.Text;



                    if (ItemDetalle.Ctb_Tipo_DH == "0004")
                    {
                        if (Es_moneda_nac == true)
                        {
                            ItemDetalle.Ctb_Importe_Debe = Math.Round(Convert.ToDecimal(txtimportecontable.Text.ToString()),2);
                        }
                        else
                        {
                            ItemDetalle.Ctb_Importe_Debe = Math.Round(Convert.ToDecimal(txtimportecontable.Text) * Convert.ToDecimal(txttipocambiovalor.Text),2);
                            ItemDetalle.Ctb_Importe_Debe_Extr = Math.Round(Convert.ToDecimal(txtimportecontable.Text.ToString()),2);
                        }


                    }
                    else if (ItemDetalle.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            ItemDetalle.Ctb_Importe_Haber = Math.Round(Convert.ToDecimal(txtimportecontable.Text.ToString()),2);
                        }
                        else
                        {
                            ItemDetalle.Ctb_Importe_Debe = Math.Round(Convert.ToDecimal(txtimportecontable.Text) * Convert.ToDecimal(txttipocambiovalor.Text),2);
                            ItemDetalle.Ctb_Importe_Haber_Extr = Math.Round(Convert.ToDecimal(txtimportecontable.Text.ToString()),2);
                        }


                    }




                    if (EstadoDetalle2 == Estados.Nuevo)
                    {
                        Detalles_CONT.Add(ItemDetalle);

                        UpdateGrilla02();
                        EstadoDetalle2 = Estados.Guardado;
                    }
                    else if (EstadoDetalle2 == Estados.Modificar)
                    {

                        Detalles_CONT[Convert.ToInt32(txtcuenta.Tag) - 1] = ItemDetalle;

                        UpdateGrilla02();
                        EstadoDetalle2 = Estados.Guardado;

                    }

                    //ResetarValoresContables();
                    Calcular(Detalles_CONT, Es_moneda_nac);
                    //Recalcular_Totales_Cabecera(Detalles_CONT_Aux, Es_moneda_nac);

                    //btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnvalores_Click(object sender, EventArgs e)
        {
            Llenar_Importes();
        }

        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        void Llenar_Importes()
        {
            try
            {
                using (frm_valores f = new frm_valores())
                {
                    f.Detalles_CONT_Val = Detalles_CONT;
                    f.Fecha = Convert.ToDateTime(txtfechadoc.Text);

                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        foreach (Entidad_Movimiento_Cab item in Detalles_CONT)
                        {
                            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();


                            if ((item.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE 01
                            {

                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtbase01.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                }
                                else
                                {
                                item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtbase01.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtbase01.Text.ToString());
                                }
                 
                              
                            }
                            else if ((item.Ctb_Operacion_Cod == "0021"))//IGV 01
                            {
                                if (Es_moneda_nac)
                                {
                                     item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtigv01.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtigv01.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtigv01.Text.ToString());
                                }
                               

                            }
                            else if ((item.Ctb_Operacion_Cod == "0022"))//BASE IMPONIBLE 02
                            {
                                if (Es_moneda_nac)
                                {
                                     item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtbase02.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtbase02.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtbase02.Text.ToString());
                                }
                              

                            }
                            else if ((item.Ctb_Operacion_Cod == "0025"))//IGV 02
                            {
                                if (Es_moneda_nac)
                                {
                                     item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtigv02.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtigv02.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtigv02.Text.ToString());
                                }
                              

                            }
                            else if ((item.Ctb_Operacion_Cod == "0026"))//BASE IMPONIBLE 03
                            {
                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtbase03.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtbase03.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtbase03.Text.ToString());
                                }

                               


                            }
                            else if ((item.Ctb_Operacion_Cod == "0036"))//IGV 03
                            {
                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtigv03.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtigv03.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtigv03.Text.ToString());
                                }
                             }
                            else if ((item.Ctb_Operacion_Cod == "0037"))//VALOR DE ADQUISICION NO GRABADA
                            {
                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtvaadgrab.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtvaadgrab.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtvaadgrab.Text.ToString());
                                }


                            }
                            else if ((item.Ctb_Operacion_Cod == "0039"))//OTROS TRIBUTOS Y CARGOS
                            {
                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtotc.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtotc.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtotc.Text.ToString());
                                }

                             

                            }
                            else if ((item.Ctb_Operacion_Cod == "0038"))//IMPUESTO SELECTIVO AL CONSUMO
                            {
                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtisc.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Debe = Convert.ToDecimal(f.txtisc.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Debe_Extr = Convert.ToDecimal(f.txtisc.Text.ToString());

                                }



                            }
                            else if ((item.Ctb_Operacion_Cod == "0040"))//IMPORTE TOTAL
                            {
                                if (Es_moneda_nac)
                                {
                                    item.Ctb_Importe_Haber = Convert.ToDecimal(f.txttotal.Text);
                                }
                                else
                                {
                                    item.Ctb_Importe_Haber = Convert.ToDecimal(f.txttotal.Text) * Convert.ToDecimal(txttipocambiovalor.Text);
                                    item.Ctb_Importe_Haber_Extr = Convert.ToDecimal(f.txttotal.Text.ToString());

                                }


                            }

                          


                        }

                        UpdateGrilla02();
                        Calcular(Detalles_CONT, Es_moneda_nac);
                    }
                   
                }
             
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;

            if ((e.RowHandle >= 0))
            {
                //bool Diferencia = Convert.ToBoolean(View.GetRowCellDisplayText(e.RowHandle, View.Columns["Cta_Destino"]));
                bool iCheckSelection = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, "Cta_Destino"));
                if ((iCheckSelection == true))
                {
                    e.Appearance.BackColor = Color.FromArgb(128, 153, 195);
                }
            }

        }

        DateTime Ctb_Afecto_Fecha;
        decimal Ctb_Afecto_Porcentaje, Ctb_Afecto_Monto;

        bool ValidaRegimen()
        {
            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un comprobante");
                txtglosa.Focus();
                return false;
            }

            return true;
        }
        private void btnregimen_Click(object sender, EventArgs e)
        {

            try
            {
                if (ValidaRegimen())
                {

                        using (frm_compras_regimen f = new frm_compras_regimen())
                        {
                            f.Estado_Ven_Boton = Estado_Ven_Boton;

                            f.Id_Empresa = Id_Empresa;
                            f.Id_Anio = Id_Anio;
                            f.Id_Periodo = Id_Periodo;
                            f.Id_Libro = Id_Libro;
                            f.Voucher = Voucher;

                            f.txtregtipodoccod_ = txttipodoc.Text;
                            f.txtregtipodoccodtag_ = IIf(txttipodoc.Tag == null, "", txttipodoc.Tag.ToString()).ToString();
                            f.txtregtipodocdesc_ = txttipodocdesc.Text;
                            f.txtregimenserie_ = txtserie.Text;
                            f.txtmonto_ =Convert.ToDecimal(txtimportetotal.Text);


                            if (f.ShowDialog() == DialogResult.OK)
                            {
                                Ctb_Afecto_RE = f.chkAfecto.Checked;

                                if (Ctb_Afecto_RE== false)
                                {
                                    Ctb_Afecto_Tipo = "";
                                }
                                else
                                {
                                    Ctb_Afecto_Tipo = f.txttiporegimencod.Tag.ToString();
                                }
                                Ctb_Afecto_Tipo_Doc = f.txtregtipodoccod.Text;
                                //Ctb_Afecto_Serie = f.txtregimenserie.Text;
                                //Ctb_Afecto_Numero = f.txtregimennumero.Text;

                                string SerNum = f.txtregimenserie.Text;
                                string[] Datos = SerNum.Split(Convert.ToChar("-"));

                                Ctb_Afecto_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                                Ctb_Afecto_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();



                                if (Ctb_Afecto_RE == true)
                                {
                                    Ctb_Afecto_Fecha = Convert.ToDateTime(f.txtfechadetrac.Text);
                                }
                                else
                                {
                                    Ctb_Afecto_Fecha = Convert.ToDateTime("01/01/1900");
                                }


                                if (Convert.ToDecimal(f.txtporcentaje.Text) == 0)
                                {
                                    Ctb_Afecto_Porcentaje = 0;
                                }
                                else
                                {
                                    Ctb_Afecto_Porcentaje = Convert.ToDecimal(f.txtporcentaje.Text);
                                }


                                if (Convert.ToDecimal(f.txtmonto.Text) == 0)
                                {
                                    Ctb_Afecto_Monto = 0;
                                }
                                else
                                {
                                    Ctb_Afecto_Monto = Convert.ToDecimal(f.txtmonto.Text);
                                }

                            }else
                            {
                    
                            }
                        }
                }

            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void BuscarCondicion_Por_Fecha(string Codigo)
        {
            try
            {
                
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = Codigo
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == Codigo)
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;
              
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                 
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void txtidanalisis_TextChanged(object sender, EventArgs e)
        {
            if (txtidanalisis.Focus() == false)
            {
                txtanalisisdesc.ResetText();
            }
        }

        private void txtcuenta_TextChanged_1(object sender, EventArgs e)
        {
            if (txtcuenta.Focus() == false)
            {
                txtcuentadesc.ResetText();
            }
        }

        private void txtcuenta_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txttipoopecod_TextChanged(object sender, EventArgs e)
        {
            if (txttipoopecod.Focus() == false)
            {
                txttipoopedesc.ResetText();
            }
        }

        private void txttipo_TextChanged_1(object sender, EventArgs e)
        {
            if (txttipo.Focus() == false)
            {
                txttipodesc.ResetText();
            }
        }



        private void txtidanalisis_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtidanalisis.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtidanalisis.Text.Substring(txtidanalisis.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_analisis_contable_busqueda f = new _1_Busquedas_Generales.frm_analisis_contable_busqueda())
                        {

                            f.Ana_Compra = true;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Analisis_Contable Entidad = new Entidad_Analisis_Contable();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtidanalisis.Text = Entidad.Id_Analisis;
                                txtanalisisdesc.Text = Entidad.Ana_Descripcion;

                                SendKeys.SendWait("{TAB}");

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtidanalisis.Text) & string.IsNullOrEmpty(txtanalisisdesc.Text))
                    {
                        BuscarAnalisis();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarAnalisis()
        {
            try
            {
                txtidanalisis.Text = Accion.Formato(txtidanalisis.Text, 3);
                Logica_Analisis_Contable log = new Logica_Analisis_Contable();

                List<Entidad_Analisis_Contable> Generales = new List<Entidad_Analisis_Contable>();
                Generales = log.Listar(new Entidad_Analisis_Contable
                { Id_Empresa=Actual_Conexion.CodigoEmpresa,
                  Id_Anio=Actual_Conexion.AnioSelect,
                  Id_Analisis = txtidanalisis.Text,
                  Ana_Compra = true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Analisis_Contable T in Generales)
                    {
                        if ((T.Id_Analisis).ToString().Trim().ToUpper() == txtidanalisis.Text.Trim().ToUpper())
                        {
                            txtidanalisis.Text = (T.Id_Analisis).ToString().Trim();
                            txtanalisisdesc.Text = T.Ana_Descripcion;

                            SendKeys.SendWait("{TAB}");
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtidanalisis.EnterMoveNextControl = false;
                    txtidanalisis.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarAnalisis_DETALLE()
        {

            Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
            Logica_Analisis_Contable log = new Logica_Analisis_Contable();
            List<Entidad_Analisis_Contable> Detalles_Analisi = new List<Entidad_Analisis_Contable>();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Analisis = txtidanalisis.Text;

            Detalles_Analisi = log.Listar_det(Ent);

            Detalles_CONT.Clear();


            if (Detalles_Analisi.Count > 0)
            {
                 dgvdatosContable.DataSource = null;
                    Detalles_CONT.Clear();
                  
                    foreach (Entidad_Analisis_Contable T in Detalles_Analisi)
                    {
                        Entidad_Movimiento_Cab Item = new Entidad_Movimiento_Cab();

                        Item.Id_Item = Detalles_CONT.Count + 1;
                        Item.Ctb_Cuenta = T.Ana_Cuenta;
                        Item.Ctb_Cuenta_Desc = T.Ana_Cuenta_Desc;
                        Item.Ctb_Operacion_Cod = T.Ana_Operacion_Cod;
                        Item.Ctb_Operacion_Cod_Interno = T.Ana_Operacion_Det;
                        Item.Ctb_Operacion_Desc = T.Ana_Operacion_Desc;
                        Item.Ctb_Tipo_DH = T.Ana_Tipo;
                        Item.CCtb_Tipo_DH_Interno = T.Ana_Tipo_Det;
                        Item.Ctb_Tipo_DH_Desc = T.Ana_Tipo_Desc;

                        Item.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadoc.Text);
                        Item.Ctb_Tipo_Ent_det = "P";
                        Item.Ctb_Ruc_dni_det = txtrucdni.Text.ToString().Trim();
                        Item.Ctb_Tipo_Doc_det = txttipodoc.Tag.ToString();
                        Item.Ctb_moneda_cod_det = txtmonedacod.Tag.ToString();
                        Item.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocod.Text;
                        Item.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodesc.Text;

                    if (txttipocambiovalor.Text == "")
                    {
                        txttipocambiovalor.Text = Convert.ToString(0);
                        Item.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text);
                    }
                    else
                    {
                        Item.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text);
                    }
                     
                        
            
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Item.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Item.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();

                    if (Item.Ctb_Tipo_DH == "0004")
                        {
                            if (Es_moneda_nac == true)
                            {
                                Item.Ctb_Importe_Debe = 0;
                            }
                            else
                            {
                                Item.Ctb_Importe_Debe_Extr = 0;
                            }


                        }
                        else if (Item.Ctb_Tipo_DH == "0005")
                        {
                            if (Es_moneda_nac == true)
                            {
                                Item.Ctb_Importe_Haber = 0;
                            }
                            else
                            {
                                Item.Ctb_Importe_Haber_Extr = 0;
                            }


                        }

                        Detalles_CONT.Add(Item);
                    }

         
          
            }
            Llenar_Importes();
            Calcular(Detalles_CONT, Es_moneda_nac);
            UpdateGrilla02();


        }


        public void BuscarAnalisis_DETALLE_Modificar()
        {

            Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
            Logica_Analisis_Contable log = new Logica_Analisis_Contable();
            List<Entidad_Analisis_Contable> Detalles_Analisi = new List<Entidad_Analisis_Contable>();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Analisis = txtidanalisis.Text;

            Detalles_Analisi = log.Listar_det(Ent);

           

            if (Detalles_Analisi.Count > 0)
            {
               
                Detalles_CONT.Clear();

                foreach (Entidad_Analisis_Contable T in Detalles_Analisi)
                {
                    Entidad_Movimiento_Cab Item = new Entidad_Movimiento_Cab();

                    Item.Id_Item = Detalles_CONT.Count + 1;
                    Item.Ctb_Cuenta = T.Ana_Cuenta;
                    Item.Ctb_Cuenta_Desc = T.Ana_Cuenta_Desc;
                    Item.Ctb_Operacion_Cod = T.Ana_Operacion_Cod;
                    Item.Ctb_Operacion_Cod_Interno = T.Ana_Operacion_Det;
                    Item.Ctb_Operacion_Desc = T.Ana_Operacion_Desc;
                    Item.Ctb_Tipo_DH = T.Ana_Tipo;
                    Item.CCtb_Tipo_DH_Interno = T.Ana_Tipo_Det;
                    Item.Ctb_Tipo_DH_Desc = T.Ana_Tipo_Desc;

                    Item.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadoc.Text);
                    Item.Ctb_Tipo_Ent_det = "P";
                    Item.Ctb_Ruc_dni_det = txtrucdni.Text.ToString().Trim();
                    Item.Ctb_Tipo_Doc_det = txttipodoc.Text;
                    Item.Ctb_moneda_cod_det = txtmonedacod.Text;
                    Item.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocod.Text;
                    Item.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodesc.Text;
                    Item.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text);
                    Item.Ctb_Tipo_Doc_det = txttipodoc.Text;

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Item.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Item.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    if (Item.Ctb_Tipo_DH == "0004")
                    {
                        if (Es_moneda_nac == true)
                        {
                            Item.Ctb_Importe_Debe = 0;//Convert.ToDecimal(txtimportecontable.Text.ToString());
                        }
                        else
                        {
                            Item.Ctb_Importe_Debe_Extr = 0;//Convert.ToDecimal(txtimportecontable.Text.ToString());
                        }


                    }
                    else if (Item.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            Item.Ctb_Importe_Haber = 0;//Convert.ToDecimal(txtimportecontable.Text.ToString());
                        }
                        else
                        {
                            Item.Ctb_Importe_Haber_Extr = 0;//Convert.ToDecimal(txtimportecontable.Text.ToString());
                        }


                    }

                    Detalles_CONT.Add(Item);
                }
                EstadoDetalle2 = Estados.Guardado;
            }


   

        }



        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)

        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
