﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable 
{
    public partial class frm_orden_cs_buscar :  frm_fuente
    {
        public frm_orden_cs_buscar()
        {
            InitializeComponent();
        }

        public List<Entidad_Orden_C_S> Lista = new List<Entidad_Orden_C_S>();
        public void Listar()
        {
            Entidad_Orden_C_S Ent = new Entidad_Orden_C_S();
            Logica_Orden_C_S log = new Logica_Orden_C_S();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
            Ent.Id_Movimiento = null;
            Ent.Id_Tipo_Orden = null;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_orden_cs_buscar_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
  
        if (Lista.Count > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
