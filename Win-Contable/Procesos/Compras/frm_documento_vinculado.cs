﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable 
{
    public partial class frm_documento_vinculado : frm_fuente
    {
        public frm_documento_vinculado()
        {
            InitializeComponent();
        }
        public string Libro;
        public string Libro_desc;
        public List<Entidad_Movimiento_Cab> Lista_Doc_ref = new List<Entidad_Movimiento_Cab>();
        public List<Entidad_Movimiento_Cab> Lista_Doc_ref_Aux = new List<Entidad_Movimiento_Cab>();

        private void btnagregar_Click(object sender, EventArgs e)
        {
            using (_1_Busquedas_Generales.frm_buscar_provisiones f = new _1_Busquedas_Generales.frm_buscar_provisiones())
            {
                f.Id_libro = Libro;
                f.Libro_nombre = Libro_desc;
               

                if (f.ShowDialog() == DialogResult.OK)
                {
                   try
                    {

                        Entidad_Movimiento_Cab Doc = new Entidad_Movimiento_Cab();
                        Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                        Doc = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                        Ent.Id_Item = Lista_Doc_ref.Count + 1;
                        Ent.Id_Anio_Ref = Doc.Id_Anio_Ref;
                        Ent.Id_Periodo_Ref = Doc.Id_Periodo_Ref;
                        Ent.Id_Libro_Ref = Doc.Id_Libro_Ref;
                        Ent.Id_Voucher_Ref = Doc.Id_Voucher_Ref;
                        Ent.Id_Item_Ref = Doc.Id_Item_Ref;
                        Ent.Ctb_Cuenta = Doc.Ctb_Cuenta;
                        Ent.Ctb_Serie_det = Doc.Ctb_Serie_det;
                        Ent.Ctb_Numero_det = Doc.Ctb_Numero_det;

                        Ent.Ctb_Tipo_Doc_det = Doc.Ctb_Tipo_Doc_det;
                        Ent.Ctb_Tipo_Doc_det_desc = Doc.Ctb_Tipo_Doc_det_desc;
                        Ent.Ctb_Ruc_dni_det = Doc.Ctb_Ruc_dni_det;
                        Ent.Entidad_det = Doc.Entidad_det;

                        Ent.Ctb_Fecha_Movimiento = Doc.Ctb_Fecha_Movimiento;
                        Ent.Ctb_moneda_cod_det = Doc.Ctb_moneda_cod_det;
                        Ent.Ctb_moneda_det_desc = Doc.Ctb_moneda_det_desc;
                        Ent.Ctb_Tipo_Cambio_Cod_Det = Doc.Ctb_Tipo_Cambio_Cod_Det;

                        Ent.Ctb_Tipo_Cambio_Valor_Det = Doc.Ctb_Tipo_Cambio_Valor_Det;
                        Ent.Ctb_Importe_Total = Doc.Ctb_Importe_Total;


                        Lista_Doc_ref.Add(Ent);


                dgvdatos.DataSource = null;
    

                    if (Lista_Doc_ref.Count>0)
                        {
                            dgvdatos.DataSource = Lista_Doc_ref;
                        }

                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }

            }
        }

        private void frm_documento_vinculado_Load(object sender, EventArgs e)
        {
            try
            {
            Lista_Doc_ref = Lista_Doc_ref_Aux;
            if (Lista_Doc_ref.Count > 0)
            {
                dgvdatos.DataSource = Lista_Doc_ref;
            }
            } catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        private void btnquitar_Click(object sender, EventArgs e)
        {
            try
            {
      if (Lista_Doc_ref.Count > 0)
            {
                Lista_Doc_ref.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                dgvdatos.DataSource = null;
                if (Lista_Doc_ref.Count > 0)
                {
                        RefrescarNumeracionDeDocumentos();
                }
            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
      
        }


        public void RefrescarNumeracionDeDocumentos()
        {
            if (Lista_Doc_ref.Count > 0)
            {
                for (int i = 0; i <= Lista_Doc_ref.Count - 1; i++)
                {
                    Lista_Doc_ref[i].Id_Item = i + 1;
                }
                dgvdatos.DataSource = null;
                dgvdatos.DataSource = Lista_Doc_ref;
            }
        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
