﻿using Contable.Reportes_Impresiones;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_libro_diario : Contable.frm_fuente
    {
        public frm_libro_diario()
        {
            InitializeComponent();
        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtCodLibro_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(TxtCodLibro.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & TxtCodLibro.Text.Substring(TxtCodLibro.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                //LibVenta = Entidad.Id_Libro;
                                TxtCodLibro.Text = Entidad.Id_SUNAT;
                                TxtCodLibro.Tag = Entidad.Id_Libro;
                                TxtNomLibro.Text = Entidad.Nombre_Libro;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(TxtCodLibro.Text) & string.IsNullOrEmpty(TxtNomLibro.Text))
                    {
                        libroVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }else if (e.KeyCode == Keys.Enter & TxtNomLibro.Text.Trim()=="")
            {
                TxtNomLibro.Text = "[TODOS]";
            }


        }

        public void libroVenta()
        {
            try
            {

                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_Libro = TxtCodLibro.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == TxtCodLibro.Text.Trim().ToUpper())
                        {
                            //LibVenta = (T.Id_SUNAT).ToString().Trim();
                            TxtCodLibro.Text = (T.Id_SUNAT).ToString().Trim();
                            TxtNomLibro.Text = T.Nombre_Libro;
                            TxtCodLibro.Tag = T.Id_Libro.Trim();

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        bool Verificar()
        {
            if (string.IsNullOrEmpty(txtmesdesc.Text))
            {
                Accion.Advertencia("Ingresar el Periodo de Analisis");
                txtmescod.Focus();
                return false;
            }
            else if ((Rbt_Centralizado.Checked == false) && ((Rbt_xLibro.Checked == false)  ))
            {
                Accion.Advertencia("Debe seleccionar el Tipo de Reporte a Generar");
                Rbt_Centralizado.Focus();
                Rbt_Centralizado.Checked = true;
                return false;
            }

            return true;
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Rbt_Centralizado.Checked = false;
            Rbt_xLibro.Checked = false;
            rbacumulado.Checked = false;
            rbmensual.Checked = false;
            BandGridReport.DataSource = null;
            txtmescod.ResetText();
            txtmesdesc.ResetText();
            TxtCodLibro.ResetText();
            TxtCodLibro.Enabled = false;
            TxtNomLibro.ResetText();
            ListCta.Clear();
            txtmescod.Focus();
        }

        List<Entidad_Reporte_Libro_Diario> ListCta = new List<Entidad_Reporte_Libro_Diario>();
        private void btnbuscar_Click(object sender, EventArgs e)
        {

            try {
         if (Verificar() == true)
                    {
                        Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
                        Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();
             
                        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        Ent.Id_Anio = Actual_Conexion.AnioSelect;
                        Ent.Id_Periodo = txtmescod.Text.Trim();

                    if (rbmensual.Checked == true)
                    {
                        Ent.Acumulado = false;
                    }
                    else
                    {
                        Ent.Acumulado = true;
                    }


                        if (Rbt_Centralizado.Checked)
                        {
                            ListCta = Log.ListarLibroDiarioCentralizado(Ent);
                        }
                        else
                        {
                            if ((TxtNomLibro.Text.Trim() == "[TODOS]"))
                            {

                            }
                            else
                            {
                                Ent.Lib_Codigo = TxtCodLibro.Tag.ToString().Trim();
                            }

                            ListCta = Log.ListarLibroDiario(Ent);
                        }

                        if ((ListCta.Count > 0))
                        {
                            BandGridReport.DataSource = ListCta;
                        }
                        else
                        {
                            Accion.Advertencia("No se encontro ningun registro");
                        }

                    }
                
            }
            catch (Exception ex) {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtmescod_TextChanged(object sender, EventArgs e)
        {
            if (txtmescod.Focus() == false)
            {
                txtmesdesc.ResetText();
            }
        }

        private void TxtCodLibro_TextChanged(object sender, EventArgs e)
        {
            if (TxtCodLibro.Focus() == false)
            {
                TxtNomLibro.ResetText();
            }
        }

        private void frm_libro_diario_Load(object sender, EventArgs e)
        {
            TxtCodLibro.Enabled = false;
        }

        private void Rbt_Centralizado_CheckedChanged(object sender, EventArgs e)
        {
            if (Rbt_Centralizado.Checked == true)
            {
                TxtCodLibro.Enabled = false;
                TxtCodLibro.ResetText();
                TxtNomLibro.ResetText();
            }
        }

        private void Rbt_xLibro_CheckedChanged(object sender, EventArgs e)
        {
            if (Rbt_xLibro.Checked == true)
            {
                TxtCodLibro.Enabled = true;
                TxtNomLibro.Text = "[TODOS]";
                TxtCodLibro.Focus();

            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path+ ("\\LibroDiario" + ".Xlsx");

            BandGridReport.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);

        }

        string CondicionInformacion = "";
        string CondicionInformacion2 = "";
        private void btnple_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                    if ((Verificar() == true))
                    {

                    Rbt_xLibro.Checked = true;
                    rbmensual.Checked = true;

                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                        if ((fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK))
                        {
                            List<Entidad_Reporte_Libro_Diario> ListCta = new List<Entidad_Reporte_Libro_Diario>();
                     List<Entidad_Reporte_Libro_Diario> ListCta_Cuentas = new List<Entidad_Reporte_Libro_Diario>();
                  
                            Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
                            Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();


                                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                                Ent.Id_Anio = Actual_Conexion.AnioSelect;
                                Ent.Id_Periodo = txtmescod.Text.Trim();
                                
                     

                        ListCta = Log.Ple_V5(Ent);
                        ListCta_Cuentas = Log.Ple_V5_Cuentas(Ent);

                        if (ListCta.Count > 0)
                                {
                                    CondicionInformacion = "1";
                                }
                                else
                                {
                                    CondicionInformacion = "0";
                                }

                           if (ListCta_Cuentas.Count > 0)
                                {
                                    CondicionInformacion2 = "1";
                                }
                                else
                                {
                                    CondicionInformacion2 = "0";
                                }
                            
                            GenerarArchivoTxt_50(ListCta, fbd.SelectedPath);
                            GenerarArchivoTxt_50_Cuentas(ListCta_Cuentas, fbd.SelectedPath);
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

         
        }

        void GenerarArchivoTxt_50(List<Entidad_Reporte_Libro_Diario> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim()  + Actual_Conexion.AnioSelect + txtmescod.Text.Trim() + "00" + "050100" + "00" + "1" + CondicionInformacion + "1" + "1" + ".txt"));
                foreach (Entidad_Reporte_Libro_Diario txt in ColumnasVisibles)
                {
                    sb.Append((txt.ple_V5 + "\r\n"));
                }

                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile  = new StreamWriter(Ruta))
                {
                        outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        void GenerarArchivoTxt_50_Cuentas(List<Entidad_Reporte_Libro_Diario> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod.Text.Trim() + "00" + "050300" + "00" + "1" + CondicionInformacion2 + "1" + "1" + ".txt"));
                foreach (Entidad_Reporte_Libro_Diario txt in ColumnasVisibles)
                {
                    sb.Append((txt.ple_V5_cuentas + "\r\n"));
                }

                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile = new StreamWriter(Ruta))
                {
                    outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                if (ListCta.Count>0)
                {
                   Rpt_Libro_Diario f = new Rpt_Libro_Diario();


                    f.TxtPerAnio.Text = txtmesdesc.Text + " DEL " + Actual_Conexion.AnioSelect;
                    f.TxtRazonSocial.Text = Actual_Conexion.EmpresaNombre;
                    f.TxtRuc.Text = Actual_Conexion.RucEmpresa;

                    f.DataSource = ListCta;
                    //f.PaperKind = PaperKind.A4;
                    //f.Landscape = true;

                    dynamic ribbonPreview = new PrintPreviewFormEx();
                    f.CreateDocument();
                    f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    ribbonPreview.PrintingSystem = f.PrintingSystem;
                    // ribbonPreview.MdiParent = frm_principal;
                    ribbonPreview.Show();

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
    }
}

