﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_libro_diario_simplificado : frm_fuente
    {
        public frm_libro_diario_simplificado()
        {
            InitializeComponent();
        }

      
       List<string> lstCls = new List<string>();
        DataTable Dt = new DataTable();
        XtraReport Rpt = new XtraReport();



        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd1= new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
        DevExpress.XtraGrid.Views.BandedGrid.GridBand GvBnd0 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();

        public static Boolean IsNumeric(string valor)
        {
            int result;
            return int.TryParse(valor, out result);
        }

        bool Verificar()
        {
            if (string.IsNullOrEmpty(txtmesdesc.Text))
            {
                Accion.Advertencia("Ingresar el Periodo de Analisis");
                txtmescod.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtnivel.Text))
            {
                Accion.Advertencia("Debe seleccionar el Tipo de nivel");
                txtnivel.Focus();
                return false;
            }

            return true;
        }

        private void btnver_Click(object sender, EventArgs e)
        {
            if (Verificar())
            {

            try
            {
                BandGridReport.DataSource = null;
                try
                {
                    
                    for (int i = BandGridVisual.Columns.Count - 1; i >= 0; i += -1)
                    {
                        try
                        {
                            if (IsNumeric(BandGridVisual.Columns[i].FieldName))
                            {
                                BandGridVisual.Columns.Remove(BandGridVisual.Columns[i]);
                            }

                        }
                        catch (Exception ex)
                        {
                        }

                    }
                }
                catch (Exception)
                {

                    throw;
                }


                try

                {

                    //int step = 2;
                    //for (int i = 0; i <= 10; i += step)
                    //{
                    //    Console.WriteLine(i);
                    //}

                    for (int i = BandGridVisual.Bands.Count - 1; i >= 10; i += -1)
                    //for (int i = 0; (i <= (BandGridVisual.Bands.Count - 1)); i++)
                    {
                        try
                        {
                            //MessageBox.Show(Convert.ToString(BandGridVisual.Bands[i].Tag));


                            if (Convert.ToString(BandGridVisual.Bands[i].Tag) != "NO")
                            {
                                BandGridVisual.Bands.RemoveAt(i);
                            }

                        }
                        catch (Exception ex)
                        {
                        }

                    }

                }
                catch (Exception ex)
                {
                }


            }
            catch (Exception)
            {

                throw;
            }


            Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
            Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();

            Ent.Id_Periodo =txtmescod.Text;
            Ent.Nivel = Convert.ToInt32(txtnivel.Text);

            string BandasIniciales = "";
            Dt = null;
            Dt = Log.Reporte_LibroDiarioSimplificado(Ent);

            

            if (Dt.Rows.Count > 0)
            {
                Int32 Andcho = 50;

                for (int i = 0; (i   <= (Dt.Columns.Count - 1)); i++)
                {
                    if ((i >= 3))
                    {
                        lstCls.Add(Dt.Columns[i].ColumnName.ToString());
                        Dt.Columns[i].ColumnName = Dt.Columns[i].ColumnName.ToString().Substring(1);

                        string rt = Dt.Columns[i].ColumnName.ToString().Substring(0, 1);
                        string ry = (BandasIniciales.IndexOf("-1-") + 1).ToString();

                        if ((Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "1"))
                        {
                              if ((BandasIniciales.IndexOf("-1-") + 1) == 0)
                            {
                                BandasIniciales = (BandasIniciales + "-1-");
                                //GvBnd1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd1.AppearanceHeader.Options.UseFont = true;
                                GvBnd1.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd1.Caption = "ACTIVOS";
                                this.BandGridVisual.Bands.Add(GvBnd1);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                          
                            GvBnd1.Columns.Add(BgClmn);

                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "2")
                        {
                            if ((BandasIniciales.IndexOf("-1-") + 1)== 0)
                            {
                                BandasIniciales = (BandasIniciales + "-2-");
                                //GvBnd1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd1.AppearanceHeader.Options.UseFont = true;
                                GvBnd1.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd1.Caption = "ACTIVOS";
                                this.BandGridVisual.Bands.Add(GvBnd1);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd1.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "3")
                        {
                            if ((BandasIniciales.IndexOf("-1-") + 1)== 0)
                            {
                                BandasIniciales = (BandasIniciales + "-2-");
                                //GvBnd1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd1.AppearanceHeader.Options.UseFont = true;
                                GvBnd1.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd1.Caption = "ACTIVOS";
                                this.BandGridVisual.Bands.Add(GvBnd1);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd1.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "4")
                        {
                            if ((BandasIniciales.IndexOf("-4-") + 1) == 0)
                            {
                                BandasIniciales = (BandasIniciales + "-4-");
                                //GvBnd4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd4.AppearanceHeader.Options.UseFont = true;
                                GvBnd4.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd4.Caption = "PASIVOS";
                                this.BandGridVisual.Bands.Add(GvBnd4);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd4.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "5")
                        {
                            if ((BandasIniciales.IndexOf("-5-") + 1)== 0)
                            {
                                BandasIniciales = (BandasIniciales + "-5-");
                                //GvBnd5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd5.AppearanceHeader.Options.UseFont = true;
                                GvBnd5.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd5.Caption = "PATRIMONIO";
                                this.BandGridVisual.Bands.Add(GvBnd5);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd5.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "6")
                        {
                            if ((BandasIniciales.IndexOf("-6-") + 1)  == 0)
                            {
                                BandasIniciales = (BandasIniciales + "-6-");
                                //GvBnd6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd6.AppearanceHeader.Options.UseFont = true;
                                GvBnd6.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd6.Caption = "GASTOS";
                                this.BandGridVisual.Bands.Add(GvBnd6);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd6.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "7")
                        {
                            if ((BandasIniciales.IndexOf("-7-") + 1) == 0)
                            {
                                BandasIniciales = (BandasIniciales + "-7-");
                                //GvBnd7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd7.AppearanceHeader.Options.UseFont = true;
                                GvBnd7.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd7.Caption = "INGRESOS";
                                this.BandGridVisual.Bands.Add(GvBnd7);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd7.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "8")
                        {
                            if ((BandasIniciales.IndexOf("-8-") + 1)== 0)
                            {
                                BandasIniciales = (BandasIniciales + "-8-");
                                //GvBnd8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd8.AppearanceHeader.Options.UseFont = true;
                                GvBnd8.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd8.Caption = "SALDOS INTERMEDIARIOS DE GESTION";
                                this.BandGridVisual.Bands.Add(GvBnd8);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd8.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "9")
                        {
                            if ((BandasIniciales.IndexOf("-9-") + 1)== 0)
                            {
                                BandasIniciales = (BandasIniciales + "-9-");
                                //GvBnd9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd9.AppearanceHeader.Options.UseFont = true;
                                GvBnd9.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd9.Caption = "CUENTAS DE FUNCION DEL GASTO";
                                this.BandGridVisual.Bands.Add(GvBnd9);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd9.Columns.Add(BgClmn);
                        }
                        else if (Dt.Columns[i].ColumnName.ToString().Substring(0, 1) == "0")
                        {
                            if ((BandasIniciales.IndexOf("-0-") + 1)== 0)
                            {
                                BandasIniciales = (BandasIniciales + "-0-");
                                //GvBnd0.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75, !);
                                GvBnd0.AppearanceHeader.Options.UseFont = true;
                                GvBnd0.AppearanceHeader.Options.UseTextOptions = true;
                                GvBnd0.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                                GvBnd0.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                                GvBnd0.Caption = "CUENTAS DE ORDEN";
                                this.BandGridVisual.Bands.Add(GvBnd0);
                            }

                            DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BgClmn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
                            //BgClmn.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7, !);
                            BgClmn.AppearanceHeader.Options.UseFont = true;
                            BgClmn.Caption = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.FieldName = Dt.Columns[i].ColumnName.ToString();
                            BgClmn.OptionsColumn.AllowEdit = false;
                            BgClmn.Visible = true;
                            BgClmn.OptionsColumn.FixedWidth = true;
                            BgClmn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Dt.Columns[i].ColumnName.ToString(), "{0:n2}")});
                            GvBnd0.Columns.Add(BgClmn);
                        }

                    }

                    // ----- vvvv
                    BandGridVisual.Columns.AddField(Dt.Columns[i].ColumnName);
                    BandGridVisual.Columns[i].Visible = true;


                }

                BandGridReport.DataSource = null;
                this.BandGridReport.DataSource = Dt;



            }
    }




       }

        private void txtnivel_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtnivel.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtnivel.Text.Substring(txtnivel.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_estructura_plan_busqueda f = new _1_Busquedas_Generales.frm_estructura_plan_busqueda())
                        {
                            //f.NumDigitos = txtnivel.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Estructura Entidad = new Entidad_Estructura();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtnivel.Text = Entidad.Est_Num_Digitos;
                                txtniveldesc.Text = Entidad.Est_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtnivel.Text) & string.IsNullOrEmpty(txtniveldesc.Text))
                    {
                        BuscarNivel();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarNivel()
        {
            try
            {
                txtnivel.Text = Accion.Formato(txtnivel.Text, 1);

                Logica_Estructura log = new Logica_Estructura();

                List<Entidad_Estructura> Generales = new List<Entidad_Estructura>();

                Generales = log.Listar(new Entidad_Estructura
                {
                    Est_Num_Digitos = txtnivel.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Estructura T in Generales)
                    {
                        if ((T.Est_Num_Digitos).ToString().Trim().ToUpper() == txtnivel.Text.Trim().ToUpper())
                        {
                            txtnivel.Text = (T.Est_Num_Digitos).ToString().Trim();
                            txtniveldesc.Text = T.Est_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro los datos");
                    txtnivel.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtnivel_TextChanged(object sender, EventArgs e)
        {
            if (txtnivel.Focus() == false)
            {
                txtniveldesc.ResetText();
            }
        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                    txtmescod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmescod_TextChanged(object sender, EventArgs e)
        {
            if (txtmescod.Focus() == false)
            {
                txtmesdesc.ResetText();
            }
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                BandGridReport.DataSource = null;
                DgvFormatoSimplificado02.DataSource = null;
                gvElectronico_V5_Det.DataSource = null;

                txtnivel.Reset();
                txtniveldesc.Reset();
                txtmescod.Reset();
                txtmesdesc.Reset();
                txtmescod2.Reset();
                txtmesdesc2.Reset();

            }
            catch (Exception)
            {

                throw;
            }
        }


        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           List<string> LabelsNombres = new List<string>();
            List<double> LabelsTotales = new List<double>();
            XtraReport Rpt = new XtraReport(); 

            Rpt.Margins = new System.Drawing.Printing.Margins(48, 24, 3, 29);
            LabelsNombres.Clear();
            LabelsTotales.Clear();

            DevExpress.XtraReports.UI.XRLabel XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel LblPeriodo = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel LblRuc = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel LblNombre = new DevExpress.XtraReports.UI.XRLabel();

            XrLabel1.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0.0F, 10.00001F);
            XrLabel1.Name = "XrLabel1";
            XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            XrLabel1.SizeF = new System.Drawing.SizeF(63.13843F, 23.0F);
            XrLabel1.StylePriority.UseFont = false;
            XrLabel1.StylePriority.UseTextAlignment = false;
            XrLabel1.Text = "PERIODO";
            XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

            XrLabel12.Font = new System.Drawing.Font("Times New Roman",7.5F);
            XrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0.0F,32.99999F);
            XrLabel12.Name = "XrLabel12";
            XrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            XrLabel12.SizeF = new System.Drawing.SizeF(63.13843F, 23.0F);
            XrLabel12.StylePriority.UseFont = false;
            XrLabel12.StylePriority.UseTextAlignment = false;
            XrLabel12.Text = "RUC";
            XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

            XrLabel13.Font = new System.Drawing.Font("Times New Roman",7.5F);
            XrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0.0F,55.99998F);
            XrLabel13.Name = "XrLabel13";
            XrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            XrLabel13.SizeF = new System.Drawing.SizeF(222.9999F, 23.0F);
            XrLabel13.StylePriority.UseFont = false;
            XrLabel13.StylePriority.UseTextAlignment = false;
            XrLabel13.Text = "APELLIDOS Y NOMBRES O RAZON SOCIAL";
            XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

            // LblPeriodo
            // 
            LblPeriodo.LocationFloat = new DevExpress.Utils.PointFloat(63.13842F,10.00001F);
            LblPeriodo.Name = "LblPeriodo";
            LblPeriodo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            LblPeriodo.SizeF = new System.Drawing.SizeF(159.8615F, 23.0F );
            LblPeriodo.StylePriority.UseTextAlignment = false;
            LblPeriodo.Text = txtmesdesc.Text;
            LblPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LblRuc
            // 
            LblRuc.LocationFloat = new DevExpress.Utils.PointFloat(63.13842F, 32.99999F);
            LblRuc.Name = "LblRuc";
            LblRuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            LblRuc.SizeF = new System.Drawing.SizeF(159.8615F,23.0F);
            LblRuc.StylePriority.UseTextAlignment = false;
            LblRuc.Text = Actual_Conexion.RucEmpresa;
            LblRuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LblNombre
            // 
            LblNombre.LocationFloat = new DevExpress.Utils.PointFloat(223.0F, 55.99998F);
            LblNombre.Name = "LblNombre";
            LblNombre.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            LblNombre.SizeF = new System.Drawing.SizeF(700.0F,  23.0F);
            LblNombre.StylePriority.UseTextAlignment = false;
            LblNombre.Text =Actual_Conexion.EmpresaNombre;
            LblNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

            DevExpress.XtraReports.UI.XRLabel XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel6 = new DevExpress.XtraReports.UI.XRLabel();

            // XrLabel3
            XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0.0F,86.45834F);
            XrLabel3.Name = "XrLabel3";
            XrLabel3.Font = new System.Drawing.Font("Times New Roman", 5.5F);
            XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0F);
            XrLabel3.SizeF = new System.Drawing.SizeF(63.13843F,38.46584F);
            XrLabel3.StylePriority.UseFont = false;
            XrLabel3.StylePriority.UseTextAlignment = false;
            XrLabel3.Text = "Nom. Correlativo o Codigo Unico de la operacion";
            XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            XrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) | DevExpress.XtraPrinting.BorderSide.Right)  | DevExpress.XtraPrinting.BorderSide.Bottom)));


            // XrLabel4
            // 
            XrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(Convert.ToSingle(63.13845),Convert.ToSingle(86.45834));
            XrLabel4.Name = "XrLabel4";
            XrLabel4.Font = new System.Drawing.Font("Times New Roman",6.0F);
            XrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0F);
            XrLabel4.SizeF = new System.Drawing.SizeF(46.16336F, 38.46584F);
            XrLabel4.StylePriority.UseFont = false;
            XrLabel4.StylePriority.UseTextAlignment = false;
            XrLabel4.Text = "Fecha o periodo de la operacion";
            XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            XrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) | DevExpress.XtraPrinting.BorderSide.Bottom)));


            XrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(109.3018F,86.45834F);
            XrLabel6.Name = "XrLabel6";
            XrLabel6.Font = new System.Drawing.Font("Times New Roman", 6.0F);
            XrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0F);
            XrLabel6.SizeF = new System.Drawing.SizeF(328.805F,38.46585F);
            XrLabel6.StylePriority.UseFont = false;
            XrLabel6.StylePriority.UseTextAlignment = false;
            XrLabel6.Text = "Glosa o descripcion de la operacion";
            XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            XrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) | DevExpress.XtraPrinting.BorderSide.Bottom)));


            DevExpress.XtraReports.UI.PageHeaderBand PageHeaderBand1 = new DevExpress.XtraReports.UI.PageHeaderBand();
          
            PageHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            XrLabel1,
            XrLabel12,
            XrLabel13,
            LblPeriodo,
            LblRuc,
            LblNombre,
            XrLabel3,
            XrLabel4,
            XrLabel6});
            PageHeaderBand1.HeightF =124.9242F;
            PageHeaderBand1.Name = "PageHeaderBand1";


            DevExpress.XtraReports.UI.FormattingRule FormattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            FormattingRule1.Condition = "Iif([DataSource.CurrentRowIndex] == 1, False ,True )";
            FormattingRule1.Formatting.ForeColor = System.Drawing.Color.Transparent;
            FormattingRule1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            FormattingRule1.Name = "FormattingRule1";



            List<string> BandasInicialesNombres = new List<string>();
            List<double> BandasInicialesTamanios = new List<double>();
            string ListaInicales = "";
            double TamFinal=0, Tamf2=0, Tamf3=0;

       
            for (int i = 0; (i  <= (lstCls.Count - 1)); i++)
            {
                if ((lstCls[i].Substring(1, 1) == "1"))
                {
                    if (((ListaInicales.IndexOf("-1-") + 1)   == 0))
                    {
                        ListaInicales = (ListaInicales + "-1-");
                        BandasInicialesNombres.Add("ACTIVOS");
                        TamFinal = 0;
                        for (int h = 0; (h <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "1"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "2"))
                {
                    if (((ListaInicales.IndexOf("-1-") + 1) == 0))
                    {
                        ListaInicales = (ListaInicales + "-1-");
                        BandasInicialesNombres.Add("ACTIVOS");
                        BandasInicialesTamanios.Add(0);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "3"))
                {
                    if (((ListaInicales.IndexOf("-1-") + 1)  == 0))
                    {
                        ListaInicales = (ListaInicales + "-1-");
                        BandasInicialesNombres.Add("ACTIVOS");
                        BandasInicialesTamanios.Add(0);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "4"))
                {
                    if (((ListaInicales.IndexOf("-4-") + 1)  == 0))
                    {
                        ListaInicales = (ListaInicales + "-4-");
                        BandasInicialesNombres.Add("PASIVOS");
                        TamFinal = 0;
                        for (int h = 0; (h <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "4"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "5"))
                {
                    if (((ListaInicales.IndexOf("-5-") + 1)  == 0))
                    {
                        ListaInicales = (ListaInicales + "-5-");
                        BandasInicialesNombres.Add("PATRIMONIO");
                        TamFinal = 0;
                        for (int h = 0; (h  <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "5"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "6"))
                {
                    if (((ListaInicales.IndexOf("-6-") + 1)  == 0))
                    {
                        ListaInicales = (ListaInicales + "-6-");
                        BandasInicialesNombres.Add("GASTOS");
                        TamFinal = 0;
                        for (int h = 0; (h  <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "6"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "7"))
                {
                    if (((ListaInicales.IndexOf("-7-") + 1)== 0))
                    {
                        ListaInicales = (ListaInicales + "-7-");
                        BandasInicialesNombres.Add("INGRESOS");
                        TamFinal = 0;
                        for (int h = 0; (h <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "7"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "8"))
                {
                    if (((ListaInicales.IndexOf("-8-") + 1) == 0))
                    {
                        ListaInicales = (ListaInicales + "-8-");
                        BandasInicialesNombres.Add("SALDOS INTERMEDIARIOS DE GESTI�N");
                        TamFinal = 0;
                        for (int h = 0; (h <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "8"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "9"))
                {
                    if (((ListaInicales.IndexOf("-9-") + 1)  == 0))
                    {
                        ListaInicales = (ListaInicales + "-9-");
                        BandasInicialesNombres.Add("CUENTAS DE FUNCION DEL GASTO");
                        TamFinal = 0;
                        for (int h = 0; (h  <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "9"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }
                else if ((lstCls[i].Substring(1, 1) == "0"))
                {
                    if (((ListaInicales.IndexOf("-0-") + 1)  == 0))
                    {
                        ListaInicales = (ListaInicales + "-0-");
                        BandasInicialesNombres.Add("CUENTAS DE ORDEN");
                        TamFinal = 0;
                        for (int h = 0; (h <= (lstCls.Count - 1)); h++)
                        {
                            if ((lstCls[h].Substring(1, 1) == "0"))
                            {
                                TamFinal += 48.94;
                            }

                        }

                        BandasInicialesTamanios.Add(TamFinal);
                    }

                }

            }


            int i1 = BandasInicialesNombres.IndexOf("ACTIVOS");

            if ((i1 >= 0))
            {
                for (int i = 0; (i <= (lstCls.Count - 1)); i++)
                {
                    if ((lstCls[i].Substring(1, 1) == "2"))
                    {
                        Tamf2 += 48.94;
                    }

                    if ((lstCls[i].Substring(1, 1) == "3"))
                    {
                        Tamf3 += 48.94;
                    }

                }

                BandasInicialesTamanios[i1] = (BandasInicialesTamanios[i1] + (Tamf2 + Tamf3));
            }


            double Titarr = 438.11;


            for (int i = 0; (i== BandasInicialesNombres.Count - 1); i++)
            {
                DevExpress.XtraReports.UI.XRLabel XrLabelDinamic = new DevExpress.XtraReports.UI.XRLabel();
                XrLabelDinamic.Font = new System.Drawing.Font("Times New Roman", 6.0F);
                XrLabelDinamic.LocationFloat = new DevExpress.Utils.PointFloat(Convert.ToSingle(Titarr),86.45834F);
                XrLabelDinamic.Name = ("XrLabelTituloDinArr" + i);
                XrLabelDinamic.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0F);
                XrLabelDinamic.SizeF = new System.Drawing.SizeF(Convert.ToSingle(BandasInicialesTamanios[i]), 17.70834F);
                XrLabelDinamic.StylePriority.UseFont = false;
                XrLabelDinamic.StylePriority.UseTextAlignment = false;
                XrLabelDinamic.Text = BandasInicialesNombres[i];
                XrLabelDinamic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                XrLabelDinamic.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
                PageHeaderBand1.Controls.Add(XrLabelDinamic);
                Titarr = (Titarr + BandasInicialesTamanios[i]);
            }


            double Titloc = 389.17;
            for (int i = 0; (i   <= (lstCls.Count - 1)); i++)
            {
                Titloc += 48.94;
                DevExpress.XtraReports.UI.XRLabel XrLabelDinamic = new DevExpress.XtraReports.UI.XRLabel();
                XrLabelDinamic.Font = new System.Drawing.Font("Times New Roman", 6.0F);
                XrLabelDinamic.LocationFloat = new DevExpress.Utils.PointFloat(Convert.ToSingle(Titloc),104.1667F);
                XrLabelDinamic.Name = ("XrLabelTituloDin" + i);
                XrLabelDinamic.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0F);
                XrLabelDinamic.SizeF = new System.Drawing.SizeF(48.94F,20.75752F);
                XrLabelDinamic.StylePriority.UseFont = false;
                XrLabelDinamic.StylePriority.UseTextAlignment = false;
                XrLabelDinamic.Text = lstCls[i].Substring(1);
                XrLabelDinamic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                XrLabelDinamic.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) | DevExpress.XtraPrinting.BorderSide.Bottom)));
                PageHeaderBand1.Controls.Add(XrLabelDinamic);
            }

            DevExpress.XtraReports.UI.XRLabel XrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel7 = new DevExpress.XtraReports.UI.XRLabel();

            XrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FOLIO")});
            XrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0.0F, 0.0F);
            XrLabel2.Name = "XrLabel2";
            XrLabel2.Font = new System.Drawing.Font("Times New Roman", 6.0F);
            XrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            XrLabel2.SizeF = new System.Drawing.SizeF(63.13843F,23.0F);
            XrLabel2.StylePriority.UseFont = false;
            XrLabel2.Text = "XrLabel2";
            XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;


            XrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FECHA", "{0:dd/MM/yyyy}")});
            XrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(63.13843F, 0.0F);
            XrLabel5.Name = "XrLabel5";
            XrLabel5.Font = new System.Drawing.Font("Times New Roman", 6.0F);
            XrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            XrLabel5.SizeF = new System.Drawing.SizeF(Convert.ToSingle(46.16336),23);
            XrLabel5.StylePriority.UseFont = false;
            XrLabel5.Text = "XrLabel5";
            XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;


            XrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "GLOSA")});
            XrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(109.3018F, 0.0F);
            XrLabel7.Name = "XrLabel7";
            XrLabel7.Font = new System.Drawing.Font("Times New Roman", 6.0F);
            XrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0F);
            XrLabel7.SizeF = new System.Drawing.SizeF(328.805F, 23.0F);
            XrLabel7.StylePriority.UseFont = false;
            XrLabel7.Text = "XrLabel7";
            XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

            DevExpress.XtraReports.UI.DetailBand Detail = new DevExpress.XtraReports.UI.DetailBand();
            Detail.Controls.Add(XrLabel7);
            Detail.Controls.Add(XrLabel5);
            Detail.Controls.Add(XrLabel2);


            double Colloc = 389.17;
            for (int i = 3; (i <= (Dt.Columns.Count - 1)); i++)
            {
                Colloc += 48.94;
                DevExpress.XtraReports.UI.XRLabel XrLabelDinamic = new DevExpress.XtraReports.UI.XRLabel();
                XrLabelDinamic.Font = new System.Drawing.Font("Times New Roman", 6.0F);
                XrLabelDinamic.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, Dt.Columns[i].ColumnName)});
                XrLabelDinamic.LocationFloat = new DevExpress.Utils.PointFloat(Convert.ToSingle(Colloc), 0.0F);
                XrLabelDinamic.Name = ("XrLabelColumnDin" + i);
                XrLabelDinamic.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
                XrLabelDinamic.SizeF = new System.Drawing.SizeF(48.94F, 23.0F);
                XrLabelDinamic.StylePriority.UseFont = false;
                XrLabelDinamic.Text = "XrLabel2";
                XrLabelDinamic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;

                Detail.Controls.Add(XrLabelDinamic);
            }


            Detail.HeightF = 23.0F;
            Detail.Name = "Detail";
            Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0F);
            Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;


            DevExpress.XtraReports.UI.XRLabel XrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            DevExpress.XtraReports.UI.XRLabel XrLabel14 = new DevExpress.XtraReports.UI.XRLabel();

            XrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(149.5652F, 0.0F);
            XrLabel14.Name = "XrLabel14";
            XrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
            XrLabel14.SizeF = new System.Drawing.SizeF(288.5417F, 19.875F);
            XrLabel14.StylePriority.UseTextAlignment = false;
            XrLabel14.Text = "TOTALES";
            XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;


            DevExpress.XtraReports.UI.ReportFooterBand ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {XrLabel14});
            ReportFooter.HeightF = 22.91667F;
            ReportFooter.Name = "ReportFooter";


            double ColSum = 389.17;
            for (int i = 3; (i <= (Dt.Columns.Count - 1)); i++)
            {
                ColSum += 48.94;
                DevExpress.XtraReports.UI.XRLabel XrLabelDinamic = new DevExpress.XtraReports.UI.XRLabel();
                DevExpress.XtraReports.UI.XRSummary XrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
                XrLabelDinamic.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, Dt.Columns[i].ColumnName)});
                XrLabelDinamic.Font = new System.Drawing.Font("Times New Roman", 6.0F);
                XrLabelDinamic.LocationFloat = new DevExpress.Utils.PointFloat(Convert.ToSingle(ColSum), 0.0F);
                XrLabelDinamic.Name = ("XrLabelColumnSumf" + i);
                XrLabelDinamic.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0F);
                XrLabelDinamic.SizeF = new System.Drawing.SizeF(48.94F, 19.875F);
                XrSummary2.IgnoreNullValues = true;
                XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
                XrLabelDinamic.Summary = XrSummary2;
                XrLabelDinamic.Text = "XrLabel11";
                XrLabelDinamic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                ReportFooter.Controls.Add(XrLabelDinamic);
            }

            DevExpress.XtraReports.UI.XRLabel XrLabel10 = new DevExpress.XtraReports.UI.XRLabel();

            DevExpress.XtraReports.UI.PageFooterBand PageFooterBand1 = new DevExpress.XtraReports.UI.PageFooterBand();

            PageFooterBand1.HeightF =  65.4583F;
            PageFooterBand1.Name = "PageFooterBand1";
            PageFooterBand1.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter;


            Rpt.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            PageHeaderBand1,
            Detail,
            ReportFooter,
            PageFooterBand1});
            Rpt.Landscape = true;
            Rpt.PageHeight = 1268;
            Rpt.PageWidth = 1752;

            Rpt.DataSource = Dt;
            Rpt.ShowPreview();

            


        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\LibroDiarioSimplificado" + ".Xlsx");

            BandGridReport.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void txtmescod_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtmescod2_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmescod2.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod2.Text.Substring(txtmescod2.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod2.Text = Entidad.Id_Periodo;
                                txtmesdesc2.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod2.Text) & string.IsNullOrEmpty(txtmesdesc2.Text))
                    {
                        BuscarMes2();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarMes2()
        {
            try
            {
                txtmescod2.Text = Accion.Formato(txtmescod2.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod2.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod2.Text.Trim().ToUpper())
                        {
                            txtmescod2.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc2.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                    txtmescod2.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        List<Entidad_Reporte_Libro_Diario> ListPLE_v5 = new List<Entidad_Reporte_Libro_Diario>();
        List<Entidad_Reporte_Libro_Diario> ListPLE_V5_Detalle = new List<Entidad_Reporte_Libro_Diario>();


        bool VerificarElectronicoV5()
        {
            if (string.IsNullOrEmpty(txtmesdesc2.Text))
            {
                Accion.Advertencia("Ingresar el Periodo de Analisis");
                txtmescod.Focus();
                return false;
            }

            return true;
        }
        private void BtnVistaPr2_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarElectronicoV5() == true)
                {
                    Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
                    Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = txtmescod2.Text.Trim();


                    ListPLE_v5 = Log.ListarLibro_Diario_Formato_Simplificado_PLE_5_0(Ent);
             


                    if (ListPLE_v5.Count > 0)
                    {
                        DgvFormatoSimplificado02.DataSource = ListPLE_v5;
                    }
                    else
                    {
                        Accion.Advertencia("No se encontro ningun registro");
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        bool VerificarElectronicoV5_Detalle()
        {
            if (string.IsNullOrEmpty(txtmesdesc2.Text))
            {
                Accion.Advertencia("Ingresar el Periodo de Analisis");
                txtmescod.Focus();
                return false;
            }

            return true;
        }

        private void BtnElectronico_V5_VistaPrevia_Detalle_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarElectronicoV5_Detalle() == true)
                {
                    Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
                    Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = txtmescod2.Text.Trim();


                    ListPLE_V5_Detalle = Log.ListarLibro_Diario_Formato_Simplificado_PLE_5_0_Detalle_Plan(Ent);



                    if ((ListPLE_V5_Detalle.Count > 0))
                    {
                        gvElectronico_V5_Det.DataSource = ListPLE_V5_Detalle;
                        TabControl_V5_Reporte.SelectedIndex = 1;

                    }
                    else
                    {
                        Accion.Advertencia("No se encontro ningun registro");
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        string CondicionInformacion = "";
        private void btnple_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                if (TabControl_V5_Reporte.SelectedIndex == 0)
                {
                    if  (VerificarElectronicoV5())
                    {
                        FolderBrowserDialog fbd = new FolderBrowserDialog();
                    if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {

                        try
                        {

                                if (ListPLE_v5.Count > 0)
                                {
                                    CondicionInformacion = "1";
                                }
                                else
                                {
                                    CondicionInformacion = "0";
                                }

                                GenerarArchivoTxt_v5(ListPLE_v5, fbd.SelectedPath);
                            MessageBox.Show("Archivo de Texto del Libro o Registro se generó correctamente", "Resultado de Proceso", MessageBoxButtons.OK);

                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                                                           
                         }
                        
                    }
                  }

        
                }else
                {
                    if (VerificarElectronicoV5_Detalle())
                    {
                      FolderBrowserDialog fbd = new FolderBrowserDialog();
                        if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            try
                            {
                                if (ListPLE_V5_Detalle.Count > 0)
                                {
                                    CondicionInformacion = "1";
                                }
                                else
                                {
                                    CondicionInformacion = "0";
                                }
                                GenerarArchivoTxtDetalle_v5(ListPLE_V5_Detalle, fbd.SelectedPath);
                                MessageBox.Show("Archivo de Texto del Libro o Registro se generó correctamente", "Resultado de Proceso", MessageBoxButtons.OK);

                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }
                
                        }
                    }
                }
             }


        }


        void GenerarArchivoTxt_v5(List<Entidad_Reporte_Libro_Diario> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                //Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod.Text.Trim() + "00" + "050100" + "00" + "1" + CondicionInformacion + "1" + "1" + ".txt"));
                Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod2.Text.Trim()+ "00" + "050200" + "00" + "1" + CondicionInformacion + "1" + "1" + ".txt"));
                foreach (Entidad_Reporte_Libro_Diario txt in ColumnasVisibles)
                {
                    sb.Append((txt.ple_V5 + "\r\n"));
                }

                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile = new StreamWriter(Ruta))
                {
                    outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        void GenerarArchivoTxtDetalle_v5(List<Entidad_Reporte_Libro_Diario> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                Ruta = SeleccionPath + "\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod2.Text.Trim() + "00" + "050400" + "00" + "1" + CondicionInformacion + "1" + "1" + ".txt";

                foreach (Entidad_Reporte_Libro_Diario txt in ColumnasVisibles)
                {
                    sb.Append((txt.ple_V5 + "\r\n"));
                }

                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile = new StreamWriter(Ruta))
                {
                    outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }



    }
}
 


