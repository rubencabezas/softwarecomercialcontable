﻿using Contable.Reportes_Impresiones;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_estado_cambio_patrimonio_neto : frm_fuente
    {
        public frm_estado_cambio_patrimonio_neto()
        {
            InitializeComponent();
        }
        List<Entidad_Flujo_Efectivo> ListRC = new List<Entidad_Flujo_Efectivo>();
        private void btnverreporte_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Logica_Flujo_Efectivo Log = new Logica_Flujo_Efectivo();

                Entidad_Flujo_Efectivo Ent = new Entidad_Flujo_Efectivo();
                Ent.Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Anio = Actual_Conexion.AnioSelect;

                ListRC = Log.Patrimonio_Neto_Reporte(Ent);

                if (ListRC.Count > 0)
                {

                    dgvdatos.DataSource = ListRC;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (ListRC.Count > 0)
                {
                    Rpt_Patrimonio_Neto f = new Rpt_Patrimonio_Neto();



                    //Logica_Flujo_Efectivo Log = new Logica_Flujo_Efectivo();
                    //Entidad_en Ent = new Entidad_Cedula_Determinacion_IGV();
                    f.lblperiodo.Text = Actual_Conexion.AnioSelect;
                    f.lblrazon.Text = Actual_Conexion.EmpresaNombre;
                    f.lblruc.Text = Actual_Conexion.RucEmpresa;

                    f.DataSource = ListRC;
                    //f.Landscape = true;

                    dynamic ribbonPreview = new PrintPreviewFormEx();
                    f.CreateDocument();
                    f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    ribbonPreview.PrintingSystem = f.PrintingSystem;
                    //ribbonPreview.MdiParent =frm_principal;
                    ribbonPreview.Show();
                }
                else
                {
                    Accion.Advertencia("No existen datos para imprimir");
                }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ListRC.Clear();
                dgvdatos.DataSource = null;
               
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnplantilla_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Patrimonioneto" + ".Xlsx");

                XlsxExportOptionsEx ExXlsxExportOptions = new XlsxExportOptionsEx();
                ExXlsxExportOptions.ExportType = DevExpress.Export.ExportType.WYSIWYG;

                dgvdatos.ExportToXlsx(Ruta, ExXlsxExportOptions);
 
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
