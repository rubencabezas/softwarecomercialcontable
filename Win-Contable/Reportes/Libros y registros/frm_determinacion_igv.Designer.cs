﻿namespace Contable.Reportes.Libros_y_registros
{
    partial class frm_determinacion_igv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.btnverreporte = new DevExpress.XtraBars.BarButtonItem();
            this.btnimpresion = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnsaldoinicial2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvdatos
            // 
            this.dgvdatos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvdatos.Location = new System.Drawing.Point(0, 40);
            this.dgvdatos.MainView = this.advBandedGridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(1027, 321);
            this.dgvdatos.TabIndex = 4;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.advBandedGridView1.Appearance.BandPanel.Options.UseForeColor = true;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand1,
            this.gridBand5,
            this.gridBand8,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14});
            this.advBandedGridView1.ColumnPanelRowHeight = 35;
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn18,
            this.bandedGridColumn19,
            this.bandedGridColumn20,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumn23,
            this.bandedGridColumn24,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn27,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn38});
            this.advBandedGridView1.GridControl = this.dgvdatos;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.advBandedGridView1.OptionsView.ShowFooter = true;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.RowHeight = 20;
            // 
            // gridBand3
            // 
            this.gridBand3.Columns.Add(this.bandedGridColumn1);
            this.gridBand3.Columns.Add(this.bandedGridColumn2);
            this.gridBand3.Columns.Add(this.bandedGridColumn3);
            this.gridBand3.Columns.Add(this.bandedGridColumn4);
            this.gridBand3.Columns.Add(this.bandedGridColumn5);
            this.gridBand3.Columns.Add(this.bandedGridColumn6);
            this.gridBand3.Columns.Add(this.bandedGridColumn7);
            this.gridBand3.Columns.Add(this.bandedGridColumn8);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 1131;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn1.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn1.Caption = "PERIODO";
            this.bandedGridColumn1.FieldName = "Periodo";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 88;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn2.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn2.Caption = "EXPORTACIONES FACTURADAS Cas 106";
            this.bandedGridColumn2.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn2.FieldName = "EXPORTACIONES_FACTURADAS";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EXPORTACIONES_FACTURADAS", "{0:n2}")});
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 157;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn3.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn3.Caption = "VENTAS NO GRAVADAS Cas 105";
            this.bandedGridColumn3.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn3.FieldName = "VENTAS_NO_GRABADAS";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VENTAS_NO_GRABADAS", "{0:n2}")});
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 145;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn4.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn4.Caption = "VENTAS NETAS Cas 100";
            this.bandedGridColumn4.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn4.FieldName = "VENTAS_NETAS";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VENTAS_NETAS", "{0:n2}")});
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 130;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn5.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn5.Caption = "DEBITO FISCAL Cas 101";
            this.bandedGridColumn5.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn5.FieldName = "DEBITO_FISCAL";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DEBITO_FISCAL", "{0:n2}")});
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 118;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn6.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn6.Caption = "DESC. CONCED. Y/O DEV. DE VENTAS Cas 102";
            this.bandedGridColumn6.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn6.FieldName = "DESC_CONCED_DEV_VENTAS";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DESC_CONCED_DEV_VENTAS", "{0:n2}")});
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 197;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn7.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn7.Caption = "DEB. FISCAL FISCAL DESC. CONCED. Cas 103";
            this.bandedGridColumn7.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn7.FieldName = "DEB_FISCAL_FISCAL_DESC_CONCED";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DEB_FISCAL_FISCAL_DESC_CONCED", "{0:n2}")});
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 164;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn8.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn8.Caption = "TOTAL DEBITO FISCAL";
            this.bandedGridColumn8.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn8.FieldName = "TOTAL_DEBITO_FISCAL";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTAL_DEBITO_FISCAL", "{0:n2}")});
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 132;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand1.Caption = "COMPRAS DEST. A. OPE GRAVADAS ECLUSIVAS";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand4});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 316;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "NACIONALES";
            this.gridBand2.Columns.Add(this.bandedGridColumn9);
            this.gridBand2.Columns.Add(this.bandedGridColumn10);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 160;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn9.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn9.Caption = "BASE IMP 1 Cas 107";
            this.bandedGridColumn9.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn9.FieldName = "BASE_IMPONIBLE_NACIONAL";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BASE_IMPONIBLE_NACIONAL", "{0:n2}")});
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 76;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn10.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn10.Caption = "IGV 1 Cas 108";
            this.bandedGridColumn10.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn10.FieldName = "IGV_NACIONAL";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IGV_NACIONAL", "{0:n2}")});
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 84;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "IMPORTADAS";
            this.gridBand4.Columns.Add(this.bandedGridColumn11);
            this.gridBand4.Columns.Add(this.bandedGridColumn12);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 1;
            this.gridBand4.Width = 156;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn11.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn11.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn11.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn11.Caption = "BASE IMP I 1 Cas 114";
            this.bandedGridColumn11.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 91;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn12.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn12.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn12.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn12.Caption = "IGV I 1\tCas 115";
            this.bandedGridColumn12.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 65;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "COMPRAS DEST. A OPE GRAVADAS Y NO GRAV.";
            this.gridBand5.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6,
            this.gridBand7});
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 2;
            this.gridBand5.Width = 300;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "NACIONALES";
            this.gridBand6.Columns.Add(this.bandedGridColumn13);
            this.gridBand6.Columns.Add(this.bandedGridColumn14);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 0;
            this.gridBand6.Width = 150;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn13.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn13.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn13.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn13.Caption = "BASE IMP 2 Cas 110";
            this.bandedGridColumn13.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn13.FieldName = "BASE_IMPONIBLE_2_NACIONAL";
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BASE_IMPONIBLE_2_NACIONAL", "{0:n2}")});
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn14.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn14.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn14.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn14.Caption = "IGV 2 Cas 111";
            this.bandedGridColumn14.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn14.FieldName = "IGV_2_NACIONAL";
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IGV_2_NACIONAL", "{0:n2}")});
            this.bandedGridColumn14.Visible = true;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "IMPORTADAS";
            this.gridBand7.Columns.Add(this.bandedGridColumn15);
            this.gridBand7.Columns.Add(this.bandedGridColumn16);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 1;
            this.gridBand7.Width = 150;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn15.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn15.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn15.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn15.Caption = "BASE IMP I 2 Cas 116";
            this.bandedGridColumn15.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn16.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn16.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn16.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn16.Caption = "IGV I 2 Cas 117";
            this.bandedGridColumn16.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn16.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "COMPRAS DEST. A NO GRAVADAS";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand10,
            this.gridBand9});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 3;
            this.gridBand8.Width = 300;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "NACIONALES";
            this.gridBand10.Columns.Add(this.bandedGridColumn17);
            this.gridBand10.Columns.Add(this.bandedGridColumn18);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 0;
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn17.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn17.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn17.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn17.Caption = "BASE IMP 3 Cas 113";
            this.bandedGridColumn17.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn17.FieldName = "BASE_IMPONIBLE_3_NACIONAL";
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BASE_IMPONIBLE_3_NACIONAL", "{0:n2}")});
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn18.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn18.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn18.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn18.Caption = "IGV 3";
            this.bandedGridColumn18.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn18.FieldName = "IGV_3_NACIONAL";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IGV_3_NACIONAL", "{0:n2}")});
            this.bandedGridColumn18.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "IMPORTADAS";
            this.gridBand9.Columns.Add(this.bandedGridColumn19);
            this.gridBand9.Columns.Add(this.bandedGridColumn20);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 1;
            this.gridBand9.Width = 150;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn19.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn19.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn19.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn19.Caption = "BASE IMP I 3 Cas 119";
            this.bandedGridColumn19.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn20.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn20.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn20.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn20.Caption = "IGV I 3";
            this.bandedGridColumn20.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn20.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn20.Visible = true;
            // 
            // gridBand11
            // 
            this.gridBand11.Columns.Add(this.bandedGridColumn21);
            this.gridBand11.Columns.Add(this.bandedGridColumn22);
            this.gridBand11.Columns.Add(this.bandedGridColumn23);
            this.gridBand11.Columns.Add(this.bandedGridColumn24);
            this.gridBand11.Columns.Add(this.bandedGridColumn25);
            this.gridBand11.Columns.Add(this.bandedGridColumn26);
            this.gridBand11.Columns.Add(this.bandedGridColumn27);
            this.gridBand11.Columns.Add(this.bandedGridColumn28);
            this.gridBand11.Columns.Add(this.bandedGridColumn29);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 4;
            this.gridBand11.Width = 1272;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn21.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn21.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn21.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn21.Caption = "ADQU. NO GRADAS Cas 120 +122";
            this.bandedGridColumn21.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn21.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn21.FieldName = "ADQU_NO_GRADAS";
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn21.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ADQU_NO_GRADAS", "{0:n2}")});
            this.bandedGridColumn21.Visible = true;
            this.bandedGridColumn21.Width = 139;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn22.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn22.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn22.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn22.Caption = "IMPUESTO SELECTIVO AL CONSUMO";
            this.bandedGridColumn22.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn22.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn22.FieldName = "IMPUESTO_SELECTIVO_CONSUMO";
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn22.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IMPUESTO_SELECTIVO_CONSUMO", "{0:n2}")});
            this.bandedGridColumn22.Visible = true;
            this.bandedGridColumn22.Width = 167;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn23.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn23.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn23.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn23.Caption = "OTROS TRIBUTOS Y CARGO";
            this.bandedGridColumn23.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn23.FieldName = "OTROS_TRIBUTOS_CARGO";
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn23.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OTROS_TRIBUTOS_CARGO", "{0:n2}")});
            this.bandedGridColumn23.Visible = true;
            this.bandedGridColumn23.Width = 124;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn24.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn24.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn24.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn24.Caption = "CREDITO FISCAL DEL MES ANTERIOR";
            this.bandedGridColumn24.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn24.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn24.FieldName = "CREDITO_FISCAL_MES_ANTERIOR_X";
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn24.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CREDITO_FISCAL_MES_ANTERIOR_X", "{0:n2}")});
            this.bandedGridColumn24.Visible = true;
            this.bandedGridColumn24.Width = 134;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn25.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn25.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn25.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn25.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn25.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn25.Caption = "CREDITO FISCAL DISPONIBLE";
            this.bandedGridColumn25.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn25.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn25.FieldName = "CREDITO_FISCAL_DISPONIBLE";
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn25.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CREDITO_FISCAL_DISPONIBLE", "{0:n2}")});
            this.bandedGridColumn25.Visible = true;
            this.bandedGridColumn25.Width = 131;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn26.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn26.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn26.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn26.Caption = "CREDITO FISCAL DEL MES SIGUIENTE";
            this.bandedGridColumn26.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn26.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn26.FieldName = "CREDITO_FISCAL_MES_SIGUIENTE";
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn26.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CREDITO_FISCAL_MES_SIGUIENTE", "{0:n2}")});
            this.bandedGridColumn26.Visible = true;
            this.bandedGridColumn26.Width = 139;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn27.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn27.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn27.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn27.Caption = "IMPUESTO DETERMINADO";
            this.bandedGridColumn27.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn27.FieldName = "IMPUESTO_DETERMINADO";
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn27.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IMPUESTO_DETERMINADO", "{0:n2}")});
            this.bandedGridColumn27.Visible = true;
            this.bandedGridColumn27.Width = 127;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn28.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn28.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn28.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn28.Caption = "COMPENSACION DEL IGV";
            this.bandedGridColumn28.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn28.FieldName = "COMPENSACION_DEL_IGV";
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn28.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "COMPENSACION_DEL_IGV", "{0:n2}")});
            this.bandedGridColumn28.Visible = true;
            this.bandedGridColumn28.Width = 126;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn29.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn29.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn29.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn29.Caption = "DEVUELTO CON NCN EXPORTADO";
            this.bandedGridColumn29.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn29.FieldName = "DEVUELTO_CON_NCN_EXPORTADO";
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn29.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DEVUELTO_CON_NCN_EXPORTADO", "{0:n2}")});
            this.bandedGridColumn29.Visible = true;
            this.bandedGridColumn29.Width = 185;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "RETENCIONES";
            this.gridBand12.Columns.Add(this.bandedGridColumn30);
            this.gridBand12.Columns.Add(this.bandedGridColumn31);
            this.gridBand12.Columns.Add(this.bandedGridColumn32);
            this.gridBand12.Columns.Add(this.bandedGridColumn33);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 5;
            this.gridBand12.Width = 415;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn30.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn30.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn30.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn30.Caption = "PER DEL MES";
            this.bandedGridColumn30.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn30.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn30.FieldName = "PERCEPCIONES_DEL_MES";
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn30.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PERCEPCIONES_DEL_MES", "{0:n2}")});
            this.bandedGridColumn30.Visible = true;
            this.bandedGridColumn30.Width = 88;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn31.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn31.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn31.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn31.Caption = "PER DEL MES ANT";
            this.bandedGridColumn31.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn31.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn31.FieldName = "PERCEPCION_MES_ANTERIOR_X";
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn31.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PERCEPCION_MES_ANTERIOR_X", "{0:n2}")});
            this.bandedGridColumn31.Visible = true;
            this.bandedGridColumn31.Width = 88;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn32.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn32.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn32.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn32.Caption = "PER DEVUELTO O COMPENSADO";
            this.bandedGridColumn32.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn32.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn32.FieldName = "PERCEPCIONES_DEVUELTO_O_COMPESADO";
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn32.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PERCEPCIONES_DEVUELTO_O_COMPESADO", "{0:n2}")});
            this.bandedGridColumn32.Visible = true;
            this.bandedGridColumn32.Width = 141;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn33.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn33.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn33.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn33.Caption = "PER APLICADAS";
            this.bandedGridColumn33.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn33.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn33.FieldName = "PERCEPCIONES_APLICADAS";
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn33.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PERCEPCIONES_APLICADAS", "{0:n2}")});
            this.bandedGridColumn33.Visible = true;
            this.bandedGridColumn33.Width = 98;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "PERCEPCIONES";
            this.gridBand13.Columns.Add(this.bandedGridColumn34);
            this.gridBand13.Columns.Add(this.bandedGridColumn35);
            this.gridBand13.Columns.Add(this.bandedGridColumn36);
            this.gridBand13.Columns.Add(this.bandedGridColumn37);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 6;
            this.gridBand13.Width = 405;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn34.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn34.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn34.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn34.Caption = "RET DEL MES";
            this.bandedGridColumn34.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn34.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn34.FieldName = "RETENCIONES_DEL_MES";
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn34.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RETENCIONES_DEL_MES", "{0:n2}")});
            this.bandedGridColumn34.Visible = true;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn35.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn35.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn35.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn35.Caption = "RET DEL MES ANT";
            this.bandedGridColumn35.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn35.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn35.FieldName = "RETENCION_MES_ANTERIOR_X";
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn35.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RETENCION_MES_ANTERIOR_X", "{0:n2}")});
            this.bandedGridColumn35.Visible = true;
            this.bandedGridColumn35.Width = 99;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn36.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn36.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn36.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn36.Caption = "RET DEVUELTO O COMPENSADO";
            this.bandedGridColumn36.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn36.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn36.FieldName = "RETENCION_DEVUELTO_O_COMPESADO";
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn36.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RETENCION_DEVUELTO_O_COMPESADO", "{0:n2}")});
            this.bandedGridColumn36.Visible = true;
            this.bandedGridColumn36.Width = 142;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn37.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn37.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn37.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn37.Caption = "RET APLICADAS";
            this.bandedGridColumn37.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn37.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn37.FieldName = "RETENCION_APLICADAS";
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn37.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RETENCION_APLICADAS", "{0:n2}")});
            this.bandedGridColumn37.Visible = true;
            this.bandedGridColumn37.Width = 89;
            // 
            // gridBand14
            // 
            this.gridBand14.Columns.Add(this.bandedGridColumn38);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 7;
            this.gridBand14.Width = 129;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn38.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn38.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn38.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn38.Caption = "TRIBUTO POR PAGAR";
            this.bandedGridColumn38.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn38.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn38.FieldName = "TRIBUTO_POR_PAGAR";
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn38.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TRIBUTO_POR_PAGAR", "{0:n2}")});
            this.bandedGridColumn38.Visible = true;
            this.bandedGridColumn38.Width = 129;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnlimpiar,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2,
            this.btnverreporte,
            this.btnimpresion,
            this.barButtonItem1,
            this.btnsaldoinicial2});
            this.barManager1.MaxItemId = 8;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnverreporte),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnimpresion),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnsaldoinicial2)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 0;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // btnverreporte
            // 
            this.btnverreporte.Caption = "Ver Reporte";
            this.btnverreporte.Id = 4;
            this.btnverreporte.ImageOptions.Image = global::Contable.Properties.Resources.Bill_32px;
            this.btnverreporte.Name = "btnverreporte";
            this.btnverreporte.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnverreporte.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnverreporte_ItemClick);
            // 
            // btnimpresion
            // 
            this.btnimpresion.Caption = "Impresion";
            this.btnimpresion.Id = 5;
            this.btnimpresion.ImageOptions.Image = global::Contable.Properties.Resources.Print_24px;
            this.btnimpresion.Name = "btnimpresion";
            this.btnimpresion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnimpresion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnimpresion_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Eportar";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnsaldoinicial2
            // 
            this.btnsaldoinicial2.Caption = "Saldo Inicial";
            this.btnsaldoinicial2.Id = 7;
            this.btnsaldoinicial2.ImageOptions.Image = global::Contable.Properties.Resources.AperturaCajachica;
            this.btnsaldoinicial2.Name = "btnsaldoinicial2";
            this.btnsaldoinicial2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnsaldoinicial2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnsaldoinicial2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1027, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 361);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 321);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1027, 40);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 321);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_determinacion_igv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1027, 361);
            this.Controls.Add(this.dgvdatos);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_determinacion_igv";
            this.Text = "Determinacion del IGV";
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarButtonItem btnverreporte;
        private DevExpress.XtraBars.BarButtonItem btnimpresion;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnsaldoinicial2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}
