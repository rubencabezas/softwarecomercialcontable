﻿using Contable.Reportes_Impresiones;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_libro_mayor : Contable.frm_fuente
    {
        public frm_libro_mayor()
        {
            InitializeComponent();
        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        bool Verificar()
        {
            if (string.IsNullOrEmpty(txtmesdesc.Text))
            {
                Accion.Advertencia("Ingresar el Periodo de Analisis");
                txtmescod.Focus();
                return false;
            }
            else if ((Rbt_Centralizado.Checked == false) && ((Rbt_xLibro.Checked == false)))
            {
                Accion.Advertencia("Debe seleccionar el Tipo de Reporte a Generar");
                Rbt_Centralizado.Focus();
                Rbt_Centralizado.Checked = true;
                return false;
            }

            return true;
        }


        List<Entidad_Reporte_Libro_Diario> ListCta = new List<Entidad_Reporte_Libro_Diario>();
        public List<Entidad_Reporte_Libro_Diario> DatosRecibidos = new List<Entidad_Reporte_Libro_Diario>();

        private void btnbuscar_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Verificar() == true)
                {
                    Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
                    Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = txtmescod.Text.Trim();

                    if (rbmensual.Checked == true)
                    {
                        Ent.Acumulado = false;
                    }
                    else
                    {
                        Ent.Acumulado = true;
                    }


                    if (Rbt_Centralizado.Checked)
                    {
                        ListCta = Log.ListarLibroDiarioCentralizado(Ent);
                    }
                    else
                    {
                         ListCta = Log.ListarLibroDiario(Ent);
                    }

                    if ((ListCta.Count > 0))
                    {
                        BandGridReport.DataSource = ListCta;

                        var PrivView = from item in ListCta
                                       where item.Cuenta.Trim().StartsWith(txtcuenta.Text.Trim())
                                       select item;
                        DatosRecibidos = PrivView.ToList();
                        BandGridReport.DataSource = null;
                        CalcularSaldos(DatosRecibidos);
                        BandGridReport.DataSource = DatosRecibidos;

                    }
                    else
                    {
                        Accion.Advertencia("No se encontro ningun registro");
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }




        void CalcularSaldos(List<Entidad_Reporte_Libro_Diario> ListCta)
        {
            for (int i = 0; ( i <= (ListCta.Count - 1)); i++)
            {
                if ((i == 0))
                {
                    ListCta[i].XSaldoDebe = ListCta[i].DebeNacional;
                    ListCta[i].XSaldoHaber = ListCta[i].HaberNacional;

                }
                else if ((ListCta[(i - 1)].XSaldoDebe > 0))
                {
                    if ((ListCta[(i - 1)].XSaldoDebe  + (ListCta[i].DebeNacional - ListCta[i].HaberNacional) )> 0 )
                    {
                        ListCta[i].XSaldoDebe = (ListCta[(i - 1)].XSaldoDebe + (ListCta[i].DebeNacional - ListCta[i].HaberNacional));
                        ListCta[i].XSaldoHaber = 0;
                    }
                    else
                    {
                        ListCta[i].XSaldoHaber = ((ListCta[(i - 1)].XSaldoDebe + (ListCta[i].DebeNacional - ListCta[i].HaberNacional)) * -1);
                        ListCta[i].XSaldoDebe = 0;
                    }

                }
                else if ((ListCta[(i - 1)].XSaldoHaber - ListCta[i].DebeNacional)  + (ListCta[i].HaberNacional ) > 0)
                {
                    ListCta[i].XSaldoDebe = 0;
                    ListCta[i].XSaldoHaber = ((ListCta[(i - 1)].XSaldoHaber - ListCta[i].DebeNacional) + ListCta[i].HaberNacional);
                }
                else
                {
                    ListCta[i].XSaldoHaber = 0;
                    ListCta[i].XSaldoDebe = (((ListCta[(i - 1)].XSaldoHaber - ListCta[i].DebeNacional)  + ListCta[i].HaberNacional)  * -1);
                }

            }

        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Rbt_Centralizado.Checked = false;
            Rbt_xLibro.Checked = false;
            rbacumulado.Checked = false;
            rbmensual.Checked = false;
            BandGridReport.DataSource = null;
            txtmescod.ResetText();
            txtmesdesc.ResetText();
            txtcuenta.ResetText();
            //TxtCodLibro.Enabled = false;
            txtcuentadesc.ResetText();
            ListCta.Clear();
            txtmescod.Focus();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\LibroMAyor" + ".Xlsx");

            BandGridReport.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        string CondicionInformacion = "";
        private void btnple_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if ((Verificar() == true))
                {

                    Rbt_xLibro.Checked = true;
                    rbmensual.Checked = true;

                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    if ((fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK))
                    {
                        List<Entidad_Reporte_Libro_Diario> ListCta = new List<Entidad_Reporte_Libro_Diario>();
                        List<Entidad_Reporte_Libro_Diario> ListCta_Cuentas = new List<Entidad_Reporte_Libro_Diario>();

                        Logica_Reporte_Libro_Diario Log = new Logica_Reporte_Libro_Diario();
                        Entidad_Reporte_Libro_Diario Ent = new Entidad_Reporte_Libro_Diario();


                        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        Ent.Id_Anio = Actual_Conexion.AnioSelect;
                        Ent.Id_Periodo = txtmescod.Text.Trim();



                        ListCta = Log.Ple_V5(Ent);
                     

                        if (ListCta.Count > 0)
                        {
                            CondicionInformacion = "1";
                        }
                        else
                        {
                            CondicionInformacion = "0";
                        }

          

                        GenerarArchivoTxt_50(ListCta, fbd.SelectedPath);
               
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        void GenerarArchivoTxt_50(List<Entidad_Reporte_Libro_Diario> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod.Text.Trim() + "00" + "060100" + "00" + "1" + CondicionInformacion + "1" + "1" + ".txt"));
                foreach (Entidad_Reporte_Libro_Diario txt in ColumnasVisibles)
                {
                    sb.Append((txt.ple_V5 + "\r\n"));
                }

                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile = new StreamWriter(Ruta))
                {
                    outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (ListCta.Count > 0)
                {
                    Rpt_Libro_Mayor f = new Rpt_Libro_Mayor();


                    f.TxtPerAnio.Text = txtmesdesc.Text + " DEL " + Actual_Conexion.AnioSelect;
                    f.TxtRazonSocial.Text = Actual_Conexion.EmpresaNombre;
                    f.TxtRuc.Text = Actual_Conexion.RucEmpresa;

                    f.DataSource = ListCta;
                    //f.PaperKind = PaperKind.A4;
                    //f.Landscape = true;

                    dynamic ribbonPreview = new PrintPreviewFormEx();
                    f.CreateDocument();
                    f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    ribbonPreview.PrintingSystem = f.PrintingSystem;
                    // ribbonPreview.MdiParent = frm_principal;
                    ribbonPreview.Show();

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
