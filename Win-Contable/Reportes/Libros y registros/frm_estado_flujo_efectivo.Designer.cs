﻿namespace Contable.Reportes.Libros_y_registros
{
    partial class frm_estado_flujo_efectivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbmensual1 = new DevExpress.XtraEditors.CheckEdit();
            this.rbacumulado1 = new DevExpress.XtraEditors.CheckEdit();
            this.txtmesdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbmensual2 = new DevExpress.XtraEditors.CheckEdit();
            this.rbacumulado2 = new DevExpress.XtraEditors.CheckEdit();
            this.txtmesdesc2 = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.btnverreporte = new DevExpress.XtraBars.BarButtonItem();
            this.btnimpresion = new DevExpress.XtraBars.BarButtonItem();
            this.btnImprimirEstadoResultados = new DevExpress.XtraBars.BarButtonItem();
            this.btnexportar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbmensual1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbacumulado1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbmensual2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbacumulado2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbmensual1);
            this.groupBox1.Controls.Add(this.rbacumulado1);
            this.groupBox1.Controls.Add(this.txtmesdesc);
            this.groupBox1.Controls.Add(this.txtmescod);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Location = new System.Drawing.Point(12, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 53);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Periodo 01";
            // 
            // rbmensual1
            // 
            this.rbmensual1.EnterMoveNextControl = true;
            this.rbmensual1.Location = new System.Drawing.Point(142, 33);
            this.rbmensual1.Name = "rbmensual1";
            this.rbmensual1.Properties.Caption = "Mensual";
            this.rbmensual1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.rbmensual1.Size = new System.Drawing.Size(66, 20);
            this.rbmensual1.TabIndex = 10;
            // 
            // rbacumulado1
            // 
            this.rbacumulado1.EnterMoveNextControl = true;
            this.rbacumulado1.Location = new System.Drawing.Point(61, 33);
            this.rbacumulado1.Name = "rbacumulado1";
            this.rbacumulado1.Properties.Caption = "Acumulado";
            this.rbacumulado1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.rbacumulado1.Size = new System.Drawing.Size(75, 20);
            this.rbacumulado1.TabIndex = 9;
            // 
            // txtmesdesc
            // 
            this.txtmesdesc.Enabled = false;
            this.txtmesdesc.EnterMoveNextControl = true;
            this.txtmesdesc.Location = new System.Drawing.Point(106, 12);
            this.txtmesdesc.Name = "txtmesdesc";
            this.txtmesdesc.Size = new System.Drawing.Size(141, 20);
            this.txtmesdesc.TabIndex = 8;
            // 
            // txtmescod
            // 
            this.txtmescod.EnterMoveNextControl = true;
            this.txtmescod.Location = new System.Drawing.Point(65, 12);
            this.txtmescod.Name = "txtmescod";
            this.txtmescod.Size = new System.Drawing.Size(38, 20);
            this.txtmescod.TabIndex = 7;
            this.txtmescod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(36, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Mes:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbmensual2);
            this.groupBox2.Controls.Add(this.rbacumulado2);
            this.groupBox2.Controls.Add(this.txtmesdesc2);
            this.groupBox2.Controls.Add(this.txtmescod2);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Location = new System.Drawing.Point(270, 45);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 53);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Periodo 02";
            // 
            // rbmensual2
            // 
            this.rbmensual2.EnterMoveNextControl = true;
            this.rbmensual2.Location = new System.Drawing.Point(146, 33);
            this.rbmensual2.Name = "rbmensual2";
            this.rbmensual2.Properties.Caption = "Mensual";
            this.rbmensual2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.rbmensual2.Size = new System.Drawing.Size(66, 20);
            this.rbmensual2.TabIndex = 10;
            // 
            // rbacumulado2
            // 
            this.rbacumulado2.EnterMoveNextControl = true;
            this.rbacumulado2.Location = new System.Drawing.Point(65, 33);
            this.rbacumulado2.Name = "rbacumulado2";
            this.rbacumulado2.Properties.Caption = "Acumulado";
            this.rbacumulado2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.rbacumulado2.Size = new System.Drawing.Size(75, 20);
            this.rbacumulado2.TabIndex = 9;
            // 
            // txtmesdesc2
            // 
            this.txtmesdesc2.Enabled = false;
            this.txtmesdesc2.EnterMoveNextControl = true;
            this.txtmesdesc2.Location = new System.Drawing.Point(105, 11);
            this.txtmesdesc2.Name = "txtmesdesc2";
            this.txtmesdesc2.Size = new System.Drawing.Size(141, 20);
            this.txtmesdesc2.TabIndex = 8;
            // 
            // txtmescod2
            // 
            this.txtmescod2.EnterMoveNextControl = true;
            this.txtmescod2.Location = new System.Drawing.Point(64, 11);
            this.txtmescod2.Name = "txtmescod2";
            this.txtmescod2.Size = new System.Drawing.Size(38, 20);
            this.txtmescod2.TabIndex = 7;
            this.txtmescod2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod2_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(35, 14);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Mes:";
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatos.Location = new System.Drawing.Point(0, 105);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(885, 344);
            this.dgvdatos.TabIndex = 9;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 35;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "DESCRIPCION";
            this.gridColumn1.FieldName = "Flujo_Efectivo_Desc";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 392;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "EJERCICIO O PERIODO";
            this.gridColumn2.DisplayFormat.FormatString = "n2";
            this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn2.FieldName = "Montos";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 150;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "EJERCICIO O PERIODO 2";
            this.gridColumn3.DisplayFormat.FormatString = "n2";
            this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn3.FieldName = "Montos2";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 191;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnlimpiar,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2,
            this.btnverreporte,
            this.btnimpresion,
            this.btnImprimirEstadoResultados,
            this.btnexportar});
            this.barManager1.MaxItemId = 8;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnverreporte),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnimpresion),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnexportar)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 0;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // btnverreporte
            // 
            this.btnverreporte.Caption = "Ver Reporte";
            this.btnverreporte.Id = 4;
            this.btnverreporte.ImageOptions.Image = global::Contable.Properties.Resources.Bill_32px;
            this.btnverreporte.Name = "btnverreporte";
            this.btnverreporte.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnverreporte.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnverreporte_ItemClick);
            // 
            // btnimpresion
            // 
            this.btnimpresion.Caption = "Impresion";
            this.btnimpresion.Id = 5;
            this.btnimpresion.ImageOptions.Image = global::Contable.Properties.Resources.Print_24px;
            this.btnimpresion.Name = "btnimpresion";
            this.btnimpresion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnimpresion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnimpresion_ItemClick);
            // 
            // btnImprimirEstadoResultados
            // 
            this.btnImprimirEstadoResultados.Caption = "Impresion Estados de Resultado";
            this.btnImprimirEstadoResultados.Id = 6;
            this.btnImprimirEstadoResultados.ImageOptions.Image = global::Contable.Properties.Resources.Print_24px;
            this.btnImprimirEstadoResultados.Name = "btnImprimirEstadoResultados";
            this.btnImprimirEstadoResultados.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnexportar
            // 
            this.btnexportar.Caption = "Exportar";
            this.btnexportar.Id = 7;
            this.btnexportar.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.btnexportar.Name = "btnexportar";
            this.btnexportar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnexportar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnexportar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(885, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 450);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(885, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 410);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(885, 40);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 410);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_estado_flujo_efectivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 450);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.dgvdatos);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frm_estado_flujo_efectivo";
            this.Text = "Estado de Flujo de Efectivo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbmensual1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbacumulado1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbmensual2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbacumulado2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtmesdesc;
        private DevExpress.XtraEditors.TextEdit txtmescod;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.CheckEdit rbmensual2;
        private DevExpress.XtraEditors.CheckEdit rbacumulado2;
        private DevExpress.XtraEditors.TextEdit txtmesdesc2;
        private DevExpress.XtraEditors.TextEdit txtmescod2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit rbmensual1;
        private DevExpress.XtraEditors.CheckEdit rbacumulado1;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarButtonItem btnverreporte;
        private DevExpress.XtraBars.BarButtonItem btnimpresion;
        private DevExpress.XtraBars.BarButtonItem btnexportar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem btnImprimirEstadoResultados;
    }
}