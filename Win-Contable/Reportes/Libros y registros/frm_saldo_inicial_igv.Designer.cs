﻿namespace Contable.Reportes.Libros_y_registros
{
    partial class frm_saldo_inicial_igv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvdatos1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.BtnGuardarAnual = new DevExpress.XtraEditors.SimpleButton();
            this.BtnQuitarAnual = new DevExpress.XtraEditors.SimpleButton();
            this.BtnNuevoAnual = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEditarAnual = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtretmesanterior = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtpermesanterior = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtcreditofiscalmesanterior = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.BtnGuardarMensual = new DevExpress.XtraEditors.SimpleButton();
            this.BtnQuitarMensual = new DevExpress.XtraEditors.SimpleButton();
            this.BtnNuevoMensual = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEditarMensual = new DevExpress.XtraEditors.SimpleButton();
            this.txtdevueltoncnexportad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtcompensacionigv = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtmesdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtretaplicadas = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtperaplicadas = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtretmesanterior.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpermesanterior.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcreditofiscalmesanterior.Properties)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdevueltoncnexportad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcompensacionigv.Properties)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtretaplicadas.Properties)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtperaplicadas.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvdatos1);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(1, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(522, 485);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saldos Anuales";
            // 
            // dgvdatos1
            // 
            this.dgvdatos1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvdatos1.Location = new System.Drawing.Point(6, 249);
            this.dgvdatos1.MainView = this.gridView2;
            this.dgvdatos1.Name = "dgvdatos1";
            this.dgvdatos1.Size = new System.Drawing.Size(510, 230);
            this.dgvdatos1.TabIndex = 29;
            this.dgvdatos1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView2.GridControl = this.dgvdatos1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Año";
            this.gridColumn1.FieldName = "Anio";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 52;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Credito Fiscal del Mes Anterior";
            this.gridColumn2.FieldName = "Credito_Fiscal_Mes_Anterior";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 122;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Percepcion del Mes Anterior";
            this.gridColumn3.FieldName = "Per_Mes_Anterior";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 138;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "Retencion del Mes Anterior";
            this.gridColumn4.FieldName = "Ret_Mes_Anterior";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 93;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.BtnGuardarAnual);
            this.groupBox5.Controls.Add(this.BtnQuitarAnual);
            this.groupBox5.Controls.Add(this.BtnNuevoAnual);
            this.groupBox5.Controls.Add(this.BtnEditarAnual);
            this.groupBox5.Location = new System.Drawing.Point(6, 183);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(510, 60);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            // 
            // BtnGuardarAnual
            // 
            this.BtnGuardarAnual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardarAnual.Appearance.Options.UseFont = true;
            this.BtnGuardarAnual.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.BtnGuardarAnual.Location = new System.Drawing.Point(284, 15);
            this.BtnGuardarAnual.Name = "BtnGuardarAnual";
            this.BtnGuardarAnual.Size = new System.Drawing.Size(87, 37);
            this.BtnGuardarAnual.TabIndex = 9;
            this.BtnGuardarAnual.Text = "Añadir";
            this.BtnGuardarAnual.Click += new System.EventHandler(this.BtnGuardarAnual_Click);
            // 
            // BtnQuitarAnual
            // 
            this.BtnQuitarAnual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQuitarAnual.Appearance.Options.UseFont = true;
            this.BtnQuitarAnual.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.BtnQuitarAnual.Location = new System.Drawing.Point(191, 15);
            this.BtnQuitarAnual.Name = "BtnQuitarAnual";
            this.BtnQuitarAnual.Size = new System.Drawing.Size(87, 37);
            this.BtnQuitarAnual.TabIndex = 52;
            this.BtnQuitarAnual.Text = "Quitar";
            this.BtnQuitarAnual.Click += new System.EventHandler(this.BtnQuitarAnual_Click);
            // 
            // BtnNuevoAnual
            // 
            this.BtnNuevoAnual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevoAnual.Appearance.Options.UseFont = true;
            this.BtnNuevoAnual.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.BtnNuevoAnual.Location = new System.Drawing.Point(5, 15);
            this.BtnNuevoAnual.Name = "BtnNuevoAnual";
            this.BtnNuevoAnual.Size = new System.Drawing.Size(87, 37);
            this.BtnNuevoAnual.TabIndex = 10;
            this.BtnNuevoAnual.Text = "Nuevo";
            this.BtnNuevoAnual.Click += new System.EventHandler(this.BtnNuevoAnual_Click);
            // 
            // BtnEditarAnual
            // 
            this.BtnEditarAnual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEditarAnual.Appearance.Options.UseFont = true;
            this.BtnEditarAnual.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.BtnEditarAnual.Location = new System.Drawing.Point(98, 15);
            this.BtnEditarAnual.Name = "BtnEditarAnual";
            this.BtnEditarAnual.Size = new System.Drawing.Size(87, 37);
            this.BtnEditarAnual.TabIndex = 53;
            this.BtnEditarAnual.Text = "Editar";
            this.BtnEditarAnual.Click += new System.EventHandler(this.BtnEditarAnual_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.txtcreditofiscalmesanterior);
            this.groupBox2.Controls.Add(this.labelControl26);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(510, 158);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtretmesanterior);
            this.groupBox4.Controls.Add(this.labelControl2);
            this.groupBox4.Location = new System.Drawing.Point(251, 45);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(229, 56);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Retenciones";
            // 
            // txtretmesanterior
            // 
            this.txtretmesanterior.EnterMoveNextControl = true;
            this.txtretmesanterior.Location = new System.Drawing.Point(94, 19);
            this.txtretmesanterior.Name = "txtretmesanterior";
            this.txtretmesanterior.Properties.Mask.EditMask = "n";
            this.txtretmesanterior.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtretmesanterior.Size = new System.Drawing.Size(114, 20);
            this.txtretmesanterior.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 26);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(79, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Del Mes Anterior";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtpermesanterior);
            this.groupBox3.Controls.Add(this.labelControl1);
            this.groupBox3.Location = new System.Drawing.Point(6, 45);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(229, 56);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Percepciones";
            // 
            // txtpermesanterior
            // 
            this.txtpermesanterior.EnterMoveNextControl = true;
            this.txtpermesanterior.Location = new System.Drawing.Point(94, 19);
            this.txtpermesanterior.Name = "txtpermesanterior";
            this.txtpermesanterior.Properties.Mask.EditMask = "n";
            this.txtpermesanterior.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtpermesanterior.Size = new System.Drawing.Size(129, 20);
            this.txtpermesanterior.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(79, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Del Mes Anterior";
            // 
            // txtcreditofiscalmesanterior
            // 
            this.txtcreditofiscalmesanterior.EnterMoveNextControl = true;
            this.txtcreditofiscalmesanterior.Location = new System.Drawing.Point(171, 19);
            this.txtcreditofiscalmesanterior.Name = "txtcreditofiscalmesanterior";
            this.txtcreditofiscalmesanterior.Properties.Mask.EditMask = "n";
            this.txtcreditofiscalmesanterior.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtcreditofiscalmesanterior.Size = new System.Drawing.Size(100, 20);
            this.txtcreditofiscalmesanterior.TabIndex = 1;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(20, 22);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(145, 13);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Credito Fiscal del Mes Anterior";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dgvdatos);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.txtdevueltoncnexportad);
            this.groupBox6.Controls.Add(this.labelControl6);
            this.groupBox6.Controls.Add(this.txtcompensacionigv);
            this.groupBox6.Controls.Add(this.labelControl5);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Location = new System.Drawing.Point(529, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(522, 485);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Saldos Mensuales";
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvdatos.Location = new System.Drawing.Point(6, 249);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(510, 230);
            this.dgvdatos.TabIndex = 29;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn9,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn10});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Año";
            this.gridColumn5.FieldName = "Anio";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 52;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn9.Caption = "Periodo";
            this.gridColumn9.FieldName = "Periodo_Desc";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.Caption = "Per Aplicadas";
            this.gridColumn6.FieldName = "Per_Aplicadas";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 122;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.Caption = "Compensacion del Igv";
            this.gridColumn7.FieldName = "Compensacion_IGV";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 138;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.Caption = "Ret Aplicadas";
            this.gridColumn8.FieldName = "Ret_Aplicadas";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 93;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn10.Caption = "Devuelto CNC Exportad";
            this.gridColumn10.FieldName = "Devuelto_CNC_Exportad";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.BtnGuardarMensual);
            this.groupBox7.Controls.Add(this.BtnQuitarMensual);
            this.groupBox7.Controls.Add(this.BtnNuevoMensual);
            this.groupBox7.Controls.Add(this.BtnEditarMensual);
            this.groupBox7.Location = new System.Drawing.Point(6, 183);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(510, 60);
            this.groupBox7.TabIndex = 14;
            this.groupBox7.TabStop = false;
            // 
            // BtnGuardarMensual
            // 
            this.BtnGuardarMensual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardarMensual.Appearance.Options.UseFont = true;
            this.BtnGuardarMensual.ImageOptions.Image = global::Contable.Properties.Resources.add_det3;
            this.BtnGuardarMensual.Location = new System.Drawing.Point(284, 16);
            this.BtnGuardarMensual.Name = "BtnGuardarMensual";
            this.BtnGuardarMensual.Size = new System.Drawing.Size(87, 37);
            this.BtnGuardarMensual.TabIndex = 15;
            this.BtnGuardarMensual.Text = "Añadir";
            this.BtnGuardarMensual.Click += new System.EventHandler(this.BtnGuardarMensual_Click);
            // 
            // BtnQuitarMensual
            // 
            this.BtnQuitarMensual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQuitarMensual.Appearance.Options.UseFont = true;
            this.BtnQuitarMensual.ImageOptions.Image = global::Contable.Properties.Resources.delete_det3;
            this.BtnQuitarMensual.Location = new System.Drawing.Point(191, 16);
            this.BtnQuitarMensual.Name = "BtnQuitarMensual";
            this.BtnQuitarMensual.Size = new System.Drawing.Size(87, 37);
            this.BtnQuitarMensual.TabIndex = 52;
            this.BtnQuitarMensual.Text = "Quitar";
            this.BtnQuitarMensual.Click += new System.EventHandler(this.BtnQuitarMensual_Click);
            // 
            // BtnNuevoMensual
            // 
            this.BtnNuevoMensual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevoMensual.Appearance.Options.UseFont = true;
            this.BtnNuevoMensual.ImageOptions.Image = global::Contable.Properties.Resources.new_det3;
            this.BtnNuevoMensual.Location = new System.Drawing.Point(5, 16);
            this.BtnNuevoMensual.Name = "BtnNuevoMensual";
            this.BtnNuevoMensual.Size = new System.Drawing.Size(87, 37);
            this.BtnNuevoMensual.TabIndex = 16;
            this.BtnNuevoMensual.Text = "Nuevo";
            this.BtnNuevoMensual.Click += new System.EventHandler(this.BtnNuevoMensual_Click);
            // 
            // BtnEditarMensual
            // 
            this.BtnEditarMensual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEditarMensual.Appearance.Options.UseFont = true;
            this.BtnEditarMensual.ImageOptions.Image = global::Contable.Properties.Resources.edit_det3;
            this.BtnEditarMensual.Location = new System.Drawing.Point(98, 16);
            this.BtnEditarMensual.Name = "BtnEditarMensual";
            this.BtnEditarMensual.Size = new System.Drawing.Size(87, 37);
            this.BtnEditarMensual.TabIndex = 53;
            this.BtnEditarMensual.Text = "Editar";
            this.BtnEditarMensual.Click += new System.EventHandler(this.BtnEditarMensual_Click);
            // 
            // txtdevueltoncnexportad
            // 
            this.txtdevueltoncnexportad.EnterMoveNextControl = true;
            this.txtdevueltoncnexportad.Location = new System.Drawing.Point(163, 159);
            this.txtdevueltoncnexportad.Name = "txtdevueltoncnexportad";
            this.txtdevueltoncnexportad.Properties.Mask.EditMask = "n";
            this.txtdevueltoncnexportad.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtdevueltoncnexportad.Size = new System.Drawing.Size(100, 20);
            this.txtdevueltoncnexportad.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 162);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(134, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Devuelto con CNC Exportad";
            // 
            // txtcompensacionigv
            // 
            this.txtcompensacionigv.EnterMoveNextControl = true;
            this.txtcompensacionigv.Location = new System.Drawing.Point(163, 138);
            this.txtcompensacionigv.Name = "txtcompensacionigv";
            this.txtcompensacionigv.Properties.Mask.EditMask = "n";
            this.txtcompensacionigv.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtcompensacionigv.Size = new System.Drawing.Size(100, 20);
            this.txtcompensacionigv.TabIndex = 11;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 141);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(106, 13);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Compensacion del IGV";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox11);
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Controls.Add(this.groupBox10);
            this.groupBox8.Location = new System.Drawing.Point(6, 13);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(510, 120);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtmesdesc);
            this.groupBox11.Controls.Add(this.txtmescod);
            this.groupBox11.Controls.Add(this.labelControl7);
            this.groupBox11.Location = new System.Drawing.Point(7, 9);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(473, 44);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            // 
            // txtmesdesc
            // 
            this.txtmesdesc.Enabled = false;
            this.txtmesdesc.Location = new System.Drawing.Point(97, 14);
            this.txtmesdesc.Name = "txtmesdesc";
            this.txtmesdesc.Size = new System.Drawing.Size(355, 20);
            this.txtmesdesc.TabIndex = 3;
            // 
            // txtmescod
            // 
            this.txtmescod.Location = new System.Drawing.Point(48, 14);
            this.txtmescod.Name = "txtmescod";
            this.txtmescod.Size = new System.Drawing.Size(47, 20);
            this.txtmescod.TabIndex = 2;
            this.txtmescod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod_KeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(6, 16);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(19, 13);
            this.labelControl7.TabIndex = 1;
            this.labelControl7.Text = "Mes";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtretaplicadas);
            this.groupBox9.Controls.Add(this.labelControl3);
            this.groupBox9.Location = new System.Drawing.Point(251, 58);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(229, 56);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Retenciones";
            // 
            // txtretaplicadas
            // 
            this.txtretaplicadas.EnterMoveNextControl = true;
            this.txtretaplicadas.Location = new System.Drawing.Point(94, 19);
            this.txtretaplicadas.Name = "txtretaplicadas";
            this.txtretaplicadas.Properties.Mask.EditMask = "n";
            this.txtretaplicadas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtretaplicadas.Size = new System.Drawing.Size(114, 20);
            this.txtretaplicadas.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(9, 26);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(45, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Aplicadas";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtperaplicadas);
            this.groupBox10.Controls.Add(this.labelControl4);
            this.groupBox10.Location = new System.Drawing.Point(6, 57);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(229, 56);
            this.groupBox10.TabIndex = 4;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Percepciones";
            // 
            // txtperaplicadas
            // 
            this.txtperaplicadas.EnterMoveNextControl = true;
            this.txtperaplicadas.Location = new System.Drawing.Point(94, 19);
            this.txtperaplicadas.Name = "txtperaplicadas";
            this.txtperaplicadas.Properties.Mask.EditMask = "n";
            this.txtperaplicadas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtperaplicadas.Size = new System.Drawing.Size(129, 20);
            this.txtperaplicadas.TabIndex = 6;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(9, 26);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(45, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "Aplicadas";
            // 
            // frm_saldo_inicial_igv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 485);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_saldo_inicial_igv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Saldo Inicial";
            this.Load += new System.EventHandler(this.frm_saldo_inicial_igv_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtretmesanterior.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpermesanterior.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcreditofiscalmesanterior.Properties)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtdevueltoncnexportad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcompensacionigv.Properties)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtretaplicadas.Properties)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtperaplicadas.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.TextEdit txtretmesanterior;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.TextEdit txtpermesanterior;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtcreditofiscalmesanterior;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        public DevExpress.XtraEditors.SimpleButton BtnGuardarAnual;
        public DevExpress.XtraEditors.SimpleButton BtnQuitarAnual;
        public DevExpress.XtraEditors.SimpleButton BtnNuevoAnual;
        public DevExpress.XtraEditors.SimpleButton BtnEditarAnual;
        private DevExpress.XtraGrid.GridControl dgvdatos1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private System.Windows.Forms.GroupBox groupBox6;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private System.Windows.Forms.GroupBox groupBox7;
        public DevExpress.XtraEditors.SimpleButton BtnGuardarMensual;
        public DevExpress.XtraEditors.SimpleButton BtnQuitarMensual;
        public DevExpress.XtraEditors.SimpleButton BtnNuevoMensual;
        public DevExpress.XtraEditors.SimpleButton BtnEditarMensual;
        private DevExpress.XtraEditors.TextEdit txtdevueltoncnexportad;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtcompensacionigv;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private DevExpress.XtraEditors.TextEdit txtretaplicadas;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.GroupBox groupBox10;
        private DevExpress.XtraEditors.TextEdit txtperaplicadas;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.GroupBox groupBox11;
        private DevExpress.XtraEditors.TextEdit txtmesdesc;
        private DevExpress.XtraEditors.TextEdit txtmescod;
        private DevExpress.XtraEditors.LabelControl labelControl7;
    }
}