﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_saldo_inicial_igv : frm_fuente
    {
        public frm_saldo_inicial_igv()
        {
            InitializeComponent();
        }

         List<Entidad_Determinacion_Igv> Lista = new List<Entidad_Determinacion_Igv>();
         List<Entidad_Determinacion_Igv> Lista1 = new List<Entidad_Determinacion_Igv>();

         Entidad_Determinacion_Igv Entidad = new Entidad_Determinacion_Igv();

         Entidad_Determinacion_Igv Entidad1 = new Entidad_Determinacion_Igv();

         string Estado_Anual;
         string Estado_Mensual;
         string Empresa;
         string Anio;
         string Periodo;











        private void frm_saldo_inicial_igv_Load(object sender, EventArgs e)
        {
            Listar();
            Listar1();
            Bloquear1();
            Bloquear();
            BtnEditarMensual.Enabled = false;
            BtnQuitarMensual.Enabled = false;
            BtnGuardarMensual.Enabled = false;
            BtnEditarAnual.Enabled = false;
            BtnQuitarAnual.Enabled = false;
            BtnGuardarAnual.Enabled = false;

        }

        private void Bloquear()
        {
            txtmescod.Enabled = false;
            txtperaplicadas.Enabled = false;
            txtretaplicadas.Enabled = false;
            txtcompensacionigv.Enabled = false;
            txtdevueltoncnexportad.Enabled = false;
        }


        private void Bloquear1()
        {
            txtcreditofiscalmesanterior.Enabled = false;
            txtpermesanterior.Enabled = false;
            txtretmesanterior.Enabled = false;
        }


        private void Desbloquear()
        {
            txtmescod.Enabled = false;
            txtperaplicadas.Enabled = true;
            txtretaplicadas.Enabled = true;
            txtcompensacionigv.Enabled = true;
            txtdevueltoncnexportad.Enabled = true;
        }



        private void Desbloquear1()
        {
            txtcreditofiscalmesanterior.Enabled = true;
            txtpermesanterior.Enabled = true;
            txtretmesanterior.Enabled = true;
        }




        private void Limpiar()
        {
            txtmescod.ResetText();
            txtmesdesc.ResetText();
            txtperaplicadas.ResetText();
            txtretaplicadas.ResetText();
            txtcompensacionigv.ResetText();
            txtdevueltoncnexportad.ResetText();
        }

        private void Limpiar1()
        {
            txtcreditofiscalmesanterior.ResetText();
            txtpermesanterior.ResetText();
            txtretmesanterior.ResetText();
        }


        public bool VerificarCabecera()
        {
            return true;
        }



        public bool VerificarCabecera1()
        {
            return true;
        }


 





        public void Listar()
        {
            Entidad_Determinacion_Igv igv = new Entidad_Determinacion_Igv();
            Logica_Determinacion_Igv igv2 = new Logica_Determinacion_Igv();
            //Lista.Clear();
            dgvdatos.DataSource = null;
            igv.Empresa = Actual_Conexion.CodigoEmpresa;
            igv.Anio = Actual_Conexion.AnioSelect;
            try
            {
                Lista = igv2.Listar_Mensual(igv);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        public void Listar1()
        {
            Entidad_Determinacion_Igv igv = new Entidad_Determinacion_Igv();
            Logica_Determinacion_Igv igv2 = new Logica_Determinacion_Igv();
            //Lista1.Clear();
            dgvdatos1.DataSource = null;
            igv.Empresa = Actual_Conexion.CodigoEmpresa;
            igv.Anio = Actual_Conexion.AnioSelect;
            try
            {
                Lista1 = igv2.Listar_Anual(igv);
                if (Lista1.Count > 0)
                {
                    dgvdatos1.DataSource = Lista1;
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }



 


        private void BtnEditarAnual_Click(object sender, EventArgs e)
        {
            Estado_Anual = "2";
            Desbloquear1();
            BtnGuardarAnual.Enabled = true;

        }

        private void BtnEditarMensual_Click(object sender, EventArgs e)
        {
            Estado_Mensual = "2";
            Desbloquear();
            BtnGuardarMensual.Enabled = true;

        }

        private void BtnGuardarAnual_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarCabecera1())
                {
                    Entidad_Determinacion_Igv igv = new Entidad_Determinacion_Igv();
                    Logica_Determinacion_Igv igv2 = new Logica_Determinacion_Igv();
                    if (Estado_Anual == "1")
                    {
                        igv.Empresa = Actual_Conexion.CodigoEmpresa;
                        igv.Anio = Actual_Conexion.AnioSelect;
                        igv.Credito_Fiscal_Mes_Anterior = Convert.ToDecimal(IIf(txtcreditofiscalmesanterior.Text == "", 0, txtcreditofiscalmesanterior.Text));
                        igv.Per_Mes_Anterior = Convert.ToDecimal(IIf(txtpermesanterior.Text == "", 0, txtpermesanterior.Text));
                        igv.Ret_Mes_Anterior = Convert.ToDecimal(IIf(txtretmesanterior.Text == "", 0, txtretmesanterior.Text));
                        if (igv2.Insertar_Anual(igv))
                        {
                            Listar1();
                            Limpiar1();
                            Accion.ExitoGuardar();
                            BtnEditarAnual.Enabled = false;
                            BtnQuitarAnual.Enabled = false;
                            BtnGuardarAnual.Enabled = false;
                        }
                    }
                    else if (Estado_Anual == "2")
                    {
                        igv.Empresa = Entidad1.Empresa;
                        igv.Anio = Entidad1.Anio;
                        igv.Credito_Fiscal_Mes_Anterior = Convert.ToDecimal(txtcreditofiscalmesanterior.Text);
                        igv.Per_Mes_Anterior = Convert.ToDecimal(IIf(txtpermesanterior.Text == "", 0, txtpermesanterior.Text));
                        igv.Ret_Mes_Anterior = Convert.ToDecimal(IIf(txtretmesanterior.Text == "", 0, txtretmesanterior.Text));
                        if (igv2.Modificar_Anual(igv))
                        {
                            Listar1();
                            Limpiar1();
                            Bloquear1();
                            Accion.ExitoModificar();
                            BtnEditarAnual.Enabled = false;
                            BtnQuitarAnual.Enabled = false;
                            BtnGuardarAnual.Enabled = false;
                            BtnNuevoAnual.Enabled = true;
                            BtnNuevoAnual.Focus();
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void BtnGuardarMensual_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {
                    Entidad_Determinacion_Igv igv = new Entidad_Determinacion_Igv();
                    Logica_Determinacion_Igv igv2 = new Logica_Determinacion_Igv();
                    if (Estado_Mensual == "1")
                    {
                        igv.Empresa = Actual_Conexion.CodigoEmpresa;
                        igv.Anio = Actual_Conexion.AnioSelect;
                        igv.Periodo = txtmescod.Text.Trim();
                        igv.Per_Aplicadas = Convert.ToDecimal(IIf(txtperaplicadas.Text == "", 0, txtperaplicadas.Text));
                        igv.Ret_Aplicadas = Convert.ToDecimal(IIf(txtretaplicadas.Text == "", 0, txtretaplicadas.Text));
                        igv.Compensacion_IGV = Convert.ToDecimal(IIf(txtcompensacionigv.Text == "", 0, txtcompensacionigv.Text));
                        igv.Devuelto_CNC_Exportad = Convert.ToDecimal(IIf(txtdevueltoncnexportad.Text == "", 0, txtdevueltoncnexportad.Text));
                        if (igv2.Insertar_Mensual(igv))
                        {
                            Listar();
                            Accion.ExitoGuardar();
                            base.Estado = Estados.Nuevo;
                            Limpiar();
                            BtnEditarMensual.Enabled = false;
                            BtnQuitarMensual.Enabled = false;
                            BtnGuardarMensual.Enabled = false;
                        }
                    }
                    else if (Estado_Mensual == "2")
                    {
                        igv.Empresa = Entidad.Empresa;
                        igv.Anio = Entidad.Anio;
                        igv.Periodo = Entidad.Periodo;
                        igv.Per_Aplicadas = Convert.ToDecimal(IIf(txtperaplicadas.Text == "", 0, txtperaplicadas.Text));
                        igv.Ret_Aplicadas = Convert.ToDecimal(IIf(txtretaplicadas.Text == "", 0, txtretaplicadas.Text));
                        igv.Compensacion_IGV = Convert.ToDecimal(IIf(txtcompensacionigv.Text == "", 0, txtcompensacionigv.Text));
                        igv.Devuelto_CNC_Exportad = Convert.ToDecimal(IIf(txtdevueltoncnexportad.Text == "", 0, txtdevueltoncnexportad.Text));
                        if (igv2.Modificar_Mensual(igv))
                        {
                            Listar();
                            Limpiar();
                            Accion.ExitoModificar();
                            Bloquear();
                            BtnEditarMensual.Enabled = false;
                            BtnQuitarMensual.Enabled = false;
                            BtnGuardarMensual.Enabled = false;
                            BtnNuevoMensual.Enabled = true;
                            BtnNuevoMensual.Focus();
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void BtnNuevoAnual_Click(object sender, EventArgs e)
        {
            Estado_Anual = "1";
            Desbloquear1();
            Limpiar1();
            BtnGuardarAnual.Enabled = true;
            txtcreditofiscalmesanterior.Select();

        }

        private void BtnNuevoMensual_Click(object sender, EventArgs e)
        {
            Estado_Mensual = "1";
            Limpiar();
            Desbloquear();
            txtmescod.Enabled = true;
            BtnGuardarMensual.Enabled = true;
            txtmescod.Focus();
        }

        private void BtnQuitarAnual_Click(object sender, EventArgs e)
        {
            if (Accion.ShowDeleted("EL registro a ELIMINAR tiene la caraterisctica:" + Entidad1.Empresa + " " + Entidad1.Anio))
            {
                try
                {
                    Entidad_Determinacion_Igv igv1 = new Entidad_Determinacion_Igv();
                    igv1.Empresa = Entidad1.Empresa;
                    igv1.Anio = Entidad1.Anio;
                    new Logica_Determinacion_Igv().Eliminar_Anual(igv1);
                    Accion.ExitoGuardar();
                    Listar1();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void BtnQuitarMensual_Click(object sender, EventArgs e)
        {
            string[] textArray1 = new string[] { "EL registro a ELIMINAR tiene la caraterisctica:", Entidad.Empresa, " ", Entidad.Anio, " ", Entidad.Periodo };
            if (Accion.ShowDeleted(string.Concat(textArray1)))
            {
                try
                {
                    Entidad_Determinacion_Igv igv1 = new Entidad_Determinacion_Igv();
                    igv1.Empresa = Entidad.Empresa;
                    igv1.Anio = Entidad.Anio;
                    igv1.Periodo = Entidad.Periodo;
                    new Logica_Determinacion_Igv().Eliminar_Mensual(igv1);
                    Accion.ExitoGuardar();
                    Listar();
                }
                catch (Exception exception1)
                {
                    Accion.ErrorSistema(exception1.Message);
                }
            }

        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                    Empresa = Entidad.Empresa;
                    Anio = Entidad.Anio;
                    Periodo = Entidad.Periodo;
                    txtmescod.Text = Entidad.Periodo;
                    txtmesdesc.Text = Entidad.Periodo_Desc;
                    txtperaplicadas.Text = Convert.ToString(Entidad.Per_Aplicadas);
                    txtretaplicadas.Text = Convert.ToString(Entidad.Ret_Aplicadas);
                    txtcompensacionigv.Text = Convert.ToString(Entidad.Compensacion_IGV);
                    txtdevueltoncnexportad.Text = Convert.ToString(Entidad.Devuelto_CNC_Exportad);
                    BtnEditarMensual.Enabled = true;
                    BtnQuitarMensual.Enabled = true;
                    txtmescod.Enabled = false;
                    Bloquear();
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (Lista1.Count > 0)
                {
                    Entidad1 = Lista1[gridView2.GetFocusedDataSourceRowIndex()];
                    Empresa = Entidad1.Empresa;
                    Anio = Entidad1.Anio;
                    txtcreditofiscalmesanterior.Text = Convert.ToString(Entidad1.Credito_Fiscal_Mes_Anterior);
                    txtpermesanterior.Text = Convert.ToString(Entidad1.Per_Mes_Anterior);
                    txtretmesanterior.Text = Convert.ToString(Entidad1.Ret_Mes_Anterior);
                    BtnEditarAnual.Enabled = true;
                    BtnQuitarAnual.Enabled = true;
                    Bloquear1();
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private object IIf(bool expression, object truePart, object falsePart)
        {
            return (expression ? truePart : falsePart);
        }





    }
}
