﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPrinting.Preview;
using System.IO;
using Contable.Reportes.Reportes_Impresiones;
using Contable.Reportes_Impresiones;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_registro_compras : Contable.frm_fuente
    {
        public frm_registro_compras()
        {
            InitializeComponent();
        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmescod_TextChanged(object sender, EventArgs e)
        {
            if (txtmescod.Focus() == false)
            {
                txtmesdesc.ResetText();
            }
        }


     List<Entidad_Registro_Compra_Reporte> ListRC = new List<Entidad_Registro_Compra_Reporte>();
        private void btnvistaprevia_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
   if (VerificarCabecera())
            {
                Rpt_Registo_Compras f = new Rpt_Registo_Compras();
           

                Logica_Registro_Compra_Reporte Log = new Logica_Registro_Compra_Reporte();

                Entidad_Registro_Compra_Reporte Ent = new Entidad_Registro_Compra_Reporte();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;
                Ent.Id_Periodo = txtmescod.Text;


                ListRC = Log.Listar(Ent);

                    f.lblperiodo.Text = txtmesdesc.Text +" DEL "+ Actual_Conexion.AnioSelect;
                    f.lblmoneda.Text = "SOLES";
                    f.lblrazonsocial.Text = Actual_Conexion.EmpresaNombre;
                    f.lblruc.Text = Actual_Conexion.RucEmpresa;

                f.DataSource = ListRC;
                //f.PaperKind = PaperKind.A4;
                f.Landscape = true;

                dynamic ribbonPreview = new PrintPreviewFormEx();
                f.CreateDocument();
                f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                ribbonPreview.PrintingSystem = f.PrintingSystem;
               // ribbonPreview.MdiParent = frm_principal;
                ribbonPreview.Show();

            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
         
        }

        public bool VerificarCabecera()
        {
            //if (string.IsNullOrEmpty(txtmesdesc.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una periodo");
            //    txtmescod.Focus();
            //    return false;
            //}

            return true;
        }

        private void txtmescod_TabStopChanged(object sender, EventArgs e)
        {

        }

        string CondicionInformacion = "";
string CondicionInformacion2 = "";
        private void btngenerarple_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera() == true)
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    if ((fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK))
                    {
                        List<Entidad_Registro_Compra_Reporte> ListCta = new List<Entidad_Registro_Compra_Reporte>();
                        List<Entidad_Registro_Compra_Reporte> ListCta_NoDomiciliado = new List<Entidad_Registro_Compra_Reporte>();

                        Logica_Registro_Compra_Reporte Log = new Logica_Registro_Compra_Reporte();
                        Entidad_Registro_Compra_Reporte Ent = new Entidad_Registro_Compra_Reporte();


                        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        Ent.Id_Anio = Actual_Conexion.AnioSelect;
                        Ent.Id_Periodo = txtmescod.Text.Trim();

                

                        ListCta = Log.Ple_V5(Ent);
     
                        if (ListCta.Count > 0)
                        {
                            CondicionInformacion = "1";
                        }
                        else
                        {
                            CondicionInformacion = "0";
                        }

                        if (ListCta_NoDomiciliado.Count > 0)
                        {
                            CondicionInformacion2 = "1";
                        }
                        else
                        {
                            CondicionInformacion2 = "0";
                        }

                        GenerarArchivoTxt_50(ListCta, fbd.SelectedPath);
                        GenerarArchivoTxt_50_NoDomiciliado(ListCta_NoDomiciliado, fbd.SelectedPath);
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        void GenerarArchivoTxt_50(List<Entidad_Registro_Compra_Reporte> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod.Text.Trim() + "00" + "080100" + "00" + "1" + CondicionInformacion + "1" + "1" + ".txt"));
                foreach (Entidad_Registro_Compra_Reporte txt in ColumnasVisibles)
                {
                    sb.Append((txt.Ple_V5 + "\r\n"));
                }

                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile = new StreamWriter(Ruta))
                {
                    outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        void GenerarArchivoTxt_50_NoDomiciliado(List<Entidad_Registro_Compra_Reporte> ColumnasVisibles, string SeleccionPath)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Ruta;
                Ruta = (SeleccionPath + ("\\LE" + Actual_Conexion.RucEmpresa.Trim() + Actual_Conexion.AnioSelect + txtmescod.Text.Trim() + "00" + "080200" + "00" + "1" + CondicionInformacion2 + "1" + "1" + ".txt"));
   
                if (File.Exists(Ruta))
                {
                    File.Delete(Ruta);
                }

                using (StreamWriter outfile = new StreamWriter(Ruta))
                {
                    outfile.Write(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
