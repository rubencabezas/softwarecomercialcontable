﻿using Contable._1_Busquedas_Generales;
using Contable.Reportes.Reportes_Impresiones;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_estado_flujo_efectivo : frm_fuente
    {
        public frm_estado_flujo_efectivo()
        {
            InitializeComponent();
        }

          List<Entidad_Flujo_Efectivo> ListRC = new List<Entidad_Flujo_Efectivo>();
        private bool Acumulado;
        private bool Acumulado2;




        private void btnverreporte_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Logica_Flujo_Efectivo efectivo = new Logica_Flujo_Efectivo();
                Entidad_Flujo_Efectivo efectivo2 = new Entidad_Flujo_Efectivo
                {
                    Empresa = Actual_Conexion.CodigoEmpresa,
                    Anio = Actual_Conexion.AnioSelect
                };
                if (this.rbacumulado1.Checked)
                {
                    this.Acumulado = true;
                }
                else
                {
                    this.Acumulado = false;
                }
                if (this.rbacumulado2.Checked)
                {
                    this.Acumulado2 = true;
                }
                else
                {
                    this.Acumulado2 = false;
                }
                efectivo2.Acumulado = this.Acumulado;
                efectivo2.Acumulado2 = this.Acumulado2;
                efectivo2.Periodo01 = this.txtmescod.Text;
                efectivo2.Periodo02 = this.txtmescod2.Text;
                this.ListRC = efectivo.Flujo_Efectivo_Reporte(efectivo2);
                if (this.ListRC.Count > 0)
                {
                    this.dgvdatos.DataSource = this.ListRC;
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }

        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtmescod.Text))
            {
                try
                {
                    if ((e.KeyCode == Keys.Enter) & (this.txtmescod.Text.Substring(this.txtmescod.Text.Length - 1, 1) == "*"))
                    {
                        using (frm_periodo_busqueda _busqueda = new frm_periodo_busqueda())
                        {
                            _busqueda.empresa = Actual_Conexion.CodigoEmpresa;
                            _busqueda.anio = Actual_Conexion.AnioSelect;
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio ejercicio = new Entidad_Ejercicio();
                                ejercicio = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                this.txtmescod.Text = ejercicio.Id_Periodo;
                                this.txtmesdesc.Text = ejercicio.Descripcion_Periodo;
                            }
                        }
                    }
                    else if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty(this.txtmescod.Text)) & string.IsNullOrEmpty(this.txtmesdesc.Text))
                    {
                        this.BuscarMes();
                    }
                }
                catch (Exception exception)
                {
                    Accion.ErrorSistema(exception.Message);
                }
            }

        }


        public void BuscarMes()
        {
            try
            {
                this.txtmescod.Text = Accion.Formato(this.txtmescod.Text, 2);
                Logica_Ejercicio ejercicio = new Logica_Ejercicio();
                List<Entidad_Ejercicio> list = new List<Entidad_Ejercicio>();
                Entidad_Ejercicio ejercicio1 = new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = this.txtmescod.Text
                };
                list = ejercicio.Listar_Periodo(ejercicio1);
                if (list.Count > 0)
                {
                    foreach (Entidad_Ejercicio ejercicio2 in list)
                    {
                        if (ejercicio2.Id_Periodo.ToString().Trim().ToUpper() == this.txtmescod.Text.Trim().ToUpper())
                        {
                            this.txtmescod.Text = ejercicio2.Id_Periodo.ToString().Trim();
                            this.txtmesdesc.Text = ejercicio2.Descripcion_Periodo;
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }
        }


        public void BuscarMes02()
        {
            try
            {
                this.txtmescod2.Text = Accion.Formato(this.txtmescod2.Text, 2);
                Logica_Ejercicio ejercicio = new Logica_Ejercicio();
                List<Entidad_Ejercicio> list = new List<Entidad_Ejercicio>();
                Entidad_Ejercicio ejercicio1 = new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = this.txtmescod2.Text
                };
                list = ejercicio.Listar_Periodo(ejercicio1);
                if (list.Count > 0)
                {
                    foreach (Entidad_Ejercicio ejercicio2 in list)
                    {
                        if (ejercicio2.Id_Periodo.ToString().Trim().ToUpper() == this.txtmescod2.Text.Trim().ToUpper())
                        {
                            this.txtmescod2.Text = ejercicio2.Id_Periodo.ToString().Trim();
                            this.txtmesdesc2.Text = ejercicio2.Descripcion_Periodo;
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }
        }

        private void txtmescod2_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtmescod2.Text))
            {
                try
                {
                    if ((e.KeyCode == Keys.Enter) & (this.txtmescod2.Text.Substring(this.txtmescod2.Text.Length - 1, 1) == "*"))
                    {
                        using (frm_periodo_busqueda _busqueda = new frm_periodo_busqueda())
                        {
                            _busqueda.empresa = Actual_Conexion.CodigoEmpresa;
                            _busqueda.anio = Actual_Conexion.AnioSelect;
                            if (_busqueda.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio ejercicio = new Entidad_Ejercicio();
                                ejercicio = _busqueda.Lista[_busqueda.gridView1.GetFocusedDataSourceRowIndex()];
                                this.txtmescod2.Text = ejercicio.Id_Periodo;
                                this.txtmesdesc2.Text = ejercicio.Descripcion_Periodo;
                            }
                        }
                    }
                    else if (((e.KeyCode == Keys.Enter) & !string.IsNullOrEmpty(this.txtmescod2.Text)) & string.IsNullOrEmpty(this.txtmesdesc2.Text))
                    {
                        this.BuscarMes02();
                    }
                }
                catch (Exception exception)
                {
                    Accion.ErrorSistema(exception.Message);
                }
            }
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.txtmescod.ResetText();
                this.txtmesdesc.ResetText();
                this.txtmescod2.ResetText();
                this.txtmesdesc2.ResetText();
                this.ListRC.Clear();
                this.dgvdatos.DataSource = null;
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }

        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\FlujoDeEfectivo" + ".Xlsx");

                XlsxExportOptionsEx ExXlsxExportOptions = new XlsxExportOptionsEx();
                ExXlsxExportOptions.ExportType = DevExpress.Export.ExportType.WYSIWYG;

                dgvdatos.ExportToXlsx(Ruta, ExXlsxExportOptions);

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (ListRC.Count > 0)
                {
                    Rpte_Flujo_Efectivo f = new Rpte_Flujo_Efectivo();

 
                    f.lblperiodo.Text = Actual_Conexion.AnioSelect;
                    f.lblrazon.Text = Actual_Conexion.EmpresaNombre;
                    f.lblruc.Text = Actual_Conexion.RucEmpresa;

                    f.DataSource = ListRC;
                    //f.Landscape = true;

                    dynamic ribbonPreview = new PrintPreviewFormEx();
                    f.CreateDocument();
                    f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    ribbonPreview.PrintingSystem = f.PrintingSystem;
                    //ribbonPreview.MdiParent =frm_principal;
                    ribbonPreview.Show();
                }
                else
                {
                    Accion.Advertencia("No existen datos para imprimir");
                }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
