﻿namespace Contable.Reportes.Libros_y_registros
{
    partial class frm_libro_diario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnnuscar = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbacumulado = new System.Windows.Forms.RadioButton();
            this.rbmensual = new System.Windows.Forms.RadioButton();
            this.Rbt_xLibro = new System.Windows.Forms.RadioButton();
            this.Rbt_Centralizado = new System.Windows.Forms.RadioButton();
            this.TxtNomLibro = new DevExpress.XtraEditors.TextEdit();
            this.TxtCodLibro = new DevExpress.XtraEditors.TextEdit();
            this.txtmesdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.BandGridReport = new DevExpress.XtraGrid.GridControl();
            this.BandGridVisual = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.BandFechaEmi = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Voucher = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FechaDoc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Glosa = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CodLibroSun = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Correlativo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DocSunat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SerieDoc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.NumeroDoc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Cuenta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Descripcion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BandMonExt = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandDocId = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandApelliyNom = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.SaldoDebe = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SaldoHaber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.btnimpresion = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnple = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNomLibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCodLibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridVisual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnnuscar);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.Rbt_xLibro);
            this.groupBox1.Controls.Add(this.Rbt_Centralizado);
            this.groupBox1.Controls.Add(this.TxtNomLibro);
            this.groupBox1.Controls.Add(this.TxtCodLibro);
            this.groupBox1.Controls.Add(this.txtmesdesc);
            this.groupBox1.Controls.Add(this.txtmescod);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Location = new System.Drawing.Point(0, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1035, 53);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnnuscar
            // 
            this.btnnuscar.Location = new System.Drawing.Point(946, 19);
            this.btnnuscar.Name = "btnnuscar";
            this.btnnuscar.Size = new System.Drawing.Size(75, 23);
            this.btnnuscar.TabIndex = 11;
            this.btnnuscar.Text = "Buscar";
            this.btnnuscar.Click += new System.EventHandler(this.btnbuscar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbacumulado);
            this.groupBox2.Controls.Add(this.rbmensual);
            this.groupBox2.Location = new System.Drawing.Point(753, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(174, 36);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // rbacumulado
            // 
            this.rbacumulado.AutoSize = true;
            this.rbacumulado.Location = new System.Drawing.Point(92, 11);
            this.rbacumulado.Name = "rbacumulado";
            this.rbacumulado.Size = new System.Drawing.Size(77, 17);
            this.rbacumulado.TabIndex = 1;
            this.rbacumulado.TabStop = true;
            this.rbacumulado.Text = "Acumulado";
            this.rbacumulado.UseVisualStyleBackColor = true;
            // 
            // rbmensual
            // 
            this.rbmensual.AutoSize = true;
            this.rbmensual.Location = new System.Drawing.Point(13, 10);
            this.rbmensual.Name = "rbmensual";
            this.rbmensual.Size = new System.Drawing.Size(64, 17);
            this.rbmensual.TabIndex = 0;
            this.rbmensual.TabStop = true;
            this.rbmensual.Text = "Mensual";
            this.rbmensual.UseVisualStyleBackColor = true;
            // 
            // Rbt_xLibro
            // 
            this.Rbt_xLibro.AutoSize = true;
            this.Rbt_xLibro.Location = new System.Drawing.Point(355, 21);
            this.Rbt_xLibro.Name = "Rbt_xLibro";
            this.Rbt_xLibro.Size = new System.Drawing.Size(72, 17);
            this.Rbt_xLibro.TabIndex = 9;
            this.Rbt_xLibro.TabStop = true;
            this.Rbt_xLibro.Text = "Por Libros";
            this.Rbt_xLibro.UseVisualStyleBackColor = true;
            this.Rbt_xLibro.CheckedChanged += new System.EventHandler(this.Rbt_xLibro_CheckedChanged);
            // 
            // Rbt_Centralizado
            // 
            this.Rbt_Centralizado.AutoSize = true;
            this.Rbt_Centralizado.Location = new System.Drawing.Point(267, 22);
            this.Rbt_Centralizado.Name = "Rbt_Centralizado";
            this.Rbt_Centralizado.Size = new System.Drawing.Size(85, 17);
            this.Rbt_Centralizado.TabIndex = 8;
            this.Rbt_Centralizado.TabStop = true;
            this.Rbt_Centralizado.Text = "Centralizado";
            this.Rbt_Centralizado.UseVisualStyleBackColor = true;
            this.Rbt_Centralizado.CheckedChanged += new System.EventHandler(this.Rbt_Centralizado_CheckedChanged);
            // 
            // TxtNomLibro
            // 
            this.TxtNomLibro.EditValue = "[TODOS]";
            this.TxtNomLibro.Enabled = false;
            this.TxtNomLibro.EnterMoveNextControl = true;
            this.TxtNomLibro.Location = new System.Drawing.Point(542, 19);
            this.TxtNomLibro.Name = "TxtNomLibro";
            this.TxtNomLibro.Size = new System.Drawing.Size(205, 20);
            this.TxtNomLibro.TabIndex = 7;
            // 
            // TxtCodLibro
            // 
            this.TxtCodLibro.EnterMoveNextControl = true;
            this.TxtCodLibro.Location = new System.Drawing.Point(486, 19);
            this.TxtCodLibro.Name = "TxtCodLibro";
            this.TxtCodLibro.Size = new System.Drawing.Size(54, 20);
            this.TxtCodLibro.TabIndex = 6;
            this.TxtCodLibro.TextChanged += new System.EventHandler(this.TxtCodLibro_TextChanged);
            this.TxtCodLibro.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtCodLibro_KeyDown);
            // 
            // txtmesdesc
            // 
            this.txtmesdesc.Enabled = false;
            this.txtmesdesc.EnterMoveNextControl = true;
            this.txtmesdesc.Location = new System.Drawing.Point(90, 20);
            this.txtmesdesc.Name = "txtmesdesc";
            this.txtmesdesc.Size = new System.Drawing.Size(171, 20);
            this.txtmesdesc.TabIndex = 2;
            // 
            // txtmescod
            // 
            this.txtmescod.EnterMoveNextControl = true;
            this.txtmescod.Location = new System.Drawing.Point(47, 20);
            this.txtmescod.Name = "txtmescod";
            this.txtmescod.Size = new System.Drawing.Size(42, 20);
            this.txtmescod.TabIndex = 1;
            this.txtmescod.TextChanged += new System.EventHandler(this.txtmescod_TextChanged);
            this.txtmescod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(448, 21);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Libros:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 24);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Mes:";
            // 
            // BandGridReport
            // 
            this.BandGridReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BandGridReport.Location = new System.Drawing.Point(0, 90);
            this.BandGridReport.MainView = this.BandGridVisual;
            this.BandGridReport.Name = "BandGridReport";
            this.BandGridReport.Size = new System.Drawing.Size(1035, 272);
            this.BandGridReport.TabIndex = 225;
            this.BandGridReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.BandGridVisual});
            // 
            // BandGridVisual
            // 
            this.BandGridVisual.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandFechaEmi,
            this.gridBand1,
            this.gridBand6,
            this.BandMonExt,
            this.gridBand3,
            this.gridBand2,
            this.gridBand5});
            this.BandGridVisual.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Glosa,
            this.FechaDoc,
            this.DocSunat,
            this.SerieDoc,
            this.NumeroDoc,
            this.Cuenta,
            this.Descripcion,
            this.Correlativo,
            this.CodLibroSun,
            this.SaldoDebe,
            this.SaldoHaber,
            this.Voucher});
            this.BandGridVisual.GridControl = this.BandGridReport;
            this.BandGridVisual.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "NomEntidad", null, "")});
            this.BandGridVisual.Name = "BandGridVisual";
            this.BandGridVisual.OptionsFilter.AllowColumnMRUFilterList = false;
            this.BandGridVisual.OptionsFilter.AllowMRUFilterList = false;
            this.BandGridVisual.OptionsPrint.AutoWidth = false;
            this.BandGridVisual.OptionsView.ColumnAutoWidth = false;
            this.BandGridVisual.OptionsView.ShowAutoFilterRow = true;
            this.BandGridVisual.OptionsView.ShowFooter = true;
            this.BandGridVisual.OptionsView.ShowGroupPanel = false;
            // 
            // BandFechaEmi
            // 
            this.BandFechaEmi.AppearanceHeader.Options.UseTextOptions = true;
            this.BandFechaEmi.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandFechaEmi.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BandFechaEmi.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.BandFechaEmi.AutoFillDown = false;
            this.BandFechaEmi.Caption = "DATOS DE LA OPERACIÓN";
            this.BandFechaEmi.Columns.Add(this.Voucher);
            this.BandFechaEmi.Columns.Add(this.FechaDoc);
            this.BandFechaEmi.Columns.Add(this.Glosa);
            this.BandFechaEmi.Name = "BandFechaEmi";
            this.BandFechaEmi.VisibleIndex = 0;
            this.BandFechaEmi.Width = 463;
            // 
            // Voucher
            // 
            this.Voucher.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Voucher.AppearanceHeader.Options.UseForeColor = true;
            this.Voucher.Caption = "VOUCHER";
            this.Voucher.FieldName = "Voucher";
            this.Voucher.Name = "Voucher";
            this.Voucher.Visible = true;
            this.Voucher.Width = 91;
            // 
            // FechaDoc
            // 
            this.FechaDoc.AppearanceCell.Options.UseTextOptions = true;
            this.FechaDoc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FechaDoc.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.FechaDoc.AppearanceHeader.Options.UseForeColor = true;
            this.FechaDoc.AppearanceHeader.Options.UseTextOptions = true;
            this.FechaDoc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FechaDoc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FechaDoc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FechaDoc.Caption = "FECHA DE EMISION";
            this.FechaDoc.DisplayFormat.FormatString = "d";
            this.FechaDoc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FechaDoc.FieldName = "FechaOperacion";
            this.FechaDoc.Name = "FechaDoc";
            this.FechaDoc.OptionsColumn.AllowEdit = false;
            this.FechaDoc.OptionsColumn.AllowSize = false;
            this.FechaDoc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Date;
            this.FechaDoc.OptionsFilter.ShowEmptyDateFilter = true;
            this.FechaDoc.RowCount = 2;
            this.FechaDoc.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.FechaDoc.Visible = true;
            this.FechaDoc.Width = 132;
            // 
            // Glosa
            // 
            this.Glosa.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Glosa.AppearanceHeader.Options.UseForeColor = true;
            this.Glosa.AppearanceHeader.Options.UseTextOptions = true;
            this.Glosa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Glosa.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Glosa.Caption = "DESCRIPCION DE LA OPERACIÓN";
            this.Glosa.FieldName = "Glosa";
            this.Glosa.Name = "Glosa";
            this.Glosa.OptionsColumn.AllowEdit = false;
            this.Glosa.OptionsColumn.AllowSize = false;
            this.Glosa.RowCount = 2;
            this.Glosa.Visible = true;
            this.Glosa.Width = 240;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "REFERENCIA DE LA OPERACIÓN";
            this.gridBand1.Columns.Add(this.CodLibroSun);
            this.gridBand1.Columns.Add(this.Correlativo);
            this.gridBand1.Columns.Add(this.DocSunat);
            this.gridBand1.Columns.Add(this.SerieDoc);
            this.gridBand1.Columns.Add(this.NumeroDoc);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 299;
            // 
            // CodLibroSun
            // 
            this.CodLibroSun.AppearanceCell.Options.UseTextOptions = true;
            this.CodLibroSun.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CodLibroSun.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.CodLibroSun.AppearanceHeader.Options.UseForeColor = true;
            this.CodLibroSun.Caption = "LIBRO O REGISTRO";
            this.CodLibroSun.FieldName = "Lib_Sunat";
            this.CodLibroSun.Name = "CodLibroSun";
            this.CodLibroSun.OptionsColumn.AllowEdit = false;
            this.CodLibroSun.Visible = true;
            this.CodLibroSun.Width = 142;
            // 
            // Correlativo
            // 
            this.Correlativo.AppearanceCell.Options.UseTextOptions = true;
            this.Correlativo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Correlativo.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Correlativo.AppearanceHeader.Options.UseForeColor = true;
            this.Correlativo.Caption = "CORRELAT.";
            this.Correlativo.FieldName = "Correlativo";
            this.Correlativo.Name = "Correlativo";
            this.Correlativo.OptionsColumn.AllowEdit = false;
            this.Correlativo.Visible = true;
            this.Correlativo.Width = 86;
            // 
            // DocSunat
            // 
            this.DocSunat.AppearanceCell.Options.UseTextOptions = true;
            this.DocSunat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DocSunat.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.DocSunat.AppearanceHeader.Options.UseForeColor = true;
            this.DocSunat.Caption = "DOC.";
            this.DocSunat.FieldName = "DocSunat";
            this.DocSunat.Name = "DocSunat";
            this.DocSunat.OptionsColumn.AllowEdit = false;
            this.DocSunat.Width = 37;
            // 
            // SerieDoc
            // 
            this.SerieDoc.AppearanceCell.Options.UseTextOptions = true;
            this.SerieDoc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SerieDoc.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SerieDoc.AppearanceHeader.Options.UseForeColor = true;
            this.SerieDoc.Caption = "SERIE";
            this.SerieDoc.FieldName = "SerieDoc";
            this.SerieDoc.Name = "SerieDoc";
            this.SerieDoc.OptionsColumn.AllowEdit = false;
            this.SerieDoc.Width = 50;
            // 
            // NumeroDoc
            // 
            this.NumeroDoc.AppearanceCell.Options.UseTextOptions = true;
            this.NumeroDoc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NumeroDoc.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.NumeroDoc.AppearanceHeader.Options.UseForeColor = true;
            this.NumeroDoc.Caption = "NUMERO";
            this.NumeroDoc.FieldName = "NumeroDoc";
            this.NumeroDoc.Name = "NumeroDoc";
            this.NumeroDoc.OptionsColumn.AllowEdit = false;
            this.NumeroDoc.Visible = true;
            this.NumeroDoc.Width = 71;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "CUENTA CONTABLE ASOCIADA";
            this.gridBand6.Columns.Add(this.Cuenta);
            this.gridBand6.Columns.Add(this.Descripcion);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 2;
            this.gridBand6.Width = 437;
            // 
            // Cuenta
            // 
            this.Cuenta.AppearanceCell.Options.UseTextOptions = true;
            this.Cuenta.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Cuenta.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Cuenta.AppearanceHeader.Options.UseForeColor = true;
            this.Cuenta.Caption = "CUENTA";
            this.Cuenta.FieldName = "Cuenta";
            this.Cuenta.Name = "Cuenta";
            this.Cuenta.OptionsColumn.AllowEdit = false;
            this.Cuenta.Visible = true;
            this.Cuenta.Width = 76;
            // 
            // Descripcion
            // 
            this.Descripcion.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Descripcion.AppearanceHeader.Options.UseForeColor = true;
            this.Descripcion.Caption = "DENOMINACIÓN";
            this.Descripcion.FieldName = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.OptionsColumn.AllowEdit = false;
            this.Descripcion.Visible = true;
            this.Descripcion.Width = 361;
            // 
            // BandMonExt
            // 
            this.BandMonExt.AppearanceHeader.Options.UseTextOptions = true;
            this.BandMonExt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandMonExt.AutoFillDown = false;
            this.BandMonExt.Caption = "CUENTA CONTABLE ASOCIADA";
            this.BandMonExt.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4});
            this.BandMonExt.Name = "BandMonExt";
            this.BandMonExt.Visible = false;
            this.BandMonExt.VisibleIndex = -1;
            this.BandMonExt.Width = 289;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.AutoFillDown = false;
            this.gridBand4.Caption = "INFORMACION DE DEUDORES";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandDocId,
            this.BandApelliyNom});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Visible = false;
            this.gridBand4.VisibleIndex = -1;
            this.gridBand4.Width = 466;
            // 
            // BandDocId
            // 
            this.BandDocId.AppearanceHeader.Options.UseTextOptions = true;
            this.BandDocId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandDocId.AutoFillDown = false;
            this.BandDocId.Caption = "DOC. IDENTIDAD";
            this.BandDocId.Name = "BandDocId";
            this.BandDocId.VisibleIndex = 0;
            this.BandDocId.Width = 160;
            // 
            // BandApelliyNom
            // 
            this.BandApelliyNom.AppearanceHeader.Options.UseTextOptions = true;
            this.BandApelliyNom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandApelliyNom.AutoFillDown = false;
            this.BandApelliyNom.Caption = "DATOS DE ENTIDAD";
            this.BandApelliyNom.Name = "BandApelliyNom";
            this.BandApelliyNom.VisibleIndex = 1;
            this.BandApelliyNom.Width = 306;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Visible = false;
            this.gridBand3.VisibleIndex = -1;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Visible = false;
            this.gridBand2.VisibleIndex = -1;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "MOVIMIENTOS";
            this.gridBand5.Columns.Add(this.SaldoDebe);
            this.gridBand5.Columns.Add(this.SaldoHaber);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 3;
            this.gridBand5.Width = 235;
            // 
            // SaldoDebe
            // 
            this.SaldoDebe.AppearanceCell.Options.UseTextOptions = true;
            this.SaldoDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SaldoDebe.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SaldoDebe.AppearanceHeader.Options.UseForeColor = true;
            this.SaldoDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.SaldoDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SaldoDebe.Caption = "DEBE";
            this.SaldoDebe.DisplayFormat.FormatString = "n2";
            this.SaldoDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SaldoDebe.FieldName = "DebeNacional";
            this.SaldoDebe.Name = "SaldoDebe";
            this.SaldoDebe.OptionsColumn.AllowEdit = false;
            this.SaldoDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DebeNacional", "{0:n2}")});
            this.SaldoDebe.Visible = true;
            this.SaldoDebe.Width = 129;
            // 
            // SaldoHaber
            // 
            this.SaldoHaber.AppearanceCell.Options.UseTextOptions = true;
            this.SaldoHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SaldoHaber.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SaldoHaber.AppearanceHeader.Options.UseForeColor = true;
            this.SaldoHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.SaldoHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SaldoHaber.Caption = "HABER";
            this.SaldoHaber.DisplayFormat.FormatString = "n2";
            this.SaldoHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SaldoHaber.FieldName = "HaberNacional";
            this.SaldoHaber.Name = "SaldoHaber";
            this.SaldoHaber.OptionsColumn.AllowEdit = false;
            this.SaldoHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "HaberNacional", "{0:n2}")});
            this.SaldoHaber.Visible = true;
            this.SaldoHaber.Width = 106;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnlimpiar,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2,
            this.btnimpresion,
            this.barButtonItem1,
            this.btnple});
            this.barManager1.MaxItemId = 7;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnimpresion),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnple)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 0;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // btnimpresion
            // 
            this.btnimpresion.Caption = "Impresion";
            this.btnimpresion.Id = 4;
            this.btnimpresion.ImageOptions.Image = global::Contable.Properties.Resources.Print_24px;
            this.btnimpresion.Name = "btnimpresion";
            this.btnimpresion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnimpresion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnimpresion_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1035, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 361);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1035, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 329);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Exportar";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnple
            // 
            this.btnple.Caption = "Generar PLE txt";
            this.btnple.Id = 6;
            this.btnple.ImageOptions.Image = global::Contable.Properties.Resources.TXT_24px;
            this.btnple.Name = "btnple";
            this.btnple.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnple.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnple_ItemClick);
            // 
            // frm_libro_diario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1035, 361);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.BandGridReport);
            this.Controls.Add(this.groupBox1);
            this.Name = "frm_libro_diario";
            this.Text = "Libro diario";
            this.Load += new System.EventHandler(this.frm_libro_diario_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNomLibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCodLibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridVisual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal DevExpress.XtraGrid.GridControl BandGridReport;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridView BandGridVisual;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandFechaEmi;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Voucher;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FechaDoc;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Glosa;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CodLibroSun;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Correlativo;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DocSunat;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SerieDoc;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NumeroDoc;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Cuenta;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Descripcion;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandMonExt;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandDocId;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandApelliyNom;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SaldoDebe;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SaldoHaber;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit TxtNomLibro;
        private DevExpress.XtraEditors.TextEdit TxtCodLibro;
        private DevExpress.XtraEditors.TextEdit txtmesdesc;
        private DevExpress.XtraEditors.TextEdit txtmescod;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbacumulado;
        private System.Windows.Forms.RadioButton rbmensual;
        private System.Windows.Forms.RadioButton Rbt_xLibro;
        private System.Windows.Forms.RadioButton Rbt_Centralizado;
        private DevExpress.XtraEditors.SimpleButton btnnuscar;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarButtonItem btnimpresion;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnple;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}
