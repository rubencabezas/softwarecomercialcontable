﻿using Contable.Reportes_Impresiones;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_determinacion_igv : Contable.frm_fuente
    {
        public frm_determinacion_igv()
        {
            InitializeComponent();
        }

         List<Entidad_Cedula_Determinacion_IGV> Lista_Final = new List<Entidad_Cedula_Determinacion_IGV>();
        List<Entidad_Cedula_Determinacion_IGV> ListRC = new List<Entidad_Cedula_Determinacion_IGV>();




        private void btnverreporte_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Logica_Cedula_Determinacion_IGV n_igv = new Logica_Cedula_Determinacion_IGV();
            Entidad_Cedula_Determinacion_IGV n_igv2 = new Entidad_Cedula_Determinacion_IGV();

            this.ListRC = n_igv.Listar(n_igv2);

            if (this.ListRC.Count > 0)
            {
                this.Calcular(this.ListRC);
                this.dgvdatos.DataSource = this.Lista_Final;
            }


        }

        private void Calcular(List<Entidad_Cedula_Determinacion_IGV> Lista)
        {
            try
            {
         

                if (Lista.Count > 0)
                {
                    for (int i = 0; i <= (Lista.Count - 1); i++)
                    {
                        if (i == 0)
                        {
                            Lista[i].CREDITO_FISCAL_MES_ANTERIOR = Lista[i].CREDITO_FISCAL_MES_ANTERIOR_X;
                        }
                        else
                        {
                            if (Lista[i].CREDITO_FISCAL_MES_ANTERIOR_X != 0)
                            {
                                Lista[i].CREDITO_FISCAL_MES_ANTERIOR = Lista[i - 1].CREDITO_FISCAL_MES_SIGUIENTE;
                            }
                            if (Lista[i].CREDITO_FISCAL_MES_ANTERIOR_X == 0)
                            {
                                Lista[i].CREDITO_FISCAL_MES_ANTERIOR = Lista[i - 1].CREDITO_FISCAL_MES_SIGUIENTE;
                            }
                        }
                        if (i == 0)
                        {
                            Lista[i].PERCEPCION_MES_ANTERIOR = Lista[i].PERCEPCION_MES_ANTERIOR_X;
                        }
                        else
                        {
                            Lista[i].PERCEPCION_MES_ANTERIOR = ((Lista[i - 1].PERCEPCION_MES_ANTERIOR_X + Lista[i - 1].PERCEPCIONES_DEL_MES) - Lista[i - 1].PERCEPCIONES_DEVUELTO_O_COMPESADO) - Lista[i - 1].PERCEPCIONES_APLICADAS;
                        }
                        if (i == 0)
                        {
                            Lista[i].RETENCION_MES_ANTERIOR = Lista[i].RETENCION_MES_ANTERIOR_X;
                        }
                        else
                        {
                            Lista[i].RETENCION_MES_ANTERIOR = ((Lista[i - 1].RETENCION_MES_ANTERIOR_X + Lista[i - 1].RETENCIONES_DEL_MES) - Lista[i - 1].RETENCION_DEVUELTO_O_COMPESADO) - Lista[i - 1].RETENCION_APLICADAS;
                        }
                    }
                }

                foreach (Entidad_Cedula_Determinacion_IGV n_igv in Lista)
                {
                    this.Lista_Final.Add(n_igv);
                }


            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }



        private void Calcular_Percepcion(List<Entidad_Cedula_Determinacion_IGV> Lista)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    int num = 0;
                    while (true)
                    {
                        if (num > (Lista.Count - 1))
                        {
                            foreach (Entidad_Cedula_Determinacion_IGV n_igv in Lista)
                            {
                                this.Lista_Final.Add(n_igv);
                            }
                            break;
                        }
                        bool flag2 = num == 0;
                        Lista[num].PERCEPCION_MES_ANTERIOR = !flag2 ? ((Lista[num - 1].PERCEPCION_MES_ANTERIOR_X + Lista[num - 1].PERCEPCIONES_DEL_MES) - Lista[num - 1].DEVUELTO_CON_NCN_EXPORTADO) : Lista[num].PERCEPCION_MES_ANTERIOR_X;
                        num++;
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }
        }

        private void btnsaldoinicial2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (frm_saldo_inicial_igv _igv = new frm_saldo_inicial_igv())
                {
                    base.Estado = Estados.Nuevo;
                    if (_igv.ShowDialog() == DialogResult.OK)
                    {
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\DeterminacionIGV" + ".Xlsx");

            advBandedGridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Rpt_Cedula_Determinacion_IGV f = new Rpt_Cedula_Determinacion_IGV();
            Logica_Cedula_Determinacion_IGV n_igv2 = new Logica_Cedula_Determinacion_IGV();
            Entidad_Cedula_Determinacion_IGV n_igv3 = new Entidad_Cedula_Determinacion_IGV();
            f.lblperiodo.Text = Actual_Conexion.AnioSelect;
            f.lblrazon.Text = Actual_Conexion.EmpresaNombre;
            f.lblruc.Text = Actual_Conexion.RucEmpresa;
            f.DataSource = this.Lista_Final;
            f.Landscape = true;
 
            dynamic ribbonPreview = new PrintPreviewFormEx();
            f.CreateDocument();
            f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            ribbonPreview.PrintingSystem = f.PrintingSystem;
            // ribbonPreview.MdiParent = frm_principal;
            ribbonPreview.Show();



        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             
            Lista_Final.Clear();
            ListRC.Clear();
            dgvdatos.DataSource = null;
        }
    }
}
