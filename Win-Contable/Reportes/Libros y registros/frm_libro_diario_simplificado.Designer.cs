﻿namespace Contable.Reportes.Libros_y_registros
{
    partial class frm_libro_diario_simplificado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.NatuDebe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl9 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl10 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl11 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl12 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl13 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl14 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl15 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl16 = new DevExpress.XtraBars.BarDockControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.BandGridReport = new DevExpress.XtraGrid.GridControl();
            this.BandGridVisual = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.BandFechaEmi = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandMonExt = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandDocId = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandApelliyNom = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnver = new DevExpress.XtraEditors.SimpleButton();
            this.txtmesdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtniveldesc = new DevExpress.XtraEditors.TextEdit();
            this.txtnivel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.TabControl_V5_Reporte = new System.Windows.Forms.TabControl();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.DgvFormatoSimplificado02 = new DevExpress.XtraGrid.GridControl();
            this.ViewFormatoSimplificado02 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TabPage7 = new System.Windows.Forms.TabPage();
            this.gvElectronico_V5_Det = new DevExpress.XtraGrid.GridControl();
            this.GridViewAsientoDet_V5_Det = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtmesdesc2 = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnElectronico_V5_VistaPrevia_Detalle = new System.Windows.Forms.Button();
            this.BtnVistaPr2 = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.FuncionDebe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.RbtAcumulado = new DevExpress.XtraEditors.CheckEdit();
            this.InvenHaber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuncionHaber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvenDebe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RbtMensual = new DevExpress.XtraEditors.CheckEdit();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NatuHaber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.btnimpresion = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnple = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl6 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl7 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridVisual)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtniveldesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnivel.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.TabControl_V5_Reporte.SuspendLayout();
            this.TabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvFormatoSimplificado02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewFormatoSimplificado02)).BeginInit();
            this.TabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvElectronico_V5_Det)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAsientoDet_V5_Det)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbtAcumulado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbtMensual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // NatuDebe
            // 
            this.NatuDebe.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.NatuDebe.AppearanceCell.Options.UseFont = true;
            this.NatuDebe.AppearanceCell.Options.UseTextOptions = true;
            this.NatuDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NatuDebe.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NatuDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.NatuDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NatuDebe.Caption = "PÉRDIDAS N";
            this.NatuDebe.DisplayFormat.FormatString = "n2";
            this.NatuDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NatuDebe.FieldName = "NatuDebe";
            this.NatuDebe.Name = "NatuDebe";
            this.NatuDebe.OptionsColumn.AllowEdit = false;
            this.NatuDebe.OptionsColumn.AllowMove = false;
            this.NatuDebe.OptionsColumn.AllowSize = false;
            this.NatuDebe.OptionsColumn.FixedWidth = true;
            this.NatuDebe.OptionsFilter.AllowAutoFilter = false;
            this.NatuDebe.OptionsFilter.AllowFilter = false;
            this.NatuDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuDebe", "{0:n2}")});
            this.NatuDebe.Visible = true;
            this.NatuDebe.VisibleIndex = 4;
            this.NatuDebe.Width = 104;
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = null;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1060, 32);
            this.barDockControlRight.Manager = null;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 514);
            this.barDockControlBottom.Manager = null;
            this.barDockControlBottom.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 32);
            this.barDockControlTop.Manager = null;
            this.barDockControlTop.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 32);
            this.barDockControl1.Manager = null;
            this.barDockControl1.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1060, 32);
            this.barDockControl2.Manager = null;
            this.barDockControl2.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(0, 514);
            this.barDockControl3.Manager = null;
            this.barDockControl3.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 32);
            this.barDockControl4.Manager = null;
            this.barDockControl4.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl9
            // 
            this.barDockControl9.CausesValidation = false;
            this.barDockControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl9.Location = new System.Drawing.Point(0, 32);
            this.barDockControl9.Manager = null;
            this.barDockControl9.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl10
            // 
            this.barDockControl10.CausesValidation = false;
            this.barDockControl10.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl10.Location = new System.Drawing.Point(1060, 32);
            this.barDockControl10.Manager = null;
            this.barDockControl10.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl11
            // 
            this.barDockControl11.CausesValidation = false;
            this.barDockControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl11.Location = new System.Drawing.Point(0, 514);
            this.barDockControl11.Manager = null;
            this.barDockControl11.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl12
            // 
            this.barDockControl12.CausesValidation = false;
            this.barDockControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl12.Location = new System.Drawing.Point(0, 32);
            this.barDockControl12.Manager = null;
            this.barDockControl12.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl13
            // 
            this.barDockControl13.CausesValidation = false;
            this.barDockControl13.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl13.Location = new System.Drawing.Point(0, 32);
            this.barDockControl13.Manager = null;
            this.barDockControl13.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl14
            // 
            this.barDockControl14.CausesValidation = false;
            this.barDockControl14.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl14.Location = new System.Drawing.Point(1060, 32);
            this.barDockControl14.Manager = null;
            this.barDockControl14.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl15
            // 
            this.barDockControl15.CausesValidation = false;
            this.barDockControl15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl15.Location = new System.Drawing.Point(0, 514);
            this.barDockControl15.Manager = null;
            this.barDockControl15.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl16
            // 
            this.barDockControl16.CausesValidation = false;
            this.barDockControl16.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl16.Location = new System.Drawing.Point(0, 32);
            this.barDockControl16.Manager = null;
            this.barDockControl16.Size = new System.Drawing.Size(1060, 0);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 32);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1060, 482);
            this.tabControl1.TabIndex = 36;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.BandGridReport);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1052, 456);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Registro manual";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // BandGridReport
            // 
            this.BandGridReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BandGridReport.Location = new System.Drawing.Point(8, 53);
            this.BandGridReport.MainView = this.BandGridVisual;
            this.BandGridReport.Name = "BandGridReport";
            this.BandGridReport.Size = new System.Drawing.Size(1038, 397);
            this.BandGridReport.TabIndex = 228;
            this.BandGridReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.BandGridVisual});
            // 
            // BandGridVisual
            // 
            this.BandGridVisual.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.BandGridVisual.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.BandPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.BandPanel.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.BandPanel.Options.UseFont = true;
            this.BandGridVisual.Appearance.BandPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.BandPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.Empty.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(237)))));
            this.BandGridVisual.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.EvenRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.EvenRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FilterPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FilterPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.BandGridVisual.Appearance.FixedLine.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.FocusedCell.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FocusedCell.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.FocusedRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FocusedRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.FooterPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.FooterPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupButton.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupButton.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.GroupButton.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupFooter.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.GroupFooter.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.GroupPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupRow.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.GroupRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.BandGridVisual.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.BandGridVisual.Appearance.HorzLine.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.BandGridVisual.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.OddRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.OddRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(248)))));
            this.BandGridVisual.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.BandGridVisual.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.Preview.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.Preview.Options.UseFont = true;
            this.BandGridVisual.Appearance.Preview.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.BandGridVisual.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.BandGridVisual.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.Row.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.Row.Options.UseFont = true;
            this.BandGridVisual.Appearance.Row.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.RowSeparator.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(133)))), ((int)(((byte)(179)))));
            this.BandGridVisual.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.SelectedRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.SelectedRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.TopNewRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.BandGridVisual.Appearance.VertLine.Options.UseBackColor = true;
            this.BandGridVisual.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandFechaEmi,
            this.gridBand13,
            this.gridBand14,
            this.BandMonExt,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand21,
            this.gridBand23,
            this.gridBand25});
            this.BandGridVisual.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn18});
            this.BandGridVisual.GridControl = this.BandGridReport;
            this.BandGridVisual.Name = "BandGridVisual";
            this.BandGridVisual.OptionsFilter.AllowColumnMRUFilterList = false;
            this.BandGridVisual.OptionsFilter.AllowMRUFilterList = false;
            this.BandGridVisual.OptionsPrint.AutoWidth = false;
            this.BandGridVisual.OptionsView.ColumnAutoWidth = false;
            this.BandGridVisual.OptionsView.EnableAppearanceEvenRow = true;
            this.BandGridVisual.OptionsView.EnableAppearanceOddRow = true;
            this.BandGridVisual.OptionsView.ShowAutoFilterRow = true;
            this.BandGridVisual.OptionsView.ShowFooter = true;
            this.BandGridVisual.OptionsView.ShowGroupPanel = false;
            // 
            // BandFechaEmi
            // 
            this.BandFechaEmi.AppearanceHeader.Options.UseTextOptions = true;
            this.BandFechaEmi.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandFechaEmi.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BandFechaEmi.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.BandFechaEmi.AutoFillDown = false;
            this.BandFechaEmi.Caption = "DATOS DE LA OPERACIÓN";
            this.BandFechaEmi.Name = "BandFechaEmi";
            this.BandFechaEmi.Visible = false;
            this.BandFechaEmi.VisibleIndex = -1;
            this.BandFechaEmi.Width = 300;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "REFERENCIA DE LA OPERACIÓN";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.Visible = false;
            this.gridBand13.VisibleIndex = -1;
            this.gridBand13.Width = 86;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "CUENTA CONTABLE ASOCIADA";
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.Visible = false;
            this.gridBand14.VisibleIndex = -1;
            this.gridBand14.Width = 250;
            // 
            // BandMonExt
            // 
            this.BandMonExt.AppearanceHeader.Options.UseTextOptions = true;
            this.BandMonExt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandMonExt.AutoFillDown = false;
            this.BandMonExt.Caption = "CUENTA CONTABLE ASOCIADA";
            this.BandMonExt.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand15});
            this.BandMonExt.Name = "BandMonExt";
            this.BandMonExt.Visible = false;
            this.BandMonExt.VisibleIndex = -1;
            this.BandMonExt.Width = 289;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.AutoFillDown = false;
            this.gridBand15.Caption = "INFORMACION DE DEUDORES";
            this.gridBand15.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandDocId,
            this.BandApelliyNom});
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.Visible = false;
            this.gridBand15.VisibleIndex = -1;
            this.gridBand15.Width = 466;
            // 
            // BandDocId
            // 
            this.BandDocId.AppearanceHeader.Options.UseTextOptions = true;
            this.BandDocId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandDocId.AutoFillDown = false;
            this.BandDocId.Caption = "DOC. IDENTIDAD";
            this.BandDocId.Name = "BandDocId";
            this.BandDocId.VisibleIndex = 0;
            this.BandDocId.Width = 160;
            // 
            // BandApelliyNom
            // 
            this.BandApelliyNom.AppearanceHeader.Options.UseTextOptions = true;
            this.BandApelliyNom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandApelliyNom.AutoFillDown = false;
            this.BandApelliyNom.Caption = "DATOS DE ENTIDAD";
            this.BandApelliyNom.Name = "BandApelliyNom";
            this.BandApelliyNom.VisibleIndex = 1;
            this.BandApelliyNom.Width = 306;
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "gridBand3";
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.Visible = false;
            this.gridBand16.VisibleIndex = -1;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "gridBand2";
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Visible = false;
            this.gridBand17.VisibleIndex = -1;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "MOVIMIENTOS";
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.Visible = false;
            this.gridBand18.VisibleIndex = -1;
            this.gridBand18.Width = 150;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.gridBand19.AppearanceHeader.Options.UseFont = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand19.Caption = "Número correlativo";
            this.gridBand19.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand20});
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 0;
            this.gridBand19.Width = 148;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 6.75F);
            this.gridBand20.AppearanceHeader.Options.UseFont = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand20.Caption = "o Código Único";
            this.gridBand20.Columns.Add(this.bandedGridColumn16);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 0;
            this.gridBand20.Width = 148;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.bandedGridColumn16.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn16.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn16.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn16.Caption = "de la Operación";
            this.bandedGridColumn16.FieldName = "FOLIO";
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn16.Visible = true;
            this.bandedGridColumn16.Width = 148;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.gridBand21.AppearanceHeader.Options.UseFont = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "Fecha";
            this.gridBand21.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand22});
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 1;
            this.gridBand21.Width = 82;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.gridBand22.AppearanceHeader.Options.UseFont = true;
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "o periodo de";
            this.gridBand22.Columns.Add(this.bandedGridColumn17);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 0;
            this.gridBand22.Width = 82;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.bandedGridColumn17.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn17.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn17.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn17.Caption = "la Operación";
            this.bandedGridColumn17.FieldName = "FECHA";
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn17.Visible = true;
            this.bandedGridColumn17.Width = 82;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.gridBand23.AppearanceHeader.Options.UseFont = true;
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "Glosa o";
            this.gridBand23.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand24});
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 2;
            this.gridBand23.Width = 291;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.gridBand24.AppearanceHeader.Options.UseFont = true;
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "descripción";
            this.gridBand24.Columns.Add(this.bandedGridColumn18);
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 0;
            this.gridBand24.Width = 291;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7F);
            this.bandedGridColumn18.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn18.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn18.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn18.Caption = "de la Operación";
            this.bandedGridColumn18.FieldName = "GLOSA";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "GLOSA", "TOTALES")});
            this.bandedGridColumn18.Visible = true;
            this.bandedGridColumn18.Width = 291;
            // 
            // gridBand25
            // 
            this.gridBand25.Caption = "gridBand13";
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.Visible = false;
            this.gridBand25.VisibleIndex = -1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnver);
            this.groupBox2.Controls.Add(this.txtmesdesc);
            this.groupBox2.Controls.Add(this.txtmescod);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.txtniveldesc);
            this.groupBox2.Controls.Add(this.txtnivel);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(579, 41);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // btnver
            // 
            this.btnver.Location = new System.Drawing.Point(495, 12);
            this.btnver.Name = "btnver";
            this.btnver.Size = new System.Drawing.Size(75, 23);
            this.btnver.TabIndex = 9;
            this.btnver.Text = "Ver";
            this.btnver.Click += new System.EventHandler(this.btnver_Click);
            // 
            // txtmesdesc
            // 
            this.txtmesdesc.Enabled = false;
            this.txtmesdesc.EnterMoveNextControl = true;
            this.txtmesdesc.Location = new System.Drawing.Point(348, 15);
            this.txtmesdesc.Name = "txtmesdesc";
            this.txtmesdesc.Size = new System.Drawing.Size(141, 20);
            this.txtmesdesc.TabIndex = 5;
            // 
            // txtmescod
            // 
            this.txtmescod.EnterMoveNextControl = true;
            this.txtmescod.Location = new System.Drawing.Point(307, 15);
            this.txtmescod.Name = "txtmescod";
            this.txtmescod.Size = new System.Drawing.Size(38, 20);
            this.txtmescod.TabIndex = 4;
            this.txtmescod.EditValueChanged += new System.EventHandler(this.txtmescod_EditValueChanged);
            this.txtmescod.TextChanged += new System.EventHandler(this.txtmescod_TextChanged);
            this.txtmescod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(278, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Mes:";
            // 
            // txtniveldesc
            // 
            this.txtniveldesc.Enabled = false;
            this.txtniveldesc.EnterMoveNextControl = true;
            this.txtniveldesc.Location = new System.Drawing.Point(88, 15);
            this.txtniveldesc.Name = "txtniveldesc";
            this.txtniveldesc.Size = new System.Drawing.Size(184, 20);
            this.txtniveldesc.TabIndex = 2;
            // 
            // txtnivel
            // 
            this.txtnivel.EnterMoveNextControl = true;
            this.txtnivel.Location = new System.Drawing.Point(47, 15);
            this.txtnivel.Name = "txtnivel";
            this.txtnivel.Size = new System.Drawing.Size(39, 20);
            this.txtnivel.TabIndex = 1;
            this.txtnivel.TextChanged += new System.EventHandler(this.txtnivel_TextChanged);
            this.txtnivel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnivel_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(14, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nivel:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.TabControl_V5_Reporte);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1052, 456);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Libro Electroninco";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // TabControl_V5_Reporte
            // 
            this.TabControl_V5_Reporte.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl_V5_Reporte.Controls.Add(this.TabPage6);
            this.TabControl_V5_Reporte.Controls.Add(this.TabPage7);
            this.TabControl_V5_Reporte.Location = new System.Drawing.Point(8, 57);
            this.TabControl_V5_Reporte.Name = "TabControl_V5_Reporte";
            this.TabControl_V5_Reporte.SelectedIndex = 0;
            this.TabControl_V5_Reporte.Size = new System.Drawing.Size(1038, 393);
            this.TabControl_V5_Reporte.TabIndex = 231;
            // 
            // TabPage6
            // 
            this.TabPage6.Controls.Add(this.DgvFormatoSimplificado02);
            this.TabPage6.Location = new System.Drawing.Point(4, 22);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage6.Size = new System.Drawing.Size(1030, 367);
            this.TabPage6.TabIndex = 0;
            this.TabPage6.Text = "Libro 5.0";
            this.TabPage6.UseVisualStyleBackColor = true;
            // 
            // DgvFormatoSimplificado02
            // 
            this.DgvFormatoSimplificado02.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DgvFormatoSimplificado02.Cursor = System.Windows.Forms.Cursors.Default;
            this.DgvFormatoSimplificado02.Location = new System.Drawing.Point(3, 2);
            this.DgvFormatoSimplificado02.MainView = this.ViewFormatoSimplificado02;
            this.DgvFormatoSimplificado02.Name = "DgvFormatoSimplificado02";
            this.DgvFormatoSimplificado02.Size = new System.Drawing.Size(1021, 359);
            this.DgvFormatoSimplificado02.TabIndex = 252;
            this.DgvFormatoSimplificado02.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ViewFormatoSimplificado02});
            // 
            // ViewFormatoSimplificado02
            // 
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.Empty.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(237)))));
            this.ViewFormatoSimplificado02.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.EvenRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.EvenRow.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.FilterPanel.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.FilterPanel.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.ViewFormatoSimplificado02.Appearance.FixedLine.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.FocusedCell.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.FocusedCell.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.FocusedRow.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.FooterPanel.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.FooterPanel.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.GroupButton.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupButton.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupButton.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.GroupFooter.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupFooter.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.GroupPanel.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupPanel.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.GroupRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupRow.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.GroupRow.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.ViewFormatoSimplificado02.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.ViewFormatoSimplificado02.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.ViewFormatoSimplificado02.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.ViewFormatoSimplificado02.Appearance.HorzLine.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.ViewFormatoSimplificado02.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.OddRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.OddRow.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(248)))));
            this.ViewFormatoSimplificado02.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.ViewFormatoSimplificado02.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.ViewFormatoSimplificado02.Appearance.Preview.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.Preview.Options.UseFont = true;
            this.ViewFormatoSimplificado02.Appearance.Preview.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.ViewFormatoSimplificado02.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.ViewFormatoSimplificado02.Appearance.Row.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.Row.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.RowSeparator.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(133)))), ((int)(((byte)(179)))));
            this.ViewFormatoSimplificado02.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.ViewFormatoSimplificado02.Appearance.SelectedRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.SelectedRow.Options.UseForeColor = true;
            this.ViewFormatoSimplificado02.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.ViewFormatoSimplificado02.Appearance.TopNewRow.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.ViewFormatoSimplificado02.Appearance.VertLine.Options.UseBackColor = true;
            this.ViewFormatoSimplificado02.ColumnPanelRowHeight = 30;
            this.ViewFormatoSimplificado02.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn19,
            this.GridColumn20,
            this.GridColumn25,
            this.GridColumn26,
            this.GridColumn27,
            this.GridColumn28,
            this.GridColumn29,
            this.GridColumn31,
            this.GridColumn32,
            this.GridColumn33,
            this.GridColumn14,
            this.GridColumn15,
            this.GridColumn16,
            this.GridColumn41,
            this.GridColumn45,
            this.GridColumn46,
            this.GridColumn47,
            this.GridColumn48,
            this.GridColumn49,
            this.GridColumn50,
            this.GridColumn51,
            this.GridColumn52});
            this.ViewFormatoSimplificado02.GridControl = this.DgvFormatoSimplificado02;
            this.ViewFormatoSimplificado02.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "NomEntidad", null, "")});
            this.ViewFormatoSimplificado02.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.ViewFormatoSimplificado02.Name = "ViewFormatoSimplificado02";
            this.ViewFormatoSimplificado02.OptionsFilter.AllowColumnMRUFilterList = false;
            this.ViewFormatoSimplificado02.OptionsFilter.AllowMRUFilterList = false;
            this.ViewFormatoSimplificado02.OptionsPrint.AutoWidth = false;
            this.ViewFormatoSimplificado02.OptionsView.ColumnAutoWidth = false;
            this.ViewFormatoSimplificado02.OptionsView.EnableAppearanceEvenRow = true;
            this.ViewFormatoSimplificado02.OptionsView.EnableAppearanceOddRow = true;
            this.ViewFormatoSimplificado02.OptionsView.ShowAutoFilterRow = true;
            this.ViewFormatoSimplificado02.OptionsView.ShowFooter = true;
            this.ViewFormatoSimplificado02.OptionsView.ShowGroupPanel = false;
            // 
            // GridColumn19
            // 
            this.GridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn19.Caption = "PLE";
            this.GridColumn19.FieldName = "ple_V5";
            this.GridColumn19.Name = "GridColumn19";
            this.GridColumn19.OptionsColumn.AllowEdit = false;
            this.GridColumn19.OptionsColumn.FixedWidth = true;
            this.GridColumn19.Width = 124;
            // 
            // GridColumn20
            // 
            this.GridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn20.Caption = "PERIODO";
            this.GridColumn20.FieldName = "periodo_V5";
            this.GridColumn20.Name = "GridColumn20";
            this.GridColumn20.OptionsColumn.AllowEdit = false;
            this.GridColumn20.OptionsColumn.FixedWidth = true;
            this.GridColumn20.Visible = true;
            this.GridColumn20.VisibleIndex = 0;
            this.GridColumn20.Width = 95;
            // 
            // GridColumn25
            // 
            this.GridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn25.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn25.Caption = "CUO";
            this.GridColumn25.FieldName = "cuo_V5";
            this.GridColumn25.Name = "GridColumn25";
            this.GridColumn25.OptionsColumn.AllowEdit = false;
            this.GridColumn25.OptionsColumn.FixedWidth = true;
            this.GridColumn25.Visible = true;
            this.GridColumn25.VisibleIndex = 1;
            this.GridColumn25.Width = 102;
            // 
            // GridColumn26
            // 
            this.GridColumn26.Caption = "CORRELATIVO";
            this.GridColumn26.FieldName = "correlativo_V5";
            this.GridColumn26.Name = "GridColumn26";
            this.GridColumn26.Visible = true;
            this.GridColumn26.VisibleIndex = 2;
            // 
            // GridColumn27
            // 
            this.GridColumn27.Caption = "CUENTA_CONTABLE";
            this.GridColumn27.FieldName = "cod_plan_cuentas_V5";
            this.GridColumn27.Name = "GridColumn27";
            this.GridColumn27.Visible = true;
            this.GridColumn27.VisibleIndex = 3;
            // 
            // GridColumn28
            // 
            this.GridColumn28.Caption = "COD_UNIDAD_OPERACION";
            this.GridColumn28.FieldName = "cod_unidad_operacion_V5";
            this.GridColumn28.Name = "GridColumn28";
            this.GridColumn28.Visible = true;
            this.GridColumn28.VisibleIndex = 4;
            // 
            // GridColumn29
            // 
            this.GridColumn29.Caption = "COD_CENTRO_COSTOS";
            this.GridColumn29.FieldName = "cod_centro_costos_V5";
            this.GridColumn29.Name = "GridColumn29";
            this.GridColumn29.Visible = true;
            this.GridColumn29.VisibleIndex = 5;
            // 
            // GridColumn31
            // 
            this.GridColumn31.Caption = "TIPO_MONEDA_ORIGEN";
            this.GridColumn31.FieldName = "tipo_moneda_V5";
            this.GridColumn31.Name = "GridColumn31";
            this.GridColumn31.Visible = true;
            this.GridColumn31.VisibleIndex = 6;
            // 
            // GridColumn32
            // 
            this.GridColumn32.Caption = "TIP_DOC_EMISOR";
            this.GridColumn32.FieldName = "tipo_doc_emisor_V5";
            this.GridColumn32.Name = "GridColumn32";
            this.GridColumn32.Visible = true;
            this.GridColumn32.VisibleIndex = 7;
            // 
            // GridColumn33
            // 
            this.GridColumn33.Caption = "NUMERO_DOC_EMISOR";
            this.GridColumn33.FieldName = "numero_doc_emisor_V5";
            this.GridColumn33.Name = "GridColumn33";
            this.GridColumn33.Visible = true;
            this.GridColumn33.VisibleIndex = 8;
            // 
            // GridColumn14
            // 
            this.GridColumn14.Caption = "TIPO COMPROBANTE PAGO";
            this.GridColumn14.FieldName = "tipo_Cmoprobante_pago_V5";
            this.GridColumn14.Name = "GridColumn14";
            this.GridColumn14.Visible = true;
            this.GridColumn14.VisibleIndex = 9;
            // 
            // GridColumn15
            // 
            this.GridColumn15.Caption = "SERIE";
            this.GridColumn15.FieldName = "serie_V5";
            this.GridColumn15.Name = "GridColumn15";
            this.GridColumn15.Visible = true;
            this.GridColumn15.VisibleIndex = 10;
            // 
            // GridColumn16
            // 
            this.GridColumn16.Caption = "NUMERO";
            this.GridColumn16.FieldName = "numero_V5";
            this.GridColumn16.Name = "GridColumn16";
            this.GridColumn16.Visible = true;
            this.GridColumn16.VisibleIndex = 11;
            // 
            // GridColumn41
            // 
            this.GridColumn41.Caption = "FECHA CONTABLE";
            this.GridColumn41.FieldName = "fecha_contable_V5";
            this.GridColumn41.Name = "GridColumn41";
            this.GridColumn41.Visible = true;
            this.GridColumn41.VisibleIndex = 12;
            // 
            // GridColumn45
            // 
            this.GridColumn45.Caption = "FECHA VENCIMIENTO";
            this.GridColumn45.FieldName = "fecha_vencimiento_V5";
            this.GridColumn45.Name = "GridColumn45";
            this.GridColumn45.Visible = true;
            this.GridColumn45.VisibleIndex = 13;
            // 
            // GridColumn46
            // 
            this.GridColumn46.Caption = "FECHA OPERACION";
            this.GridColumn46.FieldName = "fecha_operacion_V5";
            this.GridColumn46.Name = "GridColumn46";
            this.GridColumn46.Visible = true;
            this.GridColumn46.VisibleIndex = 14;
            // 
            // GridColumn47
            // 
            this.GridColumn47.Caption = "GLOSA";
            this.GridColumn47.FieldName = "glosa_V5";
            this.GridColumn47.Name = "GridColumn47";
            this.GridColumn47.Visible = true;
            this.GridColumn47.VisibleIndex = 15;
            // 
            // GridColumn48
            // 
            this.GridColumn48.Caption = "GLOSA REFERENCIAL";
            this.GridColumn48.FieldName = "glosa_referencial_V5";
            this.GridColumn48.Name = "GridColumn48";
            this.GridColumn48.Visible = true;
            this.GridColumn48.VisibleIndex = 16;
            // 
            // GridColumn49
            // 
            this.GridColumn49.Caption = "DEBE";
            this.GridColumn49.DisplayFormat.FormatString = "n2";
            this.GridColumn49.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn49.FieldName = "debe_V5";
            this.GridColumn49.Name = "GridColumn49";
            this.GridColumn49.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "debe_V5", "{0:n2}")});
            this.GridColumn49.Visible = true;
            this.GridColumn49.VisibleIndex = 17;
            // 
            // GridColumn50
            // 
            this.GridColumn50.Caption = "HABER";
            this.GridColumn50.DisplayFormat.FormatString = "n2";
            this.GridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn50.FieldName = "haber_V5";
            this.GridColumn50.Name = "GridColumn50";
            this.GridColumn50.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "haber_V5", "{0:n2}")});
            this.GridColumn50.Visible = true;
            this.GridColumn50.VisibleIndex = 18;
            // 
            // GridColumn51
            // 
            this.GridColumn51.Caption = "DATO ESTRUCTURADO";
            this.GridColumn51.FieldName = "dato_estructurado_V5";
            this.GridColumn51.Name = "GridColumn51";
            this.GridColumn51.Visible = true;
            this.GridColumn51.VisibleIndex = 19;
            // 
            // GridColumn52
            // 
            this.GridColumn52.Caption = "ESTADO";
            this.GridColumn52.FieldName = "estado_V5";
            this.GridColumn52.Name = "GridColumn52";
            this.GridColumn52.Visible = true;
            this.GridColumn52.VisibleIndex = 20;
            // 
            // TabPage7
            // 
            this.TabPage7.Controls.Add(this.gvElectronico_V5_Det);
            this.TabPage7.Location = new System.Drawing.Point(4, 22);
            this.TabPage7.Name = "TabPage7";
            this.TabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage7.Size = new System.Drawing.Size(1030, 367);
            this.TabPage7.TabIndex = 1;
            this.TabPage7.Text = "Detalle del plan Contable ";
            this.TabPage7.UseVisualStyleBackColor = true;
            // 
            // gvElectronico_V5_Det
            // 
            this.gvElectronico_V5_Det.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvElectronico_V5_Det.Cursor = System.Windows.Forms.Cursors.Default;
            this.gvElectronico_V5_Det.Location = new System.Drawing.Point(6, 7);
            this.gvElectronico_V5_Det.MainView = this.GridViewAsientoDet_V5_Det;
            this.gvElectronico_V5_Det.Name = "gvElectronico_V5_Det";
            this.gvElectronico_V5_Det.Size = new System.Drawing.Size(1018, 347);
            this.gvElectronico_V5_Det.TabIndex = 253;
            this.gvElectronico_V5_Det.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewAsientoDet_V5_Det});
            // 
            // GridViewAsientoDet_V5_Det
            // 
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.Empty.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(237)))));
            this.GridViewAsientoDet_V5_Det.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupButton.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.GridViewAsientoDet_V5_Det.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.GridViewAsientoDet_V5_Det.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.GridViewAsientoDet_V5_Det.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.GridViewAsientoDet_V5_Det.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.OddRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.OddRow.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(248)))));
            this.GridViewAsientoDet_V5_Det.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridViewAsientoDet_V5_Det.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.GridViewAsientoDet_V5_Det.Appearance.Preview.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.Preview.Options.UseFont = true;
            this.GridViewAsientoDet_V5_Det.Appearance.Preview.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.GridViewAsientoDet_V5_Det.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridViewAsientoDet_V5_Det.Appearance.Row.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.Row.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(133)))), ((int)(((byte)(179)))));
            this.GridViewAsientoDet_V5_Det.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.GridViewAsientoDet_V5_Det.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridViewAsientoDet_V5_Det.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.GridViewAsientoDet_V5_Det.Appearance.VertLine.Options.UseBackColor = true;
            this.GridViewAsientoDet_V5_Det.ColumnPanelRowHeight = 30;
            this.GridViewAsientoDet_V5_Det.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn34,
            this.GridColumn35,
            this.GridColumn36,
            this.GridColumn37,
            this.GridColumn38,
            this.GridColumn39,
            this.GridColumn40});
            this.GridViewAsientoDet_V5_Det.GridControl = this.gvElectronico_V5_Det;
            this.GridViewAsientoDet_V5_Det.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "NomEntidad", null, "")});
            this.GridViewAsientoDet_V5_Det.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.GridViewAsientoDet_V5_Det.Name = "GridViewAsientoDet_V5_Det";
            this.GridViewAsientoDet_V5_Det.OptionsFilter.AllowColumnMRUFilterList = false;
            this.GridViewAsientoDet_V5_Det.OptionsFilter.AllowMRUFilterList = false;
            this.GridViewAsientoDet_V5_Det.OptionsPrint.AutoWidth = false;
            this.GridViewAsientoDet_V5_Det.OptionsView.ColumnAutoWidth = false;
            this.GridViewAsientoDet_V5_Det.OptionsView.EnableAppearanceEvenRow = true;
            this.GridViewAsientoDet_V5_Det.OptionsView.EnableAppearanceOddRow = true;
            this.GridViewAsientoDet_V5_Det.OptionsView.ShowAutoFilterRow = true;
            this.GridViewAsientoDet_V5_Det.OptionsView.ShowFooter = true;
            this.GridViewAsientoDet_V5_Det.OptionsView.ShowGroupPanel = false;
            // 
            // GridColumn34
            // 
            this.GridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn34.Caption = "PLE";
            this.GridColumn34.FieldName = "ple_V4";
            this.GridColumn34.Name = "GridColumn34";
            this.GridColumn34.OptionsColumn.AllowEdit = false;
            this.GridColumn34.OptionsColumn.FixedWidth = true;
            this.GridColumn34.Width = 124;
            // 
            // GridColumn35
            // 
            this.GridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn35.Caption = "PERIODO";
            this.GridColumn35.FieldName = "periodo_V5";
            this.GridColumn35.Name = "GridColumn35";
            this.GridColumn35.OptionsColumn.AllowEdit = false;
            this.GridColumn35.OptionsColumn.FixedWidth = true;
            this.GridColumn35.Visible = true;
            this.GridColumn35.VisibleIndex = 0;
            this.GridColumn35.Width = 95;
            // 
            // GridColumn36
            // 
            this.GridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn36.Caption = "COD. CUENTA CONTABLE";
            this.GridColumn36.FieldName = "cuenta_ctb_V5";
            this.GridColumn36.Name = "GridColumn36";
            this.GridColumn36.OptionsColumn.AllowEdit = false;
            this.GridColumn36.OptionsColumn.FixedWidth = true;
            this.GridColumn36.Visible = true;
            this.GridColumn36.VisibleIndex = 1;
            this.GridColumn36.Width = 91;
            // 
            // GridColumn37
            // 
            this.GridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn37.Caption = "DESC. CUENTA CONTABLE";
            this.GridColumn37.FieldName = "Cta_Descripcion";
            this.GridColumn37.Name = "GridColumn37";
            this.GridColumn37.OptionsColumn.AllowEdit = false;
            this.GridColumn37.OptionsColumn.FixedWidth = true;
            this.GridColumn37.Visible = true;
            this.GridColumn37.VisibleIndex = 2;
            this.GridColumn37.Width = 257;
            // 
            // GridColumn38
            // 
            this.GridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn38.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn38.Caption = "COD. PLAN DE CUENTAS DEUDOR";
            this.GridColumn38.FieldName = "cod_plan_cuentas_V5";
            this.GridColumn38.Name = "GridColumn38";
            this.GridColumn38.OptionsColumn.AllowEdit = false;
            this.GridColumn38.OptionsColumn.FixedWidth = true;
            this.GridColumn38.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.GridColumn38.Visible = true;
            this.GridColumn38.VisibleIndex = 3;
            this.GridColumn38.Width = 96;
            // 
            // GridColumn39
            // 
            this.GridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn39.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridColumn39.Caption = "DES. PLAN DE CUENTAS DEUDOR";
            this.GridColumn39.FieldName = "desc_Plan_cuenta";
            this.GridColumn39.Name = "GridColumn39";
            this.GridColumn39.OptionsColumn.AllowEdit = false;
            this.GridColumn39.OptionsColumn.FixedWidth = true;
            this.GridColumn39.Visible = true;
            this.GridColumn39.VisibleIndex = 4;
            this.GridColumn39.Width = 226;
            // 
            // GridColumn40
            // 
            this.GridColumn40.Caption = "ESTADO";
            this.GridColumn40.FieldName = "estado_V5";
            this.GridColumn40.Name = "GridColumn40";
            this.GridColumn40.OptionsColumn.AllowEdit = false;
            this.GridColumn40.OptionsColumn.FixedWidth = true;
            this.GridColumn40.Visible = true;
            this.GridColumn40.VisibleIndex = 5;
            this.GridColumn40.Width = 81;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtmesdesc2);
            this.groupBox3.Controls.Add(this.txtmescod2);
            this.groupBox3.Controls.Add(this.BtnElectronico_V5_VistaPrevia_Detalle);
            this.groupBox3.Controls.Add(this.BtnVistaPr2);
            this.groupBox3.Controls.Add(this.Label3);
            this.groupBox3.Location = new System.Drawing.Point(8, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(784, 45);
            this.groupBox3.TabIndex = 229;
            this.groupBox3.TabStop = false;
            // 
            // txtmesdesc2
            // 
            this.txtmesdesc2.Enabled = false;
            this.txtmesdesc2.EnterMoveNextControl = true;
            this.txtmesdesc2.Location = new System.Drawing.Point(77, 14);
            this.txtmesdesc2.Name = "txtmesdesc2";
            this.txtmesdesc2.Size = new System.Drawing.Size(141, 20);
            this.txtmesdesc2.TabIndex = 20;
            // 
            // txtmescod2
            // 
            this.txtmescod2.EnterMoveNextControl = true;
            this.txtmescod2.Location = new System.Drawing.Point(36, 14);
            this.txtmescod2.Name = "txtmescod2";
            this.txtmescod2.Size = new System.Drawing.Size(38, 20);
            this.txtmescod2.TabIndex = 19;
            this.txtmescod2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod2_KeyDown);
            // 
            // BtnElectronico_V5_VistaPrevia_Detalle
            // 
            this.BtnElectronico_V5_VistaPrevia_Detalle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnElectronico_V5_VistaPrevia_Detalle.Location = new System.Drawing.Point(423, 9);
            this.BtnElectronico_V5_VistaPrevia_Detalle.Name = "BtnElectronico_V5_VistaPrevia_Detalle";
            this.BtnElectronico_V5_VistaPrevia_Detalle.Size = new System.Drawing.Size(166, 28);
            this.BtnElectronico_V5_VistaPrevia_Detalle.TabIndex = 16;
            this.BtnElectronico_V5_VistaPrevia_Detalle.Text = "Vista Previa Plan Contable";
            this.BtnElectronico_V5_VistaPrevia_Detalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnElectronico_V5_VistaPrevia_Detalle.UseVisualStyleBackColor = true;
            this.BtnElectronico_V5_VistaPrevia_Detalle.Click += new System.EventHandler(this.BtnElectronico_V5_VistaPrevia_Detalle_Click);
            // 
            // BtnVistaPr2
            // 
            this.BtnVistaPr2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnVistaPr2.Location = new System.Drawing.Point(224, 10);
            this.BtnVistaPr2.Name = "BtnVistaPr2";
            this.BtnVistaPr2.Size = new System.Drawing.Size(166, 28);
            this.BtnVistaPr2.TabIndex = 5;
            this.BtnVistaPr2.Text = "Vista Previa Libro Diario";
            this.BtnVistaPr2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnVistaPr2.UseVisualStyleBackColor = true;
            this.BtnVistaPr2.Click += new System.EventHandler(this.BtnVistaPr2_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(4, 18);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(30, 13);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Mes:";
            // 
            // FuncionDebe
            // 
            this.FuncionDebe.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.FuncionDebe.AppearanceCell.Options.UseFont = true;
            this.FuncionDebe.AppearanceCell.Options.UseTextOptions = true;
            this.FuncionDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FuncionDebe.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuncionDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.FuncionDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuncionDebe.Caption = "PÉRDIDAS ";
            this.FuncionDebe.DisplayFormat.FormatString = "n2";
            this.FuncionDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FuncionDebe.FieldName = "FuncionDebe";
            this.FuncionDebe.Name = "FuncionDebe";
            this.FuncionDebe.OptionsColumn.AllowEdit = false;
            this.FuncionDebe.OptionsColumn.AllowMove = false;
            this.FuncionDebe.OptionsColumn.AllowSize = false;
            this.FuncionDebe.OptionsColumn.FixedWidth = true;
            this.FuncionDebe.OptionsFilter.AllowAutoFilter = false;
            this.FuncionDebe.OptionsFilter.AllowFilter = false;
            this.FuncionDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionDebe", "{0:n2}")});
            this.FuncionDebe.Visible = true;
            this.FuncionDebe.VisibleIndex = 2;
            this.FuncionDebe.Width = 104;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "NATURALEZA";
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Columns.Add(this.bandedGridColumn12);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 166;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "PERDIDAS N";
            this.bandedGridColumn11.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn11.FieldName = "NatuDebe";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuDebe", "{0:n2}")});
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 83;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "GANANCIAS N";
            this.bandedGridColumn12.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn12.FieldName = "NatuHaber";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuHaber", "{0:n2}")});
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 83;
            // 
            // RbtAcumulado
            // 
            this.RbtAcumulado.Location = new System.Drawing.Point(0, 0);
            this.RbtAcumulado.Name = "RbtAcumulado";
            this.RbtAcumulado.Size = new System.Drawing.Size(75, 19);
            this.RbtAcumulado.TabIndex = 2;
            // 
            // InvenHaber
            // 
            this.InvenHaber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.InvenHaber.AppearanceCell.Options.UseFont = true;
            this.InvenHaber.AppearanceCell.Options.UseTextOptions = true;
            this.InvenHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.InvenHaber.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.InvenHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.InvenHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InvenHaber.Caption = "PASIVO Y PATRIMONIO";
            this.InvenHaber.DisplayFormat.FormatString = "n2";
            this.InvenHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.InvenHaber.FieldName = "InvenHaber";
            this.InvenHaber.Name = "InvenHaber";
            this.InvenHaber.OptionsColumn.AllowEdit = false;
            this.InvenHaber.OptionsColumn.AllowMove = false;
            this.InvenHaber.OptionsColumn.AllowSize = false;
            this.InvenHaber.OptionsColumn.FixedWidth = true;
            this.InvenHaber.OptionsFilter.AllowAutoFilter = false;
            this.InvenHaber.OptionsFilter.AllowFilter = false;
            this.InvenHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenHaber", "{0:n2}")});
            this.InvenHaber.Visible = true;
            this.InvenHaber.VisibleIndex = 1;
            this.InvenHaber.Width = 104;
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatos.Location = new System.Drawing.Point(3, 85);
            this.dgvdatos.MainView = this.bandedGridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(993, 366);
            this.dgvdatos.TabIndex = 6;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12});
            this.bandedGridView1.GridControl = this.dgvdatos;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "CUENTA";
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 295;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "CODIGO";
            this.bandedGridColumn1.FieldName = "Cuenta";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 86;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "DENOMINACION";
            this.bandedGridColumn2.FieldName = "Descripcion";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 209;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "SUMAS DEL MAYOR";
            this.gridBand2.Columns.Add(this.bandedGridColumn3);
            this.gridBand2.Columns.Add(this.bandedGridColumn4);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 222;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "DEBE";
            this.bandedGridColumn3.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn3.FieldName = "Debe";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Debe", "{0:n2}")});
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 129;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "HABER";
            this.bandedGridColumn4.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn4.FieldName = "Haber";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Haber", "{0:n2}")});
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 93;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "SALDOS";
            this.gridBand3.Columns.Add(this.bandedGridColumn5);
            this.gridBand3.Columns.Add(this.bandedGridColumn6);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 169;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "DEUDOR";
            this.bandedGridColumn5.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn5.FieldName = "SaldoDebe";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoDebe", "{0:n2}")});
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 94;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "ACREEDOR";
            this.bandedGridColumn6.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn6.FieldName = "SaldoHaber";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoHaber", "{0:n2}")});
            this.bandedGridColumn6.Visible = true;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "INVENTARIO";
            this.gridBand4.Columns.Add(this.bandedGridColumn7);
            this.gridBand4.Columns.Add(this.bandedGridColumn8);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 240;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "ACTIVO";
            this.bandedGridColumn7.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn7.FieldName = "InvenDebe";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenDebe", "{0:n2}")});
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 113;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "PASIVO Y PATRIMONIO";
            this.bandedGridColumn8.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn8.FieldName = "InvenHaber";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenHaber", "{0:n2}")});
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 127;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "FUNCION";
            this.gridBand5.Columns.Add(this.bandedGridColumn9);
            this.gridBand5.Columns.Add(this.bandedGridColumn10);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 150;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "PERDIDAS";
            this.bandedGridColumn9.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn9.FieldName = "FuncionDebe";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionDebe", "{0:n2}")});
            this.bandedGridColumn9.Visible = true;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "GANANCIAS";
            this.bandedGridColumn10.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn10.FieldName = "FuncionHaber";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionHaber", "{0:n2}")});
            this.bandedGridColumn10.Visible = true;
            // 
            // FuncionHaber
            // 
            this.FuncionHaber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.FuncionHaber.AppearanceCell.Options.UseFont = true;
            this.FuncionHaber.AppearanceCell.Options.UseTextOptions = true;
            this.FuncionHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FuncionHaber.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuncionHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.FuncionHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuncionHaber.Caption = "GANANCIAS";
            this.FuncionHaber.DisplayFormat.FormatString = "n2";
            this.FuncionHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FuncionHaber.FieldName = "FuncionHaber";
            this.FuncionHaber.Name = "FuncionHaber";
            this.FuncionHaber.OptionsColumn.AllowEdit = false;
            this.FuncionHaber.OptionsColumn.AllowMove = false;
            this.FuncionHaber.OptionsColumn.AllowSize = false;
            this.FuncionHaber.OptionsColumn.FixedWidth = true;
            this.FuncionHaber.OptionsFilter.AllowAutoFilter = false;
            this.FuncionHaber.OptionsFilter.AllowFilter = false;
            this.FuncionHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionHaber", "{0:n2}")});
            this.FuncionHaber.Visible = true;
            this.FuncionHaber.VisibleIndex = 3;
            this.FuncionHaber.Width = 104;
            // 
            // InvenDebe
            // 
            this.InvenDebe.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.InvenDebe.AppearanceCell.Options.UseFont = true;
            this.InvenDebe.AppearanceCell.Options.UseTextOptions = true;
            this.InvenDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.InvenDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.InvenDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InvenDebe.Caption = "ACTIVO";
            this.InvenDebe.DisplayFormat.FormatString = "n2";
            this.InvenDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.InvenDebe.FieldName = "InvenDebe";
            this.InvenDebe.Name = "InvenDebe";
            this.InvenDebe.OptionsColumn.AllowEdit = false;
            this.InvenDebe.OptionsColumn.AllowMove = false;
            this.InvenDebe.OptionsColumn.AllowSize = false;
            this.InvenDebe.OptionsColumn.FixedWidth = true;
            this.InvenDebe.OptionsFilter.AllowAutoFilter = false;
            this.InvenDebe.OptionsFilter.AllowFilter = false;
            this.InvenDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenDebe", "{0:n2}")});
            this.InvenDebe.Visible = true;
            this.InvenDebe.VisibleIndex = 0;
            this.InvenDebe.Width = 104;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RbtMensual);
            this.groupBox1.Controls.Add(this.RbtAcumulado);
            this.groupBox1.Location = new System.Drawing.Point(3, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(154, 41);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo";
            // 
            // RbtMensual
            // 
            this.RbtMensual.EnterMoveNextControl = true;
            this.RbtMensual.Location = new System.Drawing.Point(85, 18);
            this.RbtMensual.Name = "RbtMensual";
            this.RbtMensual.Properties.Caption = "Mensual";
            this.RbtMensual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.RbtMensual.Size = new System.Drawing.Size(66, 19);
            this.RbtMensual.TabIndex = 1;
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(430, 457);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(566, 48);
            this.GridControl1.TabIndex = 250;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NatuDebe,
            this.NatuHaber,
            this.InvenDebe,
            this.InvenHaber,
            this.FuncionDebe,
            this.FuncionHaber});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.NatuDebe;
            this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.GridView1.OptionsFilter.AllowMRUFilterList = false;
            this.GridView1.OptionsPrint.AutoWidth = false;
            this.GridView1.OptionsView.ShowColumnHeaders = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.Tag = "<Null>";
            // 
            // NatuHaber
            // 
            this.NatuHaber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.NatuHaber.AppearanceCell.Options.UseFont = true;
            this.NatuHaber.AppearanceCell.Options.UseTextOptions = true;
            this.NatuHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NatuHaber.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NatuHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.NatuHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NatuHaber.Caption = "GANANCIAS N";
            this.NatuHaber.DisplayFormat.FormatString = "n2";
            this.NatuHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NatuHaber.FieldName = "NatuHaber";
            this.NatuHaber.Name = "NatuHaber";
            this.NatuHaber.OptionsColumn.AllowEdit = false;
            this.NatuHaber.OptionsColumn.AllowMove = false;
            this.NatuHaber.OptionsColumn.AllowSize = false;
            this.NatuHaber.OptionsColumn.FixedWidth = true;
            this.NatuHaber.OptionsFilter.AllowAutoFilter = false;
            this.NatuHaber.OptionsFilter.AllowFilter = false;
            this.NatuHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuHaber", "{0:n2}")});
            this.NatuHaber.Visible = true;
            this.NatuHaber.VisibleIndex = 5;
            this.NatuHaber.Width = 104;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControl5);
            this.barManager1.DockControls.Add(this.barDockControl6);
            this.barManager1.DockControls.Add(this.barDockControl7);
            this.barManager1.DockControls.Add(this.barDockControl8);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnlimpiar,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2,
            this.btnimpresion,
            this.barButtonItem1,
            this.btnple});
            this.barManager1.MaxItemId = 7;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnimpresion),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnple)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 0;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // btnimpresion
            // 
            this.btnimpresion.Caption = "Impresion";
            this.btnimpresion.Id = 4;
            this.btnimpresion.ImageOptions.Image = global::Contable.Properties.Resources.Print_24px;
            this.btnimpresion.Name = "btnimpresion";
            this.btnimpresion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnimpresion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnimpresion_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Exportar";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnple
            // 
            this.btnple.Caption = "Generar PLE txt";
            this.btnple.Id = 6;
            this.btnple.ImageOptions.Image = global::Contable.Properties.Resources.TXT_24px;
            this.btnple.Name = "btnple";
            this.btnple.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnple.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnple_ItemClick);
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl5.Location = new System.Drawing.Point(0, 0);
            this.barDockControl5.Manager = this.barManager1;
            this.barDockControl5.Size = new System.Drawing.Size(1060, 32);
            // 
            // barDockControl6
            // 
            this.barDockControl6.CausesValidation = false;
            this.barDockControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl6.Location = new System.Drawing.Point(0, 514);
            this.barDockControl6.Manager = this.barManager1;
            this.barDockControl6.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControl7
            // 
            this.barDockControl7.CausesValidation = false;
            this.barDockControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl7.Location = new System.Drawing.Point(0, 32);
            this.barDockControl7.Manager = this.barManager1;
            this.barDockControl7.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl8.Location = new System.Drawing.Point(1060, 32);
            this.barDockControl8.Manager = this.barManager1;
            this.barDockControl8.Size = new System.Drawing.Size(0, 482);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_libro_diario_simplificado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 514);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl9);
            this.Controls.Add(this.barDockControl10);
            this.Controls.Add(this.barDockControl11);
            this.Controls.Add(this.barDockControl12);
            this.Controls.Add(this.barDockControl13);
            this.Controls.Add(this.barDockControl14);
            this.Controls.Add(this.barDockControl15);
            this.Controls.Add(this.barDockControl16);
            this.Controls.Add(this.barDockControl7);
            this.Controls.Add(this.barDockControl8);
            this.Controls.Add(this.barDockControl6);
            this.Controls.Add(this.barDockControl5);
            this.Name = "frm_libro_diario_simplificado";
            this.Text = "Libro diario simplificado";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BandGridReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridVisual)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtniveldesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnivel.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.TabControl_V5_Reporte.ResumeLayout(false);
            this.TabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvFormatoSimplificado02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewFormatoSimplificado02)).EndInit();
            this.TabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvElectronico_V5_Det)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAsientoDet_V5_Det)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbtAcumulado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RbtMensual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControl1;
        internal DevExpress.XtraBars.BarDockControl barDockControl2;
        internal DevExpress.XtraBars.BarDockControl barDockControl3;
        internal DevExpress.XtraBars.BarDockControl barDockControl4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        internal DevExpress.XtraBars.BarDockControl barDockControl9;
        internal DevExpress.XtraBars.BarDockControl barDockControl10;
        internal DevExpress.XtraBars.BarDockControl barDockControl11;
        internal DevExpress.XtraBars.BarDockControl barDockControl12;
        internal DevExpress.XtraBars.BarDockControl barDockControl13;
        internal DevExpress.XtraBars.BarDockControl barDockControl14;
        internal DevExpress.XtraBars.BarDockControl barDockControl15;
        internal DevExpress.XtraBars.BarDockControl barDockControl16;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton btnver;
        private DevExpress.XtraEditors.TextEdit txtmesdesc;
        private DevExpress.XtraEditors.TextEdit txtmescod;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtniveldesc;
        private DevExpress.XtraEditors.TextEdit txtnivel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn FuncionDebe;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn NatuDebe;
        private DevExpress.XtraEditors.CheckEdit RbtAcumulado;
        private DevExpress.XtraGrid.Columns.GridColumn InvenHaber;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn FuncionHaber;
        private DevExpress.XtraGrid.Columns.GridColumn InvenDebe;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.CheckEdit RbtMensual;
        private DevExpress.XtraGrid.GridControl GridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        private DevExpress.XtraGrid.Columns.GridColumn NatuHaber;
        internal DevExpress.XtraGrid.GridControl BandGridReport;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridView BandGridVisual;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandFechaEmi;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandMonExt;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandDocId;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand BandApelliyNom;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        internal DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        internal System.Windows.Forms.TabControl TabControl_V5_Reporte;
        internal System.Windows.Forms.TabPage TabPage6;
        internal DevExpress.XtraGrid.GridControl DgvFormatoSimplificado02;
        internal DevExpress.XtraGrid.Views.Grid.GridView ViewFormatoSimplificado02;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn19;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn20;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn25;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn26;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn27;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn28;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn29;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn31;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn32;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn33;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn14;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn15;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn16;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn41;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn45;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn46;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn47;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn48;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn49;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn50;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn51;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn52;
        internal System.Windows.Forms.TabPage TabPage7;
        internal DevExpress.XtraGrid.GridControl gvElectronico_V5_Det;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridViewAsientoDet_V5_Det;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn34;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn35;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn36;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn37;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn38;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn39;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn40;
        internal System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.TextEdit txtmesdesc2;
        private DevExpress.XtraEditors.TextEdit txtmescod2;
        internal System.Windows.Forms.Button BtnElectronico_V5_VistaPrevia_Detalle;
        internal System.Windows.Forms.Button BtnVistaPr2;
        internal System.Windows.Forms.Label Label3;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarButtonItem btnimpresion;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnple;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarDockControl barDockControl6;
        private DevExpress.XtraBars.BarDockControl barDockControl7;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}