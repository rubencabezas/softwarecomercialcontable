﻿using Contable.Reportes.Reportes_Impresiones;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Reportes.Libros_y_registros
{
    public partial class frm_estado_financiero_tributario : frm_fuente
    {
        public frm_estado_financiero_tributario()
        {
            InitializeComponent();
        }

        private void frm_estado_financiero_tributario_Load(object sender, EventArgs e)
        {

        }


        public List<Entidad_Formato_Balance_Tributario> Lista = new List<Entidad_Formato_Balance_Tributario>();
        public void Listar()
        {
            Entidad_Formato_Balance_Tributario Ent = new Entidad_Formato_Balance_Tributario();
            Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;

            try
            {
                Lista = log.Reporte_Balance_Tributario(Ent);
                if (Lista.Count > 0)
                {
                    List<Entidad_Formato_Balance_Tributario> ListaActivo = new List<Entidad_Formato_Balance_Tributario>();

                    var PrivView = from item in Lista
                                   where item.tipo == "A"
                                   //orderby item.Id_Anio descending
                                   select item;

                    ListaActivo = PrivView.ToList();


                    List<Entidad_Formato_Balance_Tributario> ListaPasivo = new List<Entidad_Formato_Balance_Tributario>();

                    var PrivViewPasivo = from item in Lista
                                         where item.tipo == "P"
                                         //orderby item.Id_Anio descending
                                         select item;

                    ListaPasivo = PrivViewPasivo.ToList();



                    List<Entidad_Formato_Balance_Tributario> ListaNaturaleza = new List<Entidad_Formato_Balance_Tributario>();

                    var PrivViewNat = from item in Lista
                                      where item.tipo == "N"
                                      //orderby item.Id_Anio descending
                                      select item;

                    ListaNaturaleza = PrivViewNat.ToList();


                    List<Entidad_Formato_Balance_Tributario> ListaFuncion = new List<Entidad_Formato_Balance_Tributario>();

                    var PrivViewFuncion = from item in Lista
                                          where item.tipo == "F"
                                          //orderby item.Id_Anio descending
                                          select item;

                    ListaFuncion = PrivViewFuncion.ToList();



                    dgvdatosactivo.DataSource = null;
                    dgvdatosactivo.DataSource = ListaActivo;


                    dgvdatospasivo.DataSource = null;
                    dgvdatospasivo.DataSource = ListaPasivo;

                    dgvdatosnaturaleza.DataSource = null;
                    dgvdatosnaturaleza.DataSource = ListaNaturaleza;


                    dgvdatosfuncion.DataSource = null;
                    dgvdatosfuncion.DataSource = ListaFuncion;

                }

            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }

        }

        private void btnverreporte_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Listar();
        }

        private void btnimpresion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Rpte_Sub_Estado_Finan_Banlance_A f01 = new Rpte_Sub_Estado_Finan_Banlance_A();

            List<Entidad_Formato_Balance_Tributario> ListaActivo = new List<Entidad_Formato_Balance_Tributario>();

            var PrivView = from item in Lista
                           where item.tipo == "A"
                           //orderby item.Id_Anio descending
                           select item;

            ListaActivo = PrivView.ToList();

            f01.DataSource = ListaActivo;


            Rpte_Sub_Estado_Finan_Banlance_B f02 = new Rpte_Sub_Estado_Finan_Banlance_B();

            List<Entidad_Formato_Balance_Tributario> ListaB = new List<Entidad_Formato_Balance_Tributario>();

            var PrivViewB = from item in Lista
                            where item.tipo == "P"
                            //orderby item.Id_Anio descending
                            select item;

            ListaB = PrivViewB.ToList();

            f02.DataSource = ListaB;


            Rpt_Estado_Situacion_Finan_Balance_General fp = new Rpt_Estado_Situacion_Finan_Balance_General();



            fp.TxtRazonSocial.Text = Actual_Conexion.EmpresaNombre;
            fp.TxtRuc.Text = Actual_Conexion.RucEmpresa;

            fp.xrSubreport1.ReportSource = f01;
            fp.xrSubreport2.ReportSource = f02;


            dynamic ribbonPreview = new PrintPreviewFormEx();
            fp.CreateDocument();
            fp.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            ribbonPreview.PrintingSystem = fp.PrintingSystem;
            // ribbonPreview.MdiParent = frm_principal;
            ribbonPreview.Show();
        }

        private void btnimpresionEstadoGanan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Rpte_Sub_Estado_Finan_Banlance_A f01 = new Rpte_Sub_Estado_Finan_Banlance_A();

            List<Entidad_Formato_Balance_Tributario> ListaActivo = new List<Entidad_Formato_Balance_Tributario>();

            var PrivView = from item in Lista
                           where item.tipo == "N"
                           //orderby item.Id_Anio descending
                           select item;

            ListaActivo = PrivView.ToList();

            f01.DataSource = ListaActivo;


            Rpte_Sub_Estado_Finan_Banlance_B f02 = new Rpte_Sub_Estado_Finan_Banlance_B();

            List<Entidad_Formato_Balance_Tributario> ListaB = new List<Entidad_Formato_Balance_Tributario>();

            var PrivViewB = from item in Lista
                            where item.tipo == "F"
                            //orderby item.Id_Anio descending
                            select item;

            ListaB = PrivViewB.ToList();

            f02.DataSource = ListaB;


            Rpt_Estado_Situacion_Finan_Balance_General fp = new Rpt_Estado_Situacion_Finan_Balance_General();



            fp.TxtRazonSocial.Text = Actual_Conexion.EmpresaNombre;
            fp.TxtRuc.Text = Actual_Conexion.RucEmpresa;

            fp.xrSubreport1.ReportSource = f01;
            fp.xrSubreport2.ReportSource = f02;


            dynamic ribbonPreview = new PrintPreviewFormEx();
            fp.CreateDocument();
            fp.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            ribbonPreview.PrintingSystem = fp.PrintingSystem;
            // ribbonPreview.MdiParent = frm_principal;
            ribbonPreview.Show();
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                dgvdatosactivo.DataSource = null;
                dgvdatospasivo.DataSource = null;
                dgvdatosfuncion.DataSource = null;
                dgvdatosnaturaleza.DataSource = null;
                Lista.Clear();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                if (tabControl1.SelectedIndex == 0)
                {
                    //dgvdatosactivo.ForceInitialize();
                    //dgvdatospasivo.ForceInitialize();
                    //compositeLink1.CreatePageForEachLink();
                    //XlsxExportOptions options = new DevExpress.XtraPrinting.XlsxExportOptions();
                    //options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    //compositeLink1.ExportToXlsx("GridFile.xlsx", options);
                    //Process.Start("GridFile.xlsx");

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "Excel (.xlsx)|  *.xlsx";
                    sfd.FileName = "Output.xlsx";
                    bool fileError = false;

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {

                        if (File.Exists(sfd.FileName))
                        {
                            try
                            {
                                File.Delete(sfd.FileName);
                            }
                            catch (IOException ex)
                            {
                                fileError = true;
                                MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                            }
                        }

                        if (!fileError)
                        {
                            Microsoft.Office.Interop.Excel.Application XcelApp = new Microsoft.Office.Interop.Excel.Application();
                            Microsoft.Office.Interop.Excel.Workbook workbook = XcelApp.Workbooks.Add(Type.Missing);
                            Microsoft.Office.Interop.Excel.Worksheet worksheet = null;

                            Microsoft.Office.Interop.Excel.Range chartRange;

                            worksheet = workbook.Sheets["Hoja1"];
                            worksheet = workbook.ActiveSheet;
                            worksheet.Name = "Output";

                            // worksheet.PageSetup.PrintGridlines = true;
                            worksheet.Application.ActiveWindow.DisplayGridlines = false;

                            worksheet.get_Range("A2", "F2").Merge();

                            chartRange = worksheet.get_Range("A2", "F2");
                            chartRange.FormulaR1C1 = "BALANCE GENERAL";
                            chartRange.HorizontalAlignment = 3;
                            chartRange.VerticalAlignment = 3;

                            worksheet.Cells[2, 1].Font.NAME = "Calibri";
                            worksheet.Cells[2, 1].Font.Bold = true;
                            worksheet.Cells[2, 1].Font.Size = 12;



                            // Grid 1
                            for (int i = 1; i < gridView1.Columns.Count + 1; i++)
                            {
                                worksheet.Cells[5, i] = gridView1.Columns[i - 1].Caption;
                                worksheet.Cells[5, i].Font.NAME = "Calibri";
                                worksheet.Cells[5, i].Font.Bold = true;
                                worksheet.Cells[5, i].Interior.Color = Color.Wheat;
                                worksheet.Cells[5, i].Font.Size = 12;
                                worksheet.Cells[5, 1].HorizontalAlignment = 3;
                                worksheet.Cells[5, 1].VerticalAlignment = 3;
                            }

                            for (int i = 0; i < gridView1.RowCount; i++)
                            {
                                for (int j = 0; j < gridView1.Columns.Count; j++)
                                {
                                    worksheet.Cells[i + 6, j + 1] = gridView1.GetRowCellValue(i, gridView1.Columns[j]);
                                    worksheet.Cells[i + 6, j + 1].NumberFormat = "0.00";
                                }
                            }

                            //Grid 2
                            for (int i = 1; i < gridView3.Columns.Count + 1; i++)
                            {
                                worksheet.Cells[5, i + 4] = gridView3.Columns[i - 1].Caption;
                                worksheet.Cells[5, i + 4].Font.NAME = "Calibri";
                                worksheet.Cells[5, i + 4].Font.Bold = true;
                                worksheet.Cells[5, i + 4].Interior.Color = Color.Wheat;
                                worksheet.Cells[5, i + 4].Font.Size = 12;
                                worksheet.Cells[5, i + 4].HorizontalAlignment = 3;
                                worksheet.Cells[5, i + 4].VerticalAlignment = 3;
                            }

                            for (int i = 0; i < gridView3.RowCount; i++)
                            {
                                for (int j = 0; j < gridView3.Columns.Count; j++)
                                {
                                    worksheet.Cells[i + 6, j + 5] = gridView3.GetRowCellValue(i, gridView3.Columns[j]);
                                    worksheet.Cells[i + 6, j + 5].NumberFormat = "0.00";
                                }
                            }


                            worksheet.Columns.AutoFit();
                            workbook.SaveAs(sfd.FileName);
                            XcelApp.Quit();

                            ReleaseObject(worksheet);
                            ReleaseObject(workbook);
                            ReleaseObject(XcelApp);

                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }

                    }


                }
                else if (tabControl1.SelectedIndex == 1)
                {
                    //dgvdatosnaturaleza.ForceInitialize();
                    //dgvdatosfuncion.ForceInitialize();
                    //compositeLink2.CreatePageForEachLink();
                    //XlsxExportOptions options = new DevExpress.XtraPrinting.XlsxExportOptions();
                    //options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    //compositeLink2.ExportToXlsx("GridFile2.xlsx", options);
                    //Process.Start("GridFile2.xlsx");

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "Excel (.xlsx)|  *.xlsx";
                    sfd.FileName = "Output.xlsx";
                    bool fileError = false;

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {

                        if (File.Exists(sfd.FileName))
                        {
                            try
                            {
                                File.Delete(sfd.FileName);
                            }
                            catch (IOException ex)
                            {
                                fileError = true;
                                MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                            }
                        }

                        if (!fileError)
                        {
                            Microsoft.Office.Interop.Excel.Application XcelApp = new Microsoft.Office.Interop.Excel.Application();
                            Microsoft.Office.Interop.Excel.Workbook workbook = XcelApp.Workbooks.Add(Type.Missing);
                            Microsoft.Office.Interop.Excel.Worksheet worksheet = null;

                            Microsoft.Office.Interop.Excel.Range chartRange;

                            worksheet = workbook.Sheets["Hoja1"];
                            worksheet = workbook.ActiveSheet;
                            worksheet.Name = "Output";

                            // worksheet.PageSetup.PrintGridlines = true;
                            worksheet.Application.ActiveWindow.DisplayGridlines = false;

                            worksheet.get_Range("A2", "F2").Merge();

                            chartRange = worksheet.get_Range("A2", "F2");
                            chartRange.FormulaR1C1 = "ESTADO DE GANANCIAS Y PERDIDAS";
                            chartRange.HorizontalAlignment = 3;
                            chartRange.VerticalAlignment = 3;

                            worksheet.Cells[2, 1].Font.NAME = "Calibri";
                            worksheet.Cells[2, 1].Font.Bold = true;
                            worksheet.Cells[2, 1].Font.Size = 12;



                            // Grid 1
                            for (int i = 1; i < gridView2.Columns.Count + 1; i++)
                            {
                                worksheet.Cells[5, i] = gridView2.Columns[i - 1].Caption;
                                worksheet.Cells[5, i].Font.NAME = "Calibri";
                                worksheet.Cells[5, i].Font.Bold = true;
                                worksheet.Cells[5, i].Interior.Color = Color.Wheat;
                                worksheet.Cells[5, i].Font.Size = 12;
                                worksheet.Cells[5, 1].HorizontalAlignment = 3;
                                worksheet.Cells[5, 1].VerticalAlignment = 3;
                            }

                            for (int i = 0; i < gridView2.RowCount; i++)
                            {
                                for (int j = 0; j < gridView2.Columns.Count; j++)
                                {
                                    worksheet.Cells[i + 6, j + 1] = gridView2.GetRowCellValue(i, gridView2.Columns[j]);
                                    worksheet.Cells[i + 6, j + 1].NumberFormat = "0.00";
                                }
                            }

                            //Grid 2
                            for (int i = 1; i < gridView4.Columns.Count + 1; i++)
                            {
                                worksheet.Cells[5, i + 4] = gridView3.Columns[i - 1].Caption;
                                worksheet.Cells[5, i + 4].Font.NAME = "Calibri";
                                worksheet.Cells[5, i + 4].Font.Bold = true;
                                worksheet.Cells[5, i + 4].Interior.Color = Color.Wheat;
                                worksheet.Cells[5, i + 4].Font.Size = 12;
                                worksheet.Cells[5, i + 4].HorizontalAlignment = 3;
                                worksheet.Cells[5, i + 4].VerticalAlignment = 3;
                            }

                            for (int i = 0; i < gridView4.RowCount; i++)
                            {
                                for (int j = 0; j < gridView4.Columns.Count; j++)
                                {
                                    worksheet.Cells[i + 6, j + 5] = gridView4.GetRowCellValue(i, gridView4.Columns[j]);
                                    worksheet.Cells[i + 6, j + 5].NumberFormat = "0.00";
                                }
                            }


                            worksheet.Columns.AutoFit();
                            workbook.SaveAs(sfd.FileName);
                            XcelApp.Quit();

                            ReleaseObject(worksheet);
                            ReleaseObject(workbook);
                            ReleaseObject(XcelApp);

                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.Message, "Error");
            }
            finally
            {
                GC.Collect();
            }
        }

    }
}
