﻿namespace Contable.Reportes.Libros_y_registros
{
    partial class frm_libro_mayor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl6 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl7 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl9 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl10 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl11 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl12 = new DevExpress.XtraBars.BarDockControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbacumulado = new System.Windows.Forms.RadioButton();
            this.rbmensual = new System.Windows.Forms.RadioButton();
            this.Rbt_xLibro = new System.Windows.Forms.RadioButton();
            this.Rbt_Centralizado = new System.Windows.Forms.RadioButton();
            this.btnbuscar = new DevExpress.XtraEditors.SimpleButton();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.txtmesdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.BandGridReport = new DevExpress.XtraGrid.GridControl();
            this.BandGridVisual = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.BandFechaEmi = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Voucher = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FechaDoc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Correlativo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Glosa = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CodLibroSun = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DocSunat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SerieDoc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.NumeroDoc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Descripcion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BandMonExt = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandDocId = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BandApelliyNom = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Debe = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Haber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.SaldoDebe = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SaldoHaber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.btnimpresion = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnple = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridVisual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 32);
            this.barDockControl1.Manager = null;
            this.barDockControl1.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1035, 32);
            this.barDockControl2.Manager = null;
            this.barDockControl2.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(0, 361);
            this.barDockControl3.Manager = null;
            this.barDockControl3.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 32);
            this.barDockControl4.Manager = null;
            this.barDockControl4.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl5.Location = new System.Drawing.Point(0, 32);
            this.barDockControl5.Manager = null;
            this.barDockControl5.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControl6
            // 
            this.barDockControl6.CausesValidation = false;
            this.barDockControl6.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl6.Location = new System.Drawing.Point(1035, 32);
            this.barDockControl6.Manager = null;
            this.barDockControl6.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControl7
            // 
            this.barDockControl7.CausesValidation = false;
            this.barDockControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl7.Location = new System.Drawing.Point(0, 361);
            this.barDockControl7.Manager = null;
            this.barDockControl7.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl8.Location = new System.Drawing.Point(0, 32);
            this.barDockControl8.Manager = null;
            this.barDockControl8.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControl9
            // 
            this.barDockControl9.CausesValidation = false;
            this.barDockControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl9.Location = new System.Drawing.Point(0, 32);
            this.barDockControl9.Manager = null;
            this.barDockControl9.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControl10
            // 
            this.barDockControl10.CausesValidation = false;
            this.barDockControl10.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl10.Location = new System.Drawing.Point(1035, 32);
            this.barDockControl10.Manager = null;
            this.barDockControl10.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControl11
            // 
            this.barDockControl11.CausesValidation = false;
            this.barDockControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl11.Location = new System.Drawing.Point(0, 361);
            this.barDockControl11.Manager = null;
            this.barDockControl11.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControl12
            // 
            this.barDockControl12.CausesValidation = false;
            this.barDockControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl12.Location = new System.Drawing.Point(0, 32);
            this.barDockControl12.Manager = null;
            this.barDockControl12.Size = new System.Drawing.Size(1035, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.Rbt_xLibro);
            this.groupBox1.Controls.Add(this.Rbt_Centralizado);
            this.groupBox1.Controls.Add(this.btnbuscar);
            this.groupBox1.Controls.Add(this.txtcuentadesc);
            this.groupBox1.Controls.Add(this.txtcuenta);
            this.groupBox1.Controls.Add(this.txtmesdesc);
            this.groupBox1.Controls.Add(this.txtmescod);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Location = new System.Drawing.Point(1, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1022, 49);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbacumulado);
            this.groupBox2.Controls.Add(this.rbmensual);
            this.groupBox2.Location = new System.Drawing.Point(738, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(168, 31);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // rbacumulado
            // 
            this.rbacumulado.AutoSize = true;
            this.rbacumulado.Location = new System.Drawing.Point(90, 11);
            this.rbacumulado.Name = "rbacumulado";
            this.rbacumulado.Size = new System.Drawing.Size(77, 17);
            this.rbacumulado.TabIndex = 1;
            this.rbacumulado.TabStop = true;
            this.rbacumulado.Text = "Acumulado";
            this.rbacumulado.UseVisualStyleBackColor = true;
            // 
            // rbmensual
            // 
            this.rbmensual.AutoSize = true;
            this.rbmensual.Location = new System.Drawing.Point(6, 9);
            this.rbmensual.Name = "rbmensual";
            this.rbmensual.Size = new System.Drawing.Size(64, 17);
            this.rbmensual.TabIndex = 0;
            this.rbmensual.TabStop = true;
            this.rbmensual.Text = "Mensual";
            this.rbmensual.UseVisualStyleBackColor = true;
            // 
            // Rbt_xLibro
            // 
            this.Rbt_xLibro.AutoSize = true;
            this.Rbt_xLibro.Location = new System.Drawing.Point(339, 21);
            this.Rbt_xLibro.Name = "Rbt_xLibro";
            this.Rbt_xLibro.Size = new System.Drawing.Size(72, 17);
            this.Rbt_xLibro.TabIndex = 4;
            this.Rbt_xLibro.TabStop = true;
            this.Rbt_xLibro.Text = "Por Libros";
            this.Rbt_xLibro.UseVisualStyleBackColor = true;
            // 
            // Rbt_Centralizado
            // 
            this.Rbt_Centralizado.AutoSize = true;
            this.Rbt_Centralizado.Location = new System.Drawing.Point(255, 21);
            this.Rbt_Centralizado.Name = "Rbt_Centralizado";
            this.Rbt_Centralizado.Size = new System.Drawing.Size(85, 17);
            this.Rbt_Centralizado.TabIndex = 3;
            this.Rbt_Centralizado.TabStop = true;
            this.Rbt_Centralizado.Text = "Centralizado";
            this.Rbt_Centralizado.UseVisualStyleBackColor = true;
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(944, 15);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(75, 23);
            this.btnbuscar.TabIndex = 9;
            this.btnbuscar.Text = "Buscar";
            this.btnbuscar.Click += new System.EventHandler(this.btnbuscar_Click_1);
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(536, 18);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Size = new System.Drawing.Size(196, 20);
            this.txtcuentadesc.TabIndex = 7;
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(455, 18);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Size = new System.Drawing.Size(77, 20);
            this.txtcuenta.TabIndex = 6;
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // txtmesdesc
            // 
            this.txtmesdesc.Enabled = false;
            this.txtmesdesc.EnterMoveNextControl = true;
            this.txtmesdesc.Location = new System.Drawing.Point(77, 20);
            this.txtmesdesc.Name = "txtmesdesc";
            this.txtmesdesc.Size = new System.Drawing.Size(171, 20);
            this.txtmesdesc.TabIndex = 2;
            // 
            // txtmescod
            // 
            this.txtmescod.Location = new System.Drawing.Point(33, 20);
            this.txtmescod.Name = "txtmescod";
            this.txtmescod.Size = new System.Drawing.Size(42, 20);
            this.txtmescod.TabIndex = 1;
            this.txtmescod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(414, 22);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Cuenta";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Mes:";
            // 
            // BandGridReport
            // 
            this.BandGridReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BandGridReport.Location = new System.Drawing.Point(6, 93);
            this.BandGridReport.MainView = this.BandGridVisual;
            this.BandGridReport.Name = "BandGridReport";
            this.BandGridReport.Size = new System.Drawing.Size(1022, 265);
            this.BandGridReport.TabIndex = 246;
            this.BandGridReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.BandGridVisual});
            // 
            // BandGridVisual
            // 
            this.BandGridVisual.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.BandGridVisual.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.BandPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.BandPanel.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.BandPanel.Options.UseFont = true;
            this.BandGridVisual.Appearance.BandPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.BandPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.Empty.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(237)))));
            this.BandGridVisual.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.EvenRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.EvenRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FilterPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FilterPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.BandGridVisual.Appearance.FixedLine.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.FocusedCell.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FocusedCell.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BandGridVisual.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.FocusedRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FocusedRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.FooterPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.FooterPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupButton.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupButton.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.GroupButton.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.GroupFooter.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.GroupFooter.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.GroupPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.GroupRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.GroupRow.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.GroupRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.BandGridVisual.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.BandGridVisual.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.BandGridVisual.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.BandGridVisual.Appearance.HorzLine.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.BandGridVisual.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.OddRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.OddRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(248)))));
            this.BandGridVisual.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.BandGridVisual.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.BandGridVisual.Appearance.Preview.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.Preview.Options.UseFont = true;
            this.BandGridVisual.Appearance.Preview.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.BandGridVisual.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.BandGridVisual.Appearance.Row.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.Row.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.RowSeparator.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(133)))), ((int)(((byte)(179)))));
            this.BandGridVisual.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.BandGridVisual.Appearance.SelectedRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.SelectedRow.Options.UseForeColor = true;
            this.BandGridVisual.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.BandGridVisual.Appearance.TopNewRow.Options.UseBackColor = true;
            this.BandGridVisual.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.BandGridVisual.Appearance.VertLine.Options.UseBackColor = true;
            this.BandGridVisual.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandFechaEmi,
            this.gridBand1,
            this.gridBand6,
            this.BandMonExt,
            this.gridBand3,
            this.gridBand2,
            this.gridBand5,
            this.gridBand7});
            this.BandGridVisual.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Glosa,
            this.FechaDoc,
            this.DocSunat,
            this.SerieDoc,
            this.NumeroDoc,
            this.Descripcion,
            this.Correlativo,
            this.CodLibroSun,
            this.Debe,
            this.Haber,
            this.Voucher,
            this.SaldoDebe,
            this.SaldoHaber,
            this.bandedGridColumn1});
            this.BandGridVisual.GridControl = this.BandGridReport;
            this.BandGridVisual.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "NomEntidad", null, "")});
            this.BandGridVisual.Name = "BandGridVisual";
            this.BandGridVisual.OptionsFilter.AllowColumnMRUFilterList = false;
            this.BandGridVisual.OptionsFilter.AllowMRUFilterList = false;
            this.BandGridVisual.OptionsPrint.AutoWidth = false;
            this.BandGridVisual.OptionsView.ColumnAutoWidth = false;
            this.BandGridVisual.OptionsView.EnableAppearanceEvenRow = true;
            this.BandGridVisual.OptionsView.EnableAppearanceOddRow = true;
            this.BandGridVisual.OptionsView.ShowAutoFilterRow = true;
            this.BandGridVisual.OptionsView.ShowFooter = true;
            this.BandGridVisual.OptionsView.ShowGroupPanel = false;
            // 
            // BandFechaEmi
            // 
            this.BandFechaEmi.AppearanceHeader.Options.UseTextOptions = true;
            this.BandFechaEmi.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandFechaEmi.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BandFechaEmi.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.BandFechaEmi.AutoFillDown = false;
            this.BandFechaEmi.Caption = "DATOS DE LA OPERACIÓN";
            this.BandFechaEmi.Columns.Add(this.Voucher);
            this.BandFechaEmi.Columns.Add(this.FechaDoc);
            this.BandFechaEmi.Columns.Add(this.Correlativo);
            this.BandFechaEmi.Columns.Add(this.Glosa);
            this.BandFechaEmi.Columns.Add(this.bandedGridColumn1);
            this.BandFechaEmi.Name = "BandFechaEmi";
            this.BandFechaEmi.VisibleIndex = 0;
            this.BandFechaEmi.Width = 671;
            // 
            // Voucher
            // 
            this.Voucher.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Voucher.AppearanceHeader.Options.UseForeColor = true;
            this.Voucher.Caption = "VOUCHER";
            this.Voucher.FieldName = "Voucher";
            this.Voucher.Name = "Voucher";
            this.Voucher.OptionsColumn.AllowEdit = false;
            this.Voucher.OptionsColumn.AllowMove = false;
            this.Voucher.OptionsColumn.FixedWidth = true;
            this.Voucher.Visible = true;
            this.Voucher.Width = 94;
            // 
            // FechaDoc
            // 
            this.FechaDoc.AppearanceCell.Options.UseTextOptions = true;
            this.FechaDoc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FechaDoc.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.FechaDoc.AppearanceHeader.Options.UseForeColor = true;
            this.FechaDoc.AppearanceHeader.Options.UseTextOptions = true;
            this.FechaDoc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FechaDoc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FechaDoc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FechaDoc.Caption = "FECHA DE EMISION";
            this.FechaDoc.DisplayFormat.FormatString = "d";
            this.FechaDoc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FechaDoc.FieldName = "FechaOperacion";
            this.FechaDoc.Name = "FechaDoc";
            this.FechaDoc.OptionsColumn.AllowEdit = false;
            this.FechaDoc.OptionsColumn.AllowMove = false;
            this.FechaDoc.OptionsColumn.AllowSize = false;
            this.FechaDoc.OptionsColumn.FixedWidth = true;
            this.FechaDoc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Date;
            this.FechaDoc.OptionsFilter.ShowEmptyDateFilter = true;
            this.FechaDoc.RowCount = 2;
            this.FechaDoc.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.FechaDoc.Visible = true;
            this.FechaDoc.Width = 95;
            // 
            // Correlativo
            // 
            this.Correlativo.AppearanceCell.Options.UseTextOptions = true;
            this.Correlativo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Correlativo.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Correlativo.AppearanceHeader.Options.UseForeColor = true;
            this.Correlativo.Caption = "CORRELAT.";
            this.Correlativo.FieldName = "Correlativo";
            this.Correlativo.Name = "Correlativo";
            this.Correlativo.OptionsColumn.AllowEdit = false;
            this.Correlativo.Visible = true;
            this.Correlativo.Width = 107;
            // 
            // Glosa
            // 
            this.Glosa.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Glosa.AppearanceHeader.Options.UseForeColor = true;
            this.Glosa.AppearanceHeader.Options.UseTextOptions = true;
            this.Glosa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Glosa.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Glosa.Caption = "DESCRIPCION DE LA OPERACIÓN";
            this.Glosa.FieldName = "Glosa";
            this.Glosa.Name = "Glosa";
            this.Glosa.OptionsColumn.AllowEdit = false;
            this.Glosa.OptionsColumn.AllowMove = false;
            this.Glosa.OptionsColumn.AllowSize = false;
            this.Glosa.OptionsColumn.FixedWidth = true;
            this.Glosa.RowCount = 2;
            this.Glosa.Visible = true;
            this.Glosa.Width = 300;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn1.Caption = "CUENTA";
            this.bandedGridColumn1.FieldName = "Cuenta";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "REFERENCIA DE LA OPERACIÓN";
            this.gridBand1.Columns.Add(this.CodLibroSun);
            this.gridBand1.Columns.Add(this.DocSunat);
            this.gridBand1.Columns.Add(this.SerieDoc);
            this.gridBand1.Columns.Add(this.NumeroDoc);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Visible = false;
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 71;
            // 
            // CodLibroSun
            // 
            this.CodLibroSun.AppearanceCell.Options.UseTextOptions = true;
            this.CodLibroSun.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CodLibroSun.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.CodLibroSun.AppearanceHeader.Options.UseForeColor = true;
            this.CodLibroSun.Caption = "LIBRO O REGISTRO";
            this.CodLibroSun.FieldName = "Lib_Sunat";
            this.CodLibroSun.Name = "CodLibroSun";
            this.CodLibroSun.OptionsColumn.AllowEdit = false;
            this.CodLibroSun.Width = 103;
            // 
            // DocSunat
            // 
            this.DocSunat.AppearanceCell.Options.UseTextOptions = true;
            this.DocSunat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DocSunat.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.DocSunat.AppearanceHeader.Options.UseForeColor = true;
            this.DocSunat.Caption = "DOC.";
            this.DocSunat.FieldName = "DocSunat";
            this.DocSunat.Name = "DocSunat";
            this.DocSunat.OptionsColumn.AllowEdit = false;
            this.DocSunat.Width = 37;
            // 
            // SerieDoc
            // 
            this.SerieDoc.AppearanceCell.Options.UseTextOptions = true;
            this.SerieDoc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SerieDoc.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SerieDoc.AppearanceHeader.Options.UseForeColor = true;
            this.SerieDoc.Caption = "SERIE";
            this.SerieDoc.FieldName = "SerieDoc";
            this.SerieDoc.Name = "SerieDoc";
            this.SerieDoc.OptionsColumn.AllowEdit = false;
            this.SerieDoc.Width = 50;
            // 
            // NumeroDoc
            // 
            this.NumeroDoc.AppearanceCell.Options.UseTextOptions = true;
            this.NumeroDoc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NumeroDoc.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.NumeroDoc.AppearanceHeader.Options.UseForeColor = true;
            this.NumeroDoc.Caption = "NUMERO";
            this.NumeroDoc.FieldName = "NumeroDoc";
            this.NumeroDoc.Name = "NumeroDoc";
            this.NumeroDoc.OptionsColumn.AllowEdit = false;
            this.NumeroDoc.Width = 71;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "CUENTA CONTABLE ASOCIADA";
            this.gridBand6.Columns.Add(this.Descripcion);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Visible = false;
            this.gridBand6.VisibleIndex = -1;
            this.gridBand6.Width = 361;
            // 
            // Descripcion
            // 
            this.Descripcion.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Descripcion.AppearanceHeader.Options.UseForeColor = true;
            this.Descripcion.Caption = "DENOMINACIÓN";
            this.Descripcion.FieldName = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.OptionsColumn.AllowEdit = false;
            this.Descripcion.Width = 361;
            // 
            // BandMonExt
            // 
            this.BandMonExt.AppearanceHeader.Options.UseTextOptions = true;
            this.BandMonExt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandMonExt.AutoFillDown = false;
            this.BandMonExt.Caption = "CUENTA CONTABLE ASOCIADA";
            this.BandMonExt.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4});
            this.BandMonExt.Name = "BandMonExt";
            this.BandMonExt.Visible = false;
            this.BandMonExt.VisibleIndex = -1;
            this.BandMonExt.Width = 289;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.AutoFillDown = false;
            this.gridBand4.Caption = "INFORMACION DE DEUDORES";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.BandDocId,
            this.BandApelliyNom});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Visible = false;
            this.gridBand4.VisibleIndex = -1;
            this.gridBand4.Width = 466;
            // 
            // BandDocId
            // 
            this.BandDocId.AppearanceHeader.Options.UseTextOptions = true;
            this.BandDocId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandDocId.AutoFillDown = false;
            this.BandDocId.Caption = "DOC. IDENTIDAD";
            this.BandDocId.Name = "BandDocId";
            this.BandDocId.VisibleIndex = 0;
            this.BandDocId.Width = 160;
            // 
            // BandApelliyNom
            // 
            this.BandApelliyNom.AppearanceHeader.Options.UseTextOptions = true;
            this.BandApelliyNom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BandApelliyNom.AutoFillDown = false;
            this.BandApelliyNom.Caption = "DATOS DE ENTIDAD";
            this.BandApelliyNom.Name = "BandApelliyNom";
            this.BandApelliyNom.VisibleIndex = 1;
            this.BandApelliyNom.Width = 306;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Visible = false;
            this.gridBand3.VisibleIndex = -1;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Visible = false;
            this.gridBand2.VisibleIndex = -1;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "MOVIMIENTOS";
            this.gridBand5.Columns.Add(this.Debe);
            this.gridBand5.Columns.Add(this.Haber);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 1;
            this.gridBand5.Width = 230;
            // 
            // Debe
            // 
            this.Debe.AppearanceCell.Options.UseTextOptions = true;
            this.Debe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Debe.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Debe.AppearanceHeader.Options.UseForeColor = true;
            this.Debe.AppearanceHeader.Options.UseTextOptions = true;
            this.Debe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Debe.Caption = "DEBE";
            this.Debe.DisplayFormat.FormatString = "n2";
            this.Debe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Debe.FieldName = "DebeNacional";
            this.Debe.Name = "Debe";
            this.Debe.OptionsColumn.AllowEdit = false;
            this.Debe.OptionsColumn.AllowMove = false;
            this.Debe.OptionsColumn.FixedWidth = true;
            this.Debe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DebeNacional", "{0:n2}")});
            this.Debe.Visible = true;
            this.Debe.Width = 115;
            // 
            // Haber
            // 
            this.Haber.AppearanceCell.Options.UseTextOptions = true;
            this.Haber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Haber.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.Haber.AppearanceHeader.Options.UseForeColor = true;
            this.Haber.AppearanceHeader.Options.UseTextOptions = true;
            this.Haber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Haber.Caption = "HABER";
            this.Haber.DisplayFormat.FormatString = "n2";
            this.Haber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Haber.FieldName = "HaberNacional";
            this.Haber.Name = "Haber";
            this.Haber.OptionsColumn.AllowEdit = false;
            this.Haber.OptionsColumn.AllowMove = false;
            this.Haber.OptionsColumn.FixedWidth = true;
            this.Haber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "HaberNacional", "{0:n2}")});
            this.Haber.Visible = true;
            this.Haber.Width = 115;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "SALDOS";
            this.gridBand7.Columns.Add(this.SaldoDebe);
            this.gridBand7.Columns.Add(this.SaldoHaber);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 2;
            this.gridBand7.Width = 230;
            // 
            // SaldoDebe
            // 
            this.SaldoDebe.AppearanceCell.Options.UseTextOptions = true;
            this.SaldoDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SaldoDebe.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SaldoDebe.AppearanceHeader.Options.UseForeColor = true;
            this.SaldoDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.SaldoDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SaldoDebe.Caption = "DEUDOR";
            this.SaldoDebe.DisplayFormat.FormatString = "N2";
            this.SaldoDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SaldoDebe.FieldName = "XSaldoDebe";
            this.SaldoDebe.Name = "SaldoDebe";
            this.SaldoDebe.OptionsColumn.AllowEdit = false;
            this.SaldoDebe.OptionsColumn.AllowMove = false;
            this.SaldoDebe.OptionsColumn.FixedWidth = true;
            this.SaldoDebe.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.SaldoDebe.Visible = true;
            this.SaldoDebe.Width = 115;
            // 
            // SaldoHaber
            // 
            this.SaldoHaber.AppearanceCell.Options.UseTextOptions = true;
            this.SaldoHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SaldoHaber.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SaldoHaber.AppearanceHeader.Options.UseForeColor = true;
            this.SaldoHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.SaldoHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SaldoHaber.Caption = "ACREEDOR";
            this.SaldoHaber.DisplayFormat.FormatString = "N2";
            this.SaldoHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SaldoHaber.FieldName = "XSaldoHaber";
            this.SaldoHaber.Name = "SaldoHaber";
            this.SaldoHaber.OptionsColumn.AllowEdit = false;
            this.SaldoHaber.OptionsColumn.AllowMove = false;
            this.SaldoHaber.OptionsColumn.FixedWidth = true;
            this.SaldoHaber.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.SaldoHaber.Visible = true;
            this.SaldoHaber.Width = 115;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnlimpiar,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2,
            this.btnimpresion,
            this.barButtonItem1,
            this.btnple});
            this.barManager1.MaxItemId = 7;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnimpresion),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnple)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 0;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // btnimpresion
            // 
            this.btnimpresion.Caption = "Impresion";
            this.btnimpresion.Id = 4;
            this.btnimpresion.ImageOptions.Image = global::Contable.Properties.Resources.Print_24px;
            this.btnimpresion.Name = "btnimpresion";
            this.btnimpresion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnimpresion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnimpresion_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Exportar";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnple
            // 
            this.btnple.Caption = "Generar PLE txt";
            this.btnple.Id = 6;
            this.btnple.ImageOptions.Image = global::Contable.Properties.Resources.TXT_24px;
            this.btnple.Name = "btnple";
            this.btnple.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnple.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnple_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1035, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 361);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1035, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1035, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 329);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_libro_mayor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1035, 361);
            this.Controls.Add(this.BandGridReport);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl5);
            this.Controls.Add(this.barDockControl6);
            this.Controls.Add(this.barDockControl7);
            this.Controls.Add(this.barDockControl8);
            this.Controls.Add(this.barDockControl9);
            this.Controls.Add(this.barDockControl10);
            this.Controls.Add(this.barDockControl11);
            this.Controls.Add(this.barDockControl12);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_libro_mayor";
            this.Text = "Libro mayor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandGridVisual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarDockControl barDockControl6;
        private DevExpress.XtraBars.BarDockControl barDockControl7;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraBars.BarDockControl barDockControl9;
        private DevExpress.XtraBars.BarDockControl barDockControl10;
        private DevExpress.XtraBars.BarDockControl barDockControl11;
        private DevExpress.XtraBars.BarDockControl barDockControl12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Rbt_xLibro;
        private System.Windows.Forms.RadioButton Rbt_Centralizado;
        private DevExpress.XtraEditors.SimpleButton btnbuscar;
        private DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.TextEdit txtcuenta;
        private DevExpress.XtraEditors.TextEdit txtmesdesc;
        private DevExpress.XtraEditors.TextEdit txtmescod;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbacumulado;
        private System.Windows.Forms.RadioButton rbmensual;
        internal DevExpress.XtraGrid.GridControl BandGridReport;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridView BandGridVisual;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandFechaEmi;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Voucher;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FechaDoc;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Correlativo;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Glosa;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CodLibroSun;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DocSunat;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SerieDoc;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NumeroDoc;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Descripcion;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandMonExt;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandDocId;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand BandApelliyNom;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Debe;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Haber;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SaldoDebe;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SaldoHaber;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarButtonItem btnimpresion;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnple;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}
