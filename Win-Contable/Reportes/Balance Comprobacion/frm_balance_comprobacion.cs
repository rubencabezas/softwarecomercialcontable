﻿using DevExpress.DataAccess.Native.ExpressionEditor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Balance_Comprobacion
{
    public partial class frm_balance_comprobacion : Contable.frm_fuente
    {
        public frm_balance_comprobacion()
        {
            InitializeComponent();
        }

        private void txtnivel_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtnivel.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtnivel.Text.Substring(txtnivel.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_estructura_plan_busqueda f = new _1_Busquedas_Generales.frm_estructura_plan_busqueda())
                        {
                            //f.NumDigitos = txtnivel.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Estructura Entidad = new Entidad_Estructura();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtnivel.Text = Entidad.Est_Num_Digitos;
                                txtniveldesc.Text = Entidad.Est_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtnivel.Text) & string.IsNullOrEmpty(txtniveldesc.Text))
                    {
                        BuscarNivel();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarNivel()
        {
            try
            {
                txtnivel.Text = Accion.Formato(txtnivel.Text,1);

                Logica_Estructura log = new Logica_Estructura();

                List<Entidad_Estructura> Generales = new List<Entidad_Estructura>();

                Generales = log.Listar(new Entidad_Estructura
                {
                    Est_Num_Digitos = txtnivel.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Estructura T in Generales)
                    {
                        if ((T.Est_Num_Digitos).ToString().Trim().ToUpper() == txtnivel.Text.Trim().ToUpper())
                        {
                            txtnivel.Text = (T.Est_Num_Digitos).ToString().Trim();
                            txtniveldesc.Text = T.Est_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro los datos");
                    txtnivel.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmonedacod.Text = Entidad.Id_Moneda;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 3);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Moneda = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Moneda).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            txtmonedacod.Text = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro la moneda");
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)

        {

            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa =Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa =Actual_Conexion.CodigoEmpresa,
                    Id_Anio=Actual_Conexion.AnioSelect,
                    Id_Periodo=txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                    txtmescod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public bool Verificar()
        {
            if (string.IsNullOrEmpty(txtniveldesc.Text))
            {
                Accion.Advertencia("Ingresar el Nivel de Estructura de la Cuenta Contable");
                txtnivel.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtmesdesc.Text))
            {
                Accion.Advertencia("Ingresar el Periodo de Análisis");
                txtmescod.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtmonedadesc.Text))
            {
                Accion.Advertencia("Ingresar la Moneda");
                txtmonedacod.Focus();
                return false;
            }
            return true;
        }


        List<Entidad_Balance_Comprobacion_Reporte> ListCta = new List<Entidad_Balance_Comprobacion_Reporte>();
        List<Entidad_Balance_Comprobacion_Reporte> ListCtaDetalle = new List<Entidad_Balance_Comprobacion_Reporte>();
        List<Entidad_Balance_Comprobacion_Reporte> ListCtaGeneral = new List<Entidad_Balance_Comprobacion_Reporte>();
        List<Entidad_Balance_Comprobacion_Reporte> ListCtaGeneral2 = new List<Entidad_Balance_Comprobacion_Reporte>();

        decimal TotalDebe, TotalHaber, SaldoDebe, SaldoHaber, InvDebe, InvHaber, FunDebe, FunHaber, NatDebe, NatHaber;

        private void txtmescod_TextChanged(object sender, EventArgs e)
        {
            if (txtmescod.Focus()==false)
            {
                txtmesdesc.ResetText();
            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacod.Focus()==false)
            {
                txtmonedadesc.Focus();
            }
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Limpiar();
        }
        public void Limpiar()
        {
            txtmescod.ResetText();
            txtmesdesc.ResetText();
            txtniveldesc.ResetText();
            txtnivel.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            ListCta.Clear();
            ListCtaDetalle.Clear();
            ListCtaGeneral.Clear ();
            ListCtaGeneral2.Clear();
            dgvdatos.DataSource = null;
            RbtMensual.Checked = false;
            RbtAcumulado.Checked = true;

        }

        private void txtnivel_TextChanged(object sender, EventArgs e)
        {
            if (txtnivel.Focus()==false)
            {
                txtniveldesc.ResetText();
            }
        }

        private void frm_balance_comprobacion_Load(object sender, EventArgs e)
        {
            RbtAcumulado.Checked = true;
        }

        private void bandedGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if ( ListCta.Count > 0)
                {
                    Entidad_Balance_Comprobacion_Reporte reporte = new Entidad_Balance_Comprobacion_Reporte();
                    reporte =  ListCta[ bandedGridView1.GetFocusedDataSourceRowIndex()];
                    using (frm_ver_detalle_balance _balance = new frm_ver_detalle_balance())
                    {
                        _balance.Empresa = Actual_Conexion.CodigoEmpresa;
                        _balance.Anio = Actual_Conexion.AnioSelect;
                        _balance.Periodo =  txtmescod.Text;
                        _balance.CtaContable = reporte.Cuenta;
                        _balance.Moneda = reporte.Id_Moneda;
                        _balance.lblcta.Caption = reporte.Cuenta;
                        _balance.lblcuentadescripcion.Caption = reporte.Descripcion;
                        _balance.Es_Acumulado =  RbtAcumulado.Checked;
                        if (_balance.ShowDialog() == DialogResult.OK)
                        {
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                Accion.ErrorSistema(exception1.Message);
            }

        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Balance" + ".Xlsx");

                bandedGridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex )
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void RbtAcumulado_CheckedChanged(object sender, EventArgs e)
        {
            

            if (RbtMensual.Checked == true)
            {
                RbtMensual.Checked = false;
            }
            else
            {
                RbtAcumulado.Checked = true;
            }
        }

        private void RbtMensual_CheckedChanged(object sender, EventArgs e)
        {
            if (RbtAcumulado.Checked == true)
            {
                RbtAcumulado.Checked = false;
            }
            else
            {
                RbtMensual.Checked = true;
            }
        }

        private void btnver_Click(object sender, EventArgs e)
        {
            try
            {

                if (Verificar())
                {
                    TotalDebe = 0;
                    TotalHaber = 0;
                    SaldoDebe = 0;
                    SaldoHaber = 0;
                    InvDebe = 0;
                    InvHaber = 0;
                    FunDebe = 0;
                    FunHaber = 0;
                    NatDebe = 0;
                    NatHaber = 0;
                    dgvdatos.DataSource = null;
                    if (ListCtaGeneral2.Count > 0)
                    {
                        ListCta.Clear();
                        ListCtaDetalle.Clear();
                        ListCtaGeneral.Clear();
                        ListCtaGeneral2.Clear();
                    }


                    Logica_Balance_Comprobacion_Reporte Log = new Logica_Balance_Comprobacion_Reporte();
                    Entidad_Balance_Comprobacion_Reporte Ent = new Entidad_Balance_Comprobacion_Reporte();
                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = txtmescod.Text.Trim();

                    Ent.Es_Num_Digitos = Convert.ToInt32(txtnivel.Text).ToString();
                    Ent.Id_Moneda = txtmonedacod.Text.ToString().Trim();
                    if (RbtAcumulado.Checked == true)
                    {
                        ListCta = Log.Balance_Acumulado(Ent);
                    }
                    else if (RbtMensual.Checked == true)
                    {
                        ListCta = Log.Balance_Mensual(Ent);
                    }


                    if (ListCta.Count > 0)
                    {

                        foreach (Entidad_Balance_Comprobacion_Reporte itencta in ListCta)
                        {

                            itencta.Es_Num_Digitos = Convert.ToInt32(txtnivel.Text).ToString();

                            if (itencta.Debe > itencta.Haber)
                            {
                                itencta.SaldoDebe = itencta.Debe - itencta.Haber;
                                itencta.SaldoHaber = 0;
                            }
                            else if (itencta.Haber > itencta.Debe)
                            {
                                itencta.SaldoHaber = itencta.Haber - itencta.Debe;
                                itencta.SaldoDebe = 0;
                            }
                            else if (itencta.Debe == itencta.Haber)
                            {
                                itencta.SaldoDebe = 0;
                                itencta.SaldoHaber = 0;
                            }

                            if (itencta.IsInventario == true)
                            {
                                itencta.InvenDebe = itencta.SaldoDebe;
                                itencta.InvenHaber = itencta.SaldoHaber;
                            }
                            if (itencta.IsFuncion == true)
                            {
                                itencta.FuncionDebe = itencta.SaldoDebe;
                                itencta.FuncionHaber = itencta.SaldoHaber;
                            }
                            if (itencta.IsNaturaleza == true)
                            {
                                itencta.NatuDebe = itencta.SaldoDebe;
                                itencta.NatuHaber = itencta.SaldoHaber;
                            }
                            TotalDebe = TotalDebe + itencta.Debe;
                            TotalHaber = TotalHaber + itencta.Haber;
                            SaldoDebe = SaldoDebe + itencta.SaldoDebe;
                            SaldoHaber = SaldoHaber + itencta.SaldoHaber;
                            InvDebe = InvDebe + itencta.InvenDebe;
                            InvHaber = InvHaber + itencta.InvenHaber;
                            FunDebe = FunDebe + itencta.FuncionDebe;
                            FunHaber = FunHaber + itencta.FuncionHaber;
                            NatDebe = NatDebe + itencta.NatuDebe;
                            NatHaber = NatHaber + itencta.NatuHaber;
                        }
                        dgvdatos.DataSource = ListCta;

                    }

                    Entidad_Plan_General EntCtaGen = new Entidad_Plan_General();
                    Logica_Plan_General LogCtaGen = new Logica_Plan_General();
                    List<Entidad_Plan_General> ListaCtaGen = new List<Entidad_Plan_General>();

                    ListaCtaGen = LogCtaGen.Listar(EntCtaGen);

                    List<string> ListTemp = new List<string>();
                    ListCtaGeneral.Clear();

                    foreach (Entidad_Plan_General itemgen in ListaCtaGen)
                    {

                        ListCtaGeneral.Add(new Entidad_Balance_Comprobacion_Reporte
                        {
                            Cuenta = itemgen.Id_Cuenta,
                            Descripcion = itemgen.Cta_Descripcion,
                            Es_Num_Digitos = itemgen.Est_Num_Digitos
                        });
                    }


                    int NivelMaxEst = 0;
                    Entidad_Plan_General En_NivelMaxEst = new Entidad_Plan_General();
                    Logica_Plan_General logEstr = new Logica_Plan_General();
                    En_NivelMaxEst = logEstr.TraerDigitoMayor();
                    NivelMaxEst = Convert.ToInt32(En_NivelMaxEst.Est_Num_Digitos);

                    //Agrego a la Lista todos los detalles de mi consulta 
                    if (Convert.ToInt32(txtnivel.Text) != 8)
                    {
                        for (int co = Convert.ToInt32(txtnivel.Text); co <= NivelMaxEst; co++)
                        {
                            foreach (Entidad_Balance_Comprobacion_Reporte CtaDetalle in ListCtaGeneral)
                            {

                                for (int k = Convert.ToInt32(CtaDetalle.Es_Num_Digitos); k <= NivelMaxEst - 1; k++)
                                {
                                    CtaDetalle.Cuenta = CtaDetalle.Cuenta + " ";
                                }
                                if (ExisteCuentanGeneral(CtaDetalle, co) == true)
                                {
                                    if (ExisteCuenta(CtaDetalle, co) == false)
                                    {
                                        Entidad_Balance_Comprobacion_Reporte NewEnt = new Entidad_Balance_Comprobacion_Reporte();
                                        Entidad_Balance_Comprobacion_Reporte newEntCta = new Entidad_Balance_Comprobacion_Reporte();

                                        NewEnt.Descripcion = TraerDescripcion(CtaDetalle);
                                        NewEnt.Es_Num_Digitos = Convert.ToInt32(TraerEstructura(CtaDetalle)).ToString();

                                        NewEnt.Cuenta = CtaDetalle.Cuenta;
                                        ListCtaGeneral2.Add(NewEnt);
                                    }
                                }
                            }
                        }
                    }


                    for (int i = 0; i <= 2; i++)
                    {
                    }

                    DataTable tabAgre = new DataTable();
                    tabAgre.Columns.Add("InvenDebe",typeof(double));
                    tabAgre.Columns.Add("InvenHaber", typeof(double));
                    tabAgre.Columns.Add("FuncionDebe", typeof(double));
                    tabAgre.Columns.Add("FuncionHaber", typeof(double));
                    tabAgre.Columns.Add("NatuDebe",typeof(double));
                    tabAgre.Columns.Add("NatuHaber", typeof(double));


                    // ORIGINAL
                    //filAgre["InvenDebe"] = IIf(Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue) > 0, 0, Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue) * -1);
                    //filAgre["InvenHaber"] = IIf(Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue) > 0, Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue), 0);
                    //filAgre["FuncionDebe"] = IIf(Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue) > 0, 0, Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue) * -1);
                    //filAgre["FuncionHaber"] = IIf(Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue) > 0, Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue), 0);
                    //filAgre["NatuDebe"] = IIf(Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue) > 0, 0, Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue) * -1);
                    //filAgre["NatuHaber"] = IIf(Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue) > 0, Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue), 0);




                    DataRow filAgre = tabAgre.NewRow();
      
                    filAgre["InvenDebe"] = Math.Abs(Convert.ToDecimal(IIf((Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue)) > 0, 0, Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue))));
                    filAgre["InvenHaber"] = Math.Abs(Convert.ToDecimal(IIf((Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue)) > 0, Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue), 0)));
                    filAgre["FuncionDebe"] = Math.Abs(Convert.ToDecimal(IIf((Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue)) > 0, 0, Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue))));
                    filAgre["FuncionHaber"] = Math.Abs(Convert.ToDecimal(IIf((Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue)) > 0, Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue), 0)));
                    filAgre["NatuDebe"] = Math.Abs(Convert.ToDecimal(IIf((Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue)) > 0, 0, Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue))));
                    filAgre["NatuHaber"] = Math.Abs(Convert.ToDecimal(IIf((Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue)) > 0, Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) - Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue), 0)));


                    tabAgre.Rows.Add(filAgre);

                    DataRow filAgre2 = tabAgre.NewRow();
                    
        
                    filAgre2["InvenDebe"] = Convert.ToDecimal(bandedGridView1.Columns["InvenDebe"].SummaryItem.SummaryValue) + Convert.ToDecimal(filAgre["InvenDebe"]);
                    filAgre2["InvenHaber"] = Convert.ToDecimal(bandedGridView1.Columns["InvenHaber"].SummaryItem.SummaryValue) + Convert.ToDecimal(filAgre["InvenHaber"]);
                    filAgre2["FuncionDebe"] =  Convert.ToDecimal(bandedGridView1.Columns["FuncionDebe"].SummaryItem.SummaryValue) + Convert.ToDecimal(filAgre["FuncionDebe"]);
                    filAgre2["FuncionHaber"] = Convert.ToDecimal(bandedGridView1.Columns["FuncionHaber"].SummaryItem.SummaryValue) + Convert.ToDecimal(filAgre["FuncionHaber"]);
                    filAgre2["NatuDebe"] = Convert.ToDecimal(bandedGridView1.Columns["NatuDebe"].SummaryItem.SummaryValue) + Convert.ToDecimal(filAgre["NatuDebe"]);
                    filAgre2["NatuHaber"] = Convert.ToDecimal(bandedGridView1.Columns["NatuHaber"].SummaryItem.SummaryValue) + Convert.ToDecimal(filAgre["NatuHaber"]);

                    tabAgre.Rows.Add(filAgre2);

                    GridControl1.DataSource = tabAgre;

          
                }
                else
                {
                    Accion.Advertencia("");
                }


            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

          



        }


        Object IIf(bool expression, object truePart, object falsePart)
        {
            return expression ? truePart : falsePart;
        }

        public bool ExisteCuenta(Entidad_Balance_Comprobacion_Reporte op, int Nivel)
        {
            foreach (Entidad_Balance_Comprobacion_Reporte Grup in ListCtaGeneral2)
            {
                string var;
                var = op.Cuenta.Substring(0, Nivel);
                if (var == Grup.Cuenta.Trim())
                {
                    return true;
                }
            }
            return false;
        }


        public bool ExisteCuentanGeneral(Entidad_Balance_Comprobacion_Reporte op, int Nivel)
        {
            foreach (Entidad_Balance_Comprobacion_Reporte Grup in ListCtaGeneral)
            {

                string var;
                var = op.Cuenta.Substring(0, Nivel);
                if (var== Grup.Cuenta.Trim())
                {
                    return true;
                }
            }
            return false;
        }
        public int TraerEstructura(Entidad_Balance_Comprobacion_Reporte op)
        {
            int NumDigit = 0;
            foreach (Entidad_Balance_Comprobacion_Reporte c in ListCtaGeneral)
            {
                
                if (op.Cuenta == c.Cuenta)
                {
                    NumDigit = Convert.ToInt32(c.Es_Num_Digitos);
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return NumDigit;
        }
        public string TraerDescripcion(Entidad_Balance_Comprobacion_Reporte op)
        {
            string Descrip = "";
            foreach (Entidad_Balance_Comprobacion_Reporte c in ListCtaGeneral)
            {
               if (op.Cuenta == c.Cuenta)
                {
                    Descrip = c.Descripcion;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return Descrip;
        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                 Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
