﻿namespace Contable.Reportes.Balance_Comprobacion
{
    partial class frm_balance_comprobacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.NatuDebe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RbtMensual = new DevExpress.XtraEditors.CheckEdit();
            this.RbtAcumulado = new DevExpress.XtraEditors.CheckEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnver = new DevExpress.XtraEditors.SimpleButton();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtmesdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmescod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtniveldesc = new DevExpress.XtraEditors.TextEdit();
            this.txtnivel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NatuHaber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvenDebe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvenHaber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuncionDebe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuncionHaber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnlimpiar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbtMensual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbtAcumulado.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtniveldesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnivel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // NatuDebe
            // 
            this.NatuDebe.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.NatuDebe.AppearanceCell.Options.UseFont = true;
            this.NatuDebe.AppearanceCell.Options.UseTextOptions = true;
            this.NatuDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NatuDebe.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NatuDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.NatuDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NatuDebe.Caption = "PÉRDIDAS N";
            this.NatuDebe.DisplayFormat.FormatString = "n2";
            this.NatuDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NatuDebe.FieldName = "NatuDebe";
            this.NatuDebe.Name = "NatuDebe";
            this.NatuDebe.OptionsColumn.AllowEdit = false;
            this.NatuDebe.OptionsColumn.AllowMove = false;
            this.NatuDebe.OptionsColumn.AllowSize = false;
            this.NatuDebe.OptionsColumn.FixedWidth = true;
            this.NatuDebe.OptionsFilter.AllowAutoFilter = false;
            this.NatuDebe.OptionsFilter.AllowFilter = false;
            this.NatuDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuDebe", "{0:n2}")});
            this.NatuDebe.Visible = true;
            this.NatuDebe.VisibleIndex = 4;
            this.NatuDebe.Width = 104;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RbtMensual);
            this.groupBox1.Controls.Add(this.RbtAcumulado);
            this.groupBox1.Location = new System.Drawing.Point(3, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(154, 41);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo";
            // 
            // RbtMensual
            // 
            this.RbtMensual.EnterMoveNextControl = true;
            this.RbtMensual.Location = new System.Drawing.Point(85, 18);
            this.RbtMensual.Name = "RbtMensual";
            this.RbtMensual.Properties.Caption = "Mensual";
            this.RbtMensual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.RbtMensual.Size = new System.Drawing.Size(66, 20);
            this.RbtMensual.TabIndex = 1;
            this.RbtMensual.CheckedChanged += new System.EventHandler(this.RbtMensual_CheckedChanged);
            // 
            // RbtAcumulado
            // 
            this.RbtAcumulado.EnterMoveNextControl = true;
            this.RbtAcumulado.Location = new System.Drawing.Point(4, 18);
            this.RbtAcumulado.Name = "RbtAcumulado";
            this.RbtAcumulado.Properties.Caption = "Acumulado";
            this.RbtAcumulado.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.RbtAcumulado.Size = new System.Drawing.Size(75, 20);
            this.RbtAcumulado.TabIndex = 0;
            this.RbtAcumulado.CheckedChanged += new System.EventHandler(this.RbtAcumulado_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnver);
            this.groupBox2.Controls.Add(this.txtmonedadesc);
            this.groupBox2.Controls.Add(this.txtmonedacod);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Controls.Add(this.txtmesdesc);
            this.groupBox2.Controls.Add(this.txtmescod);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.txtniveldesc);
            this.groupBox2.Controls.Add(this.txtnivel);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Location = new System.Drawing.Point(160, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(836, 41);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // btnver
            // 
            this.btnver.Location = new System.Drawing.Point(755, 11);
            this.btnver.Name = "btnver";
            this.btnver.Size = new System.Drawing.Size(75, 23);
            this.btnver.TabIndex = 9;
            this.btnver.Text = "Ver";
            this.btnver.Click += new System.EventHandler(this.btnver_Click);
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(586, 13);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(147, 20);
            this.txtmonedadesc.TabIndex = 8;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(543, 14);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Size = new System.Drawing.Size(40, 20);
            this.txtmonedacod.TabIndex = 7;
            this.txtmonedacod.TextChanged += new System.EventHandler(this.txtmonedacod_TextChanged);
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(495, 18);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(42, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Moneda:";
            // 
            // txtmesdesc
            // 
            this.txtmesdesc.Enabled = false;
            this.txtmesdesc.EnterMoveNextControl = true;
            this.txtmesdesc.Location = new System.Drawing.Point(348, 15);
            this.txtmesdesc.Name = "txtmesdesc";
            this.txtmesdesc.Size = new System.Drawing.Size(141, 20);
            this.txtmesdesc.TabIndex = 5;
            // 
            // txtmescod
            // 
            this.txtmescod.EnterMoveNextControl = true;
            this.txtmescod.Location = new System.Drawing.Point(307, 15);
            this.txtmescod.Name = "txtmescod";
            this.txtmescod.Size = new System.Drawing.Size(38, 20);
            this.txtmescod.TabIndex = 4;
            this.txtmescod.TextChanged += new System.EventHandler(this.txtmescod_TextChanged);
            this.txtmescod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmescod_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(278, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Mes:";
            // 
            // txtniveldesc
            // 
            this.txtniveldesc.Enabled = false;
            this.txtniveldesc.EnterMoveNextControl = true;
            this.txtniveldesc.Location = new System.Drawing.Point(88, 15);
            this.txtniveldesc.Name = "txtniveldesc";
            this.txtniveldesc.Size = new System.Drawing.Size(184, 20);
            this.txtniveldesc.TabIndex = 2;
            // 
            // txtnivel
            // 
            this.txtnivel.EnterMoveNextControl = true;
            this.txtnivel.Location = new System.Drawing.Point(47, 15);
            this.txtnivel.Name = "txtnivel";
            this.txtnivel.Size = new System.Drawing.Size(39, 20);
            this.txtnivel.TabIndex = 1;
            this.txtnivel.TextChanged += new System.EventHandler(this.txtnivel_TextChanged);
            this.txtnivel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnivel_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(14, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nivel:";
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatos.Location = new System.Drawing.Point(3, 85);
            this.dgvdatos.MainView = this.bandedGridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(993, 366);
            this.dgvdatos.TabIndex = 6;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.BandPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bandedGridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12});
            this.bandedGridView1.GridControl = this.dgvdatos;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.DoubleClick += new System.EventHandler(this.bandedGridView1_DoubleClick);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "CUENTA";
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 295;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn1.Caption = "CODIGO";
            this.bandedGridColumn1.FieldName = "Cuenta";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 86;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn2.Caption = "DENOMINACION";
            this.bandedGridColumn2.FieldName = "Descripcion";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 209;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "SUMAS DEL MAYOR";
            this.gridBand2.Columns.Add(this.bandedGridColumn3);
            this.gridBand2.Columns.Add(this.bandedGridColumn4);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 222;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn3.Caption = "DEBE";
            this.bandedGridColumn3.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn3.FieldName = "Debe";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Debe", "{0:n2}")});
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 129;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn4.Caption = "HABER";
            this.bandedGridColumn4.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn4.FieldName = "Haber";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Haber", "{0:n2}")});
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 93;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "SALDOS";
            this.gridBand3.Columns.Add(this.bandedGridColumn5);
            this.gridBand3.Columns.Add(this.bandedGridColumn6);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 169;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn5.Caption = "DEUDOR";
            this.bandedGridColumn5.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn5.FieldName = "SaldoDebe";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoDebe", "{0:n2}")});
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 94;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn6.Caption = "ACREEDOR";
            this.bandedGridColumn6.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn6.FieldName = "SaldoHaber";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoHaber", "{0:n2}")});
            this.bandedGridColumn6.Visible = true;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "INVENTARIO";
            this.gridBand4.Columns.Add(this.bandedGridColumn7);
            this.gridBand4.Columns.Add(this.bandedGridColumn8);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 240;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn7.Caption = "ACTIVO";
            this.bandedGridColumn7.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn7.FieldName = "InvenDebe";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenDebe", "{0:n2}")});
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 113;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn8.Caption = "PASIVO Y PATRIMONIO";
            this.bandedGridColumn8.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn8.FieldName = "InvenHaber";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenHaber", "{0:n2}")});
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 127;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "FUNCION";
            this.gridBand5.Columns.Add(this.bandedGridColumn9);
            this.gridBand5.Columns.Add(this.bandedGridColumn10);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 150;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn9.Caption = "PERDIDAS";
            this.bandedGridColumn9.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn9.FieldName = "FuncionDebe";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionDebe", "{0:n2}")});
            this.bandedGridColumn9.Visible = true;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn10.Caption = "GANANCIAS";
            this.bandedGridColumn10.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn10.FieldName = "FuncionHaber";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionHaber", "{0:n2}")});
            this.bandedGridColumn10.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "NATURALEZA";
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Columns.Add(this.bandedGridColumn12);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 166;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn11.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn11.Caption = "PERDIDAS N";
            this.bandedGridColumn11.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn11.FieldName = "NatuDebe";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuDebe", "{0:n2}")});
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 83;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.bandedGridColumn12.AppearanceHeader.Options.UseForeColor = true;
            this.bandedGridColumn12.Caption = "GANANCIAS N";
            this.bandedGridColumn12.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn12.FieldName = "NatuHaber";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuHaber", "{0:n2}")});
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 83;
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(430, 457);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(566, 48);
            this.GridControl1.TabIndex = 250;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NatuDebe,
            this.NatuHaber,
            this.InvenDebe,
            this.InvenHaber,
            this.FuncionDebe,
            this.FuncionHaber});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.NatuDebe;
            this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.GridView1.OptionsFilter.AllowMRUFilterList = false;
            this.GridView1.OptionsPrint.AutoWidth = false;
            this.GridView1.OptionsView.ShowColumnHeaders = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.Tag = "<Null>";
            // 
            // NatuHaber
            // 
            this.NatuHaber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.NatuHaber.AppearanceCell.Options.UseFont = true;
            this.NatuHaber.AppearanceCell.Options.UseTextOptions = true;
            this.NatuHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NatuHaber.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NatuHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.NatuHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NatuHaber.Caption = "GANANCIAS N";
            this.NatuHaber.DisplayFormat.FormatString = "n2";
            this.NatuHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NatuHaber.FieldName = "NatuHaber";
            this.NatuHaber.Name = "NatuHaber";
            this.NatuHaber.OptionsColumn.AllowEdit = false;
            this.NatuHaber.OptionsColumn.AllowMove = false;
            this.NatuHaber.OptionsColumn.AllowSize = false;
            this.NatuHaber.OptionsColumn.FixedWidth = true;
            this.NatuHaber.OptionsFilter.AllowAutoFilter = false;
            this.NatuHaber.OptionsFilter.AllowFilter = false;
            this.NatuHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NatuHaber", "{0:n2}")});
            this.NatuHaber.Visible = true;
            this.NatuHaber.VisibleIndex = 5;
            this.NatuHaber.Width = 104;
            // 
            // InvenDebe
            // 
            this.InvenDebe.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.InvenDebe.AppearanceCell.Options.UseFont = true;
            this.InvenDebe.AppearanceCell.Options.UseTextOptions = true;
            this.InvenDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.InvenDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.InvenDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InvenDebe.Caption = "ACTIVO";
            this.InvenDebe.DisplayFormat.FormatString = "n2";
            this.InvenDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.InvenDebe.FieldName = "InvenDebe";
            this.InvenDebe.Name = "InvenDebe";
            this.InvenDebe.OptionsColumn.AllowEdit = false;
            this.InvenDebe.OptionsColumn.AllowMove = false;
            this.InvenDebe.OptionsColumn.AllowSize = false;
            this.InvenDebe.OptionsColumn.FixedWidth = true;
            this.InvenDebe.OptionsFilter.AllowAutoFilter = false;
            this.InvenDebe.OptionsFilter.AllowFilter = false;
            this.InvenDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenDebe", "{0:n2}")});
            this.InvenDebe.Visible = true;
            this.InvenDebe.VisibleIndex = 0;
            this.InvenDebe.Width = 104;
            // 
            // InvenHaber
            // 
            this.InvenHaber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.InvenHaber.AppearanceCell.Options.UseFont = true;
            this.InvenHaber.AppearanceCell.Options.UseTextOptions = true;
            this.InvenHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.InvenHaber.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.InvenHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.InvenHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InvenHaber.Caption = "PASIVO Y PATRIMONIO";
            this.InvenHaber.DisplayFormat.FormatString = "n2";
            this.InvenHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.InvenHaber.FieldName = "InvenHaber";
            this.InvenHaber.Name = "InvenHaber";
            this.InvenHaber.OptionsColumn.AllowEdit = false;
            this.InvenHaber.OptionsColumn.AllowMove = false;
            this.InvenHaber.OptionsColumn.AllowSize = false;
            this.InvenHaber.OptionsColumn.FixedWidth = true;
            this.InvenHaber.OptionsFilter.AllowAutoFilter = false;
            this.InvenHaber.OptionsFilter.AllowFilter = false;
            this.InvenHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvenHaber", "{0:n2}")});
            this.InvenHaber.Visible = true;
            this.InvenHaber.VisibleIndex = 1;
            this.InvenHaber.Width = 104;
            // 
            // FuncionDebe
            // 
            this.FuncionDebe.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.FuncionDebe.AppearanceCell.Options.UseFont = true;
            this.FuncionDebe.AppearanceCell.Options.UseTextOptions = true;
            this.FuncionDebe.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FuncionDebe.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuncionDebe.AppearanceHeader.Options.UseTextOptions = true;
            this.FuncionDebe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuncionDebe.Caption = "PÉRDIDAS ";
            this.FuncionDebe.DisplayFormat.FormatString = "n2";
            this.FuncionDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FuncionDebe.FieldName = "FuncionDebe";
            this.FuncionDebe.Name = "FuncionDebe";
            this.FuncionDebe.OptionsColumn.AllowEdit = false;
            this.FuncionDebe.OptionsColumn.AllowMove = false;
            this.FuncionDebe.OptionsColumn.AllowSize = false;
            this.FuncionDebe.OptionsColumn.FixedWidth = true;
            this.FuncionDebe.OptionsFilter.AllowAutoFilter = false;
            this.FuncionDebe.OptionsFilter.AllowFilter = false;
            this.FuncionDebe.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionDebe", "{0:n2}")});
            this.FuncionDebe.Visible = true;
            this.FuncionDebe.VisibleIndex = 2;
            this.FuncionDebe.Width = 104;
            // 
            // FuncionHaber
            // 
            this.FuncionHaber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.FuncionHaber.AppearanceCell.Options.UseFont = true;
            this.FuncionHaber.AppearanceCell.Options.UseTextOptions = true;
            this.FuncionHaber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FuncionHaber.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuncionHaber.AppearanceHeader.Options.UseTextOptions = true;
            this.FuncionHaber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuncionHaber.Caption = "GANANCIAS";
            this.FuncionHaber.DisplayFormat.FormatString = "n2";
            this.FuncionHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FuncionHaber.FieldName = "FuncionHaber";
            this.FuncionHaber.Name = "FuncionHaber";
            this.FuncionHaber.OptionsColumn.AllowEdit = false;
            this.FuncionHaber.OptionsColumn.AllowMove = false;
            this.FuncionHaber.OptionsColumn.AllowSize = false;
            this.FuncionHaber.OptionsColumn.FixedWidth = true;
            this.FuncionHaber.OptionsFilter.AllowAutoFilter = false;
            this.FuncionHaber.OptionsFilter.AllowFilter = false;
            this.FuncionHaber.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuncionHaber", "{0:n2}")});
            this.FuncionHaber.Visible = true;
            this.FuncionHaber.VisibleIndex = 3;
            this.FuncionHaber.Width = 104;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnlimpiar,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 5;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnlimpiar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Caption = "Limpiar";
            this.btnlimpiar.Id = 0;
            this.btnlimpiar.ImageOptions.Image = global::Contable.Properties.Resources.Limpiar;
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnlimpiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnlimpiar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1008, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1008, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 475);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1008, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 475);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Exportar";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnexportar_ItemClick);
            // 
            // frm_balance_comprobacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1008, 507);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.dgvdatos);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frm_balance_comprobacion";
            this.Text = "Balace de comprobacion";
            this.Load += new System.EventHandler(this.frm_balance_comprobacion_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RbtMensual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbtAcumulado.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmesdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmescod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtniveldesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnivel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.CheckEdit RbtMensual;
        private DevExpress.XtraEditors.CheckEdit RbtAcumulado;
        private DevExpress.XtraEditors.TextEdit txtniveldesc;
        private DevExpress.XtraEditors.TextEdit txtnivel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraEditors.SimpleButton btnver;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtmesdesc;
        private DevExpress.XtraEditors.TextEdit txtmescod;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        internal DevExpress.XtraGrid.Columns.GridColumn NatuDebe;
        internal DevExpress.XtraGrid.Columns.GridColumn NatuHaber;
        internal DevExpress.XtraGrid.Columns.GridColumn InvenDebe;
        internal DevExpress.XtraGrid.Columns.GridColumn InvenHaber;
        internal DevExpress.XtraGrid.Columns.GridColumn FuncionDebe;
        internal DevExpress.XtraGrid.Columns.GridColumn FuncionHaber;
        public DevExpress.XtraGrid.GridControl GridControl1;
        public DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnlimpiar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}
