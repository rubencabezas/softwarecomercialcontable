﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable.Reportes.Balance_Comprobacion
{
    public partial class frm_ver_detalle_balance : Form
    {
        public frm_ver_detalle_balance()
        {
            InitializeComponent();
        }


        public string Empresa;
        public string Anio;
        public string Periodo;
        public string Moneda;
        public string CtaContable;
        public bool Es_Acumulado;
        private void frm_ver_detalle_balance_Load(object sender, EventArgs e)
        {
            Listar_Detalle();
        }

        List<Entidad_Balance_Comprobacion_Reporte> ListRC = new List<Entidad_Balance_Comprobacion_Reporte>();

        void Listar_Detalle()
        {
            try
            {
                Logica_Balance_Comprobacion_Reporte Log = new Logica_Balance_Comprobacion_Reporte();

                Entidad_Balance_Comprobacion_Reporte Ent = new Entidad_Balance_Comprobacion_Reporte();

                Ent.Id_Empresa = Empresa;
                Ent.Id_Anio = Anio;
                Ent.Id_Periodo = Periodo;
                Ent.Id_Moneda = Moneda;
                Ent.Cuenta = CtaContable;
                Ent.Acumulado = Es_Acumulado;

                if (Es_Acumulado == true){
                    ListRC = Log.Ver_Detalle_Cuenta_Balance_General_Acumulado(Ent);

                    if (ListRC.Count > 0)
                    {

                        dgvdatos.DataSource = ListRC;
                    }
                }
                else
                {
                    ListRC = Log.Ver_Detalle_Cuenta_Balance_General(Ent);

                    if (ListRC.Count > 0)
                    {

                        dgvdatos.DataSource = ListRC;
                    }
                }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\DetalleCuenta" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
