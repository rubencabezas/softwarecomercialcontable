﻿namespace Contable.Reportes.Reportes_Impresiones
{
    partial class Rpt_Estado_Situacion_Finan_Resultado_Nat_Funcion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TxtPerAnio = new DevExpress.XtraReports.UI.XRLabel();
            this.TxtRuc = new DevExpress.XtraReports.UI.XRLabel();
            this.TxtRazonSocial = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // TopMargin
            // 
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrSubreport1});
            this.Detail.Name = "Detail";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(401.1328F, 10.00001F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(235.42F, 74.37F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 10.00001F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(283.33F, 74.37F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1,
            this.TxtPerAnio,
            this.TxtRuc,
            this.TxtRazonSocial,
            this.XrLabel34});
            this.PageHeader.HeightF = 155.2917F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(409.5834F, 101.875F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(207.7994F, 22.16667F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "PERIODO/EJERCICIO";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(409.5834F, 79.70832F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(265.4166F, 22.16667F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "ESTADO DE RESULTADO (Por Funcion)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TxtPerAnio
            // 
            this.TxtPerAnio.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.TxtPerAnio.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 101.875F);
            this.TxtPerAnio.Name = "TxtPerAnio";
            this.TxtPerAnio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TxtPerAnio.SizeF = new System.Drawing.SizeF(207.7994F, 22.16667F);
            this.TxtPerAnio.StylePriority.UseFont = false;
            this.TxtPerAnio.StylePriority.UseTextAlignment = false;
            this.TxtPerAnio.Text = "PERIODO/EJERCICIO";
            this.TxtPerAnio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TxtRuc
            // 
            this.TxtRuc.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.TxtRuc.LocationFloat = new DevExpress.Utils.PointFloat(0F, 22.16666F);
            this.TxtRuc.Name = "TxtRuc";
            this.TxtRuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TxtRuc.SizeF = new System.Drawing.SizeF(414.2388F, 22.16667F);
            this.TxtRuc.StylePriority.UseFont = false;
            this.TxtRuc.StylePriority.UseTextAlignment = false;
            this.TxtRuc.Text = "RUC";
            this.TxtRuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TxtRazonSocial
            // 
            this.TxtRazonSocial.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.TxtRazonSocial.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.TxtRazonSocial.Name = "TxtRazonSocial";
            this.TxtRazonSocial.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TxtRazonSocial.SizeF = new System.Drawing.SizeF(742.3638F, 22.16667F);
            this.TxtRazonSocial.StylePriority.UseFont = false;
            this.TxtRazonSocial.StylePriority.UseTextAlignment = false;
            this.TxtRazonSocial.Text = "RAZON SOCIAL";
            this.TxtRazonSocial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // XrLabel34
            // 
            this.XrLabel34.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 79.70832F);
            this.XrLabel34.Name = "XrLabel34";
            this.XrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel34.SizeF = new System.Drawing.SizeF(300.4358F, 22.16667F);
            this.XrLabel34.StylePriority.UseFont = false;
            this.XrLabel34.StylePriority.UseTextAlignment = false;
            this.XrLabel34.Text = "ESTADO DE RESULTADO (Por Naturaleza)";
            this.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 32.20835F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Rpt_Estado_Situacion_Finan_Resultado_Nat_Funcion
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.Detail,
            this.PageHeader,
            this.ReportFooter});
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(68, 84, 100, 100);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        public DevExpress.XtraReports.UI.XRLabel TxtPerAnio;
        public DevExpress.XtraReports.UI.XRLabel TxtRuc;
        public DevExpress.XtraReports.UI.XRLabel TxtRazonSocial;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel34;
        public DevExpress.XtraReports.UI.XRLabel xrLabel2;
        internal DevExpress.XtraReports.UI.XRLabel xrLabel1;
        public DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        public DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
    }
}
