﻿namespace Contable.Reportes.Reportes_Impresiones
{
    partial class Rpt_Estado_Situacion_Finan_Activo_pasivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.XrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.TxtRazonSocial = new DevExpress.XtraReports.UI.XRLabel();
            this.TxtRuc = new DevExpress.XtraReports.UI.XRLabel();
            this.TxtPerAnio = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 43F;
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrSubreport1});
            this.Detail.HeightF = 74.37499F;
            this.Detail.Name = "Detail";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(478.9167F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(235.4167F, 74.37499F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(352.0833F, 74.37499F);
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataSource = typeof(Contable.Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel34,
            this.TxtRazonSocial,
            this.TxtRuc,
            this.TxtPerAnio});
            this.PageHeader.HeightF = 140.3333F;
            this.PageHeader.Name = "PageHeader";
            // 
            // XrLabel34
            // 
            this.XrLabel34.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(122.3108F, 54.33334F);
            this.XrLabel34.Name = "XrLabel34";
            this.XrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel34.SizeF = new System.Drawing.SizeF(351.4774F, 22.16667F);
            this.XrLabel34.StylePriority.UseFont = false;
            this.XrLabel34.StylePriority.UseTextAlignment = false;
            this.XrLabel34.Text = "ESTADO DE SITUACION FINANCIERA";
            this.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TxtRazonSocial
            // 
            this.TxtRazonSocial.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.TxtRazonSocial.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 10.00001F);
            this.TxtRazonSocial.Name = "TxtRazonSocial";
            this.TxtRazonSocial.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TxtRazonSocial.SizeF = new System.Drawing.SizeF(742.3638F, 22.16667F);
            this.TxtRazonSocial.StylePriority.UseFont = false;
            this.TxtRazonSocial.StylePriority.UseTextAlignment = false;
            this.TxtRazonSocial.Text = "RAZON SOCIAL";
            this.TxtRazonSocial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TxtRuc
            // 
            this.TxtRuc.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.TxtRuc.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 32.16667F);
            this.TxtRuc.Name = "TxtRuc";
            this.TxtRuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TxtRuc.SizeF = new System.Drawing.SizeF(414.2388F, 22.16667F);
            this.TxtRuc.StylePriority.UseFont = false;
            this.TxtRuc.StylePriority.UseTextAlignment = false;
            this.TxtRuc.Text = "RUC";
            this.TxtRuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TxtPerAnio
            // 
            this.TxtPerAnio.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.TxtPerAnio.LocationFloat = new DevExpress.Utils.PointFloat(305.6441F, 88.95833F);
            this.TxtPerAnio.Name = "TxtPerAnio";
            this.TxtPerAnio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TxtPerAnio.SizeF = new System.Drawing.SizeF(207.7994F, 22.16667F);
            this.TxtPerAnio.StylePriority.UseFont = false;
            this.TxtPerAnio.StylePriority.UseTextAlignment = false;
            this.TxtPerAnio.Text = "PERIODO/EJERCICIO";
            this.TxtPerAnio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 39.16667F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Rpt_Estado_Situacion_Finan_Activo_pasivo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.Detail,
            this.PageHeader,
            this.ReportFooter});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(44, 44, 43, 100);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel34;
        public DevExpress.XtraReports.UI.XRLabel TxtRazonSocial;
        public DevExpress.XtraReports.UI.XRLabel TxtRuc;
        public DevExpress.XtraReports.UI.XRLabel TxtPerAnio;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        public DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        public DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
    }
}
