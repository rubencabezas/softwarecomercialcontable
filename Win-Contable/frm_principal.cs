﻿using Contable.Reportes.Libros_y_registros;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

 
using System.Net.Http; //HttpClient, HttpResponseMessage
using System.IO; //File, MemoryStream
using System.Threading;
using Contable._0_Configuracion.Estados_Financieros;
using Comercial;

namespace Contable
{
    public partial class frm_principal : Contable.frm_fuente
    {
        public frm_principal()
        {
            InitializeComponent();
        }

        public  string EmpresaNombre;
        public  string CodigoEmpresa ;
        public  string RucEmpresa;

        public  string AnioSelect;
        public  string PeriodoSelect ;

        public  string UserID;
        public  string UserName = "user";
        public  string UserNameEmployee;

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda.frm_moneda().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda.frm_moneda f = new _0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda.frm_moneda();
                AbreHijo(f);
            }
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_empresa().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_empresa f = new _0_Configuracion.Mantenimiento.Generales.frm_empresa();

                AbreHijo(f);
            }
        }

        private void btnTipoImpuesto_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto f = new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto();
            ////f.MdiParent = this;
            ////f.Show();

            if (MdiNoDuplicate(f.Name) == false)
            {
                _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto ff = new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_impuesto();
                AbreHijo(ff);
            }
        }

        private void btnAsignacionImp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Impuesto.frm_asignacion_impuesto().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Impuesto.frm_asignacion_impuesto f = new _0_Configuracion.Mantenimiento.Impuesto.frm_asignacion_impuesto();

                AbreHijo(f);
            }

        }

        private void btnventas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            //Procesos.Ventas.frm_ventas f = new Procesos.Ventas.frm_ventas();
            //f.MdiParent = this;
            //f.Show();

          
        }

        private void btncompras_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (MdiNoDuplicate(new frm_compras().Name) == false)
            {
                frm_compras f = new frm_compras();
                AbreHijo(f);
            }
        }

        public bool MdiNoDuplicate(string VcNombre)
        {
            FormCollection loColFormularios = default(FormCollection);

            loColFormularios = Application.OpenForms;
            foreach (Form Form in loColFormularios)
            {
                if (Form.Name == VcNombre)
                {
                    Form.Activate();
                    return true;
                }
            }
            return false;
        }

        public void AbreHijo(Form Frm)
        {
            Frm.MdiParent = this;
            if (Frm.MdiChildren.Count() <= 0 )
            {
                //Par que se maximize en caso no encontrase ningun hijo de su c.. padre
                Frm.WindowState = FormWindowState.Maximized;
            }
          
            Frm.Show();
        }


   
     public override   void ActualizarBusquedaPorPeriodo()
        {

            try
            {
                FormCollection loColFormularios;
                string name = typeof(frm_fuente).AssemblyQualifiedName;
                Type type = Type.GetType(name);

                loColFormularios = Application.OpenForms;
                foreach (Form  loForm in loColFormularios)
                {
               
                    if ((loForm.GetType() == loForm.GetType()))
                    {
                        frm_fuente FrmChildFather = new frm_fuente();
                        FrmChildFather.ActualizarBusquedaPorPerio();
                    }

                }
             

            }
            catch (Exception ex)
            {
            }

        }

        private static void Test(Type formType)
        {
            foreach (Form f in Application.OpenForms)
            {
                if (f.GetType() == formType)
                {
                }
            }
        }
        private void btnlibroscontables_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Tablas_Sunat.Libros.frm_libros_contables().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Tablas_Sunat.Libros.frm_libros_contables f = new _0_Configuracion.Mantenimiento.Tablas_Sunat.Libros.frm_libros_contables();
                AbreHijo(f);
            }
        }

        private void btnejercicio_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_ejercicios().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_ejercicios f = new _0_Configuracion.Mantenimiento.Generales.frm_ejercicios();
                AbreHijo(f);
            }
        }

        private void btnelemento_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_elemento().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_elemento f = new _0_Configuracion.Mantenimiento.Generales.frm_elemento();
                AbreHijo(f);
            }
        }

        private void btnestructura_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_estructura().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_estructura f = new _0_Configuracion.Mantenimiento.Generales.frm_estructura();
                AbreHijo(f);
            }
        }

        private void btnplangeneral_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Generales.frm_plan_general().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Generales.frm_plan_general f = new _0_Configuracion.Mantenimiento.Generales.frm_plan_general();
                AbreHijo(f);
            }
        }

        private void btnplanempresarial_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Plan_Empresarial.frm_plan_empresarial().Name) == false)
            {
                _0_Configuracion.Plan_Empresarial.frm_plan_empresarial f = new _0_Configuracion.Plan_Empresarial.frm_plan_empresarial();
                AbreHijo(f);
            }
        }

        private void btncomprobantes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos.frm_tipo_documentos().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos.frm_tipo_documentos f = new _0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos.frm_tipo_documentos();
                AbreHijo(f);
            }
        }

        private void btninicial_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Inicio.frm_inicio().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Inicio.frm_inicio f = new _0_Configuracion.Mantenimiento.Inicio.frm_inicio();
                AbreHijo(f);
            }
        }

        private void btnentidad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Entidades.frm_entidad().Name) == false)
            {
                _0_Configuracion.Entidades.frm_entidad f = new _0_Configuracion.Entidades.frm_entidad();
                AbreHijo(f);
            }
        }

        private void btnunidad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Centro_Costo_gasto.frm_unidad().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Centro_Costo_gasto.frm_unidad f = new _0_Configuracion.Mantenimiento.Centro_Costo_gasto.frm_unidad();
                AbreHijo(f);
            }
        }

        private void btncentrocostogasto_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Centro_Costo_gasto.frm_centro_costo_gasto().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Centro_Costo_gasto.frm_centro_costo_gasto f = new _0_Configuracion.Mantenimiento.Centro_Costo_gasto.frm_centro_costo_gasto();
                AbreHijo(f);
            }
        }

        private void btntipocambio_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_cambio().Name) == false)
            {
                _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_cambio f = new _0_Configuracion.Mantenimiento.Impuesto.frm_tipo_cambio();
                AbreHijo(f);
            }
        }

        private void btnmarca_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Inventario.Catalogo.frm_marca().Name) == false)
            {
                Procesos.Inventario.Catalogo.frm_marca f = new Procesos.Inventario.Catalogo.frm_marca();
                AbreHijo(f);
            }
        }

        private void btnunidadmedida_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Inventario.Catalogo.frm_unidad_medida().Name) == false)
            {
                Procesos.Inventario.Catalogo.frm_unidad_medida f = new Procesos.Inventario.Catalogo.frm_unidad_medida();
                AbreHijo(f);
            }
        }

        private void btngrupo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new  frm_grupo().Name) == false)
            {
                 frm_grupo f = new  frm_grupo();
                AbreHijo(f);
            }
        }

        private void btnfamilia_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new  frm_familia().Name) == false)
            {
                 frm_familia f = new  frm_familia();
                AbreHijo(f);
            }
        }

        private void btncatalogo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new  frm_catalogo().Name) == false)
            {
                 frm_catalogo f = new  frm_catalogo();
                AbreHijo(f);
            }
        }

        private void btndiario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void btninventario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Inventario.frm_inventario().Name) == false)
            {
                Procesos.Inventario.frm_inventario f = new Procesos.Inventario.frm_inventario();
                AbreHijo(f);
            }
        }

        private void btnalmacen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new frm_almacen().Name) == false)
            {
                frm_almacen f = new frm_almacen();
                AbreHijo(f);
            }
        }

        private void btntesoreria_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (MdiNoDuplicate(new Procesos.Tesoreria.frm_egresos().Name) == false)
            //{
            //    Procesos.Tesoreria.frm_egresos f = new Procesos.Tesoreria.frm_egresos();
            //    AbreHijo(f);
            //}
        }

        private void btnbalancecomprobacion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Balance_Comprobacion.frm_balance_comprobacion().Name) == false)
            {
                Reportes.Balance_Comprobacion.frm_balance_comprobacion f = new Reportes.Balance_Comprobacion.frm_balance_comprobacion();
                AbreHijo(f);
            }
        }

        private void btnregistrocompras_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_registro_compras().Name) == false)
            {
                Reportes.Libros_y_registros.frm_registro_compras f = new Reportes.Libros_y_registros.frm_registro_compras();
                AbreHijo(f);
            }
        }

        private void btnregistroventa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_registo_ventas().Name) == false)
            {
                Reportes.Libros_y_registros.frm_registo_ventas f = new Reportes.Libros_y_registros.frm_registo_ventas();
                AbreHijo(f);
            }
        }

        private void frm_principal_Load(object sender, EventArgs e)
        {
            Actual_Conexion.EmpresaNombre = EmpresaNombre;
            Actual_Conexion.CodigoEmpresa = CodigoEmpresa;
            Actual_Conexion.RucEmpresa = RucEmpresa;

            Actual_Conexion.UserID = UserID;
            Actual_Conexion.UserName = UserName;
            Actual_Conexion.UserNameEmployee = UserNameEmployee;

            IniciarBarra();

            timerTipodeCambio.Enabled = true;

        }

        private void frm_principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Login.frml_login f = new Login.frml_login();
            //    f.Close();
        }

        private void frm_principal_FormClosed(object sender, FormClosedEventArgs e)
        {
    
            Application.Exit();
        }

        List<Entidad_Ejercicio> Anios = new List<Entidad_Ejercicio>();
        List<Entidad_Ejercicio> Periodos = new List<Entidad_Ejercicio>();
        List<Entidad_Ejercicio> Anios_Aux = new List<Entidad_Ejercicio>();
        public void IniciarBarra()
        {
            try
            {

               txtempresa.Caption = Actual_Conexion.EmpresaNombre;
                Logica_Ejercicio LogAnio = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

                Anios = LogAnio.Listar(Ent) ;

                var PrivView = from item in Anios
                             orderby item.Id_Anio descending
                               select item;
                Anios_Aux = PrivView.ToList();


                comboanio.ValueMember = "Id_Anio";
                comboanio.DisplayMember = "Id_Anio";
                comboanio.DataSource = Anios_Aux;
               
                ListarPeriodos();

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        public void ListarPeriodos()
        {
            try
            {

                Logica_Ejercicio LogPer = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;

                Periodos = LogPer.Listar_Periodo(Ent);

          
                comboperiodo.ValueMember = "Id_Periodo";
                comboperiodo.DisplayMember = "Descripcion_Periodo";
                comboperiodo.DataSource = Periodos;
               

                DateTime time = DateTime.Now;
                string format = "00";
                comboperiodo.SelectedValue = time.Month.ToString(format);
             
    }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }






        private void comboanio_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        if (comboanio.Items.Count > 0)
            {
        
                Actual_Conexion.AnioSelect = comboanio.SelectedValue.ToString();
                ListarPeriodos();
                Actual_Conexion.PeriodoSelect = comboperiodo.SelectedValue.ToString();
                Accion.RefreshDateDefault();
                ActualizarBusquedaPorAnio();
            }
        }

        //public Boolean Cambio=false;
        private void comboperiodo_SelectedIndexChanged(object sender, EventArgs e)
        {
       
        if (comboperiodo.Items.Count > 0)
            {
                Actual_Conexion.PeriodoSelect = comboperiodo.SelectedValue.ToString();
                Accion.RefreshDateDefault();
                ActualizarBusquedaPorPeriodo();
                //Cambio = true;
                frm_fuente FrmChildFather = new frm_fuente();
                FrmChildFather.cambio=true;
            }
        }

        private void btnorden_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Compras.frm_orden_sc().Name) == false)
            {
                Procesos.Compras.frm_orden_sc f = new Procesos.Compras.frm_orden_sc();
                AbreHijo(f);
            }
        }

        private void Btnventaa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Ventas.frm_ventas().Name) == false)
            {
                Procesos.Ventas.frm_ventas f = new Procesos.Ventas.frm_ventas();
                AbreHijo(f);
            }
        }

        private void btngruposervicios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Compras.Catalogo_Servicios.frm_grupo_ser().Name) == false)
            {
                Procesos.Compras.Catalogo_Servicios.frm_grupo_ser f = new Procesos.Compras.Catalogo_Servicios.frm_grupo_ser();
                AbreHijo(f);
            }
        }

        private void btnfamiliaservicios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Compras.Catalogo_Servicios.frm_familia_ser().Name) == false)
            {
                Procesos.Compras.Catalogo_Servicios.frm_familia_ser f = new Procesos.Compras.Catalogo_Servicios.frm_familia_ser();
                AbreHijo(f);
            }
        }

        private void btncatalogoservicios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Compras.Catalogo_Servicios.frm_catalogo_ser().Name) == false)
            {
                Procesos.Compras.Catalogo_Servicios.frm_catalogo_ser f = new Procesos.Compras.Catalogo_Servicios.frm_catalogo_ser();
                AbreHijo(f);
            }
        }

        private void btndiarioo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Diario.frm_diario().Name) == false)
            {
                Procesos.Diario.frm_diario f = new Procesos.Diario.frm_diario();
                AbreHijo(f);
            }
        }

        private void btntipooperacion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Inventario.Almacen.frm_tipo_operacion().Name) == false)
            {
                Procesos.Inventario.Almacen.frm_tipo_operacion f = new Procesos.Inventario.Almacen.frm_tipo_operacion();
                AbreHijo(f);
            }
        }

        private void btnanalisis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (MdiNoDuplicate(new _0_Configuracion.Plan_Empresarial.frm_analisis_operacion().Name) == false)
            {
                _0_Configuracion.Plan_Empresarial.frm_analisis_operacion f = new _0_Configuracion.Plan_Empresarial.frm_analisis_operacion();
                AbreHijo(f);
            }
        }

        private void btnrequerimientos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Compras.frm_requerimiento().Name) == false)
            {
                Procesos.Compras.frm_requerimiento f = new Procesos.Compras.frm_requerimiento();
                AbreHijo(f);
            }
        }

        private void btncargo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.frm_cargo().Name) == false)
            {
                _0_Configuracion.frm_cargo f = new _0_Configuracion.frm_cargo();
                AbreHijo(f);
            }
        }

        private void btnarea_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new _0_Configuracion.frm_area().Name) == false)
            {
                _0_Configuracion.frm_area f = new _0_Configuracion.frm_area();
                AbreHijo(f);
            }
        }

        private void btnlibrodiario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_libro_diario().Name) == false)
            {
                Reportes.Libros_y_registros.frm_libro_diario f = new Reportes.Libros_y_registros.frm_libro_diario();
                AbreHijo(f);
            }
        }

        private void btnlibromayor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_libro_mayor().Name) == false)
            {
                Reportes.Libros_y_registros.frm_libro_mayor f = new Reportes.Libros_y_registros.frm_libro_mayor();
                AbreHijo(f);
            }
        }

        private void btnegresos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (MdiNoDuplicate(new Procesos.Tesoreria.frm_egresos().Name) == false)
            //{
            //    Procesos.Tesoreria.frm_egresos f = new Procesos.Tesoreria.frm_egresos();
            //    AbreHijo(f);
            //}
        }

        private void btnmediopago_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_medio_pago().Name) == false)
            {
                Procesos.Tesoreria.frm_medio_pago f = new Procesos.Tesoreria.frm_medio_pago();
                AbreHijo(f);
            }
        }

        private void btnentidadfinanciera_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_entidad_financiera().Name) == false)
            {
                Procesos.Tesoreria.frm_entidad_financiera f = new Procesos.Tesoreria.frm_entidad_financiera();
                AbreHijo(f);
            }
        }

        private void btncuentacorriente_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_cuenta_corriente().Name) == false)
            {
                Procesos.Tesoreria.frm_cuenta_corriente f = new Procesos.Tesoreria.frm_cuenta_corriente();
                AbreHijo(f);
            }
        }

        private void btningresos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_ingreso().Name) == false)
            {
                Procesos.Tesoreria.frm_ingreso f = new Procesos.Tesoreria.frm_ingreso();
                AbreHijo(f);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_series_documentos().Name) == false)
            {
                Procesos.Tesoreria.frm_series_documentos f = new Procesos.Tesoreria.frm_series_documentos();
               AbreHijo(f);
           }
        }

        private void btnaperturach_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_apertura_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_apertura_cajachica f = new Procesos.Tesoreria.frm_apertura_cajachica();
                AbreHijo(f);
            }
        }

        private void btncajachica_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_caja_chica().Name) == false)
            {
                Procesos.Tesoreria.frm_caja_chica f = new Procesos.Tesoreria.frm_caja_chica();
                AbreHijo(f);
            }
        }

        private void btnrendicion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_rendicion_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_rendicion_cajachica f = new Procesos.Tesoreria.frm_rendicion_cajachica();
                AbreHijo(f);
            }
        }

        private void btnreembolso_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_reembolso_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_reembolso_cajachica f = new Procesos.Tesoreria.frm_reembolso_cajachica();
                AbreHijo(f);
            }
        }

        private void btncierre_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Procesos.Tesoreria.frm_cierre_cajachica().Name) == false)
            {
                Procesos.Tesoreria.frm_cierre_cajachica f = new Procesos.Tesoreria.frm_cierre_cajachica();
                AbreHijo(f);
            }
        }

        private void btnreplicacion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new frm_replicacion().Name) == false)
            {
                frm_replicacion f = new frm_replicacion();
                AbreHijo(f);
            }
        }

        private void btnestadosfianacierostributarios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_balance_tributario().Name))
            {
                frm_formato_balance_tributario frm = new frm_formato_balance_tributario();
                this.AbreHijo(frm);
            }
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_determinacion_igv().Name))
            {
                frm_determinacion_igv frm = new frm_determinacion_igv();
                this.AbreHijo(frm);
            }
        }

        private void btnLibroSimplificado_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MdiNoDuplicate(new Reportes.Libros_y_registros.frm_libro_diario_simplificado().Name) == false)
            {
                Reportes.Libros_y_registros.frm_libro_diario_simplificado f = new Reportes.Libros_y_registros.frm_libro_diario_simplificado();
                AbreHijo(f);
            }
        }

        public void Progreso()
        {
            _2_Alertas.WaitForm1 dlg = new _2_Alertas.WaitForm1();
            dlg.ShowDialog();
        }

        private void timerTipodeCambio_Tick(object sender, EventArgs e)
        {
            
            System.Threading.Thread p = new System.Threading.Thread((this.Progreso));
            //Dim p As New System.Threading.Thread(AddressOf Progreso)

            //Thread obj = new Thread();

            //// Creating thread 
            //// Using thread class 
            //Thread thr = new Thread(new ThreadStart(obj.Pro));


            try
            {
         
                p.Start();
               // if (Verificacion_Internet.InternetConnection())
               // { 
                // TipoDeCambio();
                    p.Abort();
               // }
            }
            catch(Exception ex)
            {
                p.Abort();
            }
        }


        void TipoDeCambio()
        {
            HttpClient cliente = new HttpClient();
            cliente.BaseAddress = new Uri("http://www.sunat.gob.pe/");
            HttpResponseMessage rpta = cliente.GetAsync("cl-at-ittipcam/tcS01Alias").Result;
            if (rpta != null && rpta.IsSuccessStatusCode)
            {
                string contenido = "";
                using (MemoryStream ms = (MemoryStream)rpta.Content.ReadAsStreamAsync().Result)
                {
                    byte[] buffer = ms.ToArray();
                    contenido = Encoding.UTF8.GetString(buffer);
                    contenido = contenido.ToLower();
                }
                if (contenido.Length > 0)
                {
                    File.WriteAllText("Sunat.txt", contenido);
                    int posInicioT1 = contenido.IndexOf("<table");
                    int posFinT1 = contenido.IndexOf("</table");
                    if (posInicioT1 > -1 && posFinT1 > -1)
                    {
                        int posInicioT2 = contenido.IndexOf("<table", posInicioT1 + 1);
                        int posFinT2 = contenido.IndexOf("</table", posFinT1 + 1);
                        string tabla = contenido.Substring(posInicioT2, posFinT2 - posInicioT2 + 8);
                        File.WriteAllText("Tabla.txt", tabla);
                        posInicioT1 = 0;
                        tabla = tabla.Replace("</strong>", "");
                        List<string> valores = new List<string>();
                        for (int i = 1; i < 4; i++)
                        {
                            posInicioT1 = tabla.LastIndexOf("</td");
                            if (posInicioT1 > -1)
                            {
                                tabla = tabla.Substring(0, posInicioT1).Trim();
                                posFinT1 = tabla.LastIndexOf(">");
                                if (posFinT1 > -1)
                                {
                                    valores.Add(tabla.Substring(posFinT1 + 1, tabla.Length - posFinT1 - 1).Trim());
                                }
                            }
                        }
                        if (valores.Count > 0)
                        {
                            Entidad_Tipo_Cambio Ent = new Entidad_Tipo_Cambio();
                            Logica_Tipo_Cambio Log = new Logica_Tipo_Cambio();

                            var date = DateTime.Now.ToString("dd/MM/yyyy");

                            Ent.Tic_Fecha = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", date)).Date;
                            Ent.Tic_Id_Moneda ="001";

                            Ent.Tic_Compra_Vigente = Convert.ToDecimal(valores[1]);

                            Ent.Tic_Venta_Vigente = Convert.ToDecimal(valores[0]);

                            Ent.Tic_Venta_Publicacion = Convert.ToDecimal(0);

                            if (Log.Insertar(Ent))
                            {
                                timerTipodeCambio.Enabled = false;
                            }

                            //txtVenta.Text = valores[0];
                            //txtCompra.Text = valores[1];
                            //txtFecha.Text = valores[2];
                        }
                    }
                }
            }
        }

        private void barButtonItem9_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_estado_situacion_finan_est_resultado().Name))
            {
                frm_formato_estado_situacion_finan_est_resultado frm = new frm_formato_estado_situacion_finan_est_resultado();
                this.AbreHijo(frm);
            }
        }

        private void barButtonItem12_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_flujo_efectivo().Name))
            {
                frm_formato_flujo_efectivo frm = new frm_formato_flujo_efectivo();
                this.AbreHijo(frm);
            }
        }

        private void barButtonItem14_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             if (!this.MdiNoDuplicate(new frm_formato_estado_cambios_patrimonio_neto_smv().Name))
                {
                    frm_formato_estado_cambios_patrimonio_neto_smv frm = new frm_formato_estado_cambios_patrimonio_neto_smv();
                    this.AbreHijo(frm);
                }
        }

        private void btnestadofinansmv_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_estado_situacion_finan_est_resultado().Name))
            {
                frm_formato_estado_situacion_finan_est_resultado frm = new frm_formato_estado_situacion_finan_est_resultado();
                this.AbreHijo(frm);
            }
        }

        private void btnesatdosituacionfinanciera_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_situacion_financiera().Name))
            {
                frm_estado_situacion_financiera frm = new frm_estado_situacion_financiera();
                this.AbreHijo(frm);
            }
        }

        private void btnflujoefectivo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_flujo_efectivo().Name))
            {
                frm_formato_flujo_efectivo frm = new frm_formato_flujo_efectivo();
                this.AbreHijo(frm);
            }
        }

        private void btncambiopatrimonionetosmv_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_formato_estado_cambios_patrimonio_neto_smv().Name))
            {
                frm_formato_estado_cambios_patrimonio_neto_smv frm = new frm_formato_estado_cambios_patrimonio_neto_smv();
                this.AbreHijo(frm);
            }
        }

        private void btnestadodflujoefectivo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_flujo_efectivo().Name))
            {
                frm_estado_flujo_efectivo frm = new frm_estado_flujo_efectivo();
                this.AbreHijo(frm);
            }
        }

        private void btnestadocambiopatrimonioneto_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_cambio_patrimonio_neto().Name))
            {
                frm_estado_cambio_patrimonio_neto frm = new frm_estado_cambio_patrimonio_neto();
                this.AbreHijo(frm);
            }
        }

        private void btnesatdofinancierosunat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!this.MdiNoDuplicate(new frm_estado_financiero_tributario().Name))
                {
                        frm_estado_financiero_tributario frm = new frm_estado_financiero_tributario();
                    this.AbreHijo(frm);
                }
        }
    }
}
