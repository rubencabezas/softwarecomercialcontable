﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable
{
    public partial class frm_fuente : DevExpress.XtraEditors.XtraForm
    {
        public frm_fuente()
        {
            InitializeComponent();
            
        }

  
        public virtual void ActualizarBusquedaPorAni()
        {
        }
        public virtual void  ActualizarBusquedaPorPerio()
        {

        }
        private void frm_fuente_Load(object sender, EventArgs e)
        {
     
        }

        //Property MyIDMenu As String
        public string MyIDMenu { get; set; }
    }

    public enum Estados
    {
        Nuevo,
        Modificar,
        Consulta,
        Guardado,
        SoloLectura,
        Ninguno
    }


}
