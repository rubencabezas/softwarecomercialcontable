﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
    public class Verificacion_Internet
    {

        public static bool InternetConnection()
        {
            //  Returns True if connection is available 
            //  Replace www.yoursite.com with a site that 
            //  is guaranteed to be online - perhaps your 
            //  corporate site, or microsoft.com 
            System.Uri objUrl = new System.Uri("http://www.google.com.pe/");
            //  Setup WebRequest 
            System.Net.WebRequest objWebReq;
            objWebReq = System.Net.WebRequest.Create(objUrl);
            System.Net.WebResponse objResp;
            try
            {
                //  Attempt to get response and return True 
                objResp = objWebReq.GetResponse();
                objResp.Close();
                objWebReq = null;
                return true;
            }
            catch (Exception ex)
            {
                //  Error, exit and return False 
                objWebReq = null;
                return false;
            }

        }
    }
}