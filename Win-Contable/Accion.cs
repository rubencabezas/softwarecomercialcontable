﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Contable
{
 public   class Accion
    {

        public enum Estados
        {
            Nuevo,
            Modificar,
            Consulta,
            Guardado,
            SoloLectura,
            Ninguno
        }


        public static string Formato(string Id, Int32 Tamanio)
        {
            if (Id.Length < Tamanio)
            {
                for (int i = Id.Length; i <= Tamanio - 1; i++)
                {
                    Id = "0" + Id;
                }
            }
            return Id;
        }

        public static void ExitoGuardar()
        {
            using (_2_Alertas.frm_alerta_exito f = new _2_Alertas.frm_alerta_exito())
            {
                //fLblMensaje.Text = "Grabado correctamente en la Base de Datos";*/
                f.ShowDialog();
            }
        }

        public static void ExitoModificar()
        {
            using (_2_Alertas.frm_alerta_modificar f = new _2_Alertas.frm_alerta_modificar())
            {
                //fLblMensaje.Text = "Grabado correctamente en la Base de Datos";*/
                f.ShowDialog();
            }
        }
        public static void ErrorSistema(String text)
        {
            using (_2_Alertas.frm_alerta_error f = new _2_Alertas.frm_alerta_error())
            {
                f.lblmensaje.Text = text;
                f.ShowDialog();
            }
        }
        public static void Advertencia(String text)
        {
            using (_2_Alertas.frm_alerta_advertencia f = new _2_Alertas.frm_alerta_advertencia())
            {
                f.lblmensaje.Text = text;
                f.ShowDialog();
            }
        }


        public static bool ShowDeleted(string DBObject)
        {
            using (_2_Alertas.frm_alerta_eliminar f = new _2_Alertas.frm_alerta_eliminar())
            {
                f.lblmensaje.Text = DBObject;

                f.ShowDialog();
                if (f.Valor == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }


                //if ( f.ShowDialog() = DialogResult.OK)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}

            }
        }

        public static bool ShowDecision(string DBObject)
        {
            using (_2_Alertas.frm_alerta_decision f = new _2_Alertas.frm_alerta_decision())
            {
                f.lblmensaje.Text = DBObject;

                f.ShowDialog();
                if (f.Valor == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

 

            }
        }

        public static DateTime? pFecha;
        public static string pRazon;
        public static bool pInformarBaja;

        public static bool ShowAnulacionRazon(string DBObject)
        {
            using (_2_Alertas.frm_alerta_anulacion f = new _2_Alertas.frm_alerta_anulacion())
            {
                f.LblMensaje.Text = DBObject;
                if (f.ShowDialog() == DialogResult.OK)
                {
                    pFecha = Convert.ToDateTime(f.TxtFecha.Text);
                    pRazon = f.TxtMotivo.Text;
                    pInformarBaja = f.chkinformar.Checked;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }



        public static bool Valida_Anio_Actual_Ejercicio(string Empresa,string Anio)
        {
            List<Entidad_Empresa> Anio_Actual = new List<Entidad_Empresa>();
            Logica_Empresa Log_Anio_Actual = new Logica_Empresa();

            Entidad_Empresa enti = new Entidad_Empresa();
            enti.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            enti.Id_Anio = Actual_Conexion.AnioSelect;

            Anio_Actual = Log_Anio_Actual.Traer_Anio_Actual_Server(enti);

            if (Anio_Actual[0].Id_Anio != Actual_Conexion.AnioSelect)
            {
                Accion.Advertencia("Debe Aperturar el Periodo Actual");
                return false;
            }

            return true;
        }


        //        Public Shared DateDefault As String = "  /" & DatosActualConexion.PeriodoSelect & "/" & DatosActualConexion.AnioSelect
        //Shared Sub RefreshDateDefault()
        //        'For refresh value variable DateDefault
        //        DateDefault = "  /" & DatosActualConexion.PeriodoSelect & "/" & DatosActualConexion.AnioSelect
        //    End Sub

        public static string DateDefault = "  /" + Actual_Conexion.PeriodoSelect + "/" + Actual_Conexion.AnioSelect;

        public static void RefreshDateDefault()
        {
            DateDefault = "  /" + Actual_Conexion.PeriodoSelect + "/" + Actual_Conexion.AnioSelect;
            //return DateDefault;
        }

        public virtual void aaa()
        {

        }

        public static string Encriptar(string textoQueEncriptaremos)
        {

            return Encriptar(textoQueEncriptaremos, "pass75dc@avz10", "s@lAvz", "MD5", 1, "@1B2c3D4e5F6g7H8", 128);
        }

        public static string Encriptar(string textoQueEncriptaremos, string passBase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {

            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(textoQueEncriptaremos);

            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);

            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();

            string cipherText = Convert.ToBase64String(cipherTextBytes);
            return cipherText;
        }

        public static string Desencriptar(string textoEncriptado)
        {
            return Desencriptar(textoEncriptado, "pass75dc@avz10", "s@lAvz", "MD5", 1, "@1B2c3D4e5F6g7H8", 128);
        }

        public static string Desencriptar(string textoEncriptado, string passBase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {

            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] cipherTextBytes = Convert.FromBase64String(textoEncriptado);

            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);

            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };

            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();

            string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            return plainText;
        }


    }

}

