﻿namespace Contable
{
    partial class frm_main_master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.txtempresa = new DevExpress.XtraBars.BarStaticItem();
            this.lblusuario = new DevExpress.XtraBars.BarStaticItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btn_establecimiento = new DevExpress.XtraNavBar.NavBarItem();
            this.btnpuntoventa = new DevExpress.XtraNavBar.NavBarItem();
            this.bntpuntocompra = new DevExpress.XtraNavBar.NavBarItem();
            this.btnalmacen = new DevExpress.XtraNavBar.NavBarItem();
            this.btndocumentospdv = new DevExpress.XtraNavBar.NavBarItem();
            this.btnseriecomprobante = new DevExpress.XtraNavBar.NavBarItem();
            this.btnprecios = new DevExpress.XtraNavBar.NavBarItem();
            this.btncaja = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btngrupo = new DevExpress.XtraNavBar.NavBarItem();
            this.btnfamilia = new DevExpress.XtraNavBar.NavBarItem();
            this.btncatalogo = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btncompracomercial = new DevExpress.XtraNavBar.NavBarItem();
            this.btningresocomercial = new DevExpress.XtraNavBar.NavBarItem();
            this.btntraspasoscomercial = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup4 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnpos = new DevExpress.XtraNavBar.NavBarItem();
            this.Consultas = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnconsultabolfac = new DevExpress.XtraNavBar.NavBarItem();
            this.btnconsultasinticket = new DevExpress.XtraNavBar.NavBarItem();
            this.btnConsultaCajas = new DevExpress.XtraNavBar.NavBarItem();
            this.btnconsultastock = new DevExpress.XtraNavBar.NavBarItem();
            this.btnusers = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnusuario = new DevExpress.XtraNavBar.NavBarItem();
            this.btnaccesos = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup5 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnimpuesto = new DevExpress.XtraNavBar.NavBarItem();
            this.btnasignacionimporte = new DevExpress.XtraNavBar.NavBarItem();
            this.btntipocambio = new DevExpress.XtraNavBar.NavBarItem();
            this.btncuentacorriente = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup6 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnComprobantes = new DevExpress.XtraNavBar.NavBarItem();
            this.btnMonedas = new DevExpress.XtraNavBar.NavBarItem();
            this.btnlibroscontables = new DevExpress.XtraNavBar.NavBarItem();
            this.btnmediopago = new DevExpress.XtraNavBar.NavBarItem();
            this.btnentidadfinanciera = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup7 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btndiario = new DevExpress.XtraNavBar.NavBarItem();
            this.btncomrpas = new DevExpress.XtraNavBar.NavBarItem();
            this.btnventas = new DevExpress.XtraNavBar.NavBarItem();
            this.btntesoreria = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup8 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btncajachica = new DevExpress.XtraNavBar.NavBarItem();
            this.btnseriesdoccajachica = new DevExpress.XtraNavBar.NavBarItem();
            this.btnaperturacajachica = new DevExpress.XtraNavBar.NavBarItem();
            this.btnrendicion = new DevExpress.XtraNavBar.NavBarItem();
            this.btnreembolso = new DevExpress.XtraNavBar.NavBarItem();
            this.btncierre = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup9 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnempresas = new DevExpress.XtraNavBar.NavBarItem();
            this.btnejercicios = new DevExpress.XtraNavBar.NavBarItem();
            this.btnreplicacion = new DevExpress.XtraNavBar.NavBarItem();
            this.btninicio = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup10 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnelemento = new DevExpress.XtraNavBar.NavBarItem();
            this.btnestructura = new DevExpress.XtraNavBar.NavBarItem();
            this.btnplangeneral = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup11 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnplanempresarial = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup12 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnbalancecomprobacion = new DevExpress.XtraNavBar.NavBarItem();
            this.btnregistrocompras = new DevExpress.XtraNavBar.NavBarItem();
            this.btnregistroventa = new DevExpress.XtraNavBar.NavBarItem();
            this.btnlibrodiario = new DevExpress.XtraNavBar.NavBarItem();
            this.btnlibromayor = new DevExpress.XtraNavBar.NavBarItem();
            this.btnigv = new DevExpress.XtraNavBar.NavBarItem();
            this.btnLibroSimplificado = new DevExpress.XtraNavBar.NavBarItem();
            this.btnesatdosituacionfinanciera = new DevExpress.XtraNavBar.NavBarItem();
            this.btnestadodflujoefectivo = new DevExpress.XtraNavBar.NavBarItem();
            this.btnestadocambiopatrimonioneto = new DevExpress.XtraNavBar.NavBarItem();
            this.btnesatdofinancierosunat = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup13 = new DevExpress.XtraNavBar.NavBarGroup();
            this.btnestadosfianacierostributarios = new DevExpress.XtraNavBar.NavBarItem();
            this.btnestadofinansmv = new DevExpress.XtraNavBar.NavBarItem();
            this.btnflujoefectivo = new DevExpress.XtraNavBar.NavBarItem();
            this.btncambiospatrimonioneto = new DevExpress.XtraNavBar.NavBarItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.DefaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.comboperiodo = new System.Windows.Forms.ComboBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboanio = new System.Windows.Forms.ComboBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView2 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.txtempresa,
            this.lblusuario});
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Herramientas";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.txtempresa),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblusuario)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Herramientas";
            // 
            // txtempresa
            // 
            this.txtempresa.Caption = "Nombre de la empresa";
            this.txtempresa.Id = 0;
            this.txtempresa.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempresa.ItemAppearance.Normal.Options.UseFont = true;
            this.txtempresa.Name = "txtempresa";
            // 
            // lblusuario
            // 
            this.lblusuario.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblusuario.Caption = "Nombre del Usuario";
            this.lblusuario.Id = 1;
            this.lblusuario.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblusuario.ItemAppearance.Normal.Options.UseFont = true;
            this.lblusuario.Name = "lblusuario";
            this.lblusuario.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar3
            // 
            this.bar3.BarName = "Barra de estado";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Barra de estado";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.MainMenuAppearance.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.barAndDockingController1.AppearancesBar.MainMenuAppearance.Normal.Options.UseBackColor = true;
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1118, 21);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 560);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1118, 21);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 21);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 539);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(1118, 21);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Size = new System.Drawing.Size(0, 539);
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.navBarGroup2,
            this.navBarGroup3,
            this.navBarGroup4,
            this.Consultas,
            this.btnusers,
            this.navBarGroup5,
            this.navBarGroup6,
            this.navBarGroup7,
            this.navBarGroup8,
            this.navBarGroup9,
            this.navBarGroup10,
            this.navBarGroup11,
            this.navBarGroup12,
            this.navBarGroup13});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.btn_establecimiento,
            this.btngrupo,
            this.btnfamilia,
            this.btncompracomercial,
            this.btningresocomercial,
            this.btnpos,
            this.btnpuntoventa,
            this.bntpuntocompra,
            this.btncatalogo,
            this.btnalmacen,
            this.btndocumentospdv,
            this.btnseriecomprobante,
            this.btnprecios,
            this.btncaja,
            this.btnconsultabolfac,
            this.btnconsultasinticket,
            this.btnusuario,
            this.btnaccesos,
            this.btntraspasoscomercial,
            this.btnConsultaCajas,
            this.btnconsultastock,
            this.btnimpuesto,
            this.btnasignacionimporte,
            this.btntipocambio,
            this.btncuentacorriente,
            this.btnComprobantes,
            this.btnMonedas,
            this.btnlibroscontables,
            this.btnmediopago,
            this.btnentidadfinanciera,
            this.btndiario,
            this.btncomrpas,
            this.btnventas,
            this.btntesoreria,
            this.btncajachica,
            this.btnseriesdoccajachica,
            this.btnaperturacajachica,
            this.btnrendicion,
            this.btnreembolso,
            this.btncierre,
            this.btnempresas,
            this.btnejercicios,
            this.btnreplicacion,
            this.btninicio,
            this.btnelemento,
            this.btnestructura,
            this.btnplangeneral,
            this.btnplanempresarial,
            this.navBarItem2,
            this.btnbalancecomprobacion,
            this.btnregistrocompras,
            this.btnregistroventa,
            this.btnlibrodiario,
            this.btnlibromayor,
            this.btnigv,
            this.btnLibroSimplificado,
            this.btnesatdosituacionfinanciera,
            this.btnestadodflujoefectivo,
            this.btnestadocambiopatrimonioneto,
            this.btnesatdofinancierosunat,
            this.btnestadosfianacierostributarios,
            this.btnestadofinansmv,
            this.btnflujoefectivo,
            this.btncambiospatrimonioneto});
            this.navBarControl1.Location = new System.Drawing.Point(2, 2);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 244;
            this.navBarControl1.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.navBarControl1.Size = new System.Drawing.Size(244, 535);
            this.navBarControl1.SkinExplorerBarViewScrollStyle = DevExpress.XtraNavBar.SkinExplorerBarViewScrollStyle.ScrollBar;
            this.navBarControl1.TabIndex = 7;
            this.navBarControl1.Text = "Punto de venta";
            this.navBarControl1.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("DevExpress Style");
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Configuracion Comercial";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btn_establecimiento),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnpuntoventa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.bntpuntocompra),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnalmacen),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btndocumentospdv),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnseriecomprobante),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnprecios),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncaja)});
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // btn_establecimiento
            // 
            this.btn_establecimiento.Caption = "Establecimiento";
            this.btn_establecimiento.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btn_establecimiento.Name = "btn_establecimiento";
            this.btn_establecimiento.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btn_establecimiento_LinkClicked);
            // 
            // btnpuntoventa
            // 
            this.btnpuntoventa.Caption = "Punto de venta";
            this.btnpuntoventa.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnpuntoventa.Name = "btnpuntoventa";
            this.btnpuntoventa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnpuntoventa_LinkClicked);
            // 
            // bntpuntocompra
            // 
            this.bntpuntocompra.Caption = "Punto de compra";
            this.bntpuntocompra.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.bntpuntocompra.Name = "bntpuntocompra";
            this.bntpuntocompra.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.bntpuntocompra_LinkClicked);
            // 
            // btnalmacen
            // 
            this.btnalmacen.Caption = "Almacen";
            this.btnalmacen.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnalmacen.Name = "btnalmacen";
            this.btnalmacen.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnalmacen_LinkClicked);
            // 
            // btndocumentospdv
            // 
            this.btndocumentospdv.Caption = "Documentos por punto";
            this.btndocumentospdv.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btndocumentospdv.Name = "btndocumentospdv";
            this.btndocumentospdv.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btndocumentospdv_LinkClicked);
            // 
            // btnseriecomprobante
            // 
            this.btnseriecomprobante.Caption = "Series de comprobante ";
            this.btnseriecomprobante.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnseriecomprobante.Name = "btnseriecomprobante";
            this.btnseriecomprobante.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnseriecomprobante_LinkClicked);
            // 
            // btnprecios
            // 
            this.btnprecios.Caption = "Precios";
            this.btnprecios.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnprecios.Name = "btnprecios";
            this.btnprecios.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnprecios_LinkClicked);
            // 
            // btncaja
            // 
            this.btncaja.Caption = "Caja";
            this.btncaja.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncaja.Name = "btncaja";
            this.btncaja.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncaja_LinkClicked);
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Productos";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btngrupo),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnfamilia),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncatalogo)});
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // btngrupo
            // 
            this.btngrupo.Caption = "Grupo";
            this.btngrupo.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btngrupo.Name = "btngrupo";
            this.btngrupo.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btngrupo_LinkClicked);
            // 
            // btnfamilia
            // 
            this.btnfamilia.Caption = "Familia";
            this.btnfamilia.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnfamilia.Name = "btnfamilia";
            this.btnfamilia.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnfamilia_LinkClicked);
            // 
            // btncatalogo
            // 
            this.btncatalogo.Caption = "Catalogo";
            this.btncatalogo.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncatalogo.Name = "btncatalogo";
            this.btncatalogo.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncatalogo_LinkClicked);
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "Operaciones";
            this.navBarGroup3.Expanded = true;
            this.navBarGroup3.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncompracomercial),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btningresocomercial),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btntraspasoscomercial)});
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // btncompracomercial
            // 
            this.btncompracomercial.Caption = "Compras";
            this.btncompracomercial.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncompracomercial.Name = "btncompracomercial";
            this.btncompracomercial.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncompracomercial_LinkClicked);
            // 
            // btningresocomercial
            // 
            this.btningresocomercial.Caption = "Ingresos";
            this.btningresocomercial.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btningresocomercial.Name = "btningresocomercial";
            this.btningresocomercial.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btningresocomercial_LinkClicked);
            // 
            // btntraspasoscomercial
            // 
            this.btntraspasoscomercial.Caption = "Traspasos";
            this.btntraspasoscomercial.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btntraspasoscomercial.Name = "btntraspasoscomercial";
            this.btntraspasoscomercial.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btntraspasoscomercial_LinkClicked);
            // 
            // navBarGroup4
            // 
            this.navBarGroup4.Caption = "Venta";
            this.navBarGroup4.Expanded = true;
            this.navBarGroup4.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnpos)});
            this.navBarGroup4.Name = "navBarGroup4";
            // 
            // btnpos
            // 
            this.btnpos.Caption = "Punto de venta";
            this.btnpos.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnpos.Name = "btnpos";
            this.btnpos.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnpos_LinkClicked);
            // 
            // Consultas
            // 
            this.Consultas.Caption = "Consultas";
            this.Consultas.Expanded = true;
            this.Consultas.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnconsultabolfac),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnconsultasinticket),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnConsultaCajas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnconsultastock)});
            this.Consultas.Name = "Consultas";
            // 
            // btnconsultabolfac
            // 
            this.btnconsultabolfac.Caption = "Tickets Boleta - Factura";
            this.btnconsultabolfac.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnconsultabolfac.Name = "btnconsultabolfac";
            this.btnconsultabolfac.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnconsultabolfac_LinkClicked);
            // 
            // btnconsultasinticket
            // 
            this.btnconsultasinticket.Caption = "Ventas sin tickets";
            this.btnconsultasinticket.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnconsultasinticket.Name = "btnconsultasinticket";
            this.btnconsultasinticket.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnconsultasinticket_LinkClicked);
            // 
            // btnConsultaCajas
            // 
            this.btnConsultaCajas.Caption = "Consultas de Cajas";
            this.btnConsultaCajas.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnConsultaCajas.Name = "btnConsultaCajas";
            this.btnConsultaCajas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnConsultaCajas_LinkClicked);
            // 
            // btnconsultastock
            // 
            this.btnconsultastock.Caption = "Consulta Stock";
            this.btnconsultastock.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnconsultastock.Name = "btnconsultastock";
            this.btnconsultastock.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnconsultastock_LinkClicked);
            // 
            // btnusers
            // 
            this.btnusers.Caption = "Usuarios-Accesos";
            this.btnusers.Expanded = true;
            this.btnusers.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnusuario),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnaccesos)});
            this.btnusers.Name = "btnusers";
            // 
            // btnusuario
            // 
            this.btnusuario.Caption = "Usuarios";
            this.btnusuario.Name = "btnusuario";
            this.btnusuario.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnusuario_LinkClicked);
            // 
            // btnaccesos
            // 
            this.btnaccesos.Caption = "Accesos";
            this.btnaccesos.Name = "btnaccesos";
            this.btnaccesos.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnaccesos_LinkClicked);
            // 
            // navBarGroup5
            // 
            this.navBarGroup5.Caption = "Configuracion Contable";
            this.navBarGroup5.Expanded = true;
            this.navBarGroup5.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnimpuesto),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnasignacionimporte),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btntipocambio),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncuentacorriente)});
            this.navBarGroup5.Name = "navBarGroup5";
            // 
            // btnimpuesto
            // 
            this.btnimpuesto.Caption = "Tipo de Impuesto";
            this.btnimpuesto.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnimpuesto.Name = "btnimpuesto";
            this.btnimpuesto.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnimpuesto_LinkClicked);
            // 
            // btnasignacionimporte
            // 
            this.btnasignacionimporte.Caption = "Asignacion de Impuestos";
            this.btnasignacionimporte.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnasignacionimporte.Name = "btnasignacionimporte";
            this.btnasignacionimporte.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnasignacionimporte_LinkClicked);
            // 
            // btntipocambio
            // 
            this.btntipocambio.Caption = "Tipo de Cambio";
            this.btntipocambio.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btntipocambio.Name = "btntipocambio";
            this.btntipocambio.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btntipocambio_LinkClicked);
            // 
            // btncuentacorriente
            // 
            this.btncuentacorriente.Caption = "Cuenta corriente";
            this.btncuentacorriente.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncuentacorriente.Name = "btncuentacorriente";
            this.btncuentacorriente.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncuentacorriente_LinkClicked);
            // 
            // navBarGroup6
            // 
            this.navBarGroup6.Caption = "Tablas SUNAT";
            this.navBarGroup6.Expanded = true;
            this.navBarGroup6.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnComprobantes),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnMonedas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnlibroscontables),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnmediopago),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnentidadfinanciera)});
            this.navBarGroup6.Name = "navBarGroup6";
            // 
            // btnComprobantes
            // 
            this.btnComprobantes.Caption = "Comprobantes";
            this.btnComprobantes.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnComprobantes.Name = "btnComprobantes";
            this.btnComprobantes.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnComprobantes_LinkClicked);
            // 
            // btnMonedas
            // 
            this.btnMonedas.Caption = "Monedas";
            this.btnMonedas.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnMonedas.Name = "btnMonedas";
            this.btnMonedas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnMonedas_LinkClicked);
            // 
            // btnlibroscontables
            // 
            this.btnlibroscontables.Caption = "Libros Contables";
            this.btnlibroscontables.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnlibroscontables.Name = "btnlibroscontables";
            this.btnlibroscontables.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnlibroscontables_LinkClicked);
            // 
            // btnmediopago
            // 
            this.btnmediopago.Caption = "Medio de Pago";
            this.btnmediopago.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnmediopago.Name = "btnmediopago";
            this.btnmediopago.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnmediopago_LinkClicked);
            // 
            // btnentidadfinanciera
            // 
            this.btnentidadfinanciera.Caption = "Entidad Financiera";
            this.btnentidadfinanciera.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnentidadfinanciera.Name = "btnentidadfinanciera";
            this.btnentidadfinanciera.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnentidadfinanciera_LinkClicked);
            // 
            // navBarGroup7
            // 
            this.navBarGroup7.Caption = "Movimientos Contables";
            this.navBarGroup7.Expanded = true;
            this.navBarGroup7.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btndiario),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncomrpas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnventas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btntesoreria)});
            this.navBarGroup7.Name = "navBarGroup7";
            // 
            // btndiario
            // 
            this.btndiario.Caption = "Diario";
            this.btndiario.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btndiario.Name = "btndiario";
            this.btndiario.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btndiario_LinkClicked);
            // 
            // btncomrpas
            // 
            this.btncomrpas.Caption = "Compras";
            this.btncomrpas.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncomrpas.Name = "btncomrpas";
            this.btncomrpas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncomrpas_LinkClicked);
            // 
            // btnventas
            // 
            this.btnventas.Caption = "Ventas";
            this.btnventas.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnventas.Name = "btnventas";
            this.btnventas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnventas_LinkClicked);
            // 
            // btntesoreria
            // 
            this.btntesoreria.Caption = "Tesoreria";
            this.btntesoreria.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btntesoreria.Name = "btntesoreria";
            this.btntesoreria.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btntesoreria_LinkClicked);
            // 
            // navBarGroup8
            // 
            this.navBarGroup8.Caption = "Caja Chica Contable";
            this.navBarGroup8.Expanded = true;
            this.navBarGroup8.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncajachica),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnseriesdoccajachica),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnaperturacajachica),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnrendicion),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnreembolso),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncierre)});
            this.navBarGroup8.Name = "navBarGroup8";
            // 
            // btncajachica
            // 
            this.btncajachica.Caption = "Caja chica";
            this.btncajachica.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncajachica.Name = "btncajachica";
            this.btncajachica.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncajachica_LinkClicked);
            // 
            // btnseriesdoccajachica
            // 
            this.btnseriesdoccajachica.Caption = "Series de documentos";
            this.btnseriesdoccajachica.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnseriesdoccajachica.Name = "btnseriesdoccajachica";
            this.btnseriesdoccajachica.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnseriesdoccajachica_LinkClicked);
            // 
            // btnaperturacajachica
            // 
            this.btnaperturacajachica.Caption = "Apertura de Caja chica";
            this.btnaperturacajachica.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnaperturacajachica.Name = "btnaperturacajachica";
            this.btnaperturacajachica.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnaperturacajachica_LinkClicked);
            // 
            // btnrendicion
            // 
            this.btnrendicion.Caption = "Rendicion";
            this.btnrendicion.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnrendicion.Name = "btnrendicion";
            this.btnrendicion.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnrendicion_LinkClicked);
            // 
            // btnreembolso
            // 
            this.btnreembolso.Caption = "Reembolso";
            this.btnreembolso.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnreembolso.Name = "btnreembolso";
            this.btnreembolso.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnreembolso_LinkClicked);
            // 
            // btncierre
            // 
            this.btncierre.Caption = "Cierre";
            this.btncierre.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncierre.Name = "btncierre";
            this.btncierre.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncierre_LinkClicked);
            // 
            // navBarGroup9
            // 
            this.navBarGroup9.Caption = "Configuracion General";
            this.navBarGroup9.Expanded = true;
            this.navBarGroup9.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnempresas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnejercicios),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnreplicacion),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btninicio)});
            this.navBarGroup9.Name = "navBarGroup9";
            // 
            // btnempresas
            // 
            this.btnempresas.Caption = "Empresas";
            this.btnempresas.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnempresas.Name = "btnempresas";
            this.btnempresas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnempresas_LinkClicked);
            // 
            // btnejercicios
            // 
            this.btnejercicios.Caption = "Ejercicios";
            this.btnejercicios.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnejercicios.Name = "btnejercicios";
            this.btnejercicios.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnejercicios_LinkClicked);
            // 
            // btnreplicacion
            // 
            this.btnreplicacion.Caption = "Replicacion";
            this.btnreplicacion.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnreplicacion.Name = "btnreplicacion";
            this.btnreplicacion.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnreplicacion_LinkClicked);
            // 
            // btninicio
            // 
            this.btninicio.Caption = "Inicio";
            this.btninicio.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btninicio.Name = "btninicio";
            this.btninicio.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btninicio_LinkClicked);
            // 
            // navBarGroup10
            // 
            this.navBarGroup10.Caption = "Plan General";
            this.navBarGroup10.Expanded = true;
            this.navBarGroup10.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnelemento),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnestructura),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnplangeneral)});
            this.navBarGroup10.Name = "navBarGroup10";
            // 
            // btnelemento
            // 
            this.btnelemento.Caption = "Elemento";
            this.btnelemento.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnelemento.Name = "btnelemento";
            this.btnelemento.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnelemento_LinkClicked);
            // 
            // btnestructura
            // 
            this.btnestructura.Caption = "Estructura";
            this.btnestructura.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnestructura.Name = "btnestructura";
            this.btnestructura.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnestructura_LinkClicked);
            // 
            // btnplangeneral
            // 
            this.btnplangeneral.Caption = "Plan contable general";
            this.btnplangeneral.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnplangeneral.Name = "btnplangeneral";
            this.btnplangeneral.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnplangeneral_LinkClicked);
            // 
            // navBarGroup11
            // 
            this.navBarGroup11.Caption = "Plan Contable Empresarial";
            this.navBarGroup11.Expanded = true;
            this.navBarGroup11.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnplanempresarial),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2)});
            this.navBarGroup11.Name = "navBarGroup11";
            // 
            // btnplanempresarial
            // 
            this.btnplanempresarial.Caption = "Plan Empresarial";
            this.btnplanempresarial.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnplanempresarial.Name = "btnplanempresarial";
            this.btnplanempresarial.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnplanempresarial_LinkClicked);
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Analisis Contable";
            this.navBarItem2.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnanalisiscontable);
            // 
            // navBarGroup12
            // 
            this.navBarGroup12.Caption = "Reportes Contables";
            this.navBarGroup12.Expanded = true;
            this.navBarGroup12.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnbalancecomprobacion),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnregistrocompras),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnregistroventa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnlibrodiario),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnlibromayor),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnigv),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnLibroSimplificado),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnesatdosituacionfinanciera),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnestadodflujoefectivo),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnestadocambiopatrimonioneto),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnesatdofinancierosunat)});
            this.navBarGroup12.Name = "navBarGroup12";
            // 
            // btnbalancecomprobacion
            // 
            this.btnbalancecomprobacion.Caption = "Balance comprobacion";
            this.btnbalancecomprobacion.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnbalancecomprobacion.Name = "btnbalancecomprobacion";
            this.btnbalancecomprobacion.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnbalancecomprobacion_LinkClicked);
            // 
            // btnregistrocompras
            // 
            this.btnregistrocompras.Caption = "Registro de compras";
            this.btnregistrocompras.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnregistrocompras.Name = "btnregistrocompras";
            this.btnregistrocompras.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnregistrocompras_LinkClicked);
            // 
            // btnregistroventa
            // 
            this.btnregistroventa.Caption = "Registro de ventas";
            this.btnregistroventa.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnregistroventa.Name = "btnregistroventa";
            this.btnregistroventa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnregistroventa_LinkClicked);
            // 
            // btnlibrodiario
            // 
            this.btnlibrodiario.Caption = "Libro diario";
            this.btnlibrodiario.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnlibrodiario.Name = "btnlibrodiario";
            this.btnlibrodiario.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnlibrodiario_LinkClicked);
            // 
            // btnlibromayor
            // 
            this.btnlibromayor.Caption = "Libro mayor";
            this.btnlibromayor.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnlibromayor.Name = "btnlibromayor";
            this.btnlibromayor.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnlibromayor_LinkClicked);
            // 
            // btnigv
            // 
            this.btnigv.Caption = "I.G.V.";
            this.btnigv.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnigv.Name = "btnigv";
            this.btnigv.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnigv_LinkClicked);
            // 
            // btnLibroSimplificado
            // 
            this.btnLibroSimplificado.Caption = "Libro diario simplificado";
            this.btnLibroSimplificado.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnLibroSimplificado.Name = "btnLibroSimplificado";
            this.btnLibroSimplificado.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnLibroSimplificado_LinkClicked);
            // 
            // btnesatdosituacionfinanciera
            // 
            this.btnesatdosituacionfinanciera.Caption = "Estado de Situacion Financiera";
            this.btnesatdosituacionfinanciera.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnesatdosituacionfinanciera.Name = "btnesatdosituacionfinanciera";
            this.btnesatdosituacionfinanciera.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnesatdosituacionfinanciera_LinkClicked);
            // 
            // btnestadodflujoefectivo
            // 
            this.btnestadodflujoefectivo.Caption = "Estado de Flujo de Efectivo";
            this.btnestadodflujoefectivo.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnestadodflujoefectivo.Name = "btnestadodflujoefectivo";
            this.btnestadodflujoefectivo.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnestadodflujoefectivo_LinkClicked);
            // 
            // btnestadocambiopatrimonioneto
            // 
            this.btnestadocambiopatrimonioneto.Caption = "Estado de Cambios en el Patrimonio Neto";
            this.btnestadocambiopatrimonioneto.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnestadocambiopatrimonioneto.Name = "btnestadocambiopatrimonioneto";
            this.btnestadocambiopatrimonioneto.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnestadocambiopatrimonioneto_LinkClicked);
            // 
            // btnesatdofinancierosunat
            // 
            this.btnesatdofinancierosunat.Caption = "Estado Financiero Sunat";
            this.btnesatdofinancierosunat.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnesatdofinancierosunat.Name = "btnesatdofinancierosunat";
            this.btnesatdofinancierosunat.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnesatdofinancierosunat_LinkClicked);
            // 
            // navBarGroup13
            // 
            this.navBarGroup13.Caption = "Formatos Estados Financieros";
            this.navBarGroup13.Expanded = true;
            this.navBarGroup13.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnestadosfianacierostributarios),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnestadofinansmv),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btnflujoefectivo),
            new DevExpress.XtraNavBar.NavBarItemLink(this.btncambiospatrimonioneto)});
            this.navBarGroup13.Name = "navBarGroup13";
            // 
            // btnestadosfianacierostributarios
            // 
            this.btnestadosfianacierostributarios.Caption = "Estados Financieros Tributarios";
            this.btnestadosfianacierostributarios.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnestadosfianacierostributarios.Name = "btnestadosfianacierostributarios";
            this.btnestadosfianacierostributarios.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnestadosfianacierostributarios_LinkClicked);
            // 
            // btnestadofinansmv
            // 
            this.btnestadofinansmv.Caption = "Estado Financiero SMW";
            this.btnestadofinansmv.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnestadofinansmv.Name = "btnestadofinansmv";
            this.btnestadofinansmv.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnestadofinansmv_LinkClicked);
            // 
            // btnflujoefectivo
            // 
            this.btnflujoefectivo.Caption = "Formato Flujo de efectivo";
            this.btnflujoefectivo.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btnflujoefectivo.Name = "btnflujoefectivo";
            this.btnflujoefectivo.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btnflujoefectivo_LinkClicked);
            // 
            // btncambiospatrimonioneto
            // 
            this.btncambiospatrimonioneto.Caption = "Estados de Cambio Patrimonio Neto";
            this.btncambiospatrimonioneto.ImageOptions.SmallImage = global::Contable.Properties.Resources.pincomercial16;
            this.btncambiospatrimonioneto.Name = "btncambiospatrimonioneto";
            this.btncambiospatrimonioneto.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.btncambiospatrimonioneto_LinkClicked);
            // 
            // dockManager1
            // 
            this.dockManager1.Controller = this.barAndDockingController1;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // comboperiodo
            // 
            this.comboperiodo.FormattingEnabled = true;
            this.comboperiodo.Location = new System.Drawing.Point(517, 3);
            this.comboperiodo.Name = "comboperiodo";
            this.comboperiodo.Size = new System.Drawing.Size(121, 21);
            this.comboperiodo.TabIndex = 11;
            this.comboperiodo.SelectedIndexChanged += new System.EventHandler(this.comboperiodo_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(109)))), ((int)(((byte)(156)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(469, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 14);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Periodo:";
            // 
            // comboanio
            // 
            this.comboanio.FormattingEnabled = true;
            this.comboanio.Location = new System.Drawing.Point(318, 3);
            this.comboanio.Name = "comboanio";
            this.comboanio.Size = new System.Drawing.Size(147, 21);
            this.comboanio.TabIndex = 9;
            this.comboanio.SelectedIndexChanged += new System.EventHandler(this.comboanio_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(109)))), ((int)(((byte)(156)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(287, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(26, 14);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Año:";
            // 
            // documentManager1
            // 
            this.documentManager1.MdiParent = this;
            this.documentManager1.MenuManager = this.barManager1;
            this.documentManager1.View = this.tabbedView2;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView2,
            this.tabbedView1});
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.navBarControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 21);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(248, 539);
            this.panelControl1.TabIndex = 22;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitterControl1.Location = new System.Drawing.Point(248, 21);
            this.splitterControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(10, 539);
            this.splitterControl1.TabIndex = 23;
            this.splitterControl1.TabStop = false;
            // 
            // frm_main_master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 581);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.comboperiodo);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.comboanio);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IsMdiContainer = true;
            this.Name = "frm_main_master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.frm_main_master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem txtempresa;
        public DevExpress.XtraBars.BarStaticItem lblusuario;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarItem btngrupo;
        private DevExpress.XtraNavBar.NavBarItem btnfamilia;
        private DevExpress.XtraNavBar.NavBarItem btncatalogo;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem btn_establecimiento;
        private DevExpress.XtraNavBar.NavBarItem btnpuntoventa;
        private DevExpress.XtraNavBar.NavBarItem bntpuntocompra;
        private DevExpress.XtraNavBar.NavBarItem btnalmacen;
        private DevExpress.XtraNavBar.NavBarItem btndocumentospdv;
        private DevExpress.XtraNavBar.NavBarItem btnseriecomprobante;
        private DevExpress.XtraNavBar.NavBarItem btnprecios;
        private DevExpress.XtraNavBar.NavBarItem btncaja;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarItem btncompracomercial;
        private DevExpress.XtraNavBar.NavBarItem btningresocomercial;
        private DevExpress.XtraNavBar.NavBarItem btntraspasoscomercial;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup4;
        private DevExpress.XtraNavBar.NavBarItem btnpos;
        private DevExpress.XtraNavBar.NavBarGroup Consultas;
        private DevExpress.XtraNavBar.NavBarItem btnconsultabolfac;
        private DevExpress.XtraNavBar.NavBarItem btnconsultasinticket;
        private DevExpress.XtraNavBar.NavBarItem btnConsultaCajas;
        private DevExpress.XtraNavBar.NavBarItem btnconsultastock;
        private DevExpress.XtraNavBar.NavBarGroup btnusers;
        private DevExpress.XtraNavBar.NavBarItem btnusuario;
        private DevExpress.XtraNavBar.NavBarItem btnaccesos;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        internal DevExpress.LookAndFeel.DefaultLookAndFeel DefaultLookAndFeel1;
        public System.Windows.Forms.ComboBox comboperiodo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public System.Windows.Forms.ComboBox comboanio;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup5;
        private DevExpress.XtraNavBar.NavBarItem btnimpuesto;
        private DevExpress.XtraNavBar.NavBarItem btnasignacionimporte;
        private DevExpress.XtraNavBar.NavBarItem btntipocambio;
        private DevExpress.XtraNavBar.NavBarItem btncuentacorriente;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup6;
        private DevExpress.XtraNavBar.NavBarItem btnComprobantes;
        private DevExpress.XtraNavBar.NavBarItem btnMonedas;
        private DevExpress.XtraNavBar.NavBarItem btnlibroscontables;
        private DevExpress.XtraNavBar.NavBarItem btnmediopago;
        private DevExpress.XtraNavBar.NavBarItem btnentidadfinanciera;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup7;
        private DevExpress.XtraNavBar.NavBarItem btndiario;
        private DevExpress.XtraNavBar.NavBarItem btncomrpas;
        private DevExpress.XtraNavBar.NavBarItem btnventas;
        private DevExpress.XtraNavBar.NavBarItem btntesoreria;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup8;
        private DevExpress.XtraNavBar.NavBarItem btncajachica;
        private DevExpress.XtraNavBar.NavBarItem btnseriesdoccajachica;
        private DevExpress.XtraNavBar.NavBarItem btnaperturacajachica;
        private DevExpress.XtraNavBar.NavBarItem btnrendicion;
        private DevExpress.XtraNavBar.NavBarItem btnreembolso;
        private DevExpress.XtraNavBar.NavBarItem btncierre;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup9;
        private DevExpress.XtraNavBar.NavBarItem btnempresas;
        private DevExpress.XtraNavBar.NavBarItem btnejercicios;
        private DevExpress.XtraNavBar.NavBarItem btnreplicacion;
        private DevExpress.XtraNavBar.NavBarItem btninicio;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup10;
        private DevExpress.XtraNavBar.NavBarItem btnelemento;
        private DevExpress.XtraNavBar.NavBarItem btnestructura;
        private DevExpress.XtraNavBar.NavBarItem btnplangeneral;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup11;
        private DevExpress.XtraNavBar.NavBarItem btnplanempresarial;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup12;
        private DevExpress.XtraNavBar.NavBarItem btnbalancecomprobacion;
        private DevExpress.XtraNavBar.NavBarItem btnregistrocompras;
        private DevExpress.XtraNavBar.NavBarItem btnregistroventa;
        private DevExpress.XtraNavBar.NavBarItem btnlibrodiario;
        private DevExpress.XtraNavBar.NavBarItem btnlibromayor;
        private DevExpress.XtraNavBar.NavBarItem btnigv;
        private DevExpress.XtraNavBar.NavBarItem btnLibroSimplificado;
        private DevExpress.XtraNavBar.NavBarItem btnesatdosituacionfinanciera;
        private DevExpress.XtraNavBar.NavBarItem btnestadodflujoefectivo;
        private DevExpress.XtraNavBar.NavBarItem btnestadocambiopatrimonioneto;
        private DevExpress.XtraNavBar.NavBarItem btnesatdofinancierosunat;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup13;
        private DevExpress.XtraNavBar.NavBarItem btnestadosfianacierostributarios;
        private DevExpress.XtraNavBar.NavBarItem btnestadofinansmv;
        private DevExpress.XtraNavBar.NavBarItem btnflujoefectivo;
        private DevExpress.XtraNavBar.NavBarItem btncambiospatrimonioneto;
    }
}