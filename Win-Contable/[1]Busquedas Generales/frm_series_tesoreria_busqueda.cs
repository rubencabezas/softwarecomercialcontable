﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_series_tesoreria_busqueda : Contable.frm_fuente
    {
        public frm_series_tesoreria_busqueda()
        {
            InitializeComponent();
        }

        public string Id_Empresa, Cta_Corriente,Tipo_Doc, Serie;
        public string Tipo_IE, Tipo_mov;

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\SeriesTesoreriaBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        public List<Entidad_Series_Tesorerira> Lista = new List<Entidad_Series_Tesorerira>();
        public void Listar()
        {
            Entidad_Series_Tesorerira Ent = new Entidad_Series_Tesorerira();
            Logica_Series_Tesoreria log = new Logica_Series_Tesoreria();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //Ent.Ser_Cta_Corriente = Cta_Corriente;
            //Ent.Ser_Tipo_Doc = Tipo_Doc;
            Ent.Ser_Tip_IE = Tipo_IE;
            Ent.Ser_Tipo_Mov = Tipo_mov;


            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_series_tesoreria_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
