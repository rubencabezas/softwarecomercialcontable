﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_centro_costo_gasto_busqueda : Contable.frm_fuente
    {
        public frm_centro_costo_gasto_busqueda()
        {
            InitializeComponent();
        }

        public List<Entidad_Centro_Costo_Gasto> Lista = new List<Entidad_Centro_Costo_Gasto>();
        public void Listar()
        {
            Entidad_Centro_Costo_Gasto Ent = new Entidad_Centro_Costo_Gasto();
            Logica_Centro_Costo_Gasto log = new Logica_Centro_Costo_Gasto();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_EditorKeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void frm_centro_costo_gasto_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\CentroCostogastoBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
