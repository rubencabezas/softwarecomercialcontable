﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_unidad_medida_busqueda : frm_fuente
    {
        public frm_unidad_medida_busqueda()
        {
            InitializeComponent();
        }

        public string unmcod;
        public string Producto_Cod;
        public bool Producto_Unm;
        public string tipo;

        public List<Entidad_Unidad_Medida> Lista = new List<Entidad_Unidad_Medida>();
        public void Listar()
        {
            Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
            Logica_Unidad_Medida log = new Logica_Unidad_Medida();

            Ent.Id_Unidad_Medida = unmcod;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Listar_Producto_Unm()
        {
            Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
            Logica_Unidad_Medida log = new Logica_Unidad_Medida();
            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Unidad_Medida = unmcod;
            Ent.Id_Catalogo = Producto_Cod;
            Ent.Id_Tipo = tipo;
            //Ent.Unm_Defecto = null;
            try
            {
                Lista = log.Listar_Cat_Unm(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccionar.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccionar.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void frm_unidad_medida_busqueda_Load(object sender, EventArgs e)
        {
            try
            {
      if (Producto_Unm)
            {
                Listar_Producto_Unm();
            }
            else
            {
                 Listar();
            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
      
          
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_unidad_medida_edicion f = new frm_unidad_medida_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
                    Logica_Unidad_Medida Log = new Logica_Unidad_Medida();

                    Ent.Und_Descripcion = f.txtdescripcion.Text;
                    Ent.Und_Abreviado = f.txtabreviado.Text;
                    Ent.Und_SunaT = f.txtsunat.Text.Trim();
                    Ent.UnmFE = f.txtunmFE.Text.Trim();

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();
                                Accion.ExitoGuardar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        private void btnmmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_unidad_medida_edicion f = new frm_unidad_medida_edicion())
            {

                f.txtdescripcion.Tag = Entidad.Id_Unidad_Medida;
                f.txtdescripcion.Text = Entidad.Und_Descripcion;
                f.txtabreviado.Text = Entidad.Und_Abreviado;
                f.txtsunat.Text = Entidad.Und_SunaT;
                f.txtunmFE.Text = Entidad.UnmFE;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Unidad_Medida Ent = new Entidad_Unidad_Medida();
                    Logica_Unidad_Medida Log = new Logica_Unidad_Medida();


                    Ent.Id_Unidad_Medida = f.txtdescripcion.Tag.ToString();
                    Ent.Und_Descripcion = f.txtdescripcion.Text.Trim();
                    Ent.Und_Abreviado = f.txtabreviado.Text.Trim();
                    Ent.Und_SunaT = f.txtsunat.Text.Trim();
                    Ent.UnmFE = f.txtunmFE.Text.Trim();

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                                Accion.ExitoModificar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();
        private void gridView1_Click(object sender, EventArgs e)
        {
            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }
    }
}
