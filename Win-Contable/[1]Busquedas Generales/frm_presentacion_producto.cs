﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial.Busquedas_Generales
{
    public partial class frm_presentacion_producto : frm_fuente
    {
        public frm_presentacion_producto()
        {
            InitializeComponent();
        }

        public string Id_Almacen;
        public string BSIFecha;

        List<Entidad_Lista_Precio> Lista_Precio = new List<Entidad_Lista_Precio>();
        List<Entidad_Catalogo> ListaFiltrada = new List<Entidad_Catalogo>();

        public List<Entidad_Catalogo> ListaFiltrada_Por_Presentacion = new List<Entidad_Catalogo>();

        public Decimal Igv_Porcentaje = 0;
        bool Es_Precio_Variable = false;
        public string Establecimiento;

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(BSIFecha) == true))
                {
                    if ((DateTime.Parse(BSIFecha).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(BSIFecha);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            Igv_Porcentaje = Lista[0].Imd_Tasa;
                            //Igv_Tasa_texto = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));
                            //Igv_TasaImpresion = Lista[0].Imd_Tasa_Impresion;
                            //LblIGV.Text = "I.G.V." + "(" + Math.Round(Lista[0].Imd_Tasa * 100, 2) + "):";
                            //LblIGV.Tag = Lista[0].Imd_Tasa;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }



        private void frm_presentacion_producto_Load(object sender, EventArgs e)
        {
            try
            {
                Entidad_Catalogo Producto = new Entidad_Catalogo();
                Logica_Catalogo Log = new Logica_Catalogo();
                Producto.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Producto.Id_Anio = Actual_Conexion.AnioSelect;
                Producto.Id_Tipo = "0031";//BIEN
                Producto.Id_Almacen = Id_Almacen;
                Producto.Id_Catalogo = ListaFiltrada_Por_Presentacion[0].Id_Catalogo;

                ListaFiltrada = Log.Consultar_Stock_Lote(Producto);

                lblproducto.Text = ListaFiltrada[0].Cat_Descripcion.Trim();

                if (ListaFiltrada[0].Acepta_Lote)
                {
                    btnanadir.Text = "BUSCAR LOTE";
                }

                if (ListaFiltrada_Por_Presentacion.Count == 1)
                {
                    dgvdatospresentacion.DataSource = ListaFiltrada_Por_Presentacion;
                    btnanadir.PerformClick();
                }
                else
                {
                 dgvdatospresentacion.DataSource = ListaFiltrada_Por_Presentacion;
                }
         
            }
            catch (Exception ex)
            {

            }


        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bandedGridView1.RowCount > 0)
            {
                              DialogResult = DialogResult.OK;
            }
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void dgvdetalle_DoubleClick(object sender, EventArgs e)
        {
            if (bandedGridView1.RowCount > 0)
            {
                btnseleccion.PerformClick();
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void bandedGridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (ListaFiltrada_Por_Presentacion.Count > 0 & bandedGridView1.GetFocusedDataSourceRowIndex() > -1)
                {

                    //Buscamos El tipo de Operacion Por Producto para definir Si Esta afecto u Otro Regimen del IGV
                    Entidad_Operaciones oper = new Entidad_Operaciones();
                    Logica_Operaciones logoper = new Logica_Operaciones();
                    List<Entidad_Operaciones> operlist = new List<Entidad_Operaciones>();
                    oper.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    oper.Id_Tipo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;// item.Adm_Tipo_BSA;
                    oper.Id_Catalogo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;// item.Adm_Catalogo;
                    operlist = logoper.Traer_Operacion_venta_Por_Producto(oper);
                    ////////////////////
                    ///
                    Buscar_Precio_por_presentacion(operlist[0].AfecIGV_Tabla);
                }


            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        void Buscar_Precio_por_presentacion(string DvtD_OperCodigo)
        {
            try
            {


                if (DvtD_OperCodigo == "0565")
                {
                    ActualizaIGV();
                }
                else
                {
                    Igv_Porcentaje = 0;
                }

                Logica_Lista_Precio log = new Logica_Lista_Precio();
                Entidad_Lista_Precio ent = new Entidad_Lista_Precio();


                ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                ent.Est_Codigo = Establecimiento;
                ent.Id_Tipo = "0031";//BIEN
                ent.Id_Catalogo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;
                ent.Pre_Tipo = "0006";//CONTADO
                ent.Pre_Id_Unm = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                ent.Pre_Moneda_cod = "001"; //SOLES
                ent.Pre_Fecha_Ini = Convert.ToDateTime(BSIFecha);
                //Lista_Precio = log.Traer_Precio_Procuto_Presentacion(ent);
                Lista_Precio = log.Traer_Precio_Procuto_Presentacion_Por_Unm(ent);

                Es_Precio_Variable = false;

                if (Lista_Precio.Count > 0)
                {
                    Es_Precio_Variable = Lista_Precio[0].Es_Precio_Variable;

                    if (Es_Precio_Variable == true)
                    {

                        //===============================
                        btncodAutoriza.Enabled = true;

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4, MidpointRounding.AwayFromZero) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                        txtprecminimo.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));

                        decimal IgvPrecMaximo = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo),2, MidpointRounding.AwayFromZero) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);

                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));

                        txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));

                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecfinal.Enabled = false;



                        Calcula_Importe_Final();

                        txtprecfinal.Focus();
                    }
                    else
                    {
                        btncodAutoriza.Enabled = false;

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4, MidpointRounding.AwayFromZero) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);

                        decimal PrecMin = Math.Round(Lista_Precio[0].Pre_Precio_Minimo,2, MidpointRounding.AwayFromZero);

                        txtprecminimo.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                        txtprecmaximo.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                        txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));

                        txtprecunitario.Text = txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecfinal.Enabled = false;

                        Calcula_Importe_Final();

                    }


                }
                else
                {
                    Accion.Advertencia("Debe configurar un precio para este producto.");
                    txtprecfinal.Enabled = false;
                    btncodAutoriza.Enabled = false;



                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        void Calcula_Importe_Final()
        {
            try
            {
                if (txtcantidad.Text != "")
                {
                    //  decimal Precio_Final = Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text);
                    decimal Precio_Final = Convert.ToDecimal(txtprecfinal.Text);
                    txtimporte.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtcantidad.Text) * Precio_Final, 2));


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcantidad_Validating(object sender, CancelEventArgs e)
        {
            Calcula_Importe_Final();
        }

        Entidad_Catalogo Entidad = new Entidad_Catalogo();
        List<Entidad_Catalogo> Lista_Lote_Producto = new List<Entidad_Catalogo>();
        Entidad_Catalogo Entidad_Indice_Lote = new Entidad_Catalogo();
        public List<Entidad_Catalogo> Lista_Pre_Seleccionada = new List<Entidad_Catalogo>();

        private void btnanadir_Click(object sender, EventArgs e)
        {
            try
            {

 
                    if (txtcantidad.Text != "")
                    {

                        if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
                        {
                            Accion.Advertencia("El precio de venta final no debe ser menor al precio venta ");

                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
                            Calcula_Importe_Final();
                            txtprecfinal.Select();
                            return;
                        }
                        else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
                        {
                            Accion.Advertencia("El precio base final no debe ser mayor al valor máximo");
                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
                            Calcula_Importe_Final();
                            txtprecfinal.Focus();
                            txtprecfinal.Select();
                            return;
                        }

                    }

                    bool Acepta_Lotes = false;
                    DateTime Lot_FechaFabricacion;
                    DateTime Lot_FechaVencimiento;

                    if (ListaFiltrada[0].Acepta_Lote == true)
                    {
                        // Buscamos si tiene mas de un lote

                        Logica_Catalogo log = new Logica_Catalogo();
                        Entidad_Catalogo pre = new Entidad_Catalogo();

                        Entidad = ListaFiltrada[0];

                        pre.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        pre.Id_Anio = Actual_Conexion.AnioSelect;
                        pre.Id_Almacen = Id_Almacen;
                        pre.Id_Tipo = "0031";//BIEN
                        pre.Id_Catalogo = Entidad.Id_Catalogo;

                        Lista_Lote_Producto = log.Consultar_Stock_Producto_Lote(pre);

                        // Si el producto tiene un solo lote , debe cargar defrente
                        // Si tiene mas de uno debe abrirse una ventana para qeu pueda escoger
                        if (Lista_Lote_Producto.Count >0)
                   
                        {
                            //Muestra una ventana para escoger que lote dar salida
                            using (frm_producto_stock_lote_varios f = new frm_producto_stock_lote_varios())
                            {
                                f.Lista_Lote_Producto = Lista_Lote_Producto;
                                f.txtproducto.Caption = ListaFiltrada[0].Cat_Descripcion;

                                // SI TIENE MAS DE UN LOTE

                                if (f.ShowDialog() == DialogResult.OK)
                                {
                                    Entidad_Indice_Lote = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()];
                                    //DialogResult = DialogResult.OK;
                                    lbllote.Text = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()].Lot_Lote;
                                    Acepta_Lotes = true;
                                    Lot_FechaFabricacion = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()].Lot_FechaFabricacion;
                                    Lot_FechaVencimiento = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()].Lot_FechaVencimiento;

                                    //AQUI SE AGREGA 

                                    Entidad_Catalogo ItemDetalle = new Entidad_Catalogo();

                                    ItemDetalle.Id_Item = Lista_Pre_Seleccionada.Count() + 1;

                                    ItemDetalle.Id_Catalogo = ListaFiltrada[0].Id_Catalogo;
                                    ItemDetalle.Cat_Descripcion = ListaFiltrada[0].Cat_Descripcion;


                                    ItemDetalle.Id_Tipo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;
                                    ItemDetalle.Unm_Cod_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                                    ItemDetalle.Unm_Desc_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Desc_Det;
                                    ItemDetalle.Und_Abreviado = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Und_Abreviado;
                                    ItemDetalle.Unm_FE = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_FE;

                                    ItemDetalle.Acepta_Lote = Acepta_Lotes;
                                    ItemDetalle.Lot_Lote = lbllote.Text;
                                    ItemDetalle.Lot_FechaFabricacion = Lot_FechaFabricacion;
                                    ItemDetalle.Lot_FechaVencimiento = Lot_FechaVencimiento;


                                    ItemDetalle.Adm_Valor_Venta = Convert.ToDecimal(txtprecmaximo.Text);
                                    ItemDetalle.Adm_Valor_Unit = Convert.ToDecimal(txtprecfinal.Text);

                                    ItemDetalle.Cantidad = Convert.ToDecimal(txtcantidad.Text);
                                    ItemDetalle.PrecUnit = Convert.ToDecimal(txtprecunitario.Text);
                                    ItemDetalle.Total = Convert.ToDecimal(txtimporte.Text);

                                    ItemDetalle.Unm_Base = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;
                                    ItemDetalle.Invd_CantidadAtendidaEnBaseAlCoefciente = Convert.ToDecimal(txtcantidad.Text) / ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;

                                    ItemDetalle.Stock = ListaFiltrada[0].Stock;

                                    if (ExistenOtrosProd_Contado_PorItem(ItemDetalle.Id_Catalogo, ItemDetalle.Unm_Cod_Det, ItemDetalle.Lot_Lote))
                                    {
                                        Lista_Pre_Seleccionada[i_con].Cantidad = Lista_Pre_Seleccionada[i_con].Cantidad + ItemDetalle.Cantidad;
                                        Lista_Pre_Seleccionada[i_con].Total = Math.Round(ItemDetalle.PrecUnit, 2) * Lista_Pre_Seleccionada[i_con].Cantidad;

                                    }
                                    else
                                    {
                                        Lista_Pre_Seleccionada.Add(ItemDetalle);
                                    }

                                 //   UpdateGrilla();

                                    lbllote.ResetText();
                                    Acepta_Lotes = false;


                                if (Lista_Pre_Seleccionada.Count() > 0)
                                {
                                    DialogResult = DialogResult.OK;
                                }
                                else
                                {
                                    Accion.Advertencia("No existen productos agregados en la lista.");
                                }

                            }
                            }
                        }

                    }
                    else
                    {
                        //DialogResult = DialogResult.OK;
                        //AQUI SE AGREGA 

                        Entidad_Catalogo ItemDetalle = new Entidad_Catalogo();

                        ItemDetalle.Id_Item = Lista_Pre_Seleccionada.Count() + 1;



                        ItemDetalle.Id_Catalogo = ListaFiltrada[0].Id_Catalogo;
                        ItemDetalle.Cat_Descripcion = ListaFiltrada[0].Cat_Descripcion;

                        ItemDetalle.Id_Tipo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;
                        ItemDetalle.Unm_Cod_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                        ItemDetalle.Unm_Desc_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Desc_Det;
                        ItemDetalle.Und_Abreviado = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Und_Abreviado;
                        ItemDetalle.Unm_FE = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_FE;

                        ItemDetalle.Lot_Lote = lbllote.Text;
                        ItemDetalle.Acepta_Lote = Acepta_Lotes;
                        //ItemDetalle.Lot_FechaFabricacion = Lot_FechaFabricacion;
                        //ItemDetalle.Lot_FechaVencimiento = Lot_FechaVencimiento;

                        ItemDetalle.Adm_Valor_Venta = Convert.ToDecimal(txtprecmaximo.Text);
                        ItemDetalle.Adm_Valor_Unit = Convert.ToDecimal(txtprecfinal.Text);

                        ItemDetalle.Cantidad = Convert.ToDecimal(txtcantidad.Text);
                        ItemDetalle.PrecUnit = Convert.ToDecimal(txtprecunitario.Text);
                        ItemDetalle.Total = Convert.ToDecimal(txtimporte.Text);

                        ItemDetalle.Unm_Base = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;
                        ItemDetalle.Invd_CantidadAtendidaEnBaseAlCoefciente = Convert.ToDecimal(txtcantidad.Text) / ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;

                        ItemDetalle.Stock = ListaFiltrada[0].Stock;


                        if (ExistenOtrosProd_Contado_PorItem(ItemDetalle.Id_Catalogo, ItemDetalle.Unm_Cod_Det, ItemDetalle.Lot_Lote))
                        {
                            Lista_Pre_Seleccionada[i_con].Cantidad = Lista_Pre_Seleccionada[i_con].Cantidad + ItemDetalle.Cantidad;
                            Lista_Pre_Seleccionada[i_con].Total = Math.Round(ItemDetalle.PrecUnit, 2) * Lista_Pre_Seleccionada[i_con].Cantidad;
                        }
                        else
                        {

                            Lista_Pre_Seleccionada.Add(ItemDetalle);
                        }



                       // UpdateGrilla();

                        lbllote.ResetText();
                        Acepta_Lotes = false;

                    if (Lista_Pre_Seleccionada.Count() > 0)
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        Accion.Advertencia("No existen productos agregados en la lista.");
                    }


                }


                //}
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }




        int i_con;
        bool ExistenOtrosProd_Contado_PorItem(string Adm_Catalogo, string Unm_Cod_Det, string Lot_Lote)
        {
            i_con = 0;
            foreach (Entidad_Catalogo T in Lista_Pre_Seleccionada)
            {
                if (T.Id_Catalogo == Adm_Catalogo & T.Unm_Cod_Det == Unm_Cod_Det & T.Lot_Lote == Lot_Lote)
                {
                    return true;
                }
                i_con += 1;
            }
            return false;
        }

        private void btncodAutoriza_Click(object sender, EventArgs e)
        {
            using (frm_valida_cod_autorizacion f = new frm_valida_cod_autorizacion())
            {

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
                    Logica_Lista_Precio LogUsu = new Logica_Lista_Precio();
                    Entidad_Lista_Precio ent = new Entidad_Lista_Precio();



                    ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    ent.Est_Codigo = Establecimiento;
                    ent.Id_Tipo = "0031";//BIEN
                    ent.Id_Catalogo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;
                    ent.Pre_Tipo = "0006";//CONTADO
                    ent.Pre_Id_Unm = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                    ent.Pre_Moneda_cod = "001"; //SOLES
                    ent.Pre_Fecha_Ini = Convert.ToDateTime(BSIFecha);
                    ent.Pre_Codigo_Autoriza = Accion.Encriptar(f.txtcodautoriza.Text.Trim());

                    Ent = LogUsu.Valida_Codigo_Autorizacion(ent);
                    if (Ent.Respuesta == "1")
                    {
                        txtprecfinal.Enabled = true;
                        txtprecfinal.Focus();
                    }
                    else
                    {
                        Accion.Advertencia("El código de autorización es incorrecto");
                        txtprecfinal.Enabled = false;
                    }
                }



            }
        }
    }
}
