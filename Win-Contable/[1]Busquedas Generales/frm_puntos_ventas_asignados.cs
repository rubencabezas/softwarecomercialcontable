﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_puntos_ventas_asignados : frm_fuente
    {
        public frm_puntos_ventas_asignados()
        {
            InitializeComponent();
        }

        public string Nombre_PC;
        public List<Entidad_Punto_Venta> Punto_Venta_PC = new List<Entidad_Punto_Venta>();

        void Buscar()
        {
            try
            {
                 Entidad_Punto_Venta ent = new Entidad_Punto_Venta();
                    Logica_Punto_Venta log = new Logica_Punto_Venta();

               
                    Punto_Venta_PC = log.Buscar_Pdv_por_PC(new Entidad_Punto_Venta
                    {
                        Id_Empresa = Actual_Conexion.CodigoEmpresa,
                        Pdv_Nombre_PC = Nombre_PC
                    });

                if (Punto_Venta_PC.Count > 0)
                {
                    dgvdatos.DataSource = Punto_Venta_PC;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
   
        }

        private void frm_puntos_ventas_asignados_Load(object sender, EventArgs e)
        {
            Buscar();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
