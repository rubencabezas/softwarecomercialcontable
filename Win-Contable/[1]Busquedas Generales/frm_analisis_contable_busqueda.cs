﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_analisis_contable_busqueda : Contable.frm_fuente
    {
        public frm_analisis_contable_busqueda()
        {
            InitializeComponent();
        }
        public string Id_Analisis;
        public Boolean Ana_Compra,Ana_Venta,Ana_Inventario;

        public List<Entidad_Analisis_Contable> Lista = new List<Entidad_Analisis_Contable>();

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        public void Listar()
        {
            Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
            Logica_Analisis_Contable log = new Logica_Analisis_Contable();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Analisis = Id_Analisis;
            Ent.Ana_Compra = Ana_Compra;
            Ent.Ana_Venta = Ana_Venta;
            Ent.Ana_Inventario = Ana_Inventario;

            try
            {
                Lista = log.Listar_Analisis(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
        private void frm_analisis_contable_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }



    }
}
