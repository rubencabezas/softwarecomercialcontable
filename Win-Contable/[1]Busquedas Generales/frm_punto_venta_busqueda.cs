﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_punto_venta_busqueda : frm_fuente
    {
        public frm_punto_venta_busqueda()
        {
            InitializeComponent();
        }

        public string Id_Empresa;
        public string Est_Codigo;
        public string Pdv_Codigo;

        public List<Entidad_Punto_Venta> Lista = new List<Entidad_Punto_Venta>();
        public void Listar()
        {
            Entidad_Punto_Venta Ent = new Entidad_Punto_Venta();
            Logica_Punto_Venta log = new Logica_Punto_Venta();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Est_Codigo = Est_Codigo;
            Ent.Pdv_Codigo = Pdv_Codigo;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }
        private void frm_punto_venta_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
