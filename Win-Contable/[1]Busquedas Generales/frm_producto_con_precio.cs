﻿using Contable;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial.Busquedas_Generales
{
    public partial class frm_producto_con_precio : frm_fuente
    {
        public frm_producto_con_precio()
        {
            InitializeComponent();
        }

   

        public List<Entidad_Lista_Precio> Lista = new List<Entidad_Lista_Precio>();
        public void Listar()
        {
            Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
            Logica_Lista_Precio log = new Logica_Lista_Precio();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //Ent.Id_Tipo = Tipo;

            try
            {
                Lista = log.Listar_Producto_Con_Precio(Ent);


                if (Lista.Count > 0)
                {

                      dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


        private void frm_producto_con_precio_Load(object sender, EventArgs e)
        {
            Listar();
         

        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\PreciosDeProductos" + ".Xlsx");



                XlsxExportOptionsEx ExXlsxExportOptions = new XlsxExportOptionsEx();
                ExXlsxExportOptions.ExportType = DevExpress.Export.ExportType.WYSIWYG;

                dgvdatos.ExportToXlsx(Ruta, ExXlsxExportOptions);

                //gridView1.OptionsPrint.ExpandAllDetails = true;
                //gridView1.OptionsPrint.PrintDetails = true;
                //gridView2.OptionsPrint.ExpandAllDetails = true;
                //gridView2.OptionsPrint.PrintDetails = true;
                //dgvdatos.ExportToXlsx(Ruta);
                //System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnTraerPrecios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                    if (Lista.Count() > 0)
                    {
                       Logica_Lista_Precio log = new Logica_Lista_Precio();

                    Mostrar_ProgressBar(true);
                    ProgressArchivo.Properties.Step = 1;
                    ProgressArchivo.Properties.PercentView = true;
                    ProgressArchivo.Properties.Maximum = Lista.Count();
                    ProgressArchivo.Properties.Minimum = 0;
                    ProgressArchivo.EditValue = 0;

                    for (int i = 0; (i <= (Lista.Count - 1)); i++)
                     {
                        //Detalles_ADM[i].Adm_Item = (i + 1);

                        List<Entidad_Lista_Precio> ListaDet = new List<Entidad_Lista_Precio>();
                        Entidad_Lista_Precio Enti = new Entidad_Lista_Precio();
                        Enti.Id_Empresa = Lista[i].Id_Empresa;

                        Enti.Est_Codigo = Lista[i].Est_Codigo;
                        Enti.Est_Descripcion = Lista[i].Est_Descripcion;
                        Enti.Id_Tipo_Interno = Lista[i].Id_Tipo_Interno;
                        Enti.Id_Tipo = Lista[i].Id_Tipo;
                        Enti.Id_Tipo_Descripcion = Lista[i].Id_Tipo_Descripcion;
                        Enti.Id_Catalogo = Lista[i].Id_Catalogo;
                        Enti.Id_Catalogo_Desc = Lista[i].Id_Catalogo_Desc;
                        //Logica_Lista_Precio Log = new Logica_Lista_Precio();

                        ListaDet = log.Listar_Producto_Con_Precio_Det(Enti);

                        Lista[i].Detalles_Precios = ListaDet;

                        ProgressArchivo.PerformStep();
                        ProgressArchivo.Update();

                    }

                    ProgressArchivo.Visible = false;
                   
                     dgvdatos.DataSource = null;
                     dgvdatos.DataSource = Lista;

                }

            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        void Mostrar_ProgressBar(bool Value)
        {
            ProgressArchivo.Visible = Value;
        }

        private void btnexpandir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
     
        }

        private void barCheckItem2_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (barCheckItem2.Checked)
                {
                    Mostrar_ProgressBar(true);
                    ProgressArchivo.Properties.Step = 1;
                    ProgressArchivo.Properties.PercentView = true;
                    ProgressArchivo.Properties.Maximum = Lista.Count();
                    ProgressArchivo.Properties.Minimum = 0;
                    ProgressArchivo.EditValue = 0;


                    for (int i = 0; (i <= (Lista.Count - 1)); i++)
                    {
                        gridView1.ExpandMasterRow(i);

                        ProgressArchivo.PerformStep();
                        ProgressArchivo.Update();

                    }

                    ProgressArchivo.Visible = false;

                    barCheckItem2.Caption = "Contraer Precios";
                }
                else
                {
                    gridView1.CollapseAllDetails();
                    barCheckItem2.Caption = "Expandir Precios";
                }
            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void dgvdatos_Click(object sender, EventArgs e)
        {

        }
    }
}
