﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_cuenta_corriente_busqueda : Contable.frm_fuente
    {
        public frm_cuenta_corriente_busqueda()
        {
            InitializeComponent();
        }

        public string Id_Empresa, Id_Anio;

        public List<Entidad_Cuenta_Corriente> Lista = new List<Entidad_Cuenta_Corriente>();
        public void Listar()
        {
            Entidad_Cuenta_Corriente Ent = new Entidad_Cuenta_Corriente();
            Logica_Cuenta_Corriente log = new Logica_Cuenta_Corriente();

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void frm_cuenta_corriente_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\CuentaCorrienteBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
