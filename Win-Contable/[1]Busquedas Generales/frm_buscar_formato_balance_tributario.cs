﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_buscar_formato_balance_tributario : frm_fuente
    {
        public frm_buscar_formato_balance_tributario()
        {
            InitializeComponent();
        }

        private void frm_buscar_formato_balance_tributario_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Formato_Balance_Tributario> Lista = new List<Entidad_Formato_Balance_Tributario>();
        public void Listar()
        {
            Entidad_Formato_Balance_Tributario Ent = new Entidad_Formato_Balance_Tributario();
            Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();


            try
            {
                Lista = log.Listar(Ent);

                if (Lista.Count > 0)
                {
                    //dgvdatosa.DataSource = null;

                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
               Accion.ErrorSistema(ex.Message);
            }

        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\buscarformatobalance" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
