﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_buscar_caja_chica_estados_busqueda : Contable.frm_fuente
    {
        public frm_buscar_caja_chica_estados_busqueda()
        {
            InitializeComponent();
        }

        public string Id_Caja, Id_Libro, Ctb_Caja_CHica_Estado;

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;
            Ent.Ctb_Tipo_IE = "0050"; 
            Ent.Ctb_Rendicion_Caja_Chica = true;
            Ent.Ctb_Caja_CHica_Estado = Ctb_Caja_CHica_Estado;
            try
            {
                Lista = log.Listar_Caja_Chica_Estado(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\BuscarCajaChicaBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void frm_buscar_caja_chica_estados_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
