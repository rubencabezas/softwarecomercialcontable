﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_marca_busqueda : Contable.frm_fuente
    {
        public frm_marca_busqueda()
        {
            InitializeComponent();
        }

        public string marca;
        public List<Entidad_Marca> Lista = new List<Entidad_Marca>();
        public void Listar()
        {
            Entidad_Marca Ent = new Entidad_Marca();
            Logica_Marca log = new Logica_Marca();

                        Ent.Id_Marca = marca;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void frm_marca_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\MarcaBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_marca_edicion f = new frm_marca_edicion())
            {
                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {
                    Entidad_Marca Ent = new Entidad_Marca();
                    Logica_Marca Log = new Logica_Marca();

                    Ent.Mar_Descripcion = f.txtdescripcion.Text;
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {
                            if (Log.Insertar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        private void btnmoddificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_marca_edicion f = new frm_marca_edicion())
            {

                f.txtdescripcion.Tag = Entidad.Id_Marca;
                f.txtdescripcion.Text = Entidad.Mar_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Marca Ent = new Entidad_Marca();
                    Logica_Marca Log = new Logica_Marca();
                    Ent.Id_Marca = f.txtdescripcion.Tag.ToString();
                    Ent.Mar_Descripcion = f.txtdescripcion.Text;
                    try
                    {
                        if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }


        Entidad_Marca Entidad = new Entidad_Marca();
        private void gridView1_Click(object sender, EventArgs e)
        {
            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnelliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La marca a eliminar es el siguiente :" + Entidad.Mar_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Marca Ent = new Entidad_Marca
                    {
                        Id_Marca = Entidad.Id_Marca
                    };

                    Logica_Marca log = new Logica_Marca();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
