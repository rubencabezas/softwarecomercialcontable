﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial.Busquedas_Generales
{
    public partial class frm_busqueda_operacion_venta : frm_fuente
    {
        public frm_busqueda_operacion_venta()
        {
            InitializeComponent();
        }

        private void frm_busqueda_operacion_venta_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Operaciones> Lista = new List<Entidad_Operaciones>();
        public void Listar()
        {
            Entidad_Operaciones Ent = new Entidad_Operaciones();
            Logica_Operaciones log = new Logica_Operaciones();

            try
            {
                Lista = log.Listar_Operaciones_Venta(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }
    }
}
