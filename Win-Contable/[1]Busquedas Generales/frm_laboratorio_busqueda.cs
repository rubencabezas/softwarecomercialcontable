﻿using Contable;
using Contable.Procesos.Inventario.Catalogo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_laboratorio_busqueda : frm_fuente
    {
        public frm_laboratorio_busqueda()
        {
            InitializeComponent();
        }

        public string fabricante;
        public List<Entidad_Laboratorio> Lista = new List<Entidad_Laboratorio>();
        public void Listar()
        {
            Entidad_Laboratorio Ent = new Entidad_Laboratorio();
            Logica_Laboratorio log = new Logica_Laboratorio();

            Ent.Id_Fabricante = fabricante;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void frm_laboratorio_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_laboratorio_edicion f = new frm_laboratorio_edicion())
            {
                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {
                    Entidad_Laboratorio Ent = new Entidad_Laboratorio();
                    Logica_Laboratorio Log = new Logica_Laboratorio();

                    Ent.Fab_Descripcion = f.txtdescripcion.Text;
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {
                            if (Log.Insertar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        private void btnmmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_laboratorio_edicion f = new frm_laboratorio_edicion())
            {

                f.txtdescripcion.Tag = Entidad.Id_Fabricante;
                f.txtdescripcion.Text = Entidad.Fab_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Laboratorio Ent = new Entidad_Laboratorio();
                    Logica_Laboratorio Log = new Logica_Laboratorio();
                    Ent.Id_Fabricante = f.txtdescripcion.Tag.ToString();
                    Ent.Fab_Descripcion = f.txtdescripcion.Text;
                    try
                    {
                        if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        Entidad_Laboratorio Entidad = new Entidad_Laboratorio();
        private void gridView1_Click(object sender, EventArgs e)
        {
            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "El Laboratorio a eliminar es el siguiente :" + Entidad.Fab_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Laboratorio Ent = new Entidad_Laboratorio
                    {
                        Id_Fabricante = Entidad.Id_Fabricante
                    };

                    Logica_Laboratorio log = new Logica_Laboratorio();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
