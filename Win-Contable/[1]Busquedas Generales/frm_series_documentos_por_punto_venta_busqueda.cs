﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_series_documentos_por_punto_venta_busqueda : frm_fuente
    {
        public frm_series_documentos_por_punto_venta_busqueda()
        {
            InitializeComponent();
        }

        public string tipodoc;
        public string pdvcodigo;

        private void frm_series_documentos_por_punto_venta_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

 public List<Entidad_Serie_Comprobante> SerieNumeros = new List<Entidad_Serie_Comprobante>();

        void Listar()
        {
            try
            {

 
                        Logica_Serie_Comprobante LogSerieNum = new Logica_Serie_Comprobante();
                      
                        SerieNumeros = LogSerieNum.Listar(new Entidad_Serie_Comprobante
                        {
                            Id_Empresa = Actual_Conexion.CodigoEmpresa,
                            Ser_Tipo_Doc = tipodoc,
                            Ser_Pdv_Codigo = pdvcodigo
                        });

                        if ((SerieNumeros.Count> 0))
                        {

                             dgvdatos.DataSource = SerieNumeros;
                        }
                        

                    }

                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }

                }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }
    }
}
