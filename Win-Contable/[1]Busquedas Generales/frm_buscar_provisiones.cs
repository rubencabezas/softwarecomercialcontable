﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_buscar_provisiones : Contable.frm_fuente
    {
        public frm_buscar_provisiones()
        {
            InitializeComponent();
        }

        public string Id_libro;
        public string Libro_nombre;

        private void frm_buscar_provisiones_Load(object sender, EventArgs e)
        {
            txtlibroid.Text = Id_libro;
            txtlibrodesc.Text = Libro_nombre;
            txtlibroid.Enabled = false;
        }

    

        private void txtlibroid_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlibroid.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlibroid.Text.Substring(txtlibroid.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtlibroid.Text = Entidad.Id_Libro;
                                txtlibrodesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlibroid.Text) & string.IsNullOrEmpty(txtlibrodesc.Text))
                    {
                        Buscar_libro();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void Buscar_libro()
        {
            try
            {
                txtlibroid.Text = Accion.Formato(txtlibroid.Text, 3);
                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_Libro = txtlibroid.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_Libro).ToString().Trim().ToUpper() == txtlibroid.Text.Trim().ToUpper())
                        {
                            txtlibroid.Text = (T.Id_Libro).ToString().Trim();
                            txtlibrodesc.Text = T.Nombre_Libro;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txttipodoc.EnterMoveNextControl = false;
                    //txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();
            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = txtmescod.Text;
            Ent.Id_Libro = Id_libro;

            try
            {
                Lista = log.Listar_Provision(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void txtmescod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtmescod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmescod.Text.Substring(txtmescod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_periodo_busqueda f = new _1_Busquedas_Generales.frm_periodo_busqueda())
                        {
                            f.empresa = Actual_Conexion.CodigoEmpresa;
                            f.anio = Actual_Conexion.AnioSelect;
                            //f.periodo = txtmescod.Text;
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ejercicio Entidad = new Entidad_Ejercicio();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmescod.Text = Entidad.Id_Periodo;
                                txtmesdesc.Text = Entidad.Descripcion_Periodo;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmescod.Text) & string.IsNullOrEmpty(txtmesdesc.Text))
                    {
                        BuscarMes();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarMes()
        {
            try
            {
                txtmescod.Text = Accion.Formato(txtmescod.Text, 2);

                Logica_Ejercicio log = new Logica_Ejercicio();

                List<Entidad_Ejercicio> Generales = new List<Entidad_Ejercicio>();

                Generales = log.Listar_Periodo(new Entidad_Ejercicio
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Periodo = txtmescod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ejercicio T in Generales)
                    {
                        if ((T.Id_Periodo).ToString().Trim().ToUpper() == txtmescod.Text.Trim().ToUpper())
                        {
                            txtmescod.Text = (T.Id_Periodo).ToString().Trim();
                            txtmesdesc.Text = T.Descripcion_Periodo;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro el periodo");
                    txtmescod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmescod_TextChanged(object sender, EventArgs e)
        {
            if (txtmescod.Focus() == false)
            {
                txtmesdesc.ResetText();
            }
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Lista.Count > 0)
            {
                if (gridView1.FocusedRowHandle >= 0)
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }

        private void gridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == Strings.Chr(13))
            //{
            //    btnseleccion.PerformClick();
            //    e.KeyChar = Convert.ToChar(Keys.None);
            //}

        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\ProvisionesBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
