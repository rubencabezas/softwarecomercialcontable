﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._1_Busquedas_Generales
{
    public partial class frm_tipo_cambio_busqueda : Contable.frm_fuente
    {
        public frm_tipo_cambio_busqueda()
        {
            InitializeComponent();
        }
        public DateTime FechaTransac;
  
        public List<Entidad_Tipo_Cambio> ListaTipoCambio = new List<Entidad_Tipo_Cambio>();
        public void Listar()
        {
            try
            {
                lblfecha.Text = Convert.ToDateTime(FechaTransac).ToString();
                Entidad_Tipo_Cambio TC = new Entidad_Tipo_Cambio
                {
                    Tic_Fecha = FechaTransac
                };
                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                ListaTipoCambio = log.Buscar_Tipo_Cambio(TC);
                lblfechaencontrada.Text =Convert.ToString( ListaTipoCambio[0].Tic_Fecha);
                dgvdatos.DataSource = ListaTipoCambio;
            }
            catch (Exception ex)
            {
               Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_tipo_cambio_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\TipoCambioBusqueda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
