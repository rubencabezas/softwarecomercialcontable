﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial.Busquedas_Generales
{
    public partial class frm_producto_stock_lote_varios : frm_fuente
    {
        public frm_producto_stock_lote_varios()
        {
            InitializeComponent();
        }

        public List<Entidad_Catalogo> Lista_Lote_Producto = new List<Entidad_Catalogo>();
        private void frm_producto_stock_lote_varios_Load(object sender, EventArgs e)
        {

            try
            {
                dgvdatos.DataSource = null;

                if (Lista_Lote_Producto.Count() > 0)
                {
                    dgvdatos.DataSource = Lista_Lote_Producto;
                }
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            btnseleccion.PerformClick();
        }
    }
}
