﻿using Contable;
using DevExpress.CodeParser;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial.Busquedas_Generales
{
    public partial class frm_producto_stock_lote : frm_fuente
    {
        public frm_producto_stock_lote()
        {
            InitializeComponent();
        }

        public string Id_Almacen;

        public List<Entidad_Catalogo> ListaFiltrada = new List<Entidad_Catalogo>();
        List<Entidad_Catalogo> VistaPrivilegios = new List<Entidad_Catalogo>();
        List<Entidad_Catalogo> Vista = new List<Entidad_Catalogo>();

        public string BSIFecha;

        public string Establecimiento;
 
        public Decimal Igv_Porcentaje = 0;

        String TipoBS = "";
        bool EsBien=false;

        List<Entidad_Lista_Precio> Lista_Precio = new List<Entidad_Lista_Precio>();

        public  Entidad_Catalogo Entidad_Indice_Lote = new Entidad_Catalogo();
        public List<Entidad_Catalogo> ListaFiltrada_Por_Presentacion = new List<Entidad_Catalogo>();

        bool Es_Precio_Variable = false;

        //Entidad_Catalogo Lista_Pre_Seleccionada = new Entidad_Catalogo();
       public List<Entidad_Catalogo> Lista_Pre_Seleccionada = new List<Entidad_Catalogo>();
        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(BSIFecha) == true))
                {
                    if ((DateTime.Parse(BSIFecha).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(BSIFecha);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            Igv_Porcentaje = Lista[0].Imd_Tasa;
                            //Igv_Tasa_texto = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));
                            //Igv_TasaImpresion = Lista[0].Imd_Tasa_Impresion;
                            //LblIGV.Text = "I.G.V." + "(" + Math.Round(Lista[0].Imd_Tasa * 100, 2) + "):";
                            //LblIGV.Tag = Lista[0].Imd_Tasa;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        void Limpiar()
        {
            txtprecminimo.Text = "0.00";
            txtprecmaximo.Text = "0.00";
            txtprecigv.Text = "0.00";
            txtprecfinal.Text = "0.00";
            txtcantidad.Text = "1.00";
            txtimporte.Text = "0.00";
            //StockTotal = 0;

        }
            private void frm_producto_stock_lote_Load(object sender, EventArgs e)
        {
            try
            {
                txtcantidad.Text = Convert.ToString(1);
                txtprecminimo.Text = "0.000";
                txtprecmaximo.Text = "0.000";
                txtprecigv.Text = "0.000";
                txtprecfinal.Text = "0.000";
                txtimporte.Text = "0.000";
                TipoBS = "0031";

                EsBien = true;
                IniciarBienes();


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void IniciarBienes()
        {
            try
            {

                //CBtnServicios.Checked = false;
                //CBtnBienes.Checked = true;
                //if (CBtnBienes.Checked)
                //{
                    Entidad_Catalogo Producto = new Entidad_Catalogo();
                    Logica_Catalogo Log = new Logica_Catalogo();

                    Producto.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Producto.Id_Anio = Actual_Conexion.AnioSelect;
                    Producto.Id_Tipo = TipoBS;//BIEN
                    Producto.Id_Almacen = Id_Almacen;
          ListaFiltrada.Clear();
                    dgvdatos.DataSource = null;
                    ListaFiltrada = Log.Consultar_Stock_Lote(Producto);
          

                   List<Entidad_Catalogo> List = new List<Entidad_Catalogo>();
                    foreach (Entidad_Catalogo t in ListaFiltrada)
                    {
                        if (t.Stock > 0)
                        {
                            List.Add(t);
                        }
                    }
                    ListaFiltrada.Clear();
                    ListaFiltrada = List;
                    if (ListaFiltrada.Count > 0)
                    {
                        dgvdatos.DataSource = ListaFiltrada;
                        //gridView1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;
                        //gridView1.ShowEditor();

                        //var gridView = dgvdatos.MainView as GridView;
                        //gridView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
                        //gridView.ShowEditor();

                        // gridView1.FocusedRowHandle = 2;
                        //gridView1.FocusedColumn = gridView1.VisibleColumns[1];
                        //gridView1.ShowEditor();

                        //DgvGeneral.DataSource = ListaFiltrada;
                        //gridView3.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;

                        dgvdatos.ForceInitialize();
                        gridView1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;
                        gridView1.FocusedColumn = gridView1.VisibleColumns[1];//gridView1.Columns["GridColumn2"];

                        gridView1.ShowEditor();
                    }
                //}

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        Entidad_Catalogo Entidad = new Entidad_Catalogo();
        public decimal StockTotal=0;

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (ListaFiltrada.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Limpiar();

                    StockTotal=ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Stock; 

                    barstaticProductoDescip.Caption ="PRODUCTO SELECCIONADO : "+ ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Cat_Descripcion.Trim(); 

                    Buscar_Producto_Por_Presentacion();

                    //Buscamos El tipo de Operacion Por Producto para definir Si Esta afecto u Otro Regimen del IGV
                    Entidad_Operaciones oper = new Entidad_Operaciones();
                    Logica_Operaciones logoper = new Logica_Operaciones();
                    List<Entidad_Operaciones> operlist = new List<Entidad_Operaciones>();
                    oper.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    oper.Id_Tipo = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;// item.Adm_Tipo_BSA;
                    oper.Id_Catalogo = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;// item.Adm_Catalogo;
                    operlist = logoper.Traer_Operacion_venta_Por_Producto(oper);
                    ////////////////////
                    ///
                    Buscar_Precio_por_presentacion(operlist[0].AfecIGV_Tabla);

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        void Buscar_Producto_Por_Presentacion()
        {
              try
            {

                Entidad_Catalogo Producto = new Entidad_Catalogo();
                Logica_Catalogo Log = new Logica_Catalogo();

                Producto.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Producto.Id_Tipo = TipoBS;//BIEN o SERVICIO
                Producto.Id_Catalogo = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;  

                ListaFiltrada_Por_Presentacion = Log.Consultar_Producto_Y_Sus_Presentaciones(Producto);
  
                if (ListaFiltrada_Por_Presentacion.Count > 0)
                {
                    dgvdatospresentacion.DataSource = ListaFiltrada_Por_Presentacion;

                    lblPresentacionSelected.Text = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Desc_Det;

                }
            }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }

        void Buscar_Precio_por_presentacion(string DvtD_OperCodigo)
        {
            try
            {


                if (DvtD_OperCodigo == "0565")
                {
                    ActualizaIGV();
                }
                else
                {
                    Igv_Porcentaje = 0;
                }

                Logica_Lista_Precio log = new Logica_Lista_Precio();
                Entidad_Lista_Precio ent = new Entidad_Lista_Precio();


                ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                ent.Est_Codigo = Establecimiento;
                ent.Id_Tipo = TipoBS;//BIEN
                ent.Id_Catalogo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;
                ent.Pre_Tipo = "0006";//CONTADO
                ent.Pre_Id_Unm = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                ent.Pre_Moneda_cod = "001"; //SOLES
                ent.Pre_Fecha_Ini = Convert.ToDateTime(BSIFecha);
                //Lista_Precio = log.Traer_Precio_Procuto_Presentacion(ent);
                Lista_Precio = log.Traer_Precio_Procuto_Presentacion_Por_Unm(ent);

                Es_Precio_Variable = false;

                if (Lista_Precio.Count > 0)
                {
                    Es_Precio_Variable = Lista_Precio[0].Es_Precio_Variable;

                    if (Es_Precio_Variable == true)
                    {

                        //===============================
                        btncodAutoriza.Enabled = true;

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo),4) * Math.Round(Igv_Porcentaje, 2));
                        
                        decimal precigvMin = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4) * Math.Round(Igv_Porcentaje, 2);
                        txtprecminimo.Text = Convert.ToString(Math.Round(precigvMin + Math.Round(Lista_Precio[0].Pre_Precio_Minimo,2),2));

                        decimal precigvMax = Math.Round(Convert.ToDecimal(txtprecigv.Text),4);
                  

                        decimal IgvPrecMaximo =  Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);
                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));

                        txtprecfinal.Text = Convert.ToString(Math.Round(precigvMax + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2),2));

                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text),2));

                        txtprecfinal.Enabled = false;



                        Calcula_Importe_Final();

                        txtprecfinal.Focus();
                    }
                    else
                    {
                        btncodAutoriza.Enabled = false;
                        //                Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo),4) * Math.Round(Igv_Porcentaje, 2));
                     
                        //decimal precigv = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4, MidpointRounding.ToEven) * Math.Round(Igv_Porcentaje, 2));

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo),4, MidpointRounding.AwayFromZero) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);

                        decimal PrecMin = Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2, MidpointRounding.AwayFromZero);

                        txtprecminimo.Text = Convert.ToString(precigv + PrecMin);//Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                        txtprecmaximo.Text = Convert.ToString(precigv + PrecMin);//Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                        txtprecfinal.Text = Convert.ToString(precigv + PrecMin);//Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));

                        txtprecunitario.Text = txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecfinal.Enabled = false;

                        Calcula_Importe_Final();

                    }


                }
                else
                {
                    //Accion.Advertencia("Debe configurar un precio para este producto.");
                    txtprecfinal.Enabled = false;
                    btncodAutoriza.Enabled = false;



                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }




        void Calcula_Importe_Final()
        {
            try
            {
                if (txtcantidad.Text != "")
                {
                    //  decimal Precio_Final = Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text);
                    decimal Precio_Final = Convert.ToDecimal(txtprecfinal.Text);
                    txtimporte.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtcantidad.Text) * Precio_Final, 2));


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcantidad_Validating(object sender, CancelEventArgs e)
        {
            Calcula_Importe_Final();
        }

        private void txtprecfinal_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txtcantidad.Text != "")
                {
                    if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
                    {
                        Accion.Advertencia("El precio final no debe ser menor al precio base");



                        //txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
                        //decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                        //txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));
                        //txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigvMin = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4) * Math.Round(Igv_Porcentaje, 2);
                        txtprecminimo.Text = Convert.ToString(Math.Round(precigvMin + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2), 2));
                        decimal precigvMax = Math.Round(Convert.ToDecimal(txtprecigv.Text), 4);
                        decimal IgvPrecMaximo = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);
                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));
                        txtprecfinal.Text = Convert.ToString(Math.Round(precigvMax + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2), 2));
                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        Calcula_Importe_Final();

                        txtprecfinal.Focus();
                        txtprecfinal.Select();
                        return;
                    }
                    else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
                    {
                        Accion.Advertencia("El precio final no debe ser mayor al valor máximo");



                        //txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
                        //decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                        //txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));
                        //txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigvMin = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4) * Math.Round(Igv_Porcentaje, 2);
                        txtprecminimo.Text = Convert.ToString(Math.Round(precigvMin + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2), 2));
                        decimal precigvMax = Math.Round(Convert.ToDecimal(txtprecigv.Text), 4);
                        decimal IgvPrecMaximo = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);
                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));
                        txtprecfinal.Text = Convert.ToString(Math.Round(precigvMax + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2), 2));
                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));


                        Calcula_Importe_Final();

                        txtprecfinal.Focus();
                        txtprecfinal.Select();
                        return;
                    }
                    else
                    {
                        if (Es_Precio_Variable == true)
                        {

                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));


                            Calcula_Importe_Final();

                            txtcantidad.Focus();
                        }
                        else
                        {
                            txtprecminimo.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                            txtprecmaximo.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));

                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 2) * Math.Round(Igv_Porcentaje, 2));
                            decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Maximo + precigv,2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));



                            txtprecfinal.Enabled = true;
                            txtimporte.Text = Convert.ToString(Convert.ToDecimal(txtcantidad.Text) * Convert.ToDecimal(txtprecfinal.Text));
                            txtcantidad.Focus();
                            txtcantidad.Select();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);

            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            //if (gridView1.RowCount > 0)
            //{
            //    btnseleccion.PerformClick();
            //}


        }


         public  List<Entidad_Catalogo> Lista_Lote_Producto = new List<Entidad_Catalogo>();
        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //try
            //{

            //    if (gridView1.RowCount > 0)
            //    {
            //        if (ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Stock <= 0)
            //        {
            //            Accion.Advertencia("El producto seleccionado no tiene lote");
            //            return;
            //        }

            //        if (Convert.ToDecimal(txtprecfinal.Text) == 0)
            //        {
            //            Accion.Advertencia("El precio base final no puede ser cero.");
            //            return;
            //        }

            //        if (txtcantidad.Text != "")
            //        {

            //            if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
            //            {
            //                Accion.Advertencia("El precio base final no debe ser menor al precio base mínimo");
            //                txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
            //                txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
            //                txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
            //                Calcula_Importe_Final();
            //                txtprecfinal.Select();
            //                return;
            //            }
            //            else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
            //            {
            //                Accion.Advertencia("El precio base final no debe ser mayor al valor máximo");
            //                txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
            //                txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
            //                txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
            //                Calcula_Importe_Final();
            //                txtprecfinal.Focus();
            //                txtprecfinal.Select();
            //                return;
            //            }

            //        }



            //        if (ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Acepta_Lote == true)
            //        {
            //            // Buscamos si tiene mas de un lote

            //            Logica_Catalogo log = new Logica_Catalogo();
            //            Entidad_Catalogo pre = new Entidad_Catalogo();

            //            Entidad = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()];

            //            pre.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            //            pre.Id_Anio = Actual_Conexion.AnioSelect;
            //            pre.Id_Almacen = Id_Almacen;
            //            pre.Id_Tipo = "0031";//BIEN
            //            pre.Id_Catalogo = Entidad.Id_Catalogo;

            //            Lista_Lote_Producto = log.Consultar_Stock_Producto_Lote(pre);

            //            // Si el producto tiene un solo lote , debe cargar defrente
            //            // Si tiene mas de uno debe abrirse una ventana para qeu pueda escoger
            //            if (Lista_Lote_Producto.Count == 1)
            //            {
            //                int xx = gridView1.GetFocusedDataSourceRowIndex();
            //                Entidad_Indice_Lote = Lista_Lote_Producto[0];
            //                DialogResult = DialogResult.OK;
            //            }
            //            else
            //            {
            //                //Muestra una ventana para escoger que lote dar salida
            //                using (frm_producto_stock_lote_varios f = new frm_producto_stock_lote_varios())
            //                {
            //                    f.Lista_Lote_Producto = Lista_Lote_Producto;

            //                    if (f.ShowDialog() == DialogResult.OK)
            //                    {
            //                        Entidad_Indice_Lote = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()];
            //                        DialogResult = DialogResult.OK;
            //                    }
            //                }
            //            }

            //        }
            //        else
            //        {
            //            DialogResult = DialogResult.OK;
            //        }


            //    }
            //}
            //catch (Exception ex)
            //{
            //    Accion.ErrorSistema(ex.Message);
            //}

            try
            {

                if (gridView1.RowCount > 0)
                {
                    if (ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Stock <= 0)
                    {
                        Accion.Advertencia("El producto seleccionado no tiene lote");
                        return;
                    }

                    if (Convert.ToDecimal(txtprecfinal.Text) == 0)
                    {
                        Accion.Advertencia("El precio base final no puede ser cero.");
                        return;
                    }

                    if (txtcantidad.Text != "")
                    {

                        if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
                        {
                            Accion.Advertencia("El precio base final no debe ser menor al precio base mínimo");
                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
                            Calcula_Importe_Final();
                            txtprecfinal.Select();
                            return;
                        }
                        else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
                        {
                            Accion.Advertencia("El precio base final no debe ser mayor al valor máximo");
                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
                            Calcula_Importe_Final();
                            txtprecfinal.Focus();
                            txtprecfinal.Select();
                            return;
                        }

                    }



                    if (ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Acepta_Lote == true)
                    {
                        // Buscamos si tiene mas de un lote

                        Logica_Catalogo log = new Logica_Catalogo();
                        Entidad_Catalogo pre = new Entidad_Catalogo();

                        Entidad = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()];

                        pre.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        pre.Id_Anio = Actual_Conexion.AnioSelect;
                        pre.Id_Almacen = Id_Almacen;
                        pre.Id_Tipo = TipoBS;//BIEN
                        pre.Id_Catalogo = Entidad.Id_Catalogo;

                        Lista_Lote_Producto = log.Consultar_Stock_Producto_Lote(pre);

                        // Si el producto tiene un solo lote , debe cargar defrente
                        // Si tiene mas de uno debe abrirse una ventana para qeu pueda escoger
                        if (Lista_Lote_Producto.Count == 1)
                        {
                            int xx = gridView1.GetFocusedDataSourceRowIndex();
                            Entidad_Indice_Lote = Lista_Lote_Producto[0];
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            //Muestra una ventana para escoger que lote dar salida
                            using (frm_producto_stock_lote_varios f = new frm_producto_stock_lote_varios())
                            {
                                f.Lista_Lote_Producto = Lista_Lote_Producto;

                                if (f.ShowDialog() == DialogResult.OK)
                                {
                                    Entidad_Indice_Lote = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()];
                                    DialogResult = DialogResult.OK;
                                }
                            }
                        }

                    }
                    else
                    {
                        DialogResult = DialogResult.OK;
                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Stock" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView2_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (ListaFiltrada_Por_Presentacion.Count > 0 & bandedGridView1.GetFocusedDataSourceRowIndex() > -1)
                {

                    lblPresentacionSelected.Text = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Desc_Det;


                    Limpiar();

                    //Buscamos El tipo de Operacion Por Producto para definir Si Esta afecto u Otro Regimen del IGV
                    Entidad_Operaciones oper = new Entidad_Operaciones();
                    Logica_Operaciones logoper = new Logica_Operaciones();
                    List<Entidad_Operaciones> operlist = new List<Entidad_Operaciones>();
                    oper.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    oper.Id_Tipo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;// item.Adm_Tipo_BSA;
                    oper.Id_Catalogo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;// item.Adm_Catalogo;
                    operlist = logoper.Traer_Operacion_venta_Por_Producto(oper);
                    ////////////////////

                    Buscar_Precio_por_presentacion(operlist[0].AfecIGV_Tabla);

                    }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            //var gridView = dgvdatos.MainView as GridView;
            //gridView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
            //gridView.ShowEditor();
        }

        private void btncodAutoriza_Click(object sender, EventArgs e)
        {
            using (frm_valida_cod_autorizacion f = new frm_valida_cod_autorizacion())
            {

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
                    Logica_Lista_Precio LogUsu = new Logica_Lista_Precio();
                    Entidad_Lista_Precio ent = new Entidad_Lista_Precio();

 
    
                    ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    ent.Est_Codigo = Establecimiento;
                    ent.Id_Tipo = TipoBS;//BIEN
                    ent.Id_Catalogo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;
                    ent.Pre_Tipo = "0006";//CONTADO
                    ent.Pre_Id_Unm = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                    ent.Pre_Moneda_cod = "001"; //SOLES
                    ent.Pre_Fecha_Ini = Convert.ToDateTime(BSIFecha);
                    ent.Pre_Codigo_Autoriza = Accion.Encriptar(f.txtcodautoriza.Text.Trim()); 

                    Ent = LogUsu.Valida_Codigo_Autorizacion(ent);
                    if (Ent.Respuesta == "1")
                    {
                        txtprecfinal.Enabled = true;
                        txtprecfinal.Focus();
                    }
                    else
                    {
                        Accion.Advertencia("El código de autorización es incorrecto");
                        txtprecfinal.Enabled = false;
                    }
                 }



            }
        }

        private void btnanadir_Click(object sender, EventArgs e)
        {
            try
            {

                if (gridView1.RowCount > 0)
                {
                    if (EsBien)
                    {
                        if (ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Stock <= 0)
                            {
                                Accion.Advertencia("El producto seleccionado no tiene stock");
                                return;
                            }
                    }
         

                    if (Convert.ToDecimal(txtprecfinal.Text) == 0)
                    {
                        Accion.Advertencia("El precio de venta final no puede ser cero.");
                        return;
                    }

                    if (txtcantidad.Text != "")
                    {

                        if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
                        {
                            Accion.Advertencia("El precio de venta final no debe ser menor al precio venta ");

                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
                            Calcula_Importe_Final();
                            txtprecfinal.Select();
                            return;
                        }
                        else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
                        {
                            Accion.Advertencia("El precio base final no debe ser mayor al valor máximo");
                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 4));
                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2) * Math.Round(Igv_Porcentaje, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text), 2));
                            Calcula_Importe_Final();
                            txtprecfinal.Focus();
                            txtprecfinal.Select();
                            return;
                        }

                    }

                    bool Acepta_Lotes = false;
                    DateTime Lot_FechaFabricacion;
                    DateTime Lot_FechaVencimiento;

                    if (ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Acepta_Lote == true)
                    {
                        // Buscamos si tiene mas de un lote

                        Logica_Catalogo log = new Logica_Catalogo();
                        Entidad_Catalogo pre = new Entidad_Catalogo();

                        Entidad = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()];

                        pre.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        pre.Id_Anio = Actual_Conexion.AnioSelect;
                        pre.Id_Almacen = Id_Almacen;
                        pre.Id_Tipo = TipoBS;//BIEN
                        pre.Id_Catalogo = Entidad.Id_Catalogo;

                        Lista_Lote_Producto = log.Consultar_Stock_Producto_Lote(pre);

                        // Si el producto tiene un solo lote , debe cargar defrente
                        // Si tiene mas de uno debe abrirse una ventana para qeu pueda escoger
                        if (Lista_Lote_Producto.Count > 0 )
                        {
                            //int xx = gridView1.GetFocusedDataSourceRowIndex();
                            //Entidad_Indice_Lote = Lista_Lote_Producto[0];
                            ////DialogResult = DialogResult.OK;
                            //lbllote.Text = Lista_Lote_Producto[0].Lot_Lote;
                            //Lot_FechaFabricacion = Lista_Lote_Producto[0].Lot_FechaFabricacion;
                            //Lot_FechaVencimiento = Lista_Lote_Producto[0].Lot_FechaVencimiento;

                            //Acepta_Lotes = true;
                        //}
                        //else
                        //{
                            //Muestra una ventana para escoger que lote dar salida
                            using (frm_producto_stock_lote_varios f = new frm_producto_stock_lote_varios())
                            {
                                f.Lista_Lote_Producto = Lista_Lote_Producto;
                                f.txtproducto.Caption = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Cat_Descripcion;
                                
                                // SI TIENE MAS DE UN LOTE

                                if (f.ShowDialog() == DialogResult.OK)
                                {
                                    Entidad_Indice_Lote = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()];
                                    //DialogResult = DialogResult.OK;
                                    lbllote.Text = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()].Lot_Lote;
                                    Acepta_Lotes = true;
                                    Lot_FechaFabricacion = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()].Lot_FechaFabricacion;
                                    Lot_FechaVencimiento = f.Lista_Lote_Producto[f.gridView1.GetFocusedDataSourceRowIndex()].Lot_FechaVencimiento;

                                    //AQUI SE AGREGA 

                                    Entidad_Catalogo ItemDetalle = new Entidad_Catalogo();

                                    ItemDetalle.Id_Item = Lista_Pre_Seleccionada.Count() + 1;

                                    if (StockTotal < Convert.ToDecimal(txtcantidad.Text))
                                    {
                                        Accion.Advertencia("NO HAY STOCK SUFICIENTE DE ESTE PRODUCTO");
                                        return;
                                    }

                                    ItemDetalle.Id_Catalogo = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;
                                    ItemDetalle.Cat_Descripcion = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Cat_Descripcion;


                                    ItemDetalle.Id_Tipo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;
                                    ItemDetalle.Unm_Cod_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                                    ItemDetalle.Unm_Desc_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Desc_Det;
                                    ItemDetalle.Und_Abreviado =ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Und_Abreviado;
                                    ItemDetalle.Unm_FE = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_FE;

                                    ItemDetalle.Acepta_Lote = Acepta_Lotes;
                                    ItemDetalle.Lot_Lote = lbllote.Text;
                                    ItemDetalle.Lot_FechaFabricacion = Lot_FechaFabricacion;
                                    ItemDetalle.Lot_FechaVencimiento = Lot_FechaVencimiento;

                                    decimal xxx = Igv_Porcentaje;

                                    ItemDetalle.Adm_Valor_Venta = Math.Round(Convert.ToDecimal(txtprecfinal.Text) / (1 + Igv_Porcentaje),2);
                                    ItemDetalle.Adm_Valor_Unit= Convert.ToDecimal(txtprecfinal.Text);

                                    ItemDetalle.Cantidad = Convert.ToDecimal(txtcantidad.Text);
                                    ItemDetalle.PrecUnit = Convert.ToDecimal(txtprecunitario.Text);
                                    ItemDetalle.Total = Convert.ToDecimal(txtimporte.Text);

                                    ItemDetalle.Unm_Base= ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;
                                    ItemDetalle.Invd_CantidadAtendidaEnBaseAlCoefciente= ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;// Convert.ToDecimal(txtcantidad.Text)/ ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;

                                    ItemDetalle.Stock = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Stock;

                                    if (ExistenOtrosProd_Contado_PorItem(ItemDetalle.Id_Catalogo, ItemDetalle.Unm_Cod_Det, ItemDetalle.Lot_Lote))
                                    {
                                        Lista_Pre_Seleccionada[i_con].Cantidad = Lista_Pre_Seleccionada[i_con].Cantidad + ItemDetalle.Cantidad;

                                        if (StockTotal < Lista_Pre_Seleccionada[i_con].Cantidad)
                                        {
                                            Lista_Pre_Seleccionada[i_con].Cantidad = Lista_Pre_Seleccionada[i_con].Cantidad - ItemDetalle.Cantidad;
                                            Accion.Advertencia("NO HAY STOCK SUFICIENTE DE ESTE PRODUCTO");
                                            return;
                                        }

                                        Lista_Pre_Seleccionada[i_con].Total = Math.Round(ItemDetalle.PrecUnit, 2) * Lista_Pre_Seleccionada[i_con].Cantidad;

                                    }
                                    else
                                    {
                                        Lista_Pre_Seleccionada.Add(ItemDetalle);
                                    }

                                    UpdateGrilla();

                                    //REGRESA EL FOCUS EN EL ROW DE BUSQUEDA DE PRODUCTOS
                                    dgvdatos.ForceInitialize();
                                    gridView1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;
                                    gridView1.FocusedColumn = gridView1.VisibleColumns[1];//gridView1.Columns["GridColumn2"];
                                    gridView1.ShowEditor();

                                    lbllote.ResetText();
                                    Acepta_Lotes = false;

                                }
                            }
                        }

                    }
                    else
                    {
                        //DialogResult = DialogResult.OK;
                        //AQUI SE AGREGA 

                        Entidad_Catalogo ItemDetalle = new Entidad_Catalogo();

                        ItemDetalle.Id_Item = Lista_Pre_Seleccionada.Count()+ 1;

                        //StockTotal
                        if (EsBien)
                        {
                            if (StockTotal < Convert.ToDecimal(txtcantidad.Text))
                            {
                                Accion.Advertencia("NO HAY STOCK SUFICIENTE DE ESTE PRODUCTO");
                                return;
                            }

                        }


                        ItemDetalle.Id_Catalogo = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Id_Catalogo;
                        ItemDetalle.Cat_Descripcion= ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Cat_Descripcion;

                        ItemDetalle.Id_Tipo = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Id_Tipo;
                        ItemDetalle.Unm_Cod_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                        ItemDetalle.Unm_Desc_Det = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Desc_Det;
                        ItemDetalle.Und_Abreviado = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Und_Abreviado;
                        ItemDetalle.Unm_FE = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_FE;

                        ItemDetalle.Lot_Lote = lbllote.Text ;
                        ItemDetalle.Acepta_Lote = Acepta_Lotes;
                        //ItemDetalle.Lot_FechaFabricacion = Lot_FechaFabricacion;
                        //ItemDetalle.Lot_FechaVencimiento = Lot_FechaVencimiento;
                        decimal xxx = Igv_Porcentaje;

                        ItemDetalle.Adm_Valor_Venta = Math.Round(Convert.ToDecimal(txtprecfinal.Text) / (1 + Igv_Porcentaje),2);
                        ItemDetalle.Adm_Valor_Unit = Convert.ToDecimal(txtprecfinal.Text);

                        ItemDetalle.Cantidad = Convert.ToDecimal(txtcantidad.Text);
                        ItemDetalle.PrecUnit = Convert.ToDecimal(txtprecunitario.Text);
                        ItemDetalle.Total = Convert.ToDecimal(txtimporte.Text);

                        ItemDetalle.Unm_Base = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;
                        ItemDetalle.Invd_CantidadAtendidaEnBaseAlCoefciente = ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;// Convert.ToDecimal(txtcantidad.Text) / ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Base;

                        ItemDetalle.Stock = ListaFiltrada[gridView1.GetFocusedDataSourceRowIndex()].Stock;


                        if (ExistenOtrosProd_Contado_PorItem(ItemDetalle.Id_Catalogo, ItemDetalle.Unm_Cod_Det, ItemDetalle.Lot_Lote)){

                  
                            Lista_Pre_Seleccionada[i_con].Cantidad  = Lista_Pre_Seleccionada[i_con].Cantidad + ItemDetalle.Cantidad;

                            if (StockTotal < Lista_Pre_Seleccionada[i_con].Cantidad)
                            {
                                Lista_Pre_Seleccionada[i_con].Cantidad = Lista_Pre_Seleccionada[i_con].Cantidad - ItemDetalle.Cantidad;
                                Accion.Advertencia("NO HAY STOCK SUFICIENTE DE ESTE PRODUCTO");
                                return;
                            }



                            Lista_Pre_Seleccionada[i_con].Total = Math.Round(ItemDetalle.PrecUnit, 2) * Lista_Pre_Seleccionada[i_con].Cantidad;
                       
                        
                        }
                        else
                        {

                          Lista_Pre_Seleccionada.Add(ItemDetalle);
                        }



                        UpdateGrilla();

                        //REGRESA EL FOCUS EN EL ROW DE BUSQUEDA DE PRODUCTOS
                        dgvdatos.ForceInitialize();
                        gridView1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;
                        gridView1.FocusedColumn = gridView1.VisibleColumns[1];//gridView1.Columns["GridColumn2"];
                        gridView1.ShowEditor();

                        lbllote.ResetText();
                        Acepta_Lotes = false;

                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }



        int i_con;
        bool ExistenOtrosProd_Contado_PorItem(string Adm_Catalogo, string Unm_Cod_Det,string Lot_Lote)
        {
            i_con = 0;
            foreach (Entidad_Catalogo T in Lista_Pre_Seleccionada)
            {
                if (T.Id_Catalogo == Adm_Catalogo & T.Unm_Cod_Det == Unm_Cod_Det & T.Lot_Lote == Lot_Lote)
                {
                    return true;
                }
                i_con += 1;
            }
            return false;
        }



        public void UpdateGrilla()
        {
            dgvlistafinal.DataSource = null;

            if (Lista_Pre_Seleccionada.Count > 0)
            {
                dgvlistafinal.DataSource = Lista_Pre_Seleccionada;
            }
          

        }

        private void btnagregarVentanaPrincipal_Click(object sender, EventArgs e)
        {
            if (Lista_Pre_Seleccionada.Count() > 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                Accion.Advertencia("No existen productos agregados en la lista.");
            }
        }

        private void BtnQuitar_Click(object sender, EventArgs e)
        {

            try
            {
                if (Lista_Pre_Seleccionada.Count > 0)
                {


                    Lista_Pre_Seleccionada.RemoveAt(gridView3.GetFocusedDataSourceRowIndex());

                    dgvlistafinal.DataSource = null;
                    if (Lista_Pre_Seleccionada.Count > 0)
                    {
                        dgvlistafinal.DataSource = Lista_Pre_Seleccionada;
                        RefreshNumeral();
                    }

                }
                else
                {
                    Accion.Advertencia("No existen productos en la lista");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Catalogo Det in Lista_Pre_Seleccionada)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void dgvdatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                txtcantidad.Select();
            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView1_CalcRowHeight(object sender, RowHeightEventArgs e)
        {
            try
            {
              if (gridView1.IsFilterRow(e.RowHandle))
                        e.RowHeight = 50;
              
            }catch(Exception ex)
            {

            }
      
        }

        private void txtcantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            //try
            //{
            //    btnanadir.Select();
            //}
            //catch (Exception ex)
            //{
            //    Accion.ErrorSistema(ex.Message);
            //}
        }

        private void btnconfirmarteclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnagregarVentanaPrincipal.PerformClick();
        }

    

        void IniciarServicios()
        {
            try
            {

                //CBtnBienes.Checked = false;
                //CBtnServicios.Checked = true;

                //if (CBtnServicios.Checked)
                //{
                Entidad_Catalogo Producto = new Entidad_Catalogo();
                Logica_Catalogo Log = new Logica_Catalogo();

                Producto.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Producto.Id_Anio = Actual_Conexion.AnioSelect;
                Producto.Id_Tipo = TipoBS;//SERVICIOS
                                            // Producto.Id_Almacen = Id_Almacen;
                ListaFiltrada.Clear();
                dgvdatos.DataSource = null;

                ListaFiltrada = Log.Consultar_Servicios(Producto);
                 
                    if (ListaFiltrada.Count > 0)
                    {
                        dgvdatos.DataSource = ListaFiltrada;
            
                       // dgvdatos.ForceInitialize();
                       // gridView1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;
                       // gridView1.FocusedColumn = gridView1.VisibleColumns[1];//gridView1.Columns["GridColumn2"];

                       // gridView1.ShowEditor();
                    }
              // }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void BtnBienesB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TipoBS = "0031";//BIEN
            EsBien = true;
            BtnBienesB.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            BtnServiciosS.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));

            IniciarBienes();
        }

        private void BtnServiciosS_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TipoBS = "0032";
            EsBien = false;
            //Bar.Appearance.BackColor
            BtnBienesB.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            BtnServiciosS.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            IniciarServicios();
        }
    }
}
