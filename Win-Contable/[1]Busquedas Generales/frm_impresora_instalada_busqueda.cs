﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_impresora_instalada_busqueda : frm_fuente
    {
        public frm_impresora_instalada_busqueda()
        {
            InitializeComponent();
        }

        private void frm_impresora_instalada_busqueda_Load(object sender, EventArgs e)
        {
            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            List<string> Impresoras = new List<string>();
            foreach (string Impre in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                dgvdatos.Rows.Add(Impre);
                //gridView1.AddNewRow();
                //int rowHandle = gridView1.GetRowHandle(gridView1.DataRowCount);
                //if (gridView1.IsNewItemRow(-1))
                //{
                //gridView1.SetRowCellValue(-1, gridView1.Columns[0], Impre);
                //}

            }
        }

        private void btnseleccion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if ((dgvdatos.RowCount > 0))
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((dgvdatos.RowCount > 0))
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter))
            {
                btnseleccion.PerformClick();
                e.SuppressKeyPress = true;
            }

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }


    }
}
