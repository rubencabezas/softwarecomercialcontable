﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion
{
    public partial class frm_cargo : Contable.frm_fuente
    {
        public frm_cargo()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_cargo_edicion f = new frm_cargo_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Cargo Ent = new Entidad_Cargo();
                    Logica_Cargo Log = new Logica_Cargo();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Cargo = null;
                    Ent.Car_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_cargo_edicion f = new frm_cargo_edicion())
            {


                f.txtdescripcion.Tag = Entidad.Id_Cargo;
                f.txtdescripcion.Text = Entidad.Car_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Cargo Ent = new Entidad_Cargo();
                    Logica_Cargo Log = new Logica_Cargo();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
   
                    Ent.Id_Cargo = f.txtdescripcion.Tag.ToString();
                    Ent.Car_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        Entidad_Cargo Entidad = new Entidad_Cargo();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        public List<Entidad_Cargo> Lista = new List<Entidad_Cargo>();
        public void Listar()
        {
            Entidad_Cargo Ent = new Entidad_Cargo();
            Logica_Cargo log = new Logica_Cargo();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Cargo = null;


            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_cargo_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
