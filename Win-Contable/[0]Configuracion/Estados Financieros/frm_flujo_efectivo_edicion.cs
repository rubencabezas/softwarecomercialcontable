﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Estados_Financieros
{
    public partial class frm_flujo_efectivo_edicion : frm_fuente
    {
        public frm_flujo_efectivo_edicion()
        {
            InitializeComponent();
        }
        public string Estado_Ven_Boton;
        public int ID;
        bool VerificarCabecera()
        {
            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}

            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}

            return true;
        }


        void LimpiarCab()
        {
             txtcodigo.ResetText();
            txtcodsmv.ResetText();
            txtdescripcion.ResetText();
            orden.Value = 0;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Flujo_Efectivo Ent = new Entidad_Flujo_Efectivo();
                    Logica_Flujo_Efectivo Log = new Logica_Flujo_Efectivo();

                    Ent.ID = ID;
                    Ent.Flujo_Efectivo_Cod = txtcodigo.Text;
                    Ent.Flujo_Efectivo_Cod_conasev = txtcodsmv.Text;
                    Ent.Flujo_Efectivo_Desc = txtdescripcion.Text.ToString();
                    Ent.Flujo_Efectivo_Grupo = Convert.ToInt32(orden.Value);


                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar_Plantilla_Flujo_Efectivo(Ent))
                        {
                            LimpiarCab();
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                    
                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar_Plantilla_Flujo_Efectivo(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_flujo_efectivo_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
               // LimpiarCab();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();

            }
        }



        public List<Entidad_Flujo_Efectivo> Lista_Modificar = new List<Entidad_Flujo_Efectivo>();
        public void ListarModificar()
        {
            Entidad_Flujo_Efectivo Ent = new Entidad_Flujo_Efectivo();
            Logica_Flujo_Efectivo log = new Logica_Flujo_Efectivo();

            //Estado = Estados.Ninguno;

            Ent.ID = ID;

            try
            {
                Lista_Modificar = log.Listar_Plantilla_Flujo_Efectivo(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Flujo_Efectivo Enti = new Entidad_Flujo_Efectivo();
                    Enti = Lista_Modificar[0];

                    ID = Enti.ID;
                    txtcodigo.Text = Enti.Flujo_Efectivo_Cod;
                    txtcodsmv.Text = Ent.Flujo_Efectivo_Cod_conasev;
                    txtdescripcion.Text = Enti.Flujo_Efectivo_Desc;
                    orden.Value = Ent.Flujo_Efectivo_Grupo;
 
                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }



    }
}
