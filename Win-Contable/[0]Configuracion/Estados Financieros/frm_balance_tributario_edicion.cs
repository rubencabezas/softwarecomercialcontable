﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Estados_Financieros
{
    public partial class frm_balance_tributario_edicion : frm_fuente
    {
        public frm_balance_tributario_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        public string tipo;
        public int ID;
        private void frm_balance_tributario_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                LimpiarCab();
 
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
 
                ListarModificar();
 
            }

        }


        public List<Entidad_Formato_Balance_Tributario> Lista_Modificar = new List<Entidad_Formato_Balance_Tributario>();
        public void ListarModificar()
        {
            Entidad_Formato_Balance_Tributario Ent = new Entidad_Formato_Balance_Tributario();
            Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();

            //Estado = Estados.Ninguno;

            Ent.ID = ID;
 
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Formato_Balance_Tributario Enti = new Entidad_Formato_Balance_Tributario();
                    Enti = Lista_Modificar[0];

                    ID = Enti.ID;
                    txtcodigo.Text = Enti.codigo;
                    txtdescripcion.Text = Enti.descripcion;
                    txtformula.Text = Enti.formula;

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }



        void LimpiarCab()
        {
            txtcodigo.ResetText();
            txtdescripcion.ResetText();
            txtformula.ResetText();
        }

        bool VerificarCabecera()
        {
            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}

            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}

            return true;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Formato_Balance_Tributario Ent = new Entidad_Formato_Balance_Tributario();
                    Logica_Formato_Balance_Tributario Log = new Logica_Formato_Balance_Tributario();

 
                    Ent.codigo = txtcodigo.Text;
                    Ent.descripcion = txtdescripcion.Text.ToString();
                    Ent.formula = txtformula.Text.ToString();
                    Ent.tipo = tipo;
                    Ent.ID = ID;

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
    }
}
