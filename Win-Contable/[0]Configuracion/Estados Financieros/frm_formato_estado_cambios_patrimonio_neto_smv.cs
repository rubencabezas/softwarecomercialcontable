﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Estados_Financieros
{
    public partial class frm_formato_estado_cambios_patrimonio_neto_smv : frm_fuente
    {
        public frm_formato_estado_cambios_patrimonio_neto_smv()
        {
            InitializeComponent();
        }

        private void frm_formato_estado_cambios_patrimonio_neto_smv_Load(object sender, EventArgs e)
        {
            Listar_Plantilla();
        }
        private List<Entidad_Flujo_Efectivo> ListRC;


        private void Listar_Plantilla()
        {
            try
            {
                Logica_Flujo_Efectivo efectivo = new Logica_Flujo_Efectivo();
                Entidad_Flujo_Efectivo efectivo2 = new Entidad_Flujo_Efectivo();
                this.ListRC = efectivo.Patrimonio_Neto_Listar(efectivo2);
                if (this.ListRC.Count > 0)
                {
                    this.dgvdatos.DataSource = this.ListRC;
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_estado_cambios_patrimonio_neto_smv_edicion f = new frm_estado_cambios_patrimonio_neto_smv_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";




                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar_Plantilla();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar_Plantilla();
                }

            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_estado_cambios_patrimonio_neto_smv_edicion f = new frm_estado_cambios_patrimonio_neto_smv_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.ID = Entidad.ID;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar_Plantilla();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar_Plantilla();
                }
            }
        }

        Entidad_Flujo_Efectivo Entidad = new Entidad_Flujo_Efectivo();
        private void gridView4_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (ListRC.Count > 0)
                {
                    Estado = Estados.Ninguno;

                    Entidad = ListRC[gridView4.GetFocusedDataSourceRowIndex()];

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Flujo_Efectivo_Cod + " " + Entidad.Flujo_Efectivo_Desc;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Flujo_Efectivo Ent = new Entidad_Flujo_Efectivo
                    {
                        ID = Entidad.ID
                    };

                    Logica_Flujo_Efectivo log = new Logica_Flujo_Efectivo();

                    log.Eliminar_Patrimonio_Neto_SMV(Ent);
                    Accion.ExitoGuardar();
                    Listar_Plantilla();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
