﻿using DevExpress.CodeParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Estados_Financieros
{
    public partial class frm_formato_balance_tributario : frm_fuente
    {
        public frm_formato_balance_tributario()
        {
            InitializeComponent();
        }

        private void frm_formato_balance_tributario_Load(object sender, EventArgs e)
        {
            Listar();
        }

        List<Entidad_Formato_Balance_Tributario> ListaFiltradaActivo = new List<Entidad_Formato_Balance_Tributario>();
        List<Entidad_Formato_Balance_Tributario> ListaFiltradaPasivo = new List<Entidad_Formato_Balance_Tributario>();
        List<Entidad_Formato_Balance_Tributario> ListaFiltradaNaturaleza = new List<Entidad_Formato_Balance_Tributario>();
        List<Entidad_Formato_Balance_Tributario> ListaFiltradaFuncion = new List<Entidad_Formato_Balance_Tributario>();

        public List<Entidad_Formato_Balance_Tributario> Lista = new List<Entidad_Formato_Balance_Tributario>();
        public void Listar()
        {
            Entidad_Formato_Balance_Tributario Ent = new Entidad_Formato_Balance_Tributario();
            Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();

   

            try
            {
                Lista = log.Listar(Ent);

                if (Lista.Count > 0)
                {
                    if (tabControl1.SelectedIndex == 0)
                    {
                      
                        var PrivView = from item in Lista
                                       where item.tipo == "A"
                                       //orderby item.Id_Anio descending
                                       select item;

                        ListaFiltradaActivo = PrivView.ToList();

                        dgvdatosa.DataSource = null;

                        dgvdatosa.DataSource = ListaFiltradaActivo;

                    }else if (tabControl1.SelectedIndex == 1)
                    {
             
                        var PrivView = from item in Lista
                                       where item.tipo == "P"
                                       //orderby item.Id_Anio descending
                                       select item;

                        ListaFiltradaPasivo = PrivView.ToList();

                        dgvdatosp.DataSource = null;
                        dgvdatosp.DataSource = ListaFiltradaPasivo;

                    }else if (tabControl1.SelectedIndex == 2)
                    {
                      
                        var PrivView = from item in Lista
                                       where item.tipo == "N"
                                       //orderby item.Id_Anio descending
                                       select item;

                        ListaFiltradaNaturaleza = PrivView.ToList();

                        dgvdatosn.DataSource = null;
                        dgvdatosn.DataSource = ListaFiltradaNaturaleza;

                    }else if (tabControl1.SelectedIndex == 3)
                    {
      

                        var PrivView = from item in Lista
                                       where item.tipo == "F"
                                       //orderby item.Id_Anio descending
                                       select item;

                        ListaFiltradaFuncion = PrivView.ToList();

                        dgvdatosf.DataSource = null;
                        dgvdatosf.DataSource = ListaFiltradaFuncion;
                    }


                }
            }
            catch (Exception ex)
            {
               Accion.ErrorSistema(ex.Message);
            }
        
       }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_balance_tributario_edicion f = new frm_balance_tributario_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (tabControl1.SelectedIndex == 0)
                {
                    f.tipo = "A";
                    f.gbtipo.Text = "ACTIVO";

                }else if (tabControl1.SelectedIndex == 1)
                {
                    f.tipo = "P";
                    f.gbtipo.Text = "PASIVO";
                }
                else if (tabControl1.SelectedIndex == 2)
                {
                    f.tipo = "N";
                    f.gbtipo.Text = "NATURALEZA";
                }
                else if (tabControl1.SelectedIndex == 3)
                {
                    f.tipo = "F";
                    f.gbtipo.Text = "FUNCION";
                }

 
                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        Entidad_Formato_Balance_Tributario Entidad = new Entidad_Formato_Balance_Tributario();
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (ListaFiltradaActivo.Count > 0)
                {
                    Estado = Estados.Ninguno;
 
                    Entidad = ListaFiltradaActivo[gridView1.GetFocusedDataSourceRowIndex()];
  
        
    
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_balance_tributario_edicion f = new frm_balance_tributario_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.ID = Entidad.ID;
                f.tipo = Entidad.tipo;

                if (tabControl1.SelectedIndex == 0)
                {
                   
                    f.gbtipo.Text = "ACTIVO";

                }
                else if (tabControl1.SelectedIndex == 1)
                {
                    
                    f.gbtipo.Text = "PASIVO";
                }
                else if (tabControl1.SelectedIndex == 2)
                {
                   
                    f.gbtipo.Text = "NATURALEZA";
                }
                else if (tabControl1.SelectedIndex == 3)
                {
                    
                    f.gbtipo.Text = "FUNCION";
                }

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        private void gridView2_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (ListaFiltradaPasivo.Count > 0)
                {
                    Estado = Estados.Ninguno;

                    Entidad = ListaFiltradaPasivo[gridView2.GetFocusedDataSourceRowIndex()];



                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView3_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (ListaFiltradaNaturaleza.Count > 0)
                {
                    Estado = Estados.Ninguno;

                    Entidad = ListaFiltradaNaturaleza[gridView3.GetFocusedDataSourceRowIndex()];



                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView4_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (ListaFiltradaFuncion.Count > 0)
                {
                    Estado = Estados.Ninguno;

                    Entidad = ListaFiltradaFuncion[gridView4.GetFocusedDataSourceRowIndex()];



                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.codigo + " " + Entidad.descripcion ;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Formato_Balance_Tributario Ent = new Entidad_Formato_Balance_Tributario
                    {
                        ID = Entidad.ID 
                    };

                    Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
