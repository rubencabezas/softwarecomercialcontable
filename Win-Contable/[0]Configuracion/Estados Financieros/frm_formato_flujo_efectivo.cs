﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Estados_Financieros
{
    public partial class frm_formato_flujo_efectivo : frm_fuente
    {
        public frm_formato_flujo_efectivo()
        {
            InitializeComponent();
        }

        private void frm_formato_flujo_efectivo_Load(object sender, EventArgs e)
        {
            Listar_Plantilla();
        }
         List<Entidad_Flujo_Efectivo> ListRC = new List<Entidad_Flujo_Efectivo>();


        private void Listar_Plantilla()
        {
            try
            {
                Logica_Flujo_Efectivo efectivo = new Logica_Flujo_Efectivo();
                Entidad_Flujo_Efectivo efectivo2 = new Entidad_Flujo_Efectivo();
                this.ListRC = efectivo.Listar_Plantilla_Flujo_Efectivo(efectivo2);
                if (this.ListRC.Count > 0)
                {
                    this.dgvdatos.DataSource = this.ListRC;
                }
            }
            catch (Exception exception)
            {
                Accion.ErrorSistema(exception.Message);
            }
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_flujo_efectivo_edicion f = new frm_flujo_efectivo_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

 


                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar_Plantilla();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar_Plantilla();
                }

            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_flujo_efectivo_edicion f = new frm_flujo_efectivo_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.ID = Entidad.ID;
 
                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar_Plantilla();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar_Plantilla();
                }
            }
        }

        Entidad_Flujo_Efectivo Entidad = new Entidad_Flujo_Efectivo();
        private void gridView4_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (ListRC.Count > 0)
                {
                    Estado = Estados.Ninguno;

                    Entidad = ListRC[gridView4.GetFocusedDataSourceRowIndex()];

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Flujo_Efectivo_Cod + " " + Entidad.Flujo_Efectivo_Desc;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Flujo_Efectivo Ent = new Entidad_Flujo_Efectivo
                    {
                        ID = Entidad.ID
                    };

                    Logica_Flujo_Efectivo log = new Logica_Flujo_Efectivo();

                    log.Eliminar_Plantilla_Flujo_Efectivo(Ent);
                    Accion.ExitoGuardar();
                    Listar_Plantilla();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
