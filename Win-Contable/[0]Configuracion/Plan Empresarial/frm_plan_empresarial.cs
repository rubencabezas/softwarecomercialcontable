﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Plan_Empresarial
{
    public partial class frm_plan_empresarial : Contable.frm_fuente
    {
        public frm_plan_empresarial()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            //{
                            using (frm_plan_empresarial_edicion f = new frm_plan_empresarial_edicion())
                        {



                            Estado = Estados.Nuevo;
                            f.Estado_Ven_Boton = "1";

                            if (f.ShowDialog() == DialogResult.OK)
                            {


                                try
                                {
                                    Listar();
                                }
                                catch (Exception ex)
                                {
                                    Accion.ErrorSistema(ex.Message);
                                }
                            }
                            else
                            {
                                Listar();
                            }

                        }
            //}



        }

        Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();
        public List<Entidad_Plan_Empresarial> Lista = new List<Entidad_Plan_Empresarial>();
        public void Listar()
        {
            Entidad_Plan_Empresarial Ent = new Entidad_Plan_Empresarial();
            Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;

            try
            {
                Lista = log.Busqueda(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_plan_empresarial_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_plan_empresarial_edicion f = new frm_plan_empresarial_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Cuenta = Entidad.Id_Cuenta;


                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La estructura a eliminar es el siguiente :" + Entidad.Id_Cuenta +" "+ Entidad.Cta_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Plan_Empresarial Ent = new Entidad_Plan_Empresarial
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Anio=Entidad.Id_Anio,
                        Id_Cuenta=Entidad.Id_Cuenta
                    };

                    Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\PlanEmpresarial" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
