﻿namespace Contable._0_Configuracion.Plan_Empresarial
{
    partial class frm_plan_empresarial_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtresdescdbalancenif = new DevExpress.XtraEditors.TextEdit();
            this.txtrecdescdbalancenif = new DevExpress.XtraEditors.TextEdit();
            this.txtrescodbalancenif = new DevExpress.XtraEditors.TextEdit();
            this.txtdescdbalancenif = new DevExpress.XtraEditors.TextEdit();
            this.txtreccodbalancenif = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtcodbalancenif = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtrecdescblancetributario = new DevExpress.XtraEditors.TextEdit();
            this.txtreccodblancetributario = new DevExpress.XtraEditors.TextEdit();
            this.txtdescblancetributario = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtcodblancetributario = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDescOblig = new DevExpress.XtraEditors.TextEdit();
            this.TxtCodOblig = new DevExpress.XtraEditors.TextEdit();
            this.txttiporegimendesc = new DevExpress.XtraEditors.TextEdit();
            this.txttiporegimencod = new DevExpress.XtraEditors.TextEdit();
            this.txttipomovimientodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txttipomovimientocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.txtestructuradesc = new DevExpress.XtraEditors.TextEdit();
            this.txtidestructura = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtelementodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtidelemento = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.rbcgasto = new DevExpress.XtraEditors.CheckEdit();
            this.rbccosto = new DevExpress.XtraEditors.CheckEdit();
            this.chkarea = new DevExpress.XtraEditors.CheckEdit();
            this.chkafecdoc = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chkinventario = new DevExpress.XtraEditors.CheckEdit();
            this.chknatrualeza = new DevExpress.XtraEditors.CheckEdit();
            this.chkfuncion = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtporcentaje = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.txtcuentadestinodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuentadestinocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtresdescdbalancenif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrecdescdbalancenif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrescodbalancenif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescdbalancenif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtreccodbalancenif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodbalancenif.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrecdescblancetributario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtreccodblancetributario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescblancetributario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodblancetributario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDescOblig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCodOblig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestructuradesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidestructura.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtelementodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidelemento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbcgasto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbccosto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkarea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkafecdoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkinventario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chknatrualeza.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkfuncion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtporcentaje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadestinodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadestinocod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(814, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 564);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(814, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 532);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(814, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 532);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupBox2);
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.TxtDescOblig);
            this.groupControl1.Controls.Add(this.TxtCodOblig);
            this.groupControl1.Controls.Add(this.txttiporegimendesc);
            this.groupControl1.Controls.Add(this.txttiporegimencod);
            this.groupControl1.Controls.Add(this.txttipomovimientodesc);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.txttipomovimientocod);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtcuentadesc);
            this.groupControl1.Controls.Add(this.txtcuenta);
            this.groupControl1.Controls.Add(this.txtestructuradesc);
            this.groupControl1.Controls.Add(this.txtidestructura);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtelementodesc);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtidelemento);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 47);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(805, 240);
            this.groupControl1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtresdescdbalancenif);
            this.groupBox2.Controls.Add(this.txtrecdescdbalancenif);
            this.groupBox2.Controls.Add(this.txtrescodbalancenif);
            this.groupBox2.Controls.Add(this.txtdescdbalancenif);
            this.groupBox2.Controls.Add(this.txtreccodbalancenif);
            this.groupBox2.Controls.Add(this.labelControl12);
            this.groupBox2.Controls.Add(this.labelControl14);
            this.groupBox2.Controls.Add(this.txtcodbalancenif);
            this.groupBox2.Controls.Add(this.labelControl13);
            this.groupBox2.Location = new System.Drawing.Point(399, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(394, 87);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Formato Balance NIF";
            // 
            // txtresdescdbalancenif
            // 
            this.txtresdescdbalancenif.Enabled = false;
            this.txtresdescdbalancenif.EnterMoveNextControl = true;
            this.txtresdescdbalancenif.Location = new System.Drawing.Point(121, 62);
            this.txtresdescdbalancenif.Name = "txtresdescdbalancenif";
            this.txtresdescdbalancenif.Size = new System.Drawing.Size(268, 20);
            this.txtresdescdbalancenif.TabIndex = 35;
            // 
            // txtrecdescdbalancenif
            // 
            this.txtrecdescdbalancenif.Enabled = false;
            this.txtrecdescdbalancenif.EnterMoveNextControl = true;
            this.txtrecdescdbalancenif.Location = new System.Drawing.Point(121, 39);
            this.txtrecdescdbalancenif.Name = "txtrecdescdbalancenif";
            this.txtrecdescdbalancenif.Size = new System.Drawing.Size(268, 20);
            this.txtrecdescdbalancenif.TabIndex = 32;
            // 
            // txtrescodbalancenif
            // 
            this.txtrescodbalancenif.EnterMoveNextControl = true;
            this.txtrescodbalancenif.Location = new System.Drawing.Point(67, 62);
            this.txtrescodbalancenif.Name = "txtrescodbalancenif";
            this.txtrescodbalancenif.Size = new System.Drawing.Size(52, 20);
            this.txtrescodbalancenif.TabIndex = 34;
            this.txtrescodbalancenif.TextChanged += new System.EventHandler(this.txtrescodbalancenif_TextChanged);
            this.txtrescodbalancenif.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrescodbalancenif_KeyDown);
            // 
            // txtdescdbalancenif
            // 
            this.txtdescdbalancenif.Enabled = false;
            this.txtdescdbalancenif.EnterMoveNextControl = true;
            this.txtdescdbalancenif.Location = new System.Drawing.Point(121, 17);
            this.txtdescdbalancenif.Name = "txtdescdbalancenif";
            this.txtdescdbalancenif.Size = new System.Drawing.Size(268, 20);
            this.txtdescdbalancenif.TabIndex = 29;
            // 
            // txtreccodbalancenif
            // 
            this.txtreccodbalancenif.EnterMoveNextControl = true;
            this.txtreccodbalancenif.Location = new System.Drawing.Point(67, 39);
            this.txtreccodbalancenif.Name = "txtreccodbalancenif";
            this.txtreccodbalancenif.Size = new System.Drawing.Size(52, 20);
            this.txtreccodbalancenif.TabIndex = 31;
            this.txtreccodbalancenif.TextChanged += new System.EventHandler(this.txtreccodbalancenif_TextChanged);
            this.txtreccodbalancenif.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtreccodbalancenif_KeyDown);
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(11, 20);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(51, 13);
            this.labelControl12.TabIndex = 27;
            this.labelControl12.Text = "C. Balance";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(13, 65);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(48, 13);
            this.labelControl14.TabIndex = 33;
            this.labelControl14.Text = "Resultado";
            // 
            // txtcodbalancenif
            // 
            this.txtcodbalancenif.EnterMoveNextControl = true;
            this.txtcodbalancenif.Location = new System.Drawing.Point(67, 17);
            this.txtcodbalancenif.Name = "txtcodbalancenif";
            this.txtcodbalancenif.Size = new System.Drawing.Size(52, 20);
            this.txtcodbalancenif.TabIndex = 28;
            this.txtcodbalancenif.TextChanged += new System.EventHandler(this.txtcodbalancenif_TextChanged);
            this.txtcodbalancenif.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcodbalancenif_KeyDown);
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(21, 42);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(41, 13);
            this.labelControl13.TabIndex = 30;
            this.labelControl13.Text = "Reclasif.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtrecdescblancetributario);
            this.groupBox1.Controls.Add(this.txtreccodblancetributario);
            this.groupBox1.Controls.Add(this.txtdescblancetributario);
            this.groupBox1.Controls.Add(this.labelControl11);
            this.groupBox1.Controls.Add(this.txtcodblancetributario);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Location = new System.Drawing.Point(11, 148);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 87);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Formato Balance Tributario";
            // 
            // txtrecdescblancetributario
            // 
            this.txtrecdescblancetributario.Enabled = false;
            this.txtrecdescblancetributario.EnterMoveNextControl = true;
            this.txtrecdescblancetributario.Location = new System.Drawing.Point(114, 42);
            this.txtrecdescblancetributario.Name = "txtrecdescblancetributario";
            this.txtrecdescblancetributario.Size = new System.Drawing.Size(268, 20);
            this.txtrecdescblancetributario.TabIndex = 25;
            // 
            // txtreccodblancetributario
            // 
            this.txtreccodblancetributario.EnterMoveNextControl = true;
            this.txtreccodblancetributario.Location = new System.Drawing.Point(60, 42);
            this.txtreccodblancetributario.Name = "txtreccodblancetributario";
            this.txtreccodblancetributario.Size = new System.Drawing.Size(52, 20);
            this.txtreccodblancetributario.TabIndex = 24;
            this.txtreccodblancetributario.TextChanged += new System.EventHandler(this.txtreccodblancetributario_TextChanged);
            this.txtreccodblancetributario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtreccodblancetributario_KeyDown);
            // 
            // txtdescblancetributario
            // 
            this.txtdescblancetributario.Enabled = false;
            this.txtdescblancetributario.EnterMoveNextControl = true;
            this.txtdescblancetributario.Location = new System.Drawing.Point(114, 20);
            this.txtdescblancetributario.Name = "txtdescblancetributario";
            this.txtdescblancetributario.Size = new System.Drawing.Size(268, 20);
            this.txtdescblancetributario.TabIndex = 22;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(14, 45);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(41, 13);
            this.labelControl11.TabIndex = 23;
            this.labelControl11.Text = "Reclasif.";
            // 
            // txtcodblancetributario
            // 
            this.txtcodblancetributario.EnterMoveNextControl = true;
            this.txtcodblancetributario.Location = new System.Drawing.Point(60, 20);
            this.txtcodblancetributario.Name = "txtcodblancetributario";
            this.txtcodblancetributario.Size = new System.Drawing.Size(52, 20);
            this.txtcodblancetributario.TabIndex = 21;
            this.txtcodblancetributario.TextChanged += new System.EventHandler(this.txtcodblancetributario_TextChanged);
            this.txtcodblancetributario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcodblancetributario_KeyDown);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(4, 23);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(51, 13);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "C. Balance";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(411, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Obligacion";
            // 
            // TxtDescOblig
            // 
            this.TxtDescOblig.Enabled = false;
            this.TxtDescOblig.EnterMoveNextControl = true;
            this.TxtDescOblig.Location = new System.Drawing.Point(522, 100);
            this.TxtDescOblig.Name = "TxtDescOblig";
            this.TxtDescOblig.Size = new System.Drawing.Size(271, 20);
            this.TxtDescOblig.TabIndex = 15;
            // 
            // TxtCodOblig
            // 
            this.TxtCodOblig.EnterMoveNextControl = true;
            this.TxtCodOblig.Location = new System.Drawing.Point(476, 100);
            this.TxtCodOblig.Name = "TxtCodOblig";
            this.TxtCodOblig.Size = new System.Drawing.Size(43, 20);
            this.TxtCodOblig.TabIndex = 14;
            this.TxtCodOblig.TextChanged += new System.EventHandler(this.TxtCodOblig_TextChanged);
            this.TxtCodOblig.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtCodOblig_KeyDown);
            // 
            // txttiporegimendesc
            // 
            this.txttiporegimendesc.Enabled = false;
            this.txttiporegimendesc.EnterMoveNextControl = true;
            this.txttiporegimendesc.Location = new System.Drawing.Point(166, 122);
            this.txttiporegimendesc.Name = "txttiporegimendesc";
            this.txttiporegimendesc.Size = new System.Drawing.Size(238, 20);
            this.txttiporegimendesc.TabIndex = 18;
            // 
            // txttiporegimencod
            // 
            this.txttiporegimencod.EnterMoveNextControl = true;
            this.txttiporegimencod.Location = new System.Drawing.Point(120, 122);
            this.txttiporegimencod.Name = "txttiporegimencod";
            this.txttiporegimencod.Size = new System.Drawing.Size(43, 20);
            this.txttiporegimencod.TabIndex = 17;
            this.txttiporegimencod.TextChanged += new System.EventHandler(this.txttiporegimencod_TextChanged);
            this.txttiporegimencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttiporegimencod_KeyDown);
            // 
            // txttipomovimientodesc
            // 
            this.txttipomovimientodesc.Enabled = false;
            this.txttipomovimientodesc.EnterMoveNextControl = true;
            this.txttipomovimientodesc.Location = new System.Drawing.Point(166, 100);
            this.txttipomovimientodesc.Name = "txttipomovimientodesc";
            this.txttipomovimientodesc.Size = new System.Drawing.Size(238, 20);
            this.txttipomovimientodesc.TabIndex = 12;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(73, 125);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(41, 13);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Regimen";
            // 
            // txttipomovimientocod
            // 
            this.txttipomovimientocod.EnterMoveNextControl = true;
            this.txttipomovimientocod.Location = new System.Drawing.Point(120, 100);
            this.txttipomovimientocod.MenuManager = this.barManager1;
            this.txttipomovimientocod.Name = "txttipomovimientocod";
            this.txttipomovimientocod.Size = new System.Drawing.Size(43, 20);
            this.txttipomovimientocod.TabIndex = 11;
            this.txttipomovimientocod.TextChanged += new System.EventHandler(this.txttipomovimientocod_TextChanged);
            this.txttipomovimientocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipomovimientocod_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(15, 103);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(99, 13);
            this.labelControl8.TabIndex = 10;
            this.labelControl8.Text = "Proceso de tesoreria";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(140, 80);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(58, 13);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "Descripcion:";
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(204, 78);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuentadesc.Size = new System.Drawing.Size(589, 20);
            this.txtcuentadesc.TabIndex = 9;
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(63, 78);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Size = new System.Drawing.Size(71, 20);
            this.txtcuenta.TabIndex = 7;
            this.txtcuenta.EditValueChanged += new System.EventHandler(this.txtcuenta_EditValueChanged);
            this.txtcuenta.TextChanged += new System.EventHandler(this.txtcuenta_TextChanged);
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // txtestructuradesc
            // 
            this.txtestructuradesc.Enabled = false;
            this.txtestructuradesc.Location = new System.Drawing.Point(110, 54);
            this.txtestructuradesc.Name = "txtestructuradesc";
            this.txtestructuradesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtestructuradesc.Properties.Appearance.Options.UseFont = true;
            this.txtestructuradesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestructuradesc.Size = new System.Drawing.Size(683, 20);
            this.txtestructuradesc.TabIndex = 5;
            // 
            // txtidestructura
            // 
            this.txtidestructura.Enabled = false;
            this.txtidestructura.Location = new System.Drawing.Point(63, 54);
            this.txtidestructura.Name = "txtidestructura";
            this.txtidestructura.Size = new System.Drawing.Size(43, 20);
            this.txtidestructura.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 81);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Cuenta:";
            // 
            // txtelementodesc
            // 
            this.txtelementodesc.Enabled = false;
            this.txtelementodesc.Location = new System.Drawing.Point(110, 31);
            this.txtelementodesc.Name = "txtelementodesc";
            this.txtelementodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtelementodesc.Properties.Appearance.Options.UseFont = true;
            this.txtelementodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtelementodesc.Size = new System.Drawing.Size(683, 20);
            this.txtelementodesc.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Estructura:";
            // 
            // txtidelemento
            // 
            this.txtidelemento.Enabled = false;
            this.txtidelemento.Location = new System.Drawing.Point(63, 31);
            this.txtidelemento.MenuManager = this.barManager1;
            this.txtidelemento.Name = "txtidelemento";
            this.txtidelemento.Size = new System.Drawing.Size(43, 20);
            this.txtidelemento.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Elemento:";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.rbcgasto);
            this.groupControl2.Controls.Add(this.rbccosto);
            this.groupControl2.Controls.Add(this.chkarea);
            this.groupControl2.Controls.Add(this.chkafecdoc);
            this.groupControl2.Location = new System.Drawing.Point(2, 292);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(478, 52);
            this.groupControl2.TabIndex = 1;
            // 
            // rbcgasto
            // 
            this.rbcgasto.EnterMoveNextControl = true;
            this.rbcgasto.Location = new System.Drawing.Point(299, 24);
            this.rbcgasto.Name = "rbcgasto";
            this.rbcgasto.Properties.Caption = "Centro de Gasto";
            this.rbcgasto.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.rbcgasto.Size = new System.Drawing.Size(105, 20);
            this.rbcgasto.TabIndex = 4;
            this.rbcgasto.CheckedChanged += new System.EventHandler(this.rbcgasto_CheckedChanged);
            // 
            // rbccosto
            // 
            this.rbccosto.EnterMoveNextControl = true;
            this.rbccosto.Location = new System.Drawing.Point(184, 24);
            this.rbccosto.Name = "rbccosto";
            this.rbccosto.Properties.Caption = "Centro de Costo";
            this.rbccosto.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.rbccosto.Size = new System.Drawing.Size(109, 20);
            this.rbccosto.TabIndex = 3;
            this.rbccosto.CheckedChanged += new System.EventHandler(this.rbccosto_CheckedChanged);
            // 
            // chkarea
            // 
            this.chkarea.EnterMoveNextControl = true;
            this.chkarea.Location = new System.Drawing.Point(131, 24);
            this.chkarea.Name = "chkarea";
            this.chkarea.Properties.Caption = "Area";
            this.chkarea.Size = new System.Drawing.Size(75, 20);
            this.chkarea.TabIndex = 2;
            // 
            // chkafecdoc
            // 
            this.chkafecdoc.EnterMoveNextControl = true;
            this.chkafecdoc.Location = new System.Drawing.Point(9, 24);
            this.chkafecdoc.MenuManager = this.barManager1;
            this.chkafecdoc.Name = "chkafecdoc";
            this.chkafecdoc.Properties.Caption = "Afecto a documento";
            this.chkafecdoc.Size = new System.Drawing.Size(127, 20);
            this.chkafecdoc.TabIndex = 1;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.chkinventario);
            this.groupControl3.Controls.Add(this.chknatrualeza);
            this.groupControl3.Controls.Add(this.chkfuncion);
            this.groupControl3.Location = new System.Drawing.Point(486, 293);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(319, 51);
            this.groupControl3.TabIndex = 2;
            // 
            // chkinventario
            // 
            this.chkinventario.EnterMoveNextControl = true;
            this.chkinventario.Location = new System.Drawing.Point(207, 23);
            this.chkinventario.Name = "chkinventario";
            this.chkinventario.Properties.Caption = "Inventario";
            this.chkinventario.Size = new System.Drawing.Size(75, 20);
            this.chkinventario.TabIndex = 3;
            // 
            // chknatrualeza
            // 
            this.chknatrualeza.EnterMoveNextControl = true;
            this.chknatrualeza.Location = new System.Drawing.Point(105, 23);
            this.chknatrualeza.Name = "chknatrualeza";
            this.chknatrualeza.Properties.Caption = "Naturaleza";
            this.chknatrualeza.Size = new System.Drawing.Size(75, 20);
            this.chknatrualeza.TabIndex = 2;
            // 
            // chkfuncion
            // 
            this.chkfuncion.EnterMoveNextControl = true;
            this.chkfuncion.Location = new System.Drawing.Point(16, 23);
            this.chkfuncion.Name = "chkfuncion";
            this.chkfuncion.Properties.Caption = "Funcion";
            this.chkfuncion.Size = new System.Drawing.Size(72, 20);
            this.chkfuncion.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.simpleButton2);
            this.groupControl4.Controls.Add(this.simpleButton1);
            this.groupControl4.Controls.Add(this.dgvdatos);
            this.groupControl4.Controls.Add(this.txtporcentaje);
            this.groupControl4.Controls.Add(this.labelControl5);
            this.groupControl4.Controls.Add(this.txttipodesc);
            this.groupControl4.Controls.Add(this.txttipo);
            this.groupControl4.Controls.Add(this.txtcuentadestinodesc);
            this.groupControl4.Controls.Add(this.labelControl7);
            this.groupControl4.Controls.Add(this.txtcuentadestinocod);
            this.groupControl4.Controls.Add(this.labelControl4);
            this.groupControl4.Location = new System.Drawing.Point(2, 347);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(803, 210);
            this.groupControl4.TabIndex = 3;
            this.groupControl4.Text = "Cargo";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(475, 46);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 10;
            this.simpleButton2.Text = "Quitar";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(381, 46);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Text = "Añadir";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(7, 74);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(710, 125);
            this.dgvdatos.TabIndex = 8;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Cuenta";
            this.gridColumn1.FieldName = "Id_Cuenta_Destino";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 98;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Descripcion";
            this.gridColumn2.FieldName = "Cta_Descripcion_Destino";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 473;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Tipo";
            this.gridColumn3.FieldName = "Ctb_Tipo_DH_Desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "% Debe";
            this.gridColumn4.FieldName = "Porcen_Debe";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 89;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "% Haber";
            this.gridColumn5.FieldName = "Porcen_Haber";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 86;
            // 
            // txtporcentaje
            // 
            this.txtporcentaje.Location = new System.Drawing.Point(264, 48);
            this.txtporcentaje.MenuManager = this.barManager1;
            this.txtporcentaje.Name = "txtporcentaje";
            this.txtporcentaje.Size = new System.Drawing.Size(100, 20);
            this.txtporcentaje.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(202, 50);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(56, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Porcentaje:";
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(104, 47);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttipodesc.Properties.Appearance.Options.UseFont = true;
            this.txttipodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodesc.Size = new System.Drawing.Size(92, 20);
            this.txttipodesc.TabIndex = 5;
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(52, 47);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(51, 20);
            this.txttipo.TabIndex = 4;
            this.txttipo.EditValueChanged += new System.EventHandler(this.txttipocargo_EditValueChanged);
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocargo_KeyDown);
            // 
            // txtcuentadestinodesc
            // 
            this.txtcuentadestinodesc.Enabled = false;
            this.txtcuentadestinodesc.EnterMoveNextControl = true;
            this.txtcuentadestinodesc.Location = new System.Drawing.Point(104, 23);
            this.txtcuentadestinodesc.Name = "txtcuentadestinodesc";
            this.txtcuentadestinodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcuentadestinodesc.Properties.Appearance.Options.UseFont = true;
            this.txtcuentadestinodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuentadestinodesc.Size = new System.Drawing.Size(687, 20);
            this.txtcuentadestinodesc.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(22, 49);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(24, 13);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "Tipo:";
            // 
            // txtcuentadestinocod
            // 
            this.txtcuentadestinocod.EnterMoveNextControl = true;
            this.txtcuentadestinocod.Location = new System.Drawing.Point(52, 23);
            this.txtcuentadestinocod.MenuManager = this.barManager1;
            this.txtcuentadestinocod.Name = "txtcuentadestinocod";
            this.txtcuentadestinocod.Size = new System.Drawing.Size(51, 20);
            this.txtcuentadestinocod.TabIndex = 1;
            this.txtcuentadestinocod.EditValueChanged += new System.EventHandler(this.txtcuentacargo_EditValueChanged);
            this.txtcuentadestinocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuentacargo_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(7, 26);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Cuenta:";
            // 
            // frm_plan_empresarial_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(814, 564);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_plan_empresarial_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Plan Empresarial";
            this.Load += new System.EventHandler(this.frm_plan_empresarial_edicion_Load);
            this.Shown += new System.EventHandler(this.frm_plan_empresarial_edicion_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtresdescdbalancenif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrecdescdbalancenif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrescodbalancenif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescdbalancenif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtreccodbalancenif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodbalancenif.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrecdescblancetributario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtreccodblancetributario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescblancetributario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodblancetributario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDescOblig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCodOblig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttiporegimencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipomovimientocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestructuradesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidestructura.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtelementodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidelemento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbcgasto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbccosto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkarea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkafecdoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkinventario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chknatrualeza.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkfuncion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtporcentaje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadestinodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadestinocod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit chkinventario;
        private DevExpress.XtraEditors.CheckEdit chknatrualeza;
        private DevExpress.XtraEditors.CheckEdit chkfuncion;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.TextEdit txtcuentadestinodesc;
        private DevExpress.XtraEditors.TextEdit txtcuentadestinocod;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit chkarea;
        private DevExpress.XtraEditors.CheckEdit chkafecdoc;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.TextEdit txttipo;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.CheckEdit rbcgasto;
        private DevExpress.XtraEditors.CheckEdit rbccosto;
        public DevExpress.XtraEditors.TextEdit txtcuentadesc;
        public DevExpress.XtraEditors.TextEdit txtcuenta;
        public DevExpress.XtraEditors.TextEdit txtestructuradesc;
        public DevExpress.XtraEditors.TextEdit txtidestructura;
        public DevExpress.XtraEditors.TextEdit txtelementodesc;
        public DevExpress.XtraEditors.TextEdit txtidelemento;
        public DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtporcentaje;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.TextEdit txttipomovimientodesc;
        private DevExpress.XtraEditors.TextEdit txttipomovimientocod;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtDescOblig;
        private DevExpress.XtraEditors.TextEdit TxtCodOblig;
        private DevExpress.XtraEditors.TextEdit txttiporegimendesc;
        private DevExpress.XtraEditors.TextEdit txttiporegimencod;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.TextEdit txtresdescdbalancenif;
        private DevExpress.XtraEditors.TextEdit txtrecdescdbalancenif;
        private DevExpress.XtraEditors.TextEdit txtrescodbalancenif;
        private DevExpress.XtraEditors.TextEdit txtdescdbalancenif;
        private DevExpress.XtraEditors.TextEdit txtreccodbalancenif;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtcodbalancenif;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtrecdescblancetributario;
        private DevExpress.XtraEditors.TextEdit txtreccodblancetributario;
        private DevExpress.XtraEditors.TextEdit txtdescblancetributario;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtcodblancetributario;
        private DevExpress.XtraEditors.LabelControl labelControl10;
    }
}
