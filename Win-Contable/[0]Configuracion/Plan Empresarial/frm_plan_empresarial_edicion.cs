﻿using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Plan_Empresarial
{
    public partial class frm_plan_empresarial_edicion : Contable.frm_fuente
    {
        public frm_plan_empresarial_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        string Cta = null;
        public string Id_empresa, Id_Anio, Id_Cuenta;

        public void LimpiarCab()
        {
            txtelementodesc.ResetText();
            txtidelemento.ResetText();
            txtestructuradesc.ResetText();
            txtidestructura.ResetText();
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();
            chkafecdoc.Checked = false;
            chkarea.Checked = false;
            chkfuncion.Checked = false;
            chkinventario.Checked = false;
            chknatrualeza.Checked = false;
            rbccosto.Checked = false;
            rbcgasto.Checked = false;

        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                Limpiar();
                ListCtaDestino.Clear();
                dgvdatos.DataSource = null;
            
                txtcuenta.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        public void TraerDatos()
        {

            if (!string.IsNullOrEmpty(txtcuenta.Text))
            {
                try
                {
                    Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                    //Entidad_Plan_General Ent = /*default*/(Entidad_Plan_General);


                    Entidad_Plan_Empresarial en = new Entidad_Plan_Empresarial();

                    Cta = null;
                    for (int i = 0; i <= txtcuenta.Text.Trim().Length - 1; i++)
                    {
                        string Car = txtcuenta.Text.Substring(i, 1);
                        if (Car != " ")
                        {
                            Cta = Cta + Car;
                        }
                    }
                    //Ent_Cta.Cuenta = Cta
                    Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();
                    en.Id_Cuenta = Cta;

                    Entidad = log.TraerDatos(en);
                    

                    txtidelemento.Text = Entidad.Id_Elemento;
                    txtelementodesc.Text = Entidad.Ele_Descripcion;
                    txtidestructura.Text = Entidad.Id_Estructura;
                    txtestructuradesc.Text = Entidad.Est_Descripcion;
                    txtcuentadesc.Text = Entidad.Cta_Descripcion;

                   

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);

                }
            }

        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text.Trim()))
            {
                TraerDatos();

                txtcuenta.Focus();
            }
        }

        private void txtcuenta_EditValueChanged(object sender, EventArgs e)
        {
        
        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            //if (txtcuenta.Focused==false)
            //{
            //    txtcuentadesc.Text="";
            //    txtidelemento.Text = "";
            //    txtelementodesc.Text = "";
            //    txtidestructura.Text = "";
            //    txtestructuradesc.Text = "";


            //}
        }

        private void frm_plan_empresarial_edicion_Shown(object sender, EventArgs e)
        {
            txtcuenta.Focus();
        }

        private void txttipocargo_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txttipocargo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipo.Text.Substring(txttipo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0002";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipo.Tag = Entidad.Id_General_Det;
                                txttipo.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipo.Text) & string.IsNullOrEmpty(txttipodesc.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipo()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0002"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Id_General).ToString().Trim().ToUpper() == txttipo.Text.Trim().ToUpper())
                        {
                            txttipo.Tag = T.Id_General_Det;
                            txttipo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoabono_EditValueChanged(object sender, EventArgs e)
        {

        }

     

       

        private void txtcuentacargo_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtcuentacargo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuentadestinocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuentadestinocod.Text.Substring(txtcuentadestinocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuentadestinocod.Text = Entidad.Id_Cuenta;
                                txtcuentadestinodesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuentadestinocod.Text) & string.IsNullOrEmpty(txtcuentadestinodesc.Text))
                    {
                        CuentaCargo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void CuentaCargo()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuentadestinocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuentadestinocod.Text.Trim().ToUpper())
                        {
                            txtcuentadestinocod.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadestinodesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcuentaabono_EditValueChanged(object sender, EventArgs e)
        {

        }

      

      

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Estado = Estados.Nuevo;

            Entidad_Plan_Empresarial Ent = new Entidad_Plan_Empresarial();
            Logica_Plan_Empresarial Log = new Logica_Plan_Empresarial();

            if (VerificarCta()==true)
            {
            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;

            Ent.Id_Elemento = txtidelemento.Text;
            Ent.Id_Estructura = txtidestructura.Text;
                        
            Ent.Id_Cuenta = txtcuenta.Text;
            Ent.Cta_Descripcion = txtcuentadesc.Text;
            Ent.Cta_Afecto_Doc = chkafecdoc.Checked;
            Ent.Cta_Area = chkarea.Checked;
            Ent.Cta_Centro_Costo = rbccosto.Checked;
            Ent.Cta_Centro_Gasto = rbcgasto.Checked;
            Ent.Cta_Funcion = chkfuncion.Checked;
            Ent.Cta_Naturaleza = chknatrualeza.Checked;
            Ent.Cta_Inventario = chkinventario.Checked;



   
               Ent.Cod_Proceso = txttipomovimientocod.Text;


                if (txttipomovimientodesc.Text == "")
                {
                    Ent.Tag_Proceso = "";
                }
                else
                {
                    Ent.Tag_Proceso = txttipomovimientocod.Tag.ToString();
                }
                
                Ent.Proceso_Desc = txttipomovimientodesc.Text;


                Ent.Cod_Obligacion = TxtCodOblig.Text;

                if (TxtDescOblig.Text == "" )
                {
                    Ent.Tag_Obligacion = "";
                }
                else
                {
                    Ent.Tag_Obligacion = TxtCodOblig.Tag.ToString();
                }
                Ent.Obligacion_Desc = TxtDescOblig.Text;



  

                if (txttiporegimendesc.Text == "")
                {
                    Ent.Cta_Afecto_Regimen_tag = "";
                }
                else
                {
                     Ent.Cta_Afecto_Regimen_tag = txttiporegimencod.Tag.ToString();
                }


                Ent.Cta_Cod_Bal_Tributario =txtcodblancetributario.Text;
                Ent.Cta_Cod_Bal_Rec_Tributario= txtreccodblancetributario.Text;
                Ent.Cta_Cod_Bal_Nif_Tributario= txtcodbalancenif.Text;
                Ent.Cta_Cod_Bal_Rec_Nif_Tributario= txtreccodbalancenif.Text;
                Ent.Cta_Cod_Bal_Res_Nif_Tributario= txtrescodbalancenif.Text;

 


                Ent.DetalleCta_Destino = ListCtaDestino;

            try
            {
                if (Estado == Estados.Nuevo)
                {

                    if (Log.Insertar(Ent))
                    {
                      Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            ListCtaDestino.Clear();
                            dgvdatos.DataSource = null;
                            txtcuenta.Focus();
                      
                        }
                    else
                    {
                       Accion.Advertencia("No se puedo insertar");
                    }
                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            catch (Exception ex)
            {
                    Accion.ErrorSistema(ex.Message);
                }

            }
           
        }



        public bool VerificarCta()
        {
            if (string.IsNullOrEmpty(txtelementodesc.Text))
            {
               
                Accion.Advertencia("Ingrese una cuenta");
                txtcuenta.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcuenta.Text))
            {
                Accion.Advertencia("Ingrese una cuenta contable");
                txtcuenta.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtcuentadesc.Text))
            {
                Accion.Advertencia("Seleccione la Estructura de la Cuenta Contable");
                txtcuentadesc.Focus();
                return false;
            }

            return true;
        }

        public bool VerificarDestinos()
        {
            if (string.IsNullOrEmpty(txtcuentadestinodesc.Text))
            {

                Accion.Advertencia("Ingrese una cuenta de destino");
                txtcuentadestinocod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttipodesc.Text))
            {

                Accion.Advertencia("Ingrese un tipo (Debe / Haber)");
                txttipodesc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtporcentaje.Text))
            {

                Accion.Advertencia("Ingrese un porcentaje (0 - 100 %)");
                txtporcentaje.Focus();
                return false;
            }
            return true;

        }
        private void rbccosto_CheckedChanged(object sender, EventArgs e)
        {
            if (rbcgasto.Checked == true)
            {
                rbcgasto.Checked = false;
            }
            else
            {
                rbccosto.Checked = true;
            }
        }

        private void rbcgasto_CheckedChanged(object sender, EventArgs e)
        {
            if (rbccosto.Checked == true)
            {
                rbccosto.Checked = false;
            }
            else
            {
                rbcgasto.Checked = true;
            }
        }

        List<Entidad_Plan_Empresarial> ListCtaDestino = new List<Entidad_Plan_Empresarial>();
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if ((VerificarDestinos() == true))
            {
                Entidad_Plan_Empresarial CtaDestino = new Entidad_Plan_Empresarial();

                CtaDestino.Id_Cuenta_Destino = txtcuentadestinocod.Text.Trim();
                CtaDestino.Cta_Descripcion_Destino = txtcuentadestinodesc.Text.Trim();

                CtaDestino.CCtb_Tipo_DH_Interno = txttipo.Text.Trim();
                CtaDestino.Ctb_Tipo_DH = txttipo.Tag.ToString();
                CtaDestino.Ctb_Tipo_DH_Desc = txttipodesc.Text.Trim();

                if ((txttipo.Tag.ToString() == "0004"))
                {
                    CtaDestino.Porcen_Debe = Convert.ToDecimal( txtporcentaje.Text);
                    CtaDestino.Porcen_Haber = 0;
                }
                else
                {
                    CtaDestino.Porcen_Debe = 0;
                    CtaDestino.Porcen_Haber = Convert.ToDecimal(txtporcentaje.Text);
                }

                ListCtaDestino.Add(CtaDestino);
             
                dgvdatos.DataSource = null;
                dgvdatos.DataSource = ListCtaDestino;
                //Log_CtaContable log = new Log_CtaContable();

                //List<double> sumas = new List<double>();
                //sumas = log.DevuelvePorc(ListCtaDestino);
     
                Limpiar();
                txtcuentadestinocod.Focus();
            }
            else
            {
                Accion.Advertencia("");
            }
        }

        public void Limpiar()
        {
            txtcuentadestinocod.ResetText();
            txtcuentadestinodesc.ResetText();
            txttipo.ResetText();
            txttipodesc.ResetText();
            txtporcentaje.ResetText();
        }

        string CodDetCta;
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if ((ListCtaDestino.Count > 0))
            {
                foreach (Entidad_Plan_Empresarial Det in ListCtaDestino)
                {
                    if ((Det.Id_Cuenta == CodDetCta))
                    {
                        ListCtaDestino.Remove(Det);
                     
                        break;
                    }

                }

                dgvdatos.DataSource = null;
                dgvdatos.DataSource = ListCtaDestino;
            }

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if ((ListCtaDestino.Count > 0))
                {
                    if ((gridView1.FocusedRowHandle >= 0))
                    {
                        Entidad_Plan_Empresarial Especificaciones = new Entidad_Plan_Empresarial();
                        Especificaciones = ListCtaDestino[gridView1.GetFocusedDataSourceRowIndex()];
                        // Lista[gridView1.GetFocusedDataSourceRowIndex()];
                        CodDetCta = Especificaciones.Id_Cuenta;
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_plan_empresarial_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();
            }
        }

        public List<Entidad_Plan_Empresarial> Lista_Modificar = new List<Entidad_Plan_Empresarial>();

        private void txttipomovimientocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipomovimientocod.Focus() == false)
            {
                txttipomovimientodesc.ResetText();

            }
        }

        private void txttipomovimientocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipomovimientocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipomovimientocod.Text.Substring(txttipomovimientocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0016";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipomovimientocod.Tag = Entidad.Id_General_Det;
                                txttipomovimientocod.Text = Entidad.Gen_Codigo_Interno;
                                txttipomovimientodesc.Text = Entidad.Gen_Descripcion_Det;

 
                                txttipomovimientocod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipomovimientocod.Text) & string.IsNullOrEmpty(txttipomovimientodesc.Text))
                    {
                        BuscarMovimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMovimiento()
        {
            try
            {
                txttipomovimientocod.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0016",
                    Gen_Codigo_Interno = txttipomovimientocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipomovimientocod.Text.Trim().ToUpper())
                        {
                            txttipomovimientocod.Tag = T.Id_General_Det;
                            txttipomovimientocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipomovimientodesc.Text = T.Gen_Descripcion_Det;


                            txttipomovimientocod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipomovimientocod.EnterMoveNextControl = false;
                    txttipomovimientocod.ResetText();
                    txttipomovimientocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtCodOblig_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(TxtCodOblig.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & TxtCodOblig.Text.Substring(TxtCodOblig.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "2201";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                TxtCodOblig.Tag = Entidad.Id_General_Det;
                                TxtCodOblig.Text = Entidad.Gen_Codigo_Interno;
                                TxtDescOblig.Text = Entidad.Gen_Descripcion_Det;


                                TxtCodOblig.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(TxtCodOblig.Text) & string.IsNullOrEmpty(TxtDescOblig.Text))
                    {
                        BuscarOblogacion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }

        }


        public void BuscarOblogacion()
        {
            try
            {
                TxtCodOblig.Text = Accion.Formato(TxtCodOblig.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "2201",
                    Gen_Codigo_Interno = TxtCodOblig.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == TxtCodOblig.Text.Trim().ToUpper())
                        {
                            TxtCodOblig.Tag = T.Id_General_Det;
                            TxtCodOblig.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            TxtDescOblig.Text = T.Gen_Descripcion_Det;


                            TxtCodOblig.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    TxtCodOblig.EnterMoveNextControl = false;
                    TxtCodOblig.ResetText();
                    TxtCodOblig.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttiporegimencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttiporegimencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttiporegimencod.Text.Substring(txttiporegimencod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0013";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttiporegimencod.Tag = Entidad.Id_General_Det;
                                txttiporegimencod.Text = Entidad.Gen_Codigo_Interno;
                                txttiporegimendesc.Text = Entidad.Gen_Descripcion_Det;
                                txttiporegimencod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttiporegimencod.Text) & string.IsNullOrEmpty(txttiporegimendesc.Text))
                    {
                        BuscarTiporEGIMEN();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTiporEGIMEN()
        {
            try
            {
                txttiporegimencod.Text = Accion.Formato(txttiporegimencod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0013",
                    Gen_Codigo_Interno = txttiporegimencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Id_General).ToString().Trim().ToUpper() == txttiporegimencod.Text.Trim().ToUpper())
                        {
                            txttiporegimencod.Tag = T.Id_General_Det;
                            txttiporegimencod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttiporegimendesc.Text = T.Gen_Descripcion_Det;
                            txttiporegimencod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttiporegimencod.EnterMoveNextControl = false;
                    txttiporegimencod.ResetText();
                    txttiporegimencod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttiporegimencod_TextChanged(object sender, EventArgs e)
        {
            if (txttiporegimencod.Focus() == false)
            {
             
                txttiporegimendesc.ResetText();
            }
        }

        private void TxtCodOblig_TextChanged(object sender, EventArgs e)
        {
            if (TxtCodOblig.Focus() == false)
            {
               
                TxtDescOblig.ResetText();

            }
        }

        private void txtcodblancetributario_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcodblancetributario.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcodblancetributario.Text.Substring(txtcodblancetributario.Text.Length - 1, 1) == "*")
                    {
                        using (frm_buscar_formato_balance_tributario f = new frm_buscar_formato_balance_tributario())
                        {
                           

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Formato_Balance_Tributario Entidad = new Entidad_Formato_Balance_Tributario();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtcodblancetributario.Text = Entidad.codigo;
                                txtdescblancetributario.Text = Entidad.descripcion;


                                txtcodblancetributario.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcodblancetributario.Text) & string.IsNullOrEmpty(txtdescblancetributario.Text))
                    {
                        BuscarBalanceTributario();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBalanceTributario()
        {
            try
            {
                //txtcodblancetributario.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();

                List<Entidad_Formato_Balance_Tributario> Generales = new List<Entidad_Formato_Balance_Tributario>();
                Generales = log.Listar(new Entidad_Formato_Balance_Tributario
                {
                       codigo = txtcodblancetributario.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Formato_Balance_Tributario T in Generales)
                    {
                        if ((T.codigo).ToString().Trim().ToUpper() == txtcodblancetributario.Text.Trim().ToUpper())
                        {

                            txtcodblancetributario.Text = (T.codigo).ToString().Trim();
                            txtdescblancetributario.Text = T.descripcion;


                            txtcodblancetributario.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcodblancetributario.EnterMoveNextControl = false;
                    txtcodblancetributario.ResetText();
                    txtcodblancetributario.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtreccodblancetributario_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtreccodblancetributario.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtreccodblancetributario.Text.Substring(txtreccodblancetributario.Text.Length - 1, 1) == "*")
                    {
                        using (frm_buscar_formato_balance_tributario f = new frm_buscar_formato_balance_tributario())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Formato_Balance_Tributario Entidad = new Entidad_Formato_Balance_Tributario();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtreccodblancetributario.Text = Entidad.codigo;
                                txtrecdescblancetributario.Text = Entidad.descripcion;


                                txtreccodblancetributario.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtreccodblancetributario.Text) & string.IsNullOrEmpty(txtrecdescblancetributario.Text))
                    {
                        BuscarBalanceTributarioRec();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBalanceTributarioRec()
        {
            try
            {
                //txtcodblancetributario.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_Formato_Balance_Tributario log = new Logica_Formato_Balance_Tributario();

                List<Entidad_Formato_Balance_Tributario> Generales = new List<Entidad_Formato_Balance_Tributario>();
                Generales = log.Listar(new Entidad_Formato_Balance_Tributario
                {
                    codigo = txtreccodblancetributario.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Formato_Balance_Tributario T in Generales)
                    {
                        if ((T.codigo).ToString().Trim().ToUpper() == txtreccodblancetributario.Text.Trim().ToUpper())
                        {

                            txtreccodblancetributario.Text = (T.codigo).ToString().Trim();
                            txtrecdescblancetributario.Text = T.descripcion;


                            txtreccodblancetributario.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtreccodblancetributario.EnterMoveNextControl = false;
                    txtreccodblancetributario.ResetText();
                    txtreccodblancetributario.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcodblancetributario_TextChanged(object sender, EventArgs e)
        {
            if (txtcodblancetributario.Focus() == false)
            {

                txtdescblancetributario.ResetText();
            }
        }

        private void txtreccodblancetributario_TextChanged(object sender, EventArgs e)
        {
            if (txtreccodblancetributario.Focus() == false)
            {

                txtrecdescblancetributario.ResetText();
            }
        }

        private void txtcodbalancenif_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcodbalancenif.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcodbalancenif.Text.Substring(txtcodbalancenif.Text.Length - 1, 1) == "*")
                    {
                        using (frm_buscar_formato_estado_finan_smv f = new frm_buscar_formato_estado_finan_smv())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Entidad = new Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtcodbalancenif.Text = Entidad.codigo;
                                txtdescdbalancenif.Text = Entidad.descripcion;


                                txtcodbalancenif.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcodbalancenif.Text) & string.IsNullOrEmpty(txtdescdbalancenif.Text))
                    {
                        BuscarBalanceNif();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBalanceNif()
        {
            try
            {
                //txtcodblancetributario.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados log = new Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados();

                List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados> Generales = new List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados>();
                Generales = log.Listar(new Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados
                {
                    codigo = txtcodbalancenif.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados T in Generales)
                    {
                        if ((T.codigo).ToString().Trim().ToUpper() == txtcodbalancenif.Text.Trim().ToUpper())
                        {

                            txtcodbalancenif.Text = (T.codigo).ToString().Trim();
                            txtdescdbalancenif.Text = T.descripcion;


                            txtcodbalancenif.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcodbalancenif.EnterMoveNextControl = false;
                    txtcodbalancenif.ResetText();
                    txtcodbalancenif.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcodbalancenif_TextChanged(object sender, EventArgs e)
        {
            if (txtcodbalancenif.Focus() == false)
            {

                txtdescdbalancenif.ResetText();
            }
        }

        private void txtreccodbalancenif_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtreccodbalancenif.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtreccodbalancenif.Text.Substring(txtreccodbalancenif.Text.Length - 1, 1) == "*")
                    {
                        using (frm_buscar_formato_estado_finan_smv f = new frm_buscar_formato_estado_finan_smv())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Entidad = new Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtreccodbalancenif.Text = Entidad.codigo;
                                txtrecdescdbalancenif.Text = Entidad.descripcion;


                                txtreccodbalancenif.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtreccodbalancenif.Text) & string.IsNullOrEmpty(txtrecdescdbalancenif.Text))
                    {
                        BuscarBalanceRecNif();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBalanceRecNif()
        {
            try
            {
                //txtcodblancetributario.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados log = new Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados();

                List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados> Generales = new List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados>();
                Generales = log.Listar(new Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados
                {
                    codigo = txtreccodbalancenif.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados T in Generales)
                    {
                        if ((T.codigo).ToString().Trim().ToUpper() == txtreccodbalancenif.Text.Trim().ToUpper())
                        {

                            txtreccodbalancenif.Text = (T.codigo).ToString().Trim();
                            txtrecdescdbalancenif.Text = T.descripcion;


                            txtreccodbalancenif.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtreccodbalancenif.EnterMoveNextControl = false;
                    txtreccodbalancenif.ResetText();
                    txtreccodbalancenif.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtrescodbalancenif_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrescodbalancenif.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrescodbalancenif.Text.Substring(txtrescodbalancenif.Text.Length - 1, 1) == "*")
                    {
                        using (frm_buscar_formato_estado_finan_smv f = new frm_buscar_formato_estado_finan_smv())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Entidad = new Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtrescodbalancenif.Text = Entidad.codigo;
                                txtresdescdbalancenif.Text = Entidad.descripcion;


                                txtrescodbalancenif.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrescodbalancenif.Text) & string.IsNullOrEmpty(txtresdescdbalancenif.Text))
                    {
                        BuscarBalanceResNif();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBalanceResNif()
        {
            try
            {
                //txtcodblancetributario.Text = Accion.Formato(txttipomovimientocod.Text, 2);
                Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados log = new Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados();

                List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados> Generales = new List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados>();
                Generales = log.Listar(new Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados
                {
                    codigo = txtrescodbalancenif.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados T in Generales)
                    {
                        if ((T.codigo).ToString().Trim().ToUpper() == txtrescodbalancenif.Text.Trim().ToUpper())
                        {

                            txtrescodbalancenif.Text = (T.codigo).ToString().Trim();
                            txtresdescdbalancenif.Text = T.descripcion;


                            txtrescodbalancenif.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtrescodbalancenif.EnterMoveNextControl = false;
                    txtrescodbalancenif.ResetText();
                    txtrescodbalancenif.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtreccodbalancenif_TextChanged(object sender, EventArgs e)
        {
            if (txtreccodbalancenif.Focus() == false)
            {

                txtrecdescdbalancenif.ResetText();
            }
        }

        private void txtrescodbalancenif_TextChanged(object sender, EventArgs e)
        {
            if (txtrescodbalancenif.Focus() == false)
            {

                txtresdescdbalancenif.ResetText();
            }
        }

        public void ListarModificar()
        {
            Entidad_Plan_Empresarial Ent = new Entidad_Plan_Empresarial();
            Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Cuenta = Id_Cuenta;
         
            try
            {
                Lista_Modificar = log.Busqueda(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Plan_Empresarial Enti = new Entidad_Plan_Empresarial();
                    Enti = Lista_Modificar[0];


                    Id_empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Cuenta = Enti.Id_Cuenta.Trim();

                    txtidelemento.Text  = Enti.Id_Elemento;
                    txtelementodesc.Text = Enti.Ele_Descripcion;
                    txtidestructura.Text =Enti.Id_Estructura  ;
                    txtestructuradesc.Text = Enti.Est_Descripcion;

                    txtcuenta.Text  =Enti.Id_Cuenta.Trim();
                    txtcuentadesc.Text= Enti.Cta_Descripcion.Trim();
                    chkafecdoc.Checked  =Enti.Cta_Afecto_Doc;
                    chkarea.Checked  =Enti.Cta_Area;
                    rbccosto.Checked= Enti.Cta_Centro_Costo ;
                    rbcgasto.Checked  =Enti.Cta_Centro_Gasto;
                    chkfuncion.Checked = Enti.Cta_Funcion;
                    chknatrualeza.Checked = Enti.Cta_Naturaleza;
                    chkinventario.Checked = Enti.Cta_Inventario;

                    txttipomovimientocod.Text = Enti.Cod_Proceso.Trim();
                    txttipomovimientocod.Tag = Enti.Tag_Proceso;
                    txttipomovimientodesc.Text = Enti.Proceso_Desc;

                    TxtCodOblig.Text = Enti.Cod_Obligacion.Trim();
                    TxtCodOblig.Tag = Enti.Tag_Obligacion.Trim();
                    TxtDescOblig.Text = Enti.Obligacion_Desc.Trim();

                    txttiporegimencod.Text = Enti.Cta_Afecto_Regimen.Trim();
                    txttiporegimencod.Tag = Enti.Cta_Afecto_Regimen_tag.Trim();
                    txttiporegimendesc.Text = Enti.Cta_Afecto_Regimen_Desc.Trim();

                    txtcodblancetributario.Text = Enti.Cta_Cod_Bal_Tributario.Trim();
                    txtdescblancetributario.Text = Enti.Cta_Cod_Bal_Tributario_desc.Trim();
                    txtreccodblancetributario.Text = Enti.Cta_Cod_Bal_Rec_Tributario.Trim();
                    txtrecdescblancetributario.Text = Enti.Cta_Cod_Bal_Rec_Tributario_desc.Trim();

                    txtcodbalancenif.Text = Enti.Cta_Cod_Bal_Nif_Tributario.Trim();
                    txtdescdbalancenif.Text = Enti.Cta_Cod_Bal_Nif_Tributario_desc.Trim();
                    txtreccodbalancenif.Text = Enti.Cta_Cod_Bal_Rec_Nif_Tributario.Trim();
                    txtrecdescdbalancenif.Text = Enti.Cta_Cod_Bal_Rec_Nif_Tributario_desc.Trim();
                    txtrescodbalancenif.Text = Enti.Cta_Cod_Bal_Res_Nif_Tributario.Trim();
                    txtresdescdbalancenif.Text = Enti.Cta_Cod_Bal_Res_Nif_Tributario_desc.Trim();



                    Logica_Plan_Empresarial log_det = new Logica_Plan_Empresarial();
                    dgvdatos.DataSource = null;
                    ListCtaDestino = log_det.Listar_Destino(Enti);
                    if (ListCtaDestino.Count > 0)
                    {
                        dgvdatos.DataSource = ListCtaDestino;
                    }

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
