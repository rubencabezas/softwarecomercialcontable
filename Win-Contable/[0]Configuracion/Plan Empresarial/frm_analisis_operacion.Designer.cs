﻿namespace Contable._0_Configuracion.Plan_Empresarial
{
    partial class frm_analisis_operacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GbxGeneral = new DevExpress.XtraEditors.GroupControl();
            this.chkasientodefecto = new System.Windows.Forms.CheckBox();
            this.chkinventario = new DevExpress.XtraEditors.CheckEdit();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkventa = new DevExpress.XtraEditors.CheckEdit();
            this.chkcompra = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoopecod = new DevExpress.XtraEditors.TextEdit();
            this.txttipoopedesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtcuenta = new DevExpress.XtraEditors.TextEdit();
            this.txtcuentadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipo = new DevExpress.XtraEditors.TextEdit();
            this.txttipodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dgvdetalles = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BtnDtNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDtEditar = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDtEliminar = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDtGrabar = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnnuevocab = new DevExpress.XtraBars.BarButtonItem();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btncancelar = new DevExpress.XtraBars.BarButtonItem();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GbxGeneral)).BeginInit();
            this.GbxGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkinventario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkventa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcompra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopecod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopedesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdetalles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatos.Location = new System.Drawing.Point(699, 32);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(538, 402);
            this.dgvdatos.TabIndex = 2;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.Caption = "Descripcion";
            this.gridColumn6.FieldName = "Ana_Descripcion";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 270;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.Caption = "Compra";
            this.gridColumn7.FieldName = "Ana_Compra";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.Caption = "Venta";
            this.gridColumn8.FieldName = "Ana_Venta";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn9.Caption = "Inventario";
            this.gridColumn9.FieldName = "Ana_Inventario";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            // 
            // GbxGeneral
            // 
            this.GbxGeneral.Controls.Add(this.chkasientodefecto);
            this.GbxGeneral.Controls.Add(this.chkinventario);
            this.GbxGeneral.Controls.Add(this.txtdescripcion);
            this.GbxGeneral.Controls.Add(this.labelControl1);
            this.GbxGeneral.Controls.Add(this.chkventa);
            this.GbxGeneral.Controls.Add(this.chkcompra);
            this.GbxGeneral.Location = new System.Drawing.Point(0, 32);
            this.GbxGeneral.Name = "GbxGeneral";
            this.GbxGeneral.Size = new System.Drawing.Size(693, 94);
            this.GbxGeneral.TabIndex = 0;
            this.GbxGeneral.Text = "General";
            // 
            // chkasientodefecto
            // 
            this.chkasientodefecto.AutoSize = true;
            this.chkasientodefecto.Location = new System.Drawing.Point(18, 73);
            this.chkasientodefecto.Name = "chkasientodefecto";
            this.chkasientodefecto.Size = new System.Drawing.Size(121, 17);
            this.chkasientodefecto.TabIndex = 5;
            this.chkasientodefecto.Text = "Asiento por defecto";
            this.chkasientodefecto.UseVisualStyleBackColor = true;
            // 
            // chkinventario
            // 
            this.chkinventario.EnterMoveNextControl = true;
            this.chkinventario.Location = new System.Drawing.Point(157, 23);
            this.chkinventario.Name = "chkinventario";
            this.chkinventario.Properties.Caption = "Inventario";
            this.chkinventario.Size = new System.Drawing.Size(75, 19);
            this.chkinventario.TabIndex = 2;
            this.chkinventario.CheckedChanged += new System.EventHandler(this.chkinventario_CheckedChanged);
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.EnterMoveNextControl = true;
            this.txtdescripcion.Location = new System.Drawing.Point(76, 48);
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Size = new System.Drawing.Size(611, 20);
            this.txtdescripcion.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 48);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Descripcion:";
            // 
            // chkventa
            // 
            this.chkventa.EnterMoveNextControl = true;
            this.chkventa.Location = new System.Drawing.Point(93, 23);
            this.chkventa.Name = "chkventa";
            this.chkventa.Properties.Caption = "Venta";
            this.chkventa.Size = new System.Drawing.Size(75, 19);
            this.chkventa.TabIndex = 1;
            this.chkventa.CheckedChanged += new System.EventHandler(this.chkventa_CheckedChanged);
            // 
            // chkcompra
            // 
            this.chkcompra.EnterMoveNextControl = true;
            this.chkcompra.Location = new System.Drawing.Point(12, 23);
            this.chkcompra.Name = "chkcompra";
            this.chkcompra.Properties.Caption = "Compra";
            this.chkcompra.Size = new System.Drawing.Size(75, 19);
            this.chkcompra.TabIndex = 0;
            this.chkcompra.CheckedChanged += new System.EventHandler(this.chkcompra_CheckedChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 23);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Tipo Operacion:";
            // 
            // txttipoopecod
            // 
            this.txttipoopecod.EnterMoveNextControl = true;
            this.txttipoopecod.Location = new System.Drawing.Point(86, 20);
            this.txttipoopecod.Name = "txttipoopecod";
            this.txttipoopecod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipoopecod.Size = new System.Drawing.Size(47, 20);
            this.txttipoopecod.TabIndex = 1;
            this.txttipoopecod.TextChanged += new System.EventHandler(this.txttipoopecod_TextChanged);
            this.txttipoopecod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoopecod_KeyDown);
            // 
            // txttipoopedesc
            // 
            this.txttipoopedesc.Enabled = false;
            this.txttipoopedesc.EnterMoveNextControl = true;
            this.txttipoopedesc.Location = new System.Drawing.Point(135, 20);
            this.txttipoopedesc.Name = "txttipoopedesc";
            this.txttipoopedesc.Size = new System.Drawing.Size(552, 20);
            this.txttipoopedesc.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(41, 44);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Cuenta:";
            // 
            // txtcuenta
            // 
            this.txtcuenta.EnterMoveNextControl = true;
            this.txtcuenta.Location = new System.Drawing.Point(86, 41);
            this.txtcuenta.Name = "txtcuenta";
            this.txtcuenta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcuenta.Size = new System.Drawing.Size(90, 20);
            this.txtcuenta.TabIndex = 4;
            this.txtcuenta.EditValueChanged += new System.EventHandler(this.txtcuenta_EditValueChanged);
            this.txtcuenta.TextChanged += new System.EventHandler(this.txtcuenta_TextChanged);
            this.txtcuenta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcuenta_KeyDown);
            // 
            // txtcuentadesc
            // 
            this.txtcuentadesc.Enabled = false;
            this.txtcuentadesc.EnterMoveNextControl = true;
            this.txtcuentadesc.Location = new System.Drawing.Point(178, 41);
            this.txtcuentadesc.Name = "txtcuentadesc";
            this.txtcuentadesc.Size = new System.Drawing.Size(509, 20);
            this.txtcuentadesc.TabIndex = 5;
            // 
            // txttipo
            // 
            this.txttipo.EnterMoveNextControl = true;
            this.txttipo.Location = new System.Drawing.Point(86, 63);
            this.txttipo.Name = "txttipo";
            this.txttipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipo.Size = new System.Drawing.Size(47, 20);
            this.txttipo.TabIndex = 7;
            this.txttipo.TextChanged += new System.EventHandler(this.txttipo_TextChanged);
            this.txttipo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipo_KeyDown);
            // 
            // txttipodesc
            // 
            this.txttipodesc.Enabled = false;
            this.txttipodesc.EnterMoveNextControl = true;
            this.txttipodesc.Location = new System.Drawing.Point(135, 63);
            this.txttipodesc.Name = "txttipodesc";
            this.txttipodesc.Size = new System.Drawing.Size(552, 20);
            this.txttipodesc.TabIndex = 8;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(56, 66);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(24, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Tipo:";
            // 
            // dgvdetalles
            // 
            this.dgvdetalles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvdetalles.Location = new System.Drawing.Point(6, 117);
            this.dgvdetalles.MainView = this.gridView2;
            this.dgvdetalles.Name = "dgvdetalles";
            this.dgvdetalles.Size = new System.Drawing.Size(681, 179);
            this.dgvdetalles.TabIndex = 20;
            this.dgvdetalles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView2.GridControl = this.dgvdetalles;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView2_RowClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Item";
            this.gridColumn1.FieldName = "Id_Item";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 36;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.Caption = "Operacion";
            this.gridColumn2.FieldName = "Ana_Operacion_Desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 226;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.Caption = "Cuenta";
            this.gridColumn3.FieldName = "Ana_Cuenta";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 92;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.Caption = "Cuenta Descripcion";
            this.gridColumn4.FieldName = "Ana_Cuenta_Desc";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 241;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.Caption = "Tipo";
            this.gridColumn5.FieldName = "Ana_Tipo_Desc";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // BtnDtNuevo
            // 
            this.BtnDtNuevo.Location = new System.Drawing.Point(6, 88);
            this.BtnDtNuevo.Name = "BtnDtNuevo";
            this.BtnDtNuevo.Size = new System.Drawing.Size(75, 23);
            this.BtnDtNuevo.TabIndex = 10;
            this.BtnDtNuevo.Text = "Nuevo";
            this.BtnDtNuevo.Click += new System.EventHandler(this.BtnDtNuevo_Click);
            // 
            // BtnDtEditar
            // 
            this.BtnDtEditar.Location = new System.Drawing.Point(87, 88);
            this.BtnDtEditar.Name = "BtnDtEditar";
            this.BtnDtEditar.Size = new System.Drawing.Size(75, 23);
            this.BtnDtEditar.TabIndex = 6;
            this.BtnDtEditar.Text = "Editar";
            this.BtnDtEditar.Click += new System.EventHandler(this.BtnDtEditar_Click);
            // 
            // BtnDtEliminar
            // 
            this.BtnDtEliminar.Location = new System.Drawing.Point(168, 88);
            this.BtnDtEliminar.Name = "BtnDtEliminar";
            this.BtnDtEliminar.Size = new System.Drawing.Size(75, 23);
            this.BtnDtEliminar.TabIndex = 6;
            this.BtnDtEliminar.Text = "Quitar";
            this.BtnDtEliminar.Click += new System.EventHandler(this.BtnDtEliminar_Click);
            // 
            // BtnDtGrabar
            // 
            this.BtnDtGrabar.Location = new System.Drawing.Point(249, 88);
            this.BtnDtGrabar.Name = "BtnDtGrabar";
            this.BtnDtGrabar.Size = new System.Drawing.Size(75, 23);
            this.BtnDtGrabar.TabIndex = 9;
            this.BtnDtGrabar.Text = "Agregar";
            this.BtnDtGrabar.Click += new System.EventHandler(this.BtnDtGrabar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.BtnDtGrabar);
            this.groupBox1.Controls.Add(this.dgvdetalles);
            this.groupBox1.Controls.Add(this.BtnDtEliminar);
            this.groupBox1.Controls.Add(this.txttipoopedesc);
            this.groupBox1.Controls.Add(this.BtnDtEditar);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.BtnDtNuevo);
            this.groupBox1.Controls.Add(this.txttipoopecod);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.txtcuenta);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.txtcuentadesc);
            this.groupBox1.Controls.Add(this.txttipodesc);
            this.groupBox1.Controls.Add(this.txttipo);
            this.groupBox1.Location = new System.Drawing.Point(0, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(693, 302);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cuentas asociadas";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnnuevocab,
            this.btnmodificar,
            this.btneliminar,
            this.btncancelar,
            this.btnguardar});
            this.barManager1.MaxItemId = 5;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnnuevocab),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnmodificar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btncancelar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btneliminar)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnnuevocab
            // 
            this.btnnuevocab.Caption = "Nuevo [F1]";
            this.btnnuevocab.Id = 0;
            this.btnnuevocab.ImageOptions.Image = global::Contable.Properties.Resources.Add_File_24px;
            this.btnnuevocab.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.btnnuevocab.Name = "btnnuevocab";
            this.btnnuevocab.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnnuevocab.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnnuevocab_ItemClick);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnmodificar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnModificar_ItemClick);
            // 
            // btncancelar
            // 
            this.btncancelar.Caption = "Cancelar";
            this.btncancelar.Id = 3;
            this.btncancelar.ImageOptions.Image = global::Contable.Properties.Resources.Delete_File_24px;
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btncancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelar_ItemClick);
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar";
            this.btnguardar.Id = 4;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGuardar_ItemClick);
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btneliminar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEliminar_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1237, 36);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 412);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1237, 22);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 36);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 376);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1237, 36);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 376);
            // 
            // frm_analisis_operacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1237, 434);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GbxGeneral);
            this.Controls.Add(this.dgvdatos);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_analisis_operacion";
            this.Text = "Analisis";
            this.Load += new System.EventHandler(this.frm_analisis_operacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GbxGeneral)).EndInit();
            this.GbxGeneral.ResumeLayout(false);
            this.GbxGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkinventario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkventa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcompra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopecod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopedesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuenta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuentadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdetalles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.SimpleButton BtnDtGrabar;
        private DevExpress.XtraGrid.GridControl dgvdetalles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton BtnDtEliminar;
        private DevExpress.XtraEditors.TextEdit txttipoopedesc;
        private DevExpress.XtraEditors.SimpleButton BtnDtEditar;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton BtnDtNuevo;
        private DevExpress.XtraEditors.TextEdit txttipoopecod;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtcuenta;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtcuentadesc;
        private DevExpress.XtraEditors.TextEdit txttipodesc;
        private DevExpress.XtraEditors.TextEdit txttipo;
        private DevExpress.XtraEditors.GroupControl GbxGeneral;
        private DevExpress.XtraEditors.CheckEdit chkinventario;
        private DevExpress.XtraEditors.TextEdit txtdescripcion;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit chkventa;
        private DevExpress.XtraEditors.CheckEdit chkcompra;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private System.Windows.Forms.CheckBox chkasientodefecto;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnnuevocab;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btncancelar;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}
