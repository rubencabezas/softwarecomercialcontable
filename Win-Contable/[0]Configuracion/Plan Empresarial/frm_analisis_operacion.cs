﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Plan_Empresarial
{
    public partial class frm_analisis_operacion : Contable.frm_fuente
    {
        public frm_analisis_operacion()
        {
            InitializeComponent();
        }

        public void LimpiarCab()
        {
            chkcompra.Checked = false;
            chkventa.Checked = false;
            chkinventario.Checked = false;
            txtdescripcion.ResetText();
            chkasientodefecto.Checked = false;
        }
        public void LimpiarDet()
        {
            txttipoopecod.ResetText();
            txttipoopedesc.ResetText();
            txtcuenta.ResetText();
            txtcuentadesc.ResetText();
            txttipo.ResetText();
            txttipodesc.ResetText();
            
        }
        void BloquearDetalle()
        {
            txttipoopecod.Enabled = false;
            txtcuenta.Enabled = false;
            txttipo.Enabled = false;

        }
        void DesbloquearDetalle()
        {
            txttipoopecod.Enabled = true;
            txtcuenta.Enabled = true;
            txttipo.Enabled = true;

        }

        void NingunoCab()
        {
            LimpiarCab();
            LimpiarDet();

            GbxGeneral.Enabled = false;

            btnnuevocab.Enabled = true;
            btnmodificar.Enabled = false;
            btncancelar.Enabled = false;
            btnguardar.Enabled = false;
            btneliminar.Enabled = false;

            groupBox1.Enabled = false;

            EstadoDetalle = Estados.Ninguno;
        }
        void NuevoCab()
        {
            LimpiarCab();
            LimpiarDet();

            GbxGeneral.Enabled = true;

            btnnuevocab.Enabled = false;
            btnmodificar.Enabled = false;
            btncancelar.Enabled = true;
            btnguardar.Enabled = true;
            btneliminar.Enabled = false;

            dgvdetalles.DataSource = null;


        }
        void ModificarCab()
        {
            GbxGeneral.Enabled = true;

            btnnuevocab.Enabled = false;
            btnmodificar.Enabled = false;
            btncancelar.Enabled = true;
            btnguardar.Enabled = true;
            btneliminar.Enabled = false;

            groupBox1.Enabled = true;

            NingunoDet();
            chkcompra.Focus();
        }
        void ConsultaCab()
        {
            GbxGeneral.Enabled = false;
            btnnuevocab.Enabled = true;
            btnmodificar.Enabled = true;
            btncancelar.Enabled = false;
            btnguardar.Enabled = false;

            btneliminar.Enabled = true;
        }

        void NingunoDet()
        {
            BtnDtNuevo.Enabled = true;
            BtnDtNuevo.Focus();
            BtnDtEditar.Enabled = false;
            BtnDtGrabar.Enabled = false;
            BtnDtEliminar.Enabled = false;
            BloquearDetalle();
        }

        void NuevoDet()
        {
            LimpiarDet();
            DesbloquearDetalle();
            Id_Item = Detalles.Count + 1;
            BtnDtNuevo.Enabled = true;
            BtnDtEditar.Enabled = false;
            BtnDtGrabar.Enabled = true;
            BtnDtEliminar.Enabled = false;
            DesbloquearDetalle();
            txttipoopecod.Focus();
        }

        void ConsultaDet()
        {
            BtnDtNuevo.Enabled = true;
            BtnDtEditar.Enabled = false;
            BtnDtGrabar.Enabled = false;
            BtnDtEliminar.Enabled = false;
            BloquearDetalle();
        }

        void GuardadoDet()
        {
            LimpiarDet();
            BloquearDetalle();
            BtnDtNuevo.Focus();

            BtnDtNuevo.Enabled = true;
            BtnDtEditar.Enabled = true;
            BtnDtEliminar.Enabled = true;
            BtnDtGrabar.Enabled = false;
        }

        void ModificarDet()
        {

            DesbloquearDetalle();


            BtnDtNuevo.Enabled = true;
            BtnDtEditar.Enabled = false;
            BtnDtEliminar.Enabled = true;
            BtnDtGrabar.Enabled = true;
        }
        void SoloLecturaDet()
        {
            BloquearDetalle();

            BtnDtNuevo.Enabled = false;
            BtnDtEditar.Enabled = true;
            BtnDtEliminar.Enabled = false;
            BtnDtGrabar.Enabled = false;
        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {

                LimpiarCab();
                LimpiarDet();
     
                GbxGeneral.Enabled = true;

                btnnuevocab.Enabled = false;
                btnmodificar.Enabled = false;
                btncancelar.Enabled = true;
                btnguardar.Enabled = true;
                btneliminar.Enabled = false;
            
                    dgvdetalles.DataSource = null;
                



            }
            else if (Estado == Estados.Ninguno)
            {
                LimpiarCab();
                LimpiarDet();

                GbxGeneral.Enabled = false;

                btnnuevocab.Enabled = true;
                btnmodificar.Enabled = false;
                btncancelar.Enabled = false;
                btnguardar.Enabled = false;
                btneliminar.Enabled = false;
        
                EstadoDetalle = Estados.Ninguno;
            }
            else if (Estado == Estados.Modificar)
            {
                GbxGeneral.Enabled = true;

                btnnuevocab.Enabled = false;
                btnmodificar.Enabled = false;
                btncancelar.Enabled = true;
                btnguardar.Enabled = true;
                btneliminar.Enabled = false;


                EstadoDetalle = Estados.Ninguno;
                chkcompra.Focus();

            }
            else if (Estado == Estados.Guardado)
            {
                GbxGeneral.Enabled = false;

               btnnuevocab.Enabled = true;
                btnmodificar.Enabled = true;
                btncancelar.Enabled = false;
                btnguardar.Enabled = false;
                btneliminar.Enabled = true;
            

            }
            else if (Estado == Estados.SoloLectura)
            {
                GbxGeneral.Enabled = false;

                btnnuevocab.Enabled = true;
                btnmodificar.Enabled = true;
                btncancelar.Enabled = false;
                btnguardar.Enabled = false;

                btneliminar.Enabled = false;
         
            }
            else if (Estado == Estados.Consulta)
            {
                GbxGeneral.Enabled = false;
                btnnuevocab.Enabled = true;
                btnmodificar.Enabled = true;
                btncancelar.Enabled = false;
                btnguardar.Enabled = false;

                btneliminar.Enabled = true;
            }
        }

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                //HabilitarDetalles();
                //LimpiarDet();
                //txtcuenta.Tag = Detalles.Count + 1;

                //btnnuevodet.Enabled = false;
                //btneditardet.Enabled = false;
                //btnquitardet.Enabled = false;
                //btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                //LimpiarDet();
                //BloquearDetalles();

                //btnnuevodet.Enabled = true;
                //btneditardet.Enabled = false;
                //btnquitardet.Enabled = false;
                //btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                //HabilitarDetalles();


                //btnnuevodet.Enabled = true;
                //btneditardet.Enabled = false;
                //btnquitardet.Enabled = true;
                //btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                //LimpiarDet();
                //BloquearDetalles();
                //btnnuevodet.Focus();

                //btnnuevodet.Enabled = true;
                //btneditardet.Enabled = true;
                //btnquitardet.Enabled = true;
                //btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                //HabilitarDetalles();

                //btnnuevodet.Enabled = false;
                //btneditardet.Enabled = true;
                //btnquitardet.Enabled = false;
                //btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                //BloquearDetalles();

                //btnnuevodet.Enabled = false;
                //btneditardet.Enabled = true;
                //btnquitardet.Enabled = false;
                //btnanadirdet.Enabled = false;
            }
        }

        string Id_General;
        void VerificarCompra()
        {
        if (chkcompra.Checked)
            {
                chkventa.Checked = false;
                chkinventario.Checked = false;
                Id_General = "0007";//compras
                txtdescripcion.Focus();
                //txttipoopecod.Enabled = true;
            }
        }

        void VerificarVenta()
        {
           if (chkventa.Checked)
                    {
                        chkcompra.Checked = false;
                        chkinventario.Checked = false;
                        Id_General = "0006";//venta
                txtdescripcion.Focus();
                //txttipoopecod.Enabled = true;
            }
        }

        void VerificarInventario()
        {
            if (chkinventario.Checked)
            {
                chkcompra.Checked = false;
                chkventa.Checked = false;
                txttipoopecod.Enabled = false;
                txtdescripcion.Focus();
            }
        }
        private void chkcompra_CheckedChanged(object sender, EventArgs e)
        {
            VerificarCompra();
        }

        private void chkventa_CheckedChanged(object sender, EventArgs e)
        {
            VerificarVenta();
        }

        private void chkinventario_CheckedChanged(object sender, EventArgs e)
        {
            VerificarInventario();
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcuenta.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcuenta.Text.Substring(txtcuenta.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcuenta.Text = Entidad.Id_Cuenta;
                                txtcuentadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text) & string.IsNullOrEmpty(txtcuentadesc.Text))
                    {
                        Cuenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void Cuenta()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcuenta.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcuenta.Text.Trim().ToUpper())
                        {
                            txtcuenta.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcuentadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoopecod_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (String.IsNullOrEmpty(txttipoopecod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoopecod.Text.Substring(txttipoopecod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = Id_General;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoopecod.Tag = Entidad.Id_General_Det;
                                txttipoopecod.Text = Entidad.Gen_Codigo_Interno;
                                txttipoopedesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoopecod.Text) & string.IsNullOrEmpty(txttipoopedesc.Text))
                    {
                        BuscarVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarVenta()
        {
            try
            {
                txttipoopecod.Text = Accion.Formato(txttipoopecod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = Id_General,
                    Gen_Codigo_Interno = txttipoopecod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipoopecod.Text.Trim().ToUpper())
                        {
                            txttipoopecod.Tag = T.Id_General_Det;
                            txttipoopecod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipoopedesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipo.Text.Substring(txttipo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0002";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipo.Tag = Entidad.Id_General_Det;
                                txttipo.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipo.Text) & string.IsNullOrEmpty(txttipodesc.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarTipo()
        {
            try
            {
                txttipo.Text = Accion.Formato(txttipo.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0002",
                    Gen_Codigo_Interno = txttipo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipo.Text.Trim().ToUpper())
                        {
                            txttipo.Tag = T.Id_General_Det;
                            txttipo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun registro");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_analisis_operacion_Load(object sender, EventArgs e)
        {
            NingunoCab();
            ConsultaDet();
            Listar();
        }

        private void btnnuevocab_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            //{
                NuevoCab();
                Id_Analisis = null;
                Detalles.Clear();
                EstadoDetalle =Estados.Ninguno;
                Estado_Ven_Boton = "1";
                txtdescripcion.Focus();
                groupBox1.Enabled = true;
            
            //}


        }

        private void btnCancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NingunoCab();
            ConsultaCab();
            BloquearDetalle();
            NingunoDet();
        }

        string Estado_Ven_Boton;
        private void BtnDtNuevo_Click(object sender, EventArgs e)
        {
            if (VerificarTipo())
            {
            NuevoDet();
            //VerificarCompra();
            //VerificarVenta();
            //VerificarInventario();
                
            if (chkcompra.Checked == true) {
                txttipoopecod.Enabled = true;
            }
            if (chkventa.Checked == true)
            {
                txttipoopecod.Enabled = true;
            }

            if (chkinventario.Checked == true)
            {
                txttipoopecod.Enabled = false;
            }

            Estado_Ven_Boton = "1";
            }

        }

        bool VerificarTipo()
        {
            if ((chkcompra.Checked == false) & (chkventa.Checked==false) & (chkinventario.Checked==false))
            {
                Accion.Advertencia("Debe Seleccionar un tipo");
                txtcuenta.Focus();
                return false;
            }
            return true;
        }
        public bool VerificarDetalle()
        {
            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una cuenta");
                txtcuenta.Focus();
                return false;
            }
    
            if (string.IsNullOrEmpty(txttipodesc.Text))
            {

                Accion.Advertencia("Debe ingresar un tipo Debe / Haber");
                txttipo.Focus();
                return false;
            }




            return true;
        }

        List<Entidad_Analisis_Contable> Detalles = new List<Entidad_Analisis_Contable>();
        int Id_Item;
        private void BtnDtGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarDetalle())
                    {
                    Entidad_Analisis_Contable ItemDetalle = new Entidad_Analisis_Contable();

                    ItemDetalle.Id_Item = Id_Item;

                    
                  if (txttipoopecod.Tag.ToString() == null)
                    {
                        ItemDetalle.Ana_Operacion_Cod = "";
                    }
                    else
                    {
                        ItemDetalle.Ana_Operacion_Cod = txttipoopecod.Tag.ToString();
                        ItemDetalle.Ana_Operacion_Det = txttipoopecod.Text;
                        ItemDetalle.Ana_Operacion_Desc = txttipoopedesc.Text;
                    }

           
                    ItemDetalle.Ana_Cuenta = txtcuenta.Text.ToString();
                    ItemDetalle.Ana_Cuenta_Desc = txtcuentadesc.Text.ToString();
              

                     if (txttipo.Tag.ToString() == null)
                    {
                        ItemDetalle.Ana_Tipo = "";
                    }
                    else
                    {
                        ItemDetalle.Ana_Tipo = txttipo.Tag.ToString();
                        ItemDetalle.Ana_Tipo_Det = txttipo.Text.ToString();
                        ItemDetalle.Ana_Tipo_Desc = txttipodesc.Text.ToString();
                    }


                        if (Estado_Ven_Boton == "1")
                        {
                            Detalles.Add(ItemDetalle);


                            UpdateGrilla();
                        //EstadoDetalle = Estados.Guardado;
                        GuardadoDet();
                        }
                        else if (Estado_Ven_Boton == "2")
                        {

                            Detalles[Convert.ToInt32(Id_Item) - 1] = ItemDetalle;

                            UpdateGrilla();
                        //EstadoDetalle = Estados.Guardado;
                        GuardadoDet();

                        }


                }

         



            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        public void UpdateGrilla()
        {
            dgvdetalles.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdetalles.DataSource = Detalles;
            }
        }

       
        private void BtnDtEditar_Click(object sender, EventArgs e)
        {
            Estado_Ven_Boton = "2";
            ModificarDet();
            txttipoopecod.Focus();
        }

        private void gridView2_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
            
                   Entidad_Analisis_Contable Entidad = new Entidad_Analisis_Contable();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView2.GetFocusedDataSourceRowIndex()];

                    Id_Item= Entidad.Id_Item;

                    txttipoopecod.Tag = Entidad.Ana_Operacion_Cod;
                    txttipoopecod.Text = Entidad.Ana_Operacion_Det;
                    txttipoopedesc.Text = Entidad.Ana_Operacion_Desc;

                    txtcuenta.Text = Entidad.Ana_Cuenta;
                    txtcuentadesc.Text = Entidad.Ana_Cuenta_Desc;

               

                    txttipo.Tag = Entidad.Ana_Tipo;
                    txttipo.Text = Entidad.Ana_Tipo_Det;
                    txttipodesc.Text = Entidad.Ana_Tipo_Desc;





                    if (Estado_Ven_Boton == "1")
                    {
                        //EstadoDetalle = Estados.Consulta;
                        ConsultaDet();
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        //EstadoDetalle = Estados.SoloLectura;
                        SoloLecturaDet();


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void RefreshNumeral()
        {
            int NumOrden = 1;
            foreach (Entidad_Analisis_Contable Det in Detalles)
            {
                Det.Id_Item = NumOrden;
                NumOrden += 1;
            }
        }
        private void BtnDtEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    //Detalles.RemoveAt[gridView1.GetFocusedDataSourceRowIndex];

                    Detalles.RemoveAt(gridView2.GetFocusedDataSourceRowIndex());
                
                    dgvdetalles.DataSource = null;
                    if (Detalles.Count > 0)
                    {
                        dgvdetalles.DataSource = Detalles;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnModificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado_Ven_Boton = "2";
            ModificarCab();
        }

        public bool VerificarCabecera()
        {
            if (chkcompra.Checked==false && chkventa.Checked ==false && chkinventario.Checked || false)
            {
                Accion.Advertencia("Debe seleccionar una opcion (Compra ,Venta , Inventarioo)");
                txtdescripcion.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtdescripcion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtdescripcion.Focus();
                return false;
            }


            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }



            return true;
        }

        private void btnGuardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
               if (VerificarCabecera())
                        {
                    Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
                    Logica_Analisis_Contable Log = new Logica_Analisis_Contable();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Analisis = Id_Analisis;

                    Ent.Ana_Descripcion = txtdescripcion.Text;
                    Ent.Ana_Compra = chkcompra.Checked;
                    Ent.Ana_Venta = chkventa.Checked;
                    Ent.Ana_Inventario = chkinventario.Checked;
                    Ent.Ana_Asiento_Defecto = chkasientodefecto.Checked;

                    Ent.Detalles = Detalles;

                    if (Estado_Ven_Boton == "1")
                    {
                        if (Log.Insertar(Ent))
                        {
                            //Listar();co 
                            //frm_compras f = new frm_compras();
                            //f.dgvdatos.DataSource = Lista;
                            //f.dgvdatos.RefreshDataSource();
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();
                            Detalles.Clear();
                            Id_Analisis = null;
                            dgvdetalles.DataSource = null;
                            BloquearDetalle();
                            Listar();


                        }
                    }
                    else if (Estado_Ven_Boton == "2")
                    {

                        if (Log.Modificar(Ent))
                        {
                          
                            Accion.ExitoModificar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();
                            Detalles.Clear();
                            dgvdetalles.DataSource = null;
                            BloquearDetalle();
                            Id_Analisis = null;
                            Listar();


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
         
        }

        
        public List<Entidad_Analisis_Contable> Lista = new List<Entidad_Analisis_Contable>();
        string Id_Analisis;
        public void Listar()
        {
            Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
            Logica_Analisis_Contable log = new Logica_Analisis_Contable();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Analisis = Id_Analisis;
         
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }
        Entidad_Analisis_Contable Entidad_Cab = new Entidad_Analisis_Contable();
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    LimpiarCab();
                    LimpiarDet();
                 

                    Entidad_Cab = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                    Id_Analisis = Entidad_Cab.Id_Analisis;
                    chkcompra.Checked = Entidad_Cab.Ana_Compra;
                    chkventa.Checked = Entidad_Cab.Ana_Venta;
                    chkinventario.Checked = Entidad_Cab.Ana_Inventario;
                    txtdescripcion.Text = Entidad_Cab.Ana_Descripcion.Trim().ToString();
                    chkasientodefecto.Checked = Entidad_Cab.Ana_Asiento_Defecto;

                    Logica_Analisis_Contable log = new Logica_Analisis_Contable();

                    Detalles = log.Listar_det(Entidad_Cab);
                    dgvdetalles.DataSource = Detalles;

                    ConsultaCab();
                    ConsultaDet();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        private void txtcuenta_EditValueChanged(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnEliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad_Cab.Ana_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable
                    {
                        Id_Empresa = Entidad_Cab.Id_Empresa,
                        Id_Anio = Entidad_Cab.Id_Anio,
                        Id_Analisis = Entidad_Cab.Id_Analisis
                    };

                    Logica_Analisis_Contable log = new Logica_Analisis_Contable();
                    Id_Analisis = null;
                    log.Eliminar(Ent);
                    Detalles.Clear();
                    dgvdetalles.DataSource = null;

                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }


        }

        private void txttipo_TextChanged(object sender, EventArgs e)
        {
            if (txttipo.Focus() == false)
            {
                txttipodesc.ResetText();
            }
        }

        private void txttipoopecod_TextChanged(object sender, EventArgs e)
        {
            if (txttipoopecod.Focus() == false)
            {
                txttipoopedesc.ResetText();
            }
        }

        private void txtcuenta_TextChanged(object sender, EventArgs e)
        {
            if (txtcuenta.Focus() == false)
            {
                txtcuentadesc.ResetText();
            }
        }
    }
}
