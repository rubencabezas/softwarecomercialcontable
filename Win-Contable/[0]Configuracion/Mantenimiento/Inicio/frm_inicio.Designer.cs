﻿namespace Contable._0_Configuracion.Mantenimiento.Inicio
{
    partial class frm_inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtrendcajadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtrendcaja = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibCajaBancodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtlibCajaBanco = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibinventariodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtlibinventario = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibdiariodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtlibdiario = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibcompradesc = new DevExpress.XtraEditors.TextEdit();
            this.txtlibcompra = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibventa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibventadesc = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtretenciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtretencion = new DevExpress.XtraEditors.TextEdit();
            this.txtigvdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtigv = new DevExpress.XtraEditors.TextEdit();
            this.txtdetracciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtdetraccion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtpercepciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpercepcion = new DevExpress.XtraEditors.TextEdit();
            this.txtcajadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcaja = new DevExpress.XtraEditors.TextEdit();
            this.txtitfdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtitf = new DevExpress.XtraEditors.TextEdit();
            this.txtperdidaredondeodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtperdidaredondeo = new DevExpress.XtraEditors.TextEdit();
            this.txtgananciaredondeodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtgananciaredondeo = new DevExpress.XtraEditors.TextEdit();
            this.txtperdidadifcambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtperdidadifcambio = new DevExpress.XtraEditors.TextEdit();
            this.txtganciadifcambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtganciadifcambio = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnnuevo = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrendcajadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrendcaja.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibCajaBancodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibCajaBanco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibinventariodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibinventario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibdiariodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibdiario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibcompradesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibcompra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibventa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibventadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtretenciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtretencion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdetracciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdetraccion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpercepciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpercepcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcajadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcaja.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtitfdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtitf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidaredondeodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidaredondeo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgananciaredondeodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgananciaredondeo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidadifcambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidadifcambio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtganciadifcambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtganciadifcambio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtrendcajadesc);
            this.groupControl1.Controls.Add(this.txtrendcaja);
            this.groupControl1.Controls.Add(this.txtlibventadesc);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtlibCajaBancodesc);
            this.groupControl1.Controls.Add(this.txtlibCajaBanco);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtlibinventariodesc);
            this.groupControl1.Controls.Add(this.txtlibinventario);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtlibdiariodesc);
            this.groupControl1.Controls.Add(this.txtlibdiario);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtlibcompradesc);
            this.groupControl1.Controls.Add(this.txtlibcompra);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtlibventa);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 48);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(671, 172);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Libros Contables";
            // 
            // txtrendcajadesc
            // 
            this.txtrendcajadesc.Enabled = false;
            this.txtrendcajadesc.EnterMoveNextControl = true;
            this.txtrendcajadesc.Location = new System.Drawing.Point(178, 144);
            this.txtrendcajadesc.Name = "txtrendcajadesc";
            this.txtrendcajadesc.Size = new System.Drawing.Size(488, 20);
            this.txtrendcajadesc.TabIndex = 17;
            // 
            // txtrendcaja
            // 
            this.txtrendcaja.EnterMoveNextControl = true;
            this.txtrendcaja.Location = new System.Drawing.Point(130, 144);
            this.txtrendcaja.Name = "txtrendcaja";
            this.txtrendcaja.Size = new System.Drawing.Size(46, 20);
            this.txtrendcaja.TabIndex = 16;
            this.txtrendcaja.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrendcaja_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(5, 147);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(119, 13);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Rendicion de Caja Chica:";
            // 
            // txtlibCajaBancodesc
            // 
            this.txtlibCajaBancodesc.Enabled = false;
            this.txtlibCajaBancodesc.EnterMoveNextControl = true;
            this.txtlibCajaBancodesc.Location = new System.Drawing.Point(159, 121);
            this.txtlibCajaBancodesc.Name = "txtlibCajaBancodesc";
            this.txtlibCajaBancodesc.Size = new System.Drawing.Size(507, 20);
            this.txtlibCajaBancodesc.TabIndex = 14;
            // 
            // txtlibCajaBanco
            // 
            this.txtlibCajaBanco.EnterMoveNextControl = true;
            this.txtlibCajaBanco.Location = new System.Drawing.Point(110, 121);
            this.txtlibCajaBanco.Name = "txtlibCajaBanco";
            this.txtlibCajaBanco.Size = new System.Drawing.Size(46, 20);
            this.txtlibCajaBanco.TabIndex = 13;
            this.txtlibCajaBanco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlibCajaBanco_KeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(15, 124);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(93, 13);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "Libro Caja y Banco:";
            // 
            // txtlibinventariodesc
            // 
            this.txtlibinventariodesc.Enabled = false;
            this.txtlibinventariodesc.EnterMoveNextControl = true;
            this.txtlibinventariodesc.Location = new System.Drawing.Point(159, 100);
            this.txtlibinventariodesc.Name = "txtlibinventariodesc";
            this.txtlibinventariodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtlibinventariodesc.Properties.Appearance.Options.UseFont = true;
            this.txtlibinventariodesc.Size = new System.Drawing.Size(507, 20);
            this.txtlibinventariodesc.TabIndex = 11;
            // 
            // txtlibinventario
            // 
            this.txtlibinventario.EnterMoveNextControl = true;
            this.txtlibinventario.Location = new System.Drawing.Point(110, 100);
            this.txtlibinventario.Name = "txtlibinventario";
            this.txtlibinventario.Size = new System.Drawing.Size(46, 20);
            this.txtlibinventario.TabIndex = 10;
            this.txtlibinventario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlibinventario_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(54, 103);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Inventario:";
            // 
            // txtlibdiariodesc
            // 
            this.txtlibdiariodesc.Enabled = false;
            this.txtlibdiariodesc.EnterMoveNextControl = true;
            this.txtlibdiariodesc.Location = new System.Drawing.Point(159, 77);
            this.txtlibdiariodesc.Name = "txtlibdiariodesc";
            this.txtlibdiariodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlibdiariodesc.Properties.Appearance.Options.UseFont = true;
            this.txtlibdiariodesc.Size = new System.Drawing.Size(507, 20);
            this.txtlibdiariodesc.TabIndex = 8;
            // 
            // txtlibdiario
            // 
            this.txtlibdiario.EnterMoveNextControl = true;
            this.txtlibdiario.Location = new System.Drawing.Point(110, 77);
            this.txtlibdiario.Name = "txtlibdiario";
            this.txtlibdiario.Size = new System.Drawing.Size(46, 20);
            this.txtlibdiario.TabIndex = 7;
            this.txtlibdiario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlibdiario_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 80);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(57, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Libro Diario:";
            // 
            // txtlibcompradesc
            // 
            this.txtlibcompradesc.Enabled = false;
            this.txtlibcompradesc.EnterMoveNextControl = true;
            this.txtlibcompradesc.Location = new System.Drawing.Point(159, 55);
            this.txtlibcompradesc.Name = "txtlibcompradesc";
            this.txtlibcompradesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlibcompradesc.Properties.Appearance.Options.UseFont = true;
            this.txtlibcompradesc.Size = new System.Drawing.Size(507, 20);
            this.txtlibcompradesc.TabIndex = 5;
            // 
            // txtlibcompra
            // 
            this.txtlibcompra.EnterMoveNextControl = true;
            this.txtlibcompra.Location = new System.Drawing.Point(110, 55);
            this.txtlibcompra.Name = "txtlibcompra";
            this.txtlibcompra.Size = new System.Drawing.Size(46, 20);
            this.txtlibcompra.TabIndex = 4;
            this.txtlibcompra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlibcompra_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(99, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Registro de Compra:";
            // 
            // txtlibventa
            // 
            this.txtlibventa.EnterMoveNextControl = true;
            this.txtlibventa.Location = new System.Drawing.Point(110, 33);
            this.txtlibventa.Name = "txtlibventa";
            this.txtlibventa.Size = new System.Drawing.Size(46, 20);
            this.txtlibventa.TabIndex = 1;
            this.txtlibventa.EditValueChanged += new System.EventHandler(this.txtlibventa_EditValueChanged);
            this.txtlibventa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlibventa_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Registro de Venta:";
            // 
            // txtlibventadesc
            // 
            this.txtlibventadesc.Enabled = false;
            this.txtlibventadesc.EnterMoveNextControl = true;
            this.txtlibventadesc.Location = new System.Drawing.Point(159, 33);
            this.txtlibventadesc.Name = "txtlibventadesc";
            this.txtlibventadesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlibventadesc.Properties.Appearance.Options.UseFont = true;
            this.txtlibventadesc.Size = new System.Drawing.Size(507, 20);
            this.txtlibventadesc.TabIndex = 2;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.txtretenciondesc);
            this.groupControl2.Controls.Add(this.txtretencion);
            this.groupControl2.Controls.Add(this.txtigvdesc);
            this.groupControl2.Controls.Add(this.txtigv);
            this.groupControl2.Controls.Add(this.txtdetracciondesc);
            this.groupControl2.Controls.Add(this.txtdetraccion);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.txtpercepciondesc);
            this.groupControl2.Controls.Add(this.txtpercepcion);
            this.groupControl2.Controls.Add(this.txtcajadesc);
            this.groupControl2.Controls.Add(this.txtcaja);
            this.groupControl2.Controls.Add(this.txtitfdesc);
            this.groupControl2.Controls.Add(this.txtitf);
            this.groupControl2.Controls.Add(this.txtperdidaredondeodesc);
            this.groupControl2.Controls.Add(this.txtperdidaredondeo);
            this.groupControl2.Controls.Add(this.txtgananciaredondeodesc);
            this.groupControl2.Controls.Add(this.txtgananciaredondeo);
            this.groupControl2.Controls.Add(this.txtperdidadifcambiodesc);
            this.groupControl2.Controls.Add(this.txtperdidadifcambio);
            this.groupControl2.Controls.Add(this.txtganciadifcambiodesc);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.txtganciadifcambio);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Location = new System.Drawing.Point(12, 226);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(671, 284);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Configuracion de cuentas";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(34, 212);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(98, 13);
            this.labelControl15.TabIndex = 19;
            this.labelControl15.Text = "Cuenta de retencion";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(27, 236);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(98, 13);
            this.labelControl16.TabIndex = 18;
            this.labelControl16.Text = "Cuenta de Impuesto";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(27, 190);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(103, 13);
            this.labelControl14.TabIndex = 18;
            this.labelControl14.Text = "Cuenta de detraccion";
            // 
            // txtretenciondesc
            // 
            this.txtretenciondesc.Enabled = false;
            this.txtretenciondesc.EnterMoveNextControl = true;
            this.txtretenciondesc.Location = new System.Drawing.Point(222, 209);
            this.txtretenciondesc.Name = "txtretenciondesc";
            this.txtretenciondesc.Size = new System.Drawing.Size(444, 20);
            this.txtretenciondesc.TabIndex = 17;
            // 
            // txtretencion
            // 
            this.txtretencion.EnterMoveNextControl = true;
            this.txtretencion.Location = new System.Drawing.Point(141, 209);
            this.txtretencion.Name = "txtretencion";
            this.txtretencion.Size = new System.Drawing.Size(79, 20);
            this.txtretencion.TabIndex = 16;
            this.txtretencion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtretencion_KeyDown);
            // 
            // txtigvdesc
            // 
            this.txtigvdesc.Enabled = false;
            this.txtigvdesc.EnterMoveNextControl = true;
            this.txtigvdesc.Location = new System.Drawing.Point(222, 233);
            this.txtigvdesc.Name = "txtigvdesc";
            this.txtigvdesc.Size = new System.Drawing.Size(444, 20);
            this.txtigvdesc.TabIndex = 17;
            // 
            // txtigv
            // 
            this.txtigv.EnterMoveNextControl = true;
            this.txtigv.Location = new System.Drawing.Point(141, 233);
            this.txtigv.Name = "txtigv";
            this.txtigv.Size = new System.Drawing.Size(79, 20);
            this.txtigv.TabIndex = 16;
            this.txtigv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtigv_KeyDown);
            // 
            // txtdetracciondesc
            // 
            this.txtdetracciondesc.Enabled = false;
            this.txtdetracciondesc.EnterMoveNextControl = true;
            this.txtdetracciondesc.Location = new System.Drawing.Point(222, 187);
            this.txtdetracciondesc.Name = "txtdetracciondesc";
            this.txtdetracciondesc.Size = new System.Drawing.Size(444, 20);
            this.txtdetracciondesc.TabIndex = 17;
            // 
            // txtdetraccion
            // 
            this.txtdetraccion.EnterMoveNextControl = true;
            this.txtdetraccion.Location = new System.Drawing.Point(141, 187);
            this.txtdetraccion.Name = "txtdetraccion";
            this.txtdetraccion.Size = new System.Drawing.Size(79, 20);
            this.txtdetraccion.TabIndex = 16;
            this.txtdetraccion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdetraccion_KeyDown);
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(27, 167);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(105, 13);
            this.labelControl13.TabIndex = 15;
            this.labelControl13.Text = "Cuenta de percepcion";
            // 
            // txtpercepciondesc
            // 
            this.txtpercepciondesc.Enabled = false;
            this.txtpercepciondesc.EnterMoveNextControl = true;
            this.txtpercepciondesc.Location = new System.Drawing.Point(222, 164);
            this.txtpercepciondesc.Name = "txtpercepciondesc";
            this.txtpercepciondesc.Size = new System.Drawing.Size(444, 20);
            this.txtpercepciondesc.TabIndex = 14;
            // 
            // txtpercepcion
            // 
            this.txtpercepcion.EnterMoveNextControl = true;
            this.txtpercepcion.Location = new System.Drawing.Point(141, 164);
            this.txtpercepcion.Name = "txtpercepcion";
            this.txtpercepcion.Size = new System.Drawing.Size(79, 20);
            this.txtpercepcion.TabIndex = 13;
            this.txtpercepcion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpercepcion_KeyDown);
            // 
            // txtcajadesc
            // 
            this.txtcajadesc.Enabled = false;
            this.txtcajadesc.EnterMoveNextControl = true;
            this.txtcajadesc.Location = new System.Drawing.Point(222, 135);
            this.txtcajadesc.Name = "txtcajadesc";
            this.txtcajadesc.Size = new System.Drawing.Size(444, 20);
            this.txtcajadesc.TabIndex = 14;
            // 
            // txtcaja
            // 
            this.txtcaja.EnterMoveNextControl = true;
            this.txtcaja.Location = new System.Drawing.Point(141, 135);
            this.txtcaja.Name = "txtcaja";
            this.txtcaja.Size = new System.Drawing.Size(79, 20);
            this.txtcaja.TabIndex = 13;
            this.txtcaja.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcaja_KeyDown);
            // 
            // txtitfdesc
            // 
            this.txtitfdesc.Enabled = false;
            this.txtitfdesc.EnterMoveNextControl = true;
            this.txtitfdesc.Location = new System.Drawing.Point(222, 113);
            this.txtitfdesc.Name = "txtitfdesc";
            this.txtitfdesc.Size = new System.Drawing.Size(444, 20);
            this.txtitfdesc.TabIndex = 14;
            // 
            // txtitf
            // 
            this.txtitf.EnterMoveNextControl = true;
            this.txtitf.Location = new System.Drawing.Point(141, 113);
            this.txtitf.Name = "txtitf";
            this.txtitf.Size = new System.Drawing.Size(79, 20);
            this.txtitf.TabIndex = 13;
            this.txtitf.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitf_KeyDown);
            // 
            // txtperdidaredondeodesc
            // 
            this.txtperdidaredondeodesc.Enabled = false;
            this.txtperdidaredondeodesc.EnterMoveNextControl = true;
            this.txtperdidaredondeodesc.Location = new System.Drawing.Point(222, 90);
            this.txtperdidaredondeodesc.Name = "txtperdidaredondeodesc";
            this.txtperdidaredondeodesc.Size = new System.Drawing.Size(444, 20);
            this.txtperdidaredondeodesc.TabIndex = 11;
            // 
            // txtperdidaredondeo
            // 
            this.txtperdidaredondeo.EnterMoveNextControl = true;
            this.txtperdidaredondeo.Location = new System.Drawing.Point(141, 90);
            this.txtperdidaredondeo.Name = "txtperdidaredondeo";
            this.txtperdidaredondeo.Size = new System.Drawing.Size(79, 20);
            this.txtperdidaredondeo.TabIndex = 10;
            this.txtperdidaredondeo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtperdidaredondeo_KeyDown);
            // 
            // txtgananciaredondeodesc
            // 
            this.txtgananciaredondeodesc.Enabled = false;
            this.txtgananciaredondeodesc.EnterMoveNextControl = true;
            this.txtgananciaredondeodesc.Location = new System.Drawing.Point(222, 67);
            this.txtgananciaredondeodesc.Name = "txtgananciaredondeodesc";
            this.txtgananciaredondeodesc.Size = new System.Drawing.Size(444, 20);
            this.txtgananciaredondeodesc.TabIndex = 8;
            // 
            // txtgananciaredondeo
            // 
            this.txtgananciaredondeo.EnterMoveNextControl = true;
            this.txtgananciaredondeo.Location = new System.Drawing.Point(141, 67);
            this.txtgananciaredondeo.Name = "txtgananciaredondeo";
            this.txtgananciaredondeo.Size = new System.Drawing.Size(79, 20);
            this.txtgananciaredondeo.TabIndex = 7;
            this.txtgananciaredondeo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgananciaredondeo_KeyDown);
            // 
            // txtperdidadifcambiodesc
            // 
            this.txtperdidadifcambiodesc.Enabled = false;
            this.txtperdidadifcambiodesc.EnterMoveNextControl = true;
            this.txtperdidadifcambiodesc.Location = new System.Drawing.Point(222, 45);
            this.txtperdidadifcambiodesc.Name = "txtperdidadifcambiodesc";
            this.txtperdidadifcambiodesc.Size = new System.Drawing.Size(444, 20);
            this.txtperdidadifcambiodesc.TabIndex = 5;
            // 
            // txtperdidadifcambio
            // 
            this.txtperdidadifcambio.EnterMoveNextControl = true;
            this.txtperdidadifcambio.Location = new System.Drawing.Point(141, 45);
            this.txtperdidadifcambio.Name = "txtperdidadifcambio";
            this.txtperdidadifcambio.Size = new System.Drawing.Size(79, 20);
            this.txtperdidadifcambio.TabIndex = 4;
            this.txtperdidadifcambio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtperdidadifcambio_KeyDown);
            // 
            // txtganciadifcambiodesc
            // 
            this.txtganciadifcambiodesc.Enabled = false;
            this.txtganciadifcambiodesc.EnterMoveNextControl = true;
            this.txtganciadifcambiodesc.Location = new System.Drawing.Point(222, 23);
            this.txtganciadifcambiodesc.Name = "txtganciadifcambiodesc";
            this.txtganciadifcambiodesc.Size = new System.Drawing.Size(444, 20);
            this.txtganciadifcambiodesc.TabIndex = 2;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(59, 138);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(73, 13);
            this.labelControl12.TabIndex = 12;
            this.labelControl12.Text = "Cuenta de caja";
            // 
            // txtganciadifcambio
            // 
            this.txtganciadifcambio.EnterMoveNextControl = true;
            this.txtganciadifcambio.Location = new System.Drawing.Point(141, 23);
            this.txtganciadifcambio.Name = "txtganciadifcambio";
            this.txtganciadifcambio.Size = new System.Drawing.Size(79, 20);
            this.txtganciadifcambio.TabIndex = 1;
            this.txtganciadifcambio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtganciadifcambio_KeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(78, 116);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(54, 13);
            this.labelControl11.TabIndex = 12;
            this.labelControl11.Text = "Cuenta ITF";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(44, 93);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(88, 13);
            this.labelControl10.TabIndex = 9;
            this.labelControl10.Text = "Perdida Redondeo";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(36, 70);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(96, 13);
            this.labelControl9.TabIndex = 6;
            this.labelControl9.Text = "Ganancia Redondeo";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(23, 48);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(109, 13);
            this.labelControl8.TabIndex = 3;
            this.labelControl8.Text = "Perdida Dif. de Cambio";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(15, 26);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(117, 13);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Ganancia Dif. de Cambio";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnnuevo,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 4;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnnuevo)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnnuevo
            // 
            this.btnnuevo.Caption = "Nuevo [F4]";
            this.btnnuevo.Id = 0;
            this.btnnuevo.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnnuevo.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnnuevo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(710, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 515);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(710, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 483);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(710, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 483);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 515);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_inicio";
            this.Text = "Parametros Iniciales";
            this.Load += new System.EventHandler(this.frm_inicio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrendcajadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrendcaja.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibCajaBancodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibCajaBanco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibinventariodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibinventario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibdiariodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibdiario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibcompradesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibcompra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibventa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibventadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtretenciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtretencion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdetracciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdetraccion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpercepciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpercepcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcajadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcaja.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtitfdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtitf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidaredondeodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidaredondeo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgananciaredondeodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgananciaredondeo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidadifcambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtperdidadifcambio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtganciadifcambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtganciadifcambio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtlibdiariodesc;
        private DevExpress.XtraEditors.TextEdit txtlibdiario;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtlibcompradesc;
        private DevExpress.XtraEditors.TextEdit txtlibventadesc;
        private DevExpress.XtraEditors.TextEdit txtlibcompra;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtlibventa;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtlibinventariodesc;
        private DevExpress.XtraEditors.TextEdit txtlibinventario;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtlibCajaBancodesc;
        private DevExpress.XtraEditors.TextEdit txtlibCajaBanco;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtperdidaredondeodesc;
        private DevExpress.XtraEditors.TextEdit txtperdidaredondeo;
        private DevExpress.XtraEditors.TextEdit txtgananciaredondeodesc;
        private DevExpress.XtraEditors.TextEdit txtgananciaredondeo;
        private DevExpress.XtraEditors.TextEdit txtperdidadifcambiodesc;
        private DevExpress.XtraEditors.TextEdit txtperdidadifcambio;
        private DevExpress.XtraEditors.TextEdit txtganciadifcambiodesc;
        private DevExpress.XtraEditors.TextEdit txtganciadifcambio;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtrendcajadesc;
        private DevExpress.XtraEditors.TextEdit txtrendcaja;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtitfdesc;
        private DevExpress.XtraEditors.TextEdit txtitf;
        private DevExpress.XtraEditors.TextEdit txtcajadesc;
        private DevExpress.XtraEditors.TextEdit txtcaja;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtretenciondesc;
        private DevExpress.XtraEditors.TextEdit txtretencion;
        private DevExpress.XtraEditors.TextEdit txtdetracciondesc;
        private DevExpress.XtraEditors.TextEdit txtdetraccion;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtpercepciondesc;
        private DevExpress.XtraEditors.TextEdit txtpercepcion;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtigvdesc;
        private DevExpress.XtraEditors.TextEdit txtigv;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnnuevo;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}