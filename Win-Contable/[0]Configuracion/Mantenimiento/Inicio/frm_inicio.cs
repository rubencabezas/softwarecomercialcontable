﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Inicio
{
    public partial class frm_inicio : frm_fuente
    {
        public frm_inicio()
        {
            InitializeComponent();
        }


   List<Entidad_Parametro_Inicial> Inicial = new List<Entidad_Parametro_Inicial>();
        public void MostrarConfiguracion()
        {
            try
            {
                    Logica_Parametro_Inicial Logica = new Logica_Parametro_Inicial();

                    Entidad_Parametro_Inicial Ent = new Entidad_Parametro_Inicial();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;

                    Inicial = Logica.Listar(Ent);
         
           if (Inicial.Count > 0)
                    {

                        //Ini_Venta = Dr.GetString(2),
                        //Ini_Venta_Desc = Dr.GetString(3),
                        //Ini_Venta_sunat = Dr.GetString(4),
                        //Ini_Compra = Dr.GetString(5),
                        //Ini_Compra_Desc = Dr.GetString(6),
                        //Ini_Compra_sunat = Dr.GetString(7),
                        //Ini_Diario = Dr.GetString(8),
                        //Ini_Diario_Desc = Dr.GetString(9),
                        //Ini_Diario_sunat = Dr.GetString(10)
                        LibVenta = Inicial[0].Ini_Venta;
                        txtlibventa.Text = Inicial[0].Ini_Venta_sunat;
                        txtlibventadesc.Text = Inicial[0].Ini_Venta_Desc;
                        LibCompra = Inicial[0].Ini_Compra;
                        txtlibcompra.Text = Inicial[0].Ini_Compra_sunat;
                        txtlibcompradesc.Text = Inicial[0].Ini_Compra_Desc;
                        LibDiario = Inicial[0].Ini_Diario;
                        txtlibdiario.Text = Inicial[0].Ini_Diario_sunat;
                        txtlibdiariodesc.Text = Inicial[0].Ini_Diario_Desc;

                        LibInventario = Inicial[0].Ini_Inventario;
                        txtlibinventario.Text=Inicial[0].Ini_Inventario_sunat;
                        txtlibinventariodesc.Text= Inicial[0].Ini_Inventario_Desc;

                        LibCajaBanco = Inicial[0].Ini_CajaBanco;
                        txtlibCajaBanco.Text = Inicial[0].Ini_CajaBanco_sunat;
                        txtlibCajaBancodesc.Text = Inicial[0].Ini_CajaBanco_Desc;

                        LibRendicionCaja = Inicial[0].Ini_Rendicion_Caja_Chica;
                        txtrendcaja.Text = Inicial[0].Ini_Rendicion_Caja_Chica_Sunat;
                        txtrendcajadesc.Text = Inicial[0].Ini_Rendicion_Caja_Chica_Desc;

                        Ini_Cta_Ganancia_Dif_Cambio= Inicial[0].Ini_Cta_Ganancia_Dif_Cambio;
                        txtganciadifcambio.Text = Inicial[0].Ini_Cta_Ganancia_Dif_Cambio;
                        txtganciadifcambiodesc.Text = Inicial[0].Ini_Cta_Ganancia_Dif_Cambio_Desc;

                        Ini_Cta_Perdida_Dif_Cambio = Inicial[0].Ini_Cta_Perdida_Dif_Cambio;
                        txtperdidadifcambio.Text = Inicial[0].Ini_Cta_Perdida_Dif_Cambio;
                        txtperdidadifcambiodesc.Text = Inicial[0].Ini_Cta_Perdida_Dif_Cambio_Desc;

                        Ini_Cta_Ganancia_Redondeo = Inicial[0].Ini_Cta_Ganancia_Redondeo;
                        txtgananciaredondeo.Text= Inicial[0].Ini_Cta_Ganancia_Redondeo;
                        txtgananciaredondeodesc.Text = Inicial[0].Ini_Cta_Ganancia_Redondeo_Desc;

                        Ini_Cta_Perdida_Redondeo = Inicial[0].Ini_Cta_Perdida_Redondeo;
                        txtperdidaredondeo.Text = Inicial[0].Ini_Cta_Perdida_Redondeo;
                        txtperdidaredondeodesc.Text = Inicial[0].Ini_Cta_Perdida_Redondeo_Desc;

                        Ini_Cta_ITF = Inicial[0].Ini_Cta_ITF;
                        txtitf.Text = Inicial[0].Ini_Cta_ITF;
                        txtitfdesc.Text = Inicial[0].Ini_Cta_ITF_Desc;

                        Ini_Cta_Caja = Inicial[0].Ini_Cta_Caja;
                        txtcaja.Text = Inicial[0].Ini_Cta_Caja;
                        txtcajadesc.Text = Inicial[0].Ini_Cta_Caja_Desc;



                        txtpercepcion.Text  = Inicial[0].Ini_Cta_Percepcion  ;
                        txtpercepciondesc.Text = Inicial[0].Ini_Cta_Percepcion_Desc;

                        txtdetraccion.Text = Inicial[0].Ini_Cta_Detraccion ;
                        txtdetracciondesc.Text = Inicial[0].Ini_Cta_Detraccion_Desc;

                        txtretencion.Text = Inicial[0].Ini_Cta_Retencion ;
                        txtretenciondesc.Text = Inicial[0].Ini_Cta_Retencion_Desc;

                    txtigv.Text = Inicial[0].Ini_Cta_Igv;
                    txtigvdesc.Text = Inicial[0].Ini_Cta_Igv_Desc;


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtlibventa_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        public void libroVenta()
        {
            try
            {
               
                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_SUNAT = txtlibventa.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtlibventa.Text.Trim().ToUpper())
                        {
                            LibVenta = (T.Id_Libro).ToString().Trim();
                            txtlibventa.Text = (T.Id_SUNAT).ToString().Trim();
                            txtlibventadesc.Text = T.Nombre_Libro;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void libroCompra()
        {
            try
            {

                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_SUNAT = txtlibcompra.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtlibcompra.Text.Trim().ToUpper())
                        {
                            LibCompra = (T.Id_Libro).ToString().Trim();
                            txtlibcompra.Text = (T.Id_SUNAT).ToString().Trim();
                            txtlibcompradesc.Text = T.Nombre_Libro;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void libroDiario()
        {
            try
            {

                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_SUNAT = txtlibdiario.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtlibdiario.Text.Trim().ToUpper())
                        {
                            LibDiario = (T.Id_Libro).ToString().Trim();
                            txtlibdiario.Text = (T.Id_SUNAT).ToString().Trim();
                            txtlibdiariodesc.Text = T.Nombre_Libro;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        String LibVenta =null;
        String LibCompra = null;
        String LibDiario = null;
        String LibInventario = null;
        String LibCajaBanco = null;
        String LibRendicionCaja = null;

        String Ini_Cta_Ganancia_Dif_Cambio = null;
        String Ini_Cta_Perdida_Dif_Cambio = null;
        String Ini_Cta_Ganancia_Redondeo = null;
        String Ini_Cta_Perdida_Redondeo = null;
        String Ini_Cta_ITF = null;
        String Ini_Cta_Caja = null;

        private void txtlibventa_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlibventa.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlibventa.Text.Substring(txtlibventa.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                LibVenta = Entidad.Id_Libro;
                                txtlibventa.Text = Entidad.Id_SUNAT;
                                txtlibventadesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlibventa.Text) & string.IsNullOrEmpty(txtlibventadesc.Text))
                    {
                        libroVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void txtlibcompra_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlibcompra.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlibcompra.Text.Substring(txtlibcompra.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                               LibCompra = Entidad.Id_Libro;
                                txtlibcompra.Text = Entidad.Id_SUNAT;
                                txtlibcompradesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlibcompra.Text) & string.IsNullOrEmpty(txtlibcompradesc.Text))
                    {
                        libroCompra();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void txtlibdiario_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlibdiario.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlibdiario.Text.Substring(txtlibdiario.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                LibDiario = Entidad.Id_Libro;
                                txtlibdiario.Text = Entidad.Id_SUNAT;
                                txtlibdiariodesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlibdiario.Text) & string.IsNullOrEmpty(txtlibdiariodesc.Text))
                    {
                        libroDiario();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Entidad_Parametro_Inicial Ent = new Entidad_Parametro_Inicial();
            Logica_Parametro_Inicial Log = new Logica_Parametro_Inicial();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio =Actual_Conexion.AnioSelect;
            Ent.Ini_Venta = LibVenta; //txtlibventa.Tag.ToString().Trim();
            Ent.Ini_Compra = LibCompra;//(txtlibcompra.Tag.ToString() == null ? null : txtlibcompra.Tag.ToString());

            Ent.Ini_Diario = LibDiario; //txtlibdiario.Tag.ToString().Trim();
            Ent.Ini_Inventario = LibInventario;

            Ent.Ini_CajaBanco = LibCajaBanco;

            Ent.Ini_Rendicion_Caja_Chica = LibRendicionCaja;
            Ent.Ini_Cta_Ganancia_Dif_Cambio = txtganciadifcambio.Text;
            Ent.Ini_Cta_Perdida_Dif_Cambio = txtperdidadifcambio.Text;
            Ent.Ini_Cta_Ganancia_Redondeo = txtgananciaredondeo.Text;
            Ent.Ini_Cta_Perdida_Redondeo = txtperdidaredondeo.Text;
            Ent.Ini_Cta_ITF = txtitf.Text;
            Ent.Ini_Cta_Caja = txtcaja.Text;
            Ent.Ini_Cta_Caja_Desc = txtcajadesc.Text;



            Ent.Ini_Cta_Percepcion = txtpercepcion.Text;
     
            Ent.Ini_Cta_Detraccion = txtdetraccion.Text;
    
            Ent.Ini_Cta_Retencion = txtretencion.Text;


            Ent.Ini_Cta_Igv = txtigv.Text.Trim();
            Ent.Ini_Cta_Igv_Desc = txtigvdesc.Text.Trim();


            try
            {
                //if (Estado == Estados.Nuevo)
                //{

                    if (Log.Insertar(Ent))
                    {

                    Accion.ExitoGuardar();
                    }
                    else
                    {
                    Accion.Advertencia("No se pudo insertar");
                    }
                //}
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_inicio_Load(object sender, EventArgs e)
        {
            MostrarConfiguracion();
        }

        private void txtlibinventario_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlibinventario.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlibinventario.Text.Substring(txtlibinventario.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                LibInventario = Entidad.Id_Libro;
                                txtlibinventario.Text = Entidad.Id_SUNAT;
                                txtlibinventariodesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlibinventario.Text) & string.IsNullOrEmpty(txtlibinventariodesc.Text))
                    {
                        libroInventario();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void libroInventario()
        {
            try
            {

                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_SUNAT = txtlibinventario.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtlibdiario.Text.Trim().ToUpper())
                        {
                            LibInventario = (T.Id_Libro).ToString().Trim();
                            txtlibinventario.Text = (T.Id_SUNAT).ToString().Trim();
                            txtlibinventariodesc.Text = T.Nombre_Libro;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtlibCajaBanco_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtlibCajaBanco.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtlibCajaBanco.Text.Substring(txtlibCajaBanco.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                LibCajaBanco = Entidad.Id_Libro;
                                txtlibCajaBanco.Text = Entidad.Id_SUNAT;
                                txtlibCajaBancodesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtlibCajaBanco.Text) & string.IsNullOrEmpty(txtlibCajaBancodesc.Text))
                    {
                        libroCaja();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void libroCaja()
        {
            try
            {

                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_SUNAT = txtlibCajaBanco.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtlibCajaBanco.Text.Trim().ToUpper())
                        {
                            LibCajaBanco = (T.Id_Libro).ToString().Trim();
                            txtlibCajaBanco.Text = (T.Id_SUNAT).ToString().Trim();
                            txtlibCajaBancodesc.Text = T.Nombre_Libro;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txtrendcaja_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrendcaja.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrendcaja.Text.Substring(txtrendcaja.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_libros_busqueda f = new _1_Busquedas_Generales.frm_libros_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                LibRendicionCaja = Entidad.Id_Libro;
                                txtrendcaja.Text = Entidad.Id_SUNAT;
                                txtrendcajadesc.Text = Entidad.Nombre_Libro;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrendcaja.Text) & string.IsNullOrEmpty(txtrendcajadesc.Text))
                    {
                        libroRendicionCaja();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void libroRendicionCaja()
        {
            try
            {

                Logica_Ctb_Libro log = new Logica_Ctb_Libro();

                List<Entidad_Ctb_Libro> Generales = new List<Entidad_Ctb_Libro>();
                Generales = log.Listar(new Entidad_Ctb_Libro
                {
                    Id_SUNAT = txtrendcaja.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Ctb_Libro T in Generales)
                    {
                        if ((T.Id_SUNAT).ToString().Trim().ToUpper() == txtrendcaja.Text.Trim().ToUpper())
                        {
                            LibRendicionCaja = (T.Id_Libro).ToString().Trim();
                            txtrendcaja.Text = (T.Id_SUNAT).ToString().Trim();
                            txtrendcajadesc.Text = T.Nombre_Libro;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtganciadifcambio_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtganciadifcambio.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtganciadifcambio.Text.Substring(txtganciadifcambio.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtganciadifcambio.Text = Entidad.Id_Cuenta;
                                txtganciadifcambiodesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtganciadifcambio.Text) & string.IsNullOrEmpty(txtganciadifcambiodesc.Text))
                    {
                        CuentaGananciaCambio();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaGananciaCambio()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtganciadifcambio.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtganciadifcambio.Text.Trim().ToUpper())
                        {
                            txtganciadifcambio.Text = (T.Id_Cuenta).ToString().Trim();
                            txtganciadifcambiodesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtganciadifcambio.EnterMoveNextControl = false;
                    txtganciadifcambio.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtperdidadifcambio_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtperdidadifcambio.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtperdidadifcambio.Text.Substring(txtperdidadifcambio.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtperdidadifcambio.Text = Entidad.Id_Cuenta;
                                txtperdidadifcambiodesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtperdidadifcambio.Text) & string.IsNullOrEmpty(txtperdidadifcambiodesc.Text))
                    {
                        CuentaPerdidaDifCambio();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaPerdidaDifCambio()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtperdidadifcambio.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtperdidadifcambio.Text.Trim().ToUpper())
                        {
                            txtperdidadifcambio.Text = (T.Id_Cuenta).ToString().Trim();
                            txtperdidadifcambiodesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtperdidadifcambio.EnterMoveNextControl = false;
                    txtperdidadifcambio.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtgananciaredondeo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtgananciaredondeo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtgananciaredondeo.Text.Substring(txtgananciaredondeo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtgananciaredondeo.Text = Entidad.Id_Cuenta;
                                txtgananciaredondeodesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtgananciaredondeo.Text) & string.IsNullOrEmpty(txtgananciaredondeodesc.Text))
                    {
                        CuentaGananciaRedondeo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaGananciaRedondeo()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtgananciaredondeo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtgananciaredondeo.Text.Trim().ToUpper())
                        {
                            txtgananciaredondeo.Text = (T.Id_Cuenta).ToString().Trim();
                            txtgananciaredondeodesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtgananciaredondeo.EnterMoveNextControl = false;
                    txtgananciaredondeo.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtperdidaredondeo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtperdidaredondeo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtperdidaredondeo.Text.Substring(txtperdidaredondeo.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtperdidaredondeo.Text = Entidad.Id_Cuenta;
                                txtperdidaredondeodesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtperdidaredondeo.Text) & string.IsNullOrEmpty(txtperdidaredondeodesc.Text))
                    {
                        CuentaPerdidaRedondeo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaPerdidaRedondeo()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtperdidaredondeo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtperdidaredondeo.Text.Trim().ToUpper())
                        {
                            txtperdidaredondeo.Text = (T.Id_Cuenta).ToString().Trim();
                            txtperdidaredondeodesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtperdidaredondeo.EnterMoveNextControl = false;
                    txtperdidaredondeo.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtitf_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtitf.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtitf.Text.Substring(txtitf.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtitf.Text = Entidad.Id_Cuenta;
                                txtitfdesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtitf.Text) & string.IsNullOrEmpty(txtitfdesc.Text))
                    {
                        CuentaITF();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaITF()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtitf.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtitf.Text.Trim().ToUpper())
                        {
                            txtitf.Text = (T.Id_Cuenta).ToString().Trim();
                            txtitfdesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtitf.EnterMoveNextControl = false;
                    txtitf.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcaja_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcaja.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcaja.Text.Substring(txtcaja.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcaja.Text = Entidad.Id_Cuenta;
                                txtcajadesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcaja.Text) & string.IsNullOrEmpty(txtcajadesc.Text))
                    {
                        CuentaCaja();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaCaja()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtcaja.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtcaja.Text.Trim().ToUpper())
                        {
                            txtcaja.Text = (T.Id_Cuenta).ToString().Trim();
                            txtcajadesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcaja.EnterMoveNextControl = false;
                    txtcaja.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtpercepcion_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpercepcion.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpercepcion.Text.Substring(txtpercepcion.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpercepcion.Text = Entidad.Id_Cuenta;
                                txtpercepciondesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpercepcion.Text) & string.IsNullOrEmpty(txtpercepciondesc.Text))
                    {
                        CuentaPercepcion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaPercepcion()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtpercepcion.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtpercepcion.Text.Trim().ToUpper())
                        {
                            txtpercepcion.Text = (T.Id_Cuenta).ToString().Trim();
                            txtpercepciondesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtpercepcion.EnterMoveNextControl = false;
                    txtpercepcion.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtdetraccion_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtdetraccion.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtdetraccion.Text.Substring(txtdetraccion.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtdetraccion.Text = Entidad.Id_Cuenta;
                                txtdetracciondesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtdetraccion.Text) & string.IsNullOrEmpty(txtdetracciondesc.Text))
                    {
                        CuentaDetraccion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaDetraccion()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtdetraccion.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtdetraccion.Text.Trim().ToUpper())
                        {
                            txtdetraccion.Text = (T.Id_Cuenta).ToString().Trim();
                            txtdetracciondesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtdetraccion.EnterMoveNextControl = false;
                    txtdetraccion.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtretencion_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtretencion.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtretencion.Text.Substring(txtretencion.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtretencion.Text = Entidad.Id_Cuenta;
                                txtretenciondesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtretencion.Text) & string.IsNullOrEmpty(txtretenciondesc.Text))
                    {
                        CuentaRetencion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaRetencion()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtretencion.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtretencion.Text.Trim().ToUpper())
                        {
                            txtretencion.Text = (T.Id_Cuenta).ToString().Trim();
                            txtretenciondesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtretencion.EnterMoveNextControl = false;
                    txtretencion.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtigv_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtigv.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtigv.Text.Substring(txtigv.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_plan_empresarial_busqueda f = new _1_Busquedas_Generales.frm_plan_empresarial_busqueda())
                        {
                            //f.Cuenta = txtcuentacargo.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Plan_Empresarial Entidad = new Entidad_Plan_Empresarial();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtigv.Text = Entidad.Id_Cuenta;
                                txtigvdesc.Text = Entidad.Cta_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtigv.Text) & string.IsNullOrEmpty(txtigvdesc.Text))
                    {
                        CuentaIgv();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void CuentaIgv()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_Plan_Empresarial log = new Logica_Plan_Empresarial();

                List<Entidad_Plan_Empresarial> Generales = new List<Entidad_Plan_Empresarial>();
                Generales = log.Busqueda(new Entidad_Plan_Empresarial
                {
                    Id_Cuenta = txtigv.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Plan_Empresarial T in Generales)
                    {
                        if ((T.Id_Cuenta).ToString().Trim().ToUpper() == txtigv.Text.Trim().ToUpper())
                        {
                            txtigv.Text = (T.Id_Cuenta).ToString().Trim();
                            txtigvdesc.Text = T.Cta_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtigv.EnterMoveNextControl = false;
                    txtigv.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }















    }
}
