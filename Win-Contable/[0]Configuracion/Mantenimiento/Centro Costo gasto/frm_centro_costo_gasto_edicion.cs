﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Centro_Costo_gasto
{
    public partial class frm_centro_costo_gasto_edicion : Contable.frm_fuente
    {
        public frm_centro_costo_gasto_edicion()
        {
            InitializeComponent();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void txttipocodcg_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txttipocodcg_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocodcg.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipocodcg.Text.Substring(txttipocodcg.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0008";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipocodcg.Tag = Entidad.Id_General_Det;
                                txttipocodcg.Text = Entidad.Gen_Codigo_Interno;
                                txttipodesccg.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocodcg.Text) & string.IsNullOrEmpty(txttipodesccg.Text))
                    {
                        BuscarTipo();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }

        }
            public void BuscarTipo()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0008"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Id_General).ToString().Trim().ToUpper() == txttipocodcg.Text.Trim().ToUpper())
                        {
                            txttipocodcg.Tag = (T.Id_General_Det);
                            txttipocodcg.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodesccg.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtunidadcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtunidadcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunidadcod.Text.Substring(txtunidadcod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_unidad_busqueda f = new _1_Busquedas_Generales.frm_unidad_busqueda())
                        {
               
                            
                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad Entidad = new Entidad_Unidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunidadcod.Text = Entidad.Id_Unidad;
                                txtunidaddesc.Text = Entidad.Und_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocodcg.Text) & string.IsNullOrEmpty(txttipodesccg.Text))
                    {
                        BuscarUnidad();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }


        public void BuscarUnidad()
        {
            try
            {
                txtunidadcod.Text = Accion.Formato(txtunidadcod.Text, 3);
                Logica_Unidad log = new Logica_Unidad();

                List<Entidad_Unidad> Generales = new List<Entidad_Unidad>();
                Generales = log.Listar(new Entidad_Unidad
                {
                    Id_Unidad = txtunidadcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad T in Generales)
                    {
                        if ((T.Id_Unidad).ToString().Trim().ToUpper() == txtunidadcod.Text.Trim().ToUpper())
                        {
                            txtunidadcod.Text = (T.Id_Unidad).ToString().Trim();
                            txtunidaddesc.Text = T.Und_Descripcion;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}

