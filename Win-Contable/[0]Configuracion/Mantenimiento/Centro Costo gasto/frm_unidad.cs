﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Centro_Costo_gasto
{
    public partial class frm_unidad : Contable.frm_fuente
    {
        public frm_unidad()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_unidad_edicion f = new frm_unidad_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Unidad Ent = new Entidad_Unidad();
                    Logica_Unidad Log = new Logica_Unidad();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Und_Descripcion = f.txtnombre.Text.Trim();
           
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }


        public List<Entidad_Unidad> Lista = new List<Entidad_Unidad>();
        public void Listar()
        {
            Entidad_Unidad Ent = new Entidad_Unidad();
            Logica_Unidad log = new Logica_Unidad();
            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        Entidad_Unidad Entidad = new Entidad_Unidad();
        private void gridView1_Click(object sender, EventArgs e)
        {
            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_unidad_edicion f = new frm_unidad_edicion())
            {

                f.txtnombre.Tag = Entidad.Id_Unidad;
                f.txtnombre.Text = Entidad.Und_Descripcion.Trim();
             

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Unidad Ent = new Entidad_Unidad();
                    Logica_Unidad Log = new Logica_Unidad();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Unidad = f.txtnombre.Tag.ToString();
                    Ent.Und_Descripcion = f.txtnombre.Text.Trim();
               

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        private void frm_unidad_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "El registro a eliminar es el siguiente:" + Entidad.Und_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Unidad Ent = new Entidad_Unidad
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Unidad=Entidad.Id_Unidad
                    };

                    Logica_Unidad log = new Logica_Unidad();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
