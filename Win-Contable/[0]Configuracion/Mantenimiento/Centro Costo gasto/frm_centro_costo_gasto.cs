﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Centro_Costo_gasto
{
    public partial class frm_centro_costo_gasto : Contable.frm_fuente
    {
        public frm_centro_costo_gasto()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_centro_costo_gasto_edicion f = new frm_centro_costo_gasto_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Centro_Costo_Gasto Ent = new Entidad_Centro_Costo_Gasto();
                    Logica_Centro_Costo_Gasto Log = new Logica_Centro_Costo_Gasto();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Unidad = f.txtunidadcod.Text.Trim();
                    Ent.Ccg_Tipo_cg = f.txttipocodcg.Tag.ToString().Trim();
                    Ent.Ccg_Descripcion = f.txtdescripcion.Text.Trim();

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }


        public List<Entidad_Centro_Costo_Gasto> Lista = new List<Entidad_Centro_Costo_Gasto>();
        public void Listar()
        {
            Entidad_Centro_Costo_Gasto Ent = new Entidad_Centro_Costo_Gasto();
            Logica_Centro_Costo_Gasto log = new Logica_Centro_Costo_Gasto();

            try
            {
                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Centro_cg = null;

                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_centro_costo_gasto_Load(object sender, EventArgs e)
        {
            Listar();
        }

        Entidad_Centro_Costo_Gasto Entidad = new Entidad_Centro_Costo_Gasto();
        private void gridView1_Click(object sender, EventArgs e)
        {
            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_centro_costo_gasto_edicion f = new frm_centro_costo_gasto_edicion())
            {

                f.txttipocodcg.Tag = Entidad.Ccg_Tipo_cg;
                f.txttipocodcg.Text = Entidad.Gen_Codigo_Interno;
                f.txttipodesccg.Text = Entidad.Gen_Descripcion_Det;
                f.txtunidadcod.Text = Entidad.Id_Unidad;
                f.txtunidaddesc.Text = Entidad.Und_Descripcion;
                f.txtdescripcion.Text = Entidad.Ccg_Descripcion;


                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Centro_Costo_Gasto Ent = new Entidad_Centro_Costo_Gasto();
                    Logica_Centro_Costo_Gasto Log = new Logica_Centro_Costo_Gasto();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Entidad.Ccg_Tipo_cg     = f.txttipocodcg.Tag.ToString() ;
                    Entidad.Gen_Codigo_Interno =     f.txttipocodcg.Text  ;
                    Entidad.Gen_Descripcion_Det  =    f.txttipodesccg.Text;
                    Entidad.Id_Unidad   = f.txtunidadcod.Text ;
                    Entidad.Und_Descripcion   =  f.txtunidaddesc.Text;
                    Entidad.Ccg_Descripcion=   f.txtdescripcion.Text ;


                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }

        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "El registro a eliminar es el siguiente:" + Entidad.Ccg_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Centro_Costo_Gasto Ent = new Entidad_Centro_Costo_Gasto
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Centro_cg = Entidad.Id_Centro_cg
                    };

                    Logica_Centro_Costo_Gasto log = new Logica_Centro_Costo_Gasto();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\CentrodeCosto" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
