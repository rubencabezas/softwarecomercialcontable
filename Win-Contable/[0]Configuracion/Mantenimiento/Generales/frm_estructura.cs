﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_estructura : Contable.frm_fuente
    {
        public frm_estructura()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_estructura_edicion f = new frm_estructura_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Estructura Ent = new Entidad_Estructura();
                    Logica_Estructura Log = new Logica_Estructura();

                    Ent.Est_Descripcion = f.txtdesc.Text;
                    Ent.Est_Num_Digitos = f.txtdigitos.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }


        public List<Entidad_Estructura> Lista = new List<Entidad_Estructura>();
        public void Listar()
        {
            Entidad_Estructura Ent = new Entidad_Estructura();
            Logica_Estructura log = new Logica_Estructura();
            Ent.Id_Estructura = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        Entidad_Estructura Entidad = new Entidad_Estructura();
        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_estructura_edicion f = new frm_estructura_edicion())
            {

                f.txtdesc.Tag = Entidad.Id_Estructura;
                f.txtdesc.Text = Entidad.Est_Descripcion;
                f.txtdigitos.Text = Entidad.Est_Num_Digitos;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Estructura Ent = new Entidad_Estructura();
                    Logica_Estructura Log = new Logica_Estructura();


                    Ent.Id_Estructura = f.txtdesc.Tag.ToString();
                    Ent.Est_Descripcion = f.txtdesc.Text;
                    Ent.Est_Num_Digitos = f.txtdigitos.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                Listar();
                            }
                            else
                            {
                              Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        private void frm_estructura_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
            }
            catch (Exception ex) {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La estructura a eliminar es el siguiente :" + Entidad.Est_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Estructura Ent = new Entidad_Estructura
                    {
                        Id_Estructura = Entidad.Id_Estructura
                    };

                    Logica_Estructura log = new Logica_Estructura();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Estructura" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}