﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_plan_general_edicion : Contable.frm_fuente
    {
        public frm_plan_general_edicion()
        {
            InitializeComponent();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VerificarCabecera())
            {
                DialogResult = DialogResult.OK;
            }
          
        }

        public bool VerificarCabecera()
        {

            if (string.IsNullOrEmpty(txtelementodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe un elemento");
                txtidelemento.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtestructuradesc.Text.Trim()))
            {
                Accion.Advertencia("Debe una estructura");
                txtidestructura.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcuenta.Text.Trim()))
            {
                Accion.Advertencia("Debe una cuenta contable");
                txtcuenta.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtcuentadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar la descripcion de la cuenta contable");
                txtcuentadesc.Focus();
                return false;
            }

            return true;
        }

        private void txtcuenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcuenta.Text.Trim()))
            {
                TraerDatos();
            }
        }

        string Cta = null;
   
        public void TraerDatos()
        {
     
            if (!string.IsNullOrEmpty(txtcuenta.Text))
            {
                try
                {
                    Entidad_Plan_General Entidad = new  Entidad_Plan_General();

                    //Entidad_Plan_General Ent = /*default*/(Entidad_Plan_General);


                    Entidad_Plan_General en = new Entidad_Plan_General();

                     Cta = null;
                    for (int i  = 0; i <= txtcuenta.Text.Trim().Length - 1; i++)
                    {
                        string Car = txtcuenta.Text.Substring(i, 1);
                        if (Car != " ")
                        {
                            Cta = Cta + Car;
                        }
                    }
                    //Ent_Cta.Cuenta = Cta
                    Logica_Plan_General log = new Logica_Plan_General();
                    en.Id_Cuenta = Cta;
                
                    Entidad =log.Traer(en);

                    txtidelemento.Text = Entidad.Id_Elemento;
                    txtelementodesc.Text = Entidad.Ele_Descripcion;
                    txtidestructura.Text = Entidad.Id_Estructura;
                    txtestructuradesc.Text = Entidad.Est_Descripcion;
                  
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);

                }
            }

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
