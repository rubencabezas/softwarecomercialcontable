﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_ejercicio_edicion : Contable.frm_fuente
    {
        public frm_ejercicio_edicion()
        {
            InitializeComponent();
        }

        private void txtempcod_EditValueChanged(object sender, EventArgs e)
        {
            if (txtempcod.Focused)
            {
                txtempdesc.Text = "";
            }
        }

        private void txtempcod_KeyDown(object sender, KeyEventArgs e)
        {


            if (String.IsNullOrEmpty(txtempcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtempcod.Text.Substring(txtempcod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_empresa_busqueda f = new _1_Busquedas_Generales.frm_empresa_busqueda())
                        {

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Empresa Entidad = new Entidad_Empresa();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtempcod.Text = Entidad.Id_Empresa;
                                txtempdesc.Text = Entidad.Emp_Nombre;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtempcod.Text) & string.IsNullOrEmpty(txtempdesc.Text))
                    {
                        BuscarTrabajador();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }


        public void BuscarTrabajador()
        {
            try
            {
                txtempcod.Text = Accion.Formato(txtempcod.Text, 10);
                Logica_Empresa log = new Logica_Empresa();

                List<Entidad_Empresa> Generales = new List<Entidad_Empresa>();
                Generales = log.Listar(new Entidad_Empresa
                {
                    Id_Empresa = txtempcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Empresa T in Generales)
                    {
                        if ((T.Id_Empresa).ToString().Trim().ToUpper() == txtempcod.Text.Trim().ToUpper())
                        {
                            txtempcod.Text = (T.Id_Empresa).ToString().Trim();
                            txtempdesc.Text = T.Emp_Nombre;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
