﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_elemento : Contable.frm_fuente
    {
        public frm_elemento()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_elemto_edicion f = new frm_elemto_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Elemento Ent = new Entidad_Elemento();
                    Logica_Elemento Log = new Logica_Elemento();

                    Ent.Ele_Descripcion = f.txtdescripcion.Text;
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                               Accion.Advertencia("No se puede insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }


        public List<Entidad_Elemento> Lista = new List<Entidad_Elemento>();
        public void Listar()
        {
            Entidad_Elemento Ent = new Entidad_Elemento();
            Logica_Elemento log = new Logica_Elemento();
            Ent.Id_Elemento = null ;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_elemento_Load(object sender, EventArgs e)
        {
            Listar();
        }



        Entidad_Elemento Entidad = new Entidad_Elemento();

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_elemto_edicion f = new frm_elemto_edicion())
            {

                f.txtdescripcion.Tag = Entidad.Id_Elemento;
                f.txtdescripcion.Text = Entidad.Ele_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Elemento Ent = new Entidad_Elemento();
                    Logica_Elemento Log = new Logica_Elemento();


                    Ent.Id_Elemento = f.txtdescripcion.Tag.ToString();
                    Ent.Ele_Descripcion = f.txtdescripcion.Text;
    
                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
              if (Lista.Count > 0)
                        {
                            Estado = Estados.Ninguno;
                            Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                        }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "El elemento a eliminar es el siguiente :" + Entidad.Ele_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Elemento Ent = new Entidad_Elemento
                    {
                        Id_Elemento = Entidad.Id_Elemento
                    };

                    Logica_Elemento log = new Logica_Elemento();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void dgvdatos_Click(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Elemento" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
