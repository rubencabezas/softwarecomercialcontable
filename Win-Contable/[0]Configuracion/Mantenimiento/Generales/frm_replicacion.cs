﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable
{
    public partial class frm_replicacion : frm_fuente
    {
        public frm_replicacion()
        {
            InitializeComponent();
        }


        void LimpiarCab(){

            txtempcod.ResetText();
            txtempdesc.ResetText();
            txtempcod2.ResetText();
            txtempdesc2.ResetText();
            comboanio.DataSource = null;
            comboanio2.DataSource = null;
            chkanalisiscontable.Checked = false;
            chkcuentascorrientes.Checked = false;
            chkparametosiniciales.Checked = false;
            chkplancontable.Checked = false;

            txtempcod.Focus();

        }
        private void txtempcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtempcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtempcod.Text.Substring(txtempcod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_empresa_busqueda f = new _1_Busquedas_Generales.frm_empresa_busqueda())
                        {

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Empresa Entidad = new Entidad_Empresa();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtempcod.Text = Entidad.Id_Empresa;
                                txtempdesc.Text = Entidad.Emp_Nombre;
                                ListarAnioOrigen();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtempcod.Text) & string.IsNullOrEmpty(txtempdesc.Text))
                    {
                        BuscarEmpresa();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }


        public void BuscarEmpresa()
        {
            try
            {
                txtempcod.Text = Accion.Formato(txtempcod.Text, 3);
                Logica_Empresa log = new Logica_Empresa();

                List<Entidad_Empresa> Generales = new List<Entidad_Empresa>();
                Generales = log.Listar(new Entidad_Empresa
                {
                    Id_Empresa = txtempcod.Text,
                    Usuario_dni = Actual_Conexion.UserID
            });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Empresa T in Generales)
                    {
                        if ((T.Id_Empresa).ToString().Trim().ToUpper() == txtempcod.Text.Trim().ToUpper())
                        {
                            txtempcod.Text = (T.Id_Empresa).ToString().Trim();
                            txtempdesc.Text = T.Emp_Nombre;
                            ListarAnioOrigen();
                            comboanio.Focus();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }




        void ListarAnioOrigen()
        {
            try
            {

                List<Entidad_Ejercicio> Anios = new List<Entidad_Ejercicio>();
                List<Entidad_Ejercicio> Anios_Aux = new List<Entidad_Ejercicio>();
                Logica_Ejercicio LogAnio = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = txtempcod.Text;

                Anios = LogAnio.Listar(Ent);

                var PrivView = from item in Anios
                               orderby item.Id_Anio descending
                               select item;
                Anios_Aux = PrivView.ToList();


                comboanio.ValueMember = "Id_Anio";
                comboanio.DisplayMember = "Id_Anio";
                comboanio.DataSource = Anios_Aux;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        private void txtempcod2_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtempcod2.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtempcod2.Text.Substring(txtempcod2.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_empresa_busqueda f = new _1_Busquedas_Generales.frm_empresa_busqueda())
                        {

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Empresa Entidad = new Entidad_Empresa();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtempcod2.Text = Entidad.Id_Empresa;
                                txtempdesc2.Text = Entidad.Emp_Nombre;
                                ListarAnioDestino();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtempcod2.Text) & string.IsNullOrEmpty(txtempdesc2.Text))
                    {
                        BuscarEmpresa2();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }


        public void BuscarEmpresa2()
        {
            try
            {
                txtempcod2.Text = Accion.Formato(txtempcod2.Text, 3);
                Logica_Empresa log = new Logica_Empresa();

                List<Entidad_Empresa> Generales = new List<Entidad_Empresa>();
                Generales = log.Listar(new Entidad_Empresa
                {
                    Id_Empresa = txtempcod2.Text,
                    Usuario_dni = Actual_Conexion.UserID
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Empresa T in Generales)
                    {
                        if ((T.Id_Empresa).ToString().Trim().ToUpper() == txtempcod2.Text.Trim().ToUpper())
                        {
                            txtempcod2.Text = (T.Id_Empresa).ToString().Trim();
                            txtempdesc2.Text = T.Emp_Nombre;
                            ListarAnioDestino();
                            comboanio.Focus();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void ListarAnioDestino()
        {
            try
            {

                List<Entidad_Ejercicio> Anios = new List<Entidad_Ejercicio>();
                List<Entidad_Ejercicio> Anios_Aux = new List<Entidad_Ejercicio>();
                Logica_Ejercicio LogAnio = new Logica_Ejercicio();
                Entidad_Ejercicio Ent = new Entidad_Ejercicio();

                Ent.Id_Empresa = txtempcod2.Text;

                Anios = LogAnio.Listar(Ent);

                var PrivView = from item in Anios
                               orderby item.Id_Anio descending
                               select item;
                Anios_Aux = PrivView.ToList();


                comboanio2.ValueMember = "Id_Anio";
                comboanio2.DisplayMember = "Id_Anio";
                comboanio2.DataSource = Anios_Aux;

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Ejercicio Ent = new Entidad_Ejercicio();
                    Logica_Ejercicio Log = new Logica_Ejercicio();


                    Ent.Id_Empresa = txtempcod.Text;
                    Ent.Id_Anio = comboanio.SelectedValue.ToString();
                    Ent.Id_Empresa_Destino = txtempcod2.Text;
                    Ent.Id_Anio_Destino = comboanio2.SelectedValue.ToString();

                    Ent.Es_Plan_contable = chkplancontable.Checked;
                    Ent.Es_Analisis = chkanalisiscontable.Checked;
                    Ent.Es_Cuenta_Corriente = chkcuentascorrientes.Checked;
                    Ent.Es_Parametro_inicial = chkparametosiniciales.Checked;

                  
                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Replicacion_General(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();

                        }

                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool VerificarCabecera()
        {

            if (string.IsNullOrEmpty(txtempdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una empresa de origen");
                txtempcod.Focus();
                return false;
            }
           if (string.IsNullOrEmpty(txtempdesc2.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una empresa de origen");
                txtempcod2.Focus();
                return false;
            }
            if (comboanio.SelectedValue.ToString() ==comboanio2.SelectedValue.ToString())
            {
                Accion.Advertencia("El año de origen debe ser menor al año de destino");
                txtempcod2.Focus();
                return false;
            }


            if (chkplancontable.Checked==false || chkanalisiscontable.Checked==false || chkcuentascorrientes.Checked==false || chkparametosiniciales.Checked==false)
            {
                Accion.Advertencia("Debe seleccionar todas las opciones para poder replicar");
             
                return false;
            }

            
            return true;
        }

        private void frm_replicacion_Load(object sender, EventArgs e)
        {
            Estado = Estados.Nuevo;
        }

        private void txtempcod_TextChanged(object sender, EventArgs e)
        {
            if (txtempcod.Focused == false)
            {
                txtempdesc.ResetText();
            }
        }

        private void txtempcod2_TextChanged(object sender, EventArgs e)
        {
            if (txtempcod2.Focused == false)
            {
                txtempdesc2.ResetText();
            }
        }


    }
}
