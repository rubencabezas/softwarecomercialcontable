﻿using DevExpress.CodeParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_plan_general_subir_archivo : frm_fuente
    {
        public frm_plan_general_subir_archivo()
        {
            InitializeComponent();
        }

        List<Entidad_Plan_General> ListaFinal = new List<Entidad_Plan_General>();
        private void btnbuscararchivo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                List<Entidad_Plan_General> ListaArchivo = new List<Entidad_Plan_General>();
                ListaArchivo.Clear();
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Archivos Excel |*.xls;*.xlsx";
                dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);//My.Computer.FileSystem.SpecialDirectories.Desktop;
                dialog.Title = "Seleccione el archivo a importar";
                if ((dialog.ShowDialog() == DialogResult.OK))
                {
                    DataTable dt;
                    dt = ImportExceltoDatatable(dialog.FileName);
                    if ((dt.Rows.Count > 0))
                    {
                        Mostrar_ProgressBar(true);
                        ProgressArchivo.Properties.Step = 1;
                        ProgressArchivo.Properties.PercentView = true;
                        ProgressArchivo.Properties.Maximum = dt.Rows.Count;
                        ProgressArchivo.Properties.Minimum = 0;
                        ProgressArchivo.EditValue = 0;

                        foreach (DataRow filas in dt.Rows)
                        {
                            Entidad_Plan_General EntAsien = new Entidad_Plan_General();

                            EntAsien.Id_Cuenta = filas["CUENTA"].ToString().Trim();

                            if (EntAsien.Id_Cuenta == "")
                            {
                                Accion.Advertencia("Uno de los detalles no tiene cuenta contable");
                                Mostrar_ProgressBar(false);
                                break;
                            }

                            EntAsien.Cta_Descripcion = filas["DESCRIPCION"].ToString().Trim().ToUpper();

                            if (EntAsien.Cta_Descripcion == "")
                            {
                                Accion.Advertencia("Uno de los detalles no tiene descripcion");
                                Mostrar_ProgressBar(false);
                                break;
                            }

                            Entidad_Plan_General Entidad_Busca_Estructura_Elemento = new Entidad_Plan_General();

                            Entidad_Busca_Estructura_Elemento = TraerDatos(EntAsien.Id_Cuenta);

                            EntAsien.Id_Elemento = Entidad_Busca_Estructura_Elemento.Id_Elemento;
                            EntAsien.Ele_Descripcion = Entidad_Busca_Estructura_Elemento.Ele_Descripcion;
                            EntAsien.Id_Estructura = Entidad_Busca_Estructura_Elemento.Id_Estructura;
                            EntAsien.Est_Descripcion = Entidad_Busca_Estructura_Elemento.Est_Descripcion;


                            ListaArchivo.Add(EntAsien);

                            ProgressArchivo.PerformStep();
                            ProgressArchivo.Update();

                        }

                        foreach (Entidad_Plan_General T in ListaArchivo)
                        {
                            ListaFinal.Add(T);
                        }


                        dgvdatos.DataSource = null;
                        dgvdatos.DataSource = ListaFinal;
                    }

                }

            }
            catch (Exception ex)
            {

                Accion.ErrorSistema(ex.Message);
                //Mostrar_ProgressBar(false);
                //MsgBox(Err.Description, MsgBoxStyle.Critical);

            }
        }


        public static DataTable ImportExceltoDatatable(string filepath)
        {
            //  string sqlquery= "Select * From [SheetName$] Where YourCondition";
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = new DataSet();
                string constring = ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                            + (filepath + ";Extended Properties=\"Excel 12.0;HDR=YES;\""));
                OleDbConnection con = new OleDbConnection((constring + ""));
                con.Open();
                object myTableName = con.GetSchema("Tables").Rows[0]["TABLE_NAME"];
                string sqlquery = string.Format("SELECT * FROM [{0}]", myTableName);
                //  "Select * From " & myTableName  
                OleDbDataAdapter da = new OleDbDataAdapter(sqlquery, con);
                da.Fill(ds);
                dt = ds.Tables[0];
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                //MsgBox(Err.Description, MsgBoxStyle.Critical);
                Accion.ErrorSistema(ex.Message);
                return dt;
            }

        }

        private Entidad_Plan_General TraerDatos(string cuenta)
        {
            string txtidelemento = null;

            Entidad_Plan_General Entidad_Final = new Entidad_Plan_General();

            if (!string.IsNullOrEmpty(cuenta))
            {
                try
                {
                    Entidad_Plan_General Entidad = new Entidad_Plan_General();
                    Entidad_Plan_General en = new Entidad_Plan_General();
                    
                    string Cta = null;
                

                    Cta = null;
                    for (int i = 0; i <= cuenta.Trim().Length - 1; i++)
                    {
                        string Car = cuenta.Substring(i, 1);
                        if (Car != " ")
                        {
                            Cta = Cta + Car;
                        }
                    }
                    //Ent_Cta.Cuenta = Cta
                    Logica_Plan_General log = new Logica_Plan_General();
                    en.Id_Cuenta = Cta;

                    Entidad = log.Traer(en);

                    Entidad_Final = Entidad;
                    //txtelementodesc.Text = Entidad.Ele_Descripcion;
                    //txtidestructura.Text = Entidad.Id_Estructura;
                    //txtestructuradesc.Text = Entidad.Est_Descripcion;
                    return Entidad_Final;
                }

                   
                catch (Exception ex)
                {
                   
                    Accion.ErrorSistema(ex.Message);

                }

            }

            return Entidad_Final;
           

        }

        void Mostrar_ProgressBar(bool Value)
        {
            ProgressArchivo.Visible = Value;
        }

        private void btnlimpiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ListaFinal.Clear();
                dgvdatos.DataSource = null;

            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        bool validar()
        {
            foreach (Entidad_Plan_General T in ListaFinal)
            {
                if (T.Est_Descripcion == "")
                {
                    Accion.Advertencia("Verificar que todas las cuentas tenga asignada una estructura,caso contrario verificar la configuracion de estructura");
                    return false;
                }

                if (T.Ele_Descripcion == "")
                {
                    Accion.Advertencia("Verificar que todas las cuentas tenga asignada un elemento,caso contrario verificar la configuracion de estructura");
                    return false;
                }
            }

            return true;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                    if (ListaFinal.Count() > 0)
                    {

                      if (validar())
                    {
                        Estado = Estados.Nuevo;

                        Entidad_Plan_General Ent = new Entidad_Plan_General();
                        Logica_Plan_General Log = new Logica_Plan_General();

                        Ent.DetallesCuentas = ListaFinal;

                        try
                        {
                            if (Estado == Estados.Nuevo)
                            {

                                if (Log.Insertar_En_Cantidad(Ent))
                                {
                                    Accion.ExitoGuardar();
                                    ListaFinal.Clear();
                                    dgvdatos.DataSource = null;
                                }
                                else
                                {
                                    Accion.Advertencia("No se puedo insertar");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                        }
                    }


 
                    }
                    else
                    {
                        Accion.Advertencia("No existen datos para guardar");
                    }
            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_plan_general_subir_archivo_Load(object sender, EventArgs e)
        {

        }
    }
}
