﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_empresa : Contable.frm_fuente
    {
        public frm_empresa()
        {
            InitializeComponent();
        }

        private void frm_empresa_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Empresa> Lista = new List<Entidad_Empresa>();
        public void Listar()
        {
            Entidad_Empresa Ent = new Entidad_Empresa();
            Logica_Empresa log = new Logica_Empresa();

            Ent.Usuario_dni = Actual_Conexion.UserID;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_empresa_edicion f = new frm_empresa_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Empresa Ent = new Entidad_Empresa();
                    Logica_Empresa Log = new Logica_Empresa();

                    Ent.Emp_Nombre = f.txtrz.Text;
                    Ent.Emp_Ruc = f.txtruc.Text;
                    Ent.Emp_Direccion = f.txtdireccion.Text;
                    Ent.Emp_Repres1 = f.txtrl.Text;
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }


        Entidad_Empresa Entidad = new Entidad_Empresa();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

  
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_empresa_edicion f = new frm_empresa_edicion())
            {

                f.txtrz.Tag = Entidad.Id_Empresa;
                f.txtrz.Text = Entidad.Emp_Nombre;
                f.txtruc.Text = Entidad.Emp_Ruc;
                f.txtdireccion.Text = Entidad.Emp_Direccion;
                f.txtrl.Text = Entidad.Emp_Repres1;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Empresa Ent = new Entidad_Empresa();
                    Logica_Empresa Log = new Logica_Empresa();


                    Ent.Id_Empresa = f.txtrz.Tag.ToString() ;
                    Ent.Emp_Nombre = f.txtrz.Text ;
                    Ent.Emp_Ruc = f.txtruc.Text ;
                    Ent.Emp_Direccion =f.txtdireccion.Text  ;
                    Ent.Emp_Repres1 = f.txtrl.Text ;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Empresa" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
