﻿namespace Contable
{
    partial class frm_replicacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.comboanio2 = new System.Windows.Forms.ComboBox();
            this.comboanio = new System.Windows.Forms.ComboBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtempdesc2 = new DevExpress.XtraEditors.TextEdit();
            this.txtempcod2 = new DevExpress.XtraEditors.TextEdit();
            this.txtempdesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtempcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.chkplancontable = new System.Windows.Forms.CheckBox();
            this.chkparametosiniciales = new System.Windows.Forms.CheckBox();
            this.chkcuentascorrientes = new System.Windows.Forms.CheckBox();
            this.chkanalisiscontable = new System.Windows.Forms.CheckBox();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnnuevo = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtempdesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtempcod2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtempdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtempcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.comboanio2);
            this.groupControl1.Controls.Add(this.comboanio);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtempdesc2);
            this.groupControl1.Controls.Add(this.txtempcod2);
            this.groupControl1.Controls.Add(this.txtempdesc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtempcod);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(821, 88);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // comboanio2
            // 
            this.comboanio2.FormattingEnabled = true;
            this.comboanio2.Location = new System.Drawing.Point(691, 59);
            this.comboanio2.Name = "comboanio2";
            this.comboanio2.Size = new System.Drawing.Size(69, 21);
            this.comboanio2.TabIndex = 9;
            // 
            // comboanio
            // 
            this.comboanio.FormattingEnabled = true;
            this.comboanio.Location = new System.Drawing.Point(691, 33);
            this.comboanio.Name = "comboanio";
            this.comboanio.Size = new System.Drawing.Size(69, 21);
            this.comboanio.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(604, 63);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(81, 13);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Ejercicio destino:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(603, 37);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Ejercicio origen:";
            // 
            // txtempdesc2
            // 
            this.txtempdesc2.Enabled = false;
            this.txtempdesc2.Location = new System.Drawing.Point(139, 60);
            this.txtempdesc2.Name = "txtempdesc2";
            this.txtempdesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempdesc2.Properties.Appearance.Options.UseFont = true;
            this.txtempdesc2.Size = new System.Drawing.Size(460, 20);
            this.txtempdesc2.TabIndex = 7;
            // 
            // txtempcod2
            // 
            this.txtempcod2.Location = new System.Drawing.Point(91, 60);
            this.txtempcod2.Name = "txtempcod2";
            this.txtempcod2.Properties.MaxLength = 3;
            this.txtempcod2.Size = new System.Drawing.Size(45, 20);
            this.txtempcod2.TabIndex = 6;
            this.txtempcod2.TextChanged += new System.EventHandler(this.txtempcod2_TextChanged);
            this.txtempcod2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtempcod2_KeyDown);
            // 
            // txtempdesc
            // 
            this.txtempdesc.Enabled = false;
            this.txtempdesc.Location = new System.Drawing.Point(139, 34);
            this.txtempdesc.Name = "txtempdesc";
            this.txtempdesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempdesc.Properties.Appearance.Options.UseFont = true;
            this.txtempdesc.Size = new System.Drawing.Size(460, 20);
            this.txtempdesc.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 63);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(83, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Empresa destino:";
            // 
            // txtempcod
            // 
            this.txtempcod.Location = new System.Drawing.Point(91, 34);
            this.txtempcod.Name = "txtempcod";
            this.txtempcod.Properties.MaxLength = 3;
            this.txtempcod.Size = new System.Drawing.Size(45, 20);
            this.txtempcod.TabIndex = 1;
            this.txtempcod.TextChanged += new System.EventHandler(this.txtempcod_TextChanged);
            this.txtempcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtempcod_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 37);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(78, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Empresa origen:";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl2.Controls.Add(this.chkplancontable);
            this.groupControl2.Controls.Add(this.chkparametosiniciales);
            this.groupControl2.Controls.Add(this.chkcuentascorrientes);
            this.groupControl2.Controls.Add(this.chkanalisiscontable);
            this.groupControl2.Location = new System.Drawing.Point(12, 132);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(821, 227);
            this.groupControl2.TabIndex = 1;
            // 
            // chkplancontable
            // 
            this.chkplancontable.AutoSize = true;
            this.chkplancontable.Location = new System.Drawing.Point(9, 29);
            this.chkplancontable.Name = "chkplancontable";
            this.chkplancontable.Size = new System.Drawing.Size(90, 17);
            this.chkplancontable.TabIndex = 0;
            this.chkplancontable.Text = "Plan contable";
            this.chkplancontable.UseVisualStyleBackColor = true;
            // 
            // chkparametosiniciales
            // 
            this.chkparametosiniciales.AutoSize = true;
            this.chkparametosiniciales.Location = new System.Drawing.Point(9, 98);
            this.chkparametosiniciales.Name = "chkparametosiniciales";
            this.chkparametosiniciales.Size = new System.Drawing.Size(120, 17);
            this.chkparametosiniciales.TabIndex = 3;
            this.chkparametosiniciales.Text = "Parametros iniciales";
            this.chkparametosiniciales.UseVisualStyleBackColor = true;
            // 
            // chkcuentascorrientes
            // 
            this.chkcuentascorrientes.AutoSize = true;
            this.chkcuentascorrientes.Location = new System.Drawing.Point(9, 75);
            this.chkcuentascorrientes.Name = "chkcuentascorrientes";
            this.chkcuentascorrientes.Size = new System.Drawing.Size(117, 17);
            this.chkcuentascorrientes.TabIndex = 2;
            this.chkcuentascorrientes.Text = "Cuentas corrientes";
            this.chkcuentascorrientes.UseVisualStyleBackColor = true;
            // 
            // chkanalisiscontable
            // 
            this.chkanalisiscontable.AutoSize = true;
            this.chkanalisiscontable.Location = new System.Drawing.Point(9, 52);
            this.chkanalisiscontable.Name = "chkanalisiscontable";
            this.chkanalisiscontable.Size = new System.Drawing.Size(105, 17);
            this.chkanalisiscontable.TabIndex = 1;
            this.chkanalisiscontable.Text = "Analisis contable";
            this.chkanalisiscontable.UseVisualStyleBackColor = true;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnnuevo,
            this.btnmodificar,
            this.btneliminar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 4;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnnuevo)});
            this.bar1.OptionsBar.DrawBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnnuevo
            // 
            this.btnnuevo.Caption = "Guardar [F4]";
            this.btnnuevo.Id = 0;
            this.btnnuevo.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnnuevo.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnnuevo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(845, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 361);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 329);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(845, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 329);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar  [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Exportar";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_replicacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(845, 361);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_replicacion";
            this.Text = "Replicacion de informacion";
            this.Load += new System.EventHandler(this.frm_replicacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtempdesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtempcod2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtempdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtempcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.CheckBox chkplancontable;
        private System.Windows.Forms.CheckBox chkparametosiniciales;
        private System.Windows.Forms.CheckBox chkcuentascorrientes;
        private System.Windows.Forms.CheckBox chkanalisiscontable;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.TextEdit txtempdesc2;
        public DevExpress.XtraEditors.TextEdit txtempcod2;
        public DevExpress.XtraEditors.TextEdit txtempdesc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        public DevExpress.XtraEditors.TextEdit txtempcod;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public System.Windows.Forms.ComboBox comboanio;
        public System.Windows.Forms.ComboBox comboanio2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnnuevo;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}
