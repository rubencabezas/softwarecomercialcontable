﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_plan_general : Contable.frm_fuente
    {
        public frm_plan_general()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_plan_general_edicion f = new frm_plan_general_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Plan_General Ent = new Entidad_Plan_General();
                    Logica_Plan_General Log = new Logica_Plan_General();

                    Ent.Id_Elemento = f.txtidelemento.Text;
                    Ent.Id_Estructura = f.txtidestructura.Text;
                    Ent.Id_Cuenta = f.txtcuenta.Text;
                    Ent.Cta_Descripcion = f.txtcuentadesc.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }


        public List<Entidad_Plan_General> Lista = new List<Entidad_Plan_General>();
        public void Listar()
        {
            Entidad_Plan_General Ent = new Entidad_Plan_General();
            Logica_Plan_General log = new Logica_Plan_General();
            Ent.Id_Cuenta = null;

            try
            {
                Lista = log.Listar(Ent);
                
                dgvdatos.DataSource = null;

                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_plan_general_Load(object sender, EventArgs e)
        {
            Listar();
        }


        Entidad_Plan_General Entidad = new Entidad_Plan_General();
        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_plan_general_edicion f = new frm_plan_general_edicion())
            {

    
                f.txtidelemento.Text   = Entidad.Id_Elemento ;
                f.txtelementodesc.Text = Entidad.Ele_Descripcion;
                f.txtidestructura.Text = Entidad.Id_Estructura ;
                f.txtestructuradesc.Text = Entidad.Est_Descripcion;
                f.txtcuenta.Text = Entidad.Id_Cuenta ;
                f.txtcuentadesc.Text  = Entidad.Cta_Descripcion ;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Plan_General Ent = new Entidad_Plan_General();
                    Logica_Plan_General Log = new Logica_Plan_General();


                    Ent.Id_Elemento   =  f.txtidelemento.Text ;
                    Ent.Ele_Descripcion = f.txtelementodesc.Text;

                    Ent.Id_Estructura =    f.txtidestructura.Text ;
                    Ent.Est_Descripcion = f.txtestructuradesc.Text;
                    Ent.Id_Cuenta  =   f.txtcuenta.Text ;
                    Ent.Cta_Descripcion  =    f.txtcuentadesc.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                                Accion.ExitoModificar();
                            }
                            else
                            {
                               Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {

            try
            {
             if (Lista.Count > 0)
                        {
                            Estado = Estados.Ninguno;
                            Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                        }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
           
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La cuenta a eliminar es el siguiente :" + Entidad.Id_Cuenta +" "+ Entidad.Cta_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Plan_General Ent = new Entidad_Plan_General
                    {
                        Id_Cuenta = Entidad.Id_Cuenta
                    };

                    Logica_Plan_General log = new Logica_Plan_General();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btncargararchivo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using ( frm_plan_general_subir_archivo f = new frm_plan_general_subir_archivo()) 
                {
                    
                    if (f.ShowDialog() == DialogResult.OK)
                    {

                    }
                    else
                    {
                        Listar();
                    }
                }

            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\PlanGeneral" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
