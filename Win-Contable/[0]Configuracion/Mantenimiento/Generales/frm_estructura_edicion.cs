﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_estructura_edicion : Contable.frm_fuente
    {
        public frm_estructura_edicion()
        {
            InitializeComponent();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VerificarCabecera())
            {
 DialogResult = DialogResult.OK;
            }
           
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtdesc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtdigitos.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar los digitos");
                txtdigitos.Focus();
                return false;
            }

            return true;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
