﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Generales
{
    public partial class frm_empresa_edicion : Contable.frm_fuente
    {
        public frm_empresa_edicion()
        {
            InitializeComponent();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        Retornar_Sunat BuscarRuc = new Retornar_Sunat();
        Retornar_Reniec BusquedaSunat = new Retornar_Reniec();
        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        private void txtruc_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter & txtruc.Text.Trim().Length == 11)
                {
                    if (ValidarRUC.validarRuc(txtruc.Text.Trim()) == true)
                    {
                        if (Verificacion_Internet.InternetConnection())
                        {
                            Entidad_Entidad Entidad = new Entidad_Entidad();
                            Entidad = BuscarRuc.Retornar_Sunat(txtruc.Text.Trim());


                            if (Entidad.Estado_No_Existe)
                            {

                            }
                            if (Entidad.EstadoContr != "ACTIVO")
                            {
                                Accion.Advertencia("El estado de esta endidad es" + " " + "NO ACTIVO");
                            }

                            if (Entidad.Estado_De_Consulta)
                            {
                                txtrz.Text = Entidad.Ent_Razon_Social_Nombre +" "+ IIf(Entidad.Ent_Ape_Paterno == null, "", Entidad.Ent_Ape_Paterno).ToString() +" "+ IIf(Entidad.Ent_Ape_Materno == null, "", Entidad.Ent_Ape_Materno).ToString();
                                txtdireccion.Text = Entidad.Ent_Domicilio_Fiscal;

                            }
                            //txttipopersonacod.Focus();
                        }
                    }
                    else
                    {
                        Accion.Advertencia("EL NUMERO DE RUC ES INCORRECTO");
                        //LimpiarCab();
                        txtruc.Focus();
                    }


                }
                else if (e.KeyCode == Keys.Enter & txtruc.Text.Trim().Length == 8)
                {
                    if (Verificacion_Internet.InternetConnection())
                    {
                        Entidad_Entidad Entidad = new Entidad_Entidad();
                        Entidad = BusquedaSunat.Retornar_Reniec(txtruc.Text.Trim());


                        if (Entidad.Estado_No_Existe)
                        {

                        }

                        if (Entidad.Estado_De_Consulta)
                        {
                            txtrz.Text = Entidad.Ent_Razon_Social_Nombre + " " + IIf(Entidad.Ent_Ape_Paterno == null, "", Entidad.Ent_Ape_Paterno).ToString() + " " + IIf(Entidad.Ent_Ape_Materno == null, "", Entidad.Ent_Ape_Materno).ToString();
                            txtdireccion.Text = Entidad.Ent_Domicilio_Fiscal;
                     
                            //txttipopersonacod.Focus();
                        }

                        //txttipopersonacod.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
