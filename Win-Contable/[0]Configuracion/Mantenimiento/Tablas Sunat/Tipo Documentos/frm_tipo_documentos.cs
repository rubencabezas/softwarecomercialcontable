﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Tablas_Sunat.Tipo_Documentos
{
    public partial class frm_tipo_documentos : Contable.frm_fuente
    {
        public frm_tipo_documentos()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_tipo_documento_edicion f = new frm_tipo_documento_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Comprobantes Ent = new Entidad_Comprobantes();
                    Logica_Comprobante Log = new Logica_Comprobante();

                    Ent.Nombre_Comprobante = f.txtdesc.Text;
                    Ent.Id_SUnat = f.txtsunat.Text;
                    Ent.Es_Documento_Interno = f.chkdocinterno.Checked;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                               Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
        }

                 public List<Entidad_Comprobantes> Lista = new List<Entidad_Comprobantes>();
        public void Listar()
        {
            Entidad_Comprobantes Ent = new Entidad_Comprobantes();
            Logica_Comprobante log = new Logica_Comprobante();

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_tipo_documento_edicion f = new frm_tipo_documento_edicion())
            {

                f.txtdesc.Tag = Entidad.Id_Comprobante;
                f.txtdesc.Text = Entidad.Nombre_Comprobante;
                f.txtsunat.Text = Entidad.Id_SUnat;
                f.chkdocinterno.Checked= Entidad.Es_Documento_Interno ;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Comprobantes Ent = new Entidad_Comprobantes();
                    Logica_Comprobante Log = new Logica_Comprobante();

                    Ent.Id_Comprobante = f.txtdesc.Tag.ToString();
                    Ent.Nombre_Comprobante = f.txtdesc.Text;
                    Ent.Id_SUnat = f.txtsunat.Text;
                    Ent.Es_Documento_Interno = f.chkdocinterno.Checked;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                Listar();
                            }
                            else
                            {
                               Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        Entidad_Comprobantes Entidad = new Entidad_Comprobantes();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        private void frm_tipo_documentos_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
                string DocDelted = "El comprobante a eliminar es el siguiente :" + Entidad.Nombre_Comprobante;
                if (Accion.ShowDeleted(DocDelted))
                {
                    try
                    {
                        Entidad_Comprobantes Ent = new Entidad_Comprobantes
                        {
                            Id_Comprobante = Entidad.Id_Comprobante
                        };

                        Logica_Comprobante log = new Logica_Comprobante();

                        log.Eliminar(Ent);
                        Accion.ExitoGuardar();
                        Listar();
                        
                    }
                    catch (Exception ex)
                    {
                    Accion.ErrorSistema(ex.Message);
                    }

                }
         



        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\TipoDocumento" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
 }
            
