﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Tablas_Sunat.Libros
{
    public partial class frm_libros_contables : Contable.frm_fuente
    {
        public frm_libros_contables()
        {
            InitializeComponent();
        }

        public override void CambiandoEstado()
        {
            if (Estado == Estados.Nuevo)
            {
                //Botones
                //btnNuevo.Enabled = false;
                //btnModificar.Enabled = false;
                //btnCancelar.Enabled = true;
                //btnGuardar.Enabled = true;
                //btnEliminar.Enabled = false;
                //Limpiar();
                //GbxGeneral.Enabled = true;
                //TxtEstablecimientoCodigo.Enabled = true;
                //TxtPuntoVentaCodigo.Enabled = true;
                //TxtEstablecimientoCodigo.Focus();

            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones
                //btnNuevo.Enabled = true;
                //btnModificar.Enabled = false;
                //btnCancelar.Enabled = false;
                //btnGuardar.Enabled = false;
                //btnEliminar.Enabled = false;
                //Limpiar();
                //GbxGeneral.Enabled = false;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones
                //btnNuevo.Enabled = false;
                //btnModificar.Enabled = false;
                //btnCancelar.Enabled = true;
                //btnGuardar.Enabled = true;
                //btnEliminar.Enabled = true;
                //GbxGeneral.Enabled = true;
                //TxtEstablecimientoCodigo.Enabled = false;
                //TxtPuntoVentaCodigo.Enabled = false;
                //TxtTipoDocCodigo.Focus();
            }
            else if (Estado == Estados.Guardado)
            {
                //Botones
                //btnNuevo.Enabled = true;
                //btnModificar.Enabled = false;
                //btnCancelar.Enabled = false;
                //btnGuardar.Enabled = false;
                //btnEliminar.Enabled = true;
                //GbxGeneral.Enabled = false;

            }
            else if (Estado == Estados.SoloLectura)
            {
                //Botones
                //btnNuevo.Enabled = false;
                //btnModificar.Enabled = false;
                //btnCancelar.Enabled = true;
                //btnGuardar.Enabled = false;
                //btnEliminar.Enabled = false;
                //GbxGeneral.Enabled = false;

            }
            else if (Estado == Estados.Consulta)
            {
                //GbxGeneral.Enabled = false;
                //btnNuevo.Enabled = true;
                //btnModificar.Enabled = true;
                //btnCancelar.Enabled = true;
                //btnGuardar.Enabled = false;
                //btnEliminar.Enabled = true;

            }
        }


        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_libros_contables_edicion f = new frm_libros_contables_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {
                
                    Entidad_Ctb_Libro Ent = new Entidad_Ctb_Libro();
                    Logica_Ctb_Libro Log = new Logica_Ctb_Libro();

                    Ent.Nombre_Libro = f.txtdesc.Text;
                    Ent.Id_SUNAT = f.txtsunat.Text;
                    Ent.Lib_CentralizacionMes = f.chkconsolidar.Checked;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();
                                
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                   }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }




        }

    public   List<Entidad_Ctb_Libro> Lista = new List<Entidad_Ctb_Libro>();
        public void Listar()
        {
            Entidad_Ctb_Libro Ent = new Entidad_Ctb_Libro();
            Logica_Ctb_Libro log = new Logica_Ctb_Libro();

            try
            {
                    Lista = log.Listar(Ent);
                        if (Lista.Count > 0)
                        {
                            dgvdatos.DataSource = Lista;
                        }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            

        }

        private void frm_libros_contables_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
                Estado = Estados.Modificar;

            using (frm_libros_contables_edicion f = new frm_libros_contables_edicion())
            {

                    f.txtdesc.Tag = Entidad.Id_Libro;
                    f.txtdesc.Text  =  Entidad.Nombre_Libro;
                    f.txtsunat.Text = Entidad.Id_SUNAT ;
             f.chkconsolidar.Checked    = Entidad.Lib_CentralizacionMes;


                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Ctb_Libro Ent = new Entidad_Ctb_Libro();
                    Logica_Ctb_Libro Log = new Logica_Ctb_Libro();

                    Ent.Id_Libro = f.txtdesc.Tag.ToString();
                    Ent.Nombre_Libro = f.txtdesc.Text;
                    Ent.Id_SUNAT = f.txtsunat.Text;
                    Ent.Lib_CentralizacionMes = f.chkconsolidar.Checked;
                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }

        }


        Entidad_Ctb_Libro Entidad = new Entidad_Ctb_Libro();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                //Entidad = Lista.Contains(gridView1.GetFocusedDataSourceRowIndex());
  
                //ent = Lista[gridView1.GetFocusedDataSourceRowIndex].Id_Libro;

                //TxtNomTipoEntidad.Text = ent.nombretipo;
                //TxtCodigo.Text = ent.Tipo;
             
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //string EntiDelted = "El tipo de Entidad a eliminar tiene la siguientes caracteristicas:" + Constants.vbNewLine + "Código:" + TxtCodigo.Text + Constants.vbNewLine + "Tipo de Entidad: " + TxtNomTipoEntidad.Text + "";
            //if (Tools.ShowDeleted(EntiDelted))
            //{
                try
                {
                    Entidad_Ctb_Libro Ent = new Entidad_Ctb_Libro();
                    Logica_Ctb_Libro Log = new Logica_Ctb_Libro();

                //Ent.empresa = DatosActualConexion.CodigoEmpresa;
                //Ent.Tipo = TxtCodigo.Text.Trim;
                Ent.Id_Libro = Entidad.Id_Libro;

                    if (Log.Eliminar(Ent) == true)
                    {
                        //Tools.ShowSuccessful(1);
                       
                        Listar();
                        //BtnEliminar.Enabled = false;
                        //BtnModificar.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                //Tools.ShowError(ex.Message);
                throw new Exception(ex.Message);
            }
            //}

        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\LibrosContables" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
