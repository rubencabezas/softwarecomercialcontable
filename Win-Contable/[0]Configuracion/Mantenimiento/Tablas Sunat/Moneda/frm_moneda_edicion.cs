﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda
{
    public partial class frm_moneda_edicion : Contable.frm_fuente
    {
        public frm_moneda_edicion()
        {
            InitializeComponent();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VerificarCabecera())
            {
                DialogResult = DialogResult.OK;
            }
         
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtnombre.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtnombre.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtsunat.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un codigo sunat");
                txtsunat.Focus();
                return false;
            }


            return true;
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
