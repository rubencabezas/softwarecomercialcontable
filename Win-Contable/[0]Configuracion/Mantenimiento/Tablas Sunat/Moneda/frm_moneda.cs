﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Tablas_Sunat.Moneda
{
    public partial class frm_moneda : Contable.frm_fuente
    {
        public frm_moneda()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_moneda_edicion f = new frm_moneda_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Moneda Ent = new Entidad_Moneda();
                    Logica_Moneda Log = new Logica_Moneda();

                    Ent.Nombre_Moneda = f.txtnombre.Text;
                    Ent.Id_Sunat = f.txtsunat.Text;
                    Ent.Es_nacional = f.chmonedanac.Checked;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                               Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
        }


                       public List<Entidad_Moneda> Lista = new List<Entidad_Moneda>();
        public void Listar()
        {
            Entidad_Moneda Ent = new Entidad_Moneda();
            Logica_Moneda log = new Logica_Moneda();

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_moneda_edicion f = new frm_moneda_edicion())
            {

                f.txtnombre.Tag = Entidad.Id_Moneda;
                f.txtnombre.Text = Entidad.Nombre_Moneda;
                f.txtsunat.Text = Entidad.Id_Sunat;
                f.chmonedanac.Checked = Entidad.Es_nacional;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Moneda Ent = new Entidad_Moneda();
                    Logica_Moneda Log = new Logica_Moneda();

                    Ent.Id_Moneda = f.txtnombre.Tag.ToString();
                    Ent.Nombre_Moneda = f.txtnombre.Text;
                    Ent.Id_Sunat = f.txtsunat.Text;
                    Ent.Es_nacional = f.chmonedanac.Checked;
                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        Entidad_Moneda Entidad = new Entidad_Moneda();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
             if (Lista.Count > 0)
                    {
                        Estado = Estados.Ninguno;
                        Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                    }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

       
        }

        private void frm_moneda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "La moneda a eliminar es el siguiente :" + Entidad.Nombre_Moneda;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Moneda Ent = new Entidad_Moneda
                    {
                        Id_Moneda = Entidad.Id_Moneda
                    };

                    Logica_Moneda log = new Logica_Moneda();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }


        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Moneda" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }


    }
        