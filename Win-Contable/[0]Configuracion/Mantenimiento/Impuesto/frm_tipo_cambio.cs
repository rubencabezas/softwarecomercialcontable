﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    public partial class frm_tipo_cambio : Contable.frm_fuente
    {
        public frm_tipo_cambio()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_tipo_cambio_edicion f = new frm_tipo_cambio_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Tipo_Cambio Ent = new Entidad_Tipo_Cambio();
                    Logica_Tipo_Cambio Log = new Logica_Tipo_Cambio();

                    Ent.Tic_Fecha = Convert.ToDateTime( string.Format("{0:yyyy-MM-dd}",f.txtfecha.Text) ).Date; 
                    Ent.Tic_Id_Moneda = f.txtmonedacod.Text.Trim();
               
                    Ent.Tic_Compra_Vigente =Convert.ToDecimal(f.txtcompravigente.Text);
                 
                    Ent.Tic_Venta_Vigente =Convert.ToDecimal(f.txtventavigente.Text);
              
                    Ent.Tic_Venta_Publicacion =Convert.ToDecimal(f.txtventapublicacion.Text);

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        public List<Entidad_Tipo_Cambio> Lista = new List<Entidad_Tipo_Cambio>();
        public void Listar()
        {
            Entidad_Tipo_Cambio Ent = new Entidad_Tipo_Cambio();
            Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
            
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
              if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
          
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_tipo_cambio_edicion f = new frm_tipo_cambio_edicion())
            {


                f.txtfecha.Enabled = false;
                f.txtfecha.Text = Convert.ToDateTime(Entidad.Tic_Fecha.ToString()).ToShortDateString();
                f.txtmonedacod.Text = Entidad.Tic_Id_Moneda;
                f.txtmonedadesc.Text = Entidad.Moneda_des;

           
                f.txtcompravigente.Text =Entidad.Tic_Compra_Vigente.ToString();

               
                f.txtventavigente.Text = Entidad.Tic_Venta_Vigente.ToString();

             
                f.txtventapublicacion.Text = Entidad.Tic_Venta_Publicacion.ToString();


                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Tipo_Cambio Ent = new Entidad_Tipo_Cambio();
                    Logica_Tipo_Cambio Log = new Logica_Tipo_Cambio();


                    Ent.Tic_Fecha = Convert.ToDateTime(f.txtfecha.Text);
                    Ent.Tic_Id_Moneda = f.txtmonedacod.Text.Trim();

              
                    Ent.Tic_Compra_Vigente = Convert.ToDecimal(f.txtcompravigente.Text);

                
                    Ent.Tic_Venta_Vigente = Convert.ToDecimal(f.txtventavigente.Text);

      
                    Ent.Tic_Venta_Publicacion = Convert.ToDecimal(f.txtventapublicacion.Text);


                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        private void frm_tipo_cambio_Load(object sender, EventArgs e)
        {
            Listar();
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\TipoCambio" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
