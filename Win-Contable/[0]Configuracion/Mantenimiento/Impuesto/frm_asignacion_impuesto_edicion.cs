﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    public partial class frm_asignacion_impuesto_edicion : Form
    {
        public frm_asignacion_impuesto_edicion()
        {
            InitializeComponent();
        }

        private void txtimpuestocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtimpuestocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtimpuestocod.Text.Substring(txtimpuestocod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_impuesto_busqueda f = new _1_Busquedas_Generales.frm_tipo_impuesto_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Impuesto Entidad = new Entidad_Impuesto();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtimpuestocod.Text = Entidad.Imc_Codigo;
                                txtimpuestodesc.Text = Entidad.Imc_Descripcion;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtimpuestocod.Text) & string.IsNullOrEmpty(txtimpuestodesc.Text))
                    {
                        BuscarImpuesto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarImpuesto()
        {
            try
            {
                txtimpuestocod.Text = Accion.Formato(txtimpuestocod.Text, 2);
                Logica_Impuesto log = new Logica_Impuesto();

                List<Entidad_Impuesto> Generales = new List<Entidad_Impuesto>();
                Generales = log.Listar(new Entidad_Impuesto
                {
                    Imc_Codigo = txtimpuestocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Impuesto T in Generales)
                    {
                        if ((T.Imc_Codigo).ToString().Trim().ToUpper() == txtimpuestocod.Text.Trim().ToUpper())
                        {
                            txtimpuestocod.Text = (T.Imc_Codigo).ToString().Trim();
                            txtimpuestodesc.Text = T.Imc_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtimpuestocod.EnterMoveNextControl = false;
                    txtimpuestodesc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtimpuestocod_TextChanged(object sender, EventArgs e)
        {
            if (txtimpuestocod.Focus() == false)
            {
                txtimpuestodesc.ResetText();
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VerificarCabecera())
            {
                DialogResult = DialogResult.OK;
            }
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtimpuestodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de impuesto");
                txtimpuestodesc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfechainicio.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha de inicio");
                txtfechainicio.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttasa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una tasa de impuesto");
                txttasa.Focus();
                return false;
            }


            return true;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
