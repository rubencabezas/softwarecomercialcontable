﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    public partial class frm_tipo_impuesto : frm_fuente
    {
        public frm_tipo_impuesto()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_tipo_impuesto_edicion f = new frm_tipo_impuesto_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Impuesto Ent = new Entidad_Impuesto();
                    Logica_Impuesto Log = new Logica_Impuesto();

                    Ent.Imc_Descripcion = f.txtimpuestodesc.Text.Trim();
                    Ent.Imc_Abreviado = f.txtabreviado.Text.Trim();
                    Ent.Imc_Es_Vigente = true;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
        }


        public List<Entidad_Impuesto> Lista = new List<Entidad_Impuesto>();
        public void Listar()
        {
            Entidad_Impuesto Ent = new Entidad_Impuesto();
            Logica_Impuesto log = new Logica_Impuesto();

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_tipo_impuesto_edicion f = new frm_tipo_impuesto_edicion())
            {

                f.txtimpuestodesc.Tag = Entidad.Imc_Codigo;
                f.txtimpuestodesc.Text = Entidad.Imc_Descripcion.Trim();
                f.txtabreviado.Text = Entidad.Imc_Abreviado.Trim();
             

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Impuesto Ent = new Entidad_Impuesto();
                    Logica_Impuesto Log = new Logica_Impuesto();

                    Ent.Imc_Codigo = f.txtimpuestodesc.Tag.ToString();
                    Ent.Imc_Descripcion = f.txtimpuestodesc.Text.Trim();
                    Ent.Imc_Abreviado = f.txtabreviado.Text.Trim();
                                  try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        Entidad_Impuesto Entidad = new Entidad_Impuesto();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_tipo_impuesto_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\TipoImpuesto" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
