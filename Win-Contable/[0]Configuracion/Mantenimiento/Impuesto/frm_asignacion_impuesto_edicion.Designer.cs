﻿namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    partial class frm_asignacion_impuesto_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.btncancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtfechafin = new DevExpress.XtraEditors.TextEdit();
            this.txtfechainicio = new DevExpress.XtraEditors.TextEdit();
            this.txtimpuestodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtimpresiontasa = new DevExpress.XtraEditors.TextEdit();
            this.txtimpresiondesc = new DevExpress.XtraEditors.TextEdit();
            this.txttasa = new DevExpress.XtraEditors.TextEdit();
            this.txtimpuestocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechafin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechainicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpuestodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpresiontasa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpresiondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttasa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpuestocod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.btncancelar});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btncancelar)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // btncancelar
            // 
            this.btncancelar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btncancelar.Caption = "Cancelar";
            this.btncancelar.Id = 1;
            this.btncancelar.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(468, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 161);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(468, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 129);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(468, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 129);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtfechafin);
            this.groupControl1.Controls.Add(this.txtfechainicio);
            this.groupControl1.Controls.Add(this.txtimpuestodesc);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtimpresiontasa);
            this.groupControl1.Controls.Add(this.txtimpresiondesc);
            this.groupControl1.Controls.Add(this.txttasa);
            this.groupControl1.Controls.Add(this.txtimpuestocod);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(2, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(464, 122);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos generales";
            // 
            // txtfechafin
            // 
            this.txtfechafin.Location = new System.Drawing.Point(238, 51);
            this.txtfechafin.MenuManager = this.barManager1;
            this.txtfechafin.Name = "txtfechafin";
            this.txtfechafin.Properties.Mask.EditMask = "d";
            this.txtfechafin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechafin.Size = new System.Drawing.Size(80, 20);
            this.txtfechafin.TabIndex = 6;
            // 
            // txtfechainicio
            // 
            this.txtfechainicio.EnterMoveNextControl = true;
            this.txtfechainicio.Location = new System.Drawing.Point(96, 51);
            this.txtfechainicio.MenuManager = this.barManager1;
            this.txtfechainicio.Name = "txtfechainicio";
            this.txtfechainicio.Properties.Mask.EditMask = "d";
            this.txtfechainicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechainicio.Size = new System.Drawing.Size(70, 20);
            this.txtfechainicio.TabIndex = 4;
            // 
            // txtimpuestodesc
            // 
            this.txtimpuestodesc.Enabled = false;
            this.txtimpuestodesc.EnterMoveNextControl = true;
            this.txtimpuestodesc.Location = new System.Drawing.Point(167, 29);
            this.txtimpuestodesc.Name = "txtimpuestodesc";
            this.txtimpuestodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtimpuestodesc.Size = new System.Drawing.Size(289, 20);
            this.txtimpuestodesc.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(47, 98);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(90, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Tasa de impresion:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(16, 76);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(121, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Descripcion de impresion:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(339, 54);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(27, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Tasa:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(169, 54);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(63, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Fecha de fin:";
            // 
            // txtimpresiontasa
            // 
            this.txtimpresiontasa.EnterMoveNextControl = true;
            this.txtimpresiontasa.Location = new System.Drawing.Point(139, 95);
            this.txtimpresiontasa.Name = "txtimpresiontasa";
            this.txtimpresiontasa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtimpresiontasa.Size = new System.Drawing.Size(194, 20);
            this.txtimpresiontasa.TabIndex = 12;
            // 
            // txtimpresiondesc
            // 
            this.txtimpresiondesc.EnterMoveNextControl = true;
            this.txtimpresiondesc.Location = new System.Drawing.Point(139, 73);
            this.txtimpresiondesc.Name = "txtimpresiondesc";
            this.txtimpresiondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtimpresiondesc.Size = new System.Drawing.Size(317, 20);
            this.txtimpresiondesc.TabIndex = 10;
            // 
            // txttasa
            // 
            this.txttasa.EnterMoveNextControl = true;
            this.txttasa.Location = new System.Drawing.Point(386, 51);
            this.txttasa.Name = "txttasa";
            this.txttasa.Properties.Mask.EditMask = "n2";
            this.txttasa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txttasa.Size = new System.Drawing.Size(70, 20);
            this.txttasa.TabIndex = 8;
            // 
            // txtimpuestocod
            // 
            this.txtimpuestocod.EnterMoveNextControl = true;
            this.txtimpuestocod.Location = new System.Drawing.Point(96, 29);
            this.txtimpuestocod.MenuManager = this.barManager1;
            this.txtimpuestocod.Name = "txtimpuestocod";
            this.txtimpuestocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtimpuestocod.Size = new System.Drawing.Size(70, 20);
            this.txtimpuestocod.TabIndex = 1;
            this.txtimpuestocod.TextChanged += new System.EventHandler(this.txtimpuestocod_TextChanged);
            this.txtimpuestocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtimpuestocod_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(16, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(74, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Fecha de inicio:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(85, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo de impuesto:";
            // 
            // frm_asignacion_impuesto_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 161);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_asignacion_impuesto_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asignacion de impuesto";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechafin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechainicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpuestodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpresiontasa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpresiondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttasa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpuestocod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem btncancelar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.TextEdit txtfechafin;
        public DevExpress.XtraEditors.TextEdit txtfechainicio;
        public DevExpress.XtraEditors.TextEdit txtimpuestodesc;
        public DevExpress.XtraEditors.TextEdit txtimpresiontasa;
        public DevExpress.XtraEditors.TextEdit txtimpresiondesc;
        public DevExpress.XtraEditors.TextEdit txttasa;
        public DevExpress.XtraEditors.TextEdit txtimpuestocod;
    }
}