﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    public partial class frm_tipo_cambio_edicion : Contable.frm_fuente
    {
        public frm_tipo_cambio_edicion()
        {
            InitializeComponent();
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmonedacod.Text = Entidad.Id_Moneda;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }

   

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 3);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Moneda = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Moneda).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            txtmonedacod.Text = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public bool VerificarCabecera()
        {

            if (string.IsNullOrEmpty(txtfecha.Text.Trim()))
            {
                Accion.Advertencia("Debe una fecha");
                txtfecha.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcompravigente.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un valor para la compra vigente");
                txtcompravigente.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtventavigente.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un valor para la venta vigente");
                txtventavigente.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtventapublicacion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un valor para la venta de publicacion");
                txtventavigente.Focus();
                return false;
            }


            return true;
        }

        private void txttipocod01_EditValueChanged(object sender, EventArgs e)
        {

        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }




    }
}
