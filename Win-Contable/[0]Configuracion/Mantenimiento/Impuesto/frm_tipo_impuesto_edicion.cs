﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    public partial class frm_tipo_impuesto_edicion : Form
    {
        public frm_tipo_impuesto_edicion()
        {
            InitializeComponent();
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (VerificarCabecera())
            {
                DialogResult = DialogResult.OK;
            }
            
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtimpuestodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtimpuestodesc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtabreviado.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un texto abreviado");
                txtabreviado.Focus();
                return false;
            }


            return true;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
