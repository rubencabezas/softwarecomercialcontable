﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    public partial class frm_asignacion_impuesto : frm_fuente
    {
        public frm_asignacion_impuesto()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_asignacion_impuesto_edicion f = new frm_asignacion_impuesto_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Impuesto_Det Ent = new Entidad_Impuesto_Det();
                    Logica_Impuesto_Det Log = new Logica_Impuesto_Det();

                    Ent.Imd_Codigo = f.txtimpuestocod.Text.Trim();
                    Ent.Imd_Fecha_Inicio = Convert.ToDateTime(f.txtfechainicio.Text);
                   
                    if (Ent.Imd_Fecha_Fin == Convert.ToDateTime("01/01/0001").Date)
                    {
                        Ent.Imd_Fecha_Fin = Convert.ToDateTime("01/01/0001").Date;
                    }
                    else
                    {
                        Ent.Imd_Fecha_Fin = Convert.ToDateTime(f.txtfechafin.Text);
                    }
                    Ent.Imd_Tasa = Convert.ToDecimal(f.txttasa.Text);
                    Ent.Imd_Descripcion_Impresion = f.txtimpresiondesc.Text.Trim();
                    Ent.Imd_Tasa_Impresion = f.txtimpresiontasa.Text.Trim();

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
        }

        private void dgvdatos_Click(object sender, EventArgs e)
        {

        }


        public List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
        public void Listar()
        {
            Entidad_Impuesto_Det Ent = new Entidad_Impuesto_Det();
            Logica_Impuesto_Det log = new Logica_Impuesto_Det();

            try
            {
                Ent.Imd_Fecha_Inicio = Convert.ToDateTime("01/01/0001").Date;

                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_asignacion_impuesto_edicion f = new frm_asignacion_impuesto_edicion())
            {

  

              f.txtimpuestocod.Text   = Entidad.Imd_Codigo;
                f.txtimpuestodesc.Text = Entidad.Imc_Descripcion;
              f.txtfechainicio.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Imd_Fecha_Inicio); 
              f.txtfechafin.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Imd_Fecha_Fin);
              f.txttasa.Text  = Convert.ToString(Entidad.Imd_Tasa) ;
              f.txtimpresiondesc.Text  = Entidad.Imd_Descripcion_Impresion;
               f.txtimpresiontasa.Text  = Entidad.Imd_Tasa_Impresion;


                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Impuesto_Det Ent = new Entidad_Impuesto_Det();
                    Logica_Impuesto_Det Log = new Logica_Impuesto_Det();


                    Ent.Imd_Codigo = f.txtimpuestocod.Text.Trim();
                    Ent.Imc_Descripcion = f.txtimpuestodesc.Text.Trim();
                    Ent.Imd_Fecha_Inicio = Convert.ToDateTime(f.txtfechainicio.Text);
                    Ent.Imd_Fecha_Fin = Convert.ToDateTime(f.txtfechafin.Text);
                    Ent.Imd_Tasa = Convert.ToDecimal(f.txttasa.Text);
                    Ent.Imd_Descripcion_Impresion = f.txtimpresiondesc.Text.Trim();
                    Ent.Imd_Tasa_Impresion = f.txtimpresiontasa.Text.Trim();


                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        Entidad_Impuesto_Det Entidad = new Entidad_Impuesto_Det();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_asignacion_impuesto_Load(object sender, EventArgs e)
        {
            Listar();
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Impuesto" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
