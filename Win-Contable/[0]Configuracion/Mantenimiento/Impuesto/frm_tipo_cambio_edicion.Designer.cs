﻿namespace Contable._0_Configuracion.Mantenimiento.Impuesto
{
    partial class frm_tipo_cambio_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtventapublicacion = new DevExpress.XtraEditors.TextEdit();
            this.txtventavigente = new DevExpress.XtraEditors.TextEdit();
            this.txtcompravigente = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtfecha = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtventapublicacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtventavigente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcompravigente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfecha.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(467, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 153);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(467, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 121);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(467, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 121);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txtventapublicacion);
            this.groupControl1.Controls.Add(this.txtventavigente);
            this.groupControl1.Controls.Add(this.txtcompravigente);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.txtfecha);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(5, 43);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(457, 108);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos generales";
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(242, 37);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(203, 20);
            this.txtmonedadesc.TabIndex = 4;
            // 
            // txtventapublicacion
            // 
            this.txtventapublicacion.EnterMoveNextControl = true;
            this.txtventapublicacion.Location = new System.Drawing.Point(102, 82);
            this.txtventapublicacion.Name = "txtventapublicacion";
            this.txtventapublicacion.Properties.Mask.EditMask = "f3";
            this.txtventapublicacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtventapublicacion.Size = new System.Drawing.Size(110, 20);
            this.txtventapublicacion.TabIndex = 19;
            // 
            // txtventavigente
            // 
            this.txtventavigente.EnterMoveNextControl = true;
            this.txtventavigente.Location = new System.Drawing.Point(296, 59);
            this.txtventavigente.Name = "txtventavigente";
            this.txtventavigente.Properties.Mask.EditMask = "f3";
            this.txtventavigente.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtventavigente.Size = new System.Drawing.Size(110, 20);
            this.txtventavigente.TabIndex = 14;
            // 
            // txtcompravigente
            // 
            this.txtcompravigente.EnterMoveNextControl = true;
            this.txtcompravigente.Location = new System.Drawing.Point(102, 59);
            this.txtcompravigente.Name = "txtcompravigente";
            this.txtcompravigente.Properties.Mask.EditMask = "f3";
            this.txtcompravigente.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtcompravigente.Size = new System.Drawing.Size(110, 20);
            this.txtcompravigente.TabIndex = 9;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(202, 37);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Size = new System.Drawing.Size(38, 20);
            this.txtmonedacod.TabIndex = 3;
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // txtfecha
            // 
            this.txtfecha.Location = new System.Drawing.Point(65, 37);
            this.txtfecha.MenuManager = this.barManager1;
            this.txtfecha.Name = "txtfecha";
            this.txtfecha.Properties.Mask.EditMask = "d";
            this.txtfecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfecha.Size = new System.Drawing.Size(86, 20);
            this.txtfecha.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(9, 83);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(87, 13);
            this.labelControl5.TabIndex = 18;
            this.labelControl5.Text = "Venta publicacion:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(219, 63);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(71, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Venta vigente:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 62);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(80, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Compra vigente:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(154, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(42, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Moneda:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(33, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Fecha:";
            // 
            // frm_tipo_cambio_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(467, 153);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_tipo_cambio_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tipo de cambio";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtventapublicacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtventavigente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcompravigente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfecha.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        public DevExpress.XtraEditors.TextEdit txtmonedadesc;
        public DevExpress.XtraEditors.TextEdit txtventapublicacion;
        public DevExpress.XtraEditors.TextEdit txtventavigente;
        public DevExpress.XtraEditors.TextEdit txtcompravigente;
        public DevExpress.XtraEditors.TextEdit txtmonedacod;
        public DevExpress.XtraEditors.TextEdit txtfecha;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}
