﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion.Entidades
{
    public partial class frm_entidad : Contable.frm_fuente
    {
        public frm_entidad()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //using (frm_entidad_edicion f = new frm_entidad_edicion())
            //{


            //    Estado = Estados.Nuevo;

            //    if (f.ShowDialog() == DialogResult.OK)
            //    {

            //        Entidad_Entidad Ent = new Entidad_Entidad();
            //        Logica_Entidad Log = new Logica_Entidad();

            //        Ent.Ent_Tipo_Persona = f.txttipopersonacod.Tag.ToString();
            //        Ent.Ent_Tipo_Doc = f.txttipodoccod.Tag.ToString();
            //        Ent.Ent_RUC_DNI = f.txtnumero.Text;
            //        Ent.Ent_Razon_Social_Nombre = f.txtrazonsocial.Text;
            //        Ent.Ent_Ape_Paterno = f.txtappaterno.Text;
            //        Ent.Ent_Ape_Materno = f.txtapematerno.Text;
            //        Ent.Ent_Telefono = f.txttelefono.Text;
            //        Ent.Ent_Repre_Legal = f.txtrepresentantelegal.Text;
            //        Ent.Ent_Telefono_movil = f.txtmovil.Text;
            //        Ent.Ent_Domicilio_Fiscal = f.txtdomifiscal.Text;
            //        Ent.Ent_Correo = f.txtcorreo.Text;
            //        Ent.Ent_Pagina_Web = f.txtpaginaweb.Text;



            //        try
            //        {
            //            if (Estado == Estados.Nuevo)
            //            {

            //                if (Log.Insertar(Ent))
            //                {
            //                    Listar();

            //                }
            //                else
            //                {
            //                    MessageBox.Show("No se puedo insertar");
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception(ex.Message);
            //        }
            //    }
            //}

            using (frm_entidad_edicion f = new frm_entidad_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }



        public List<Entidad_Entidad> Lista = new List<Entidad_Entidad>();
        public void Listar()
        {
            Entidad_Entidad Ent = new Entidad_Entidad();
            Logica_Entidad log = new Logica_Entidad();

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        Entidad_Entidad Entidad = new Entidad_Entidad();
        private void gridView1_Click(object sender, EventArgs e)
        {

        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Estado = Estados.Modificar;

            //using (frm_entidad_edicion f = new frm_entidad_edicion())
            //{

            //        f.txttipopersonacod.Tag = Entidad.Ent_Tipo_Persona ;
            //        f.txttipopersonacod.Text = Entidad.Ent_Tipo_Persona_Cod_Interno;
            //        f.txttipopersonadesc.Text = Entidad.Ent_Tipo_Persona_Descripcion;



            //        f.txttipodoccod.Tag  =Entidad.Ent_Tipo_Doc;
            //        f.txttipodoccod.Text = Entidad.Ent_Tipo_Doc_Cod_Interno;
            //        f.txttipodocdesc.Text = Entidad.Ent_Tipo_Doc_Descripcion;


            //        f.txtnumero.Text   =  Entidad.Ent_RUC_DNI;
            //        f.txtrazonsocial.Text  =  Entidad.Ent_Razon_Social_Nombre;
            //        f.txtappaterno.Text  =  Entidad.Ent_Ape_Paterno ;
            //        f.txtapematerno.Text  =   Entidad.Ent_Ape_Materno;
            //        f.txttelefono.Text   = Entidad.Ent_Telefono ;
            //        f.txtrepresentantelegal.Text  =  Entidad.Ent_Repre_Legal;
            //        f.txtmovil.Text   =Entidad.Ent_Telefono_movil ;
            //        f.txtdomifiscal.Text = Entidad.Ent_Domicilio_Fiscal  ;
            //        f.txtcorreo.Text  = Entidad.Ent_Correo;
            //        f.txtpaginaweb.Text  =  Entidad.Ent_Pagina_Web;


            //    if (f.ShowDialog() == DialogResult.OK)
            //    {

            //        Entidad_Entidad Ent = new Entidad_Entidad();
            //        Logica_Entidad Log = new Logica_Entidad();



            //        Ent.Ent_Tipo_Persona = f.txttipopersonacod.Tag.ToString();
            //        Ent.Ent_Tipo_Doc = f.txttipodoccod.Tag.ToString();
            //        Ent.Ent_RUC_DNI = f.txtnumero.Text;
            //        Ent.Ent_Razon_Social_Nombre = f.txtrazonsocial.Text;
            //        Ent.Ent_Ape_Paterno = f.txtappaterno.Text;
            //        Ent.Ent_Ape_Materno = f.txtapematerno.Text;
            //        Ent.Ent_Telefono = f.txttelefono.Text;
            //        Ent.Ent_Repre_Legal = f.txtrepresentantelegal.Text;
            //        Ent.Ent_Telefono_movil = f.txtmovil.Text;
            //        Ent.Ent_Domicilio_Fiscal = f.txtdomifiscal.Text;
            //        Ent.Ent_Correo = f.txtcorreo.Text;
            //        Ent.Ent_Pagina_Web = f.txtpaginaweb.Text;

            //        try
            //        {
            //            if (Estado == Estados.Modificar)
            //            {

            //                if (Log.Modificar(Ent))
            //                {
            //                    Listar();
            //                }
            //                else
            //                {
            //                    MessageBox.Show("No se puedo insertar");
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception(ex.Message);
            //        }



            //    }
            //}

            using (frm_entidad_edicion f = new frm_entidad_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                //f.Id_Empresa = Entidad.Id_Empresa;
                //f.Id_Anio = Entidad.Id_Anio;
                //f.Id_Periodo = Entidad.Id_Periodo;
                //f.Id_Libro = Entidad.Id_Libro;
                //f.Voucher = Entidad.Id_Voucher;
                f.RUC_DNI = Entidad.Ent_RUC_DNI;
                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }


        }

        private void frm_entidad_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                    if (gridView1.GetFocusedDataSourceRowIndex() > -1)
                        {
                                    if (Lista.Count > 0)
                                    {
                                        Estado = Estados.Ninguno;
                                        Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                                    }
                        }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Ent_RUC_DNI + " " + Entidad.Ent_Razon_Social_Nombre + " " + Entidad.Ent_Ape_Paterno + " " + Entidad.Ent_Ape_Materno;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Entidad Ent = new Entidad_Entidad
                    {
                        Ent_RUC_DNI = Entidad.Ent_RUC_DNI
                    };

                    Logica_Entidad log = new Logica_Entidad();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Ruta;
            Ruta = path + ("\\Entidad" + ".Xlsx");

            gridView1.ExportToXlsx(Ruta);
            System.Diagnostics.Process.Start(Ruta);
        }
    }
}
