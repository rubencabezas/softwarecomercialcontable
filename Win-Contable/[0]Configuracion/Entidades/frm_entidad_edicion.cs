﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Contable._0_Configuracion.Entidades
{
    public partial class frm_entidad_edicion : Contable.frm_fuente
    {
        public frm_entidad_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;
        public string RUC_DNI;
        private void textEdit10_EditValueChanged(object sender, EventArgs e)
        {

        }

        void LimpiarCab()
        {
            txtnumero.ResetText();
            txttipopersonacod.ResetText();
            txttipopersonadesc.ResetText();
            txttipodoccod.ResetText();
            txttipodocdesc.ResetText();
            txtrazonsocial.ResetText();
            txtappaterno.ResetText();
            txtapematerno.ResetText();
            txttelefono.ResetText();
            txtrepresentantelegal.ResetText();
            txtmovil.ResetText();
            txtdomifiscal.ResetText();
            txtcorreo.ResetText();
            txtpaginaweb.ResetText();

        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
           
                txtnumero.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }



        private void txttipopersonacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipopersonacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipopersonacod.Text.Substring(txttipopersonacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0004";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipopersonacod.Tag = Entidad.Id_General_Det;
                                txttipopersonacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipopersonadesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipopersonacod.Text) & string.IsNullOrEmpty(txttipopersonadesc.Text))
                    {
                        BuscarTipoPersona();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }

        public void BuscarTipoPersona()
        {
            try
            {
                txttipopersonacod.Text = Accion.Formato(txttipopersonacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0004",
                    Gen_Codigo_Interno=txttipopersonacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipopersonacod.Text.Trim().ToUpper())
                        {
                            txttipopersonacod.Tag = (T.Id_General_Det).ToString().Trim();
                            txttipopersonacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipopersonadesc.Text = T.Gen_Descripcion_Det;
                            txttipopersonacod.EnterMoveNextControl = true;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipopersonacod.EnterMoveNextControl = false;
                    txttipopersonacod.ResetText();
                    txttipopersonacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodoccod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoccod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoccod.Text.Substring(txttipodoccod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0005";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoccod.Tag = Entidad.Id_General_Det;
                                txttipodoccod.Text = Entidad.Gen_Codigo_Interno;
                                txttipodocdesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoccod.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDoc();
                    }
                }
                catch (Exception ex)
                {
                    //Tools.ShowError(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }



        public void BuscarTipoDoc()
        {
            try
            {
                //txttipocargo.Text = Accion.Formato(txttipocargo.Text, 3);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0005",
                    Gen_Codigo_Interno= txttipodoccod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipodoccod.Text.Trim().ToUpper())
                        {
                            txttipodoccod.Tag = (T.Id_General_Det).ToString().Trim();
                            txttipodoccod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipodocdesc.Text = T.Gen_Descripcion_Det;
                            txttipodoccod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoccod.EnterMoveNextControl = false;
                    txttipodoccod.ResetText();
                    txttipodoccod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);//Tools.ShowError(ex.Message);
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {


                            
                        Entidad_Entidad Ent = new Entidad_Entidad();
                        Logica_Entidad Log = new Logica_Entidad();

                        Ent.Ent_Tipo_Persona = txttipopersonacod.Tag.ToString();
                        Ent.Ent_Tipo_Doc = txttipodoccod.Tag.ToString();
                        Ent.Ent_RUC_DNI = txtnumero.Text;
                        Ent.Ent_Razon_Social_Nombre = txtrazonsocial.Text;
                        Ent.Ent_Ape_Paterno = txtappaterno.Text;
                        Ent.Ent_Ape_Materno = txtapematerno.Text;
                        Ent.Ent_Telefono = txttelefono.Text;
                        Ent.Ent_Repre_Legal = txtrepresentantelegal.Text;
                        Ent.Ent_Telefono_movil = txtmovil.Text;
                        Ent.Ent_Domicilio_Fiscal = txtdomifiscal.Text;
                        Ent.Ent_Correo = txtcorreo.Text;
                        Ent.Ent_Pagina_Web = txtpaginaweb.Text;



                        try
                        {
                            if (Estado == Estados.Nuevo)
                            {

                                if (Log.Insertar(Ent))
                                {
                                Accion.ExitoGuardar();
                                Estado = Estados.Nuevo;
                                LimpiarCab();
                                txtnumero.Focus();
                                this.Close();

                            }
                               
                            } else if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                this.Close();
                            }
                        }
                        }
                        catch (Exception ex)
                        {
                        Accion.ErrorSistema(ex.Message);
                    }
                  

            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        
          
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtnumero.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero para el documento");
                txtnumero.Focus();
                return false;
            }

        if (string.IsNullOrEmpty(txttipopersonadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de persona");
                txttipopersonacod.Focus();
                return false;
            }
        if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoccod.Focus();
                return false;
            }


            return true;
        }



        Retornar_Sunat BuscarRuc = new Retornar_Sunat();
        Retornar_Reniec BusquedaSunat = new Retornar_Reniec();
        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        private void txtnumero_KeyDown(object sender, KeyEventArgs e)

        {
            try
            {
                if (e.KeyCode == Keys.Enter & txtnumero.Text.Trim().Length == 11)
                {
                    if (ValidarRUC.validarRuc(txtnumero.Text.Trim()) == true)
                    {
                        if (Verificacion_Internet.InternetConnection())
                        {
                            Entidad_Entidad Entidad = new Entidad_Entidad();
                            Entidad = BuscarRuc.Retornar_Sunat(txtnumero.Text.Trim());


                            if (Entidad.Estado_No_Existe)
                            {

                            }
                            if (Entidad.EstadoContr != "ACTIVO")
                            {
                                Accion.Advertencia("El estado de esta endidad es" + " " + "NO ACTIVO");
                            }

                            if (Entidad.Estado_De_Consulta)
                            {
                                txtrazonsocial.Text = Entidad.Ent_Razon_Social_Nombre;
                                txtappaterno.Text = IIf(Entidad.Ent_Ape_Paterno == null, "", Entidad.Ent_Ape_Paterno).ToString();
                                txtapematerno.Text = IIf(Entidad.Ent_Ape_Materno == null, "", Entidad.Ent_Ape_Materno).ToString();
                                txtdomifiscal.Text = Entidad.Ent_Domicilio_Fiscal;
                                txttelefono.Text = Entidad.Ent_Telefono;

                              
                            }
                                 txttipopersonacod.Focus();
                        }
                    }
                    else
                    {
                        Accion.Advertencia("EL NUMERO DE RUC ES INCORRECTO");
                        LimpiarCab();
                        txtnumero.Focus();
                    }


                }
                else if (e.KeyCode == Keys.Enter & txtnumero.Text.Trim().Length == 8)
                {
                    if (Verificacion_Internet.InternetConnection())
                    {
                        Entidad_Entidad Entidad = new Entidad_Entidad();
                        Entidad = BusquedaSunat.Retornar_Reniec(txtnumero.Text.Trim());


                        if (Entidad.Estado_No_Existe)
                        {

                        }

                        if (Entidad.Estado_De_Consulta)
                        {
                            txtrazonsocial.Text = Entidad.Ent_Razon_Social_Nombre;
                            txtappaterno.Text = IIf(Entidad.Ent_Ape_Paterno == null, "", Entidad.Ent_Ape_Paterno).ToString();
                            txtapematerno.Text = IIf(Entidad.Ent_Ape_Materno == null, "", Entidad.Ent_Ape_Materno).ToString();
                            txtdomifiscal.Text = Entidad.Ent_Domicilio_Fiscal;
                            txttelefono.Text = Entidad.Ent_Telefono;

                            txttipopersonacod.Focus();
                        }

                        txttipopersonacod.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtnumero_TextChanged(object sender, EventArgs e)
        {
            //if (txtnumero.Focus() == false)
            //{
            //    txttipopersonacod.ResetText();
            //    txttipopersonadesc.ResetText();
            //    txttipodoccod.ResetText();
            //    txttipodocdesc.ResetText();
            //    txtrazonsocial.ResetText();
            //    txtappaterno.ResetText();
            //    txtapematerno.ResetText();
            //    txttelefono.ResetText();
            //    txtrepresentantelegal.ResetText();
            //    txtmovil.ResetText();
            //    txtdomifiscal.ResetText();
            //    txtcorreo.ResetText();
            //    txtpaginaweb.ResetText();

            //}
        }

        private void frm_entidad_edicion_Load(object sender, EventArgs e)
        {
           
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;

                if (txtnumero.Text != "")
                {
                    ActualizarDatos();

                    //txtnumero.EnterMoveNextControl = true;
                    //txtnumero.Enabled = false;
                   
                }
                
                txttipopersonacod.Focus();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                txtnumero.Enabled = false;
                ListarModificar();
               
            }
        }


        public List<Entidad_Entidad> Lista_Modificar = new List<Entidad_Entidad>();

        public void ListarModificar()
        {
            Entidad_Entidad Ent = new Entidad_Entidad();
            Logica_Entidad log = new Logica_Entidad();

            Ent.Ent_RUC_DNI = RUC_DNI;
            try
            {
                Lista_Modificar = log.Listar(Ent);

                Entidad_Entidad Entidad = new Entidad_Entidad();
                Entidad = Lista_Modificar[0];


                txttipopersonacod.Tag = Entidad.Ent_Tipo_Persona;
                txttipopersonacod.Text = Entidad.Ent_Tipo_Persona_Cod_Interno;
                txttipopersonadesc.Text = Entidad.Ent_Tipo_Persona_Descripcion;

                txttipodoccod.Tag = Entidad.Ent_Tipo_Doc;
                txttipodoccod.Text = Entidad.Ent_Tipo_Doc_Cod_Interno;
                txttipodocdesc.Text = Entidad.Ent_Tipo_Doc_Descripcion;


                txtnumero.Text = Entidad.Ent_RUC_DNI.Trim();
                txtrazonsocial.Text = Entidad.Ent_Razon_Social_Nombre;
                txtappaterno.Text = Entidad.Ent_Ape_Paterno;
                txtapematerno.Text = Entidad.Ent_Ape_Materno;
                txttelefono.Text = Entidad.Ent_Telefono;
                txtrepresentantelegal.Text = Entidad.Ent_Repre_Legal;
                txtmovil.Text = Entidad.Ent_Telefono_movil;
                txtdomifiscal.Text = Entidad.Ent_Domicilio_Fiscal;
                txtcorreo.Text = Entidad.Ent_Correo;
                txtpaginaweb.Text = Entidad.Ent_Pagina_Web;

                ActualizarDatos();


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void ActualizarDatos()
        {
            try
            {
                if (txtnumero.Text.Trim().Length == 11)
                {
           
                         if (Verificacion_Internet.InternetConnection())
                                    {
                                        Entidad_Entidad Entidad = new Entidad_Entidad();
                                        Entidad = BuscarRuc.Retornar_Sunat(txtnumero.Text.Trim());


                                        if (Entidad.Estado_No_Existe)
                                        {

                                        }
                                        if (Entidad.EstadoContr != "ACTIVO")
                                        {
                                            Accion.Advertencia("El estado de esta endidad es" + " " + "NO ACTIVO");
                                        }

                                        if (Entidad.Estado_De_Consulta)
                                        {
                                            txtrazonsocial.Text = Entidad.Ent_Razon_Social_Nombre;
                                            txtappaterno.Text = IIf(Entidad.Ent_Ape_Paterno == null, "", Entidad.Ent_Ape_Paterno).ToString();
                                            txtapematerno.Text = IIf(Entidad.Ent_Ape_Materno == null, "", Entidad.Ent_Ape_Materno).ToString();
                                            txtdomifiscal.Text = Entidad.Ent_Domicilio_Fiscal;
                                            txttelefono.Text = Entidad.Ent_Telefono;

                                            txttipopersonacod.Focus();
                                        }
                                    }

                   
                }
                else if (txtnumero.Text.Trim().Length == 8)
                {
                    if (Verificacion_Internet.InternetConnection())
                    {
                        Entidad_Entidad Entidad = new Entidad_Entidad();
                        Entidad = BusquedaSunat.Retornar_Reniec(txtnumero.Text.Trim());


                        if (Entidad.Estado_No_Existe)
                        {

                        }

                        if (Entidad.Estado_De_Consulta)
                        {
                            txtrazonsocial.Text = Entidad.Ent_Razon_Social_Nombre;
                            txtappaterno.Text = IIf(Entidad.Ent_Ape_Paterno == null, "", Entidad.Ent_Ape_Paterno).ToString();
                            txtapematerno.Text = IIf(Entidad.Ent_Ape_Materno == null, "", Entidad.Ent_Ape_Materno).ToString();
                            txtdomifiscal.Text = Entidad.Ent_Domicilio_Fiscal;
                            txttelefono.Text = Entidad.Ent_Telefono;

                            txttipopersonacod.Focus();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtnumero_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txttipopersonacod_TextChanged(object sender, EventArgs e)
        {
            if (txttipopersonacod.Focus() == false)
            {
                txttipopersonadesc.ResetText();
    
            }
        }

        private void txttipodoccod_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoccod.Focus() == false)
            {
                txttipodocdesc.ResetText();

            }
        }
    }
}
