﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Contable._0_Configuracion
{
    public partial class frm_area : Contable.frm_fuente
    {
        public frm_area()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_area_edicion f = new frm_area_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Area Ent = new Entidad_Area();
                    Logica_Area Log = new Logica_Area();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Area = null;
                    Ent.Are_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            Estado = Estados.Modificar;

            using (frm_area_edicion f = new frm_area_edicion())
            {


                f.txtdescripcion.Tag = Entidad.Id_Area;
                f.txtdescripcion.Text = Entidad.Are_Descripcion;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Area Ent = new Entidad_Area();
                    Logica_Area Log = new Logica_Area();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

                    Ent.Id_Area = f.txtdescripcion.Tag.ToString();
                    Ent.Are_Descripcion = f.txtdescripcion.Text;

                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                MessageBox.Show("No se puedo insertar");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                }
            }
        }

        Entidad_Area Entidad = new Entidad_Area();
        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        public List<Entidad_Area> Lista = new List<Entidad_Area>();
        public void Listar()
        {
            Entidad_Area Ent = new Entidad_Area();
            Logica_Area log = new Logica_Area();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Area = null;


            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        private void frm_area_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);



        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
