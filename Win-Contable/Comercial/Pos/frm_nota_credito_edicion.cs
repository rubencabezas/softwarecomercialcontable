﻿using Comercial;
using Contable._1_Busquedas_Generales;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Pos
{
    public partial class frm_nota_credito_edicion : frm_fuente
    {
        public frm_nota_credito_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, ctb_pdv_codigo, Id_Movimiento;

        public bool Es_moneda_nac;
        public string NombreImpresora = "";

        decimal MBase1 = 0, MValor_Fac_Export = 0;
        decimal MExonerada = 0, MInafecta, MIgv1 = 0;
        decimal MIsc = 0, MOtrosTributos=0 , MImporteTotal=0;

        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();

        List<Entidad_Movimiento_Cab> Comprobantes = new List<Entidad_Movimiento_Cab>();

        public void LimpiarCab()
        {
            txtlibro.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();

            txtfechadoc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();

            txtidanalisis.ResetText();
            txtanalisisdesc.ResetText();



        }

        public void ResetearImportes()
        {
            txtbaseimponible.Text = ("0.00");
            txtvalorfactexport.Text = ("0.00");
            txtigv.Text = ("0.00");
            txtimporteisc.Text = ("0.00");
            txtotrostribuimporte.Text = ("0.00");
            txtexonerada.Text = ("0.00");
            txtinafecta.Text = ("0.00");
            txtimportetotal.Text = ("0.00");

        }

        public void BloquearDetalles2()
        {
            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtalmacencod.Enabled = false;
            txtcantidad.Enabled = false;
            txtvalorunit.Enabled = false;
            txtimporte.Enabled = false;
            txtunmcod.Enabled = false;
            txttipooperacion.Enabled = false;
        }

        public void HabilitarDetalles2()
        {
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtalmacencod.Enabled = true;
            txtcantidad.Enabled = true;
            txtvalorunit.Enabled = true;
            txtimporte.Enabled = true;
            txtunmcod.Enabled = true;
            txttipooperacion.Enabled = true;
        }

        public void LimpiarDet2()
        {

            txtimporte.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtalmacencod.ResetText();
            txtalmacendesc.ResetText();
            txtvalorunit.ResetText();
            txtcantidad.ResetText();
            txtunmcod.ResetText();
            txtunmdesc.ResetText();
            TxtAfectIGVCodigo.ResetText();
            TxtAfectIGVDescripcion.ResetText();
            chklote.Checked = false;

        }


        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();


                Detalles.Clear();
                Dgvdetalles.DataSource = null;

                //TraerLibro();
                BuscarMoneda_Inicial();
                ResetearImportes();

            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        public void BuscarMoneda_Inicial()
        {
            try
            {

                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = "1"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == "1")
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;

                        }

                    }
                    VerificarMoneda();
                    //txtglosa.Focus();
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(txtfechadoc.Text) == true))
                {
                    if ((DateTime.Parse(txtfechadoc.Text).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(txtfechadoc.Text);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            txtigvporcentaje.Tag = Lista[0].Imd_Tasa;
                            txtigvporcentaje.Text = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));
 
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_moneda_busqueda f = new _1_Busquedas_Generales.frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Tag = Entidad.Id_Moneda;

                                txtmonedacod.Text = Entidad.Id_Sunat;
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;
                                VerificarMoneda();
                                txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1.000";

                }
                else
                {
                    if (string.IsNullOrEmpty(txttipocambiocod.Text))
                    {
                        txttipocambiocod.Enabled = true;
                        txttipocambiocod.ResetText();
                        txttipocambiodesc.ResetText();
                        txttipocambiovalor.ResetText();
                    }

                }
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private void txttipocambiocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipocambiocod.Text) == false)
            {
                if (IsDate(txtfechadoc.Text))
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter & txttipocambiocod.Text.Substring(txttipocambiocod.Text.Length - 1, 1) == "*")
                        {
                            using (_1_Busquedas_Generales.frm_tipo_cambio_busqueda f = new _1_Busquedas_Generales.frm_tipo_cambio_busqueda())
                            {

                                f.FechaTransac = Convert.ToDateTime(txtfechadoc.Text);
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {
                                    Entidad_Tipo_Cambio Entidad = new Entidad_Tipo_Cambio();

                                    Entidad = f.ListaTipoCambio[f.gridView1.GetFocusedDataSourceRowIndex()];

                                    txttipocambiocod.Text = Entidad.Codigo;
                                    txttipocambiodesc.Text = Entidad.Nombre;
                                    txttipocambiovalor.Text = Convert.ToDouble(Entidad.Valor).ToString();
                                    if (txttipocambiocod.Text == "OTR")
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }
                                    else
                                    {
                                        txttipocambiovalor.Enabled = true;
                                    }
                                    txttipocambiocod.EnterMoveNextControl = true;

                                }
                            }
                        }
                        else
                            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipocambiocod.Text) & string.IsNullOrEmpty(txttipocambiodesc.Text))
                        {
                            try
                            {
                                Logica_Tipo_Cambio log = new Logica_Tipo_Cambio();
                                List<Entidad_Tipo_Cambio> TiposCambio = new List<Entidad_Tipo_Cambio>();
                                TiposCambio = log.Buscar_Tipo_Cambio(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Convert.ToDateTime(txtfechadoc.Text)
                                });
                                if (TiposCambio.Count > 0)
                                {
                                    foreach (Entidad_Tipo_Cambio T in TiposCambio)
                                    {
                                        //T = T_loopVariable;
                                        if ((T.Codigo).ToString().Trim().ToUpper() == txttipocambiocod.Text.Trim().ToUpper())
                                        {
                                            txttipocambiocod.Text = T.Codigo;
                                            txttipocambiodesc.Text = T.Nombre;
                                            txttipocambiovalor.Text = Convert.ToDouble(T.Valor).ToString();
                                            if (txttipocambiocod.Text == "OTR")
                                            {
                                                txttipocambiovalor.Enabled = true;
                                            }
                                            else
                                            {
                                                txttipocambiovalor.Enabled = true;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Accion.Advertencia("Debe ingresar primero una fecha válida para obtener un Tipo de Cambio con esa fecha");
                    txtfechadoc.Focus();

                }
            }
        }

        string Tipo_Doc_SUNAT_Cliente = "";
        string Correo_Electronico="";
        string Direccion_cliente = "";
        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                //Ent_TpDcEnt_SUNAT
                               Tipo_Doc_SUNAT_Cliente   = Entidad.Ent_Tipo_Doc_Cod_Interno;

                                txtrucdni.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            //Ent_TpDcEnt_SUNAT
                            Tipo_Doc_SUNAT_Cliente = T.Ent_Tipo_Doc_Cod_Interno;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {

                    using (_0_Configuracion.Entidades.frm_entidad_edicion f = new _0_Configuracion.Entidades.frm_entidad_edicion())
                    {

                        f.txtnumero.Text = txtrucdni.Text;

                        f.Estado_Ven_Boton = "1";
                        if (f.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                        else
                        {
                            Logica_Entidad logi = new Logica_Entidad();

                            List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                            Generalesi = logi.Listar(new Entidad_Entidad
                            {
                                Ent_RUC_DNI = txtrucdni.Text
                            });

                            if (Generalesi.Count > 0)
                            {

                                foreach (Entidad_Entidad T in Generalesi)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                        txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                        Tipo_Doc_SUNAT_Cliente = T.Ent_Tipo_Doc_Cod_Interno;
                                        txtrucdni.EnterMoveNextControl = true;
                                    }
                                }

                            }
                            else
                            {
                                txtrucdni.EnterMoveNextControl = false;
                                txtrucdni.ResetText();
                                txtrucdni.Focus();
                            }
                        }
                    }
                    //    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //txtrucdni.EnterMoveNextControl = false;
                    //txtrucdni.ResetText();
                    //txtrucdni.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcondicioncod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcondicioncod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcondicioncod.Text.Substring(txtcondicioncod.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_generales_busqueda f = new _1_Busquedas_Generales.frm_generales_busqueda())
                        {
                            f.Id_General = "0003";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcondicioncod.Tag = Entidad.Id_General_Det;
                                txtcondicioncod.Text = Entidad.Gen_Codigo_Interno;
                                txtcondiciondesc.Text = Entidad.Gen_Descripcion_Det;
                                txtcondicioncod.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcondicioncod.Text) & string.IsNullOrEmpty(txtcondiciondesc.Text))
                    {
                        BuscarCondicion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarCondicion()
        {
            try
            {
                txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = txtcondicioncod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtcondicioncod.Text.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;
                            txtcondicioncod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarCondicion_Load(string condicionCod)
        {
            try
            {
                //txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = condicionCod
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == condicionCod.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;
                            txtcondicioncod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void frm_nota_credito_edicion_Load(object sender, EventArgs e)
        {


            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                EstadoDetalle2 = Estados.Ninguno;

                LimpiarCab();
                Detalles.Clear();
                Dgvdetalles.DataSource = null;
                //TraerLibro();
                BuscarMoneda_Inicial();
                ResetearImportes();
                BloquearDetalles2();
                BuscarTipoDocumento_Load();//Carga solo el comprobante NOTA CREDITO
                BuscarCondicion_Load("01");
                txtpdvcod.Select();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
               // TraerLibro();
                BloquearDetalles2();
              ListarModificar();

            }
        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.ctb_pdv_codigo = ctb_pdv_codigo;
            Ent.Id_Movimiento = Id_Movimiento;
            try
            {
                Lista_Modificar = log.Listar_NC(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    ctb_pdv_codigo = Enti.ctb_pdv_codigo;
                    Id_Movimiento = Enti.Id_Movimiento;

                    txtpdvcod.Text = Enti.ctb_pdv_codigo.Trim();
                    txtpdvdesc.Text = Enti.ctb_pdv_Nombre.Trim();

                    txtmotivocod.Tag = Enti.Ven_Motivo_Cod.Trim();
                    txtmotivocod.Text = Enti.Ven_Motivo_Cod_Interno.Trim();
                    txtmotivodesc.Text = Enti.Ven_Motivo_Desc.Trim();


                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;



                    txtserie.Text = Enti.Ctb_Serie + "-" + Enti.Ctb_Numero;

                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));
                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtcondicioncod.Tag = Enti.Ctb_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ctb_Condicion_Interno;
                    txtcondiciondesc.Text = Enti.Ctb_Condicion_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;


                    MBase1 = Enti.Ctb_Base_Imponible;
                    MValor_Fac_Export = Enti.Ctb_Valor_Fact_Exonerada;
                    MIgv1 = Enti.Ctb_Igv;
                    MIsc = Enti.Ctb_Isc_Importe;
                    MOtrosTributos = Enti.Ctb_Otros_Tributos_Importe;
                    MExonerada = Enti.Ctb_Exonerada;
                    MInafecta = Enti.Ctb_Inafecta;
                    MImporteTotal = Enti.Ctb_Importe_Total;

                    //  { 0:00.00}         ", value)
                    txtbaseimponible.Text = Convert.ToDecimal(MBase1).ToString("0.00");
                    txtvalorfactexport.Text = Convert.ToDecimal(MValor_Fac_Export).ToString("0.00");
                    txtigv.Text = Convert.ToDecimal(MIgv1).ToString("0.00");
                    txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");
                    txtotrostribuimporte.Text = Convert.ToDecimal(MOtrosTributos).ToString("0.00");
                    txtexonerada.Text = Convert.ToDecimal(MExonerada).ToString("0.00");
                    txtinafecta.Text = Convert.ToDecimal(MInafecta).ToString("0.00");
                    txtimportetotal.Text = Convert.ToDecimal(MImporteTotal).ToString("0.00");



                    txtidanalisis.Text = Enti.Ctb_Analisis;
                    txtanalisisdesc.Text = Enti.Ctb_Analisis_Desc;

                    if (String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/0001" || String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/1900")
                    {
                        txtfechavencimiento.Text = "";
                    }
                    else
                    {
                        txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Vencimiento);
                    }


                    //listar cnotable
                    Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                    Dgvdetalles.DataSource = null;
                    Detalles = log_det_con.Listar_NC_Det(Enti);

                    if (Detalles.Count > 0)
                    {

                        Dgvdetalles.DataSource = Detalles;


                    }


                    //Listar_Doc_Afecto_NC_

                    dgvcomprobantes.DataSource = null;
                    Comprobantes = log_det_con.Listar_Doc_Afecto_NC_(Enti);

                    if (Comprobantes.Count > 0)
                    {

                        dgvcomprobantes.DataSource = Comprobantes;


                    }

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (_1_Busquedas_Generales.frm_tipo_documento_busqueda f = new _1_Busquedas_Generales.frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void BuscarTipoDocumento_Load()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = "07"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == "07")
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtfechadoc_Leave(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txtfechadoc_TextChanged(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txtpdvcod_EditValueChanged(object sender, EventArgs e)
        {

        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcondiciondesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una condicion");
                txtcondicioncod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

 

            return true;
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Id_Periodo;
                    Ent.ctb_pdv_codigo = txtpdvcod.Text.Trim();
                    Ent.Id_Movimiento = Id_Movimiento;

                    Ent.Ctb_Glosa = txtglosa.Text;

                    Ent.Nombre_Comprobante = txttipodocdesc.Text.Trim();
                    Ent.Ctb_Tipo_Doc = txttipodoc.Tag.ToString();
                    Ent.Ctb_Tipo_Doc_Sunat = txttipodoc.Text.Trim();

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Ven_Motivo_Cod= txtmotivocod.Tag.ToString();

                    Ent.Ctb_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Ctb_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);

                    Ent.Ctb_Tipo_Ent = "C";
                    Ent.Ctb_Ruc_dni = txtrucdni.Text;
                    Ent.Cliente_Razon_Social = txtentidad.Text.Trim();
                    Ent.Ent_TpDcEnt_SUNAT=Tipo_Doc_SUNAT_Cliente ;
                    Ent.Cliente_Direccion = Direccion_cliente;
                    Ent.Correo_Cliente = Correo_Electronico;



                    Ent.Ctb_Condicion_Cod = txtcondicioncod.Tag.ToString();
                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;

                    Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                    Ent.Ctb_Base_Imponible = Convert.ToDecimal(txtbaseimponible.Text);
                    Ent.Ctb_Valor_Fact_Exonerada = Convert.ToDecimal(txtvalorfactexport.Text);
                    Ent.Ctb_Exonerada = Convert.ToDecimal(txtexonerada.Text);
                    Ent.Ctb_Inafecta = Convert.ToDecimal(txtinafecta.Text);

                    Ent.Ctb_Isc = chkisc.Checked;
                    Ent.Ctb_Isc_Importe = Convert.ToDecimal(txtimporteisc.Text);

                    Ent.Ctb_Igv = Convert.ToDecimal(txtigv.Text);

                    Ent.Ctb_Otros_Tributos = chkotrostri.Checked;
                    Ent.Ctb_Otros_Tributos_Importe = Convert.ToDecimal(txtotrostribuimporte.Text);

                    Ent.Ctb_Importe_Total = Convert.ToDecimal(txtimportetotal.Text);


                    if (txtfechavencimiento.Text == "" || txtfechavencimiento.Text == "01/01/1900")
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime("01/01/1900");
                    }
                    else
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime(txtfechavencimiento.Text);
                    }
 

                    Ent.Ctb_Tasa_IGV = Convert.ToDecimal(txtigvporcentaje.Text);
                    Ent.Ctb_Analisis = txtidanalisis.Text.Trim();


                    //detalle Contable
                    //  Ent.DetalleAsiento = Detalles_CONT;
                    //Detalle Administrativo
                    Ent.DetalleADM = Detalles;
                    Ent.DetalleComprobantes = Comprobantes;


                    //0020    01  BASE IMPONIBLE 01
                    //0021    02  IGV 01
                    //0022    03  BASE IMPONIBLE 02
                    //0025    04  IGV 02
                    //0026    05  BASE IMPONIBLE 03
                    //0036    06  IGV 03
                    //0037    07  VALOR DE ADQUISICION NO GRABADA
                    //0038    08  IMPUESTO SELECTIVO AL CONSUMO
                    //0039    09  OTROS TRIBUTOS Y CARGOS
                    //0040    10  IMPORTE TOTAL

                    //Calcular(Detalles, Ent);

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar_adm_NC(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Impresion_Boleta_Factura_Electronica(Ent, false, Es_Electronico);

                            LimpiarCab();

                            LimpiarDet2();
                            Detalles.Clear();
                            Comprobantes.Clear();

                            dgvcomprobantes.DataSource = null;
                            Dgvdetalles.DataSource = null;

                            //BloquearDetalles2();
                            //TraerLibro();
                            //BuscarMoneda_Inicial();
                            ResetearImportes();
                            txtpdvcod.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar_adm_Ventas(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }



        string Nombre = "";

        void Impresion_Boleta_Factura_Electronica(Entidad_Movimiento_Cab Doc, bool Impresion_Automatica, bool Es_Electronico)
        {
            try
            {


                string Modelo_Impresion = "000";
                Nombre = Modelo_Impresion;

                // Warning!!! Optional parameters not supported
                DocumentoElectronico Doc_Electronico_Bol_Fact;
                Doc_Electronico_Bol_Fact = Convertir_Cliente_Objet(Doc);

                if (Es_Electronico)
                {



                    string ws_Rpta = Facte.Registrar_Para_Enviar(Doc_Electronico_Bol_Fact);
                    string[] datosRpta = ws_Rpta.Split(Convert.ToChar("#"));
                    string DataVar;
                    DataVar = datosRpta[0];
                    DataVar = datosRpta[1];
                    DataVar = datosRpta[2];
                    DataVar = datosRpta[3];
                    // Valores Codigo de barras PDF
                    // DataVar = datosRpta(4) 'Sunat Codigo '-1 problema conexion, 0 Aceptado, 'x' un error
                    // DataVar = datosRpta(5) 'Sunat Descripcion Aceptado/Rechazado en caso de los envios/ o error de conexion

                }
                else
                {
                    //Reportes_Impresiones.Rpt_Libro_Diario f = new Reportes_Impresiones.Rpt_Libro_Diario();

                    Print_Invoice_POS RptPOS = new Print_Invoice_POS();
                    List<DocumentoElectronico> listRpt = new List<DocumentoElectronico>();
                    listRpt.Add(Doc_Electronico_Bol_Fact);

                    if (listRpt[0].Cliente_Documento_Numero.Length != 11)
                    {
                        //RptPOS.XrLabel13.Text = "DNI:";
                        ////'Cuando es boleta ocultamos estos campos
                        //RptPOS.XrLabel19.Visible = false;
                        //RptPOS.XrLabel20.Visible = false;
                        //RptPOS.XrLabel5.Visible = false;
                        //RptPOS.XrLabel6.Visible = false;
                        //RptPOS.XrLabel23.Visible = false;
                        //RptPOS.XrLabel30.Visible = false;
                    }


                    RptPOS.lblplaca.Visible = false;
                    RptPOS.lblhash.Visible = false;
                    RptPOS.CodigoBarras.Visible = false;
                    RptPOS.AutorizacionSunat.Visible = false;
                    RptPOS.DetailPagoTarjeta.Visible = false;
                    RptPOS.DatosVendedor.Visible = false;

                    int HeightDoc = 1000;
                    HeightDoc = HeightDoc + (32 * Detalles.Count());
                    RptPOS.PageHeight = HeightDoc;


                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    RptPOS.DataSource = listRpt;

                    RptPOS.CreateDocument();
                    RptPOS.PrinterName = NombreImpresora;

                   // PdfExportOptions pdfOptions = RptPOS.ExportOptions.Pdf;

                  //  pdfOptions.DocumentOptions.Title = BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim();
                  //  string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "VENTAS AL CONTADO");
                   // string NombreArchivo = Carpeta + "\\" + BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim() + ".pdf";
                   // RptPOS.ExportToPdf(NombreArchivo, pdfOptions);

                    try
                    {
                        using (ReportPrintTool printTool = new ReportPrintTool(RptPOS))
                        {
                            printTool.Print(NombreImpresora);
                            //RptPOS.ExportToPdf(NombreArchivo, pdfOptions);
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


        DocumentoElectronico Convertir_Cliente_Objet(Entidad_Movimiento_Cab Documento_Cliente)
        {
            DocumentoElectronico Doc = new DocumentoElectronico();
            Doc.Impresion_Modelo_Codigo = Nombre;

            Doc.Fecha_Emision = Documento_Cliente.Ctb_Fecha_Movimiento;
            Doc.Emisor_ApellidosNombres_RazonSocial = Actual_Conexion.EmpresaNombre.Trim();
            Doc.Emisor_Documento_Tipo = "6";
            Doc.Emisor_Documento_Numero = Actual_Conexion.RucEmpresa.Trim();
            // ----------------------------
            // --- Domicilio fiscal -- PostalAddress
            Doc.Emisor_Direccion_Ubigeo = "020101";
            Doc.Emisor_Direccion_Calle = Actual_Conexion.Emp_DireccionCorta.Trim();//Actual_Conexion.Emp_DireccionCorta.Trim();
            Doc.Emisor_Direccion_Urbanizacion = Actual_Conexion.Emp_Urbanizacion.Trim();// Actual_Conexion.Emp_Urbanizacion.Trim();
            Doc.Emisor_Direccion_Departamento = Actual_Conexion.Emp_Direccion_Departamento.Trim();// Actual_Conexion.Emp_Direccion_Departamento.Trim();
            Doc.Emisor_Direccion_Provincia = Actual_Conexion.Emp_Direccion_Provincia.Trim();// Actual_Conexion.Emp_Direccion_Provincia.Trim();
            Doc.Emisor_Direccion_Distrito = Actual_Conexion.Emp_Direccion_Distrito.Trim();//Actual_Conexion.Emp_Direccion_Distrito.Trim();
            Doc.Emisor_Direccion_PaisCodigo = "PE";

            // Doc.Fecha_Vencimiento = DBNull.Value;

            // "01" ' Si es Boleta o Factura segun Sunat ==>
            Doc.TipoDocumento_Codigo = Documento_Cliente.Ctb_Tipo_Doc_Sunat.Trim();
            Doc.TipoDocumento_Serie = Documento_Cliente.Ctb_Serie.Trim();
            Doc.TipoDocumento_Numero = Documento_Cliente.Ctb_Numero.Trim();

            string Desc = "";
            if (Es_Electronico)
            {
                Desc = "ELECTRÓNICA";
            }

            Doc.TipoDocumento_Descripcion = (Documento_Cliente.Nombre_Comprobante.Trim() + Desc);

            // "6" 'Si es DNI o RUC segun sunat
            Doc.Cliente_Documento_Tipo = Documento_Cliente.Ent_TpDcEnt_SUNAT.Trim();
            Doc.Cliente_Documento_Numero = ((Documento_Cliente.Ctb_Ruc_dni.Trim() == "00000001") ? "00000000" : Documento_Cliente.Ctb_Ruc_dni.Trim());
            Doc.Cliente_RazonSocial_Nombre = Documento_Cliente.Cliente_Razon_Social.Trim();
            Doc.Cliente_Direccion = Documento_Cliente.Cliente_Direccion.Trim();
            Doc.Cliente_Correo = Documento_Cliente.Correo_Cliente.Trim();
            Doc.Numero_Placa_del_Vehiculo = "";// 'Documento_Cliente.Vnt_placa_Vehiculo;


            List<DocumentoElectronicoDetalle> feDetalles = new List<DocumentoElectronicoDetalle>();

            List<Entidad_Movimiento_Cab> DetalleVenta = new List<Entidad_Movimiento_Cab>();


            foreach (Entidad_Movimiento_Cab bf in Detalles)
            {
                DocumentoElectronicoDetalle det = new DocumentoElectronicoDetalle();

                det.Item = bf.Adm_Item;
                det.Producto_Codigo = bf.Adm_Catalogo.Trim();
                det.Producto_Descripcion = (bf.Adm_Catalogo_Desc.Trim() + (" x " + bf.Adm_Unm_Desc.Trim()));
                det.Producto_UnidadMedida_Codigo = bf.UNM_CoigodFE.Trim();
                det.Producto_Cantidad = Convert.ToDouble(bf.Adm_Cantidad);
                det.Unitario_Precio_Venta = Convert.ToDouble(bf.Adm_Valor_Unit);//DvtD_PrecUnitario
                det.Unitario_Precio_Venta_Tipo_Codigo = "01";
                det.Item_IGV_Total = bf.DvtD_IGVMonto;
                det.Unitario_IGV_Porcentaje = Convert.ToDouble(bf.DvtD_IGVTasaPorcentaje);
                det.Item_Tipo_Afectacion_IGV_Codigo = "10";
                det.Item_ISC_Total = 0;
                det.Unitario_Valor_Unitario = Convert.ToDouble(bf.Adm_Valor_Venta);//DvtD_ValorVenta
                det.Item_ValorVenta_Total = bf.DvtD_SubTotal;
                det.Item_Total = Convert.ToDouble(bf.Adm_Total);
                det.Numero_Placa_del_Vehiculo = "";//(((bf.UNM_CoigodFE == "GLL") || (bf.UNM_CoigodFE == "LTR")) ? Documento_Cliente.Vnt_placa_Vehiculo : null);
                feDetalles.Add(det);
            }



            List<DocumentoElectronicoReferenciaDocumentos> feDetallesDocRefencia = new List<DocumentoElectronicoReferenciaDocumentos>();

            foreach (Entidad_Movimiento_Cab bf in Comprobantes)
            {
                DocumentoElectronicoReferenciaDocumentos det = new DocumentoElectronicoReferenciaDocumentos();
                det.Documento_Tipo = bf.Ctb_Tipo_Doc_Sunat;
                det.Documento_Serie = bf.Ctb_Serie;
                det.Documento_Numero = bf.Ctb_Numero;
                det.Documento_Ref_Tipo = txtmotivocod.Text.Trim();
                det.Documento_Ref_Observaciones = txtmotivodesc.Text.Trim();
                feDetallesDocRefencia.Add(det);
            }


            Doc.Detalles = feDetalles.ToArray();
            Doc.DocumentoAfectados = feDetallesDocRefencia.ToArray();

            Doc.Total_ValorVenta_OperacionesGravadas = Convert.ToDouble(Documento_Cliente.Ctb_Base_Imponible); //Dvt_MntoBaseImponible
            Doc.Total_ValorVenta_OperacionesInafectas = Convert.ToDouble(Documento_Cliente.Ctb_Inafecta);//Dvt_MntoInafecta
            Doc.Total_ValorVenta_OperacionesExoneradas = Convert.ToDouble(Documento_Cliente.Ctb_Exonerada); //Dvt_MntoExonerado
            Doc.Total_ValorVenta_OperacionesGratuitas = 0;
            Doc.Total_IGV = Convert.ToDouble(Documento_Cliente.Ctb_Igv);// Dvt_MntoIGV
            Doc.IGV_Porcentaje = Convert.ToDouble(Documento_Cliente.Ctb_Tasa_IGV);// Dvt_PrcIGV
            Doc.Total_ISC = Convert.ToDouble(Documento_Cliente.Ctb_Isc_Importe);// Dvt_MntoISC
            Doc.Total_OtrosTributos = 0;
            Doc.Total_OtrosCargos = 0;
            // Doc.Descuentos_Globales = 0
            Doc.Total_Importe_Venta = Convert.ToDouble(Documento_Cliente.Ctb_Importe_Total);// Dvt_MntoTotal
            //MONEDAS
            Doc.Moneda_Codigo = "PEN";
            Doc.Moneda_Descripcion = "SOLES";
            Doc.Moneda_Simbolo = "S/";

            Doc.Total_Importe_Venta_Texto = Imprimir_Letras.NroEnLetras(Convert.ToDecimal(Doc.Total_Importe_Venta)) + " " + Doc.Moneda_Descripcion;


            Doc.Impresion_Modelo_Codigo = "000";

            //Venta percepcion
            Doc.Tipo_Operacion_Codigo = null;
            Doc.Total_Importe_Percepcion = 0;
            Doc.Percep_Importe_Total_Cobrado = 0;

            //    45 Leyendas
            List<DocumentoElectronicoLeyenda> ListaLeyendas = new List<DocumentoElectronicoLeyenda>();
            //ListaLeyendas.Add(new DocumentoElectronicoLeyenda() With {, ., Codigo = 1000, ., ((NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) + (" " + Doc.Moneda_Descripcion))));

            ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "1000", Descripcion = Imprimir_Letras.NroEnLetras(Convert.ToDecimal(Math.Truncate(Doc.Total_Importe_Venta))) + " " + Doc.Moneda_Descripcion });

            if (Doc.Total_ValorVenta_OperacionesGratuitas > 0)
            {
                ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "1002", Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y / O SERVICIO PRESTADO GRATUITAMENTE" });
            }

            if ((Doc.Total_Importe_Percepcion > 0))
            {
                ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "2000", Descripcion = "COMPROBANTE DE PERCEPCION" });
            }

            if (!string.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo))
            {
                if (Doc.Tipo_Operacion_Codigo == "05")
                {
                    ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "2005", Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE" });
                }

            }

            Doc.Leyendas = ListaLeyendas.ToArray();

            //List<DocumentoElectronicoPagoTarjeta> feDetallesPago = new List<DocumentoElectronicoPagoTarjeta>();
           // foreach (Entidad_Movimiento_Cab bf in PagosDoc)
           // {
           //     DocumentoElectronicoDetalle det = new DocumentoElectronicoDetalle();
           //     feDetalles.Add(det);
           // }
           // Doc.PagoTarjeta = feDetallesPago.ToArray();

            //ZONA MODIFICADA
            //Doc.Sunat_Autorizacion = ("Autorizado mediante Resolución N� " + Documento_Cliente.Vnt_Cod_Autorizacion_Sunat.Trim);
            //// "018005000949/SUNAT"
            //Doc.Emisor_Web_Visualizacion = ("Consulte su documento en " + "www.grupoortiz.pe/facturacion");
            //Doc.RepresentacionImpresaDela = ("Representaci�n Impresa de la " + Doc.TipoDocumento_Descripcion);
            //Doc.Hora_Emision = (!string.IsNullOrEmpty(Documento_Cliente.Dvt_AtencHora) ? Convert.ToDateTime(Documento_Cliente.Dvt_AtencHora) : Now);
            //Doc.Cajero_Nombre = Actual_Conexion.UserName;//.UserName;
            //Doc.Emisor_Establecimiento = ("Establecimiento: " + ((Documento_Cliente.Vnt_IsVenta_Automatica == true) ? Documento_Cliente.Vnt_Establecimiento_Descripcion : BSIEstablecimiento.Caption));
            //Doc.Emisor_Establecimiento_Direccion = ((Documento_Cliente.Vnt_IsVenta_Automatica == true) ? Documento_Cliente.Vnt_Establecimiento_Direccion : DireccionDescripcionPuntoVenta);


            return Doc;

        }





        private void txtmotivocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmotivocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmotivocod.Text.Substring(txtmotivocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0513";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmotivocod.Tag = Entidad.Id_General_Det;
                                txtmotivocod.Text = Entidad.Gen_Codigo_Interno;
                                txtmotivodesc.Text = Entidad.Gen_Descripcion_Det;

                                txtmotivocod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmotivocod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarMotivo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMotivo()
        {
            try
            {
                txtmotivocod.Text = Accion.Formato(txtmotivocod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0513",
                    Gen_Codigo_Interno = txtmotivocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtmotivocod.Text.Trim().ToUpper())
                        {
                            txtmotivocod.Tag = T.Id_General_Det;
                            txtmotivocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtmotivodesc.Text = T.Gen_Descripcion_Det;
                            txtmotivocod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmotivocod.EnterMoveNextControl = false;
                    txtmotivocod.ResetText();
                    txtmotivocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        int Ser_Max_Lineas;

        private void txtserie_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtserie.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtserie.Text.Substring(txtserie.Text.Length - 1, 1) == "*")
                    {
                        using (frm_series_documentos_por_punto_venta_busqueda f = new frm_series_documentos_por_punto_venta_busqueda())
                        {
                            f.pdvcodigo =txtpdvcod.Text.Trim();
                            f.tipodoc =txttipodoc.Tag.ToString().Trim();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Serie_Comprobante Entidad = new Entidad_Serie_Comprobante();

                                Entidad = f.SerieNumeros[f.gridView1.GetFocusedDataSourceRowIndex()];

  string Serie = "";
                                string Numero = "";

                                //int NumActual = Convert.ToInt32(Entidad.Ser_Numero_Actual);
                                //int Incrementado = (NumActual + 1);
                                //string Resultado = Convert.ToString(Incrementado);
                                //Numero = Accion.Formato(Resultado, 8);

                                //txtserie.Text = Entidad.Ser_Serie+ "-" + Numero;

                              
                          

                                Serie = Entidad.Ser_Serie;

                                Es_Electronico = Entidad.Es_Electronico;

                                if (Entidad.Ser_Numero_Actual == "")
                                {
                                    Entidad.Ser_Numero_Actual = Convert.ToString("0");
                                }



                                if (((int.Parse(Entidad.Ser_Numero_Actual) >= (int.Parse(Entidad.Ser_Numero_Ini) - 1)) && (int.Parse(Entidad.Ser_Numero_Actual) < int.Parse(Entidad.Ser_Numero_Fin))))
                                {

                                    int NumActual = Convert.ToInt32(Entidad.Ser_Numero_Actual);
                                    int Incrementado = (NumActual + 1);
                                    string Resultado = Convert.ToString(Incrementado);
                                    Numero = Accion.Formato(Resultado, 8);

                                    txtserie.Text = Serie + "-" + Numero;
                                    txtserie.EnterMoveNextControl = true;
                                }
                                else
                                {
                                    Accion.Advertencia("El numero de esta serie esta fuera del intervalo, corrija su Base de datos");
                                    //Estado = Estados.Ninguno;
                                    return;
                                }


                            }
                        }
                    }
                    //else
                    //    if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtserie.Text) & string.IsNullOrEmpty(txtserie.Text))
                    //{
                    //    BuscarSerieNumero();
                    //}
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarSerieNumero()
        {
            try
            {
                //txtmedpago.Text = /*Accion.Formato(txtmedpago.Text, 2);*/
                Logica_Cuenta_Corriente log = new Logica_Cuenta_Corriente();

                List<Entidad_Cuenta_Corriente> Generales = new List<Entidad_Cuenta_Corriente>();
                Generales = log.Listar(new Entidad_Cuenta_Corriente
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                   // Id_Cuenta_Corriente = tctctacorriente.Text
                });

                if (Generales.Count > 0)
                {

                    //foreach (Entidad_Cuenta_Corriente T in Generales)
                    //{
                    //    if ((T.Id_Cuenta_Corriente).ToString().Trim().ToUpper() == tctctacorriente.Text.Trim().ToUpper())
                    //    {
                    //        tctctacorriente.Text = (T.Id_Cuenta_Corriente).ToString().Trim();

                    //        tctctacorriente.EnterMoveNextControl = true;
                    //    }
                    //}

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    //tctctacorriente.EnterMoveNextControl = false;
                    //tctctacorriente.ResetText();
                    //tctctacorriente.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnanadircomprobante_Click(object sender, EventArgs e)
        {
            try
            {
                //If Not String.IsNullOrEmpty(TxtCliDescripcion.Text.Trim) Then
                //if (string.IsNullOrEmpty(txtrucdni.Text.Trim()))
             //   {
                    using (frm_nota_credito_afectar_busqueda f = new frm_nota_credito_afectar_busqueda())
                    {
                        f.ctb_pdv_codigo = txtpdvcod.Text.Trim();
                        f.Ctb_Ruc_dni = txtrucdni.Text.Trim();

                        if (f.ShowDialog() == DialogResult.OK)
                        {
                        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                        Entidad = f.Lista[f.bandedGridView1.GetFocusedDataSourceRowIndex()];


                        MBase1 = Entidad.Ctb_Base_Imponible;
                        MValor_Fac_Export = Entidad.Ctb_Valor_Fact_Exonerada;
                        MIgv1 = Entidad.Ctb_Igv;
                        MIsc = Entidad.Ctb_Isc_Importe;
                        MOtrosTributos = Entidad.Ctb_Otros_Tributos_Importe;
                        MExonerada = Entidad.Ctb_Exonerada;
                        MInafecta = Entidad.Ctb_Inafecta;
                        MImporteTotal = Entidad.Ctb_Importe_Total;

           
                        txtbaseimponible.Text = Convert.ToDecimal(MBase1).ToString("0.00");
                        txtvalorfactexport.Text = Convert.ToDecimal(MValor_Fac_Export).ToString("0.00");
                        txtigv.Text = Convert.ToDecimal(MIgv1).ToString("0.00");
                        txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");
                        txtotrostribuimporte.Text = Convert.ToDecimal(MOtrosTributos).ToString("0.00");
                        txtexonerada.Text = Convert.ToDecimal(MExonerada).ToString("0.00");
                        txtinafecta.Text = Convert.ToDecimal(MInafecta).ToString("0.00");
                        txtimportetotal.Text = Convert.ToDecimal(MImporteTotal).ToString("0.00");


                        Comprobantes.Add(Entidad);

                        dgvcomprobantes.DataSource = null;

                        if (Comprobantes.Count > 0)
                            {
                                dgvcomprobantes.DataSource = Comprobantes;

                                Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                                Dgvdetalles.DataSource = null;
                                Detalles = log_det_con.Listar_Det_(Entidad);

                                if (Detalles.Count > 0)
                                {

                                    Dgvdetalles.DataSource = Detalles;
                                    
                                //MostrarResumen();

                                }


                        }

                    }
                    }


                //}
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void MostrarResumen()
        {
            decimal MBase1 = 0, MValor_Fac_Export = 0;
            decimal MExonerada = 0, MInafecta = 0, MIGV = 0;
            decimal MISC = 0;
            decimal MOtrosTributos = 0, MImporteTotal = 0;
            decimal TotalOpgravada = 0;

            foreach (Entidad_Movimiento_Cab T in Detalles)
            {
                MValor_Fac_Export += T.DvtD_ValorFactExportacion;
                // MBase1 += T.DvtD_BaseImpobleOperacionGravada;//Math.Round((T.Adm_Total / (Igv_Porcentaje + 1)),4);
                MExonerada += T.DvtD_Exonerada;
                MInafecta += T.DvtD_Inafecta;
                MISC += T.DvtD_ISC;
                // MIGV += T.DvtD_IGV_IPM;

                if (T.Adm_Tipo_Operacion == "0565")
                {
                    MBase1 += Math.Round((T.Adm_Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + 1)), 2);
                    MIGV += Math.Round((T.Adm_Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + 1)) * Convert.ToDecimal(txtigvporcentaje.Tag), 2);

                }


            }


            MImporteTotal = (MValor_Fac_Export + MBase1 + MExonerada + MInafecta + MISC + MIGV);
            txtvalorfactexport.Text = String.Format("{0:0,0.00}", Math.Round(MValor_Fac_Export, 2));
            txtbaseimponible.Text = String.Format("{0:0,0.00}", Math.Round(MBase1, 2));
            txtexonerada.Text = String.Format("{0:0,0.00}", Math.Round(MExonerada, 2));
            txtinafecta.Text = String.Format("{0:0,0.00}", Math.Round(MInafecta, 2));
            txtimporteisc.Text = String.Format("{0:0,0.00}", Math.Round(MISC, 2));
            txtigv.Text = String.Format("{0:0,0.00}", Math.Round(MIGV, 2));
            txtimportetotal.Text = String.Format("{0:0,0.00}", Math.Round(MImporteTotal, 2));



        }


        bool Es_Electronico;
        void BuscarSerieNumDocPtoVta(string DocumentoCod)
        {
            try
            {
                Logica_Serie_Comprobante LogSerieNum = new Logica_Serie_Comprobante();
                List<Entidad_Serie_Comprobante> SerieNumeros = new List<Entidad_Serie_Comprobante>();

                SerieNumeros = LogSerieNum.Listar(new Entidad_Serie_Comprobante
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Ser_Tipo_Doc = txttipodoc.Tag.ToString().Trim(),
                    Ser_Pdv_Codigo = txtpdvcod.Text.Trim()
                });

                if ((SerieNumeros.Count <= 0))
                {

                    Accion.Advertencia("La configuracion de series para este documento en este punto de venta esta pendiente");
    
                    return;
                }
                else
                {

                    string Serie = "";
                    string Numero = "";

                    Serie = SerieNumeros[0].Ser_Serie;
                    
                    Es_Electronico = SerieNumeros[0].Es_Electronico;

                    if (SerieNumeros[0].Ser_Numero_Actual == "")
                    {
                        SerieNumeros[0].Ser_Numero_Actual = Convert.ToString("0");
                    }
 
 

                    if (((int.Parse(SerieNumeros[0].Ser_Numero_Actual) >= (int.Parse(SerieNumeros[0].Ser_Numero_Ini) - 1)) && (int.Parse(SerieNumeros[0].Ser_Numero_Actual) < int.Parse(SerieNumeros[0].Ser_Numero_Fin))))
                    {

                        int NumActual = Convert.ToInt32(SerieNumeros[0].Ser_Numero_Actual);
                        int Incrementado = (NumActual + 1);
                        string Resultado = Convert.ToString(Incrementado);
                        Numero = Accion.Formato(Resultado, 8);

                        txtserie.Text = Serie + "-" + Numero;
                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("El numero de esta serie esta fuera del intervalo, corrija su Base de datos");
                        //Estado = Estados.Ninguno;
                        return;
                    }

                }

            }

            catch (Exception ex)
            {
 
            }

        }



        private void txtpdvcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpdvcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpdvcod.Text.Substring(txtpdvcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_punto_venta_busqueda f = new frm_punto_venta_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpdvcod.Text = Entidad.Pdv_Codigo.Trim();
                                txtpdvdesc.Text = Entidad.Pdv_Nombre.Trim();

                                BuscarSerieNumDocPtoVta(txttipodoc.Text.Trim());
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpdvcod.Text) & string.IsNullOrEmpty(txtpdvdesc.Text))
                    {
                        BuscarPuntoVenta();
                        BuscarSerieNumDocPtoVta(txttipodoc.Text.Trim());
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarPuntoVenta()
        {
            try
            {
                txtpdvcod.Text = Accion.Formato(txtpdvcod.Text, 2);

                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Generales = new List<Entidad_Punto_Venta>();

                Generales = log.Listar(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,

                    Pdv_Codigo = txtpdvcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Punto_Venta T in Generales)
                    {
                        if ((T.Pdv_Codigo).ToString().Trim().ToUpper() == txtpdvcod.Text.Trim().ToUpper())
                        {
                            txtpdvcod.Text = (T.Pdv_Codigo).ToString().Trim();
                            txtpdvdesc.Text = T.Pdv_Nombre.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }





    }
}
