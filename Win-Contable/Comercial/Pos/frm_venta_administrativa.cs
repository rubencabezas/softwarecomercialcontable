﻿using Comercial;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Pos
{
    public partial class frm_venta_administrativa : frm_fuente
    {
        public frm_venta_administrativa()
        {
            InitializeComponent();
        }

        private void frm_venta_administrativa_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.ctb_pdv_codigo = null;
            Ent.Id_Movimiento = null;
            try
            {
                Lista = log.Listar_(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_venta_administrativa_edicion f = new frm_venta_administrativa_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Pdv_Codigo = Entidad.ctb_pdv_codigo;
                f.Id_Movimiento = Entidad.Id_Movimiento;


                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

        private void bandedGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & bandedGridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;

                    Entidad = Lista[bandedGridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnanular_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                if (Entidad.Estado == "0002")
                {
                    Accion.Advertencia("El comprobante ya fue anulado,verifique su estado.");
                    return;
                }

                string DBObject = "La venta a anular tiene la siguientes caracteristicas:";
                //DateTime pFecha;
                DateTime? pFecha = null;
                //Nullable<DateTime> pFecha = null;

                string pRazon = "";
                bool pInformarBaja = false;

                if (Accion.ShowAnulacionRazon(DBObject))
                {
                    
                    pFecha = Accion.pFecha;
                    pRazon = Accion.pRazon;
                    pInformarBaja = Accion.pInformarBaja;
                    //Anular_adm_Ventas
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Anio = Entidad.Id_Anio,
                        Id_Periodo = Entidad.Id_Periodo,
                        ctb_pdv_codigo = Entidad.ctb_pdv_codigo,
                        Id_Movimiento = Entidad.Id_Movimiento
                    };

                    Logica_Movimiento_Cab logi = new Logica_Movimiento_Cab();

                    logi.Anular_adm_Ventas(Enti);

                    List<DocumentoElectronico> DocList = new List<DocumentoElectronico>();
                    DocumentoElectronico  docElec = new DocumentoElectronico();

                   
                    docElec.Empresa = Entidad.Id_Empresa;
                    docElec.Anio = Entidad.Id_Anio;
                    docElec.Periodo = Entidad.Id_Periodo;
                    docElec.Tipo_Doc_Sistema = Entidad.Ctb_Tipo_Doc.Trim();
                    docElec.Emisor_Documento_Tipo = "6";
                    docElec.Emisor_Documento_Numero = Actual_Conexion.RucEmpresa.Trim();
                    docElec.TipoDocumento_Codigo = Entidad.Ctb_Tipo_Doc_Sunat.Trim();
                    docElec.TipoDocumento_Serie = Entidad.Ctb_Serie.Trim();
                    docElec.TipoDocumento_Numero = Entidad.Ctb_Numero.Trim();
                    docElec.RazonAnulacion = pRazon;


                    string ws_Rpta = Facte.Dar_Baja(docElec);
                    string[] datosRpta = ws_Rpta.Split(Convert.ToChar("#"));
                    string DataVar;
                    DataVar = datosRpta[0];
                    //DataVar = datosRpta[1];
                    //DataVar = datosRpta[2];
                    //DataVar = datosRpta[3];

                    if (datosRpta[0].ToString().Trim() != "0" & datosRpta[0].ToString().Trim() != "-")
                    {
                        Accion.ExitoGuardar();
                        Estado = Estados.Ninguno;
                    }
                    else
                    {
                        //Tools.ShowWarnig();
                    }

                 

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();
        private void bntgenerarXML_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Entidad.Ven_FactElectronica_Estado == "4108")//4108	RECHAZADO
                {
                    Accion.Advertencia("No se puede generar el comprobante fue RECHAZADO");
                }
                else if (Entidad.Ven_FactElectronica_Estado == "4107")// 4107 ACEPTADO
                {
                    Accion.Advertencia("No se puede generar el comprobante fue ACEPTADO");
                }
                else
                {
                    Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                 
                    Detalles = log_det_con.Listar_Det_(Entidad);
                    Generar(Entidad);
                     
                }

            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


        public void Generar(Entidad_Movimiento_Cab Doc)
        {

            DocumentoElectronico Doc_Electronico_Bol_Fact;
            Doc_Electronico_Bol_Fact = Convertir_Cliente_Objet(Doc);
            string ws_Rpta = Facte.Registrar_Para_Enviar(Doc_Electronico_Bol_Fact);

            if (!string.IsNullOrEmpty(ws_Rpta))
            {
                string[] datosRpta = ws_Rpta.Split(Convert.ToChar("#"));
                string DataVar;
                DataVar = datosRpta[0]; // Correlativo '- error, 0 documento no enviado, 1 documento registrado/actualizado
                DataVar = datosRpta[1]; // Codigo web
                DataVar = datosRpta[2]; // DigestValue (Código Hash)
                DataVar = datosRpta[3];
                if (datosRpta[0].ToString().Trim() != "0" & datosRpta[0].ToString().Trim() != "-")
                {
                 
                    Accion.Advertencia("El archivo fue generado correctamente");
                }
                else
                {
                    Accion.Advertencia("El archivo ya fue generado");
                }
            }

        }


        DocumentoElectronico Convertir_Cliente_Objet(Entidad_Movimiento_Cab Documento_Cliente)
        {
            DocumentoElectronico Doc = new DocumentoElectronico();
            Doc.Impresion_Modelo_Codigo = "";

            Doc.Fecha_Emision = Documento_Cliente.Ctb_Fecha_Movimiento;
            Doc.Emisor_ApellidosNombres_RazonSocial = Actual_Conexion.EmpresaNombre.Trim();
            Doc.Emisor_Documento_Tipo = "6";
            Doc.Emisor_Documento_Numero = Actual_Conexion.RucEmpresa.Trim();
            // ----------------------------
            // --- Domicilio fiscal -- PostalAddress
            Doc.Emisor_Direccion_Ubigeo = "020101";
            Doc.Emisor_Direccion_Calle = Actual_Conexion.Emp_DireccionCorta.Trim();//Actual_Conexion.Emp_DireccionCorta.Trim();
            Doc.Emisor_Direccion_Urbanizacion = Actual_Conexion.Emp_Urbanizacion.Trim();// Actual_Conexion.Emp_Urbanizacion.Trim();
            Doc.Emisor_Direccion_Departamento = Actual_Conexion.Emp_Direccion_Departamento.Trim();// Actual_Conexion.Emp_Direccion_Departamento.Trim();
            Doc.Emisor_Direccion_Provincia = Actual_Conexion.Emp_Direccion_Provincia.Trim();// Actual_Conexion.Emp_Direccion_Provincia.Trim();
            Doc.Emisor_Direccion_Distrito = Actual_Conexion.Emp_Direccion_Distrito.Trim();//Actual_Conexion.Emp_Direccion_Distrito.Trim();
            Doc.Emisor_Direccion_PaisCodigo = "PE";

            // Doc.Fecha_Vencimiento = DBNull.Value;

            // "01" ' Si es Boleta o Factura segun Sunat ==>
            Doc.TipoDocumento_Codigo = Documento_Cliente.Ctb_Tipo_Doc_Sunat.Trim();
            Doc.TipoDocumento_Serie = Documento_Cliente.Ctb_Serie.Trim();
            Doc.TipoDocumento_Numero = Documento_Cliente.Ctb_Numero.Trim();

            string Desc = "";
            //if (Es_Electronico)
            //{
                Desc = "ELECTRONICA";
            //}

            Doc.TipoDocumento_Descripcion = (Documento_Cliente.Nombre_Comprobante.Trim() + Desc);

            // "6" 'Si es DNI o RUC segun sunat
            Doc.Cliente_Documento_Tipo = Documento_Cliente.Ent_TpDcEnt_SUNAT.Trim();
            Doc.Cliente_Documento_Numero = ((Documento_Cliente.Ctb_Ruc_dni.Trim() == "00000001") ? "00000000" : Documento_Cliente.Ctb_Ruc_dni.Trim());
            Doc.Cliente_RazonSocial_Nombre = Documento_Cliente.Entidad.Trim();
            Doc.Cliente_Direccion = Documento_Cliente.Cliente_Direccion.Trim();
            Doc.Cliente_Correo = Documento_Cliente.Correo_Cliente.Trim();
            Doc.Numero_Placa_del_Vehiculo = "";// 'Documento_Cliente.Vnt_placa_Vehiculo;


            List<DocumentoElectronicoDetalle> feDetalles = new List<DocumentoElectronicoDetalle>();

            List<Entidad_Movimiento_Cab> DetalleVenta = new List<Entidad_Movimiento_Cab>();


            foreach (Entidad_Movimiento_Cab bf in Detalles)
            {
                DocumentoElectronicoDetalle det = new DocumentoElectronicoDetalle();

                det.Item = bf.Adm_Item;
                det.Producto_Codigo = bf.Adm_Catalogo.Trim();
                det.Producto_Descripcion = (bf.Adm_Catalogo_Desc.Trim() + (" x " + bf.Adm_Unm_Desc.Trim()));
                det.Producto_UnidadMedida_Codigo = bf.UNM_CoigodFE.Trim();
                det.Producto_Cantidad = Convert.ToDouble(bf.Adm_Cantidad);
                det.Unitario_Precio_Venta = Convert.ToDouble(bf.Adm_Valor_Unit);//DvtD_PrecUnitario
                det.Unitario_Precio_Venta_Tipo_Codigo = "01";
                det.Item_IGV_Total = bf.DvtD_IGVMonto;
                det.Unitario_IGV_Porcentaje = Convert.ToDouble(bf.DvtD_IGVTasaPorcentaje);
                det.Item_Tipo_Afectacion_IGV_Codigo = "10";
                det.Item_ISC_Total = 0;
                det.Unitario_Valor_Unitario = Convert.ToDouble(bf.Adm_Valor_Venta);//DvtD_ValorVenta
                det.Item_ValorVenta_Total = bf.DvtD_SubTotal;
                det.Item_Total = Convert.ToDouble(bf.Adm_Total);
                det.Numero_Placa_del_Vehiculo = "";//(((bf.UNM_CoigodFE == "GLL") || (bf.UNM_CoigodFE == "LTR")) ? Documento_Cliente.Vnt_placa_Vehiculo : null);
                feDetalles.Add(det);
            }

            Doc.Detalles = feDetalles.ToArray();
            Doc.Total_ValorVenta_OperacionesGravadas = Convert.ToDouble(Documento_Cliente.Ctb_Base_Imponible); //Dvt_MntoBaseImponible
            Doc.Total_ValorVenta_OperacionesInafectas = Convert.ToDouble(Documento_Cliente.Ctb_Inafecta);//Dvt_MntoInafecta
            Doc.Total_ValorVenta_OperacionesExoneradas = Convert.ToDouble(Documento_Cliente.Ctb_Exonerada); //Dvt_MntoExonerado
            Doc.Total_ValorVenta_OperacionesGratuitas = 0;
            Doc.Total_IGV = Convert.ToDouble(Documento_Cliente.Ctb_Igv);// Dvt_MntoIGV
            Doc.IGV_Porcentaje = Convert.ToDouble(Documento_Cliente.Ctb_Tasa_IGV);// Dvt_PrcIGV
            Doc.Total_ISC = Convert.ToDouble(Documento_Cliente.Ctb_Isc_Importe);// Dvt_MntoISC
            Doc.Total_OtrosTributos = 0;
            Doc.Total_OtrosCargos = 0;
            // Doc.Descuentos_Globales = 0
            Doc.Total_Importe_Venta = Convert.ToDouble(Documento_Cliente.Ctb_Importe_Total);// Dvt_MntoTotal
            Doc.Moneda_Codigo = "PEN";
            Doc.Moneda_Descripcion = "SOLES";
            Doc.Moneda_Simbolo = "S/";

            Doc.Total_Importe_Venta_Texto = Imprimir_Letras.NroEnLetras(Convert.ToDecimal(Doc.Total_Importe_Venta)) + " " + Doc.Moneda_Descripcion;


            Doc.Impresion_Modelo_Codigo = "000";

            //Venta percepcion
            Doc.Tipo_Operacion_Codigo = null;
            Doc.Total_Importe_Percepcion = 0;
            Doc.Percep_Importe_Total_Cobrado = 0;

            //    45 Leyendas
            List<DocumentoElectronicoLeyenda> ListaLeyendas = new List<DocumentoElectronicoLeyenda>();
            //ListaLeyendas.Add(new DocumentoElectronicoLeyenda() With {, ., Codigo = 1000, ., ((NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) + (" " + Doc.Moneda_Descripcion))));

            ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "1000", Descripcion = Imprimir_Letras.NroEnLetras(Convert.ToDecimal(Math.Truncate(Doc.Total_Importe_Venta))) + " " + Doc.Moneda_Descripcion });

            if (Doc.Total_ValorVenta_OperacionesGratuitas > 0)
            {
                ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "1002", Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y / O SERVICIO PRESTADO GRATUITAMENTE" });
            }

            if ((Doc.Total_Importe_Percepcion > 0))
            {
                ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "2000", Descripcion = "COMPROBANTE DE PERCEPCION" });
            }

            if (!string.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo))
            {
                if (Doc.Tipo_Operacion_Codigo == "05")
                {
                    ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "2005", Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE" });
                }

            }

            Doc.Leyendas = ListaLeyendas.ToArray();

            List<DocumentoElectronicoPagoTarjeta> feDetallesPago = new List<DocumentoElectronicoPagoTarjeta>();

            //foreach (Entidad_Movimiento_Cab bf in PagosDoc)
            //{
            //    DocumentoElectronicoDetalle det = new DocumentoElectronicoDetalle();
            //    feDetalles.Add(det);

            //}

           // Doc.PagoTarjeta = feDetallesPago.ToArray();

            //ZONA MODIFICADA
            //Doc.Sunat_Autorizacion = ("Autorizado mediante Resolución N� " + Documento_Cliente.Vnt_Cod_Autorizacion_Sunat.Trim);
            //// "018005000949/SUNAT"
            //Doc.Emisor_Web_Visualizacion = ("Consulte su documento en " + "www.grupoortiz.pe/facturacion");
            //Doc.RepresentacionImpresaDela = ("Representaci�n Impresa de la " + Doc.TipoDocumento_Descripcion);
            //Doc.Hora_Emision = (!string.IsNullOrEmpty(Documento_Cliente.Dvt_AtencHora) ? Convert.ToDateTime(Documento_Cliente.Dvt_AtencHora) : Now);
            //Doc.Cajero_Nombre = Actual_Conexion.UserName;//.UserName;
            //Doc.Emisor_Establecimiento = ("Establecimiento: " + ((Documento_Cliente.Vnt_IsVenta_Automatica == true) ? Documento_Cliente.Vnt_Establecimiento_Descripcion : BSIEstablecimiento.Caption));
            //Doc.Emisor_Establecimiento_Direccion = ((Documento_Cliente.Vnt_IsVenta_Automatica == true) ? Documento_Cliente.Vnt_Establecimiento_Direccion : DireccionDescripcionPuntoVenta);


            return Doc;

        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Listar();
        }

        public void Re_Imprimir(Entidad_Movimiento_Cab Doc)
        {

            DocumentoElectronico Doc_Electronico_Bol_Fact;
            Doc_Electronico_Bol_Fact = Convertir_Cliente_Objet(Doc);


            Print_Invoice_POS RptPOS = new Print_Invoice_POS();
            List<DocumentoElectronico> listRpt = new List<DocumentoElectronico>();
            listRpt.Add(Doc_Electronico_Bol_Fact);
            listRpt[0].Print_BarCode = Doc.XML_SignatureValue;
            listRpt[0].Print_DigestValue = Doc.XML_DigestValue;

            if (listRpt[0].Cliente_Documento_Numero.Length != 11)
            {
                RptPOS.XrLabel13.Text = "DNI:";
                ////'Cuando es boleta ocultamos estos campos
                //RptPOS.XrLabel19.Visible = false;
                //RptPOS.XrLabel20.Visible = false;
                //RptPOS.XrLabel5.Visible = false;
                //RptPOS.XrLabel6.Visible = false;
                //RptPOS.XrLabel23.Visible = false;
                //RptPOS.XrLabel30.Visible = false;
            }
            else
            {
                RptPOS.XrLabel13.Text = "RUC:";
            }


            RptPOS.lblplaca.Visible = false;
            RptPOS.lblhash.Visible = true;
            RptPOS.CodigoBarras.Visible = true;
            RptPOS.AutorizacionSunat.Visible = false;
            RptPOS.DetailPagoTarjeta.Visible = false;
            RptPOS.DatosVendedor.Visible = false;

            int HeightDoc = 1000;
            HeightDoc = HeightDoc + (32 * Detalles.Count());
            RptPOS.PageHeight = HeightDoc;


            RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            RptPOS.DataSource = listRpt;

            RptPOS.CreateDocument();
            //RptPOS.PrinterName = NombreImpresora;

            //PdfExportOptions pdfOptions = RptPOS.ExportOptions.Pdf;

            //pdfOptions.DocumentOptions.Title = BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim();
            //string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "VENTAS AL CONTADO");
            //string NombreArchivo = Carpeta + "\\" + BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim() + ".pdf";
            //RptPOS.ExportToPdf(NombreArchivo, pdfOptions);

            try
            {
                //using (ReportPrintTool printTool = new ReportPrintTool(RptPOS))
                //{
                //  printTool.Print(NombreImpresora);

                //}
                var RibbonPreview = new PrintPreviewRibbonFormEx();
                RibbonPreview.PrintingSystem = RptPOS.PrintingSystem;
                RibbonPreview.ShowDialog();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnimprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();

                Detalles = log_det_con.Listar_Det_(Entidad);
                Re_Imprimir(Entidad);
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }

}
