﻿namespace Comercial
{
    partial class Rpte_Comprobante_No_Electronico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.lbl_direccion = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_dni = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cliente = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_fecha_emision = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_TipoDocumento_Serie_Numero = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_TipoDocumento_Descripcion = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Emisor_Documento_Numero = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Emisor_Apellidos_RazonSocial = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.XrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblotroscargos = new DevExpress.XtraReports.UI.XRLabel();
            this.lblexonerada = new DevExpress.XtraReports.UI.XRLabel();
            this.lblinafecta = new DevExpress.XtraReports.UI.XRLabel();
            this.lblisc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_igv = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_op_gravadas = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.HeightF = 14F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Invd_Total", "{0:n2}")});
            this.xrLabel4.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(207.324F, 2.000014F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(60.34235F, 11.99999F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "xrLabel4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Invd_Valor_Unit", "{0:n2}")});
            this.xrLabel3.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(152.0005F, 0.9999911F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(55.32364F, 13.00001F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Invd_Cantidad")});
            this.xrLabel2.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00051F, 0.9999911F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(41.99994F, 13.00001F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Invd_Catalogo_Desc")});
            this.xrLabel1.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(52.00046F, 0.9999911F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 12F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "xrLabel1";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6});
            this.BottomMargin.HeightF = 16.5164F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.BottomMargin.Visible = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(11.58486F, 4.516396F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(88.26543F, 12F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "ddd";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_direccion,
            this.lbl_dni,
            this.lbl_cliente,
            this.lbl_fecha_emision,
            this.XrLabel36,
            this.XrLabel8,
            this.XrLabel13,
            this.XrLabel43,
            this.lbl_TipoDocumento_Serie_Numero,
            this.lbl_TipoDocumento_Descripcion,
            this.lbl_Emisor_Documento_Numero,
            this.lbl_Emisor_Apellidos_RazonSocial,
            this.XrLabel34,
            this.XrLabel35,
            this.XrLabel33,
            this.XrLabel32,
            this.XrLine3});
            this.PageHeader.HeightF = 156.5689F;
            this.PageHeader.Name = "PageHeader";
            // 
            // lbl_direccion
            // 
            this.lbl_direccion.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_direccion.LocationFloat = new DevExpress.Utils.PointFloat(62.00008F, 120.9166F);
            this.lbl_direccion.Name = "lbl_direccion";
            this.lbl_direccion.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_direccion.SizeF = new System.Drawing.SizeF(205.6663F, 12F);
            this.lbl_direccion.StylePriority.UseFont = false;
            this.lbl_direccion.Text = "direccion";
            // 
            // lbl_dni
            // 
            this.lbl_dni.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_dni.LocationFloat = new DevExpress.Utils.PointFloat(28.50005F, 108.9166F);
            this.lbl_dni.Name = "lbl_dni";
            this.lbl_dni.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_dni.SizeF = new System.Drawing.SizeF(238.5812F, 11.99999F);
            this.lbl_dni.StylePriority.UseFont = false;
            this.lbl_dni.Text = "dni";
            // 
            // lbl_cliente
            // 
            this.lbl_cliente.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_cliente.LocationFloat = new DevExpress.Utils.PointFloat(61F, 95.91676F);
            this.lbl_cliente.Name = "lbl_cliente";
            this.lbl_cliente.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cliente.SizeF = new System.Drawing.SizeF(206.6664F, 12F);
            this.lbl_cliente.StylePriority.UseFont = false;
            this.lbl_cliente.Text = "cliente";
            // 
            // lbl_fecha_emision
            // 
            this.lbl_fecha_emision.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_fecha_emision.LocationFloat = new DevExpress.Utils.PointFloat(100.0146F, 82.91677F);
            this.lbl_fecha_emision.Name = "lbl_fecha_emision";
            this.lbl_fecha_emision.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_fecha_emision.SizeF = new System.Drawing.SizeF(167.0671F, 11.99998F);
            this.lbl_fecha_emision.StylePriority.UseFont = false;
            this.lbl_fecha_emision.Text = "fecha_emision";
            // 
            // XrLabel36
            // 
            this.XrLabel36.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(0F, 82.91677F);
            this.XrLabel36.Name = "XrLabel36";
            this.XrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel36.SizeF = new System.Drawing.SizeF(100.0146F, 11.99998F);
            this.XrLabel36.StylePriority.UseFont = false;
            this.XrLabel36.Text = "FECHA DE EMISIÓN:";
            // 
            // XrLabel8
            // 
            this.XrLabel8.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 95.91676F);
            this.XrLabel8.Name = "XrLabel8";
            this.XrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel8.SizeF = new System.Drawing.SizeF(61F, 12F);
            this.XrLabel8.StylePriority.UseFont = false;
            this.XrLabel8.Text = "SEÑOR(ES):";
            // 
            // XrLabel13
            // 
            this.XrLabel13.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 108.9166F);
            this.XrLabel13.Name = "XrLabel13";
            this.XrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel13.SizeF = new System.Drawing.SizeF(28.50005F, 12F);
            this.XrLabel13.StylePriority.UseFont = false;
            this.XrLabel13.Text = "DNI:";
            // 
            // XrLabel43
            // 
            this.XrLabel43.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 120.9166F);
            this.XrLabel43.Name = "XrLabel43";
            this.XrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel43.SizeF = new System.Drawing.SizeF(62F, 12F);
            this.XrLabel43.StylePriority.UseFont = false;
            this.XrLabel43.Text = "DIRECCION:";
            // 
            // lbl_TipoDocumento_Serie_Numero
            // 
            this.lbl_TipoDocumento_Serie_Numero.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.lbl_TipoDocumento_Serie_Numero.LocationFloat = new DevExpress.Utils.PointFloat(10.37818F, 55.64001F);
            this.lbl_TipoDocumento_Serie_Numero.Name = "lbl_TipoDocumento_Serie_Numero";
            this.lbl_TipoDocumento_Serie_Numero.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_TipoDocumento_Serie_Numero.SizeF = new System.Drawing.SizeF(256F, 16F);
            this.lbl_TipoDocumento_Serie_Numero.StylePriority.UseFont = false;
            this.lbl_TipoDocumento_Serie_Numero.StylePriority.UseTextAlignment = false;
            this.lbl_TipoDocumento_Serie_Numero.Text = "TipoDocumento_Serie_Numero";
            this.lbl_TipoDocumento_Serie_Numero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_TipoDocumento_Descripcion
            // 
            this.lbl_TipoDocumento_Descripcion.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.lbl_TipoDocumento_Descripcion.LocationFloat = new DevExpress.Utils.PointFloat(10.37819F, 34.00001F);
            this.lbl_TipoDocumento_Descripcion.Name = "lbl_TipoDocumento_Descripcion";
            this.lbl_TipoDocumento_Descripcion.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_TipoDocumento_Descripcion.SizeF = new System.Drawing.SizeF(256F, 21.64F);
            this.lbl_TipoDocumento_Descripcion.StylePriority.UseFont = false;
            this.lbl_TipoDocumento_Descripcion.StylePriority.UseTextAlignment = false;
            this.lbl_TipoDocumento_Descripcion.Text = "TipoDocumento_Descripcion";
            this.lbl_TipoDocumento_Descripcion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_Emisor_Documento_Numero
            // 
            this.lbl_Emisor_Documento_Numero.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_Emisor_Documento_Numero.LocationFloat = new DevExpress.Utils.PointFloat(52.00046F, 22F);
            this.lbl_Emisor_Documento_Numero.Name = "lbl_Emisor_Documento_Numero";
            this.lbl_Emisor_Documento_Numero.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_Emisor_Documento_Numero.SizeF = new System.Drawing.SizeF(186.4583F, 12F);
            this.lbl_Emisor_Documento_Numero.StylePriority.UseFont = false;
            this.lbl_Emisor_Documento_Numero.StylePriority.UseTextAlignment = false;
            this.lbl_Emisor_Documento_Numero.Text = "Emisor_Documento_Numero";
            this.lbl_Emisor_Documento_Numero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_Emisor_Apellidos_RazonSocial
            // 
            this.lbl_Emisor_Apellidos_RazonSocial.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_Emisor_Apellidos_RazonSocial.LocationFloat = new DevExpress.Utils.PointFloat(52.00046F, 10F);
            this.lbl_Emisor_Apellidos_RazonSocial.Name = "lbl_Emisor_Apellidos_RazonSocial";
            this.lbl_Emisor_Apellidos_RazonSocial.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_Emisor_Apellidos_RazonSocial.SizeF = new System.Drawing.SizeF(186.4583F, 12F);
            this.lbl_Emisor_Apellidos_RazonSocial.StylePriority.UseFont = false;
            this.lbl_Emisor_Apellidos_RazonSocial.StylePriority.UseTextAlignment = false;
            this.lbl_Emisor_Apellidos_RazonSocial.Text = "Emisor_Apellidos_RazonSocial";
            this.lbl_Emisor_Apellidos_RazonSocial.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XrLabel34
            // 
            this.XrLabel34.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold);
            this.XrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(152.7036F, 143.569F);
            this.XrLabel34.Name = "XrLabel34";
            this.XrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel34.SizeF = new System.Drawing.SizeF(54.03578F, 11F);
            this.XrLabel34.StylePriority.UseFont = false;
            this.XrLabel34.StylePriority.UseTextAlignment = false;
            this.XrLabel34.Text = "PRECIO";
            this.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XrLabel35
            // 
            this.XrLabel35.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold);
            this.XrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(206.7393F, 143.569F);
            this.XrLabel35.Name = "XrLabel35";
            this.XrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel35.SizeF = new System.Drawing.SizeF(60.34239F, 11.00001F);
            this.XrLabel35.StylePriority.UseFont = false;
            this.XrLabel35.StylePriority.UseTextAlignment = false;
            this.XrLabel35.Text = "IMPORTE";
            this.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XrLabel33
            // 
            this.XrLabel33.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold);
            this.XrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(51.41578F, 143.569F);
            this.XrLabel33.Name = "XrLabel33";
            this.XrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel33.SizeF = new System.Drawing.SizeF(101.2878F, 11.00001F);
            this.XrLabel33.StylePriority.UseFont = false;
            this.XrLabel33.StylePriority.UseTextAlignment = false;
            this.XrLabel33.Text = "DESCRIPCIÓN";
            this.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XrLabel32
            // 
            this.XrLabel32.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold);
            this.XrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(9.415829F, 143.569F);
            this.XrLabel32.Name = "XrLabel32";
            this.XrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel32.SizeF = new System.Drawing.SizeF(41.99995F, 11F);
            this.XrLabel32.StylePriority.UseFont = false;
            this.XrLabel32.StylePriority.UseTextAlignment = false;
            this.XrLabel32.Text = "CANT.";
            this.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XrLine3
            // 
            this.XrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.XrLine3.LocationFloat = new DevExpress.Utils.PointFloat(9.415325F, 154.5689F);
            this.XrLine3.Name = "XrLine3";
            this.XrLine3.SizeF = new System.Drawing.SizeF(257.666F, 2F);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLine2,
            this.lblotroscargos,
            this.lblexonerada,
            this.lblinafecta,
            this.lblisc,
            this.xrLabel5,
            this.XrLabel17,
            this.XrLabel57,
            this.XrLabel58,
            this.XrLabel60,
            this.lbl_total,
            this.lbl_igv,
            this.lbl_op_gravadas,
            this.XrLabel21,
            this.XrLabel19});
            this.ReportFooter.HeightF = 106.7095F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // XrLine2
            // 
            this.XrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.XrLine2.LocationFloat = new DevExpress.Utils.PointFloat(8.580584F, 3F);
            this.XrLine2.Name = "XrLine2";
            this.XrLine2.SizeF = new System.Drawing.SizeF(257.666F, 2F);
            // 
            // lblotroscargos
            // 
            this.lblotroscargos.Font = new System.Drawing.Font("Arial", 7F);
            this.lblotroscargos.LocationFloat = new DevExpress.Utils.PointFloat(174.373F, 69.94846F);
            this.lblotroscargos.Name = "lblotroscargos";
            this.lblotroscargos.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblotroscargos.SizeF = new System.Drawing.SizeF(92.70828F, 12F);
            this.lblotroscargos.StylePriority.UseFont = false;
            this.lblotroscargos.StylePriority.UseTextAlignment = false;
            this.lblotroscargos.Text = "0.00";
            this.lblotroscargos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblotroscargos.TextFormatString = "{0:n2}";
            // 
            // lblexonerada
            // 
            this.lblexonerada.Font = new System.Drawing.Font("Arial", 7F);
            this.lblexonerada.LocationFloat = new DevExpress.Utils.PointFloat(175.0385F, 57.94854F);
            this.lblexonerada.Name = "lblexonerada";
            this.lblexonerada.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblexonerada.SizeF = new System.Drawing.SizeF(92.62704F, 12F);
            this.lblexonerada.StylePriority.UseFont = false;
            this.lblexonerada.StylePriority.UseTextAlignment = false;
            this.lblexonerada.Text = "0.00";
            this.lblexonerada.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblexonerada.TextFormatString = "{0:n2}";
            // 
            // lblinafecta
            // 
            this.lblinafecta.Font = new System.Drawing.Font("Arial", 7F);
            this.lblinafecta.LocationFloat = new DevExpress.Utils.PointFloat(175.0385F, 45.94854F);
            this.lblinafecta.Name = "lblinafecta";
            this.lblinafecta.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblinafecta.SizeF = new System.Drawing.SizeF(92.62704F, 12F);
            this.lblinafecta.StylePriority.UseFont = false;
            this.lblinafecta.StylePriority.UseTextAlignment = false;
            this.lblinafecta.Text = "0.00";
            this.lblinafecta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblinafecta.TextFormatString = "{0:n2}";
            // 
            // lblisc
            // 
            this.lblisc.Font = new System.Drawing.Font("Arial", 7F);
            this.lblisc.LocationFloat = new DevExpress.Utils.PointFloat(174.373F, 21.94855F);
            this.lblisc.Name = "lblisc";
            this.lblisc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblisc.SizeF = new System.Drawing.SizeF(93.29294F, 12F);
            this.lblisc.StylePriority.UseFont = false;
            this.lblisc.StylePriority.UseTextAlignment = false;
            this.lblisc.Text = "0.00";
            this.lblisc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblisc.TextFormatString = "{0:n2}";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 7F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(11.58486F, 33.94854F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(87.5995F, 12F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "I.G.V.";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // XrLabel17
            // 
            this.XrLabel17.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(11.66612F, 21.94855F);
            this.XrLabel17.Name = "XrLabel17";
            this.XrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel17.SizeF = new System.Drawing.SizeF(88.18419F, 12F);
            this.XrLabel17.StylePriority.UseFont = false;
            this.XrLabel17.StylePriority.UseTextAlignment = false;
            this.XrLabel17.Text = "I.S.C.";
            this.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // XrLabel57
            // 
            this.XrLabel57.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(11.66612F, 45.94854F);
            this.XrLabel57.Name = "XrLabel57";
            this.XrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel57.SizeF = new System.Drawing.SizeF(87.51826F, 12F);
            this.XrLabel57.StylePriority.UseFont = false;
            this.XrLabel57.StylePriority.UseTextAlignment = false;
            this.XrLabel57.Text = "Op. Inafecta";
            this.XrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // XrLabel58
            // 
            this.XrLabel58.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(11.66612F, 57.94854F);
            this.XrLabel58.Name = "XrLabel58";
            this.XrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel58.SizeF = new System.Drawing.SizeF(87.51826F, 12F);
            this.XrLabel58.StylePriority.UseFont = false;
            this.XrLabel58.StylePriority.UseTextAlignment = false;
            this.XrLabel58.Text = "Op. Exonerada";
            this.XrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // XrLabel60
            // 
            this.XrLabel60.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(11.58486F, 69.94855F);
            this.XrLabel60.Name = "XrLabel60";
            this.XrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel60.SizeF = new System.Drawing.SizeF(88.26543F, 12F);
            this.XrLabel60.StylePriority.UseFont = false;
            this.XrLabel60.StylePriority.UseTextAlignment = false;
            this.XrLabel60.Text = "Otros Cargos";
            this.XrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl_total
            // 
            this.lbl_total.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_total.LocationFloat = new DevExpress.Utils.PointFloat(174.373F, 88.75272F);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total.SizeF = new System.Drawing.SizeF(92.70828F, 12F);
            this.lbl_total.StylePriority.UseFont = false;
            this.lbl_total.StylePriority.UseTextAlignment = false;
            this.lbl_total.Text = "Total";
            this.lbl_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_igv
            // 
            this.lbl_igv.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_igv.LocationFloat = new DevExpress.Utils.PointFloat(174.373F, 33.94854F);
            this.lbl_igv.Name = "lbl_igv";
            this.lbl_igv.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_igv.SizeF = new System.Drawing.SizeF(93.29288F, 12F);
            this.lbl_igv.StylePriority.UseFont = false;
            this.lbl_igv.StylePriority.UseTextAlignment = false;
            this.lbl_igv.Text = "0.00";
            this.lbl_igv.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_op_gravadas
            // 
            this.lbl_op_gravadas.Font = new System.Drawing.Font("Arial", 7F);
            this.lbl_op_gravadas.LocationFloat = new DevExpress.Utils.PointFloat(174.373F, 9.999996F);
            this.lbl_op_gravadas.Name = "lbl_op_gravadas";
            this.lbl_op_gravadas.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_op_gravadas.SizeF = new System.Drawing.SizeF(93.29253F, 12F);
            this.lbl_op_gravadas.StylePriority.UseFont = false;
            this.lbl_op_gravadas.StylePriority.UseTextAlignment = false;
            this.lbl_op_gravadas.Text = "0.00";
            this.lbl_op_gravadas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // XrLabel21
            // 
            this.XrLabel21.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.XrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(9.496146F, 87.75293F);
            this.XrLabel21.Name = "XrLabel21";
            this.XrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel21.SizeF = new System.Drawing.SizeF(99.9344F, 12F);
            this.XrLabel21.StylePriority.UseFont = false;
            this.XrLabel21.StylePriority.UseTextAlignment = false;
            this.XrLabel21.Text = "IMPORTE TOTAL";
            this.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // XrLabel19
            // 
            this.XrLabel19.Font = new System.Drawing.Font("Arial", 7F);
            this.XrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00051F, 10F);
            this.XrLabel19.Name = "XrLabel19";
            this.XrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel19.SizeF = new System.Drawing.SizeF(89.26485F, 12F);
            this.XrLabel19.StylePriority.UseFont = false;
            this.XrLabel19.StylePriority.UseTextAlignment = false;
            this.XrLabel19.Text = "OP. GRAVADAS";
            this.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(Contable.Entidad_Movimiento_Inventario);
            // 
            // Rpte_Comprobante_No_Electronico
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.ReportFooter});
            this.DataSource = this.bindingSource1;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 17);
            this.PageWidth = 272;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic;
            this.ShowPreviewMarginLines = false;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel34;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel35;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel33;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel32;
        internal DevExpress.XtraReports.UI.XRLine XrLine3;
        public DevExpress.XtraReports.UI.XRLabel lbl_TipoDocumento_Serie_Numero;
        public DevExpress.XtraReports.UI.XRLabel lbl_TipoDocumento_Descripcion;
        public DevExpress.XtraReports.UI.XRLabel lbl_Emisor_Documento_Numero;
        public DevExpress.XtraReports.UI.XRLabel lbl_Emisor_Apellidos_RazonSocial;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        public DevExpress.XtraReports.UI.XRLabel lbl_total;
        public DevExpress.XtraReports.UI.XRLabel lbl_igv;
        public DevExpress.XtraReports.UI.XRLabel lbl_op_gravadas;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel21;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel19;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel36;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel8;
        public DevExpress.XtraReports.UI.XRLabel XrLabel13;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel43;
        public DevExpress.XtraReports.UI.XRLabel lbl_direccion;
        public DevExpress.XtraReports.UI.XRLabel lbl_dni;
        public DevExpress.XtraReports.UI.XRLabel lbl_cliente;
        public DevExpress.XtraReports.UI.XRLabel lbl_fecha_emision;
        public DevExpress.XtraReports.UI.XRLabel lblotroscargos;
        public DevExpress.XtraReports.UI.XRLabel lblexonerada;
        public DevExpress.XtraReports.UI.XRLabel lblinafecta;
        public DevExpress.XtraReports.UI.XRLabel lblisc;
        internal DevExpress.XtraReports.UI.XRLabel xrLabel5;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel17;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel57;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel58;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel60;
        internal DevExpress.XtraReports.UI.XRLine XrLine2;
        internal DevExpress.XtraReports.UI.XRLabel xrLabel6;
    }
}
