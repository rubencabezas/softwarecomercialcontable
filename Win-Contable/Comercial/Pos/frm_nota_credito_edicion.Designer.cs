﻿namespace Contable.Comercial.Pos
{
    partial class frm_nota_credito_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.txtpdvdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpdvcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechavencimiento = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtanalisisdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtidanalisis = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.chklote = new System.Windows.Forms.CheckBox();
            this.txtunmabrev = new DevExpress.XtraEditors.TextEdit();
            this.txtunmdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtunmcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.Dgvdetalles = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Lote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtcantidad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtvalorunit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtalmacendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacencod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.TxtAfectIGVDescripcion = new DevExpress.XtraEditors.TextEdit();
            this.TxtAfectIGVCodigo = new DevExpress.XtraEditors.TextEdit();
            this.txttipooperaciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipooperacion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtimporte = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtmotivodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmotivocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvcomprobantes = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnanadircomprobante = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitarcomprobante = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtigvporcentaje = new DevExpress.XtraEditors.TextEdit();
            this.chkotrostri = new DevExpress.XtraEditors.CheckEdit();
            this.chkisc = new DevExpress.XtraEditors.CheckEdit();
            this.txtimportetotal = new DevExpress.XtraEditors.TextEdit();
            this.txtotrostribuimporte = new DevExpress.XtraEditors.TextEdit();
            this.txtigv = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteisc = new DevExpress.XtraEditors.TextEdit();
            this.txtinafecta = new DevExpress.XtraEditors.TextEdit();
            this.txtexonerada = new DevExpress.XtraEditors.TextEdit();
            this.txtvalorfactexport = new DevExpress.XtraEditors.TextEdit();
            this.txtbaseimponible = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmabrev.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvdetalles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorunit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVDescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperaciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivocod.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcomprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtinafecta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexonerada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorfactexport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1009, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 578);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1009, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 546);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1009, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 546);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // txtpdvdesc
            // 
            this.txtpdvdesc.Enabled = false;
            this.txtpdvdesc.EnterMoveNextControl = true;
            this.txtpdvdesc.Location = new System.Drawing.Point(483, 14);
            this.txtpdvdesc.MenuManager = this.barManager1;
            this.txtpdvdesc.Name = "txtpdvdesc";
            this.txtpdvdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpdvdesc.Size = new System.Drawing.Size(213, 20);
            this.txtpdvdesc.TabIndex = 4;
            // 
            // txtpdvcod
            // 
            this.txtpdvcod.EnterMoveNextControl = true;
            this.txtpdvcod.Location = new System.Drawing.Point(443, 14);
            this.txtpdvcod.MenuManager = this.barManager1;
            this.txtpdvcod.Name = "txtpdvcod";
            this.txtpdvcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpdvcod.Size = new System.Drawing.Size(37, 20);
            this.txtpdvcod.TabIndex = 3;
            this.txtpdvcod.EditValueChanged += new System.EventHandler(this.txtpdvcod_EditValueChanged);
            this.txtpdvcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpdvcod_KeyDown);
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(363, 14);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(74, 13);
            this.labelControl18.TabIndex = 2;
            this.labelControl18.Text = "Punto de venta";
            // 
            // txtfechavencimiento
            // 
            this.txtfechavencimiento.Enabled = false;
            this.txtfechavencimiento.EnterMoveNextControl = true;
            this.txtfechavencimiento.Location = new System.Drawing.Point(105, 80);
            this.txtfechavencimiento.MenuManager = this.barManager1;
            this.txtfechavencimiento.Name = "txtfechavencimiento";
            this.txtfechavencimiento.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechavencimiento.Properties.Mask.BeepOnError = true;
            this.txtfechavencimiento.Properties.Mask.EditMask = "d";
            this.txtfechavencimiento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechavencimiento.Properties.Mask.SaveLiteral = false;
            this.txtfechavencimiento.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechavencimiento.Properties.MaxLength = 10;
            this.txtfechavencimiento.Size = new System.Drawing.Size(108, 20);
            this.txtfechavencimiento.TabIndex = 15;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(6, 84);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(93, 13);
            this.labelControl35.TabIndex = 14;
            this.labelControl35.Text = "Fecha Vencimiento:";
            // 
            // txtanalisisdesc
            // 
            this.txtanalisisdesc.Enabled = false;
            this.txtanalisisdesc.Location = new System.Drawing.Point(390, 125);
            this.txtanalisisdesc.MenuManager = this.barManager1;
            this.txtanalisisdesc.Name = "txtanalisisdesc";
            this.txtanalisisdesc.Size = new System.Drawing.Size(47, 20);
            this.txtanalisisdesc.TabIndex = 31;
            this.txtanalisisdesc.Visible = false;
            // 
            // txtidanalisis
            // 
            this.txtidanalisis.Location = new System.Drawing.Point(343, 125);
            this.txtidanalisis.MenuManager = this.barManager1;
            this.txtidanalisis.Name = "txtidanalisis";
            this.txtidanalisis.Size = new System.Drawing.Size(44, 20);
            this.txtidanalisis.TabIndex = 30;
            this.txtidanalisis.Visible = false;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(226, 128);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(111, 13);
            this.labelControl25.TabIndex = 29;
            this.labelControl25.Text = "Analisis de la operacion";
            this.labelControl25.Visible = false;
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(631, 80);
            this.txttipocambiovalor.MenuManager = this.barManager1;
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Size = new System.Drawing.Size(65, 20);
            this.txttipocambiovalor.TabIndex = 22;
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(100, 58);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(192, 20);
            this.txttipodocdesc.TabIndex = 9;
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(607, 58);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(89, 20);
            this.txtfechadoc.TabIndex = 13;
            this.txtfechadoc.TextChanged += new System.EventHandler(this.txtfechadoc_TextChanged);
            this.txtfechadoc.Leave += new System.EventHandler(this.txtfechadoc_Leave);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(544, 61);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(58, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Fecha Doc.:";
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(337, 58);
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(201, 20);
            this.txtserie.TabIndex = 11;
            this.txtserie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtserie_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(297, 61);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Serie:";
            // 
            // txttipodoc
            // 
            this.txttipodoc.Enabled = false;
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(60, 58);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 8;
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(20, 61);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "T.Doc.:";
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(146, 102);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtentidad.Size = new System.Drawing.Size(550, 20);
            this.txtentidad.TabIndex = 25;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(522, 80);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 21;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(486, 80);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 20;
            this.txttipocambiocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipocambiocod_KeyDown);
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(305, 80);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedadesc.Size = new System.Drawing.Size(149, 20);
            this.txtmonedadesc.TabIndex = 18;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(265, 80);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedacod.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacod.TabIndex = 17;
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(100, 125);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondiciondesc.Size = new System.Drawing.Size(115, 20);
            this.txtcondiciondesc.TabIndex = 28;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(461, 81);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(21, 13);
            this.labelControl10.TabIndex = 19;
            this.labelControl10.Text = "T.C.";
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.Enabled = false;
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(61, 125);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(38, 20);
            this.txtcondicioncod.TabIndex = 27;
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(219, 83);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Moneda:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(4, 129);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 26;
            this.labelControl8.Text = "Condicion:";
            // 
            // txtrucdni
            // 
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(61, 102);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 24;
            this.txtrucdni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdni_KeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(21, 105);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(37, 13);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "R.U.C.:";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(60, 35);
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(636, 20);
            this.txtglosa.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(20, 38);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Glosa:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(260, -3);
            this.txtlibro.MenuManager = this.barManager1;
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlibro.Size = new System.Drawing.Size(221, 20);
            this.txtlibro.TabIndex = 1;
            this.txtlibro.Visible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(223, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            this.labelControl1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(240)))), ((int)(((byte)(112)))));
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Falta asignar lote";
            // 
            // chklote
            // 
            this.chklote.AutoSize = true;
            this.chklote.Enabled = false;
            this.chklote.Location = new System.Drawing.Point(632, 62);
            this.chklote.Name = "chklote";
            this.chklote.Size = new System.Drawing.Size(91, 17);
            this.chklote.TabIndex = 22;
            this.chklote.Text = "Acepta lotes?";
            this.chklote.UseVisualStyleBackColor = true;
            // 
            // txtunmabrev
            // 
            this.txtunmabrev.Enabled = false;
            this.txtunmabrev.Location = new System.Drawing.Point(401, 38);
            this.txtunmabrev.MenuManager = this.barManager1;
            this.txtunmabrev.Name = "txtunmabrev";
            this.txtunmabrev.Size = new System.Drawing.Size(82, 20);
            this.txtunmabrev.TabIndex = 12;
            // 
            // txtunmdesc
            // 
            this.txtunmdesc.Enabled = false;
            this.txtunmdesc.Location = new System.Drawing.Point(157, 38);
            this.txtunmdesc.MenuManager = this.barManager1;
            this.txtunmdesc.Name = "txtunmdesc";
            this.txtunmdesc.Size = new System.Drawing.Size(243, 20);
            this.txtunmdesc.TabIndex = 11;
            // 
            // txtunmcod
            // 
            this.txtunmcod.Enabled = false;
            this.txtunmcod.Location = new System.Drawing.Point(109, 38);
            this.txtunmcod.MenuManager = this.barManager1;
            this.txtunmcod.Name = "txtunmcod";
            this.txtunmcod.Size = new System.Drawing.Size(46, 20);
            this.txtunmcod.TabIndex = 10;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(20, 41);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(85, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Unidad de medida";
            // 
            // Dgvdetalles
            // 
            gridLevelNode1.RelationName = "Level1";
            this.Dgvdetalles.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.Dgvdetalles.Location = new System.Drawing.Point(6, 106);
            this.Dgvdetalles.MainView = this.gridView2;
            this.Dgvdetalles.MenuManager = this.barManager1;
            this.Dgvdetalles.Name = "Dgvdetalles";
            this.Dgvdetalles.Size = new System.Drawing.Size(990, 174);
            this.Dgvdetalles.TabIndex = 49;
            this.Dgvdetalles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn1,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.Lote,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.GridControl = this.Dgvdetalles;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tipo";
            this.gridColumn10.FieldName = "Adm_Tipo_BSA_Desc";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 79;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Unidad de medida";
            this.gridColumn1.FieldName = "Adm_Unm_Desc";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 161;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Producto";
            this.gridColumn11.FieldName = "Adm_Catalogo_Desc";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            this.gridColumn11.Width = 215;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Almacen";
            this.gridColumn12.FieldName = "Adm_Almacen_desc";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            this.gridColumn12.Width = 207;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Cantidad";
            this.gridColumn13.DisplayFormat.FormatString = "n2";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "Adm_Cantidad";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Adm_Cantidad", "{0:n2}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 4;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Valor Unit.";
            this.gridColumn14.DisplayFormat.FormatString = "n2";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn14.FieldName = "Adm_Valor_Unit";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Adm_Valor_Unit", "{0:n2}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 5;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Total";
            this.gridColumn15.DisplayFormat.FormatString = "n2";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn15.FieldName = "Adm_Total";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Adm_Total", "{0:n2}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 6;
            // 
            // Lote
            // 
            this.Lote.Caption = "Lote";
            this.Lote.FieldName = "Acepta_lotes";
            this.Lote.Name = "Lote";
            this.Lote.Visible = true;
            this.Lote.VisibleIndex = 7;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Adm_Catalogo";
            this.gridColumn2.FieldName = "Adm_Catalogo";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Adm_Item";
            this.gridColumn3.FieldName = "Adm_Item";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // txtcantidad
            // 
            this.txtcantidad.EnterMoveNextControl = true;
            this.txtcantidad.Location = new System.Drawing.Point(109, 60);
            this.txtcantidad.MenuManager = this.barManager1;
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Size = new System.Drawing.Size(100, 20);
            this.txtcantidad.TabIndex = 17;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(58, 63);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(47, 13);
            this.labelControl11.TabIndex = 16;
            this.labelControl11.Text = "Cantidad:";
            // 
            // txtvalorunit
            // 
            this.txtvalorunit.EnterMoveNextControl = true;
            this.txtvalorunit.Location = new System.Drawing.Point(268, 60);
            this.txtvalorunit.MenuManager = this.barManager1;
            this.txtvalorunit.Name = "txtvalorunit";
            this.txtvalorunit.Size = new System.Drawing.Size(100, 20);
            this.txtvalorunit.TabIndex = 19;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(215, 63);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(50, 13);
            this.labelControl12.TabIndex = 18;
            this.labelControl12.Text = "Valor Unit.";
            // 
            // txtalmacendesc
            // 
            this.txtalmacendesc.Enabled = false;
            this.txtalmacendesc.EnterMoveNextControl = true;
            this.txtalmacendesc.Location = new System.Drawing.Point(582, 38);
            this.txtalmacendesc.MenuManager = this.barManager1;
            this.txtalmacendesc.Name = "txtalmacendesc";
            this.txtalmacendesc.Size = new System.Drawing.Size(141, 20);
            this.txtalmacendesc.TabIndex = 15;
            // 
            // txtalmacencod
            // 
            this.txtalmacencod.Enabled = false;
            this.txtalmacencod.EnterMoveNextControl = true;
            this.txtalmacencod.Location = new System.Drawing.Point(537, 38);
            this.txtalmacencod.MenuManager = this.barManager1;
            this.txtalmacencod.Name = "txtalmacencod";
            this.txtalmacencod.Size = new System.Drawing.Size(44, 20);
            this.txtalmacencod.TabIndex = 14;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(489, 42);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(44, 13);
            this.labelControl13.TabIndex = 13;
            this.labelControl13.Text = "Almacen:";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(262, 19);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(47, 13);
            this.labelControl14.TabIndex = 6;
            this.labelControl14.Text = "Producto:";
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(390, 17);
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Size = new System.Drawing.Size(333, 20);
            this.txtproductodesc.TabIndex = 8;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(313, 17);
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Size = new System.Drawing.Size(76, 20);
            this.txtproductocod.TabIndex = 7;
            // 
            // TxtAfectIGVDescripcion
            // 
            this.TxtAfectIGVDescripcion.Enabled = false;
            this.TxtAfectIGVDescripcion.EnterMoveNextControl = true;
            this.TxtAfectIGVDescripcion.Location = new System.Drawing.Point(154, 82);
            this.TxtAfectIGVDescripcion.Name = "TxtAfectIGVDescripcion";
            this.TxtAfectIGVDescripcion.Size = new System.Drawing.Size(214, 20);
            this.TxtAfectIGVDescripcion.TabIndex = 25;
            // 
            // TxtAfectIGVCodigo
            // 
            this.TxtAfectIGVCodigo.Enabled = false;
            this.TxtAfectIGVCodigo.EnterMoveNextControl = true;
            this.TxtAfectIGVCodigo.Location = new System.Drawing.Point(109, 82);
            this.TxtAfectIGVCodigo.Name = "TxtAfectIGVCodigo";
            this.TxtAfectIGVCodigo.Size = new System.Drawing.Size(44, 20);
            this.TxtAfectIGVCodigo.TabIndex = 24;
            // 
            // txttipooperaciondesc
            // 
            this.txttipooperaciondesc.Enabled = false;
            this.txttipooperaciondesc.EnterMoveNextControl = true;
            this.txttipooperaciondesc.Location = new System.Drawing.Point(508, 81);
            this.txttipooperaciondesc.Name = "txttipooperaciondesc";
            this.txttipooperaciondesc.Size = new System.Drawing.Size(214, 20);
            this.txttipooperaciondesc.TabIndex = 25;
            // 
            // txttipooperacion
            // 
            this.txttipooperacion.EnterMoveNextControl = true;
            this.txttipooperacion.Location = new System.Drawing.Point(463, 81);
            this.txttipooperacion.Name = "txttipooperacion";
            this.txttipooperacion.Size = new System.Drawing.Size(44, 20);
            this.txttipooperacion.TabIndex = 24;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(71, 85);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(32, 13);
            this.labelControl17.TabIndex = 23;
            this.labelControl17.Text = "Af.IGV";
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(112, 16);
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(141, 20);
            this.txttipobsadesc.TabIndex = 5;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(405, 87);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(53, 13);
            this.labelControl15.TabIndex = 23;
            this.labelControl15.Text = "Operación:";
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(67, 16);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Size = new System.Drawing.Size(44, 20);
            this.txttipobsacod.TabIndex = 4;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(9, 22);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(54, 13);
            this.labelControl16.TabIndex = 3;
            this.labelControl16.Text = "Tipo B/S/A:";
            // 
            // txtimporte
            // 
            this.txtimporte.EnterMoveNextControl = true;
            this.txtimporte.Location = new System.Drawing.Point(432, 60);
            this.txtimporte.Name = "txtimporte";
            this.txtimporte.Properties.Mask.EditMask = "n2";
            this.txtimporte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporte.Size = new System.Drawing.Size(82, 20);
            this.txtimporte.TabIndex = 21;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(374, 64);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(38, 13);
            this.labelControl20.TabIndex = 20;
            this.labelControl20.Text = "Importe";
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnanadirdet.Appearance.Options.UseFont = true;
            this.btnanadirdet.Location = new System.Drawing.Point(828, 62);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(87, 37);
            this.btnanadirdet.TabIndex = 26;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Visible = false;
            // 
            // btnquitardet
            // 
            this.btnquitardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnquitardet.Appearance.Options.UseFont = true;
            this.btnquitardet.Location = new System.Drawing.Point(828, 19);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(87, 37);
            this.btnquitardet.TabIndex = 4;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Visible = false;
            // 
            // btneditardet
            // 
            this.btneditardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneditardet.Appearance.Options.UseFont = true;
            this.btneditardet.Location = new System.Drawing.Point(735, 62);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(87, 37);
            this.btneditardet.TabIndex = 4;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Visible = false;
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevodet.Appearance.Options.UseFont = true;
            this.btnnuevodet.Location = new System.Drawing.Point(735, 19);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(87, 37);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtproductodesc);
            this.groupBox1.Controls.Add(this.Dgvdetalles);
            this.groupBox1.Controls.Add(this.chklote);
            this.groupBox1.Controls.Add(this.btnanadirdet);
            this.groupBox1.Controls.Add(this.TxtAfectIGVDescripcion);
            this.groupBox1.Controls.Add(this.btnquitardet);
            this.groupBox1.Controls.Add(this.btneditardet);
            this.groupBox1.Controls.Add(this.labelControl16);
            this.groupBox1.Controls.Add(this.btnnuevodet);
            this.groupBox1.Controls.Add(this.TxtAfectIGVCodigo);
            this.groupBox1.Controls.Add(this.txtunmabrev);
            this.groupBox1.Controls.Add(this.txttipooperaciondesc);
            this.groupBox1.Controls.Add(this.txttipooperacion);
            this.groupBox1.Controls.Add(this.txtcantidad);
            this.groupBox1.Controls.Add(this.labelControl17);
            this.groupBox1.Controls.Add(this.txtunmdesc);
            this.groupBox1.Controls.Add(this.labelControl15);
            this.groupBox1.Controls.Add(this.labelControl11);
            this.groupBox1.Controls.Add(this.txttipobsacod);
            this.groupBox1.Controls.Add(this.txtvalorunit);
            this.groupBox1.Controls.Add(this.txtunmcod);
            this.groupBox1.Controls.Add(this.labelControl12);
            this.groupBox1.Controls.Add(this.txttipobsadesc);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.txtproductocod);
            this.groupBox1.Controls.Add(this.labelControl14);
            this.groupBox1.Controls.Add(this.labelControl13);
            this.groupBox1.Controls.Add(this.txtalmacencod);
            this.groupBox1.Controls.Add(this.txtalmacendesc);
            this.groupBox1.Controls.Add(this.txtimporte);
            this.groupBox1.Controls.Add(this.labelControl20);
            this.groupBox1.Location = new System.Drawing.Point(1, 286);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(998, 287);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalles";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtanalisisdesc);
            this.groupBox2.Controls.Add(this.txtidanalisis);
            this.groupBox2.Controls.Add(this.txtfechavencimiento);
            this.groupBox2.Controls.Add(this.labelControl25);
            this.groupBox2.Controls.Add(this.txtmotivodesc);
            this.groupBox2.Controls.Add(this.txtpdvdesc);
            this.groupBox2.Controls.Add(this.txtcondiciondesc);
            this.groupBox2.Controls.Add(this.labelControl35);
            this.groupBox2.Controls.Add(this.txtcondicioncod);
            this.groupBox2.Controls.Add(this.txtentidad);
            this.groupBox2.Controls.Add(this.txtmotivocod);
            this.groupBox2.Controls.Add(this.labelControl8);
            this.groupBox2.Controls.Add(this.labelControl19);
            this.groupBox2.Controls.Add(this.txtpdvcod);
            this.groupBox2.Controls.Add(this.labelControl18);
            this.groupBox2.Controls.Add(this.txtrucdni);
            this.groupBox2.Controls.Add(this.labelControl7);
            this.groupBox2.Controls.Add(this.txttipocambiovalor);
            this.groupBox2.Controls.Add(this.txtglosa);
            this.groupBox2.Controls.Add(this.txttipodocdesc);
            this.groupBox2.Controls.Add(this.txttipocambiodesc);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.txttipocambiocod);
            this.groupBox2.Controls.Add(this.txtfechadoc);
            this.groupBox2.Controls.Add(this.txtmonedadesc);
            this.groupBox2.Controls.Add(this.txtmonedacod);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Controls.Add(this.labelControl6);
            this.groupBox2.Controls.Add(this.labelControl10);
            this.groupBox2.Controls.Add(this.txttipodoc);
            this.groupBox2.Controls.Add(this.txtserie);
            this.groupBox2.Controls.Add(this.labelControl9);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Location = new System.Drawing.Point(1, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(722, 157);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos Generales";
            // 
            // txtmotivodesc
            // 
            this.txtmotivodesc.Enabled = false;
            this.txtmotivodesc.EnterMoveNextControl = true;
            this.txtmotivodesc.Location = new System.Drawing.Point(100, 14);
            this.txtmotivodesc.Name = "txtmotivodesc";
            this.txtmotivodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmotivodesc.Size = new System.Drawing.Size(257, 20);
            this.txtmotivodesc.TabIndex = 4;
            // 
            // txtmotivocod
            // 
            this.txtmotivocod.EnterMoveNextControl = true;
            this.txtmotivocod.Location = new System.Drawing.Point(60, 14);
            this.txtmotivocod.Name = "txtmotivocod";
            this.txtmotivocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmotivocod.Size = new System.Drawing.Size(37, 20);
            this.txtmotivocod.TabIndex = 3;
            this.txtmotivocod.EditValueChanged += new System.EventHandler(this.txtpdvcod_EditValueChanged);
            this.txtmotivocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmotivocod_KeyDown);
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(18, 15);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(32, 13);
            this.labelControl19.TabIndex = 2;
            this.labelControl19.Text = "Motivo";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvcomprobantes);
            this.groupBox3.Controls.Add(this.btnanadircomprobante);
            this.groupBox3.Controls.Add(this.btnquitarcomprobante);
            this.groupBox3.Location = new System.Drawing.Point(1, 195);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(922, 90);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Comprobante que modifica";
            // 
            // dgvcomprobantes
            // 
            this.dgvcomprobantes.Location = new System.Drawing.Point(9, 20);
            this.dgvcomprobantes.MainView = this.gridView1;
            this.dgvcomprobantes.MenuManager = this.barManager1;
            this.dgvcomprobantes.Name = "dgvcomprobantes";
            this.dgvcomprobantes.Size = new System.Drawing.Size(813, 65);
            this.dgvcomprobantes.TabIndex = 0;
            this.dgvcomprobantes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn16});
            this.gridView1.GridControl = this.dgvcomprobantes;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "PERIODO";
            this.gridColumn4.FieldName = "Id_Periodo_Desc";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "CORRELATIVO";
            this.gridColumn5.FieldName = "Id_Movimiento";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "TIPO DOC.";
            this.gridColumn6.FieldName = "Nombre_Comprobante";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "SERIE";
            this.gridColumn7.FieldName = "Ctb_Serie";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "NUMERO";
            this.gridColumn8.FieldName = "Ctb_Numero";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "MONEDA";
            this.gridColumn9.FieldName = "Ctn_Moneda_Desc";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "TOTAL";
            this.gridColumn16.FieldName = "Ctb_Importe_Total";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            // 
            // btnanadircomprobante
            // 
            this.btnanadircomprobante.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnanadircomprobante.Appearance.Options.UseFont = true;
            this.btnanadircomprobante.Location = new System.Drawing.Point(828, 19);
            this.btnanadircomprobante.Name = "btnanadircomprobante";
            this.btnanadircomprobante.Size = new System.Drawing.Size(87, 25);
            this.btnanadircomprobante.TabIndex = 26;
            this.btnanadircomprobante.Text = "Añadir";
            this.btnanadircomprobante.Click += new System.EventHandler(this.btnanadircomprobante_Click);
            // 
            // btnquitarcomprobante
            // 
            this.btnquitarcomprobante.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnquitarcomprobante.Appearance.Options.UseFont = true;
            this.btnquitarcomprobante.Location = new System.Drawing.Point(828, 48);
            this.btnquitarcomprobante.Name = "btnquitarcomprobante";
            this.btnquitarcomprobante.Size = new System.Drawing.Size(87, 25);
            this.btnquitarcomprobante.TabIndex = 4;
            this.btnquitarcomprobante.Text = "Quitar";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtigvporcentaje);
            this.groupBox4.Controls.Add(this.chkotrostri);
            this.groupBox4.Controls.Add(this.chkisc);
            this.groupBox4.Controls.Add(this.txtimportetotal);
            this.groupBox4.Controls.Add(this.txtotrostribuimporte);
            this.groupBox4.Controls.Add(this.txtigv);
            this.groupBox4.Controls.Add(this.txtimporteisc);
            this.groupBox4.Controls.Add(this.txtinafecta);
            this.groupBox4.Controls.Add(this.txtexonerada);
            this.groupBox4.Controls.Add(this.txtvalorfactexport);
            this.groupBox4.Controls.Add(this.txtbaseimponible);
            this.groupBox4.Controls.Add(this.labelControl28);
            this.groupBox4.Controls.Add(this.labelControl27);
            this.groupBox4.Controls.Add(this.labelControl24);
            this.groupBox4.Controls.Add(this.labelControl23);
            this.groupBox4.Controls.Add(this.labelControl22);
            this.groupBox4.Controls.Add(this.labelControl21);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(729, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(194, 185);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Resumen";
            // 
            // txtigvporcentaje
            // 
            this.txtigvporcentaje.EditValue = "0.00";
            this.txtigvporcentaje.Location = new System.Drawing.Point(55, 119);
            this.txtigvporcentaje.MenuManager = this.barManager1;
            this.txtigvporcentaje.Name = "txtigvporcentaje";
            this.txtigvporcentaje.Size = new System.Drawing.Size(39, 20);
            this.txtigvporcentaje.TabIndex = 18;
            // 
            // chkotrostri
            // 
            this.chkotrostri.Location = new System.Drawing.Point(9, 141);
            this.chkotrostri.MenuManager = this.barManager1;
            this.chkotrostri.Name = "chkotrostri";
            this.chkotrostri.Properties.Caption = "Otros Tributos";
            this.chkotrostri.Size = new System.Drawing.Size(92, 20);
            this.chkotrostri.TabIndex = 17;
            // 
            // chkisc
            // 
            this.chkisc.Location = new System.Drawing.Point(9, 99);
            this.chkisc.MenuManager = this.barManager1;
            this.chkisc.Name = "chkisc";
            this.chkisc.Properties.Caption = "I.S.C.";
            this.chkisc.Size = new System.Drawing.Size(75, 20);
            this.chkisc.TabIndex = 16;
            // 
            // txtimportetotal
            // 
            this.txtimportetotal.EditValue = "0.00";
            this.txtimportetotal.Location = new System.Drawing.Point(100, 161);
            this.txtimportetotal.MenuManager = this.barManager1;
            this.txtimportetotal.Name = "txtimportetotal";
            this.txtimportetotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimportetotal.Properties.Appearance.Options.UseFont = true;
            this.txtimportetotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimportetotal.Size = new System.Drawing.Size(87, 20);
            this.txtimportetotal.TabIndex = 15;
            // 
            // txtotrostribuimporte
            // 
            this.txtotrostribuimporte.EditValue = "0.00";
            this.txtotrostribuimporte.Location = new System.Drawing.Point(100, 140);
            this.txtotrostribuimporte.MenuManager = this.barManager1;
            this.txtotrostribuimporte.Name = "txtotrostribuimporte";
            this.txtotrostribuimporte.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtotrostribuimporte.Properties.Appearance.Options.UseFont = true;
            this.txtotrostribuimporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtotrostribuimporte.Size = new System.Drawing.Size(87, 20);
            this.txtotrostribuimporte.TabIndex = 14;
            // 
            // txtigv
            // 
            this.txtigv.EditValue = "0.00";
            this.txtigv.Location = new System.Drawing.Point(100, 119);
            this.txtigv.MenuManager = this.barManager1;
            this.txtigv.Name = "txtigv";
            this.txtigv.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtigv.Properties.Appearance.Options.UseFont = true;
            this.txtigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtigv.Size = new System.Drawing.Size(87, 20);
            this.txtigv.TabIndex = 13;
            // 
            // txtimporteisc
            // 
            this.txtimporteisc.EditValue = "0.00";
            this.txtimporteisc.Location = new System.Drawing.Point(100, 97);
            this.txtimporteisc.MenuManager = this.barManager1;
            this.txtimporteisc.Name = "txtimporteisc";
            this.txtimporteisc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimporteisc.Properties.Appearance.Options.UseFont = true;
            this.txtimporteisc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimporteisc.Size = new System.Drawing.Size(87, 20);
            this.txtimporteisc.TabIndex = 12;
            // 
            // txtinafecta
            // 
            this.txtinafecta.EditValue = "0.00";
            this.txtinafecta.Location = new System.Drawing.Point(100, 76);
            this.txtinafecta.MenuManager = this.barManager1;
            this.txtinafecta.Name = "txtinafecta";
            this.txtinafecta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtinafecta.Properties.Appearance.Options.UseFont = true;
            this.txtinafecta.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtinafecta.Size = new System.Drawing.Size(87, 20);
            this.txtinafecta.TabIndex = 11;
            // 
            // txtexonerada
            // 
            this.txtexonerada.EditValue = "0.00";
            this.txtexonerada.Location = new System.Drawing.Point(100, 55);
            this.txtexonerada.MenuManager = this.barManager1;
            this.txtexonerada.Name = "txtexonerada";
            this.txtexonerada.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtexonerada.Properties.Appearance.Options.UseFont = true;
            this.txtexonerada.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtexonerada.Size = new System.Drawing.Size(87, 20);
            this.txtexonerada.TabIndex = 10;
            // 
            // txtvalorfactexport
            // 
            this.txtvalorfactexport.EditValue = "0.00";
            this.txtvalorfactexport.Location = new System.Drawing.Point(100, 34);
            this.txtvalorfactexport.MenuManager = this.barManager1;
            this.txtvalorfactexport.Name = "txtvalorfactexport";
            this.txtvalorfactexport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtvalorfactexport.Properties.Appearance.Options.UseFont = true;
            this.txtvalorfactexport.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtvalorfactexport.Size = new System.Drawing.Size(87, 20);
            this.txtvalorfactexport.TabIndex = 9;
            // 
            // txtbaseimponible
            // 
            this.txtbaseimponible.EditValue = "0.00";
            this.txtbaseimponible.Location = new System.Drawing.Point(100, 13);
            this.txtbaseimponible.MenuManager = this.barManager1;
            this.txtbaseimponible.Name = "txtbaseimponible";
            this.txtbaseimponible.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtbaseimponible.Properties.Appearance.Options.UseFont = true;
            this.txtbaseimponible.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtbaseimponible.Size = new System.Drawing.Size(87, 20);
            this.txtbaseimponible.TabIndex = 8;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(9, 122);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(29, 13);
            this.labelControl28.TabIndex = 7;
            this.labelControl28.Text = "I.G.V.";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(9, 164);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(69, 13);
            this.labelControl27.TabIndex = 6;
            this.labelControl27.Text = "Importe Total:";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(9, 78);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(41, 13);
            this.labelControl24.TabIndex = 3;
            this.labelControl24.Text = "Inafecta";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(9, 56);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(52, 13);
            this.labelControl23.TabIndex = 2;
            this.labelControl23.Text = "Exonerada";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(9, 37);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(85, 13);
            this.labelControl22.TabIndex = 1;
            this.labelControl22.Text = "Val. Fact. Export.";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(9, 16);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(72, 13);
            this.labelControl21.TabIndex = 0;
            this.labelControl21.Text = "Base Imponible";
            // 
            // frm_nota_credito_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 578);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtlibro);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_nota_credito_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_nota_credito_edicion";
            this.Load += new System.EventHandler(this.frm_nota_credito_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmabrev.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvdetalles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorunit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVDescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperaciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivocod.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvcomprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtinafecta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexonerada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorfactexport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.TextEdit txtpdvdesc;
        private DevExpress.XtraEditors.TextEdit txtpdvcod;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtfechavencimiento;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtanalisisdesc;
        private DevExpress.XtraEditors.TextEdit txtidanalisis;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtentidad;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chklote;
        private DevExpress.XtraEditors.TextEdit txtunmabrev;
        private DevExpress.XtraEditors.TextEdit txtunmdesc;
        private DevExpress.XtraEditors.TextEdit txtunmcod;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.GridControl Dgvdetalles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn Lote;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.TextEdit txtcantidad;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtvalorunit;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtalmacendesc;
        private DevExpress.XtraEditors.TextEdit txtalmacencod;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.TextEdit TxtAfectIGVDescripcion;
        private DevExpress.XtraEditors.TextEdit TxtAfectIGVCodigo;
        private DevExpress.XtraEditors.TextEdit txttipooperaciondesc;
        private DevExpress.XtraEditors.TextEdit txttipooperacion;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txttipobsadesc;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        public DevExpress.XtraEditors.TextEdit txtimporte;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        public DevExpress.XtraEditors.SimpleButton btnanadirdet;
        public DevExpress.XtraEditors.SimpleButton btnquitardet;
        public DevExpress.XtraEditors.SimpleButton btneditardet;
        public DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraGrid.GridControl dgvcomprobantes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        public DevExpress.XtraEditors.SimpleButton btnanadircomprobante;
        public DevExpress.XtraEditors.SimpleButton btnquitarcomprobante;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.TextEdit txtigvporcentaje;
        private DevExpress.XtraEditors.CheckEdit chkotrostri;
        private DevExpress.XtraEditors.CheckEdit chkisc;
        private DevExpress.XtraEditors.TextEdit txtimportetotal;
        private DevExpress.XtraEditors.TextEdit txtotrostribuimporte;
        private DevExpress.XtraEditors.TextEdit txtigv;
        private DevExpress.XtraEditors.TextEdit txtimporteisc;
        private DevExpress.XtraEditors.TextEdit txtinafecta;
        private DevExpress.XtraEditors.TextEdit txtexonerada;
        private DevExpress.XtraEditors.TextEdit txtvalorfactexport;
        private DevExpress.XtraEditors.TextEdit txtbaseimponible;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.TextEdit txtmotivodesc;
        private DevExpress.XtraEditors.TextEdit txtmotivocod;
        private DevExpress.XtraEditors.LabelControl labelControl19;
    }
}