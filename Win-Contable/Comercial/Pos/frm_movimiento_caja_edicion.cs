﻿using Contable;
using Contable._1_Busquedas_Generales;
using Contable.Comercial.Pos;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_movimiento_caja_edicion : frm_fuente
    {
        public frm_movimiento_caja_edicion()
        {
            InitializeComponent();
        }
  
        public string Id_Empresa, Pdv_Codigo, Cja_Codigo;
        public int Cja_Aper_Cierre_Codigo;
        private void txttipoEgreso_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipoEgreso.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoEgreso.Text.Substring(txttipoEgreso.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0015";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoEgreso.Tag = Entidad.Id_General_Det;
                                txttipoEgreso.Text = Entidad.Gen_Codigo_Interno;
                                txttipoEgresodesc.Text = Entidad.Gen_Descripcion_Det;

                                if (txttipoEgreso.Text == "01") // Cuando es Ingreso
                                {
                                    btnBuscar.Enabled = true;
                                    btnquitar.Enabled = true;
                                }

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoEgreso.Text) & string.IsNullOrEmpty(txttipoEgresodesc.Text))
                    {
                        BuscarTipoEgre();

                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarTipoEgre()
        {
            try
            {
                txttipoEgreso.Text = Accion.Formato(txttipoEgreso.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0015",
                    Gen_Codigo_Interno = txttipoEgreso.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipoEgreso.Text.Trim())
                        {
                            txttipoEgreso.Tag = T.Id_General_Det;
                            txttipoEgreso.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipoEgresodesc.Text = T.Gen_Descripcion_Det;
                            txttipoEgreso.EnterMoveNextControl = true;

                            if (txttipoEgreso.Text == "01") // Cuando es Ingreso
                            {
                                btnBuscar.Enabled = true;
                                btnquitar.Enabled = true;
                            }
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipoEgreso.EnterMoveNextControl = false;
                    txttipoEgreso.ResetText();
                    txttipoEgreso.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        bool Verificar()
        {
            if (string.IsNullOrWhiteSpace(txttipoEgreso.Text))
            {
                Accion.Advertencia("Ingrese el tipo");
                return false;
            }
            if ((Convert.ToDecimal(txtmonto.Text)==0))
            {
                Accion.Advertencia("Ingrese un monto");
                return false;
            }
            if (string.IsNullOrWhiteSpace(txtdesceripcion.Text))
            {
                Accion.Advertencia("Ingrese una descripcion");
                return false;
            }
            if (txttipoEgreso.Tag.ToString() == "0049")//INGRESO
            {
                decimal total = 0;
                foreach (Entidad_Movimiento_Caja Item in DetallesCobrosCaja)
                {
                    total += Item.Amortizado;
                }

                if (DetallesCobrosCaja.Count > 0)
                {
                    if(total> Convert.ToDecimal(txtmonto.Text) || total< Convert.ToDecimal(txtmonto.Text))
                    {
                        Accion.Advertencia("La suma de los detalles no coincide con el total");
                        return false;
                    }
                }


            }
            return true;
        }

        private void btnverlista_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using(frm_ver_lista_egresos_ingresos f = new frm_ver_lista_egresos_ingresos())
                {

                    f.Pdv_Codigo = Pdv_Codigo;
                    f.Cja_Codigo = Cja_Codigo;
                    f.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;

                    if (f.ShowDialog() == DialogResult.OK)
                    {

                    }

                }

            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

       // List<Entidad_Movimiento_Cab> DetallesCobros = new List<Entidad_Movimiento_Cab>();
        List<Entidad_Movimiento_Caja> DetallesCobrosCaja = new List<Entidad_Movimiento_Caja>();


        private void btnBuscar_Click(object sender, EventArgs e)
        {
            using (frm_pendientes_por_pagar_pdv f = new frm_pendientes_por_pagar_pdv())
            {
                if (f.ShowDialog() == DialogResult.OK)
                {
                 

                    foreach (int i in f.gridView1.GetSelectedRows())
                    {
                        Entidad_Movimiento_Cab Doc = new Entidad_Movimiento_Cab();
                        Entidad_Movimiento_Caja Caja = new Entidad_Movimiento_Caja();

                        Doc = f.Lista[f.gridView1.GetDataSourceRowIndex(i)];
                        //Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                        Doc.Id_Item = (DetallesCobrosCaja.Count + 1);

                        Caja.Item = Doc.Id_Item;
                        Caja.Anio_Ref = Doc.Id_Anio;
                        Caja.Periodo_Ref = Doc.Id_Periodo;
                        Caja.Id_Tipo_Mov_Ref = Doc.Ctb_Tipo_Movimiento;
                        Caja.Id_Almacen_Ref =  Doc.Adm_Almacen;
                        Caja.Id_Movimiento_Ref = Doc.Id_Movimiento;
                        Caja.Es_Venta_Sin_Ticket = Doc.Es_Venta;
                        Caja.Ruc_DNI =  Doc.Ctb_Ruc_dni;
                        Caja.Total =  Doc.Saldo_Por_Amortizar;
                        Caja.TipoDoc = Doc.Ctb_Tipo_Doc;
                        Caja.TipoDocNombre = Doc.Nombre_Comprobante;
                        Caja.Serie = Doc.Ctb_Serie;
                        Caja.Numero = Doc.Ctb_Numero;
                        Caja.Amortizado = Doc.Saldo_Por_Amortizar;




                        if (ExistsInDetails(Caja.TipoDoc, Caja.Serie, Caja.Numero))
                        {
                            Accion.Advertencia("El Documento ya existe en la lista");
                            break;
                        }
                        //else if (TotalImporteDt > Convert.ToDecimal(txtmonto.Text))
                        //{
                        //    Accion.Advertencia("Verificación de detalle ,El Importe a Rendir sobrepasa el importe total a cobrar");
 
                        //    break;
                        //}
                        else
                        {
                            //DetallesCobros.Add(Doc);
                            DetallesCobrosCaja.Add(Caja);

                            dgvdatos.DataSource = null;
                            if (DetallesCobrosCaja.Count > 0)
                            {
                                dgvdatos.DataSource = DetallesCobrosCaja;

                            }

                            decimal TotalImporteDt = 0;

                            foreach (Entidad_Movimiento_Caja item in DetallesCobrosCaja)
                            {
                                TotalImporteDt += item.Amortizado;
                            }




                            txtmonto.Text = Convert.ToString(TotalImporteDt);

                        }
                    }
                }
            }
        }

        bool ExistsInDetails(  string TipDoc, string Serie, string Numero )
        {
            foreach (Entidad_Movimiento_Caja Item in DetallesCobrosCaja)
            {
                if (  (Item.TipoDoc == TipDoc) && (Item.Serie == Serie) && (Item.Numero == Numero)  )
                {
                    return true;
                }

            }

            return false;
        }

        private void txttipoEgreso_TextChanged(object sender, EventArgs e)
        {
            if (txttipoEgreso.Focused == false)
            {
                txttipoEgresodesc.ResetText();
                txtmonto.Text = "0.00";
               // TotalImporteDt = 0;
                dgvdatos.DataSource = null;
                btnquitar.Enabled = false;
                btnBuscar.Enabled = false;
                DetallesCobrosCaja.Clear();
            }
        }

     
        private void btnquitar_Click(object sender, EventArgs e)
        {
            try
            {
                if (DetallesCobrosCaja.Count > 0)
                {
                    decimal TotalImporte = 0;

                    DetallesCobrosCaja.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                    dgvdatos.DataSource = null;
                    if (DetallesCobrosCaja.Count > 0)
                    {
                        dgvdatos.DataSource = DetallesCobrosCaja;
                        //RefreshNumeral();
                    }

                    foreach (Entidad_Movimiento_Caja item in DetallesCobrosCaja)
                    {
                        TotalImporte += item.Amortizado;
                    }
                    txtmonto.Text = Convert.ToString(TotalImporte);

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoEgreso_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void frm_movimiento_caja_edicion_Load(object sender, EventArgs e)
        {

            btnBuscar.Enabled = false;
            btnquitar.Enabled = false;
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null) return;
            //if (e.Column.Caption != "FirstName") return;
            string Saldo =  view.GetRowCellValue(e.RowHandle, view.Columns["Total"]).ToString();//Saldo
            string Amortizar = view.GetRowCellValue(e.RowHandle, view.Columns["Amortizado"]).ToString();//Amortizar
            // view.SetRowCellValue(e.RowHandle, view.Columns["FullName"], cellValue);

            if (Convert.ToDecimal(Saldo)< Convert.ToDecimal(Amortizar))
            {
                Accion.Advertencia("Verifique el monto ingresado");
                decimal valor = 0;
                view.SetRowCellValue(e.RowHandle, view.Columns["Amortizado"], valor);
                return;
            }
            else
            {
                decimal TotalImporte = 0;
                foreach (Entidad_Movimiento_Caja item in DetallesCobrosCaja)
                {
                    TotalImporte += item.Amortizado;
                }
                txtmonto.Text = Convert.ToString(TotalImporte);

            }

        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Verificar())
                {
                    Entidad_Movimiento_Caja Ent = new Entidad_Movimiento_Caja();
                    Logica_Movimiento_Caja Log = new Logica_Movimiento_Caja();


                    Ent.Id_Empresa = Id_Empresa;
                    Ent.Pdv_Codigo = Pdv_Codigo;
                    Ent.Cja_Codigo = Cja_Codigo;
                    Ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;


                    Ent.Cja_Tipo = txttipoEgreso.Tag.ToString();
                    Ent.Cja_Monto = Convert.ToDecimal(txtmonto.Text);
                    Ent.Cja_Motivo = txtdesceripcion.Text.Trim();

                    Ent.DetalleCobroCaja = DetallesCobrosCaja;

                    if (Log.Insertar(Ent))
                    {
                        Accion.ExitoGuardar();
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }
    }
}
