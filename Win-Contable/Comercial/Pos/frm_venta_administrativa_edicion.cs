﻿using Comercial;
using Comercial.Busquedas_Generales;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Pos
{
    public partial class frm_venta_administrativa_edicion : frm_fuente

    {
        public frm_venta_administrativa_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Pdv_Codigo, Id_Movimiento;

        decimal MBase1 = 0, MValor_Fac_Export = 0;
        decimal MExonerada = 0, MInafecta, MIgv1 = 0;
        decimal MIsc = 0;

        int Adm_Item;

        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();


                Detalles.Clear();
                Dgvdetalles.DataSource = null;

                //TraerLibro();
                //BuscarMoneda_Inicial();
                ResetearImportes();

            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }
 
        public override void CambiandoEstadoDetalle2()
        {
            if (EstadoDetalle2 == Estados.Nuevo)
            {
                HabilitarDetalles2();
                LimpiarDet2();
                Adm_Item = Detalles.Count + 1;

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Ninguno)
            {
                LimpiarDet2();
                BloquearDetalles2();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Modificar)
            {

                HabilitarDetalles2();


                btnanadirdet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Guardado)
            {

                LimpiarDet2();
                BloquearDetalles2();
                btnanadirdet.Focus();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Consulta)
            {
                HabilitarDetalles2();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = false;

            }
            else if (EstadoDetalle2 == Estados.SoloLectura)
            {
                BloquearDetalles2();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = false;


            }
        }

        public void ResetearImportes()
        {
            txtbaseimponible.Text = ("0.00");
            txtvalorfactexport.Text = ("0.00");
            txtigv.Text = ("0.00");
            txtimporteisc.Text = ("0.00");
            txtotrostribuimporte.Text = ("0.00");
            txtexonerada.Text = ("0.00");
            txtinafecta.Text = ("0.00");
            txtimportetotal.Text = ("0.00");

        }
        public void LimpiarCab()
        {
            txtlibro.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();

            txtfechadoc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();

            txtpdvcod.ResetText();
            txtpdvdesc.ResetText();
            txtfechavencimiento.ResetText();




        }

        public void HabilitarDetalles2()
        {
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
          //  txtalmacencod.Enabled = true;
            txtcantidad.Enabled = true;
            txtvalorunit.Enabled = true;
            txtimporte.Enabled = true;
            txtunmcod.Enabled = true;
            //txttipooperacion.Enabled = true;
            //TxtAfectIGVCodigo.Enabled = true;
        }
        public void LimpiarDet2()
        {

            txtimporte.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtalmacencod.ResetText();
            txtalmacendesc.ResetText();
            txtvalorunit.ResetText();
            txtcantidad.ResetText();
            txtunmcod.ResetText();
            txtunmdesc.ResetText();
            TxtAfectIGVCodigo.ResetText();
            TxtAfectIGVDescripcion.ResetText();
            chklote.Checked = false;
            
        }

        public void HabilitarDetalles()
        {
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtimporte.Enabled = true;
           // txtalmacencod.Enabled = true;
            txtvalorunit.Enabled = true;
            txtcantidad.Enabled = true;
            txtunmcod.Enabled = true;
            //TxtAfectIGVCodigo.Enabled = true;
            txttipobsacod.Focus();
        }

        public bool VerificarNuevoDet()
        {
            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}


            return true;
        }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            if (VerificarNuevoDet())
            {
                EstadoDetalle2 = Estados.Nuevo;

                HabilitarDetalles();
                LimpiarDet2();
                Adm_Item = Detalles.Count + 1;

                txtvalorunit.Text = "0.00";
                txtimporte.Text = "0.00";
                txtcantidad.Text = "0.00";

                BuscarPuntoVenta_Almacen();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

                txtproductocod.Select();
            }
        }

        decimal MOtrosTributos = 0, MImporteTotal = 0;

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle2 = Estados.Modificar;
            txttipobsacod.Focus();
        }


        public bool VerificarDetalle()
        {

            // if (Convert.ToDouble(txtimporte.Text) <= 0)
            //{
            // Accion.Advertencia("Debe ingresar un importe mayor a cero");
            //  txtimporte.Focus();
            //  return false;
            // }
            //  if (string.IsNullOrEmpty(txttipooperaciondesc.Text.Trim()))
            // {
            // Accion.Advertencia("Debe ingresar un importe mayor a cero");
            //  txtimporte.Focus();
            //  return false;
            // }


            return true;
        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarDetalle())
                {
                    Entidad_Movimiento_Cab ItemDetalle = new Entidad_Movimiento_Cab();

                    ItemDetalle.Adm_Item = Adm_Item;

                    if (txttipobsacod.Tag.ToString() == null)
                    {
                        ItemDetalle.Adm_Tipo_BSA = "";
                    }
                    else
                    {
                        ItemDetalle.Adm_Tipo_BSA = txttipobsacod.Tag.ToString();
                    }

                    ItemDetalle.Adm_Tipo_BSA_Interno = txttipobsacod.Text;
                    ItemDetalle.Adm_Tipo_BSA_Desc = txttipobsadesc.Text;

                    ItemDetalle.Adm_Catalogo = txtproductocod.Text.Trim();
                    ItemDetalle.Adm_Catalogo_Desc = txtproductodesc.Text;

                    ItemDetalle.Adm_Unm_Id = txtunmcod.Text.Trim();
                    ItemDetalle.Adm_Unm_Desc = txtunmdesc.Text.Trim();



                    ItemDetalle.Adm_Almacen = txtalmacencod.Text;
                    ItemDetalle.Adm_Almacen_desc = txtalmacendesc.Text;

                    ItemDetalle.Adm_Cantidad = Convert.ToDecimal(txtcantidad.Text);
                    ItemDetalle.Adm_Valor_Unit = Convert.ToDecimal(txtvalorunit.Text);

                    ItemDetalle.Adm_Total = Convert.ToDecimal(txtimporte.Text);

                    ItemDetalle.Acepta_lotes = chklote.Checked;

                    //ItemDetalle.Adm_Marca = codMarca;
                   // ItemDetalle.Adm_Marca_Desc = desMarca;



                    if (txttipooperacion.Tag.ToString() == null)
                    {
                        ItemDetalle.Adm_Tipo_Operacion_Interno = "";
                    }
                    else
                    {
                        ItemDetalle.Adm_Tipo_Operacion = txttipooperacion.Tag.ToString();
                       // ItemDetalle.DvtD_OperCodigo = txttipooperacion.Tag.ToString();
                    }

                    ItemDetalle.Adm_Tipo_Operacion_Interno = txttipooperacion.Text.Trim();
                    ItemDetalle.Adm_Tipo_Operacion_Desc = txttipooperaciondesc.Text.Trim();

                    ItemDetalle.Ven_Afecto_IGV_FE = TxtAfectIGVCodigo.Text ;
                    ItemDetalle.Ven_Afecto_IGV_FE_Desc= TxtAfectIGVDescripcion.Text  ;


                    if (EstadoDetalle2 == Estados.Nuevo)
                    {
                        Detalles.Add(ItemDetalle);
                        UpdateGrilla();
                        EstadoDetalle2 = Estados.Guardado;
                    }
                    else if (EstadoDetalle2 == Estados.Modificar)
                    {

                        Detalles[Convert.ToInt32(Adm_Item) - 1] = ItemDetalle;

                        UpdateGrilla();
                        EstadoDetalle2 = Estados.Guardado;

                    }

                    btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void UpdateGrilla()
        {
            Dgvdetalles.DataSource = null;
            if (Detalles.Count > 0)
            {
                Dgvdetalles.DataSource = Detalles;
            }

            MostrarResumen();
        }

        void MostrarResumen()
        {
            decimal MBase1 = 0, MValor_Fac_Export = 0;
            decimal MExonerada = 0, MInafecta = 0, MIGV = 0;
            decimal MISC = 0;
            decimal MOtrosTributos = 0, MImporteTotal = 0;
            decimal TotalOpgravada = 0;
      
                foreach (Entidad_Movimiento_Cab T in Detalles)
                {
                    MValor_Fac_Export += T.DvtD_ValorFactExportacion;
                   // MBase1 += T.DvtD_BaseImpobleOperacionGravada;//Math.Round((T.Adm_Total / (Igv_Porcentaje + 1)),4);
                    MExonerada += T.DvtD_Exonerada;
                    MInafecta += T.DvtD_Inafecta;
                    MISC += T.DvtD_ISC;
                   // MIGV += T.DvtD_IGV_IPM;

                if (T.Adm_Tipo_Operacion == "0565") {
                    MBase1 += Math.Round((T.Adm_Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + 1)), 2);
                    MIGV += Math.Round((T.Adm_Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + 1)) * Convert.ToDecimal(txtigvporcentaje.Tag), 2);

                }


            }


                MImporteTotal = (MValor_Fac_Export + MBase1 + MExonerada + MInafecta + MISC + MIGV);
                txtvalorfactexport.Text = String.Format("{0:0,0.00}", Math.Round(MValor_Fac_Export, 2));
                txtbaseimponible.Text = String.Format("{0:0,0.00}", Math.Round(MBase1, 2));
                txtexonerada.Text = String.Format("{0:0,0.00}", Math.Round(MExonerada, 2));
                txtinafecta.Text = String.Format("{0:0,0.00}", Math.Round(MInafecta, 2));
                txtimporteisc.Text = String.Format("{0:0,0.00}", Math.Round(MISC, 2));
                txtigv.Text = String.Format("{0:0,0.00}", Math.Round(MIGV, 2));
                txtimportetotal.Text = String.Format("{0:0,0.00}", Math.Round(MImporteTotal, 2));

   

        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipobsacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;
                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.ResetText();
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        string codMarca;
        string desMarca;

        private void txttipobsacod_TextChanged(object sender, EventArgs e)
        {
            if (txttipobsacod.Focus() == false)
            {
                txttipobsadesc.ResetText();
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                       

                        using (frm_catalogo f = new frm_catalogo())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();
                            //f.Desde_Catalogo = "2";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;

                                txtunmcod.Text = Entidad.Id_Unidad_Medida;
                                txtunmdesc.Text = Entidad.Und_Descripcion;
                                txtunmabrev.Text = Entidad.Und_Abreviado;


                                codMarca = Entidad.Id_Marca.Trim();
                                desMarca = Entidad.Mar_Descripcion.Trim();

                                chklote.Checked = Entidad.Acepta_Lote;

                          

                                txtproductocod.EnterMoveNextControl = true;

                                BuscarUnm_Defecto_Producto();

                                Logica_Catalogo logi = new Logica_Catalogo();
                                List<Entidad_Catalogo> ListaOperacion_Venta = new List<Entidad_Catalogo>();

                                ListaOperacion_Venta = logi.Listar_Operacion_venta(Entidad);

                                if (ListaOperacion_Venta.Count > 0)
                                {
                                    TxtAfectIGVCodigo.Text = ListaOperacion_Venta[0].Id_Afecto_Venta.Trim();
                                    TxtAfectIGVDescripcion.Text = ListaOperacion_Venta[0].Afecto_Venta_Descripcion.Trim();
                          
                                    BuscarOperacionVenta();
                                }

                            }
                        }

                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                        BuscarUnm_Defecto_Producto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {
                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;

                            txtunmcod.Text = T.Id_Unidad_Medida;
                            txtunmdesc.Text = T.Und_Descripcion;
                            txtunmabrev.Text = T.Und_Abreviado;
                            chklote.Checked = T.Acepta_Lote;

                            codMarca = T.Id_Marca.Trim();
                            desMarca = T.Mar_Descripcion.Trim();


                            Logica_Catalogo logi = new Logica_Catalogo();
                            List<Entidad_Catalogo> ListaOperacion_Venta = new List<Entidad_Catalogo>();

                            ListaOperacion_Venta = logi.Listar_Operacion_venta(T);

                            if (ListaOperacion_Venta.Count > 0)
                            {
                                TxtAfectIGVCodigo.Text = ListaOperacion_Venta[0].Id_Afecto_Venta.Trim();
                                TxtAfectIGVDescripcion.Text = ListaOperacion_Venta[0].Afecto_Venta_Descripcion.Trim();

                                BuscarOperacionVenta();
                            }

                            txtproductocod.EnterMoveNextControl = true;
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.ResetText();
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

     

        void BuscarUnm_Defecto_Producto()
        {
            try
            {

                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Catalogo = txtproductocod.Text.Trim(),
                    Unm_Defecto = true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Unm_Defecto) == true)
                        {
                            txtunmcod.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdesc.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtunmcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtunmcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunmcod.Text.Substring(txtunmcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_unidad_medida_busqueda f = new frm_unidad_medida_busqueda())
                        {
                            f.Producto_Cod = txtproductocod.Text;
                            f.Producto_Unm = true;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunmcod.Text = Entidad.Id_Unidad_Medida;
                                txtunmdesc.Text = Entidad.Und_Descripcion;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtunmcod.Text) & string.IsNullOrEmpty(txtunmdesc.Text))
                    {
                        BuscarUnmPres();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarUnmPres()
        {
            try
            {
                txtunmcod.Text = Accion.Formato(txtunmcod.Text, 3);
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Unidad_Medida = txtunmcod.Text,
                    Id_Catalogo = txtproductocod.Text.Trim()
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Id_Unidad_Medida).ToString().Trim().ToUpper() == txtunmcod.Text.Trim().ToUpper())
                        {
                            txtunmcod.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdesc.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtAfectIGVCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(TxtAfectIGVCodigo.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & TxtAfectIGVCodigo.Text.Substring(TxtAfectIGVCodigo.Text.Length - 1, 1) == "*")
                    {
                        using (frm_busqueda_operacion_venta f = new frm_busqueda_operacion_venta())
                        {
                            //f.unmcod = txtunmcod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Operaciones Entidad = new Entidad_Operaciones();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                TxtAfectIGVCodigo.Text = Entidad.AfecIGV_Codigo;
                                TxtAfectIGVDescripcion.Text = Entidad.AfecIGV_Descripcion;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(TxtAfectIGVCodigo.Text) & string.IsNullOrEmpty(TxtAfectIGVDescripcion.Text))
                    {
                        BuscarOperacionVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarOperacionVenta()
        {
            try
            {
                TxtAfectIGVCodigo.Text = Accion.Formato(TxtAfectIGVCodigo.Text, 2);
                Logica_Operaciones log = new Logica_Operaciones();

                List<Entidad_Operaciones> Generales = new List<Entidad_Operaciones>();
                Generales = log.Listar_Operaciones_Venta(new Entidad_Operaciones
                {
                    AfecIGV_Codigo = TxtAfectIGVCodigo.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Operaciones T in Generales)
                    {
                        if ((T.AfecIGV_Codigo).ToString().Trim().ToUpper() == TxtAfectIGVCodigo.Text.Trim().ToUpper())
                        {
                            TxtAfectIGVCodigo.Text = (T.AfecIGV_Codigo).ToString().Trim();
                            TxtAfectIGVDescripcion.Text = T.AfecIGV_Descripcion;
 
                            txttipooperacion.Tag = T.AfecIGV_Tabla;
                            txttipooperacion.Text = T.Gen_Codigo_Interno;
                            txttipooperaciondesc.Text = T.Gen_Descripcion_Det;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }




        private void gridView2_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView2.GetFocusedDataSourceRowIndex()];

                    Adm_Item = Entidad.Adm_Item;

                    txttipobsacod.Tag = Entidad.Adm_Tipo_BSA;
                    txttipobsacod.Text = Entidad.Adm_Tipo_BSA_Interno;
                    txttipobsadesc.Text = Entidad.Adm_Tipo_BSA_Desc;

                    txtunmcod.Text = Entidad.Adm_Unm_Id;
                    txtunmdesc.Text = Entidad.Adm_Unm_Desc;


                    txtproductocod.Text = Entidad.Adm_Catalogo;
                    txtproductodesc.Text = Entidad.Adm_Catalogo_Desc;


                    txtalmacencod.Text = Entidad.Adm_Almacen;
                    txtalmacendesc.Text = Entidad.Adm_Almacen_desc;

                    txtcantidad.Text = Entidad.Adm_Cantidad.ToString();
                    txtvalorunit.Text = Entidad.Adm_Valor_Unit.ToString();
                    txtimporte.Text = Entidad.Adm_Total.ToString();

                    chklote.Checked = Entidad.Acepta_lotes;

                    //codMarca = Entidad.Adm_Marca;
                    // desMarca = Entidad.Adm_Marca_Desc;
                    TxtAfectIGVCodigo.Text = Entidad.Ven_Afecto_IGV_FE;
                    TxtAfectIGVDescripcion.Text = Entidad.Ven_Afecto_IGV_FE_Desc;

                    txttipooperacion.Tag = Entidad.Adm_Tipo_Operacion;
                    txttipooperacion.Text = Entidad.Adm_Tipo_Operacion_Interno;
                    txttipooperaciondesc.Text = Entidad.Adm_Tipo_Operacion_Desc;



                   
                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle2 = Estados.Consulta;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle2 = Estados.SoloLectura;
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool Es_moneda_nac;
        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcondiciondesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una condicion");
                txtcondicioncod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtanalisisdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una tipo de analisis");
                txtidanalisis.Focus();
                return false;
            }

   


            return true;
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Id_Periodo;
                    Ent.ctb_pdv_codigo = txtpdvcod.Text.Trim();
                    Ent.Id_Movimiento = Id_Movimiento;

                    Ent.Ctb_Glosa = txtglosa.Text;
                    Ent.Ctb_Tipo_Doc = txttipodoc.Tag.ToString();

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Ctb_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Ctb_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent = "C";
                    Ent.Ctb_Ruc_dni = txtrucdni.Text;
                    Ent.Ctb_Condicion_Cod = txtcondicioncod.Tag.ToString();
                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;

                    Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                    Ent.Ctb_Base_Imponible = Convert.ToDecimal(txtbaseimponible.Text);
                    Ent.Ctb_Valor_Fact_Exonerada = Convert.ToDecimal(txtvalorfactexport.Text);
                    Ent.Ctb_Exonerada = Convert.ToDecimal(txtexonerada.Text);
                    Ent.Ctb_Inafecta = Convert.ToDecimal(txtinafecta.Text);

                    Ent.Ctb_Isc = chkisc.Checked;
                    Ent.Ctb_Isc_Importe = Convert.ToDecimal(txtimporteisc.Text);

                    Ent.Ctb_Igv = Convert.ToDecimal(txtigv.Text);

                    Ent.Ctb_Otros_Tributos = chkotrostri.Checked;
                    Ent.Ctb_Otros_Tributos_Importe = Convert.ToDecimal(txtotrostribuimporte.Text);

                    Ent.Ctb_Importe_Total = Convert.ToDecimal(txtimportetotal.Text);


                    if (txtfechavencimiento.Text == "" || txtfechavencimiento.Text == "01/01/1900")
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime("01/01/1900");
                    }
                    else
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime(txtfechavencimiento.Text);
                    }


         



                    Ent.Ctb_Tasa_IGV = Convert.ToDecimal(txtigvporcentaje.Text);
                    Ent.Ctb_Analisis = txtidanalisis.Text.Trim();


                    //detalle Contable
                  //  Ent.DetalleAsiento = Detalles_CONT;
                    //Detalle Administrativo
                    Ent.DetalleADM = Detalles;
                    Ent.DetalleLotes = DetalleLotes;


                    //0020    01  BASE IMPONIBLE 01
                    //0021    02  IGV 01
                    //0022    03  BASE IMPONIBLE 02
                    //0025    04  IGV 02
                    //0026    05  BASE IMPONIBLE 03
                    //0036    06  IGV 03
                    //0037    07  VALOR DE ADQUISICION NO GRABADA
                    //0038    08  IMPUESTO SELECTIVO AL CONSUMO
                    //0039    09  OTROS TRIBUTOS Y CARGOS
                    //0040    10  IMPORTE TOTAL

                    //Calcular(Detalles, Ent);

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar_adm_Ventas(Ent))
                        {
 
                            //Accion.ExitoGuardar();
                            //Estado = Estados.Nuevo;
                            //LimpiarCab();

                            //LimpiarDet2();
                            //Detalles_ADM.Clear();
                            //Detalles_CONT.Clear();

                            //Detalles_Orden_Compra.Clear();
                            //Lista_Doc_ref_Lista.Clear();


                            //dgvdatosContable.DataSource = null;

                            //BloquearDetalles2();
                            //TraerLibro();
                            //BuscarMoneda_Inicial();
                            //ResetearImportes();
                            //txtglosa.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar_adm_Ventas(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();
        List<Entidad_Movimiento_Cab> DetalleLotes = new List<Entidad_Movimiento_Cab>();

        public void BuscarPuntoVenta_Almacen()
        {
            try
            {
                Entidad_Punto_Venta ent = new Entidad_Punto_Venta();
                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Punto_Venta_PC = new List<Entidad_Punto_Venta>();

                Punto_Venta_PC = log.Listar_Punto_Venta_Almacen(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Pdv_Codigo = txtpdvcod.Text.Trim()
                });

                if (Punto_Venta_PC.Count > 0)
                {
                    txtalmacencod.Text = Punto_Venta_PC[0].Pdv_Almacen.Trim();
                    txtalmacendesc.Text = Punto_Venta_PC[0].Alm_Descrcipcion.Trim();
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BloquearDetalles2()
        {
            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtalmacencod.Enabled = false;
            txtcantidad.Enabled = false;
            txtvalorunit.Enabled = false;
            txtimporte.Enabled = false;
            txtunmcod.Enabled = false;
            txttipooperacion.Enabled = false;
            TxtAfectIGVCodigo.Enabled = false;
        }

        private void frm_venta_administrativa_edicion_Load(object sender, EventArgs e)
        {

            if (Estado_Ven_Boton == "1")
            {
                //Estado = Estados.Nuevo;
                //EstadoDetalle2 = Estados.Ninguno;

                //LimpiarCab();
                //Detalles_CONT.Clear();
                //dgvdatosContable.DataSource = null;
                //TraerLibro();
                //BuscarMoneda_Inicial();
                //ResetearImportes();
                //BloquearDetalles2();
                //txtglosa.Select();
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
              
                //TraerLibro();
                BloquearDetalles2();
                ListarModificar();
                ActualizaIGV();
            }
        }


        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(txtfechadoc.Text) == true))
                {
                    if ((DateTime.Parse(txtfechadoc.Text).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(txtfechadoc.Text);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            txtigvporcentaje.Tag = Lista[0].Imd_Tasa;
                            txtigvporcentaje.Text = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));
                            //String.Format("{0:yyyy-MM-dd}", Enti.Ord_Fecha)
                            //string igv = string.Format("{0:0.##}", txtigv.Text).ToString();
                            //txtigv.Text = igv;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.ctb_pdv_codigo = Id_Pdv_Codigo;
            Ent.Id_Movimiento = Id_Movimiento;
            try
            {
                Lista_Modificar = log.Listar_(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Pdv_Codigo = Enti.ctb_pdv_codigo;
                    Id_Movimiento = Enti.Id_Movimiento;

                    txtpdvcod.Text = Enti.ctb_pdv_codigo.Trim();
                    txtpdvdesc.Text = Enti.ctb_pdv_Nombre.Trim();

                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;

 

                    txtserie.Text = Enti.Ctb_Serie + "-" + Enti.Ctb_Numero;
 
                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));
                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtcondicioncod.Tag = Enti.Ctb_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ctb_Condicion_Interno;
                    txtcondiciondesc.Text = Enti.Ctb_Condicion_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;


                    MBase1 = Enti.Ctb_Base_Imponible;
                    MValor_Fac_Export = Enti.Ctb_Valor_Fact_Exonerada;
                    MIgv1 = Enti.Ctb_Igv;
                    MIsc = Enti.Ctb_Isc_Importe;
                    MOtrosTributos = Enti.Ctb_Otros_Tributos_Importe;
                    MExonerada = Enti.Ctb_Exonerada;
                    MInafecta = Enti.Ctb_Inafecta;
                    MImporteTotal = Enti.Ctb_Importe_Total;

                    //  { 0:00.00}         ", value)
                    txtbaseimponible.Text = Convert.ToDecimal(MBase1).ToString("0.00");
                    txtvalorfactexport.Text = Convert.ToDecimal(MValor_Fac_Export).ToString("0.00");
                    txtigv.Text = Convert.ToDecimal(MIgv1).ToString("0.00");
                    txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");
                    txtotrostribuimporte.Text = Convert.ToDecimal(MOtrosTributos).ToString("0.00");
                    txtexonerada.Text = Convert.ToDecimal(MExonerada).ToString("0.00");
                    txtinafecta.Text = Convert.ToDecimal(MInafecta).ToString("0.00");
                    txtimportetotal.Text = Convert.ToDecimal(MImporteTotal).ToString("0.00");


                  
                    txtidanalisis.Text = Enti.Ctb_Analisis;
                    txtanalisisdesc.Text = Enti.Ctb_Analisis_Desc;

                    if (String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/0001" || String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/1900")
                    {
                        txtfechavencimiento.Text = "";
                    }
                    else
                    {
                        txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Vencimiento);
                    }


                    //listar cnotable
                    Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                    Dgvdetalles.DataSource = null;
                    Detalles = log_det_con.Listar_Det_(Enti);

                    if (Detalles.Count > 0)
                    {

                        Dgvdetalles.DataSource = Detalles;


                    }


                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

    }
}
