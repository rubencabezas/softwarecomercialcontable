﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_valida_cod_autorizacion : frm_fuente
    {
        public frm_valida_cod_autorizacion()
        {
            InitializeComponent();
        }

        private void btnaceptar_Click(object sender, EventArgs e)
        {
            try
            {
                   if (txtcodautoriza.Text=="")
                    {
                        Accion.ErrorSistema("Debe ingresar la clave");
                    }
                    else
                    {
                          DialogResult = DialogResult.OK;
                    }
            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
 

   
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
