﻿namespace Comercial
{
    partial class frm_cobrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gbtarjeta = new System.Windows.Forms.GroupBox();
            this.TxtTarjeta = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtEntrega = new DevExpress.XtraEditors.TextEdit();
            this.Label3 = new System.Windows.Forms.Label();
            this.TxtImporteTotal = new System.Windows.Forms.Label();
            this.TxtEfecTar = new System.Windows.Forms.Label();
            this.TxtAdevolver = new System.Windows.Forms.Label();
            this.LblVuelto = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.BtnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.CheckBtnTipoTarjeta = new DevExpress.XtraEditors.CheckButton();
            this.CheckBtnTipoContado = new DevExpress.XtraEditors.CheckButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.Gbtarjeta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTarjeta.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntrega.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Gbtarjeta
            // 
            this.Gbtarjeta.Controls.Add(this.TxtTarjeta);
            this.Gbtarjeta.Controls.Add(this.label1);
            this.Gbtarjeta.Location = new System.Drawing.Point(4, 63);
            this.Gbtarjeta.Name = "Gbtarjeta";
            this.Gbtarjeta.Size = new System.Drawing.Size(255, 161);
            this.Gbtarjeta.TabIndex = 2;
            this.Gbtarjeta.TabStop = false;
            this.Gbtarjeta.Text = "Tarjeta";
            // 
            // TxtTarjeta
            // 
            this.TxtTarjeta.EditValue = "0.00";
            this.TxtTarjeta.Location = new System.Drawing.Point(69, 33);
            this.TxtTarjeta.Name = "TxtTarjeta";
            this.TxtTarjeta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTarjeta.Properties.Appearance.Options.UseFont = true;
            this.TxtTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtTarjeta.Size = new System.Drawing.Size(165, 30);
            this.TxtTarjeta.TabIndex = 40;
            this.TxtTarjeta.Leave += new System.EventHandler(this.TxtTarjeta_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(2, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tarjeta:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TxtEntrega);
            this.groupBox2.Controls.Add(this.Label3);
            this.groupBox2.Controls.Add(this.TxtImporteTotal);
            this.groupBox2.Controls.Add(this.TxtEfecTar);
            this.groupBox2.Controls.Add(this.TxtAdevolver);
            this.groupBox2.Controls.Add(this.LblVuelto);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.Label5);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(265, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(248, 163);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "EFECTIVO";
            // 
            // TxtEntrega
            // 
            this.TxtEntrega.EditValue = "0.00";
            this.TxtEntrega.EnterMoveNextControl = true;
            this.TxtEntrega.Location = new System.Drawing.Point(88, 55);
            this.TxtEntrega.Name = "TxtEntrega";
            this.TxtEntrega.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntrega.Properties.Appearance.Options.UseFont = true;
            this.TxtEntrega.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TxtEntrega.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.TxtEntrega.Properties.Mask.EditMask = "n2";
            this.TxtEntrega.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TxtEntrega.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtEntrega.Size = new System.Drawing.Size(152, 30);
            this.TxtEntrega.TabIndex = 3;
            this.TxtEntrega.TextChanged += new System.EventHandler(this.TxtEntrega_TextChanged);
            this.TxtEntrega.Validating += new System.ComponentModel.CancelEventHandler(this.TxtEntrega_Validating);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.Black;
            this.Label3.Location = new System.Drawing.Point(18, 98);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(56, 19);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "E + T:";
            // 
            // TxtImporteTotal
            // 
            this.TxtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtImporteTotal.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.TxtImporteTotal.Location = new System.Drawing.Point(88, 18);
            this.TxtImporteTotal.Name = "TxtImporteTotal";
            this.TxtImporteTotal.Size = new System.Drawing.Size(152, 27);
            this.TxtImporteTotal.TabIndex = 1;
            this.TxtImporteTotal.Text = "0.00";
            this.TxtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEfecTar
            // 
            this.TxtEfecTar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtEfecTar.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.TxtEfecTar.Location = new System.Drawing.Point(88, 93);
            this.TxtEfecTar.Name = "TxtEfecTar";
            this.TxtEfecTar.Size = new System.Drawing.Size(152, 27);
            this.TxtEfecTar.TabIndex = 5;
            this.TxtEfecTar.Text = "0.00";
            this.TxtEfecTar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAdevolver
            // 
            this.TxtAdevolver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtAdevolver.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.TxtAdevolver.Location = new System.Drawing.Point(88, 129);
            this.TxtAdevolver.Name = "TxtAdevolver";
            this.TxtAdevolver.Size = new System.Drawing.Size(152, 27);
            this.TxtAdevolver.TabIndex = 7;
            this.TxtAdevolver.Text = "0.00";
            this.TxtAdevolver.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblVuelto
            // 
            this.LblVuelto.AutoSize = true;
            this.LblVuelto.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVuelto.ForeColor = System.Drawing.Color.Black;
            this.LblVuelto.Location = new System.Drawing.Point(14, 134);
            this.LblVuelto.Name = "LblVuelto";
            this.LblVuelto.Size = new System.Drawing.Size(60, 19);
            this.LblVuelto.TabIndex = 6;
            this.LblVuelto.Text = "Vuelto:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 19);
            this.label4.TabIndex = 2;
            this.label4.Text = "Efectivo:";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.Color.Black;
            this.Label5.Location = new System.Drawing.Point(23, 23);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(51, 19);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "Total:";
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardar.Appearance.Options.UseFont = true;
            this.BtnGuardar.Location = new System.Drawing.Point(4, 230);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(504, 72);
            this.BtnGuardar.TabIndex = 8;
            this.BtnGuardar.Text = "CONFIRMAR";
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // CheckBtnTipoTarjeta
            // 
            this.CheckBtnTipoTarjeta.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBtnTipoTarjeta.Appearance.Options.UseFont = true;
            this.CheckBtnTipoTarjeta.GroupIndex = 2;
            this.CheckBtnTipoTarjeta.Location = new System.Drawing.Point(146, 8);
            this.CheckBtnTipoTarjeta.Name = "CheckBtnTipoTarjeta";
            this.CheckBtnTipoTarjeta.Size = new System.Drawing.Size(111, 51);
            this.CheckBtnTipoTarjeta.TabIndex = 43;
            this.CheckBtnTipoTarjeta.TabStop = false;
            this.CheckBtnTipoTarjeta.Text = "[T]\r\nTarjeta";
            this.CheckBtnTipoTarjeta.CheckedChanged += new System.EventHandler(this.CheckBtnTipoTarjeta_CheckedChanged);
            // 
            // CheckBtnTipoContado
            // 
            this.CheckBtnTipoContado.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBtnTipoContado.Appearance.Options.UseFont = true;
            this.CheckBtnTipoContado.Checked = true;
            this.CheckBtnTipoContado.GroupIndex = 2;
            this.CheckBtnTipoContado.Location = new System.Drawing.Point(23, 7);
            this.CheckBtnTipoContado.Name = "CheckBtnTipoContado";
            this.CheckBtnTipoContado.Size = new System.Drawing.Size(111, 51);
            this.CheckBtnTipoContado.TabIndex = 42;
            this.CheckBtnTipoContado.Text = "[C]\r\nContado";
            this.CheckBtnTipoContado.CheckedChanged += new System.EventHandler(this.CheckBtnTipoContado_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtcondiciondesc);
            this.groupBox1.Controls.Add(this.txtcondicioncod);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(263, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 55);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Condición";
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(50, 20);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtcondiciondesc.Properties.Appearance.Options.UseFont = true;
            this.txtcondiciondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondiciondesc.Size = new System.Drawing.Size(192, 30);
            this.txtcondiciondesc.TabIndex = 30;
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(11, 20);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtcondicioncod.Properties.Appearance.Options.UseFont = true;
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(38, 30);
            this.txtcondicioncod.TabIndex = 29;
            this.txtcondicioncod.TextChanged += new System.EventHandler(this.txtcondicioncod_TextChanged);
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // frm_cobrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(520, 310);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CheckBtnTipoTarjeta);
            this.Controls.Add(this.CheckBtnTipoContado);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Gbtarjeta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_cobrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cobrar";
            this.Load += new System.EventHandler(this.frm_cobrar_Load);
            this.Gbtarjeta.ResumeLayout(false);
            this.Gbtarjeta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTarjeta.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntrega.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Gbtarjeta;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label LblVuelto;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label Label5;
        internal DevExpress.XtraEditors.SimpleButton BtnGuardar;
        public DevExpress.XtraEditors.CheckButton CheckBtnTipoTarjeta;
        internal DevExpress.XtraEditors.CheckButton CheckBtnTipoContado;
        public DevExpress.XtraEditors.TextEdit TxtTarjeta;
        public DevExpress.XtraEditors.TextEdit TxtEntrega;
        public System.Windows.Forms.Label TxtImporteTotal;
        public System.Windows.Forms.Label TxtEfecTar;
        public System.Windows.Forms.Label TxtAdevolver;
        private System.Windows.Forms.GroupBox groupBox1;
        public DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        public DevExpress.XtraEditors.TextEdit txtcondicioncod;
    }
}
