﻿namespace Comercial
{
    partial class frm_ingresar_cantidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtItPrecUnit = new System.Windows.Forms.Label();
            this.LblTotalStock = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblunidadmedida = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.LblDescripcionProducto = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.lblimporte = new System.Windows.Forms.Label();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtMonto = new DevExpress.XtraEditors.TextEdit();
            this.TxtCantidad = new DevExpress.XtraEditors.TextEdit();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.BtnAceptar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtprecunitario = new DevExpress.XtraEditors.TextEdit();
            this.txtprecfinal = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtprecmaximo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtprecigv = new DevExpress.XtraEditors.TextEdit();
            this.txtprecminimo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btncodAutoriza = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMonto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCantidad.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecunitario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecfinal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecmaximo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecminimo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtItPrecUnit);
            this.groupBox1.Controls.Add(this.LblTotalStock);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblunidadmedida);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Controls.Add(this.LblDescripcionProducto);
            this.groupBox1.Controls.Add(this.lbl0);
            this.groupBox1.Location = new System.Drawing.Point(3, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(716, 78);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // TxtItPrecUnit
            // 
            this.TxtItPrecUnit.AutoSize = true;
            this.TxtItPrecUnit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItPrecUnit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.TxtItPrecUnit.Location = new System.Drawing.Point(452, 53);
            this.TxtItPrecUnit.Name = "TxtItPrecUnit";
            this.TxtItPrecUnit.Size = new System.Drawing.Size(36, 16);
            this.TxtItPrecUnit.TabIndex = 5;
            this.TxtItPrecUnit.Text = "0.00";
            this.TxtItPrecUnit.Visible = false;
            // 
            // LblTotalStock
            // 
            this.LblTotalStock.AutoSize = true;
            this.LblTotalStock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalStock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.LblTotalStock.Location = new System.Drawing.Point(101, 53);
            this.LblTotalStock.Name = "LblTotalStock";
            this.LblTotalStock.Size = new System.Drawing.Size(36, 16);
            this.LblTotalStock.TabIndex = 5;
            this.LblTotalStock.Text = "0.00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(287, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Precio Unitario:";
            this.label1.Visible = false;
            // 
            // lblunidadmedida
            // 
            this.lblunidadmedida.AutoSize = true;
            this.lblunidadmedida.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblunidadmedida.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblunidadmedida.Location = new System.Drawing.Point(140, 36);
            this.lblunidadmedida.Name = "lblunidadmedida";
            this.lblunidadmedida.Size = new System.Drawing.Size(126, 16);
            this.lblunidadmedida.TabIndex = 4;
            this.lblunidadmedida.Text = "Unidad de Medida:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(8, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Unidad de Medida:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Label3.Location = new System.Drawing.Point(10, 53);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(85, 16);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Total Stock:";
            this.Label3.Visible = false;
            // 
            // LblDescripcionProducto
            // 
            this.LblDescripcionProducto.AutoSize = true;
            this.LblDescripcionProducto.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDescripcionProducto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(90)))), ((int)(((byte)(156)))));
            this.LblDescripcionProducto.Location = new System.Drawing.Point(183, 17);
            this.LblDescripcionProducto.Name = "LblDescripcionProducto";
            this.LblDescripcionProducto.Size = new System.Drawing.Size(166, 18);
            this.LblDescripcionProducto.TabIndex = 2;
            this.LblDescripcionProducto.Text = "Descripción producto";
            // 
            // lbl0
            // 
            this.lbl0.AutoSize = true;
            this.lbl0.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl0.Location = new System.Drawing.Point(6, 17);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(171, 18);
            this.lbl0.TabIndex = 1;
            this.lbl0.Text = "Descripción producto:";
            // 
            // lblimporte
            // 
            this.lblimporte.AutoSize = true;
            this.lblimporte.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblimporte.Location = new System.Drawing.Point(80, 67);
            this.lblimporte.Name = "lblimporte";
            this.lblimporte.Size = new System.Drawing.Size(65, 16);
            this.lblimporte.TabIndex = 2;
            this.lblimporte.Text = "Importe:";
            this.lblimporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.Location = new System.Drawing.Point(5, 31);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(130, 16);
            this.lblCantidad.TabIndex = 0;
            this.lblCantidad.Text = "Ingresar Cantidad:";
            this.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TxtMonto);
            this.groupBox2.Controls.Add(this.TxtCantidad);
            this.groupBox2.Controls.Add(this.lblCantidad);
            this.groupBox2.Controls.Add(this.lblimporte);
            this.groupBox2.Location = new System.Drawing.Point(214, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(302, 89);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // TxtMonto
            // 
            this.TxtMonto.EditValue = "0.00";
            this.TxtMonto.EnterMoveNextControl = true;
            this.TxtMonto.Location = new System.Drawing.Point(156, 59);
            this.TxtMonto.Name = "TxtMonto";
            this.TxtMonto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMonto.Properties.Appearance.Options.UseFont = true;
            this.TxtMonto.Properties.ReadOnly = true;
            this.TxtMonto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtMonto.Size = new System.Drawing.Size(127, 24);
            this.TxtMonto.TabIndex = 3;
            this.TxtMonto.TextChanged += new System.EventHandler(this.TxtMonto_TextChanged);
            // 
            // TxtCantidad
            // 
            this.TxtCantidad.EditValue = "0.00";
            this.TxtCantidad.EnterMoveNextControl = true;
            this.TxtCantidad.Location = new System.Drawing.Point(156, 23);
            this.TxtCantidad.Name = "TxtCantidad";
            this.TxtCantidad.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCantidad.Properties.Appearance.Options.UseFont = true;
            this.TxtCantidad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCantidad.Size = new System.Drawing.Size(127, 24);
            this.TxtCantidad.TabIndex = 1;
            this.TxtCantidad.TextChanged += new System.EventHandler(this.TxtCantidad_TextChanged);
            this.TxtCantidad.Leave += new System.EventHandler(this.TxtCantidad_Leave);
            this.TxtCantidad.Validating += new System.ComponentModel.CancelEventHandler(this.TxtCantidad_Validating);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.Location = new System.Drawing.Point(217, 271);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(124, 40);
            this.BtnCancelar.TabIndex = 3;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // BtnAceptar
            // 
            this.BtnAceptar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAceptar.Location = new System.Drawing.Point(405, 270);
            this.BtnAceptar.Name = "BtnAceptar";
            this.BtnAceptar.Size = new System.Drawing.Size(117, 40);
            this.BtnAceptar.TabIndex = 2;
            this.BtnAceptar.Text = "Aceptar";
            this.BtnAceptar.UseVisualStyleBackColor = true;
            this.BtnAceptar.Click += new System.EventHandler(this.BtnAceptar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtprecunitario);
            this.groupBox3.Controls.Add(this.txtprecfinal);
            this.groupBox3.Controls.Add(this.labelControl7);
            this.groupBox3.Controls.Add(this.txtprecmaximo);
            this.groupBox3.Controls.Add(this.labelControl4);
            this.groupBox3.Controls.Add(this.labelControl2);
            this.groupBox3.Controls.Add(this.txtprecigv);
            this.groupBox3.Controls.Add(this.txtprecminimo);
            this.groupBox3.Controls.Add(this.labelControl3);
            this.groupBox3.Controls.Add(this.labelControl1);
            this.groupBox3.Location = new System.Drawing.Point(0, 84);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(719, 74);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            // 
            // txtprecunitario
            // 
            this.txtprecunitario.EditValue = "0.00";
            this.txtprecunitario.Enabled = false;
            this.txtprecunitario.EnterMoveNextControl = true;
            this.txtprecunitario.Location = new System.Drawing.Point(576, 43);
            this.txtprecunitario.Name = "txtprecunitario";
            this.txtprecunitario.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprecunitario.Properties.Appearance.Options.UseFont = true;
            this.txtprecunitario.Properties.Mask.EditMask = "n3";
            this.txtprecunitario.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtprecunitario.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtprecunitario.Size = new System.Drawing.Size(139, 22);
            this.txtprecunitario.TabIndex = 7;
            // 
            // txtprecfinal
            // 
            this.txtprecfinal.EditValue = "0.00";
            this.txtprecfinal.EnterMoveNextControl = true;
            this.txtprecfinal.Location = new System.Drawing.Point(405, 44);
            this.txtprecfinal.Name = "txtprecfinal";
            this.txtprecfinal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprecfinal.Properties.Appearance.Options.UseFont = true;
            this.txtprecfinal.Properties.Mask.EditMask = "n3";
            this.txtprecfinal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtprecfinal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtprecfinal.Size = new System.Drawing.Size(139, 22);
            this.txtprecfinal.TabIndex = 7;
        
            this.txtprecfinal.Validating += new System.ComponentModel.CancelEventHandler(this.txtprecfinal_Validating);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(592, 18);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(87, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "Prec. Unitario";
            // 
            // txtprecmaximo
            // 
            this.txtprecmaximo.EditValue = "0.00";
            this.txtprecmaximo.Enabled = false;
            this.txtprecmaximo.EnterMoveNextControl = true;
            this.txtprecmaximo.Location = new System.Drawing.Point(404, 15);
            this.txtprecmaximo.Name = "txtprecmaximo";
            this.txtprecmaximo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprecmaximo.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtprecmaximo.Properties.Appearance.Options.UseFont = true;
            this.txtprecmaximo.Properties.Appearance.Options.UseForeColor = true;
            this.txtprecmaximo.Properties.Mask.EditMask = "n3";
            this.txtprecmaximo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtprecmaximo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtprecmaximo.Size = new System.Drawing.Size(139, 22);
            this.txtprecmaximo.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(290, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(108, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Prec. Venta Final";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(269, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(129, 16);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Valor Venta Máximo";
            // 
            // txtprecigv
            // 
            this.txtprecigv.EditValue = "0.00";
            this.txtprecigv.Enabled = false;
            this.txtprecigv.EnterMoveNextControl = true;
            this.txtprecigv.Location = new System.Drawing.Point(117, 41);
            this.txtprecigv.Name = "txtprecigv";
            this.txtprecigv.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprecigv.Properties.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.txtprecigv.Properties.Appearance.Options.UseFont = true;
            this.txtprecigv.Properties.Appearance.Options.UseForeColor = true;
            this.txtprecigv.Properties.Mask.EditMask = "n2";
            this.txtprecigv.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtprecigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtprecigv.Size = new System.Drawing.Size(139, 22);
            this.txtprecigv.TabIndex = 5;
            // 
            // txtprecminimo
            // 
            this.txtprecminimo.EditValue = "0.00";
            this.txtprecminimo.Enabled = false;
            this.txtprecminimo.EnterMoveNextControl = true;
            this.txtprecminimo.Location = new System.Drawing.Point(117, 15);
            this.txtprecminimo.Name = "txtprecminimo";
            this.txtprecminimo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprecminimo.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtprecminimo.Properties.Appearance.Options.UseFont = true;
            this.txtprecminimo.Properties.Appearance.Options.UseForeColor = true;
            this.txtprecminimo.Properties.Mask.EditMask = "n3";
            this.txtprecminimo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtprecminimo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtprecminimo.Size = new System.Drawing.Size(139, 22);
            this.txtprecminimo.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(88, 47);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(23, 16);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "IGV";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(4, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(107, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Valor Venta Min.";
            // 
            // btncodAutoriza
            // 
            this.btncodAutoriza.Enabled = false;
            this.btncodAutoriza.Location = new System.Drawing.Point(522, 173);
            this.btncodAutoriza.Name = "btncodAutoriza";
            this.btncodAutoriza.Size = new System.Drawing.Size(90, 55);
            this.btncodAutoriza.TabIndex = 9;
            this.btncodAutoriza.Text = "Ingresar\r\nCod.\r\nAutorización";
            this.btncodAutoriza.Click += new System.EventHandler(this.btncodAutoriza_Click);
            // 
            // frm_ingresar_cantidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(721, 322);
            this.Controls.Add(this.btncodAutoriza);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.BtnAceptar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_ingresar_cantidad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ingresar cantidad";
            this.Load += new System.EventHandler(this.frm_ingresar_cantidad_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMonto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCantidad.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecunitario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecfinal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecmaximo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprecminimo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label TxtItPrecUnit;
        internal System.Windows.Forms.Label LblTotalStock;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label LblDescripcionProducto;
        internal System.Windows.Forms.Label lbl0;
        internal System.Windows.Forms.Label lblimporte;
        internal System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.GroupBox groupBox2;
        public DevExpress.XtraEditors.TextEdit TxtMonto;
        public DevExpress.XtraEditors.TextEdit TxtCantidad;
        internal System.Windows.Forms.Button BtnCancelar;
        internal System.Windows.Forms.Button BtnAceptar;
        internal System.Windows.Forms.Label lblunidadmedida;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        public DevExpress.XtraEditors.TextEdit txtprecunitario;
        public DevExpress.XtraEditors.TextEdit txtprecfinal;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        public DevExpress.XtraEditors.TextEdit txtprecmaximo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.TextEdit txtprecigv;
        public DevExpress.XtraEditors.TextEdit txtprecminimo;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btncodAutoriza;
    }
}
