﻿using Comercial.Busquedas_Generales;
using Contable;
using Contable._1_Busquedas_Generales;
using Contable.Comercial.ConfiguracionComercial;
using Contable.Comercial.Pos;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraReports.UI;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_pos_edicion : frm_fuente
    {
        public frm_pos_edicion()
        {
            InitializeComponent();
        }

        public string Cja_Estado_Abierto_Cerrado;
        public string Cja_Codigo;
        public int Cja_Aper_Cierre_Codigo;
        public int Cja_Aper_Cierre_Codigo_Item;

        List<Entidad_Documentos_Punto_Venta> DocumentosXPuntoVenta = new List<Entidad_Documentos_Punto_Venta>();
        List<Entidad_Documentos_Punto_Venta> DocumentosXModVenta = new List<Entidad_Documentos_Punto_Venta>();

        public Boolean VAR_BOLETA;
        public Boolean VAR_FACTURA;
        public Boolean VAR_NOTA_SALIDA;
        string ComprobanteCodAsociada, ComprobanteCod, ComprobanteDescripcion;

        string PersonaNJ, TipoDoc;
        int i_con;
    
        Entidad_Lista_Precio ItemEdi = new Entidad_Lista_Precio();
        List<Entidad_Lista_Precio> Lista_Precio = new List<Entidad_Lista_Precio>();

        List<Entidad_Movimiento_Cab> Detalles_ADM = new List<Entidad_Movimiento_Cab>();
        List<Entidad_Movimiento_Cab> Detalles_CONT = new List<Entidad_Movimiento_Cab>();

        List<Entidad_Movimiento_Inventario> Detalles_NS = new List<Entidad_Movimiento_Inventario>();
        List<Entidad_Movimiento_Cab> DetallesControlador = new List<Entidad_Movimiento_Cab>();

        List<Entidad_Movimiento_Inventario> Detalles_Lotes_Inv = new List<Entidad_Movimiento_Inventario>();
        List<Entidad_Movimiento_Cab> Detalles_Lotes = new List<Entidad_Movimiento_Cab>();

        string Igv_Tasa_texto;
        decimal Igv_Porcentaje;
        string Igv_TasaImpresion;
        string AlmacenCodigo;

        //DATOS PARA EL ASIENTO CONTABLE
        string Id_Analisis;
        bool Es_moneda_nac;
        string Id_Libro;
        //caja
        List<Entidad_Caja> Total_ventas_contado = new List<Entidad_Caja>();
        List<Entidad_Caja> Total_ventas_tarjeta = new List<Entidad_Caja>();
        List<Entidad_Caja> Total_ventas_inventario = new List<Entidad_Caja>();

        List<Entidad_Caja> Total_ventas_credito = new List<Entidad_Caja>();
        List<Entidad_Caja> Total_ventas_inventario_credito = new List<Entidad_Caja>();


        decimal XtotalEfectivo;
        decimal XtotalCreditoEfectivo;
        decimal XtotalTarjeta;
        decimal XtotalInventario;
        decimal XtotalCreditoInventario;
        decimal XtotalOtrosIngresos;
        decimal XtotalOtrosEgresos;
        //datos para la impresion se cargar al traer el punto de venta

        string DireccionDescripcionPuntoVenta = "";
        string NombreImpresora = "";
        string Numero_maquina_registradora = "";
        int NumCaracteresFila = 42;
        bool EsEpson;
        string Codigo_Autorizacion_Sunat = "";


        int Adm_Item;

        string Estado = "";

        //FACTURACION ELEC
        string DireccionCodigo;
        string Correo_Electronico;
        string Tipo_Doc_SUNAT_Cliente;

        string Elect_Documento_Descripcion;
        string Elect_Documento_Sunat;
        void Ninguno()
        {
            Bloquear();
            Limpiar();
            dgvdatos.DataSource = null;
            Detalles_ADM.Clear();
            Detalles_CONT.Clear();
            TxtRsBaseImponible.Text = "0.00";
            TxtRsIGVMonto.Text = "0.00";
            TxtRsImporteTotal.Text = "0.00";

            btnuevo.Select();
        }
        public void Bloquear()
        {
            
            txtproductodesc.Enabled = false;
            BtnCancelar.Enabled = false;
            btncobrar.Enabled = false;
            btnmovcaja.Enabled = false;
            gbdatoscliente.Enabled = false;
            txtproductodesc.Enabled = false;
            BtnModificar.Enabled = false;
            BtnQuitar.Enabled = false;
            DDBtnModVenta.Enabled = false;
        }
        public void Habilitar()
        {
 
            txtproductodesc.Enabled = false;
            BtnCancelar.Enabled = true;
            btncobrar.Enabled = true;
            btnmovcaja.Enabled = true;
            gbdatoscliente.Enabled = true;
            txtproductodesc.Enabled = true;
            BtnModificar.Enabled = true;
            BtnQuitar.Enabled = true;
            DDBtnModVenta.Enabled = true;
        }
        public void Limpiar()
        {
            txtproductodesc.ResetText();
            txtrucdni.ResetText();
            txtclientedesc.ResetText();
            txtdireccion.ResetText();
        }
        public void Buscar_Punto_Venta_PC()
        {
            try
            {

                Entidad_Punto_Venta ent = new Entidad_Punto_Venta();
                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Punto_Venta_PC = new List<Entidad_Punto_Venta>();

                Punto_Venta_PC = log.Buscar_Pdv_por_PC(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Pdv_Nombre_PC = Actual_Conexion.Maquina
                });

                if (Punto_Venta_PC.Count > 1)
                {
                    using (frm_puntos_ventas_asignados fi = new frm_puntos_ventas_asignados())
                    {
                        fi.Nombre_PC = Actual_Conexion.Maquina;
                        if (fi.ShowDialog() == DialogResult.OK)
                        {
                            Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

                            Entidad = fi.Punto_Venta_PC[fi.gridView1.GetFocusedDataSourceRowIndex()];
                            BSIEstablecimiento.Tag = Entidad.Est_Codigo;
                            BSIEstablecimiento.Caption = Entidad.Est_Descripcion;
                            txtpdvdesc.Tag = Entidad.Pdv_Codigo;
                            txtpdvdesc.Text = Entidad.Pdv_Nombre;
                            AlmacenCodigo = Entidad.Pdv_Almacen;

                            NombreImpresora = Entidad.Pdv_Impresora;
                            Numero_maquina_registradora = Entidad.Pdv_Num_Maquina_Registradora;
                            EsEpson = false;
                            DireccionDescripcionPuntoVenta = Entidad.Est_Direccion;
                          


                            //VERIFICAMOS SI ESTA ASIGNADA UNA CAJA A UN PUNTO DE VENTA
                            Entidad_Caja EntCaja = new Entidad_Caja();
                            Logica_Caja logCaja = new Logica_Caja();
                            List<Entidad_Caja> Caja = new List<Entidad_Caja>();

                            EntCaja.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            EntCaja.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                            EntCaja.Cja_Codigo = null;

                            Caja = logCaja.Listar(EntCaja);
                            if (Caja.Count > 0)
                            {
                                //BUSCAMOS UNA CAJA ABIERTA 

                       
                                Logica_Caja Log_Caja_Abierta = new Logica_Caja();
                                List<Entidad_Caja> List_Caja_Abierta = new List<Entidad_Caja>();


                                EntCaja.Cja_Codigo = Caja[0].Cja_Codigo;
                                EntCaja.Cja_Aper_Cierre_Codigo = Caja[0].Cja_Aper_Cierre_Codigo;
                                EntCaja.Cja_Estado = "0058";
                                //0018    0058    01  APERTURADO
                                //0018    0059    02  CERRADO
                                List_Caja_Abierta = Log_Caja_Abierta.Listar_Caja_Aperturado(EntCaja);

                                if (List_Caja_Abierta.Count > 0)
                                {

                                    Cja_Codigo = List_Caja_Abierta[0].Cja_Codigo;
                                    Cja_Aper_Cierre_Codigo = Convert.ToInt32(List_Caja_Abierta[0].Cja_Aper_Cierre_Codigo);
                                    Cja_Estado_Abierto_Cerrado = List_Caja_Abierta[0].Cja_Estado;

                                }
                                else
                                {
                                    //SI NO HAY UNA CAJA ABIERTA SE CREARA UNO NUEVA PARA PODER VENDER
                                    using (frm_asignacion_caja_monto_pdv f = new frm_asignacion_caja_monto_pdv())
                                    {
                                        if (f.ShowDialog() == DialogResult.OK)
                                        {

                                            EntCaja.Cja_Saldo_Inicial = Convert.ToDecimal(f.txtsaldoinicial.Text);
                                            EntCaja.Cja_Estado = "0058";//estado de caja "Aperturado"
                                            if (Log_Caja_Abierta.Insertar_Aperturando_Caja(EntCaja))
                                            {
                                                Logica_Caja Logi_Caja_Abierta = new Logica_Caja();
                                                List<Entidad_Caja> Listi_Caja_Abierta = new List<Entidad_Caja>();
                                                EntCaja.Cja_Codigo = Caja[0].Cja_Codigo;
                                                EntCaja.Cja_Aper_Cierre_Codigo = Caja[0].Cja_Aper_Cierre_Codigo;
                                                EntCaja.Cja_Estado = "0058";
                                                //0018    0058    01  APERTURADO
                                                //0018    0059    02  CERRADO
                                                Listi_Caja_Abierta = Logi_Caja_Abierta.Listar_Caja_Aperturado(EntCaja);

                                                if (Listi_Caja_Abierta.Count > 0)
                                                {
                                                    Cja_Codigo = Listi_Caja_Abierta[0].Cja_Codigo;
                                                    Cja_Aper_Cierre_Codigo = Convert.ToInt32(Listi_Caja_Abierta[0].Cja_Aper_Cierre_Codigo);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Accion.Advertencia("DEBE CREAR UNA CAJA PARA ESTE PUNTO DE VENTA");
                                Ninguno();
                            }
                        }
                        else
                        {
                            Ninguno();
                        }
                    }

                }
                else if (Punto_Venta_PC.Count == 1)
                {
                    BSIEstablecimiento.Tag = Punto_Venta_PC[0].Est_Codigo;
                    BSIEstablecimiento.Caption = Punto_Venta_PC[0].Est_Descripcion;
                    txtpdvdesc.Tag = Punto_Venta_PC[0].Pdv_Codigo;
                    txtpdvdesc.Text = Punto_Venta_PC[0].Pdv_Nombre;
                    AlmacenCodigo = Punto_Venta_PC[0].Pdv_Almacen;

                    NombreImpresora = Punto_Venta_PC[0].Pdv_Impresora;
                    Numero_maquina_registradora = Punto_Venta_PC[0].Pdv_Num_Maquina_Registradora;
                    EsEpson = false;
                    DireccionDescripcionPuntoVenta = Punto_Venta_PC[0].Est_Direccion;


                    //VERIFICAMOS SI ESTA ASIGNADA UNA CAJA A UN PUNTO DE VENTA
                    Entidad_Caja EntCaja = new Entidad_Caja();
                    Logica_Caja logCaja = new Logica_Caja();
                    List<Entidad_Caja> Caja = new List<Entidad_Caja>();

                    EntCaja.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    EntCaja.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                    EntCaja.Cja_Codigo = null;


                    Caja = logCaja.Listar(EntCaja);
                    if (Caja.Count > 0)
                    {
                        //BUSCAMOS UNA CAJA ABIERTA 

                        Logica_Caja Log_Caja_Abierta = new Logica_Caja();
                        List<Entidad_Caja> List_Caja_Abierta = new List<Entidad_Caja>();

                        EntCaja.Cja_Codigo = Caja[0].Cja_Codigo;
                        EntCaja.Cja_Aper_Cierre_Codigo = Caja[0].Cja_Aper_Cierre_Codigo;
                        EntCaja.Cja_Estado = "0058";
                        //0018    0058    01  APERTURADO
                        //0018    0059    02  CERRADO
                        List_Caja_Abierta = Log_Caja_Abierta.Listar_Caja_Aperturado(EntCaja);

                        if (List_Caja_Abierta.Count > 0)
                        {

                            Cja_Codigo = List_Caja_Abierta[0].Cja_Codigo;
                            Cja_Aper_Cierre_Codigo = Convert.ToInt32(List_Caja_Abierta[0].Cja_Aper_Cierre_Codigo);
                            Cja_Estado_Abierto_Cerrado = List_Caja_Abierta[0].Cja_Estado;
                        }
                        else
                        {
                            //SI NO HAY UNA CAJA ABIERTA SE CREARA UNO NUEVA PARA PODER VENDER
                            using (frm_asignacion_caja_monto_pdv f = new frm_asignacion_caja_monto_pdv())
                            {
                                if (f.ShowDialog() == DialogResult.OK)
                                {

                                    EntCaja.Cja_Saldo_Inicial = Convert.ToDecimal(f.txtsaldoinicial.Text);
                                    EntCaja.Cja_Estado = "0058";//estado de caja "Aperturado"
                                    if (Log_Caja_Abierta.Insertar_Aperturando_Caja(EntCaja))
                                    {


                                        Buscar_Punto_Venta_PC();

                                    }
                                }
                                else
                                {
                                    Ninguno();
                                }
                            }
                        }
                    }
                    else
                    {
                        Accion.Advertencia("DEBE CREAR UNA CAJA PARA ESTE PUNTO DE VENTA");
                        Ninguno();
                    }

                }
                else
                {
                    Accion.Advertencia("Usted No tiene asignado un punto de venta");
                    Ninguno();
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
                Ninguno();
            }
        }

        void Buscar_Comprabante_Punto_Venta()
        {


            try
            {
                DocumentosXPuntoVenta.Clear();

                if (((BSIEstablecimiento.Tag.ToString() != "") && (txtpdvdesc.Tag.ToString() != "")))
                {
                    Entidad_Documentos_Punto_Venta Documento = new Entidad_Documentos_Punto_Venta();
                    Logica_Documentos_Punto_Venta Log_Documento = new Logica_Documentos_Punto_Venta();

                    Documento.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Documento.Est_Codigo = BSIEstablecimiento.Tag.ToString();
                    Documento.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                    DocumentosXPuntoVenta = Log_Documento.Listar(Documento);

                    DocumentosXModVenta.Clear();

                    if (DocumentosXPuntoVenta.Count > 0)
                    {


                        foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXPuntoVenta)
                        {
                            if (Ent_Doc.Doc_Es_Nota_Salida)
                            {
                                CheckBtnNotSal.Enabled = true;
                                DocumentosXModVenta.Add(Ent_Doc);
                                break;
                            }

                        }

                        if (DocumentosXModVenta.Count == 0)
                        {
                            Entidad_Documentos_Punto_Venta Ent_ = new Entidad_Documentos_Punto_Venta();
                            Ent_.Doc_Es_Nota_Salida = false;
                            DocumentosXModVenta.Add(Ent_);
                        }

                        foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXPuntoVenta)
                        {
                            if (Ent_Doc.Doc_Es_Boleta)
                            {
                                CheckBtnTicketBol.Enabled = true;
                                DocumentosXModVenta.Add(Ent_Doc);
                                break;
                            }

                        }

                        if (DocumentosXModVenta.Count == 1)
                        {
                            Entidad_Documentos_Punto_Venta Ent_ = new Entidad_Documentos_Punto_Venta();
                            Ent_.Doc_Es_Boleta = false;
                            DocumentosXModVenta.Add(Ent_);
                        }



                        foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXPuntoVenta)
                        {
                            if (Ent_Doc.Doc_Es_Factura)
                            {
                                CheckBtnTicketFac.Enabled = true;
                                DocumentosXModVenta.Add(Ent_Doc);
                                break;
                            }

                        }

                        if (DocumentosXModVenta.Count == 2)
                        {
                            Entidad_Documentos_Punto_Venta Ent_ = new Entidad_Documentos_Punto_Venta();
                            Ent_.Doc_Es_Factura = false;
                            DocumentosXModVenta.Add(Ent_);
                        }

                        


                        ComprobanteCod = DocumentosXModVenta[0].Doc_Tipo_Doc;
                        ComprobanteCodAsociada = DocumentosXModVenta[0].Doc_Tipo_Doc_Asoc;
                        ComprobanteDescripcion = DocumentosXModVenta[0].Nombre_Comprobante;
                        BSITipoDocumento.Text = DocumentosXModVenta[0].Nombre_Comprobante;

                        VAR_BOLETA = DocumentosXModVenta[0].Doc_Es_Boleta;
                        VAR_FACTURA = DocumentosXModVenta[0].Doc_Es_Factura;
                        VAR_NOTA_SALIDA = DocumentosXModVenta[0].Doc_Es_Nota_Salida;

                        //foreach (Entidad_Documentos_Punto_Venta enti in DocumentosXModVenta)
                        //{
                        //    if (enti.Doc_Por_Defecto)
                        //    {
                        //        ComprobanteCod = enti.Doc_Tipo_Doc;
                        //        ComprobanteCodAsociada = enti.Doc_Tipo_Doc_Asoc;
                        //        ComprobanteDescripcion = enti.Nombre_Comprobante;
                        //        BSITipoDocumento.Caption = enti.Nombre_Comprobante;
                        //    }

                        //}

                        //VAR_BOLETA = DocumentosXModVenta[0].Doc_Es_Boleta;
                        //VAR_FACTURA = DocumentosXModVenta[0].Doc_Es_Factura;
                        //VAR_NOTA_SALIDA = DocumentosXModVenta[0].Doc_Es_Nota_Salida;


                        BuscarSerieNumDocPtoVta(ComprobanteCod);
                        Longitud_RUC_DNI();
                    }
                    else
                    {
                        Accion.Advertencia("FALTA CONFIGURAR LOS COMPROBANTES DE VENTA PARA ESTE PUNTO DE VENTA " + BSIPuntoVenta.Caption);
                        Ninguno();
                    }

                }

            }
            catch (Exception ex)
            {

                Ninguno();
            }

            //Comentario de prueba
            //subri
        }

        int Ser_Max_Lineas;
        bool Es_Electronico;
        void BuscarSerieNumDocPtoVta(string DocumentoCod)
        {
            try
            {
                Logica_Serie_Comprobante LogSerieNum = new Logica_Serie_Comprobante();
                List<Entidad_Serie_Comprobante> SerieNumeros = new List<Entidad_Serie_Comprobante>();

                SerieNumeros = LogSerieNum.Listar(new Entidad_Serie_Comprobante
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Ser_Tipo_Doc = DocumentoCod,
                    Ser_Pdv_Codigo = txtpdvdesc.Tag.ToString()
                });

                if ((SerieNumeros.Count <= 0))
                {

                    Accion.Advertencia("La configuracion de series para este documento en este punto de venta esta pendiente");
                    //Estado = Estados.Ninguno;
                    gbxBotones.Enabled = false;
                    gbxAgregarDetalle.Enabled = false;
                    return;
                }
                else
                {
                    gbxBotones.Enabled = true;
                    gbxAgregarDetalle.Enabled = true;

                    BSISerieDocum.Text = SerieNumeros[0].Ser_Serie;
                    Es_Electronico = SerieNumeros[0].Es_Electronico;

                    if (SerieNumeros[0].Ser_Numero_Actual == "")
                    {
                        SerieNumeros[0].Ser_Numero_Actual = Convert.ToString("0");
                    }

                    Codigo_Autorizacion_Sunat = SerieNumeros[0].Ser_Cod_Autorizacion_SUNAT;
                    Ser_Max_Lineas = SerieNumeros[0].Ser_Max_Lineas;

                    if (((int.Parse(SerieNumeros[0].Ser_Numero_Actual) >= (int.Parse(SerieNumeros[0].Ser_Numero_Ini) - 1)) && (int.Parse(SerieNumeros[0].Ser_Numero_Actual) < int.Parse(SerieNumeros[0].Ser_Numero_Fin))))
                    {

                        int NumActual = Convert.ToInt32(SerieNumeros[0].Ser_Numero_Actual);
                        int Incrementado = (NumActual + 1);
                        string Resultado = Convert.ToString(Incrementado);
                        BSINumeroDocum.Text = Accion.Formato(Resultado, 8);

                    }
                    else
                    {
                        Accion.Advertencia("El numero de esta serie esta fuera del intervalo, corrija su Base de datos");
                        //Estado = Estados.Ninguno;
                        return;
                    }

                }

            }

            catch (Exception ex)
            {

                Ninguno();
            }

        }


        void CambiarModVenta(bool Valor)
        {
            VAR_BOLETA = Valor;
            VAR_FACTURA = Valor;
            VAR_NOTA_SALIDA = Valor;
        }
        void Longitud_RUC_DNI()
        {
            if (VAR_BOLETA)
            {
                txtrucdni.Properties.MaxLength = 8;
            }
            else if (VAR_FACTURA)
            {
                txtrucdni.Properties.MaxLength = 11;
            }
            else if (VAR_NOTA_SALIDA)
            {
                txtrucdni.Properties.MaxLength = 8;
            }
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(BSIFecha.Caption) == true))
                {
                    if ((DateTime.Parse(BSIFecha.Caption).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(BSIFecha.Caption);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            Igv_Porcentaje = Lista[0].Imd_Tasa;
                            Igv_Tasa_texto = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));
                            Igv_TasaImpresion = Lista[0].Imd_Tasa_Impresion;
                            LblIGV.Text = "I.G.V." + "(" + Math.Round(Lista[0].Imd_Tasa * 100, 2) + "):";
                            LblIGV.Tag = Lista[0].Imd_Tasa;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Venta(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_Venta;
                    //txtlibro.Text = Generales[0].Ini_Venta_Desc;
                }
                else
                {
                    Accion.Advertencia("Debe configurar un libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void Cliente_Por_Defecto()
        {
            List<Entidad_Entidad> Lista = new List<Entidad_Entidad>();
            Entidad_Entidad Ent = new Entidad_Entidad();
            Logica_Entidad log = new Logica_Entidad();

            try
            {
                Ent.Ent_RUC_DNI = "00000001";
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    //txtrucdni.Text=Lista[0].
                    txtrucdni.Text = Lista[0].Ent_RUC_DNI.ToString().Trim();
                    txtclientedesc.Text = Lista[0].Ent_Razon_Social_Nombre.Trim() + " " + Lista[0].Ent_Ape_Paterno.Trim() + " " + Lista[0].Ent_Ape_Materno.Trim();
                    txtdireccion.Text = Lista[0].Ent_Domicilio_Fiscal.Trim();

                    Correo_Electronico = Lista[0].Ent_Correo;
                    Tipo_Doc_SUNAT_Cliente = Lista[0].Ent_Tipo_Doc_Cod_Interno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        void TraerFechaServidor()
        {
            List<Entidad_Punto_Venta> Fecha_Actual = new List<Entidad_Punto_Venta>();
            Logica_Punto_Venta Log_Fecha_Actual = new Logica_Punto_Venta();
            Entidad_Punto_Venta d_fecha = new Entidad_Punto_Venta();
            Fecha_Actual = Log_Fecha_Actual.Traer_Fecha(d_fecha);
            if (Fecha_Actual.Count > 0)
            {
                BSIFecha.Caption = Fecha_Actual[0].FechaActual_Servidor;
            }
        }

        bool ObtenerValorVentaOtrosProductos(Entidad_Catalogo Product)
        {
            try
            {
                if (string.IsNullOrEmpty(Product.Id_Catalogo))
                {
                    Accion.Advertencia("DEBE INGRESAR UN PRODUCTO");
                    return false;
                }

                Logica_Lista_Precio log = new Logica_Lista_Precio();
                Entidad_Lista_Precio ent = new Entidad_Lista_Precio();


                ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                ent.Est_Codigo = BSIEstablecimiento.Tag.ToString();
                ent.Id_Tipo = "0031";//BIEN
                ent.Id_Catalogo = Product.Id_Catalogo;
                ent.Pre_Tipo = "0006";//CONTADO
                ent.Pre_Moneda_cod = "001"; //SOLES
                ent.Pre_Fecha_Ini = Convert.ToDateTime(BSIFecha.Caption);
                ent.Pre_Id_Mod_Venta = Product.Pre_Id_Mod_Venta;
                Lista_Precio = log.Traer_Precio_Procuto(ent);

            }
            catch (Exception ex)
            {
                //Accion.ErrorSistema(ex.Message);
                return false;
            }
            return true;
        }

        bool ExistenOtrosProd_Contado(Entidad_Catalogo Product)
        {
            i_con = 0;
            foreach (Entidad_Movimiento_Cab T in Detalles_ADM)
            {
                if (T.Adm_Catalogo == Product.Id_Catalogo & T.Adm_Unm_Id == Product.Unm_Cod_Det)
                {
                    return true;
                }
                i_con += 1;
            }
            return false;
        }

        bool ExistenOtrosProd_Contado_PorItem(Entidad_Catalogo Product)
        {
            i_con = 0;
            foreach (Entidad_Movimiento_Cab T in Detalles_ADM)
            {
                if (T.Adm_Catalogo == Product.Id_Catalogo & T.Adm_Unm_Id == Product.Unm_Cod_Det & T.Adm_Tipo_BSA == Product.Id_Tipo)
                {
                    return true;
                }
                i_con += 1;
            }
            return false;
        }

        int i_lot;
        bool ExistenOtrosProd_Lote(Entidad_Catalogo Lote )
        {
            i_lot = 0;
            foreach (Entidad_Movimiento_Cab T in Detalles_Lotes)
            {
                if (T.Lot_Catalogo == Lote.Id_Catalogo & T.Lot_Lote == Lote.Lot_Lote)
                {
                    return true;
                }
                i_lot += 1;
            }
            return false;
        }

        bool ExistenOtrosProd_Lote_PorItem(string Adm_Catalogo, string Lote)
        {
            i_lot = 0;
            foreach (Entidad_Movimiento_Cab T in Detalles_Lotes)
            {
                if (T.Lot_Catalogo == Adm_Catalogo & T.Lot_Lote == Lote)
                {
                    return true;
                }
                i_lot += 1;
            }
            return false;
        }


        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;

            if (Detalles_ADM.Count > 0)
            {
                dgvdatos.DataSource = Detalles_ADM;
            }
            MostrarResumen();

        }
        void IngresarCantidadOtrosProductos(Entidad_Catalogo Product,Entidad_Catalogo Presentacio_Produc,Entidad_Catalogo Lotes, decimal Cantidad,decimal Total_Stock,decimal prec_valor_venta,decimal precfinal,decimal total,bool Acepta_Lotes)
        {
            try
            {
                //if (ObtenerValorVentaOtrosProductos(Product))
                //{

                 
                    Entidad_Movimiento_Cab item = new Entidad_Movimiento_Cab();

                    item.Adm_Item = Adm_Item;

                    item.Adm_Tipo_BSA = "0031";
              

                    item.Adm_Catalogo = Product.Id_Catalogo;
                    item.Adm_Catalogo_Desc = Product.Cat_Descripcion;

                    //Buscamos El tipo de Operacion Por Producto para definir Si Esta afecto u Otro Regimen del IGV
                    Entidad_Operaciones oper = new Entidad_Operaciones();
                    Logica_Operaciones logoper = new Logica_Operaciones();
                    List<Entidad_Operaciones> operlist = new List<Entidad_Operaciones>();
                    oper.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    oper.Id_Tipo = item.Adm_Tipo_BSA;
                    oper.Id_Catalogo = item.Adm_Catalogo;
                    operlist = logoper.Traer_Operacion_venta_Por_Producto(oper);
                    ////////////////////


                    item.DvtD_OperCodigo = operlist[0].AfecIGV_Tabla; //"0565";
                    item.Adm_Almacen = AlmacenCodigo;
                    item.Adm_Almacen_desc = "";

                    if (item.DvtD_OperCodigo == "0565")
                    {
                        item.DvtD_IGVTasaPorcentaje = Convert.ToDecimal(Igv_Tasa_texto);
                    }
                    else
                    {
                        item.DvtD_IGVTasaPorcentaje = 0;
                    }
                   


                    //Unidad de medida
                    item.Adm_Unm_Abrev = Presentacio_Produc.Und_Abreviado;// 'Product.Unm_Desc_Det;
                    item.Adm_Unm_Desc = Presentacio_Produc.Unm_Desc_Det;
                    item.UNM_CoigodFE = Presentacio_Produc.Unm_FE;

                    item.Adm_Unm_Id = Presentacio_Produc.Unm_Cod_Det;

                    bool PosUlt = false;

                    if (ExistenOtrosProd_Contado(Product))
                    {
                        PosUlt = false;

                        Detalles_ADM[i_con].Adm_Cantidad = Detalles_ADM[i_con].Adm_Cantidad + 1;

                    // verificamos si el producto acepta lotes 
                    // para editar los lotes
                        if (Acepta_Lotes)
                        {
                            if (ExistenOtrosProd_Lote(Lotes))
                                {
                                    Detalles_Lotes[i_lot].Lot_Cantidad = Detalles_Lotes[i_lot].Lot_Cantidad + 1;
                                }
                                else
                                {
                                    //Entidad_Movimiento_Cab Lote = new Entidad_Movimiento_Cab();
                                    //Lote.Lot_Adm_item = Adm_Item;
                                    //Lote.Lot_item = Detalles_Lotes.Count + 1;
                                    //Lote.Lot_Catalogo = Product.Id_Catalogo;
                                    //Lote.Lot_Lote = Lotes.Lot_Lote;
                                    //Lote.Lot_FechaFabricacion = Lotes.Lot_FechaFabricacion;
                                    //Lote.Lot_FechaVencimiento = Lotes.Lot_FechaVencimiento;
                                    //Lote.Lot_Cantidad = Cantidad;

                                    //Detalles_Lotes.Add(Lote);
                                Accion.Advertencia("Solo permite añadir un lote del producto en el detalle");
                                Detalles_ADM[i_con].Adm_Cantidad = Detalles_ADM[i_con].Adm_Cantidad - 1;
                                return;
                            }
                        }


                        if (Total_Stock < Detalles_ADM[i_con].Adm_Cantidad)
                        {
                            Accion.Advertencia("NO HAY STOCK PARA ESTE PRODUCTO");
                            Detalles_ADM[i_con].Adm_Cantidad = Detalles_ADM[i_con].Adm_Cantidad - 1;
                        }

                    }
                    else
                    {

                        Adm_Item = Detalles_ADM.Count + 1;
                        if (Adm_Item <= Ser_Max_Lineas)
                        {
                            PosUlt = true;

                            item.Adm_Item = Adm_Item;
                            item.Adm_Cantidad = Cantidad; 
                            item.Adm_Total =Math.Round(item.Adm_Valor_Unit,2) * item.Adm_Cantidad;

                            item.Acepta_lotes = Product.Acepta_Lote;

                            Detalles_ADM.Add(item);

                        if (Acepta_Lotes)
                        {
                            //Añadimos detalle para productos qeu tienen lote
                            //Esto para las ventas con boleta - factura
                            Entidad_Movimiento_Cab Lote = new Entidad_Movimiento_Cab();
                            Lote.Lot_Adm_item = Adm_Item;
                            Lote.Lot_item = Detalles_Lotes.Count + 1;
                            Lote.Lot_Catalogo = Product.Id_Catalogo;
                            Lote.Lot_Lote = Lotes.Lot_Lote;
                            Lote.Lot_FechaFabricacion = Lotes.Lot_FechaFabricacion;
                            Lote.Lot_FechaVencimiento = Lotes.Lot_FechaVencimiento;
                            Lote.Lot_Cantidad = Cantidad;

                            Detalles_Lotes.Add(Lote);
                        }


                    }
                        else
                        {

                            Accion.Advertencia("Esta serie solo permite " + Ser_Max_Lineas + " registros en el detalle");
                            return;
                        };
                    }

                item.Adm_Valor_Venta = prec_valor_venta;
                item.Adm_Valor_Unit = precfinal;
                Detalles_ADM[i_con].Adm_Valor_Unit =Math.Round(item.Adm_Valor_Unit,2);
                Detalles_ADM[i_con].Adm_Total =Math.Round(item.Adm_Valor_Unit,2) * Detalles_ADM[i_con].Adm_Cantidad;

 

                UpdateGrilla();

                if (PosUlt)
                {
                    gridView1.MoveLastVisible();
                }
                else
                {
                    gridView1.FocusedRowHandle = i_con;
                }

                    OtrosProductosOtrosDocumentos();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        void IngresarCantidadOtrosProductosNuevo(List<Entidad_Catalogo> DetallesLista)
        {
            try
            {

                foreach (Entidad_Catalogo lst in DetallesLista)
                {

                Entidad_Movimiento_Cab item = new Entidad_Movimiento_Cab();

                 CantidadTotal = lst.Stock;

                 item.Adm_Item = lst.Id_Item;

                item.Adm_Tipo_BSA = lst.Id_Tipo;

                item.Adm_Catalogo = lst.Id_Catalogo;
                item.Adm_Catalogo_Desc = lst.Cat_Descripcion;

                //Buscamos El tipo de Operacion Por Producto para definir Si Esta afecto u Otro Regimen del IGV
                Entidad_Operaciones oper = new Entidad_Operaciones();
                Logica_Operaciones logoper = new Logica_Operaciones();
                List<Entidad_Operaciones> operlist = new List<Entidad_Operaciones>();
                oper.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                oper.Id_Tipo = item.Adm_Tipo_BSA;
                oper.Id_Catalogo = item.Adm_Catalogo;
                operlist = logoper.Traer_Operacion_venta_Por_Producto(oper);
                ////////////////////


                item.DvtD_OperCodigo = operlist[0].AfecIGV_Tabla; //"0565";
                item.Adm_Almacen = AlmacenCodigo;
                item.Adm_Almacen_desc = "";

                if (item.DvtD_OperCodigo == "0565")
                {
                    item.DvtD_IGVTasaPorcentaje = Convert.ToDecimal(Igv_Tasa_texto);
                }
                else
                {
                    item.DvtD_IGVTasaPorcentaje = 0;
                }

                    item.Adm_Tipo_Operacion = operlist[0].AfecIGV_Tabla; 
                    item.Ven_Afecto_IGV_FE = operlist[0].AfecIGV_Codigo;

                    //Unidad de medida
                item.Adm_Unm_Abrev = lst.Und_Abreviado;// 'Product.Unm_Desc_Det;
                item.Adm_Unm_Desc = lst.Unm_Desc_Det;
                item.UNM_CoigodFE = lst.Unm_FE;

                item.Adm_Unm_Id = lst.Unm_Cod_Det;

                bool PosUlt = false;

                item.Unm_Base = lst.Unm_Base;   
                    
                item.Acepta_lotes = lst.Acepta_Lote;
                item.Lot_Lote = lst.Lot_Lote;
                item.Lot_FechaFabricacion = lst.Lot_FechaFabricacion;
                item.Lot_FechaVencimiento = lst.Lot_FechaVencimiento;


                Entidad_Catalogo producto = new Entidad_Catalogo();

                producto.Id_Catalogo = lst.Id_Catalogo;
                producto.Unm_Cod_Det = lst.Unm_Cod_Det;
                    producto.Id_Tipo = lst.Id_Tipo;

                    if (ExistenOtrosProd_Contado_PorItem(producto))
                    {
                        PosUlt = false;

                        Detalles_ADM[i_con].Adm_Cantidad = Detalles_ADM[i_con].Adm_Cantidad + 1;

                        // verificamos si el producto acepta lotes 
                        // para editar los lotes
                        if (item.Acepta_lotes)
                        {
                            if (ExistenOtrosProd_Lote_PorItem(item.Adm_Catalogo,item.Lot_Lote))
                            {
                                Detalles_Lotes[i_lot].Lot_Cantidad = Detalles_Lotes[i_lot].Lot_Cantidad + 1;
                            }
                        }

                    }
                    else
                    {

                        Adm_Item = Detalles_ADM.Count + 1;
                        if (Adm_Item <= Ser_Max_Lineas)
                        {
                            PosUlt = true;

                            item.Adm_Item = lst.Id_Item;
                            item.Adm_Cantidad = lst.Cantidad;

                            item.Adm_CantidadAtendidaEnBaseAlCoefciente = lst.Invd_CantidadAtendidaEnBaseAlCoefciente;

                            item.Adm_Total = Math.Round(lst.Adm_Valor_Unit, 2) * lst.Cantidad;

                            item.Acepta_lotes = lst.Acepta_Lote ;

                            Detalles_ADM.Add(item);

                            if (item.Acepta_lotes)
                            {
                                //Añadimos detalle para productos qeu tienen lote
                                //Esto para las ventas con boleta - factura
                                Entidad_Movimiento_Cab Lote = new Entidad_Movimiento_Cab();
                                Lote.Lot_Adm_item = Adm_Item;
                                Lote.Lot_item = Detalles_Lotes.Count + 1;
                                Lote.Lot_Catalogo = lst.Id_Catalogo;
                                Lote.Lot_Lote = lst.Lot_Lote;
                                Lote.Lot_FechaFabricacion = lst.Lot_FechaFabricacion;
                                Lote.Lot_FechaVencimiento = lst.Lot_FechaVencimiento;
                                Lote.Lot_Cantidad = lst.Cantidad;

                                Detalles_Lotes.Add(Lote);
                            }


                        }
                        else
                        {

                            Accion.Advertencia("Esta serie solo permite " + Ser_Max_Lineas + " registros en el detalle");
                            return;
                        };
                    }



                    item.Adm_Valor_Venta = lst.Adm_Valor_Venta;
                    item.Adm_Valor_Unit = lst.Adm_Valor_Unit;
                    Detalles_ADM[i_con].Adm_Valor_Unit = Math.Round(item.Adm_Valor_Unit, 2);
                    Detalles_ADM[i_con].Adm_Total = Math.Round(item.Adm_Valor_Unit, 2) * Detalles_ADM[i_con].Adm_Cantidad;



                    UpdateGrilla();

                    if (PosUlt)
                    {
                        gridView1.MoveLastVisible();
                    }
                    else
                    {
                        gridView1.FocusedRowHandle = i_con;
                    }

                }

                OtrosProductosOtrosDocumentos();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }












        void OtrosProductosOtrosDocumentos()
        {
            Renumerar();
            
            CargarOtrosPproductos();
            CargarOtrosPproductosLote();

            if (VAR_NOTA_SALIDA)
            {
                List<Entidad_Movimiento_Inventario> NS = new List<Entidad_Movimiento_Inventario>();
                NS.Clear();
                foreach (Entidad_Movimiento_Inventario T in Detalles_NS)
                {
                    foreach (Entidad_Movimiento_Cab D in DetallesControlador)
                    {
                        if (T.Invd_Catalogo == D.Adm_Catalogo)
                        {
                            NS.Add(T);
                        }
                    }
                }
                Detalles_NS.Clear();
                Detalles_NS = NS;
                Renumerar3();
                foreach (Entidad_Movimiento_Cab Ent_Aux in OtrosProductos)
                {
                    Entidad_Movimiento_Inventario Item = new Entidad_Movimiento_Inventario();

                    Item.Id_Item = Detalles_NS.Count + 1;

                    Item.Invd_TipoBSA = Ent_Aux.Adm_Tipo_BSA;
      
                    Item.Invd_Catalogo = Ent_Aux.Adm_Catalogo;
                    Item.Invd_Catalogo_Desc = Ent_Aux.Adm_Catalogo_Desc;

                    Item.Invd_Cantidad = Ent_Aux.Adm_Cantidad;
                    Item.Invd_Valor_Unit = Ent_Aux.Adm_Valor_Unit;
                    Item.Invd_Total = Ent_Aux.Adm_Total;
                    Item.Invd_Unm = Ent_Aux.Adm_Unm_Id;
                    Item.Acepta_lotes = Ent_Aux.Acepta_lotes;
                    Item.Lot_Lote = Ent_Aux.Lot_Lote;
                    Item.Lot_FechaFabricacion = Ent_Aux.Lot_FechaFabricacion;
                    Item.Lot_FechaVencimiento = Ent_Aux.Lot_FechaVencimiento;

                    Item.Unm_Base = Ent_Aux.Unm_Base;
                    Item.Invd_CantidadAtendidaEnBaseAlCoefciente = Ent_Aux.Adm_CantidadAtendidaEnBaseAlCoefciente;


                    Detalles_NS.Add(Item);
                }

                Detalles_Lotes.Clear();
                ////Para añadir en las ventas Sin Comprobante
                foreach (Entidad_Movimiento_Cab Ent_Aux in OtrosProductosLote)
                {
                    Entidad_Movimiento_Cab Lote_inv = new Entidad_Movimiento_Cab();

                    Lote_inv.Lot_Adm_item = Ent_Aux.Lot_Adm_item;
                    Lote_inv.Lot_item = Ent_Aux.Lot_item;
                    Lote_inv.Lot_Catalogo = Ent_Aux.Lot_Catalogo;
                    Lote_inv.Lot_Lote = Ent_Aux.Lot_Lote;
                    Lote_inv.Lot_FechaFabricacion = Ent_Aux.Lot_FechaFabricacion;
                    Lote_inv.Lot_FechaVencimiento = Ent_Aux.Lot_FechaVencimiento;
                    Lote_inv.Lot_Cantidad = Ent_Aux.Lot_Cantidad;

                    Detalles_Lotes.Add(Lote_inv);
                }


                Detalles_Lotes_Inv.Clear();
                ////Para añadir en las ventas Sin Comprobante
                foreach (Entidad_Movimiento_Cab Ent_Aux in OtrosProductosLote)
                {
                    Entidad_Movimiento_Inventario Lote_inv = new Entidad_Movimiento_Inventario();

                    Lote_inv.Lot_Adm_item = Ent_Aux.Lot_Adm_item;
                    Lote_inv.Lot_item = Ent_Aux.Lot_item;
                    Lote_inv.Lot_Catalogo = Ent_Aux.Lot_Catalogo;
                    Lote_inv.Lot_Lote = Ent_Aux.Lot_Lote;
                    Lote_inv.Lot_FechaFabricacion = Ent_Aux.Lot_FechaFabricacion;
                    Lote_inv.Lot_FechaVencimiento = Ent_Aux.Lot_FechaVencimiento;
                    Lote_inv.Lot_Cantidad = Ent_Aux.Lot_Cantidad;

                   Detalles_Lotes_Inv.Add(Lote_inv);
                 }


                Renumerar3();
            }
        }

        void Renumerar()
        {
            for (int i = 0; (i <= (Detalles_ADM.Count - 1)); i++)
            {
                Detalles_ADM[i].Adm_Item = (i + 1);
            }
        }
        void Renumerar3()
        {
            for (int i = 0; (i <= (Detalles_ADM.Count - 1)); i++)//Detalles_NS
            {
                Detalles_ADM[i].Adm_Item = (i + 1);//Detalles_NS
            }
            //MostrarResumen();
        }
        List<Entidad_Movimiento_Cab> OtrosProductos = new List<Entidad_Movimiento_Cab>();

        List<Entidad_Movimiento_Cab> OtrosProductosLote = new List<Entidad_Movimiento_Cab>();

        void CargarOtrosPproductos()
        {
            OtrosProductos.Clear();
            int j = 0;
            foreach (Entidad_Movimiento_Cab T in Detalles_ADM)
            {
                Entidad_Movimiento_Cab ItemO = new Entidad_Movimiento_Cab();
                foreach (Entidad_Movimiento_Cab C in DetallesControlador)
                {
                    if (T.Adm_Catalogo != C.Adm_Catalogo)
                    {
                        ItemO = DetallesControlador[j];
                        OtrosProductos.Add(ItemO);
                    }
                }
                if (DetallesControlador.Count == 0)
                {
                    ItemO = Detalles_ADM[j];
                    OtrosProductos.Add(ItemO);
                }
                j += 1;
            }
        }

        void CargarOtrosPproductosLote()
        {
            OtrosProductosLote.Clear();
            int j = 0;
            foreach (Entidad_Movimiento_Cab T in Detalles_Lotes)
            {
                Entidad_Movimiento_Cab ItemO = new Entidad_Movimiento_Cab();
                foreach (Entidad_Movimiento_Cab C in DetallesControlador)
                {
                    if (T.Adm_Catalogo != C.Adm_Catalogo)
                    {
                        ItemO = DetallesControlador[j];
                        OtrosProductosLote.Add(ItemO);
                    }
                }
                if (DetallesControlador.Count == 0)
                {
                    ItemO = Detalles_Lotes[j];
                    OtrosProductosLote.Add(ItemO);
                }
                j += 1;
            }
        }

        void MostrarResumen()
        {
            decimal MBase1 = 0, MValor_Fac_Export = 0;
            decimal MExonerada = 0, MInafecta=0, MIGV = 0;
            decimal MISC = 0;
            decimal MOtrosTributos = 0, MImporteTotal = 0;

            if ((VAR_BOLETA) || (VAR_FACTURA) || (VAR_NOTA_SALIDA))
            {
                foreach (Entidad_Movimiento_Cab T in Detalles_ADM)
                {
                    MValor_Fac_Export += T.DvtD_ValorFactExportacion;
                    MBase1 += T.DvtD_BaseImpobleOperacionGravada;//Math.Round((T.Adm_Total / (Igv_Porcentaje + 1)),4);
                    MExonerada += T.DvtD_Exonerada;
                    MInafecta += T.DvtD_Inafecta;
                    MISC += T.DvtD_ISC;
                    MIGV += T.DvtD_IGV_IPM;
                }


                //TxtRsBaseImponible.Text = Convert.ToString(Math.Round(MBase1, 2));
                //TxtRsIGVMonto.Text = Convert.ToString(Math.Round(Igv_Porcentaje * MBase1, 2,MidpointRounding.AwayFromZero ));
                //TxtRsImporteTotal.Text = Convert.ToString(Math.Round(MBase1 + Convert.ToDecimal(TxtRsIGVMonto.Text), 2));
                //               TxtHME.Text = String.Format("{0:0,0.00}", SumaHaberExt);
                MImporteTotal = (MValor_Fac_Export + MBase1 + MExonerada + MInafecta + MISC+ MIGV);
                TxtRsValExporta.Text = String.Format("{0:0,0.00}", Math.Round(MValor_Fac_Export, 2));   
                TxtRsBaseImponible.Text = String.Format("{0:0,0.00}", Math.Round(MBase1, 2))  ;  
                TxtRsExonerada.Text = String.Format("{0:0,0.00}", Math.Round(MExonerada, 2)) ; 
                TxtRsInAfecta.Text = String.Format("{0:0,0.00}", Math.Round(MInafecta, 2)) ; 
                TxtRsISC.Text = String.Format("{0:0,0.00}", Math.Round(MISC, 2));
                TxtRsIGVMonto.Text = String.Format("{0:0,0.00}", Math.Round(MIGV, 2)); 
                TxtRsImporteTotal.Text = String.Format("{0:0,0.00}", Math.Round(MImporteTotal, 2));  

                BuscarTipoAnalisisDefecto();

                int xx = Detalles_NS.Count();
            }



        }

        void BuscarTipoAnalisisDefecto()
        {
            Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
            Logica_Analisis_Contable log = new Logica_Analisis_Contable();
            List<Entidad_Analisis_Contable> Detalles_Analisi = new List<Entidad_Analisis_Contable>();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Analisis = Id_Analisis;
            Ent.Ana_Venta = true;
            Ent.Ana_Asiento_Defecto = true;
            Ent.Ana_Compra = false;


            Detalles_Analisi = log.Traer_Analisis_Defecto(Ent);
            Detalles_CONT.Clear();

            if (Detalles_Analisi.Count > 0)
            {
                Detalles_CONT.Clear();

                Id_Analisis = Detalles_Analisi[0].Id_Analisis;

                foreach (Entidad_Analisis_Contable T in Detalles_Analisi)
                {
                    Entidad_Movimiento_Cab Item = new Entidad_Movimiento_Cab();

                    Item.Id_Item = Detalles_CONT.Count + 1;
                    Item.Ctb_Cuenta = T.Ana_Cuenta;
                    Item.Ctb_Cuenta_Desc = T.Ana_Cuenta_Desc;
                    Item.Ctb_Operacion_Cod = T.Ana_Operacion_Cod;
                    Item.Ctb_Operacion_Cod_Interno = T.Ana_Operacion_Det;
                    Item.Ctb_Operacion_Desc = T.Ana_Operacion_Desc;
                    Item.Ctb_Tipo_DH = T.Ana_Tipo;
                    Item.CCtb_Tipo_DH_Interno = T.Ana_Tipo_Det;
                    Item.Ctb_Tipo_DH_Desc = T.Ana_Tipo_Desc;

                    Item.Ctb_Fecha_Mov_det = Convert.ToDateTime(BSIFecha.Caption);
                    Item.Ctb_Tipo_Ent_det = "C";
                    Item.Ctb_Ruc_dni_det = txtrucdni.Text.ToString().Trim();

                    Item.Ctb_Tipo_Doc_det = ComprobanteCod;
                    Item.Ctb_moneda_cod_det = "001";

                    Item.Ctb_Tipo_Cambio_Cod_Det = "SVC";
                    Item.Ctb_Tipo_Cambio_Desc_Det = "SIN CONVERSION";

                    Item.Ctb_Tipo_Cambio_Valor_Det = 1;

                    Item.Ctb_Serie_det = BSISerieDocum.Text;
                    Item.Ctb_Numero_det = BSINumeroDocum.Text;

                    if (Item.Ctb_Tipo_DH == "0004")
                    {
                        if (Es_moneda_nac == true)
                        {
                            Item.Ctb_Importe_Debe = 0;
                        }
                        else
                        {
                            Item.Ctb_Importe_Debe_Extr = 0;
                        }
                    }
                    else if (Item.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            Item.Ctb_Importe_Haber = 0;
                        }
                        else
                        {
                            Item.Ctb_Importe_Haber_Extr = 0;
                        }
                    }

                    Detalles_CONT.Add(Item);
                }
            }
            Calcular(Detalles_CONT, Es_moneda_nac);
            //UpdateGrilla02();
        }

        decimal MBase1 = 0, MValor_Fac_Export = 0;
        decimal MExonerada = 0, MInafecta, MIgv1 = 0;
        decimal MIsc = 0;
        decimal MOtrosTributos = 0, MImporteTotal = 0;
        public void Calcular(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

            try
            {
                decimal Base1 = 0, Valor_Fac_Export = 0;
                decimal Exonerada = 0, Inafecta = 0, Igv1 = 0;
                decimal Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;


                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();

                    Ent = Detalles_CONT[itenms.Id_Item - 1];

                    Ent.Id_Item = itenms.Id_Item;

                    Ent.Ctb_Cuenta = itenms.Ctb_Cuenta;
                    Ent.Ctb_Cuenta_Desc = itenms.Ctb_Cuenta_Desc;
                    Ent.Ctb_Operacion_Cod = itenms.Ctb_Operacion_Cod;
                    Ent.Ctb_Operacion_Cod_Interno = itenms.Ctb_Operacion_Cod_Interno;
                    Ent.Ctb_Operacion_Desc = itenms.Ctb_Operacion_Desc;
                    Ent.Ctb_Tipo_DH = itenms.Ctb_Tipo_DH;
                    Ent.CCtb_Tipo_DH_Interno = itenms.CCtb_Tipo_DH_Interno;
                    Ent.Ctb_Tipo_DH_Desc = itenms.Ctb_Tipo_DH_Desc;

                    Ent.Ctb_Fecha_Mov_det = itenms.Ctb_Fecha_Mov_det;
                    Ent.Ctb_Tipo_Ent_det = "C";
                    Ent.Ctb_Ruc_dni_det = txtrucdni.Text;
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;
                    Ent.Ctb_moneda_cod_det = itenms.Ctb_moneda_cod_det;
                    Ent.Ctb_Tipo_Cambio_Cod_Det = itenms.Ctb_Tipo_Cambio_Cod_Det;
                    Ent.Ctb_Tipo_Cambio_Desc_Det = itenms.Ctb_Tipo_Cambio_Desc_Det;
                    Ent.Ctb_Tipo_Cambio_Valor_Det = itenms.Ctb_Tipo_Cambio_Valor_Det;
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;
                    Ent.Ctb_Serie_det = itenms.Ctb_Serie_det;
                    Ent.Ctb_Numero_det = itenms.Ctb_Numero_det;

                    if ((itenms.Ctb_Operacion_Cod == "0016"))//BASE IMPONIBLE
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible += Convert.ToDecimal(TxtRsBaseImponible.Text);
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Debe_Extr;//(itenms.Ctb_Importe_Debe_Extr);
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                            }
                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible += Convert.ToDecimal(TxtRsBaseImponible.Text);//(itenms.Ctb_Importe_Haber);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Haber_Extr;//(itenms.Ctb_Importe_Haber_Extr);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                        }
                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0017")) // IGV
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv += Convert.ToDecimal(TxtRsIGVMonto.Text);//();
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                            }
                            else
                            {
                                Ent.Ctb_Igv += itenms.Ctb_Importe_Debe_Extr;//(itenms.Ctb_Importe_Debe_Extr);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                            }
                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv += Convert.ToDecimal(TxtRsIGVMonto.Text);//(itenms.Ctb_Importe_Haber);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                        }
                        else
                        {
                            Ent.Ctb_Igv += itenms.Ctb_Importe_Haber_Extr;// (itenms.Ctb_Importe_Haber_Extr);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                        }
                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0043")) // IMPORTE TOTAL
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total += Convert.ToDecimal(TxtRsImporteTotal.Text);// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Debe_Extr;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total += Convert.ToDecimal(TxtRsImporteTotal.Text);// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Haber_Extr;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                    }



                    ////PAAR EL DEBE Y EL HABER
                    ////
                    if (Ent.Ctb_Tipo_DH == "0004")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0016") //Base 1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Base1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Base1 * 1;
                                Ent.Ctb_Importe_Debe_Extr = Base1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0017")//igv1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Igv1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Igv1 * 1;
                                Ent.Ctb_Importe_Debe_Extr = Igv1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0043")// 
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = ImporteTotal;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = ImporteTotal * 1;
                                Ent.Ctb_Importe_Debe_Extr = ImporteTotal;
                            }
                        }
                    }
                    else if (Ent.Ctb_Tipo_DH == "0005")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0016") //Base 1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Base1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Base1 * 1;
                                Ent.Ctb_Importe_Haber_Extr = Base1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0017")//igv1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Igv1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Igv1 * 1;
                                Ent.Ctb_Importe_Haber_Extr = Igv1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0043")// 
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = ImporteTotal;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = ImporteTotal * 1;
                                Ent.Ctb_Importe_Haber_Extr = ImporteTotal;
                            }
                        }
                    }

                    //Detalles_CONT[itenms.Id_Item] = Ent;

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        bool Verificar_Detalles()
        {
            if (VAR_BOLETA || VAR_FACTURA || VAR_NOTA_SALIDA)
            {
                if (Detalles_ADM.Count == 0)
                {
                    Accion.Advertencia("Deve Ingresar un detalle");
                    return false;
                }
                else if (VAR_FACTURA & string.IsNullOrWhiteSpace(txtrucdni.Text))
                {
                    Accion.Advertencia("Ingrese un RUC para el cliente");
                    return false;
                }
                else if (VAR_FACTURA & string.IsNullOrWhiteSpace(txtclientedesc.Text))
                {
                    Accion.Advertencia("Ingrese una Razón Social.");
                    return false;
                }
                else if (VAR_FACTURA & string.IsNullOrWhiteSpace(txtdireccion.Text))
                {
                    Accion.Advertencia("Ingrese una direccion ");
                    return false;
                }
                else if (VAR_BOLETA & string.IsNullOrWhiteSpace(txtrucdni.Text))
                {
                    Accion.Advertencia("Ingrese el cliente por defecto");
                    return false;
                }
                else if (VAR_NOTA_SALIDA & string.IsNullOrWhiteSpace(txtrucdni.Text))
                {
                    Accion.Advertencia("Ingrese el cliente por defecto");
                    return false;
                }
            }

            return true;
        }

        public string TextoCentro(string StrC)
        {
            string StrPrint = "";
            string VNext = "";
            if ((StrC.Length > NumCaracteresFila))
            {
                string[] TxtWords = StrC.Split(Convert.ToChar(" "));
                string CurrentSub = "";
                string AfterSub = "";
                bool FoundLength = false;
                foreach (string Txt in TxtWords)
                {
                    if ((((CurrentSub + (" " + Txt)).Length <= NumCaracteresFila) && (FoundLength == false)))
                    {
                        CurrentSub = (CurrentSub + (" " + Txt));
                    }
                    else
                    {
                        FoundLength = true;
                        AfterSub = (AfterSub + (" " + Txt));
                    }

                }

                StrPrint = CurrentSub;
                VNext = TextoCentro(AfterSub);
            }
            else
            {
                StrPrint = StrC;
            }

            if ((StrPrint.Trim().Length == NumCaracteresFila))
            {
                return (StrPrint + ("\n" + VNext));
            }
            else
            {
                int espc = ((NumCaracteresFila - StrPrint.Trim().Length) / 2);
                StrPrint = (new string(' ', espc) + StrPrint.Trim());
                // & StrDup(espc, " ")
                return (StrPrint + ("\n" + VNext));
            }

        }


        string FormatearLimite(string StrF)
        {
            string StrPrint = "";
            string VNext = "";
            if ((StrF.Length > NumCaracteresFila))
            {
                string[] TxtWords = StrF.Split(Convert.ToChar(" "));
                string CurrentSub = "";
                string AfterSub = "";
                bool FoundLength = false;
                foreach (string Txt in TxtWords)
                {
                    if ((((CurrentSub + (" " + Txt)).Length <= NumCaracteresFila) && (FoundLength == false)))
                    {
                        CurrentSub = (CurrentSub + (" " + Txt)).Trim();
                    }
                    else
                    {
                        FoundLength = true;
                        AfterSub = (AfterSub + (" " + Txt));
                    }

                }

                StrPrint = CurrentSub;
                VNext = FormatearLimite(AfterSub.Trim());
            }
            else
            {
                StrPrint = StrF.Trim();
            }

            return (StrPrint + ("\n" + VNext));
        }




        public string AgregaDetalle(string cant, string par1, string precio, string total)
        {
            string StrPrint = "";
            string DescripLarga = "";
            StrPrint = ("        ".Substring(0, (8 - cant.ToString().Length)) + (cant.ToString() + " "));
            if ((par1.Trim().Length > 17))
            {
                // par1 = par1.Remove(0, 17)
                StrPrint = (StrPrint + (par1.Substring(0, 17) + " "));
                DescripLarga = DescripcionLarga(par1.Substring(17));
            }
            else
            {
                StrPrint = (StrPrint + par1.PadRight(18));
            }

            // ----Prec. Unit.
            StrPrint = (StrPrint + (precio.PadLeft(7) + " "));
            if ((NumCaracteresFila == 42))
            {
                StrPrint = (StrPrint + (total.PadLeft(7) + "\n"));
                // (StrDup(7, "-") & total).Substring((StrDup(7, "-") & total).Length - (total.Length)) '& " "
            }
            else
            {
                StrPrint = (StrPrint + (total.PadLeft(5) + "\n"));
            }

            if (!string.IsNullOrWhiteSpace(DescripLarga))
            {
                StrPrint = (StrPrint + DescripLarga);
            }

            return StrPrint;
        }

        string DescripcionLarga(string strr)
        {
            string strRestante = new string(' ', 9);
            if ((strr.Length > 17))
            {
                string StrNext = strr.Substring(17);
                return (strRestante + (strr.Substring(0, 17) + ("\n" + DescripcionLarga(StrNext))));
            }
            else
            {
                return (strRestante + (strr + "\n"));
            }

        }

        public static bool IsNumeric(object value)
        {
            try
            {
                int i = Convert.ToInt32(value.ToString());
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public void GuardarDocumentosEmitidos(string Txt, string NombreArchivo, string NombreCarpeta)
        {
            try
            {

                string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + NombreCarpeta.ToUpper().ToString());
                bool exists = System.IO.Directory.Exists(Carpeta);
                string Archivo = Carpeta + "\\" + NombreArchivo + " " + (DateTime.Now.ToString("dd-MM-yyyy hh.mm.ss tt", CultureInfo.InvariantCulture) + ".txt");

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(Carpeta);
                }

                // Crear documento
                File.WriteAllText(Archivo, Txt);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        void Imprimir_Ticket()
        {

            Comercial.Rpte_Comprobante_No_Electronico RptPOS = new Comercial.Rpte_Comprobante_No_Electronico();
            List<Entidad_Movimiento_Inventario> listRpt = new List<Entidad_Movimiento_Inventario>();
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();

            Ent.Detalle = Detalles_NS;
            listRpt.Add(Ent);

            RptPOS.lbl_Emisor_Apellidos_RazonSocial.Text = Actual_Conexion.EmpresaNombre.Trim();
            RptPOS.lbl_Emisor_Documento_Numero.Text = Actual_Conexion.RucEmpresa.Trim();
            RptPOS.lbl_TipoDocumento_Descripcion.Text = "TICKET INTERNO";
            RptPOS.lbl_TipoDocumento_Serie_Numero.Text = BSISerieDocum.Text +"-"+ BSINumeroDocum.Text;

            //Convert.ToDateTime(BSIFecha.Caption);
            RptPOS.lbl_fecha_emision.Text =Convert.ToString(Convert.ToDateTime(BSIFecha.Caption));
            RptPOS.lbl_cliente.Text = txtclientedesc.Text;
            RptPOS.lbl_dni.Text =txtrucdni.Text;
            RptPOS.lbl_direccion.Text = txtdireccion.Text;


            RptPOS.lbl_op_gravadas.Text = Convert.ToString(Convert.ToDecimal(TxtRsBaseImponible.Text));
            RptPOS.lbl_igv.Text = Convert.ToString(Convert.ToDecimal(TxtRsIGVMonto.Text));
            RptPOS.lbl_total.Text = Convert.ToString(Convert.ToDecimal(TxtRsImporteTotal.Text));


            RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            RptPOS.DataSource = Detalles_NS;

            int HeightDoc = 400;
            HeightDoc = HeightDoc + (32 * Detalles_NS.Count());
            RptPOS.PageHeight = HeightDoc;

            RptPOS.CreateDocument();
            RptPOS.PrinterName = NombreImpresora;

            PdfExportOptions pdfOptions = RptPOS.ExportOptions.Pdf;

            pdfOptions.DocumentOptions.Title =  BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim();
            string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "VENTAS SIN COMPROBANTE");
            string NombreArchivo = Carpeta + "\\" + BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim() + ".pdf";
            RptPOS.ExportToPdf(NombreArchivo, pdfOptions);

            try
            {

                string DocDecision = "Desea imprimir el ticket " + BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim();
                if (Accion.ShowDecision(DocDecision))
                {
                    try
                    {

                        using (ReportPrintTool printTool = new ReportPrintTool(RptPOS))
                        {
                            printTool.Print(NombreImpresora);
                            //RptPOS.ExportToPdf(NombreArchivo, pdfOptions);
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
        void ImprimirTicket()
        {
            try
            {

                PrintRaw pr = new PrintRaw();
                string bufferpr = "";
                bufferpr = (bufferpr + TextoCentro(Actual_Conexion.EmpresaNombre));
                bufferpr = (bufferpr + TextoCentro(Actual_Conexion.RucEmpresa));
                bufferpr = (bufferpr + TextoCentro(DireccionDescripcionPuntoVenta));
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));
                bufferpr = (bufferpr + ("Maq.Reg. Nro.: " + (Numero_maquina_registradora + "\r\n")));

                string TipoTicket = "";
                string TipoDoc = "";
                if (VAR_NOTA_SALIDA)
                {
                    TipoTicket = "Ticket:";
                    TipoDoc = "DNI: ";
                }
                if (VAR_BOLETA)
                {
                    TipoTicket = "Ticket Boleta:";
                    TipoDoc = "DNI: ";
                }
                if (VAR_FACTURA)
                {
                    TipoTicket = "Ticket Factura:";
                    TipoDoc = "R.U.C.: ";
                }

                bufferpr = (bufferpr + (TipoTicket + (BSISerieDocum.Text + (" - " + (BSINumeroDocum.Text + "\r\n")))));

                bufferpr = (bufferpr + FormatearLimite(("Nombre: " + txtclientedesc.Text)));

                if (!string.IsNullOrWhiteSpace(txtdireccion.Text))//DireccionDescripcion
                {
                    bufferpr = (bufferpr + FormatearLimite(("Direccion: " + txtdireccion.Text.Trim())));
                }

                bufferpr = (bufferpr + (TipoDoc + (txtrucdni.Text + "\r\n")));


                bufferpr = (bufferpr + (((DateTime.Now.ToShortDateString() + (" " + DateTime.Now.ToShortTimeString()))).PadLeft(NumCaracteresFila) + "\r\n"));
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));

                if ((NumCaracteresFila == 42))
                {
                    bufferpr = (bufferpr + ("CANT     DESCRIPCION      PRECIO   IMPORTE" + "\n"));
                }
                else
                {
                    bufferpr = (bufferpr + ("CANT     DESCRIPCION     PRECIO  IMPORTE" + "\n"));
                }

                foreach (Entidad_Movimiento_Cab det in Detalles_ADM)
                {
                    bufferpr = (bufferpr + AgregaDetalle(Convert.ToDecimal(det.Adm_Cantidad).ToString("0.000"), det.Adm_Catalogo_Desc, Convert.ToDecimal(det.Adm_Valor_Unit).ToString("0.00"), Convert.ToDecimal(det.Adm_Total).ToString("0.00")));
                }

                if (VAR_BOLETA || VAR_NOTA_SALIDA)
                {
                    bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));
                    bufferpr = (bufferpr + ("TOTAL" + (TxtRsImporteTotal.Text.PadLeft(((NumCaracteresFila == 42) ? 37 : 35)) + "\r\n")));
                    //Imprimir_Letras numeros = new Imprimir_Letras();
                    //bufferpr = (bufferpr + FormatearLimite(("Son: " + (numeros.NroEnLetras(Convert.ToDouble(TxtRsImporteTotal.Text)) + " SOLES"))));

                }
                else if (VAR_FACTURA)
                {
                    bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));
                    bufferpr = (bufferpr + ("SUBTOTAL" + (TxtRsBaseImponible.Text.PadLeft(((NumCaracteresFila == 42) ? 34 : 32)) + "\r\n")));
                    bufferpr = (bufferpr + ("IGV(" + (Igv_TasaImpresion + (")" + (TxtRsIGVMonto.Text.PadLeft(((NumCaracteresFila == 42) ? 34 : 32)) + "\r\n")))));
                    bufferpr = (bufferpr + ("TOTAL" + (TxtRsImporteTotal.Text.PadLeft(((NumCaracteresFila == 42) ? 37 : 35)) + "\r\n")));
                    bufferpr = (bufferpr + "\r\n");
                    bufferpr = (bufferpr + "\r\n");
                    //Imprimir_Letras numeros = new Imprimir_Letras();
                    //bufferpr = (bufferpr + FormatearLimite(("Son: " + (numeros.NroEnLetras(Convert.ToDouble(TxtRsImporteTotal.Text)) + " SOLES"))));
                    //bufferpr = (bufferpr + "\r\n");
                }


                bufferpr = (bufferpr + ("Usuario: " + (Actual_Conexion.UserName + "\r\n")));
                bufferpr = (bufferpr + ("Cod.Auto. Sunat: " + (Codigo_Autorizacion_Sunat + "\r\n")));
                bufferpr = (bufferpr + ("" + "\n"));
                bufferpr = (bufferpr + TextoCentro("GRACIAS POR SU PREFERENCIA"));
                //  & vbCrLf
                bufferpr = (bufferpr + TextoCentro("Vuelva pronto"));
                //  & vbCrLf

                //Entidad_Establecimiento Guardar = new Entidad_Establecimiento();
                string Nombre = ("TK-Bol " + (BSISerieDocum.Text + ("-" + BSINumeroDocum.Text)));
                GuardarDocumentosEmitidos(bufferpr, Nombre, "Ventas al contado");

                if (EsEpson)
                {
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ('' + 'i'));
                }
                else
                {
                    bufferpr = (bufferpr + Convert.ToString((char)(27)).ToString() + "d" + (char)(2)).ToString();
                    bufferpr = (bufferpr + Convert.ToString((char)(7)).ToString());
                }

                PrintRaw.SendStringToPrinter(NombreImpresora, bufferpr);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        #region Impresion de Boleta Factura Electronica

        string Nombre = "";
        void Impresion_Boleta_Factura_Electronica(Entidad_Movimiento_Cab Doc, bool Impresion_Automatica,bool Es_Electronico)
        {
            try
            {


                string Modelo_Impresion = "000";
                Nombre = Modelo_Impresion;

                // Warning!!! Optional parameters not supported
                DocumentoElectronico Doc_Electronico_Bol_Fact;
                Doc_Electronico_Bol_Fact = Convertir_Cliente_Objet(Doc);

                if (Es_Electronico)
                {


                    //var settings = new System.Xml.XmlWriterSettings() { ConformanceLevel = System.Xml.ConformanceLevel.Auto, Indent = true, IndentChars = Conversions.ToString('\t'), Encoding = Encoding.UTF8 };
                    //using (var writer = System.Xml.XmlWriter.Create("DocFactura.xml", settings))
                    //{
                    //    var typeToSerialize = typeof(DocumentoElectronico);
                    //    var xs = new System.Xml.Serialization.XmlSerializer(typeToSerialize);
                    //    xs.Serialize(writer, Doc_Electronico_Bol_Fact); // Doc
                    //}

                    //var doc12 = new System.Xml.XmlDocument();
                    //doc12.Load("DocFactura.xml");

                    //var PruebaXML__1 = new Doc_Generar_Xml();
                    //string RptaWS = PruebaXML__1.Documento_RegistrarActualizar(doc12);

                    //string[] datosRpta = RptaWS.Split(Convert.ToChar("#"));
                    //string DataVar;
                    //DataVar = datosRpta[0]; // Correlativo '- error, 0 documento no enviado, 1 documento registrado/actualizado
                    //DataVar = datosRpta[1]; // Codigo web
                    //DataVar = datosRpta[2]; // DigestValue (Código Hash)   
                    //DataVar = datosRpta[3]; // Valores Codigo de barras PDF



                    string ws_Rpta = Facte.Registrar_Para_Enviar(Doc_Electronico_Bol_Fact);
                    string[] datosRpta = ws_Rpta.Split(Convert.ToChar("#"));
                    string DataVar;
                    DataVar = datosRpta[0];
                    DataVar = datosRpta[1];
                    DataVar = datosRpta[2];
                    DataVar = datosRpta[3];
                    // Valores Codigo de barras PDF
                    // DataVar = datosRpta(4) 'Sunat Codigo '-1 problema conexion, 0 Aceptado, 'x' un error
                    // DataVar = datosRpta(5) 'Sunat Descripcion Aceptado/Rechazado en caso de los envios/ o error de conexion


                    Print_Invoice_POS RptPOS = new Print_Invoice_POS();
                    List<DocumentoElectronico> listRpt = new List<DocumentoElectronico>();
                    listRpt.Add(Doc_Electronico_Bol_Fact);
                    listRpt[0].Print_BarCode = datosRpta[3].Trim();
                    listRpt[0].Print_DigestValue = datosRpta[2].Trim();

                    if (listRpt[0].Cliente_Documento_Numero.Length != 11)
                    {
                         RptPOS.XrLabel13.Text = "DNI:";
                        ////'Cuando es boleta ocultamos estos campos
                        //RptPOS.XrLabel19.Visible = false;
                        //RptPOS.XrLabel20.Visible = false;
                        //RptPOS.XrLabel5.Visible = false;
                        //RptPOS.XrLabel6.Visible = false;
                        //RptPOS.XrLabel23.Visible = false;
                        //RptPOS.XrLabel30.Visible = false;
                    }
                    else
                    {
                        RptPOS.XrLabel13.Text = "RUC:";
                    }


                    RptPOS.lblplaca.Visible = false;
                    RptPOS.lblhash.Visible = true;
                    RptPOS.CodigoBarras.Visible = true;
                    RptPOS.AutorizacionSunat.Visible = false;
                    RptPOS.DetailPagoTarjeta.Visible = false;
                    RptPOS.DatosVendedor.Visible = false;

                    //int HeightDoc = 1000;
                    //HeightDoc = HeightDoc + (32 * Detalles_ADM.Count());
                    //RptPOS.PageHeight = HeightDoc;


                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    RptPOS.DataSource = listRpt;

                    RptPOS.CreateDocument();
                    RptPOS.PrinterName = NombreImpresora;

                    PdfExportOptions pdfOptions = RptPOS.ExportOptions.Pdf;

                    pdfOptions.DocumentOptions.Title = BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim();
                    string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "VENTAS AL CONTADO");
                    string NombreArchivo = Carpeta + "\\" + BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim() + ".pdf";
                    RptPOS.ExportToPdf(NombreArchivo, pdfOptions);

                    try
                    {
                        using (ReportPrintTool printTool = new ReportPrintTool(RptPOS))
                        {
                            printTool.Print(NombreImpresora);
                            //RptPOS.ExportToPdf(NombreArchivo, pdfOptions);
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }


                }
                else
                {
                    //Reportes_Impresiones.Rpt_Libro_Diario f = new Reportes_Impresiones.Rpt_Libro_Diario();

                    Print_Invoice_POS RptPOS = new Print_Invoice_POS();
                    List<DocumentoElectronico> listRpt = new List<DocumentoElectronico>();
                    listRpt.Add(Doc_Electronico_Bol_Fact);

                    if (listRpt[0].Cliente_Documento_Numero.Length != 11)
                    {
                        //RptPOS.XrLabel13.Text = "DNI:";
                        ////'Cuando es boleta ocultamos estos campos
                        //RptPOS.XrLabel19.Visible = false;
                        //RptPOS.XrLabel20.Visible = false;
                        //RptPOS.XrLabel5.Visible = false;
                        //RptPOS.XrLabel6.Visible = false;
                        //RptPOS.XrLabel23.Visible = false;
                        //RptPOS.XrLabel30.Visible = false;
                    }


                    RptPOS.lblplaca.Visible = false;
                    RptPOS.lblhash.Visible = false;
                    RptPOS.CodigoBarras.Visible = false;
                    RptPOS.AutorizacionSunat.Visible = false;
                    RptPOS.DetailPagoTarjeta.Visible = false;
                    RptPOS.DatosVendedor.Visible = false;

                    int HeightDoc = 1000;
                    HeightDoc = HeightDoc + (32 * Detalles_ADM.Count());
                    RptPOS.PageHeight = HeightDoc;


                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    RptPOS.DataSource = listRpt;

                    RptPOS.CreateDocument();
                    RptPOS.PrinterName = NombreImpresora;

                    PdfExportOptions pdfOptions = RptPOS.ExportOptions.Pdf;

                    pdfOptions.DocumentOptions.Title =  BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim();
                    string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "VENTAS AL CONTADO");
                    string NombreArchivo = Carpeta + "\\" +  BSISerieDocum.Text.Trim() + "-" + BSINumeroDocum.Text.Trim()+".pdf";
                    RptPOS.ExportToPdf(NombreArchivo, pdfOptions);

                    try
                    {
                        using (ReportPrintTool printTool = new ReportPrintTool(RptPOS))
                        {
                            printTool.Print(NombreImpresora);
                            //RptPOS.ExportToPdf(NombreArchivo, pdfOptions);
                        }
                    }
                    catch(Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }

                }
              
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        List<Entidad_Movimiento_Cab> PagosDoc = new List<Entidad_Movimiento_Cab>(); 
        DocumentoElectronico Convertir_Cliente_Objet(Entidad_Movimiento_Cab Documento_Cliente)
        {
            DocumentoElectronico Doc = new DocumentoElectronico();
            Doc.Impresion_Modelo_Codigo = Nombre;

            Doc.Fecha_Emision = Documento_Cliente.Ctb_Fecha_Movimiento;
            Doc.Emisor_ApellidosNombres_RazonSocial = Actual_Conexion.EmpresaNombre.Trim();
            Doc.Emisor_Documento_Tipo = "6";
            Doc.Emisor_Documento_Numero = Actual_Conexion.RucEmpresa.Trim();
            // ----------------------------
            // --- Domicilio fiscal -- PostalAddress
            Doc.Emisor_Direccion_Ubigeo = "020101";
            Doc.Emisor_Direccion_Calle = Actual_Conexion.Emp_DireccionCorta.Trim();//Actual_Conexion.Emp_DireccionCorta.Trim();
            Doc.Emisor_Direccion_Urbanizacion = Actual_Conexion.Emp_Urbanizacion.Trim();// Actual_Conexion.Emp_Urbanizacion.Trim();
            Doc.Emisor_Direccion_Departamento = Actual_Conexion.Emp_Direccion_Departamento.Trim();// Actual_Conexion.Emp_Direccion_Departamento.Trim();
            Doc.Emisor_Direccion_Provincia = Actual_Conexion.Emp_Direccion_Provincia.Trim();// Actual_Conexion.Emp_Direccion_Provincia.Trim();
            Doc.Emisor_Direccion_Distrito = Actual_Conexion.Emp_Direccion_Distrito.Trim();//Actual_Conexion.Emp_Direccion_Distrito.Trim();
            Doc.Emisor_Direccion_PaisCodigo = "PE";

           // Doc.Fecha_Vencimiento = DBNull.Value;

            // "01" ' Si es Boleta o Factura segun Sunat ==>
            Doc.TipoDocumento_Codigo = Documento_Cliente.Ctb_Tipo_Doc_Sunat.Trim();
            Doc.TipoDocumento_Serie = Documento_Cliente.Ctb_Serie.Trim();
            Doc.TipoDocumento_Numero = Documento_Cliente.Ctb_Numero.Trim();

            string Desc="";
            if (Es_Electronico)
            {
                Desc = "ELECTRONICA";
            }

            Doc.TipoDocumento_Descripcion = (Documento_Cliente.Nombre_Comprobante.Trim() +  Desc);
       
            // "6" 'Si es DNI o RUC segun sunat
            Doc.Cliente_Documento_Tipo = Documento_Cliente.Ent_TpDcEnt_SUNAT.Trim();
            Doc.Cliente_Documento_Numero = ((Documento_Cliente.Ctb_Ruc_dni.Trim() == "00000001") ? "00000000" : Documento_Cliente.Ctb_Ruc_dni.Trim());
            Doc.Cliente_RazonSocial_Nombre = Documento_Cliente.Cliente_Razon_Social.Trim();
            Doc.Cliente_Direccion = Documento_Cliente.Cliente_Direccion.Trim();
            Doc.Cliente_Correo = Documento_Cliente.Correo_Cliente.Trim();
            Doc.Numero_Placa_del_Vehiculo = "";// 'Documento_Cliente.Vnt_placa_Vehiculo;


            List<DocumentoElectronicoDetalle> feDetalles = new List<DocumentoElectronicoDetalle>();

            List<Entidad_Movimiento_Cab> DetalleVenta = new List<Entidad_Movimiento_Cab>();
            

            foreach (Entidad_Movimiento_Cab bf in Detalles_ADM)
            {
                DocumentoElectronicoDetalle det = new DocumentoElectronicoDetalle();

                det.Item = bf.Adm_Item;
                det.Producto_Codigo = bf.Adm_Catalogo.Trim();
                det.Producto_Descripcion = (bf.Adm_Catalogo_Desc.Trim() + (" x " + bf.Adm_Unm_Desc.Trim()));
                det.Producto_UnidadMedida_Codigo = bf.UNM_CoigodFE.Trim();
                det.Producto_Cantidad = Convert.ToDouble(bf.Adm_Cantidad);
                det.Unitario_Precio_Venta = Convert.ToDouble(bf.Adm_Valor_Unit);//DvtD_PrecUnitario
                det.Unitario_Precio_Venta_Tipo_Codigo = "01";
                det.Item_IGV_Total = bf.DvtD_IGVMonto;
                det.Unitario_IGV_Porcentaje = Convert.ToDouble(bf.DvtD_IGVTasaPorcentaje);
                det.Item_Tipo_Afectacion_IGV_Codigo = "10";
                det.Item_ISC_Total = 0;
                det.Unitario_Valor_Unitario = Convert.ToDouble(bf.Adm_Valor_Venta);//DvtD_ValorVenta
                det.Item_ValorVenta_Total = bf.DvtD_SubTotal;
                det.Item_Total = Convert.ToDouble(bf.Adm_Total);
                det.Numero_Placa_del_Vehiculo = "";//(((bf.UNM_CoigodFE == "GLL") || (bf.UNM_CoigodFE == "LTR")) ? Documento_Cliente.Vnt_placa_Vehiculo : null);
                feDetalles.Add(det);
            }

            Doc.Detalles = feDetalles.ToArray();
            Doc.Total_ValorVenta_OperacionesGravadas = Convert.ToDouble(Documento_Cliente.Ctb_Base_Imponible); //Dvt_MntoBaseImponible
            Doc.Total_ValorVenta_OperacionesInafectas = Convert.ToDouble(Documento_Cliente.Ctb_Inafecta);//Dvt_MntoInafecta
            Doc.Total_ValorVenta_OperacionesExoneradas = Convert.ToDouble(Documento_Cliente.Ctb_Exonerada); //Dvt_MntoExonerado
            Doc.Total_ValorVenta_OperacionesGratuitas = 0;
            Doc.Total_IGV = Convert.ToDouble(Documento_Cliente.Ctb_Igv);// Dvt_MntoIGV
            Doc.IGV_Porcentaje = Convert.ToDouble(Documento_Cliente.Ctb_Tasa_IGV);// Dvt_PrcIGV
            Doc.Total_ISC = Convert.ToDouble(Documento_Cliente.Ctb_Isc_Importe);// Dvt_MntoISC
            Doc.Total_OtrosTributos = 0;
            Doc.Total_OtrosCargos = 0;
            // Doc.Descuentos_Globales = 0
            Doc.Total_Importe_Venta = Convert.ToDouble(Documento_Cliente.Ctb_Importe_Total);// Dvt_MntoTotal
            Doc.Moneda_Codigo = "PEN";
            Doc.Moneda_Descripcion = "SOLES";
            Doc.Moneda_Simbolo = "S/";

            Doc.Total_Importe_Venta_Texto = Imprimir_Letras.NroEnLetras(Convert.ToDecimal(Doc.Total_Importe_Venta)) + " " + Doc.Moneda_Descripcion;


            Doc.Impresion_Modelo_Codigo = "000";

            //Venta percepcion
            Doc.Tipo_Operacion_Codigo = null;
            Doc.Total_Importe_Percepcion = 0;
            Doc.Percep_Importe_Total_Cobrado = 0;

            //    45 Leyendas
            List<DocumentoElectronicoLeyenda> ListaLeyendas = new List<DocumentoElectronicoLeyenda>();
            //ListaLeyendas.Add(new DocumentoElectronicoLeyenda() With {, ., Codigo = 1000, ., ((NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) + (" " + Doc.Moneda_Descripcion))));

            ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "1000", Descripcion = Imprimir_Letras.NroEnLetras(Convert.ToDecimal(Math.Truncate(Doc.Total_Importe_Venta))) + " " + Doc.Moneda_Descripcion });

            if (Doc.Total_ValorVenta_OperacionesGratuitas > 0)
            {
                ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "1002", Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y / O SERVICIO PRESTADO GRATUITAMENTE" });
            }

            if ((Doc.Total_Importe_Percepcion > 0))
            {
                ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "2000", Descripcion = "COMPROBANTE DE PERCEPCION" });
            }

            if (!string.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo))
            {
                if (Doc.Tipo_Operacion_Codigo == "05")
                {
                    ListaLeyendas.Add(new DocumentoElectronicoLeyenda { Codigo = "2005", Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE" });
                }

            }

            Doc.Leyendas = ListaLeyendas.ToArray();

            List<DocumentoElectronicoPagoTarjeta> feDetallesPago = new List<DocumentoElectronicoPagoTarjeta>();

            foreach (Entidad_Movimiento_Cab bf in PagosDoc)
            {
                DocumentoElectronicoDetalle det = new DocumentoElectronicoDetalle();
                feDetalles.Add(det);

            }

            Doc.PagoTarjeta = feDetallesPago.ToArray();

            //ZONA MODIFICADA
            //Doc.Sunat_Autorizacion = ("Autorizado mediante Resolución N� " + Documento_Cliente.Vnt_Cod_Autorizacion_Sunat.Trim);
            //// "018005000949/SUNAT"
            //Doc.Emisor_Web_Visualizacion = ("Consulte su documento en " + "www.grupoortiz.pe/facturacion");
            //Doc.RepresentacionImpresaDela = ("Representaci�n Impresa de la " + Doc.TipoDocumento_Descripcion);
            //Doc.Hora_Emision = (!string.IsNullOrEmpty(Documento_Cliente.Dvt_AtencHora) ? Convert.ToDateTime(Documento_Cliente.Dvt_AtencHora) : Now);
            //Doc.Cajero_Nombre = Actual_Conexion.UserName;//.UserName;
            //Doc.Emisor_Establecimiento = ("Establecimiento: " + ((Documento_Cliente.Vnt_IsVenta_Automatica == true) ? Documento_Cliente.Vnt_Establecimiento_Descripcion : BSIEstablecimiento.Caption));
            //Doc.Emisor_Establecimiento_Direccion = ((Documento_Cliente.Vnt_IsVenta_Automatica == true) ? Documento_Cliente.Vnt_Establecimiento_Direccion : DireccionDescripcionPuntoVenta);


            return Doc;

        }


        List<Entidad_Movimiento_Cab> Agrupar_Detalles(List<Entidad_Movimiento_Cab> Detalle)
        {
            List<Entidad_Movimiento_Cab> ListaDet = new List<Entidad_Movimiento_Cab>();


            return ListaDet;
        }


        #endregion

        string Catalogo_Cod_Verificar;
        bool SelectItem()
        {
            try
            {
                if ((VAR_BOLETA || VAR_FACTURA))
                {
                    if (((gridView1.RowCount > 0) && (gridView1.GetFocusedDataSourceRowIndex() > -1)))
                    {
                        Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                        Ent = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
                        //TipoProc = Ent.ProductoIngreso;
                        Adm_Item = Ent.Adm_Item;
                        Catalogo_Cod_Verificar = Ent.Adm_Catalogo;
                        return true;
                    }
                }
                else if (VAR_NOTA_SALIDA)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Ent = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
                    Adm_Item = Ent.Adm_Item;
                    Catalogo_Cod_Verificar = Ent.Adm_Catalogo;
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
                return false;
            }
        }

        bool VerificarModificacion()
        {
            Entidad_Movimiento_Cab ent = new Entidad_Movimiento_Cab();

            if (VAR_BOLETA || VAR_FACTURA)
            {
                ent = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
            }
            else if (VAR_NOTA_SALIDA)
            {
                ent = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
            }
            return true;
        }


        void ConsultandoTotalVentasContado(Entidad_Caja ent, Logica_Caja log)
        {
            try
            {
                Total_ventas_contado = log.Ventas_Contado_Total(ent);
                if (Total_ventas_contado.Count > 0)
                {
                    XtotalEfectivo = Total_ventas_contado[0].Cja_Total_Efectivo;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        void ConsultandoTotalVentasCredito(Entidad_Caja ent, Logica_Caja log)
        {
            try
            {
                Total_ventas_credito = log.Ventas_Credito_Total(ent);
                if (Total_ventas_credito.Count > 0)
                {
                    XtotalCreditoEfectivo = Total_ventas_credito[0].Cja_Total_Efectivo;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void ConsultandoTotalVentasInventarioContado(Entidad_Caja ent, Logica_Caja log)
        {
            try
            {
                Total_ventas_inventario = log.Ventas_Contado_Inventario_Total(ent);
                if (Total_ventas_inventario.Count > 0)
                {
                    XtotalInventario = Total_ventas_inventario[0].Cja_Total_Efectivo;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        void ConsultandoTotalVentasInventarioCredito(Entidad_Caja ent, Logica_Caja log)
        {
            try
            {
                Total_ventas_inventario_credito = log.Ventas_Credito_Inventario_Total(ent);
                if (Total_ventas_inventario_credito.Count > 0)
                {
                    XtotalCreditoInventario = Total_ventas_inventario_credito[0].Cja_Total_Efectivo;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        void VerificarExistenciaCierreCorte()
        {
            try
            {
                Entidad_Punto_Venta ent = new Entidad_Punto_Venta();
                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Punto_Venta_PC = new List<Entidad_Punto_Venta>();

                Punto_Venta_PC = log.Buscar_Pdv_por_PC(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Pdv_Nombre_PC = Actual_Conexion.Maquina
                });

                if (Punto_Venta_PC.Count > 1)
                {
                    using (frm_puntos_ventas_asignados fi = new frm_puntos_ventas_asignados())
                    {
                        fi.Nombre_PC = Actual_Conexion.Maquina;
                        if (fi.ShowDialog() == DialogResult.OK)
                        {
                            Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

                            Entidad = fi.Punto_Venta_PC[fi.gridView1.GetFocusedDataSourceRowIndex()];
                            BSIEstablecimiento.Tag = Entidad.Est_Codigo;
                            BSIEstablecimiento.Caption = Entidad.Est_Descripcion;
                            txtpdvdesc.Tag = Entidad.Pdv_Codigo;
                            txtpdvdesc.Text = Entidad.Pdv_Nombre;
                            AlmacenCodigo = Entidad.Pdv_Almacen;

                            //VERIFICAMOS SI ESTA ASIGNADA UNA CAJA A UN PUNTO DE VENTA
                            Entidad_Caja EntCaja = new Entidad_Caja();
                            Logica_Caja logCaja = new Logica_Caja();
                            List<Entidad_Caja> Caja = new List<Entidad_Caja>();

                            EntCaja.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            EntCaja.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                            EntCaja.Cja_Codigo = null;

                            Caja = logCaja.Listar(EntCaja);
                            if (Caja.Count > 0)
                            {
                                //BUSCAMOS UNA CAJA ABIERTA 

                                // Entidad_Caja Caja_Abierta = new Entidad_Caja();
                                Logica_Caja Log_Caja_Abierta = new Logica_Caja();
                                List<Entidad_Caja> List_Caja_Abierta = new List<Entidad_Caja>();

                                EntCaja.Cja_Codigo = Caja[0].Cja_Codigo;
                                EntCaja.Cja_Aper_Cierre_Codigo = Caja[0].Cja_Aper_Cierre_Codigo;
                                EntCaja.Cja_Estado = "0058";
                                //0018    0058    01  APERTURADO
                                //0018    0059    02  CERRADO
                                List_Caja_Abierta = Log_Caja_Abierta.Listar_Caja_Aperturado(EntCaja);

                                if (List_Caja_Abierta.Count > 0)
                                {
                                    Cja_Codigo = List_Caja_Abierta[0].Cja_Codigo;
                                    Cja_Aper_Cierre_Codigo = Convert.ToInt32(List_Caja_Abierta[0].Cja_Aper_Cierre_Codigo);
                                    Cja_Estado_Abierto_Cerrado = List_Caja_Abierta[0].Cja_Estado;
                                }
                                else
                                {
                                    //SI NO HAY UNA CAJA ABIERTA SE CREARA UNO NUEVA PARA PODER VENDER
                                    using (frm_asignacion_caja_monto_pdv f = new frm_asignacion_caja_monto_pdv())
                                    {
                                        if (f.ShowDialog() == DialogResult.OK)
                                        {
                                            EntCaja.Cja_Saldo_Inicial = Convert.ToDecimal(f.txtsaldoinicial.Text);
                                            EntCaja.Cja_Estado = "0058";//estado de caja "Aperturado"
                                            if (Log_Caja_Abierta.Insertar_Aperturando_Caja(EntCaja))
                                            {
                                                Logica_Caja Logi_Caja_Abierta = new Logica_Caja();
                                                List<Entidad_Caja> Listi_Caja_Abierta = new List<Entidad_Caja>();
                                                EntCaja.Cja_Codigo = Caja[0].Cja_Codigo;
                                                EntCaja.Cja_Aper_Cierre_Codigo = Caja[0].Cja_Aper_Cierre_Codigo;
                                                EntCaja.Cja_Estado = "0058";
                                                //0018    0058    01  APERTURADO
                                                //0018    0059    02  CERRADO
                                                Listi_Caja_Abierta = Logi_Caja_Abierta.Listar_Caja_Aperturado(EntCaja);

                                                if (Listi_Caja_Abierta.Count > 0)
                                                {
                                                    Cja_Codigo = Listi_Caja_Abierta[0].Cja_Codigo;
                                                    Cja_Aper_Cierre_Codigo = Convert.ToInt32(Listi_Caja_Abierta[0].Cja_Aper_Cierre_Codigo);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Accion.Advertencia("DEBE CREAR UNA CAJA PARA ESTE PUNTO DE VENTA");
                            }
                        }
                    }
                }
                else if (Punto_Venta_PC.Count == 1)
                {
                    BSIEstablecimiento.Tag = Punto_Venta_PC[0].Est_Codigo;
                    BSIEstablecimiento.Caption = Punto_Venta_PC[0].Est_Descripcion;
                    txtpdvdesc.Tag = Punto_Venta_PC[0].Pdv_Codigo;
                    txtpdvdesc.Text = Punto_Venta_PC[0].Pdv_Nombre;
                    AlmacenCodigo = Punto_Venta_PC[0].Pdv_Almacen;

                    //VERIFICAMOS SI ESTA ASIGNADA UNA CAJA A UN PUNTO DE VENTA
                    Entidad_Caja EntCaja = new Entidad_Caja();
                    Logica_Caja logCaja = new Logica_Caja();
                    List<Entidad_Caja> Caja = new List<Entidad_Caja>();

                    EntCaja.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    EntCaja.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                    EntCaja.Cja_Codigo = null;


                    Caja = logCaja.Listar(EntCaja);
                    if (Caja.Count > 0)
                    {
                        //BUSCAMOS UNA CAJA ABIERTA 

                        Logica_Caja Log_Caja_Abierta = new Logica_Caja();
                        List<Entidad_Caja> List_Caja_Abierta = new List<Entidad_Caja>();

                        EntCaja.Cja_Codigo = Caja[0].Cja_Codigo;
                        EntCaja.Cja_Aper_Cierre_Codigo = Caja[0].Cja_Aper_Cierre_Codigo;
                        EntCaja.Cja_Estado = "0058";
                        //0018    0058    01  APERTURADO
                        //0018    0059    02  CERRADO
                        List_Caja_Abierta = Log_Caja_Abierta.Listar_Caja_Aperturado(EntCaja);

                        if (List_Caja_Abierta.Count > 0)
                        {

                            Cja_Codigo = List_Caja_Abierta[0].Cja_Codigo;
                            Cja_Aper_Cierre_Codigo = Convert.ToInt32(List_Caja_Abierta[0].Cja_Aper_Cierre_Codigo);
                            Cja_Estado_Abierto_Cerrado = List_Caja_Abierta[0].Cja_Estado;
                        }
                        else
                        {
                            //SI NO HAY UNA CAJA ABIERTA SE CREARA UNO NUEVA PARA PODER VENDER
                            using (frm_asignacion_caja_monto_pdv f = new frm_asignacion_caja_monto_pdv())
                            {
                                if (f.ShowDialog() == DialogResult.OK)
                                {
                                    EntCaja.Cja_Saldo_Inicial = Convert.ToDecimal(f.txtsaldoinicial.Text);
                                    EntCaja.Cja_Estado = "0058";//estado de caja "Aperturado"
                                    if (Log_Caja_Abierta.Insertar_Aperturando_Caja(EntCaja))
                                    {
                                        Buscar_Punto_Venta_PC();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Accion.Advertencia("DEBE CREAR UNA CAJA PARA ESTE PUNTO DE VENTA");
                    }
                }
                else
                {
                    Accion.Advertencia("Usted No tiene asignado un punto de venta");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public bool VerificarRucDni()
        {
            if (VAR_BOLETA || VAR_NOTA_SALIDA)
            {
                if (txtrucdni.Text.Trim().Length != 8)
                {
                    return false;
                }

            }
            else if (VAR_FACTURA)
            {
                if (txtrucdni.Text.Trim().Length != 11)
                {
                    return false;
                }
            }

            return true;

        }
        void Nuevo_()
        {
            try
            {
                Habilitar();
                Buscar_Punto_Venta_PC();
                Buscar_Comprabante_Punto_Venta();
                ActualizaIGV();
                Es_moneda_nac = true;
                TraerLibro();
                Cliente_Por_Defecto();
                Estado = "1";//NUEVO;
                CheckBtnNotSal.Checked = true;

                txtproductodesc.Select();

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btncancelarteclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BtnCancelar.PerformClick();
        }

        private void btnmovcaja_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtpdvdesc.Tag.ToString() != "")
                {
                    using (frm_movimiento_caja_edicion f = new frm_movimiento_caja_edicion())
                    {
                        //        public string Id_Empresa, Pdv_Codigo, Cja_Codigo;
                        //public int Cja_Aper_Cierre_Codigo;
                        f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        f.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                        f.Cja_Codigo = Cja_Codigo;
                        f.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;

                        if (f.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }
                }
                else
                {
                    Accion.Advertencia("DEBE SELECCIONAR UN PUNTO DE VENTA");
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    if (SelectItem())
                    {
                        gridView1.Tag = 1;
                        int IdActualizador;
                        if (VAR_BOLETA || VAR_FACTURA)
                        {
                            IdActualizador = gridView1.GetFocusedDataSourceRowIndex();
                        }
                        else if (VAR_NOTA_SALIDA)
                        {
                            IdActualizador = gridView1.GetFocusedDataSourceRowIndex();
                        }

                        if (VerificarModificacion())
                        {
                            using (frm_ingresar_cantidad f = new frm_ingresar_cantidad())
                            {

                                f.Establecimiento = BSIEstablecimiento.Tag.ToString();
                   

                                f.VariableTag = Convert.ToInt32(gridView1.Tag);

                                if (VAR_BOLETA || VAR_FACTURA)
                                {
                                    f.entii = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];

                                }
                                else if (VAR_NOTA_SALIDA)
                                {
                                    f.entii = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
                                }

                                f.AlmacenCod = AlmacenCodigo;
                                f.BuscarStock();
                                f.Est_Codigo = BSIEstablecimiento.Tag.ToString();
                                f.Fecha = Convert.ToDateTime(BSIFecha.Caption);


                                if (f.ShowDialog() == DialogResult.OK)
                                {
                                    if (VAR_BOLETA || VAR_FACTURA || VAR_NOTA_SALIDA)
                                    {
                                        dgvdatos.DataSource = null;

                                        foreach (Entidad_Movimiento_Cab T in Detalles_ADM)
                                        {
                                            Entidad_Movimiento_Cab Item = new Entidad_Movimiento_Cab();
                                            if (T.Adm_Item == Adm_Item)
                                            {
                                                T.Adm_Cantidad = Math.Round(Convert.ToDecimal(f.TxtCantidad.Text), 4);
                                                T.Adm_Valor_Unit = Convert.ToDecimal(f.txtprecfinal.Text);
                                                T.Adm_Total = T.Adm_Cantidad * T.Adm_Valor_Unit; //Convert.ToDecimal(f.TxtMonto.Text);
                                                //
                                            }

                                            UpdateGrilla();
                                        }

                                        // si hay lotes se tiene qeu aumenta la cantidad
                                        //del producto selecionado 

                                    foreach (Entidad_Movimiento_Cab T in Detalles_Lotes)
                                    {
                                        Entidad_Movimiento_Cab Item = new Entidad_Movimiento_Cab();
                                            if (T.Lot_Catalogo == Catalogo_Cod_Verificar && T.Lot_Adm_item == Adm_Item )
                                            {
                                                T.Lot_Cantidad = Math.Round(Convert.ToDecimal(f.TxtCantidad.Text), 4);

                                            }
                                     }


                                    }

                                    OtrosProductosOtrosDocumentos();

                                    //if (VAR_BOLETA || VAR_FACTURA)
                                    //{
                                    //    //GridView1.FocusedRowHandle = IdActualizador;
                                    //}
                                }

                            }
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No existe ningun producto para seleccionar");
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
            txtproductodesc.Focus();
        }

        private void BtnQuitar_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    if (VAR_BOLETA || VAR_FACTURA)
                    {
                        if (Detalles_ADM.Count > 0)
                        {
                            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                            Ent = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
                            Adm_Item = Ent.Adm_Item;
                            Catalogo_Cod_Verificar = Ent.Adm_Catalogo;

                            Detalles_Lotes.RemoveAll(x => x.Lot_Catalogo == Catalogo_Cod_Verificar && x.Lot_Adm_item == Adm_Item);

                            Detalles_ADM.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                            dgvdatos.DataSource = null;


                            Renumerar();

                            if (Detalles_ADM.Count > 0)
                            {
                                dgvdatos.DataSource = Detalles_ADM;
                            }

                            UpdateGrilla();

                            if (Detalles_ADM.Count > 0)
                            {
                                gridView1.MoveLastVisible();
                            }
                            else
                            {
                                dgvdatos.DataSource = null;
                            }
                        }
                    }
                    else if (VAR_NOTA_SALIDA)
                    {
                        if (Detalles_ADM.Count > 0)
                        {

                            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                            Ent = Detalles_ADM[gridView1.GetFocusedDataSourceRowIndex()];
                            Adm_Item = Ent.Adm_Item;
                            Catalogo_Cod_Verificar = Ent.Adm_Catalogo;

                            Detalles_Lotes_Inv.RemoveAll(x => x.Lot_Catalogo == Catalogo_Cod_Verificar && x.Lot_Adm_item == Adm_Item);


                            Detalles_ADM.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                            dgvdatos.DataSource = null;
                            Renumerar3();

                            if (Detalles_ADM.Count > 0)
                            {
                                dgvdatos.DataSource = Detalles_ADM;
                                Detalles_NS.Clear();

                                foreach (Entidad_Movimiento_Cab Ent_Aux in Detalles_ADM)
                                {
                                    Entidad_Movimiento_Inventario Item = new Entidad_Movimiento_Inventario();

                                    Item.Id_Item = Detalles_NS.Count + 1;

                                    Item.Invd_TipoBSA = Ent_Aux.Adm_Tipo_BSA;

                                    Item.Invd_Catalogo = Ent_Aux.Adm_Catalogo;
                                    Item.Invd_Catalogo_Desc = Ent_Aux.Adm_Catalogo_Desc;

                                    Item.Invd_Cantidad = Ent_Aux.Adm_Cantidad;
                                    Item.Invd_Valor_Unit = Ent_Aux.Adm_Valor_Unit;
                                    Item.Invd_Total = Ent_Aux.Adm_Total;
                                    Detalles_NS.Add(Item);
                                }


                            }


                            UpdateGrilla();
                            if (Detalles_ADM.Count > 0)
                            {
                                gridView1.MoveLastVisible();
                            }
                            else
                            {
                                dgvdatos.DataSource = null;
                            }
                        }


                    }
                }
                else
                {
                    Accion.Advertencia("No existe ningun producto");
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                XtotalOtrosIngresos = 0;
                XtotalOtrosEgresos = 0;
                
                VerificarExistenciaCierreCorte();
                if (BSIEstablecimiento.Tag.ToString() == "")
                {

                }
                else
                {
                    //0018    0058    01  APERTURADO
                    //0018    0059    02  CERRADO
                    if ((Cja_Aper_Cierre_Codigo != 0) & (Cja_Estado_Abierto_Cerrado == "0058"))
                    {
                        using (frm_corte_cierre_edicion f = new frm_corte_cierre_edicion())
                        {
                            f.Es_Cierre = true;
                            f.gbnumcorte.Visible = false;

                            Entidad_Caja ent = new Entidad_Caja();
                            Logica_Caja log = new Logica_Caja();

                            ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            ent.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                            ent.Cja_Codigo = Cja_Codigo;
                            ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
                            ent.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;
                            ent.Cja_Estado = Cja_Estado_Abierto_Cerrado;

                            ConsultandoTotalVentasContado(ent, log);
                            ConsultandoTotalVentasInventarioContado(ent, log);

                            ConsultandoTotalVentasCredito(ent, log);
                            ConsultandoTotalVentasInventarioCredito(ent, log);

                            Ventas_Otros_Ingresos();
                            Ventas_Otros_Egresos();

                            List<Entidad_Caja> Totales_Cortes = new List<Entidad_Caja>();

                            Totales_Cortes = log.Listar_Cortes_Totales(ent);

                            f.TxtContado.Text = Convert.ToString(Math.Round(Totales_Cortes[0].Cja_Total_Efectivo  + XtotalEfectivo + XtotalInventario, 2));
                            f.TxtTarjeta.Text = Convert.ToString(Math.Round(Totales_Cortes[0].Cja_Total_Tarjeta + XtotalTarjeta, 2));
                            f.TxtCredito.Text= Convert.ToString(Math.Round(Totales_Cortes[0].Cja_Total_Credito + XtotalCreditoEfectivo + XtotalCreditoInventario, 2));

                            f.TxtTotalVentas.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtCredito.Text), 2));

                           //f.TxtOtrosIngresos.Text = Convert.ToString(Math.Round(XtotalOtrosIngresos, 2));
                           //f.TxtOtrosEgresos.Text = Convert.ToString(Math.Round(XtotalOtrosEgresos, 2));
                            var totalOtrosIngresos = Math.Round(XtotalOtrosIngresos, 2).ToString("0.00");//Convert.ToDecimal(MBase1).ToString("0.00");
                            var totalOtrosEgresos = Math.Round(XtotalOtrosEgresos, 2).ToString("0.00");
                            f.TxtOtrosIngresos.Text = totalOtrosIngresos;
                            f.TxtOtrosEgresos.Text = totalOtrosEgresos;


                            //f.TxtTotalEfectivo.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtTarjeta.Text), 2));
                            f.TxtTotalEfectivo.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtOtrosIngresos.Text) - Convert.ToDecimal(f.TxtOtrosEgresos.Text), 2));



                            ////f.TxtContado.Text = Convert.ToString(Math.Round(XtotalEfectivo + XtotalInventario, 2));
                            ////f.TxtCredito.Text = Convert.ToString(Math.Round(XtotalCreditoEfectivo + XtotalCreditoInventario, 2));
                            ////f.TxtTotalVentas.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtCredito.Text), 2));

                            ////f.TxtOtrosIngresos.Text = Convert.ToString(Math.Round(XtotalOtrosIngresos, 2));
                            ////f.TxtOtrosEgresos.Text = Convert.ToString(Math.Round(XtotalOtrosEgresos, 2));

                            ////f.TxtTotalEfectivo.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtOtrosIngresos.Text) - Convert.ToDecimal(f.TxtOtrosEgresos.Text), 2));




                            ent.Cja_Total_Efectivo = Convert.ToDecimal(f.TxtContado.Text);
                            ent.Cja_Total_Credito = Convert.ToDecimal(f.TxtCredito.Text);

                            f.Ent = ent;


                            if ((XtotalEfectivo != 0) || (XtotalInventario != 0) || (XtotalTarjeta != 0))
                            {
                                f.XtotalEfectivo = XtotalEfectivo;
                                f.XtotalInventario = XtotalInventario;
                                f.XtotalTarjeta = XtotalTarjeta;
                                f.HacerCorte = true;
                            }



                            //public string NombreImpresora;
                            //public bool Es_Termico;
                            //public bool Es_Epson;
                            //public decimal IgvActual;
                            //public string PdvCodigo;
                            //public string Cja_Codigo;
                            //public string Cja_Codigo_Apertura;
                            f.PdvCodigo = txtpdvdesc.Tag.ToString();
                            f.Cja_Codigo = Cja_Codigo;
                            f.Cja_Codigo_Apertura = Cja_Aper_Cierre_Codigo;
                            f.NombreImpresora = NombreImpresora;
                            f.Es_Epson = EsEpson;
                            f.IgvActual = Igv_Porcentaje;

                            if (f.ShowDialog() == DialogResult.OK)
                            {
                                //AQUI DEBEMOS IMPRIMIR EL CORTE
                                //DEBEMOS MOSTRAR UN MENSAJE DE QUE EL CORTE SE REALIZO CON EXITO
                              
                             f.ImiprimirCierreTotall_();

                                

                                Nuevo_();


                            }
                        }
                    }
                    else
                    {
                        Accion.Advertencia("La caja debe estar aperturada para realizar el corte");
                    }

                }
                txtproductodesc.Focus();

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

      
        void Ventas_Otros_Ingresos()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;

                ListaOtros_Ingresos = LogAf.Buscar_Otros_Ingresos_Corte_Total_Todo(Entidad);
                if ((ListaOtros_Ingresos.Count > 0))
                {
                    XtotalOtrosIngresos = ListaOtros_Ingresos[0].Importe;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        void Ventas_Otros_Egresos()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;

                ListaOtros_Egresos = LogAf.Buscar_Otros_Egresos_Corte_Total_Todo(Entidad);
                if ((ListaOtros_Egresos.Count > 0))
                {
                    XtotalOtrosEgresos = ListaOtros_Egresos[0].Importe;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void CheckBtnNotSal_CheckedChanged(object sender, EventArgs e)
        {
            CambiarModVenta(false);
            
            VAR_NOTA_SALIDA = true;
            Cliente_Por_Defecto();
            foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXModVenta)
            {
                if (Ent_Doc.Doc_Es_Nota_Salida)
                {
                    ComprobanteCod = Ent_Doc.Doc_Tipo_Doc;
                    ComprobanteCodAsociada = Ent_Doc.Doc_Tipo_Doc_Asoc;
                    ComprobanteDescripcion = Ent_Doc.Nombre_Comprobante;
                    BSITipoDocumento.Text = Ent_Doc.Nombre_Comprobante;
                }
            }
            PopControlConModVenta.HidePopup();
            BuscarSerieNumDocPtoVta(ComprobanteCod);
            OtrosProductosOtrosDocumentos();
            txtproductodesc.Focus();

        }

        private void CheckBtnTicketBol_CheckedChanged(object sender, EventArgs e)
        {
            CambiarModVenta(false);

            if (CheckBtnTicketBol.Checked)
            {
                    VAR_BOLETA = true;
                    Cliente_Por_Defecto();
                    foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXModVenta)
                    {
                        if (Ent_Doc.Doc_Es_Boleta)
                        {
                            ComprobanteCod = Ent_Doc.Doc_Tipo_Doc;
                            ComprobanteCodAsociada = Ent_Doc.Doc_Tipo_Doc_Asoc;
                            ComprobanteDescripcion = Ent_Doc.Nombre_Comprobante;
                            BSITipoDocumento.Text = Ent_Doc.Nombre_Comprobante;

                            Elect_Documento_Descripcion = Ent_Doc.Nombre_Comprobante;
                            Elect_Documento_Sunat = Ent_Doc.Id_Sunat;

                        }
                    }
                    PopControlConModVenta.HidePopup();
                    BuscarSerieNumDocPtoVta(ComprobanteCod);

                    Longitud_RUC_DNI();
                    txtproductodesc.Focus();
            }

        }

        private void CheckBtnTicketFac_CheckedChanged(object sender, EventArgs e)
        {
            CambiarModVenta(false);
            VAR_FACTURA = true;
            txtrucdni.ResetText();
            txtclientedesc.ResetText();
            txtdireccion.ResetText();
            foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXModVenta)
            {
                if (Ent_Doc.Doc_Es_Factura)
                {
                    ComprobanteCod = Ent_Doc.Doc_Tipo_Doc;
                    ComprobanteCodAsociada = Ent_Doc.Doc_Tipo_Doc_Asoc;
                    ComprobanteDescripcion = Ent_Doc.Nombre_Comprobante;
                    BSITipoDocumento.Text = Ent_Doc.Nombre_Comprobante;

                    Elect_Documento_Descripcion = Ent_Doc.Nombre_Comprobante;
                    Elect_Documento_Sunat = Ent_Doc.Id_Sunat;
                }
            }
            PopControlConModVenta.HidePopup();
            BuscarSerieNumDocPtoVta(ComprobanteCod);
            Longitud_RUC_DNI();
            txtproductodesc.Focus();
        }

        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtrucdni.Text.Trim().ToString()))
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {
                            if (VAR_BOLETA || VAR_NOTA_SALIDA)
                            {

                            }

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];
                                txtrucdni.Text = Entidad.Ent_RUC_DNI.ToString().Trim();
                                txtclientedesc.Text = Entidad.Ent_Razon_Social_Nombre.Trim() + " " + Entidad.Ent_Ape_Paterno.Trim() + " " + Entidad.Ent_Ape_Materno.Trim();
                                txtdireccion.Text = Entidad.Ent_Domicilio_Fiscal.Trim();

                                //DireccionCodigo = Entidad.Ent_DirecPrincipal;
                                Correo_Electronico = Entidad.Ent_Correo;
                                Tipo_Doc_SUNAT_Cliente = Entidad.Ent_Tipo_Doc_Cod_Interno;

                            }
                        }
                    }
                    else if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtclientedesc.Text))
                    {
                        if (VerificarRucDni())
                        {
                            Logica_Entidad LogEnt = new Logica_Entidad();
                            Entidad_Entidad Ent = new Entidad_Entidad();
                            List<Entidad_Entidad> Entidades = new List<Entidad_Entidad>();

                            if (VAR_BOLETA || VAR_NOTA_SALIDA)
                            {
                                TipoDoc = "1";
                            }
                            else if (VAR_FACTURA)
                            {
                                TipoDoc = "6";
                            }
                            Ent.Ent_RUC_DNI = txtrucdni.Text.Trim().ToString();
                            Entidades = LogEnt.Listar(Ent);
                            if (Entidades.Count > 0)
                            {
                                foreach (Entidad_Entidad T in Entidades)
                                {
                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                    {
                                        txtrucdni.Text = T.Ent_RUC_DNI.ToString().Trim();
                                        txtclientedesc.Text = T.Ent_Razon_Social_Nombre.Trim() + " " + T.Ent_Ape_Paterno.Trim() + " " + T.Ent_Ape_Materno.Trim();
                                        txtdireccion.Text = T.Ent_Domicilio_Fiscal.Trim();

                                        Correo_Electronico = T.Ent_Correo;
                                        Tipo_Doc_SUNAT_Cliente = T.Ent_Tipo_Doc_Cod_Interno;

                                    }
                                }
                            }
                            if (string.IsNullOrEmpty(txtclientedesc.Text))
                            {
                                e.Handled = false;
                                using (frm_registrar_cliente f = new frm_registrar_cliente())
                                {
                                    f.RUCDNI_NoExist = txtrucdni.Text.Trim().ToString();
                                    if (VAR_BOLETA || VAR_NOTA_SALIDA)
                                    {
                                        f.TipoPersona = "01";//NATURAL
                                        f.TipoDoc = "1";//DOCUMENTO NACIONAL DE IDENTIDAD(DNI)

                                        //if (Config_Ruta_Email.InternetConnection())
                                        //{
                                        //    try
                                        //    {
                                        //        Entidad_Entidad S = new Entidad_Entidad();
                                        //        Retornar_Reniec Reniec = new Retornar_Reniec();
                                        //        S = Reniec.Retornar_Reniec(txtrucdni.Text.Trim().ToString());

                                        //        if ((string.IsNullOrWhiteSpace(S.Ent_Razon_Social_Nombre) || (S.Ent_Razon_Social_Nombre == null)) && (S.Estado_No_Existe == false))
                                        //        {
                                        //            S = Reniec.Retornar_Reniec(txtrucdni.Text.Trim().ToString());
                                        //        }

                                        //        if (S.Estado_No_Existe) {
                                        //            f.Close();
                                        //        }
                                        //        if (S.Estado_De_Consulta)
                                        //        {
                                        //            f.TxtEnt1Ape.Text = S.Ent_Ape_Materno;
                                        //            f.TxtEnt2Ape.Text = S.Ent_Ape_Materno;
                                        //            f.TxtEntNombre.Text = S.Ent_Razon_Social_Nombre;
                                        //            f.TxtDirecionCompleta.Text = S.Ent_Domicilio_Fiscal;

                                        //        }
                                        //    }
                                        //    catch (Exception ex)
                                        //    {
                                        //        Accion.ErrorSistema("ERROR DE CONEXION A RENIEC");
                                        //    }
                                        //}

                                        Entidad_Entidad S = new Entidad_Entidad();
                                        Consulta_Entidad_Linea Linea = new  Consulta_Entidad_Linea();

                                       S = Linea.Buscar_Entidad_Linea(txtrucdni.Text.Trim());
                                        if (S.Ent_Razon_Social_Nombre != "")
                                        {
                                            f.TxtEnt1Ape.Text = S.Ent_Ape_Paterno;
                                            f.TxtEnt2Ape.Text = S.Ent_Ape_Materno;
                                            f.TxtEntNombre.Text = S.Ent_Razon_Social_Nombre;
                                            f.TxtDirecionCompleta.Text = S.Ent_Domicilio_Fiscal;

                                        }

                                        if (f.ShowDialog() == DialogResult.OK)
                                        {
                                            Logica_Entidad Log = new Logica_Entidad();
                                            Entidad_Entidad En = new Entidad_Entidad();
                                            List<Entidad_Entidad> Enti = new List<Entidad_Entidad>();

                                            En.Ent_RUC_DNI = txtrucdni.Text.Trim().ToString();
                                            Enti = LogEnt.Listar(En);
                                            if (Enti.Count > 0)
                                            {
                                                foreach (Entidad_Entidad T in Enti)
                                                {
                                                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                                    {
                                                        txtrucdni.Text = T.Ent_RUC_DNI.ToString().Trim();
                                                        txtclientedesc.Text = T.Ent_Razon_Social_Nombre.Trim() + " " + T.Ent_Ape_Paterno.Trim() + " " + T.Ent_Ape_Materno.Trim();
                                                        txtdireccion.Text = T.Ent_Domicilio_Fiscal.Trim();

                                                        Correo_Electronico = T.Ent_Correo;
                                                        Tipo_Doc_SUNAT_Cliente = T.Ent_Tipo_Doc_Cod_Interno;

                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            f.Close();
                                        }

                                    }
                                    else if (VAR_FACTURA)
                                    {
                                        if (ValidarRUC.validarRuc(txtrucdni.Text.Trim()) == true)
                                        {
                                            PersonaNJ = txtrucdni.Text.Substring(0, 2);
                                            if ((PersonaNJ == "10" || PersonaNJ == "15" || PersonaNJ == "17"))
                                            {
                                                f.TipoPersona = "01";
                                                f.TipoDoc = "6";
                                            }
                                            else if (PersonaNJ == "20")
                                            {
                                                f.TipoPersona = "02";
                                                f.TipoDoc = "6";
                                            }

                                            //if (Config_Ruta_Email.InternetConnection())
                                            //{
                                            //    Entidad_Entidad S = new Entidad_Entidad();
                                            //    Retornar_Sunat Sunat = new Retornar_Sunat();
                                            //    S = Sunat.Retornar_Sunat(txtrucdni.Text.Trim().ToString());

                                            //    if ((string.IsNullOrWhiteSpace(S.Ent_Razon_Social_Nombre) || (S.Ent_Razon_Social_Nombre == null)) && (S.Estado_No_Existe == false))
                                            //    {
                                            //        S = Sunat.Retornar_Sunat(txtrucdni.Text.Trim().ToString());
                                            //    }

                                            //    if (S.Estado_No_Existe)
                                            //    {
                                            //        f.Close();
                                            //    }

                                            //    if (S.Estado_De_Consulta)
                                            //    {
                                            //        f.TxtEnt1Ape.Text = S.Ent_Ape_Materno;
                                            //        f.TxtEnt2Ape.Text = S.Ent_Ape_Materno;
                                            //        f.TxtEntNombre.Text = S.Ent_Razon_Social_Nombre;
                                            //        f.TxtDirecionCompleta.Text = S.Ent_Domicilio_Fiscal;
                                            //    }

                                            //}

                                            Entidad_Entidad S = new Entidad_Entidad();
                                            Consulta_Entidad_Linea Linea = new Consulta_Entidad_Linea();

                                            S = Linea.Buscar_Entidad_Linea(txtrucdni.Text.Trim());
                                            if (S.Ent_Razon_Social_Nombre != "")
                                            {
                                                f.TxtEnt1Ape.Text = S.Ent_Ape_Paterno;
                                                f.TxtEnt2Ape.Text = S.Ent_Ape_Materno;
                                                f.TxtEntNombre.Text = S.Ent_Razon_Social_Nombre;
                                                f.TxtDirecionCompleta.Text = S.Ent_Domicilio_Fiscal;

                                            }

                                            if (f.ShowDialog() == DialogResult.OK)
                                            {
                                                Logica_Entidad Log = new Logica_Entidad();
                                                Entidad_Entidad En = new Entidad_Entidad();
                                                List<Entidad_Entidad> Enti = new List<Entidad_Entidad>();

                                                En.Ent_RUC_DNI = txtrucdni.Text.Trim().ToString();
                                                Enti = LogEnt.Listar(En);
                                                if (Enti.Count > 0)
                                                {
                                                    foreach (Entidad_Entidad T in Enti)
                                                    {
                                                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                                        {
                                                            txtrucdni.Text = T.Ent_RUC_DNI.ToString().Trim();
                                                            txtclientedesc.Text = T.Ent_Razon_Social_Nombre.Trim() + " " + T.Ent_Ape_Paterno.Trim() + " " + T.Ent_Ape_Materno.Trim();
                                                            txtdireccion.Text = T.Ent_Domicilio_Fiscal.Trim();

                                                            Correo_Electronico = T.Ent_Correo;
                                                            Tipo_Doc_SUNAT_Cliente = T.Ent_Tipo_Doc_Cod_Interno;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Accion.Advertencia("EL NUMERO DE RUC NO ES CORRECTO");
                                        }
                                    }
                                }
                            }
                        }  else
                            {
                                Accion.Advertencia("EL NUMERO DE DNI NO ES CORRECTO");
                            }

                    }
                  
                }

                catch (Exception ex)
                {
                    //Accion.ErrorSistema(ex.Message);

                }
            }
        }

        private void txtrucdni_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdni.Focus() == false)
            {
                txtclientedesc.ResetText();
                txtdireccion.ResetText();
            }
        }

        private void btnmovcajateclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnmovcaja.PerformClick();
        }

        private void btnmodificarteclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BtnModificar.PerformClick();
        }

        private void btnquitarteclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BtnQuitar.PerformClick();
        }

        private void btncierretotalcajateclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BtnCerrar.PerformClick();
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            BtnModificar.PerformClick();
        }

        private void frm_pos_edicion_Load(object sender, EventArgs e)
        {
            try
            {
               Bloquear();

                CheckBtnNotSal.Enabled = false;
                CheckBtnTicketBol.Enabled = false;
                CheckBtnTicketFac.Enabled = false;
                Limpiar();
                Nuevo_();


                TraerFechaServidor();
            }catch(Exception ex)
            {

            }
 
        }


        string ProductoDescripcion;

        private void BtnCorte_Click(object sender, EventArgs e)
        {
            XtotalOtrosIngresos = Convert.ToDecimal(0.00);
            XtotalOtrosEgresos =Convert.ToDecimal(0.00);
            using (frm_corte_cierre_edicion f = new frm_corte_cierre_edicion())
            {

                VerificarExistenciaCierreCorte();

                f.Es_Corte = true;
                if (BSIEstablecimiento.Tag.ToString() == "")
                {
                    Accion.Advertencia("Debe seleccionar un punto de venta");
                    Bloquear();
                    Limpiar();
                    dgvdatos.DataSource = null;
                    Detalles_ADM.Clear();
                    Detalles_CONT.Clear();
                    TxtRsBaseImponible.Text = "0.00";
                    TxtRsIGVMonto.Text = "0.00";
                    TxtRsImporteTotal.Text = "0.00";

                    btnuevo.Select();
                }
                else
                {
                    Entidad_Caja ent = new Entidad_Caja();
                    Logica_Caja log = new Logica_Caja();

                    ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

                    ent.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                    ent.Cja_Codigo = Cja_Codigo;
                    ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
                    ent.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;
                    ent.Cja_Estado = Cja_Estado_Abierto_Cerrado;
                    //0018    0058    01  APERTURADO
                    //0018    0059    02  CERRADO
                    if ((Cja_Aper_Cierre_Codigo != 0) & (Cja_Estado_Abierto_Cerrado == "0058"))
                    {
                        f.txtpdvdesc.Tag = txtpdvdesc.Tag.ToString();
                        f.txtpdvdesc.Text = txtpdvdesc.Text;

                        int cantidadCortes = log.Listar_Caja_Aperturado_DET(ent).Count;
                        if (cantidadCortes == 0)
                        {
                            f.TxtNumItem.Text = Convert.ToString(1);
                        }
                        else
                        {
                            f.TxtNumItem.Text = Convert.ToString(cantidadCortes + 1);
                        }

                        ConsultandoTotalVentasContado(ent, log);
                        ConsultandoTotalVentasInventarioContado(ent, log);

                        ConsultandoTotalVentasCredito(ent, log);
                        ConsultandoTotalVentasInventarioCredito(ent, log);

                        Ventas_Otros_Ingresos_Corte();
                        Ventas_Otros_Egresos_Corte();


                        f.TxtContado.Text = Convert.ToString(Math.Round(XtotalEfectivo + XtotalInventario, 2));
                        f.TxtCredito.Text = Convert.ToString(Math.Round(XtotalCreditoEfectivo + XtotalCreditoInventario, 2));
                        f.TxtTotalVentas.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtCredito.Text),2));

                        //var sssss = string.Format("{0:0.00}", Convert.ToString(Math.Round(XtotalOtrosIngresos, 2)));
                        var totalOtrosIngresos = Math.Round(XtotalOtrosIngresos, 2).ToString("0.00");//Convert.ToDecimal(MBase1).ToString("0.00");
                        var totalOtrosEgresos = Math.Round(XtotalOtrosEgresos, 2).ToString("0.00");
                        f.TxtOtrosIngresos.Text = totalOtrosIngresos;
                        f.TxtOtrosEgresos.Text = totalOtrosEgresos;
                        //string.Format("{0:0.00}", myNumber);
                        f.TxtTotalEfectivo.Text = Convert.ToString(Math.Round(Convert.ToDecimal(f.TxtContado.Text) + Convert.ToDecimal(f.TxtOtrosIngresos.Text) - Convert.ToDecimal(f.TxtOtrosEgresos.Text), 2));



                        if (f.ShowDialog() == DialogResult.OK)
                        {
                            ent.Cja_Total_Efectivo = XtotalEfectivo + XtotalInventario;
                            ent.Cja_Total_Credito = XtotalCreditoEfectivo + XtotalCreditoInventario;

                            try
                            {

                                f.PdvCodigo = txtpdvdesc.Tag.ToString();
                                f.Cja_Codigo = Cja_Codigo;
                                f.Cja_Codigo_Apertura = Cja_Aper_Cierre_Codigo;
                                f.Cja_Aper_Cierre_Codigo_Item = Convert.ToInt32(f.TxtNumItem.Text); //'Cja_Aper_Cierre_Codigo_Item;
                                f.NombreImpresora = NombreImpresora;
                                f.Es_Epson = EsEpson;
                                f.IgvActual = Igv_Porcentaje;
                                f.Ent = ent;

                                if (log.Insertar_Apertura_Det(ent))
                                {
                                    //AQUI DEBE IR UNMESNAJE EL CUAL DIGA QUE EL CORTE SE REALIZO CORRECTAMENTE

                                   f.ImiprimirCierreTotall_();

                             

                                    Nuevo_();
                                }
                                else
                                {
                                    Accion.Advertencia("No se pudo Insertar el corte");
                                }
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        Accion.Advertencia("La caja debe estar aperturado,para realizar el corte");
                    }
                }
            }
            txtproductodesc.Focus();
        }


        List<Entidad_Caja> ListaOtros_Ingresos = new List<Entidad_Caja>();
        void Ventas_Otros_Ingresos_Corte()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = txtpdvdesc.Tag.ToString(); 
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
               // Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaOtros_Ingresos = LogAf.Buscar_Otros_Ingresos_Corte_Total_Before(Entidad);
                if ((ListaOtros_Ingresos.Count > 0))
                {
                    XtotalOtrosIngresos= ListaOtros_Ingresos[0].Importe;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        List<Entidad_Caja> ListaOtros_Egresos = new List<Entidad_Caja>();
        void Ventas_Otros_Egresos_Corte()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = txtpdvdesc.Tag.ToString();
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaOtros_Egresos = LogAf.Buscar_Otros_Egresos_Corte_Total_Before(Entidad);
                if ((ListaOtros_Egresos.Count > 0))
                {
                    XtotalOtrosEgresos = ListaOtros_Egresos[0].Importe;
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void PopControlConModVenta_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnvistapreviaImpresion_Click(object sender, EventArgs e)
        {
            if (VAR_NOTA_SALIDA)
            {
                if (Detalles_NS.Count > 0)
                {
                     VistaPrevia_Imprimir_Ticket();
                }
                else
                {
                    Accion.Advertencia("No existe detalles");
                }

          

            }else if (VAR_BOLETA || VAR_FACTURA)
            {

                Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Id_Anio = Actual_Conexion.AnioSelect;
                Ent.Id_Periodo = Actual_Conexion.PeriodoSelect;
                Ent.Id_Libro = Id_Libro;
                Ent.Id_Voucher = "";

                Ent.Ctb_Glosa = "POR VENTAS AL CONTADO";

                Ent.Ctb_Tipo_Doc_Sunat = Elect_Documento_Sunat.Trim();
                Ent.Ctb_Tipo_Doc = ComprobanteCod;
                Ent.Nombre_Comprobante = Elect_Documento_Descripcion;

                Ent.ctb_pdv_codigo = txtpdvdesc.Tag.ToString();

                Ent.Ctb_Serie = BSISerieDocum.Text;
                Ent.Ctb_Numero = BSINumeroDocum.Text;


                Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(BSIFecha.Caption);
                Ent.Ctb_Tipo_Ent = "C";

                Ent.Ctb_Ruc_dni = txtrucdni.Text;
                Ent.Ent_TpDcEnt_SUNAT = Tipo_Doc_SUNAT_Cliente;
                Ent.Correo_Cliente = Correo_Electronico;
                Ent.Cliente_Razon_Social = txtclientedesc.Text.Trim();
                Ent.Cliente_Direccion = txtdireccion.Text.Trim();


                Ent.Ctb_Condicion_Cod = "0006";// CONTADO
                Ent.Ctn_Moneda_Cod = "001";//SOLES

                Ent.Ctb_Tipo_Cambio_Cod = "SVC";
                Ent.Ctb_Tipo_Cambio_desc = "SIN CONVERSION";
                Ent.Ctb_Tipo_Cambio_Valor = 1;

                Ent.Ctb_Base_Imponible = Convert.ToDecimal(TxtRsBaseImponible.Text);
                Ent.Ctb_Valor_Fact_Exonerada = Convert.ToDecimal(TxtRsValExporta.Text);
                Ent.Ctb_Exonerada = Convert.ToDecimal(TxtRsExonerada.Text);
                Ent.Ctb_Inafecta = Convert.ToDecimal(TxtRsInAfecta.Text);

                Ent.Ctb_Isc = Convert.ToBoolean(IIf(Convert.ToDecimal(TxtRsISC.Text) == 0, false, true));
                Ent.Ctb_Isc_Importe = Convert.ToDecimal(TxtRsISC.Text);

                Ent.Ctb_Igv = Convert.ToDecimal(TxtRsIGVMonto.Text);

                Ent.Ctb_Otros_Tributos = Convert.ToBoolean(IIf(Convert.ToDecimal(TxtRsOtrosTribCarg.Text) == 0, false, true));
                Ent.Ctb_Otros_Tributos_Importe = Convert.ToDecimal(TxtRsOtrosTribCarg.Text);

                Ent.Ctb_Importe_Total = Convert.ToDecimal(TxtRsImporteTotal.Text);

                Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime("01/01/1900");



                Ent.Ctb_Tasa_IGV = Convert.ToDecimal(Igv_Porcentaje);
                Ent.Ctb_Analisis = Id_Analisis;
                Ent.Cja_Codigo = Cja_Codigo;
                Ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
                Ent.Cja_Aper_Cierre_Codigo_Item = 0;
                Ent.cja_estado = "0058";

                //detalle Contable
                Ent.DetalleAsiento = Detalles_CONT;
                //Detalle Administrativo

                Ent.DetalleADM = Detalles_ADM;

                Ent.DetalleLotes = Detalles_Lotes;

                if (Detalles_ADM.Count > 0)
                {
                      VistaPreviaImpresion_Boleta_Factura_Electronica(Ent);
                }
                else
                {
                    Accion.Advertencia("No existe detalles");
                }


            }


        }

        void VistaPrevia_Imprimir_Ticket()
        {
            try
            {
            Comercial.Rpte_Comprobante_No_Electronico RptPOS = new Comercial.Rpte_Comprobante_No_Electronico();
            List<Entidad_Movimiento_Inventario> listRpt = new List<Entidad_Movimiento_Inventario>();
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();

            Ent.Detalle = Detalles_NS;
            listRpt.Add(Ent);

            RptPOS.lbl_Emisor_Apellidos_RazonSocial.Text = Actual_Conexion.EmpresaNombre.Trim();
            RptPOS.lbl_Emisor_Documento_Numero.Text = Actual_Conexion.RucEmpresa.Trim();
            RptPOS.lbl_TipoDocumento_Descripcion.Text = "TICKET INTERNO";
            RptPOS.lbl_TipoDocumento_Serie_Numero.Text = BSISerieDocum.Text + "-" + BSINumeroDocum.Text;

            //Convert.ToDateTime(BSIFecha.Caption);
            RptPOS.lbl_fecha_emision.Text = Convert.ToString(Convert.ToDateTime(BSIFecha.Caption));
            RptPOS.lbl_cliente.Text = txtclientedesc.Text;
            RptPOS.lbl_dni.Text = txtrucdni.Text;
            RptPOS.lbl_direccion.Text = txtdireccion.Text;

            RptPOS.lblisc.Text = Convert.ToString(Math.Round(Convert.ToDecimal(TxtRsISC.Text), 2));
            RptPOS.lblinafecta.Text = Convert.ToString(Math.Round(Convert.ToDecimal(TxtRsInAfecta.Text), 2));
            RptPOS.lblexonerada.Text = Convert.ToString(Math.Round(Convert.ToDecimal(TxtRsExonerada.Text), 2));
            RptPOS.lblotroscargos.Text = Convert.ToString(Math.Round(Convert.ToDecimal(TxtRsOtrosTribCarg.Text), 2));

            RptPOS.lbl_op_gravadas.Text = Convert.ToString(Convert.ToDecimal(TxtRsBaseImponible.Text));
            RptPOS.lbl_igv.Text = Convert.ToString(Convert.ToDecimal(TxtRsIGVMonto.Text));
            RptPOS.lbl_total.Text = Convert.ToString(Convert.ToDecimal(TxtRsImporteTotal.Text));





            int HeightDoc = 400;
            HeightDoc = HeightDoc + (32 * Detalles_NS.Count());
            RptPOS.PageHeight = HeightDoc;

                //If Doc.Dvt_Medios_Pago.Count > 0 Then
                //    RptPOS.DetailPagoTarjeta.Visible = True
                //    HeightDoc = HeightDoc + 35 + (18 * listRpt(0).PagoTarjeta.Count)
                //End If


            RptPOS.Watermark.ImageAlign = ContentAlignment.TopCenter;
            RptPOS.Watermark.Text = "VISTA PREVIA";

                RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                RptPOS.DataSource = Detalles_NS;

                RptPOS.CreateDocument();

                //dynamic ribbonPreview = new PrintPreviewFormEx();
                //RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                //ribbonPreview.PrintingSystem = RptPOS.PrintingSystem;
                // ribbonPreview.Show();
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
               // RptPOS.PrintingSystem.SaveDocument(stream);

                RptPOS.ExportToPdf(stream);
                using (frm_vista_previa_pos f = new frm_vista_previa_pos())
                  {
                    f.pdfViewer1.LoadDocument(stream);

                    if (f.ShowDialog() == DialogResult.OK)
                    {

                    }

                  }
 
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        void VistaPreviaImpresion_Boleta_Factura_Electronica(Entidad_Movimiento_Cab Doc)
        {
            try
            {


                string Modelo_Impresion = "000";
                Nombre = Modelo_Impresion;

                // Warning!!! Optional parameters not supported
                DocumentoElectronico Doc_Electronico_Bol_Fact;
                Doc_Electronico_Bol_Fact = Convertir_Cliente_Objet(Doc);
 
    
                    Print_Invoice_POS RptPOS = new Print_Invoice_POS();
                    List<DocumentoElectronico> listRpt = new List<DocumentoElectronico>();
                    listRpt.Add(Doc_Electronico_Bol_Fact);

                    if (listRpt[0].Cliente_Documento_Numero.Length != 11)
                    {
                        //RptPOS.XrLabel13.Text = "DNI:";
                        ////'Cuando es boleta ocultamos estos campos
                        //RptPOS.XrLabel19.Visible = false;
                        //RptPOS.XrLabel20.Visible = false;
                        //RptPOS.XrLabel5.Visible = false;
                        //RptPOS.XrLabel6.Visible = false;
                        //RptPOS.XrLabel23.Visible = false;
                        //RptPOS.XrLabel30.Visible = false;
                    }



                    RptPOS.lblplaca.Visible = false;
                    RptPOS.lblhash.Visible = false;
                    RptPOS.CodigoBarras.Visible = false;
                    RptPOS.AutorizacionSunat.Visible = false;
                    RptPOS.DetailPagoTarjeta.Visible = false;
                    RptPOS.DatosVendedor.Visible = false;

                int HeightDoc = 1000;
                HeightDoc = HeightDoc + (32 * Detalles_ADM.Count());
                RptPOS.PageHeight = HeightDoc;

                RptPOS.Watermark.Text="VISTA PREVIA";
                RptPOS.Watermark.ImageAlign = ContentAlignment.TopCenter;

                RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                RptPOS.DataSource = listRpt;

                RptPOS.CreateDocument();

                //dynamic ribbonPreview = new PrintPreviewFormEx();
                //RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                //ribbonPreview.PrintingSystem = RptPOS.PrintingSystem;
                //ribbonPreview.Show();

                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                // RptPOS.PrintingSystem.SaveDocument(stream);

                RptPOS.ExportToPdf(stream);
                using (frm_vista_previa_pos f = new frm_vista_previa_pos())
                {
                    f.pdfViewer1.LoadDocument(stream);

                    if (f.ShowDialog() == DialogResult.OK)
                    {

                    }

                }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }



        decimal CantidadTotal;
        string Unidad_Medida;

        private void txtproductodesc_Enter(object sender, EventArgs e)
        {
            txtproductodesc.BackColor = Color.FromArgb(255, 251, 149);
        }

        private void txtproductodesc_Leave(object sender, EventArgs e)
        {
            txtproductodesc.BackColor = Color.White;
        }

        private void btnmodificarcliente_Click(object sender, EventArgs e)
        {
            Logica_Entidad LogEnt = new Logica_Entidad();

            using (frm_modificar_cliente f = new frm_modificar_cliente())
            {
                f.rucdni = txtrucdni.Text.Trim().ToString();
 

                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        txtrucdni.Text = f.TxtTipDocNumer.Text.ToString().Trim();
                        txtclientedesc.Text = f.TxtEntNombre.Text.Trim() + " " + f.TxtEnt1Ape.Text.Trim() + " " + f.TxtEnt2Ape.Text.Trim();
                        txtdireccion.Text = f.TxtDirecionCompleta.Text.Trim();

                        Correo_Electronico = "";
                        Tipo_Doc_SUNAT_Cliente = f.Ent_Tipo_Doc_Cod_Interno;
                    }
            }
        }

        private void gbdatoscliente_Enter(object sender, EventArgs e)
        {

        }

        string Unidad_Medida_FE;
        private void txtproductodesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                char vari;
                vari = Convert.ToChar("\r");


                if (!string.IsNullOrEmpty(txtproductodesc.Text.Trim().ToString()))
                {
                    if (((e.KeyChar == (char)13 && (txtproductodesc.Text.Substring((txtproductodesc.Text.Length - 1), 1) == "*"))))
                    {

                        // MODIFICADO PARA VENDER POR LOTES 
                        using (frm_producto_stock_lote f = new frm_producto_stock_lote())
                        {
                            f.Id_Almacen = AlmacenCodigo;
                            f.BSIFecha = BSIFecha.Caption;
                            f.Establecimiento = BSIEstablecimiento.Tag.ToString();
                            f.Igv_Porcentaje = Igv_Porcentaje;

                            if (f.ShowDialog() == DialogResult.OK)
                            {

                                List<Entidad_Catalogo> Detalles = new List<Entidad_Catalogo>();
                                Detalles = f.Lista_Pre_Seleccionada;

                                IngresarCantidadOtrosProductosNuevo(Detalles);
                               
                                txtproductodesc.ResetText();
                                txtproductodesc.Focus();
                                txtproductodesc.Text.TrimEnd();
                            }
                        }
                        ////////////////////////////


                     

                    }
                    else if (e.KeyChar == (char)13)
                    {
                        try
                        {
                            Logica_Catalogo log = new Logica_Catalogo();
                            List<Entidad_Catalogo> ConsulTaStock = new List<Entidad_Catalogo>();
                            ConsulTaStock = log.Consultar_Stock_Pro_Codigo_Barra(new Entidad_Catalogo()
                            {
                                Id_Empresa = Actual_Conexion.CodigoEmpresa,
                                Id_Anio = Actual_Conexion.AnioSelect,
                                Id_Tipo = "0031",//BIEN
                                Id_Almacen = AlmacenCodigo,
                                Cat_Barra = txtproductodesc.Text.Trim().ToString()
                            });

                            if ((ConsulTaStock.Count > 0))
                            {
                                foreach (Entidad_Catalogo T in ConsulTaStock)
                                {
                                    if (T.Stock > 0)
                                    {
                                        Entidad_Catalogo Product = new Entidad_Catalogo();
                                        Product = T;
                                        ProductoDescripcion = Product.Cat_Descripcion;
                                        CantidadTotal = Product.Stock;

                                        //AQUI BUSCAMOS LAS PRESENTACIONES DEL PRODUCTO

                                        Logica_Catalogo logi = new Logica_Catalogo();
                                        List<Entidad_Catalogo> Lista_Presentacion = new List<Entidad_Catalogo>();
                                        Entidad_Catalogo pre = new Entidad_Catalogo();
                                        pre.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                                        pre.Id_Tipo = "0031";//BIEN
                                        pre.Id_Catalogo = Product.Id_Catalogo;

                                        Lista_Presentacion = logi.Consultar_Producto_Y_Sus_Presentaciones(pre);
                                      
                                        Entidad_Catalogo Product_Presentacion = new Entidad_Catalogo();

                                        if (Lista_Presentacion.Count() > 0)
                                        {

                                        using (Busquedas_Generales.frm_presentacion_producto ff = new Busquedas_Generales.frm_presentacion_producto())
                                            {
                                                ff.Id_Almacen = AlmacenCodigo;
                                                ff.BSIFecha = BSIFecha.Caption;
                                                ff.Establecimiento = BSIEstablecimiento.Tag.ToString();
                                                ff.ListaFiltrada_Por_Presentacion = Lista_Presentacion;


                                                if (ff.ShowDialog() == DialogResult.OK)
                                                {
 
                                                    List<Entidad_Catalogo> Detalles = new List<Entidad_Catalogo>();
                                                    Detalles = ff.Lista_Pre_Seleccionada;

                                                    IngresarCantidadOtrosProductosNuevo(Detalles);
                         
                                                        
                                                    txtproductodesc.ResetText();
                                                    txtproductodesc.Focus();
                                                    txtproductodesc.Text.TrimEnd();
                                                }
                                                else
                                                {
                                                    txtproductodesc.ResetText();
                                                    txtproductodesc.Focus();
                                                    txtproductodesc.Text.TrimEnd();
                                                }
                                            }
                                        }
                           
 
                                    }
                                    else
                                    {
                                        txtproductodesc.ResetText();
                                        txtproductodesc.Focus();
                                        txtproductodesc.Text.TrimEnd();
                                        Accion.Advertencia("El Producto" + " " + T.Cat_Descripcion + " " + "no tiene stock en el sistema");
                                        break;
                                    }

                                }

                            }
                            else
                            {
                                txtproductodesc.ResetText();
                                txtproductodesc.Focus();
                                txtproductodesc.Text.TrimEnd();

                                Accion.Advertencia("El Producto no existe o no esta registrado en el sistema");
                            }

                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Bloquear();
            Limpiar();
            dgvdatos.DataSource = null;
            Detalles_ADM.Clear();
            Detalles_CONT.Clear();
            Detalles_NS.Clear();
            TxtRsBaseImponible.Text = "0.00";
            TxtRsIGVMonto.Text = "0.00";
            TxtRsImporteTotal.Text = "0.00";
            TxtRsValExporta.Text = "0.00";
            TxtRsOtrosTribCarg.Text = "0.00";
            TxtRsExonerada.Text = "0.00";
            TxtRsInAfecta.Text = "0.00";
            TxtRsISC.Text = "0.00";

            btnuevo.Select();

            Nuevo_();
        }



        Entidad_Movimiento_Cab SoloVistaPrevia = new Entidad_Movimiento_Cab();
        private void btncobrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Verificar_Detalles())
                {
                    if (VAR_NOTA_SALIDA)
                    {
                        
                        using (frm_cobrar f = new frm_cobrar())
                        {
                            f.RucDni = txtrucdni.Text.Trim();
                            f.TxtImporteTotal.Text = TxtRsImporteTotal.Text;
                            f.TxtEntrega.Text = TxtRsImporteTotal.Text;
                            if (f.ShowDialog() == DialogResult.OK)
                            {
                                Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
                                Logica_Movimiento_Inventario Log = new Logica_Movimiento_Inventario();

                                DateTime dt = Convert.ToDateTime(BSIFecha.Caption);

                                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                                Ent.Id_Anio = Convert.ToString(dt.Year); // Actual_Conexion.AnioSelect;
                                Ent.Id_Periodo =Convert.ToString((dt.Month < 10 ? "0" : "") + dt.Month); //Actual_Conexion.PeriodoSelect;
                                Ent.Id_Tipo_Mov = "0035";
                                Ent.Id_Almacen = AlmacenCodigo;
                                Ent.Id_Movimiento = "";
                                Ent.Id_Tipo_Operacion = "01";//VENTAS

                                Ent.Inv_Tipo_Entidad = "C";
                                Ent.Inv_Ruc_Dni = txtrucdni.Text;

                                Ent.Glosa = "";
                                Ent.Inv_Tipo_Doc = ComprobanteCod;

                                Ent.Inv_Serie = BSISerieDocum.Text;
                                Ent.Inv_Numero = BSINumeroDocum.Text;

                                Ent.Inv_Condicion = f.txtcondicioncod.Tag.ToString().Trim();// "0006";// CONTADO

                                //0587 : Cancelado
                                //0585: Pendiente
                                Ent.Est_Facturacion = (string)IIf(f.txtcondicioncod.Text.ToString().Trim() == "01" , "0587", "0585") ;

                                Ent.Inv_Fecha = Convert.ToDateTime(BSIFecha.Caption);

                                Ent.Inv_Almacen_O_D = null;
                                Ent.Inv_Responsable_Ruc_Dni = null;

                                Ent.Inv_Libro = "";// txtlibro.Tag.ToString();
                                Ent.Inv_Base = Convert.ToDecimal(TxtRsBaseImponible.Text);
                                Ent.Inv_Igv = Convert.ToDecimal(TxtRsIGVMonto.Text);
                                Ent.Inv_Total =Math.Round(Convert.ToDecimal(TxtRsImporteTotal.Text),2);


                                Ent.Inv_Valor_Fact_Exonerada = Math.Round(Convert.ToDecimal(TxtRsValExporta.Text), 2);
                                Ent.Inv_Exonerada = Math.Round(Convert.ToDecimal(TxtRsExonerada.Text), 2);
                                Ent.Inv_Inafecta = Math.Round(Convert.ToDecimal(TxtRsInAfecta.Text), 2);
                                Ent.Inv_Isc_Importe = Math.Round(Convert.ToDecimal(TxtRsISC.Text), 2);
                                Ent.Inv_Otros_Tributos_Importe = Math.Round(Convert.ToDecimal(TxtRsOtrosTribCarg.Text), 2);


                                Ent.Inv_pdv_codigo = txtpdvdesc.Tag.ToString();
                                Ent.Cja_Codigo = Cja_Codigo;
                                Ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
                                Ent.Cja_Aper_Cierre_Codigo_Item = 0;
                                Ent.cja_estado = "0058"; //caja abierto
                                Ent.Inv_Es_Venta_Sin_Ticket = true;
                                Ent.Inv_Es_Venta = true;
                                Ent.Detalle = Detalles_NS;

                               Ent.DetalleLotes = Detalles_Lotes_Inv;
    

                                if (Verificar_Detalles())
                                {
                                    if (Estado == "1")
                                    {
                                        if (Log.Insertar_Ventas_INV(Ent))
                                        {
                                          
                                            //ImprimirTicket();
                                             Imprimir_Ticket();
                                    


                                            Bloquear();
                                            Limpiar();
                                            dgvdatos.DataSource = null;
                                            Detalles_ADM.Clear();
                                            Detalles_CONT.Clear();
                                            Detalles_NS.Clear();
                                            Detalles_Lotes.Clear();
                                            Detalles_Lotes_Inv.Clear();
                                            TxtRsBaseImponible.Text = "0.00";
                                            TxtRsIGVMonto.Text = "0.00";
                                            TxtRsImporteTotal.Text = "0.00";
                                            CheckBtnNotSal.Checked = true;
                                            Cliente_Por_Defecto();
                                            Nuevo_();
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (VAR_BOLETA || VAR_FACTURA)
                    {
                        using (frm_cobrar f = new frm_cobrar())
                        {
                            f.RucDni = txtrucdni.Text.Trim();
                            f.TxtImporteTotal.Text = TxtRsImporteTotal.Text;
                            f.TxtEntrega.Text = TxtRsImporteTotal.Text;

                            if (f.ShowDialog() == DialogResult.OK)
                            {

                                Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                                Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();

                                DateTime dt = Convert.ToDateTime(BSIFecha.Caption);


                                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                                Ent.Id_Anio = Convert.ToString(dt.Year);// Actual_Conexion.AnioSelect;
                                Ent.Id_Periodo = Convert.ToString((dt.Month < 10 ? "0" : "") + dt.Month); //Actual_Conexion.PeriodoSelect;
                                Ent.Id_Libro = Id_Libro;
                                Ent.Id_Voucher = "";

                                Ent.Ctb_Glosa = "POR VENTAS AL CONTADO";

                                Ent.Ctb_Tipo_Doc_Sunat = Elect_Documento_Sunat.Trim();
                                Ent.Ctb_Tipo_Doc = ComprobanteCod;
                                Ent.Nombre_Comprobante = Elect_Documento_Descripcion;

                                Ent.ctb_pdv_codigo = txtpdvdesc.Tag.ToString();

                                Ent.Ctb_Serie = BSISerieDocum.Text;
                                Ent.Ctb_Numero = BSINumeroDocum.Text;


                                Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(BSIFecha.Caption);
                                Ent.Ctb_Tipo_Ent = "C";

                                Ent.Ctb_Ruc_dni = txtrucdni.Text;
                                Ent.Ent_TpDcEnt_SUNAT = Tipo_Doc_SUNAT_Cliente;
                                Ent.Correo_Cliente = Correo_Electronico;
                                Ent.Cliente_Razon_Social = txtclientedesc.Text.Trim();
                                Ent.Cliente_Direccion = txtdireccion.Text.Trim();


                                Ent.Ctb_Condicion_Cod = f.txtcondicioncod.Tag.ToString().Trim();// "0006";// CONTADO
                                Ent.Est_Facturacion = (string)IIf(f.txtcondicioncod.Text.ToString().Trim() == "01", "0587", "0585");


                                Ent.Ctn_Moneda_Cod = "001";//SOLES

                                Ent.Ctb_Tipo_Cambio_Cod = "SVC";
                                Ent.Ctb_Tipo_Cambio_desc = "SIN CONVERSION";
                                Ent.Ctb_Tipo_Cambio_Valor = 1;

                                Ent.Ctb_Base_Imponible = Convert.ToDecimal(TxtRsBaseImponible.Text);
                                Ent.Ctb_Valor_Fact_Exonerada = Convert.ToDecimal(TxtRsValExporta.Text);
                                Ent.Ctb_Exonerada = Convert.ToDecimal(TxtRsExonerada.Text);
                                Ent.Ctb_Inafecta = Convert.ToDecimal(TxtRsInAfecta.Text);

                                Ent.Ctb_Isc = Convert.ToBoolean(IIf(Convert.ToDecimal(TxtRsISC.Text) == 0, false, true));
                                Ent.Ctb_Isc_Importe = Convert.ToDecimal(TxtRsISC.Text);

                                Ent.Ctb_Igv = Convert.ToDecimal(TxtRsIGVMonto.Text);

                                Ent.Ctb_Otros_Tributos =Convert.ToBoolean(IIf(Convert.ToDecimal(TxtRsOtrosTribCarg.Text)== 0 , false, true));// false;
                                Ent.Ctb_Otros_Tributos_Importe = Convert.ToDecimal(TxtRsOtrosTribCarg.Text);

                                Ent.Ctb_Importe_Total = Convert.ToDecimal(TxtRsImporteTotal.Text);

                                Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime("01/01/1900");



                                Ent.Ctb_Tasa_IGV = Convert.ToDecimal(Igv_Porcentaje);
                                Ent.Ctb_Analisis = Id_Analisis;
                                Ent.Cja_Codigo = Cja_Codigo;
                                Ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;
                                Ent.Cja_Aper_Cierre_Codigo_Item = 0;
                                Ent.cja_estado = "0058";

                                //detalle Contable
                                Ent.DetalleAsiento = Detalles_CONT;
                                //Detalle Administrativo
                                
                                Ent.DetalleADM = Detalles_ADM;

                                Ent.DetalleLotes = Detalles_Lotes;

                             


                                if (Verificar_Detalles())
                                {
                                    if (Estado == "1")
                                    {
                                         if (Log.Insertar_adm_Ventas(Ent))//PRIMERO QUE REGISTRE A LA TABLA ADMINISTRATIVA
                                        {
                                            if (Log.Insertar_ctb_Ventas(Ent))
                                            {
                                                // ImprimirTicket();

                                                if (VAR_BOLETA || VAR_FACTURA)
                                                {
                                                    if (Es_Electronico == false)
                                                    {
                                                        Impresion_Boleta_Factura_Electronica(Ent, false,Es_Electronico);
                                                        //ImprimirTicket();
                                                    }
                                                    else
                                                    {
                                                        // CUANDO ES ELECTRONICO
                                                        Impresion_Boleta_Factura_Electronica(Ent, false,Es_Electronico);
                                                    }
                                                }
                                                Bloquear();
                                                Limpiar();
                                                dgvdatos.DataSource = null;
                                                Detalles_ADM.Clear();
                                                Detalles_CONT.Clear();
                                                Detalles_Lotes.Clear();
                                                Detalles_Lotes_Inv.Clear();
                                                Detalles_NS.Clear();
                                                TxtRsBaseImponible.Text = "0.00";
                                                TxtRsIGVMonto.Text = "0.00";
                                                TxtRsImporteTotal.Text = "0.00";
                                                CheckBtnNotSal.Checked = true;
                                                Cliente_Por_Defecto();
                                                //btnuevo.Select();
                                                Nuevo_();
                                            }


                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }



        private void btncobrarteclacorta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btncobrar.PerformClick();
        }
    }
}
