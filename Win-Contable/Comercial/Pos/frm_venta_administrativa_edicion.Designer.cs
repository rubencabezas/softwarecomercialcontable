﻿namespace Contable.Comercial.Pos
{
    partial class frm_venta_administrativa_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtpdvdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpdvcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechavencimiento = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtanalisisdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtidanalisis = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txttipocambiovalor = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtentidad = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiodesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipocambiocod = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtigvporcentaje = new DevExpress.XtraEditors.TextEdit();
            this.chkotrostri = new DevExpress.XtraEditors.CheckEdit();
            this.chkisc = new DevExpress.XtraEditors.CheckEdit();
            this.txtimportetotal = new DevExpress.XtraEditors.TextEdit();
            this.txtotrostribuimporte = new DevExpress.XtraEditors.TextEdit();
            this.txtigv = new DevExpress.XtraEditors.TextEdit();
            this.txtimporteisc = new DevExpress.XtraEditors.TextEdit();
            this.txtinafecta = new DevExpress.XtraEditors.TextEdit();
            this.txtexonerada = new DevExpress.XtraEditors.TextEdit();
            this.txtvalorfactexport = new DevExpress.XtraEditors.TextEdit();
            this.txtbaseimponible = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.GbxDetalle = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.chklote = new System.Windows.Forms.CheckBox();
            this.txtunmabrev = new DevExpress.XtraEditors.TextEdit();
            this.txtunmdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtunmcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.Dgvdetalles = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Lote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtcantidad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtvalorunit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtalmacendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacencod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.TxtAfectIGVDescripcion = new DevExpress.XtraEditors.TextEdit();
            this.TxtAfectIGVCodigo = new DevExpress.XtraEditors.TextEdit();
            this.txttipooperaciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipooperacion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtimporte = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtinafecta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexonerada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorfactexport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GbxDetalle)).BeginInit();
            this.GbxDetalle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmabrev.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvdetalles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorunit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVDescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperaciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1023, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 556);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1023, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 524);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1023, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 524);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = global::Contable.Properties.Resources.Export_24px;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 38);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(719, 190);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(717, 165);
            this.xtraTabPage1.Text = "Datos Generales";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtpdvdesc);
            this.groupControl1.Controls.Add(this.txtpdvcod);
            this.groupControl1.Controls.Add(this.labelControl18);
            this.groupControl1.Controls.Add(this.txtfechavencimiento);
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.txtanalisisdesc);
            this.groupControl1.Controls.Add(this.txtidanalisis);
            this.groupControl1.Controls.Add(this.labelControl25);
            this.groupControl1.Controls.Add(this.txttipocambiovalor);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtentidad);
            this.groupControl1.Controls.Add(this.txttipocambiodesc);
            this.groupControl1.Controls.Add(this.txttipocambiocod);
            this.groupControl1.Controls.Add(this.txtmonedadesc);
            this.groupControl1.Controls.Add(this.txtmonedacod);
            this.groupControl1.Controls.Add(this.txtcondiciondesc);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txtcondicioncod);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtrucdni);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(717, 165);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // txtpdvdesc
            // 
            this.txtpdvdesc.Enabled = false;
            this.txtpdvdesc.EnterMoveNextControl = true;
            this.txtpdvdesc.Location = new System.Drawing.Point(484, 26);
            this.txtpdvdesc.MenuManager = this.barManager1;
            this.txtpdvdesc.Name = "txtpdvdesc";
            this.txtpdvdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpdvdesc.Size = new System.Drawing.Size(213, 20);
            this.txtpdvdesc.TabIndex = 33;
            // 
            // txtpdvcod
            // 
            this.txtpdvcod.Enabled = false;
            this.txtpdvcod.EnterMoveNextControl = true;
            this.txtpdvcod.Location = new System.Drawing.Point(444, 26);
            this.txtpdvcod.MenuManager = this.barManager1;
            this.txtpdvcod.Name = "txtpdvcod";
            this.txtpdvcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpdvcod.Size = new System.Drawing.Size(37, 20);
            this.txtpdvcod.TabIndex = 32;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(364, 26);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(74, 13);
            this.labelControl18.TabIndex = 31;
            this.labelControl18.Text = "Punto de venta";
            // 
            // txtfechavencimiento
            // 
            this.txtfechavencimiento.Enabled = false;
            this.txtfechavencimiento.EnterMoveNextControl = true;
            this.txtfechavencimiento.Location = new System.Drawing.Point(106, 95);
            this.txtfechavencimiento.MenuManager = this.barManager1;
            this.txtfechavencimiento.Name = "txtfechavencimiento";
            this.txtfechavencimiento.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechavencimiento.Properties.Mask.BeepOnError = true;
            this.txtfechavencimiento.Properties.Mask.EditMask = "d";
            this.txtfechavencimiento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechavencimiento.Properties.Mask.SaveLiteral = false;
            this.txtfechavencimiento.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechavencimiento.Properties.MaxLength = 10;
            this.txtfechavencimiento.Size = new System.Drawing.Size(108, 20);
            this.txtfechavencimiento.TabIndex = 14;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(7, 99);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(93, 13);
            this.labelControl35.TabIndex = 13;
            this.labelControl35.Text = "Fecha Vencimiento:";
            // 
            // txtanalisisdesc
            // 
            this.txtanalisisdesc.Enabled = false;
            this.txtanalisisdesc.Location = new System.Drawing.Point(171, 138);
            this.txtanalisisdesc.MenuManager = this.barManager1;
            this.txtanalisisdesc.Name = "txtanalisisdesc";
            this.txtanalisisdesc.Size = new System.Drawing.Size(308, 20);
            this.txtanalisisdesc.TabIndex = 27;
            // 
            // txtidanalisis
            // 
            this.txtidanalisis.Enabled = false;
            this.txtidanalisis.Location = new System.Drawing.Point(124, 138);
            this.txtidanalisis.MenuManager = this.barManager1;
            this.txtidanalisis.Name = "txtidanalisis";
            this.txtidanalisis.Size = new System.Drawing.Size(44, 20);
            this.txtidanalisis.TabIndex = 26;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(7, 141);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(111, 13);
            this.labelControl25.TabIndex = 25;
            this.labelControl25.Text = "Analisis de la operacion";
            // 
            // txttipocambiovalor
            // 
            this.txttipocambiovalor.EnterMoveNextControl = true;
            this.txttipocambiovalor.Location = new System.Drawing.Point(632, 95);
            this.txttipocambiovalor.MenuManager = this.barManager1;
            this.txttipocambiovalor.Name = "txttipocambiovalor";
            this.txttipocambiovalor.Size = new System.Drawing.Size(65, 20);
            this.txttipocambiovalor.TabIndex = 21;
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(101, 73);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(192, 20);
            this.txttipodocdesc.TabIndex = 6;
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.Enabled = false;
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(608, 73);
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(89, 20);
            this.txtfechadoc.TabIndex = 12;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(545, 76);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(58, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Fecha Doc.:";
            // 
            // txtserie
            // 
            this.txtserie.Enabled = false;
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(338, 73);
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(201, 20);
            this.txtserie.TabIndex = 8;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(298, 76);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Serie:";
            // 
            // txttipodoc
            // 
            this.txttipodoc.Enabled = false;
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(61, 73);
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(39, 20);
            this.txttipodoc.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(21, 76);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "T.Doc.:";
            // 
            // txtentidad
            // 
            this.txtentidad.Enabled = false;
            this.txtentidad.EnterMoveNextControl = true;
            this.txtentidad.Location = new System.Drawing.Point(146, 116);
            this.txtentidad.Name = "txtentidad";
            this.txtentidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtentidad.Size = new System.Drawing.Size(550, 20);
            this.txtentidad.TabIndex = 24;
            // 
            // txttipocambiodesc
            // 
            this.txttipocambiodesc.Enabled = false;
            this.txttipocambiodesc.EnterMoveNextControl = true;
            this.txttipocambiodesc.Location = new System.Drawing.Point(523, 95);
            this.txttipocambiodesc.Name = "txttipocambiodesc";
            this.txttipocambiodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiodesc.Size = new System.Drawing.Size(105, 20);
            this.txttipocambiodesc.TabIndex = 20;
            // 
            // txttipocambiocod
            // 
            this.txttipocambiocod.Enabled = false;
            this.txttipocambiocod.EnterMoveNextControl = true;
            this.txttipocambiocod.Location = new System.Drawing.Point(487, 95);
            this.txttipocambiocod.Name = "txttipocambiocod";
            this.txttipocambiocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipocambiocod.Size = new System.Drawing.Size(34, 20);
            this.txttipocambiocod.TabIndex = 19;
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(306, 95);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedadesc.Size = new System.Drawing.Size(149, 20);
            this.txtmonedadesc.TabIndex = 17;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.Enabled = false;
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(266, 95);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonedacod.Size = new System.Drawing.Size(39, 20);
            this.txtmonedacod.TabIndex = 16;
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(581, 137);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondiciondesc.Size = new System.Drawing.Size(115, 20);
            this.txtcondiciondesc.TabIndex = 30;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(462, 96);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(21, 13);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "T.C.";
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.Enabled = false;
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(542, 137);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcondicioncod.Size = new System.Drawing.Size(38, 20);
            this.txtcondicioncod.TabIndex = 29;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(220, 98);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(42, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "Moneda:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(485, 141);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "Condicion:";
            // 
            // txtrucdni
            // 
            this.txtrucdni.Enabled = false;
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(61, 116);
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrucdni.Size = new System.Drawing.Size(82, 20);
            this.txtrucdni.TabIndex = 23;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(21, 119);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(37, 13);
            this.labelControl7.TabIndex = 22;
            this.labelControl7.Text = "R.U.C.:";
            // 
            // txtglosa
            // 
            this.txtglosa.Enabled = false;
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(61, 50);
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(636, 20);
            this.txtglosa.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(21, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Glosa:";
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(61, 27);
            this.txtlibro.MenuManager = this.barManager1;
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlibro.Size = new System.Drawing.Size(221, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(24, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtigvporcentaje);
            this.groupBox1.Controls.Add(this.chkotrostri);
            this.groupBox1.Controls.Add(this.chkisc);
            this.groupBox1.Controls.Add(this.txtimportetotal);
            this.groupBox1.Controls.Add(this.txtotrostribuimporte);
            this.groupBox1.Controls.Add(this.txtigv);
            this.groupBox1.Controls.Add(this.txtimporteisc);
            this.groupBox1.Controls.Add(this.txtinafecta);
            this.groupBox1.Controls.Add(this.txtexonerada);
            this.groupBox1.Controls.Add(this.txtvalorfactexport);
            this.groupBox1.Controls.Add(this.txtbaseimponible);
            this.groupBox1.Controls.Add(this.labelControl28);
            this.groupBox1.Controls.Add(this.labelControl27);
            this.groupBox1.Controls.Add(this.labelControl24);
            this.groupBox1.Controls.Add(this.labelControl23);
            this.groupBox1.Controls.Add(this.labelControl22);
            this.groupBox1.Controls.Add(this.labelControl21);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(738, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 185);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resumen";
            // 
            // txtigvporcentaje
            // 
            this.txtigvporcentaje.EditValue = "0.00";
            this.txtigvporcentaje.Location = new System.Drawing.Point(55, 119);
            this.txtigvporcentaje.MenuManager = this.barManager1;
            this.txtigvporcentaje.Name = "txtigvporcentaje";
            this.txtigvporcentaje.Size = new System.Drawing.Size(39, 20);
            this.txtigvporcentaje.TabIndex = 18;
            // 
            // chkotrostri
            // 
            this.chkotrostri.Location = new System.Drawing.Point(9, 141);
            this.chkotrostri.MenuManager = this.barManager1;
            this.chkotrostri.Name = "chkotrostri";
            this.chkotrostri.Properties.Caption = "Otros Tributos";
            this.chkotrostri.Size = new System.Drawing.Size(92, 20);
            this.chkotrostri.TabIndex = 17;
            // 
            // chkisc
            // 
            this.chkisc.Location = new System.Drawing.Point(9, 99);
            this.chkisc.MenuManager = this.barManager1;
            this.chkisc.Name = "chkisc";
            this.chkisc.Properties.Caption = "I.S.C.";
            this.chkisc.Size = new System.Drawing.Size(75, 20);
            this.chkisc.TabIndex = 16;
            // 
            // txtimportetotal
            // 
            this.txtimportetotal.EditValue = "0.00";
            this.txtimportetotal.Location = new System.Drawing.Point(100, 161);
            this.txtimportetotal.MenuManager = this.barManager1;
            this.txtimportetotal.Name = "txtimportetotal";
            this.txtimportetotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimportetotal.Properties.Appearance.Options.UseFont = true;
            this.txtimportetotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimportetotal.Size = new System.Drawing.Size(87, 20);
            this.txtimportetotal.TabIndex = 15;
            // 
            // txtotrostribuimporte
            // 
            this.txtotrostribuimporte.EditValue = "0.00";
            this.txtotrostribuimporte.Location = new System.Drawing.Point(100, 140);
            this.txtotrostribuimporte.MenuManager = this.barManager1;
            this.txtotrostribuimporte.Name = "txtotrostribuimporte";
            this.txtotrostribuimporte.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtotrostribuimporte.Properties.Appearance.Options.UseFont = true;
            this.txtotrostribuimporte.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtotrostribuimporte.Size = new System.Drawing.Size(87, 20);
            this.txtotrostribuimporte.TabIndex = 14;
            // 
            // txtigv
            // 
            this.txtigv.EditValue = "0.00";
            this.txtigv.Location = new System.Drawing.Point(100, 119);
            this.txtigv.MenuManager = this.barManager1;
            this.txtigv.Name = "txtigv";
            this.txtigv.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtigv.Properties.Appearance.Options.UseFont = true;
            this.txtigv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtigv.Size = new System.Drawing.Size(87, 20);
            this.txtigv.TabIndex = 13;
            // 
            // txtimporteisc
            // 
            this.txtimporteisc.EditValue = "0.00";
            this.txtimporteisc.Location = new System.Drawing.Point(100, 97);
            this.txtimporteisc.MenuManager = this.barManager1;
            this.txtimporteisc.Name = "txtimporteisc";
            this.txtimporteisc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtimporteisc.Properties.Appearance.Options.UseFont = true;
            this.txtimporteisc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtimporteisc.Size = new System.Drawing.Size(87, 20);
            this.txtimporteisc.TabIndex = 12;
            // 
            // txtinafecta
            // 
            this.txtinafecta.EditValue = "0.00";
            this.txtinafecta.Location = new System.Drawing.Point(100, 76);
            this.txtinafecta.MenuManager = this.barManager1;
            this.txtinafecta.Name = "txtinafecta";
            this.txtinafecta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtinafecta.Properties.Appearance.Options.UseFont = true;
            this.txtinafecta.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtinafecta.Size = new System.Drawing.Size(87, 20);
            this.txtinafecta.TabIndex = 11;
            // 
            // txtexonerada
            // 
            this.txtexonerada.EditValue = "0.00";
            this.txtexonerada.Location = new System.Drawing.Point(100, 55);
            this.txtexonerada.MenuManager = this.barManager1;
            this.txtexonerada.Name = "txtexonerada";
            this.txtexonerada.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtexonerada.Properties.Appearance.Options.UseFont = true;
            this.txtexonerada.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtexonerada.Size = new System.Drawing.Size(87, 20);
            this.txtexonerada.TabIndex = 10;
            // 
            // txtvalorfactexport
            // 
            this.txtvalorfactexport.EditValue = "0.00";
            this.txtvalorfactexport.Location = new System.Drawing.Point(100, 34);
            this.txtvalorfactexport.MenuManager = this.barManager1;
            this.txtvalorfactexport.Name = "txtvalorfactexport";
            this.txtvalorfactexport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtvalorfactexport.Properties.Appearance.Options.UseFont = true;
            this.txtvalorfactexport.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtvalorfactexport.Size = new System.Drawing.Size(87, 20);
            this.txtvalorfactexport.TabIndex = 9;
            // 
            // txtbaseimponible
            // 
            this.txtbaseimponible.EditValue = "0.00";
            this.txtbaseimponible.Location = new System.Drawing.Point(100, 13);
            this.txtbaseimponible.MenuManager = this.barManager1;
            this.txtbaseimponible.Name = "txtbaseimponible";
            this.txtbaseimponible.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtbaseimponible.Properties.Appearance.Options.UseFont = true;
            this.txtbaseimponible.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtbaseimponible.Size = new System.Drawing.Size(87, 20);
            this.txtbaseimponible.TabIndex = 8;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(9, 122);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(29, 13);
            this.labelControl28.TabIndex = 7;
            this.labelControl28.Text = "I.G.V.";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(9, 164);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(69, 13);
            this.labelControl27.TabIndex = 6;
            this.labelControl27.Text = "Importe Total:";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(9, 78);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(41, 13);
            this.labelControl24.TabIndex = 3;
            this.labelControl24.Text = "Inafecta";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(9, 56);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(52, 13);
            this.labelControl23.TabIndex = 2;
            this.labelControl23.Text = "Exonerada";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(9, 37);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(85, 13);
            this.labelControl22.TabIndex = 1;
            this.labelControl22.Text = "Val. Fact. Export.";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(9, 16);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(72, 13);
            this.labelControl21.TabIndex = 0;
            this.labelControl21.Text = "Base Imponible";
            // 
            // GbxDetalle
            // 
            this.GbxDetalle.Controls.Add(this.label1);
            this.GbxDetalle.Controls.Add(this.chklote);
            this.GbxDetalle.Controls.Add(this.txtunmabrev);
            this.GbxDetalle.Controls.Add(this.txtunmdesc);
            this.GbxDetalle.Controls.Add(this.txtunmcod);
            this.GbxDetalle.Controls.Add(this.labelControl5);
            this.GbxDetalle.Controls.Add(this.Dgvdetalles);
            this.GbxDetalle.Controls.Add(this.txtcantidad);
            this.GbxDetalle.Controls.Add(this.labelControl11);
            this.GbxDetalle.Controls.Add(this.txtvalorunit);
            this.GbxDetalle.Controls.Add(this.labelControl12);
            this.GbxDetalle.Controls.Add(this.txtalmacendesc);
            this.GbxDetalle.Controls.Add(this.txtalmacencod);
            this.GbxDetalle.Controls.Add(this.labelControl13);
            this.GbxDetalle.Controls.Add(this.labelControl14);
            this.GbxDetalle.Controls.Add(this.txtproductodesc);
            this.GbxDetalle.Controls.Add(this.txtproductocod);
            this.GbxDetalle.Controls.Add(this.TxtAfectIGVDescripcion);
            this.GbxDetalle.Controls.Add(this.TxtAfectIGVCodigo);
            this.GbxDetalle.Controls.Add(this.txttipooperaciondesc);
            this.GbxDetalle.Controls.Add(this.txttipooperacion);
            this.GbxDetalle.Controls.Add(this.labelControl17);
            this.GbxDetalle.Controls.Add(this.txttipobsadesc);
            this.GbxDetalle.Controls.Add(this.labelControl15);
            this.GbxDetalle.Controls.Add(this.txttipobsacod);
            this.GbxDetalle.Controls.Add(this.labelControl16);
            this.GbxDetalle.Controls.Add(this.txtimporte);
            this.GbxDetalle.Controls.Add(this.labelControl20);
            this.GbxDetalle.Controls.Add(this.btnanadirdet);
            this.GbxDetalle.Controls.Add(this.btnquitardet);
            this.GbxDetalle.Controls.Add(this.btneditardet);
            this.GbxDetalle.Controls.Add(this.btnnuevodet);
            this.GbxDetalle.Location = new System.Drawing.Point(13, 234);
            this.GbxDetalle.Name = "GbxDetalle";
            this.GbxDetalle.Size = new System.Drawing.Size(998, 310);
            this.GbxDetalle.TabIndex = 8;
            this.GbxDetalle.Text = "Detalles ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(240)))), ((int)(((byte)(112)))));
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Falta asignar lote";
            // 
            // chklote
            // 
            this.chklote.AutoSize = true;
            this.chklote.Enabled = false;
            this.chklote.Location = new System.Drawing.Point(527, 76);
            this.chklote.Name = "chklote";
            this.chklote.Size = new System.Drawing.Size(91, 17);
            this.chklote.TabIndex = 22;
            this.chklote.Text = "Acepta lotes?";
            this.chklote.UseVisualStyleBackColor = true;
            // 
            // txtunmabrev
            // 
            this.txtunmabrev.Enabled = false;
            this.txtunmabrev.Location = new System.Drawing.Point(391, 50);
            this.txtunmabrev.MenuManager = this.barManager1;
            this.txtunmabrev.Name = "txtunmabrev";
            this.txtunmabrev.Size = new System.Drawing.Size(82, 20);
            this.txtunmabrev.TabIndex = 12;
            // 
            // txtunmdesc
            // 
            this.txtunmdesc.Enabled = false;
            this.txtunmdesc.Location = new System.Drawing.Point(147, 50);
            this.txtunmdesc.MenuManager = this.barManager1;
            this.txtunmdesc.Name = "txtunmdesc";
            this.txtunmdesc.Size = new System.Drawing.Size(243, 20);
            this.txtunmdesc.TabIndex = 11;
            // 
            // txtunmcod
            // 
            this.txtunmcod.Enabled = false;
            this.txtunmcod.Location = new System.Drawing.Point(99, 50);
            this.txtunmcod.MenuManager = this.barManager1;
            this.txtunmcod.Name = "txtunmcod";
            this.txtunmcod.Size = new System.Drawing.Size(46, 20);
            this.txtunmcod.TabIndex = 10;
            this.txtunmcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtunmcod_KeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(10, 53);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(85, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Unidad de medida";
            // 
            // Dgvdetalles
            // 
            gridLevelNode1.RelationName = "Level1";
            this.Dgvdetalles.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.Dgvdetalles.Location = new System.Drawing.Point(5, 142);
            this.Dgvdetalles.MainView = this.gridView2;
            this.Dgvdetalles.MenuManager = this.barManager1;
            this.Dgvdetalles.Name = "Dgvdetalles";
            this.Dgvdetalles.Size = new System.Drawing.Size(977, 153);
            this.Dgvdetalles.TabIndex = 49;
            this.Dgvdetalles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn1,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.Lote,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.GridControl = this.Dgvdetalles;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView2_RowClick);
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tipo";
            this.gridColumn10.FieldName = "Adm_Tipo_BSA_Desc";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 79;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Unidad de medida";
            this.gridColumn1.FieldName = "Adm_Unm_Desc";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 161;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Producto";
            this.gridColumn11.FieldName = "Adm_Catalogo_Desc";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            this.gridColumn11.Width = 215;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Almacen";
            this.gridColumn12.FieldName = "Adm_Almacen_desc";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            this.gridColumn12.Width = 207;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Cantidad";
            this.gridColumn13.DisplayFormat.FormatString = "n2";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "Adm_Cantidad";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Adm_Cantidad", "{0:n2}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 4;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Valor Unit.";
            this.gridColumn14.DisplayFormat.FormatString = "n2";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn14.FieldName = "Adm_Valor_Unit";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Adm_Valor_Unit", "{0:n2}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 5;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Total";
            this.gridColumn15.DisplayFormat.FormatString = "n2";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn15.FieldName = "Adm_Total";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Adm_Total", "{0:n2}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 6;
            // 
            // Lote
            // 
            this.Lote.Caption = "Lote";
            this.Lote.ColumnEdit = this.repositoryItemButtonEdit1;
            this.Lote.FieldName = "Acepta_lotes";
            this.Lote.Name = "Lote";
            this.Lote.Visible = true;
            this.Lote.VisibleIndex = 7;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Adm_Catalogo";
            this.gridColumn2.FieldName = "Adm_Catalogo";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Adm_Item";
            this.gridColumn3.FieldName = "Adm_Item";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // txtcantidad
            // 
            this.txtcantidad.EnterMoveNextControl = true;
            this.txtcantidad.Location = new System.Drawing.Point(100, 72);
            this.txtcantidad.MenuManager = this.barManager1;
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Size = new System.Drawing.Size(100, 20);
            this.txtcantidad.TabIndex = 17;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(49, 75);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(47, 13);
            this.labelControl11.TabIndex = 16;
            this.labelControl11.Text = "Cantidad:";
            // 
            // txtvalorunit
            // 
            this.txtvalorunit.EnterMoveNextControl = true;
            this.txtvalorunit.Location = new System.Drawing.Point(259, 72);
            this.txtvalorunit.MenuManager = this.barManager1;
            this.txtvalorunit.Name = "txtvalorunit";
            this.txtvalorunit.Size = new System.Drawing.Size(100, 20);
            this.txtvalorunit.TabIndex = 19;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(206, 75);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(50, 13);
            this.labelControl12.TabIndex = 18;
            this.labelControl12.Text = "Valor Unit.";
            // 
            // txtalmacendesc
            // 
            this.txtalmacendesc.Enabled = false;
            this.txtalmacendesc.EnterMoveNextControl = true;
            this.txtalmacendesc.Location = new System.Drawing.Point(572, 49);
            this.txtalmacendesc.MenuManager = this.barManager1;
            this.txtalmacendesc.Name = "txtalmacendesc";
            this.txtalmacendesc.Size = new System.Drawing.Size(141, 20);
            this.txtalmacendesc.TabIndex = 15;
            // 
            // txtalmacencod
            // 
            this.txtalmacencod.Enabled = false;
            this.txtalmacencod.EnterMoveNextControl = true;
            this.txtalmacencod.Location = new System.Drawing.Point(527, 49);
            this.txtalmacencod.MenuManager = this.barManager1;
            this.txtalmacencod.Name = "txtalmacencod";
            this.txtalmacencod.Size = new System.Drawing.Size(44, 20);
            this.txtalmacencod.TabIndex = 14;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(479, 53);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(44, 13);
            this.labelControl13.TabIndex = 13;
            this.labelControl13.Text = "Almacen:";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(263, 30);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(47, 13);
            this.labelControl14.TabIndex = 6;
            this.labelControl14.Text = "Producto:";
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(391, 28);
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Size = new System.Drawing.Size(323, 20);
            this.txtproductodesc.TabIndex = 8;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(314, 28);
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Size = new System.Drawing.Size(76, 20);
            this.txtproductocod.TabIndex = 7;
            this.txtproductocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproductocod_KeyDown);
            // 
            // TxtAfectIGVDescripcion
            // 
            this.TxtAfectIGVDescripcion.Enabled = false;
            this.TxtAfectIGVDescripcion.EnterMoveNextControl = true;
            this.TxtAfectIGVDescripcion.Location = new System.Drawing.Point(145, 95);
            this.TxtAfectIGVDescripcion.Name = "TxtAfectIGVDescripcion";
            this.TxtAfectIGVDescripcion.Size = new System.Drawing.Size(569, 20);
            this.TxtAfectIGVDescripcion.TabIndex = 25;
            // 
            // TxtAfectIGVCodigo
            // 
            this.TxtAfectIGVCodigo.Enabled = false;
            this.TxtAfectIGVCodigo.EnterMoveNextControl = true;
            this.TxtAfectIGVCodigo.Location = new System.Drawing.Point(100, 95);
            this.TxtAfectIGVCodigo.Name = "TxtAfectIGVCodigo";
            this.TxtAfectIGVCodigo.Size = new System.Drawing.Size(44, 20);
            this.TxtAfectIGVCodigo.TabIndex = 24;
            this.TxtAfectIGVCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAfectIGVCodigo_KeyDown);
            // 
            // txttipooperaciondesc
            // 
            this.txttipooperaciondesc.Enabled = false;
            this.txttipooperaciondesc.EnterMoveNextControl = true;
            this.txttipooperaciondesc.Location = new System.Drawing.Point(145, 117);
            this.txttipooperaciondesc.Name = "txttipooperaciondesc";
            this.txttipooperaciondesc.Size = new System.Drawing.Size(569, 20);
            this.txttipooperaciondesc.TabIndex = 25;
            // 
            // txttipooperacion
            // 
            this.txttipooperacion.EnterMoveNextControl = true;
            this.txttipooperacion.Location = new System.Drawing.Point(100, 117);
            this.txttipooperacion.Name = "txttipooperacion";
            this.txttipooperacion.Size = new System.Drawing.Size(44, 20);
            this.txttipooperacion.TabIndex = 24;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(62, 98);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(32, 13);
            this.labelControl17.TabIndex = 23;
            this.labelControl17.Text = "Af.IGV";
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(113, 27);
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(141, 20);
            this.txttipobsadesc.TabIndex = 5;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(42, 123);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(53, 13);
            this.labelControl15.TabIndex = 23;
            this.labelControl15.Text = "Operación:";
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(68, 27);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Size = new System.Drawing.Size(44, 20);
            this.txttipobsacod.TabIndex = 4;
            this.txttipobsacod.TextChanged += new System.EventHandler(this.txttipobsacod_TextChanged);
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(10, 33);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(54, 13);
            this.labelControl16.TabIndex = 3;
            this.labelControl16.Text = "Tipo B/S/A:";
            // 
            // txtimporte
            // 
            this.txtimporte.EnterMoveNextControl = true;
            this.txtimporte.Location = new System.Drawing.Point(423, 72);
            this.txtimporte.Name = "txtimporte";
            this.txtimporte.Properties.Mask.EditMask = "n2";
            this.txtimporte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporte.Size = new System.Drawing.Size(82, 20);
            this.txtimporte.TabIndex = 21;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(365, 76);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(38, 13);
            this.labelControl20.TabIndex = 20;
            this.labelControl20.Text = "Importe";
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnanadirdet.Appearance.Options.UseFont = true;
            this.btnanadirdet.Location = new System.Drawing.Point(819, 69);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(87, 37);
            this.btnanadirdet.TabIndex = 26;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnquitardet.Appearance.Options.UseFont = true;
            this.btnquitardet.Location = new System.Drawing.Point(819, 26);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(87, 37);
            this.btnquitardet.TabIndex = 4;
            this.btnquitardet.Text = "Quitar";
            // 
            // btneditardet
            // 
            this.btneditardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneditardet.Appearance.Options.UseFont = true;
            this.btneditardet.Location = new System.Drawing.Point(726, 69);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(87, 37);
            this.btneditardet.TabIndex = 4;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevodet.Appearance.Options.UseFont = true;
            this.btnnuevodet.Location = new System.Drawing.Point(726, 26);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(87, 37);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // frm_venta_administrativa_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 556);
            this.Controls.Add(this.GbxDetalle);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_venta_administrativa_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_venta_administrativa_edicion";
            this.Load += new System.EventHandler(this.frm_venta_administrativa_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtanalisisdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtidanalisis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiovalor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtentidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipocambiocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtigvporcentaje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkotrostri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimportetotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtotrostribuimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtigv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporteisc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtinafecta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexonerada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorfactexport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtbaseimponible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GbxDetalle)).EndInit();
            this.GbxDetalle.ResumeLayout(false);
            this.GbxDetalle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmabrev.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvdetalles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorunit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVDescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAfectIGVCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperaciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtfechavencimiento;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtanalisisdesc;
        private DevExpress.XtraEditors.TextEdit txtidanalisis;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txttipocambiovalor;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtentidad;
        private DevExpress.XtraEditors.TextEdit txttipocambiodesc;
        private DevExpress.XtraEditors.TextEdit txttipocambiocod;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtigvporcentaje;
        private DevExpress.XtraEditors.CheckEdit chkotrostri;
        private DevExpress.XtraEditors.CheckEdit chkisc;
        private DevExpress.XtraEditors.TextEdit txtimportetotal;
        private DevExpress.XtraEditors.TextEdit txtotrostribuimporte;
        private DevExpress.XtraEditors.TextEdit txtigv;
        private DevExpress.XtraEditors.TextEdit txtimporteisc;
        private DevExpress.XtraEditors.TextEdit txtinafecta;
        private DevExpress.XtraEditors.TextEdit txtexonerada;
        private DevExpress.XtraEditors.TextEdit txtvalorfactexport;
        private DevExpress.XtraEditors.TextEdit txtbaseimponible;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.GroupControl GbxDetalle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chklote;
        private DevExpress.XtraEditors.TextEdit txtunmabrev;
        private DevExpress.XtraEditors.TextEdit txtunmdesc;
        private DevExpress.XtraEditors.TextEdit txtunmcod;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.GridControl Dgvdetalles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn Lote;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.TextEdit txtcantidad;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtvalorunit;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtalmacendesc;
        private DevExpress.XtraEditors.TextEdit txtalmacencod;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.TextEdit txttipooperaciondesc;
        private DevExpress.XtraEditors.TextEdit txttipooperacion;
        private DevExpress.XtraEditors.TextEdit txttipobsadesc;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        public DevExpress.XtraEditors.TextEdit txtimporte;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        public DevExpress.XtraEditors.SimpleButton btnanadirdet;
        public DevExpress.XtraEditors.SimpleButton btnquitardet;
        public DevExpress.XtraEditors.SimpleButton btneditardet;
        public DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.TextEdit TxtAfectIGVDescripcion;
        private DevExpress.XtraEditors.TextEdit TxtAfectIGVCodigo;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtpdvdesc;
        private DevExpress.XtraEditors.TextEdit txtpdvcod;
        private DevExpress.XtraEditors.LabelControl labelControl18;
    }
}