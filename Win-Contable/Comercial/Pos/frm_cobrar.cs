﻿using Contable;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_cobrar :frm_fuente
    {
        public frm_cobrar()
        {
            InitializeComponent();
        }

        public string RucDni = "";
        private void frm_cobrar_Load(object sender, EventArgs e)
        {

            Cargar();
            BuscarCondicionLoad("01"); // Condicion 01= CONTADO
        }

        void BloquearTarjeta()
        {
            Gbtarjeta.Enabled = false;
            VerificarMontoEntregar();
        }
        void Cargar()
        {
            try
            {
                TxtEntrega.Text = "0.00";
                TxtTarjeta.Text = "0.00";
                BloquearTarjeta();
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void VerificarMontoEntregar()
        {
            TxtEfecTar.Text = TxtEntrega.Text;
        }
        void Desbloqueartarjeta()
        {
            Gbtarjeta.Enabled = true;
            TxtTarjeta.Focus();

        }
        void Adevolver()
        {
            try
            {
                if ((TxtEfecTar.Text!="")&(TxtImporteTotal.Text != "")){
                    if(Convert.ToDecimal(TxtEfecTar.Text)>= Convert.ToDecimal(TxtImporteTotal.Text))
                    {
                        TxtAdevolver.Text = Convert.ToString(Convert.ToDecimal(TxtEfecTar.Text) - Convert.ToDecimal(TxtImporteTotal.Text));
                    }else
                    {
                        TxtAdevolver.Text = "0.00";
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void CheckBtnTipoContado_CheckedChanged(object sender, EventArgs e)
        {
            BloquearTarjeta();
            Adevolver();
            TxtEntrega.Focus();
        }

        private void CheckBtnTipoTarjeta_CheckedChanged(object sender, EventArgs e)
        {
            Desbloqueartarjeta();
            TxtTarjeta.Text = TxtImporteTotal.Text;
        }

        private void TxtTarjeta_Leave(object sender, EventArgs e)
        {
       
        }

        private void TxtEntrega_TextChanged(object sender, EventArgs e)
        {
            if ((TxtEntrega.Text!="") & (TxtImporteTotal.Text!="")& (TxtEntrega.Text != "0.0")){
                TxtEfecTar.Text = TxtEntrega.Text;
                if (Convert.ToDecimal(TxtEfecTar.Text)>= Convert.ToDecimal(TxtImporteTotal.Text))
                {
                    TxtEfecTar.Text = TxtEntrega.Text;
                    TxtAdevolver.Text = Convert.ToString(Convert.ToDecimal(TxtEntrega.Text) - Convert.ToDecimal(TxtImporteTotal.Text));
                }
                else
                {
                    TxtAdevolver.Text = "0.00";
                }
            }
            else
            {
                TxtEfecTar.Text = "0.00";
                TxtEntrega.Text = "0.00";
            }
        }

        private void TxtEntrega_Validating(object sender, CancelEventArgs e)
        {
            Adevolver02();
        }
        void Adevolver02()
        {
            try
            {
                    if (TxtEntrega.Text == "")
                    {
                        TxtEntrega.Text = "0.00";
                    }
                    if (TxtEntrega.Text != "0.0")
                    {
                        if (Convert.ToDecimal(TxtTarjeta.Text) <= Convert.ToDecimal(TxtImporteTotal.Text))
                        {
                            if ((TxtEfecTar.Text!="")&(TxtImporteTotal.Text != "")){
                                TxtEfecTar.Text = TxtEntrega.Text;
                                if (Convert.ToDecimal(TxtEfecTar.Text)>= Convert.ToDecimal(TxtImporteTotal.Text))
                                {
                                    TxtEfecTar.Text = TxtEntrega.Text;
                                    TxtAdevolver.Text =Convert.ToString( Convert.ToDecimal( TxtEntrega.Text)- Convert.ToDecimal(TxtImporteTotal.Text) );
                                }
                                else
                                {
                                    TxtEfecTar.Text = "0.00";
                                    TxtAdevolver.Text = "0.00";
                                }
                            }
                            else
                            {
                                TxtEfecTar.Text = "0.00";
                            }
                        }
                        else
                        {
                            Accion.Advertencia("");
                        }
                    }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //if(Convert.ToDecimal(TxtEfecTar.Text)>= Convert.ToDecimal(TxtImporteTotal.Text))
                //{
                    if (txtcondicioncod.Text == "")
                    {
                        Accion.Advertencia("Debe ingresar una condicion");
                        txtcondicioncod.Focus();
                    }
                  
                    if (txtcondicioncod.Text == "01")//Contado
                    {
                            if (Convert.ToDecimal(TxtEntrega.Text) >= Convert.ToDecimal(TxtImporteTotal.Text))
                            {
                                       DialogResult = DialogResult.OK;  

                            }
                            else
                            {
                                    Accion.Advertencia("Revisar los montos");
                                    TxtEntrega.Focus();
                             
                            }
                    

                    }

                    if (txtcondicioncod.Text == "02")//Credito
                    {
                        if (RucDni == "00000001" || RucDni == "00000000")
                        {
                            Accion.Advertencia("Debe ingresar un cliente con su número de DNI");
                            DialogResult = DialogResult.Cancel;
                        }
                        else
                        {
                            DialogResult = DialogResult.OK;
                        }
                    }
  

                //}
                //else
                //{
                //    Accion.Advertencia("Revisar los montos");
                //    TxtEntrega.Focus();
                //}
            } catch ( Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void txtcondicioncod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtcondicioncod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcondicioncod.Text.Substring(txtcondicioncod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0003";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcondicioncod.Tag = Entidad.Id_General_Det;
                                txtcondicioncod.Text = Entidad.Gen_Codigo_Interno;
                                txtcondiciondesc.Text = Entidad.Gen_Descripcion_Det;
                               
                                if(txtcondicioncod.Text=="02")//Credito
                                {
                                    TxtEntrega.Enabled = false;
                                }
                                else
                                {
                                    TxtEntrega.Enabled = true;
                                }

                                txtcondicioncod.EnterMoveNextControl = true;

                     

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcondicioncod.Text) & string.IsNullOrEmpty(txtcondiciondesc.Text))
                    {
                        BuscarCondicion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarCondicion()
        {
            try
            {
                txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = txtcondicioncod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtcondicioncod.Text.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;

                            if (txtcondicioncod.Text == "02")//Credito
                            {
                                TxtEntrega.Enabled = false;
                            }
                            else
                            {
                                TxtEntrega.Enabled = true;
                            }

                            txtcondicioncod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarCondicionLoad(string condicion_cod)
        {
            try
            {
                condicion_cod = Accion.Formato(condicion_cod, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = condicion_cod
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == condicion_cod.Trim().ToUpper())
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;
                            txtcondicioncod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcondicioncod_TextChanged(object sender, EventArgs e)
        {
            if (txtcondicioncod.Focus() == false)
            {
                txtcondiciondesc.ResetText();

            }
        }





    }
}
