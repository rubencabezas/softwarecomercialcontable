﻿using Contable;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_corte_cierre_edicion : frm_fuente
    {
        public frm_corte_cierre_edicion()
        {
            InitializeComponent();
        }

        public bool Es_Corte,Es_Cierre;
        public bool HacerCorte;

        public string PdvCodigo;
        public string Cja_Codigo;
        public int Cja_Codigo_Apertura;
        public int Cja_Aper_Cierre_Codigo_Item;

        public string NombreImpresora="";
        public bool Es_Termico;
        public bool Es_Epson;
        public decimal IgvActual;

        public decimal XtotalEfectivo, XtotalTarjeta, XtotalInventario;

        private void frm_corte_cierre_edicion_Load(object sender, EventArgs e)
        {

        }

        public Entidad_Caja Ent = new Entidad_Caja();
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Es_Corte)
                {
                    DialogResult = DialogResult.OK;
                }
                else if (Es_Cierre)
                {
                    //  cuando es cierre;
                    Logica_Caja log = new Logica_Caja();
                    if (log.Actualizar_Cierre_Total_caja(Ent)){

                        if (HacerCorte)
                        {
                            if (!log.Insertar_Apertura_Det(Ent))
                            {
                                //Accion.Advertencia("");
                            }

                        }
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        Accion.Advertencia("");
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        public void ImiprimirCierreTotall_()
        {
            try
            {

                ListaSaldoInicial_AperturaCaja();

                Entidad_Caja Doc = new Entidad_Caja();

                if (Es_Corte)
                {
                    VentasDepartamento_Corte_Total();
                    VentasSin_Documento_Corte();
                    Ventas_Otros_Ingresos_Corte();
                    Ventas_Otros_Egresos_Corte();
                    Ventas_Contado_Credito_Corte_Total();


                }

                if (Es_Cierre)
                {
                    VentasDepartamento_CierreTotal();
                    VentasSin_Documento();
                    Ventas_Otros_Ingresos();
                    Ventas_Otros_Egresos();
                   Ventas_Contado_Credito_Cierre_Total();
                }

                Doc.VentasDepartamento_CierreTotal = ListaVentasDepartamento;
                Doc.VentasSin_Documento = ListaVentas_SinDocumento;
                Doc.Ventas_Otros_Ingresos = ListaOtros_Ingresos;
                Doc.Ventas_Otros_Egresos = ListaOtros_Egresos;

                decimal suma_ListaVentasDepartamento=0;
                decimal suma_ListaVentas_SinDocumento = 0;
                decimal suma_ListaOtros_Ingresos = 0;
                decimal suma_ListaOtros_Egresos = 0;


                foreach (Entidad_Caja T in ListaVentasDepartamento)
                {

                    suma_ListaVentasDepartamento += T.Importe;
                }
                foreach (Entidad_Caja T in ListaVentas_SinDocumento)
                {

                    suma_ListaVentas_SinDocumento += T.Importe;
                }
                foreach (Entidad_Caja T in ListaOtros_Ingresos)
                {

                    suma_ListaOtros_Ingresos += T.Importe;
                }
                foreach (Entidad_Caja T in ListaOtros_Egresos)
                {
                    suma_ListaOtros_Egresos += T.Importe;
                }




                // decimal Saldo_Final = (ListaSaldoInicialAperturaCaja[0].Importe + suma_ListaVentasDepartamento + suma_ListaVentas_SinDocumento + suma_ListaOtros_Ingresos) - suma_ListaOtros_Egresos;

                decimal Saldo_Final = (Convert.ToDecimal(ListaVentas_Contado_Credito[0].Cja_Total_Efectivo) + suma_ListaOtros_Ingresos) - suma_ListaOtros_Egresos;


                Rpte_Movimiento_Cierre_Caja RptPOS = new Rpte_Movimiento_Cierre_Caja();
                List<Entidad_Caja> listRpt = new List<Entidad_Caja>();
                listRpt.Add(Doc);

                // Math.Round(XtotalOtrosEgresos, 2).ToString("0.00");

                RptPOS.lbltotalventas.Text = Math.Round(ListaVentas_Contado_Credito[0].Cja_Total_Efectivo + ListaVentas_Contado_Credito[0].Cja_Total_Credito, 2).ToString("0.00");
                RptPOS.LblContado.Text = Math.Round(ListaVentas_Contado_Credito[0].Cja_Total_Efectivo,2).ToString("0.00");
                RptPOS.lblCredito.Text = Math.Round(ListaVentas_Contado_Credito[0].Cja_Total_Credito,2).ToString("0.00");
                RptPOS.lblotrosingresos.Text = Math.Round(suma_ListaOtros_Ingresos,2).ToString("0.00");
                RptPOS.lblotrosegresos.Text = Math.Round(suma_ListaOtros_Egresos,2).ToString("0.00");
                //String.Format("{0:0.00}", 123.4)
                RptPOS.lbl_saldo_final.Text = Math.Round(Saldo_Final,2).ToString("0.00");

                RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                RptPOS.DataSource = listRpt;

                RptPOS.lbl_apertura_caja.Text = Math.Round(ListaSaldoInicialAperturaCaja[0].Importe, 2).ToString("0.00");

                if (Es_Cierre)
                {
                RptPOS.lbl_titulo_reporte.Text = "REPORTE DE CIERRE";
                RptPOS.lbl_texto.Text = "Siendo las horas ............,del dia ......., se procedió al cierre de caja. Teniendo como saldo el importe de .........";
                }
                if (Es_Corte)
                {
                    RptPOS.lbl_titulo_reporte.Text = "REPORTE DE CORTE PARCIAL";
                    RptPOS.lbl_texto.Text = "Yo, ..................................... con DNI N° ..............., realice la entrega del importe de:  ............. Soles, correspondiente a las venta de realizadas del ........... Al ...........  De(mes) del (año), de horas ............. Al ..........";


                }
                //RptPOS.lblplaca.Visible = false;
                //RptPOS.lblhash.Visible = false;
                //RptPOS.CodigoBarras.Visible = false;
                //RptPOS.AutorizacionSunat.Visible = false;
                //RptPOS.DetailPagoTarjeta.Visible = false;
                //RptPOS.DatosVendedor.Visible = false;

                RptPOS.CreateDocument();
                RptPOS.PrinterName = NombreImpresora;

                PdfExportOptions pdfOptions = RptPOS.ExportOptions.Pdf;

                string DocDecision = "";

                 if (Es_Cierre)
                {
                    
                    DocDecision = "Desea imprimir el cierre total";
                   
                    pdfOptions.DocumentOptions.Title = PdvCodigo.Trim() + "-" + Cja_Codigo.Trim()  + "-" + Cja_Codigo_Apertura;
                    string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "CIERRETOTAL");
                    string NombreArchivo = Carpeta + "\\" + PdvCodigo.Trim() + "-" + Cja_Codigo.Trim() + "-" + Cja_Codigo_Apertura + ".pdf";
                    RptPOS.ExportToPdf(NombreArchivo, pdfOptions);
                }
                if (Es_Corte)
                {
                    DocDecision = "Desea imprimir el cierre parcial";
                   
                    pdfOptions.DocumentOptions.Title = PdvCodigo.Trim() + "-" + Cja_Codigo.Trim()  + "-" + Cja_Codigo_Apertura+ "-" + Cja_Aper_Cierre_Codigo_Item;
                    string Carpeta = (Application.StartupPath.ToString() + "\\DOCUMENTOS\\" + "CIERREPARCIAL");
                    string NombreArchivo = Carpeta + "\\" + PdvCodigo.Trim() + "-" + Cja_Codigo.Trim() + "-" + Cja_Codigo_Apertura + "-" + Cja_Aper_Cierre_Codigo_Item + ".pdf";
                    RptPOS.ExportToPdf(NombreArchivo, pdfOptions);
                }


               
                if (Accion.ShowDecision(DocDecision))
                {
                    try
                    {
                        using (ReportPrintTool printTool = new ReportPrintTool(RptPOS))
                        {
                            printTool.Print(NombreImpresora);
                        }

                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }

                }

 


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        public void ImprimirCierreTotall()
        {
            try
            {
                Decimal valor = (Convert.ToDecimal(TxtContado.Text) + (Convert.ToDecimal(TxtTarjeta.Text)));

                ListaSaldoInicial_AperturaCaja();

                PrintRaw pr = new PrintRaw();
                string bufferpr = "";
                bufferpr = (bufferpr + TextoCentro(Actual_Conexion.EmpresaNombre));
               
                bufferpr = (bufferpr + TextoCentro(Actual_Conexion.RucEmpresa));
          
                bufferpr = (bufferpr + TextoCentro(DireccionDescripcionPuntoVenta));
               
                bufferpr = (bufferpr + ("" + "\r\n"));
                bufferpr = (bufferpr + TextoCentro("Cierre total de caja"));
               
                bufferpr = (bufferpr + TextoCentro(txtpdvdesc.Text));

                bufferpr = (bufferpr + ("" + "\r\n"));
                bufferpr = (bufferpr + ("Maq. Registradora".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": "+ (Numero_maquina_registradora + "\r\n"))));
                bufferpr = (bufferpr + ("Fecha".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": " + (DateTime.Now.ToShortDateString() + "\r\n"))));
                bufferpr = (bufferpr + ("Hora".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": "  + (DateTime.Now.ToShortTimeString() + "\r\n"))));
                bufferpr = (bufferpr + ("Operador DNI".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": " + (Actual_Conexion.UserID + "\r\n"))));
                bufferpr = (bufferpr + FormatearLimite(("Operador Nombres".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": " + Actual_Conexion.UserName))));
                bufferpr = (bufferpr + ("Subtotal".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": " + (Convert.ToString(Math.Round(valor - (IgvActual * valor),2))) + "\r\n")));
                bufferpr = (bufferpr + ("I.G.V.".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": "   + (Convert.ToString(Math.Round(IgvActual * valor,2))) + "\r\n")));
                bufferpr = (bufferpr + ("Total".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": " + (valor) + "\r\n")));
                bufferpr = (bufferpr + ("SALDO INICIAL CAJA".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": " +  (Convert.ToString(Math.Round(ListaSaldoInicialAperturaCaja[0].Importe,2))) + "\r\n")));

  
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));
                bufferpr = (bufferpr + TextoCentro("VENTAS POR PRODUCTO"));
                bufferpr = (bufferpr + ("" + "\r\n"));
                VentasDepartamento_CierreTotal();
                bufferpr = (bufferpr + ("DESCRIPCION              CANT.   IMPORTE" + "\r\n"));
                bufferpr = (bufferpr + ("" + "\r\n"));

                Decimal TotalDepartamento=0;

                foreach ( Entidad_Caja Det in ListaVentasDepartamento)
                {
                    bufferpr = (bufferpr + AgregaDetalleDepartamento(Det.Catalogo_Descripcion, Convert.ToString(Det.Cantidad),Convert.ToString(Math.Round(Det.Importe,2))));
                    TotalDepartamento = (TotalDepartamento + Det.Importe);
                }

                bufferpr = (bufferpr + ("" + "\r\n"));
                bufferpr = (bufferpr + ("TOTAL: S/".PadLeft(16) + (Convert.ToString(Math.Round(TotalDepartamento,2)).PadLeft(((Convert.ToDecimal(NumCaracteresFila) == 42) ? 26 : 24)) + "\r\n")));
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));


   
                bufferpr = (bufferpr + TextoCentro("VENTAS SIN DOCUMENTO"));
                bufferpr = (bufferpr + ("" + "\r\n"));
                VentasSin_Documento();
                bufferpr = (bufferpr + ("DESCRIPCION              CANT.   IMPORTE" + "\r\n"));
                bufferpr = (bufferpr + ("" + "\r\n"));

                Decimal TotalSinDocumento = 0;

                foreach (Entidad_Caja Det in ListaVentas_SinDocumento)
                {
                    bufferpr = (bufferpr + AgregaDetalleDepartamento(Det.Catalogo_Descripcion, Convert.ToString(Det.Cantidad), Convert.ToString(Math.Round(Det.Importe,2))));
                    TotalSinDocumento = (TotalSinDocumento + Det.Importe);
                }
                bufferpr = (bufferpr + ("" + "\r\n"));
                bufferpr = (bufferpr + ("TOTAL: S/".PadLeft(16) + (Convert.ToString(Math.Round(TotalSinDocumento,2)).PadLeft(((Convert.ToDecimal(NumCaracteresFila) == 42) ? 26 : 24)) + "\r\n")));
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));


                bufferpr = (bufferpr + TextoCentro("OTROS INGRESOS"));
                bufferpr = (bufferpr + ("" + "\r\n"));
                Ventas_Otros_Ingresos();
                bufferpr = (bufferpr + ("DESCRIPCION              CANT.   IMPORTE" + "\r\n"));
                bufferpr = (bufferpr + ("" + "\r\n"));

                Decimal TotalOtrosIngresos = 0;

                foreach (Entidad_Caja Det in ListaOtros_Ingresos)
                {
                    bufferpr = (bufferpr + AgregaDetalleDepartamento(Det.Catalogo_Descripcion, Convert.ToString(Det.Cantidad), Convert.ToString(Math.Round(Det.Importe,2))));
                    TotalOtrosIngresos = (TotalOtrosIngresos + Det.Importe);
                }
                bufferpr = (bufferpr + ("" + "\r\n"));
                bufferpr = (bufferpr + ("TOTAL: S/".PadLeft(16) + (Convert.ToString(Math.Round(TotalOtrosIngresos,2)).PadLeft(((Convert.ToDecimal(NumCaracteresFila) == 42) ? 26 : 24)) + "\r\n")));
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));


                bufferpr = (bufferpr + TextoCentro("OTROS EGRESOS"));
                bufferpr = (bufferpr + ("" + "\r\n"));
                Ventas_Otros_Egresos();
                bufferpr = (bufferpr + ("DESCRIPCION              CANT.   IMPORTE" + "\r\n"));
                bufferpr = (bufferpr + ("" + "\r\n"));

                Decimal TotalOtrosEgresos = 0;

                foreach (Entidad_Caja Det in ListaOtros_Egresos)
                {
                    bufferpr = (bufferpr + AgregaDetalleDepartamento(Det.Catalogo_Descripcion, Convert.ToString(Det.Cantidad), Convert.ToString(Math.Round(Det.Importe,2))));
                    TotalOtrosEgresos = (TotalOtrosEgresos + Det.Importe);
                }
                bufferpr = (bufferpr + ("" + "\r\n"));
                bufferpr = (bufferpr + ("TOTAL: S/".PadLeft(16) + (Convert.ToString(Math.Round(TotalOtrosEgresos, 2)).PadLeft(((Convert.ToDecimal(NumCaracteresFila) == 42) ? 26 : 24)) + "\r\n")));
                bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));


                bufferpr = (bufferpr + TextoCentro("Ventas Por Tipo Pago"));
                //bufferpr = (bufferpr + ("" + "\r\n"));
                //bufferpr = (bufferpr + ("EFECTIVO".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": S/ " + (TxtEfectivo.Text + "\r\n"))));
                ////bufferpr = (bufferpr + ("TARJETA DE CREDITO".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": S/ " + (TxtTarjeta.Text + "\r\n"))));
                ////bufferpr = (bufferpr + ("CREDITO".PadRight(((NumCaracteresFila == 42) ? 20 : 18)) + (": S/ " + (TxtCredito.Text + "\r\n"))));
                //bufferpr = (bufferpr + ("" + "\r\n"));
                //bufferpr = (bufferpr + ("TOTAL: S/".PadLeft(16) + (Convert.ToString(valor).PadLeft(((NumCaracteresFila == 42) ? 26 : 24)) + "\r\n")));
                //bufferpr = (bufferpr + ("" + "\r\n"));
                //bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));
                //bufferpr = (bufferpr + ("" + "\r\n"));

                //bufferpr = (bufferpr + ("Ventas por Usuario" + "\r\n"));
                //bufferpr = (bufferpr + ("" + "\r\n"));
                //VentasPorUsuario();
                //if ((ListaVentasPorUsuario.Count > 0))
                //{
                //    bufferpr = (bufferpr + ("     CONTADO        CREDITO" + "\r\n"));
                //}

                //foreach (Detu in ListaVentasPorUsuario)
                //{
                //    bufferpr = (bufferpr + (Detu.Vendedor_Nombre + "\r\n"));
                //    bufferpr = (bufferpr + ((("S/ " + Format(Detu.Venta_Contado, "0.00"))).PadLeft(12)   + ((("S/ " + Format(Detu.Venta_Credito, "0.00"))).PadLeft(15) + "\r\n")));
                //}
                //bufferpr = (bufferpr + ("" + "\r\n"));

                //bufferpr = (bufferpr + ("TOTAL: S/".PadLeft(16) + (Convert.ToString(valor).PadLeft(((NumCaracteresFila == 42) ? 26 : 24)) + "\r\n")));
                //bufferpr = (bufferpr + (new string('-', NumCaracteresFila) + "\r\n"));

                //Config_Establecimiento Guardar = new Config_Establecimiento();

                //Guardar.GuardarDocumentoCorteCierre(bufferpr, txtpdvdesc.Text.Trim(), 2);

                if (Es_Epson)
                {
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ("" + "\r\n"));
                    bufferpr = (bufferpr + ('' + 'i'));
                }
                else
                {
                    bufferpr = (bufferpr + Convert.ToString((char)(27)).ToString() + "d" + (char)(2)).ToString();
                    bufferpr = (bufferpr + Convert.ToString((char)(7)).ToString());
                }

                PrintRaw.SendStringToPrinter(NombreImpresora, bufferpr);

                if (Es_Termico)
                {
                    //System.Drawing.Printing.PrintDocument Print = new System.Drawing.Printing.PrintDocument();
                    //Print.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
                    //Print.PrinterSettings.PrinterName = NombreImpresora;
                    //Print.PrintPage += new System.EventHandler(PrintPage();

                    //Print.Print();
                }

            } catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


        void PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString("", new Font("Arial", 5), Brushes.Black, 0, 0);
        }

        int NumCaracteresFila=42;
        string DireccionDescripcionPuntoVenta="";
        string Numero_maquina_registradora="";
        public string TextoCentro(string StrC)
        {
            string StrPrint = "";
            string VNext = "";
            if ((StrC.Length > NumCaracteresFila))
            {
                string[] TxtWords = StrC.Split(Convert.ToChar(" "));
                string CurrentSub = "";
                string AfterSub = "";
                bool FoundLength = false;
                foreach (string Txt in TxtWords)
                {
                    if ((((CurrentSub + (" " + Txt)).Length <= NumCaracteresFila) && (FoundLength == false)))
                    {
                        CurrentSub = (CurrentSub + (" " + Txt));
                    }
                    else
                    {
                        FoundLength = true;
                        AfterSub = (AfterSub + (" " + Txt));
                    }

                }

                StrPrint = CurrentSub;
                VNext = TextoCentro(AfterSub);
            }
            else
            {
                StrPrint = StrC;
            }

            if ((StrPrint.Trim().Length == NumCaracteresFila))
            {
                return (StrPrint + ("\n" + VNext));
            }
            else
            {
                int espc = ((NumCaracteresFila - StrPrint.Trim().Length) / 2);
                StrPrint = (new string(' ', espc) + StrPrint.Trim());
                // & StrDup(espc, " ")
                return (StrPrint + ("\n" + VNext));
            }

        }



        string FormatearLimite(string StrF)
        {
            string StrPrint = "";
            string VNext = "";
            if ((StrF.Length > NumCaracteresFila))
            {
                string[] TxtWords = StrF.Split(Convert.ToChar(" "));
                string CurrentSub = "";
                string AfterSub = "";
                bool FoundLength = false;
                foreach (string Txt in TxtWords)
                {
                    if ((((CurrentSub + (" " + Txt)).Length <= NumCaracteresFila) && (FoundLength == false)))
                    {
                        CurrentSub = (CurrentSub + (" " + Txt)).Trim();
                    }
                    else
                    {
                        FoundLength = true;
                        AfterSub = (AfterSub + (" " + Txt));
                    }

                }

                StrPrint = CurrentSub;
                VNext = FormatearLimite(AfterSub.Trim());
            }
            else
            {
                StrPrint = StrF.Trim();
            }

            return (StrPrint + ("\r\n" + VNext));
        }



        public string AgregaDetalleDepartamento(string descripcion, string cant, string importe)
        {
            string StrPrint = "";
            string DescripLarga = "";
            if ((descripcion.Trim().Length > 21))
            {
                StrPrint = (StrPrint + (descripcion.Substring(0, 21) + " "));
                DescripLarga = DescripcionLargaDepartamento(descripcion.Substring(21));
            }
            else
            {
                StrPrint = (StrPrint + descripcion.PadRight(22));
            }

            // --Cantidad
            StrPrint = (StrPrint + (cant.PadLeft(8) + " "));
            StrPrint = (StrPrint + importe.PadLeft(((NumCaracteresFila == 42) ? 11 : 9)));
            if (!string.IsNullOrWhiteSpace(DescripLarga))
            {
                StrPrint = (StrPrint + DescripLarga);
            }

            return StrPrint;
        }



        string DescripcionLargaDepartamento(string strr)
        {
            string strRestante = "";
            if ((strr.Length > 21))
            {
                string StrNext = strr.Substring(21);
                return (strRestante + (strr.Substring(0, 21) + ("\r\n" + DescripcionLargaDepartamento(StrNext))));
            }
            else
            {
                return (strRestante + (strr + "\r\n"));
            }

        }

        //SALDO INICIAL DE CAJA
        List<Entidad_Caja> ListaSaldoInicialAperturaCaja = new List<Entidad_Caja>();
        void ListaSaldoInicial_AperturaCaja()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;

                ListaSaldoInicialAperturaCaja = LogAf.Saldo_Inicial_Apertura_Caja(Entidad);
                if ((ListaSaldoInicialAperturaCaja.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        List<Entidad_Caja> ListaVentasDepartamento = new List<Entidad_Caja>();
        List<Entidad_Caja> ListaVentas_Contado_Credito= new List<Entidad_Caja>();
        void VentasDepartamento_CierreTotal()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo =Cja_Codigo_Apertura;

                ListaVentasDepartamento = LogAf.Buscar_Ventas_Departamento_CierreTotal(Entidad);
                if ((ListaVentasDepartamento.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void Ventas_Contado_Credito_Cierre_Total()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
               

                ListaVentas_Contado_Credito = LogAf.Buscar_Ventas_Contado_Credito_Cierre_Total(Entidad);
                if ((ListaVentas_Contado_Credito.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void Ventas_Contado_Credito_Corte_Total()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
                Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaVentas_Contado_Credito = LogAf.Buscar_Ventas_Contado_Credito_Corte_Total(Entidad);
                if ((ListaVentas_Contado_Credito.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        void VentasDepartamento_Corte_Total()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
                Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaVentasDepartamento = LogAf.Buscar_Ventas_Departamento_Corte_Total(Entidad);
                if ((ListaVentasDepartamento.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        List<Entidad_Caja> ListaVentas_SinDocumento = new List<Entidad_Caja>();
        void VentasSin_Documento()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo =Cja_Codigo_Apertura;

                ListaVentas_SinDocumento = LogAf.Buscar_Ventas_Sin_Documentos_Cierre_Total(Entidad);
                if ((ListaVentas_SinDocumento.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void VentasSin_Documento_Corte()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
                Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaVentas_SinDocumento = LogAf.Buscar_Ventas_Sin_Documentos_Corte_Total(Entidad);
                if ((ListaVentas_SinDocumento.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        List<Entidad_Caja> ListaOtros_Ingresos = new List<Entidad_Caja>();
        void Ventas_Otros_Ingresos()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;

                ListaOtros_Ingresos = LogAf.Buscar_Otros_Ingresos_Cierre_Total(Entidad);
                if ((ListaOtros_Ingresos.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void Ventas_Otros_Ingresos_Corte()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
                Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaOtros_Ingresos = LogAf.Buscar_Otros_Ingresos_Corte_Total(Entidad);
                if ((ListaOtros_Ingresos.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        List<Entidad_Caja> ListaOtros_Egresos = new List<Entidad_Caja>();
        void Ventas_Otros_Egresos()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;

                ListaOtros_Egresos = LogAf.Buscar_Otros_Egresos_Cierre_Total(Entidad);
                if ((ListaOtros_Egresos.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        void Ventas_Otros_Egresos_Corte()
        {
            try
            {
                Logica_Caja LogAf = new Logica_Caja();
                Entidad_Caja Entidad = new Entidad_Caja();

                Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Entidad.Pdv_Codigo = PdvCodigo;
                Entidad.Cja_Codigo = Cja_Codigo;
                Entidad.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
                Entidad.Cja_Aper_Cierre_Codigo_Item = Cja_Aper_Cierre_Codigo_Item;

                ListaOtros_Egresos = LogAf.Buscar_Otros_Egresos_Corte_Total(Entidad);
                if ((ListaOtros_Egresos.Count > 0))
                {

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

    }
}
