﻿namespace Comercial
{
    partial class frm_pos_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.BarraHerramienta = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.BSIFecha = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.BSIEstablecimiento = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.BSIVendedorNombre = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem9 = new DevExpress.XtraBars.BarStaticItem();
            this.BSITipoCambio = new DevExpress.XtraBars.BarStaticItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btncobrarteclacorta = new DevExpress.XtraBars.BarButtonItem();
            this.btncancelarteclacorta = new DevExpress.XtraBars.BarButtonItem();
            this.btnmovcajateclacorta = new DevExpress.XtraBars.BarButtonItem();
            this.btnmodificarteclacorta = new DevExpress.XtraBars.BarButtonItem();
            this.btnquitarteclacorta = new DevExpress.XtraBars.BarButtonItem();
            this.btncierretotalcajateclacorta = new DevExpress.XtraBars.BarButtonItem();
            this.BSIPuntoVenta = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barStaticItem7 = new DevExpress.XtraBars.BarStaticItem();
            this.barHeaderItem1 = new DevExpress.XtraBars.BarHeaderItem();
            this.barStaticItem10 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem11 = new DevExpress.XtraBars.BarStaticItem();
            this.btncobrar = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PopControlConModVenta = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.CheckBtnTicketFac = new DevExpress.XtraEditors.CheckButton();
            this.CheckBtnTicketBol = new DevExpress.XtraEditors.CheckButton();
            this.CheckBtnNotSal = new DevExpress.XtraEditors.CheckButton();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.BSITipoDocumento = new DevExpress.XtraEditors.TextEdit();
            this.DDBtnModVenta = new DevExpress.XtraEditors.DropDownButton();
            this.BSINumeroDocum = new DevExpress.XtraEditors.TextEdit();
            this.BSISerieDocum = new DevExpress.XtraEditors.TextEdit();
            this.txtpdvdesc = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.gbxBotones = new System.Windows.Forms.GroupBox();
            this.BtnCorte = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCerrar = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnvistapreviaImpresion = new DevExpress.XtraEditors.SimpleButton();
            this.btnmovcaja = new DevExpress.XtraEditors.SimpleButton();
            this.gbdatoscliente = new System.Windows.Forms.GroupBox();
            this.btnmodificarcliente = new DevExpress.XtraEditors.SimpleButton();
            this.txtdireccion = new DevExpress.XtraEditors.TextEdit();
            this.txtclientedesc = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtrucdni = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gbxAgregarDetalle = new System.Windows.Forms.GroupBox();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BtnQuitar = new DevExpress.XtraEditors.SimpleButton();
            this.BtnModificar = new DevExpress.XtraEditors.SimpleButton();
            this.PanelTotales = new System.Windows.Forms.Panel();
            this.TxtRsImporteTotal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtRsIGVMonto = new System.Windows.Forms.Label();
            this.LblIGV = new System.Windows.Forms.Label();
            this.TxtRsOtrosTribCarg = new System.Windows.Forms.Label();
            this.TxtRsValExporta = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtRsISC = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtRsInAfecta = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtRsExonerada = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtRsBaseImponible = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnuevo = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopControlConModVenta)).BeginInit();
            this.PopControlConModVenta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSITipoDocumento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSINumeroDocum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSISerieDocum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            this.gbxBotones.SuspendLayout();
            this.gbdatoscliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdireccion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtclientedesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).BeginInit();
            this.gbxAgregarDetalle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.PanelTotales.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.BarraHerramienta});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.BSIFecha,
            this.barStaticItem2,
            this.BSIEstablecimiento,
            this.barStaticItem3,
            this.BSIVendedorNombre,
            this.barStaticItem4,
            this.barStaticItem5,
            this.barStaticItem6,
            this.barLargeButtonItem1,
            this.barStaticItem7,
            this.barHeaderItem1,
            this.barStaticItem9,
            this.BSITipoCambio,
            this.barStaticItem10,
            this.barStaticItem11,
            this.barSubItem1,
            this.btncobrarteclacorta,
            this.BSIPuntoVenta,
            this.btncancelarteclacorta,
            this.btnmovcajateclacorta,
            this.btnmodificarteclacorta,
            this.btnquitarteclacorta,
            this.btncierretotalcajateclacorta});
            this.barManager1.MaxItemId = 28;
            // 
            // BarraHerramienta
            // 
            this.BarraHerramienta.BarName = "Herramientas";
            this.BarraHerramienta.DockCol = 0;
            this.BarraHerramienta.DockRow = 0;
            this.BarraHerramienta.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.BarraHerramienta.FloatLocation = new System.Drawing.Point(265, 153);
            this.BarraHerramienta.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.BSIFecha),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.BSIEstablecimiento),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.BSIVendedorNombre),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.BSITipoCambio),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.BSIPuntoVenta)});
            this.BarraHerramienta.OptionsBar.AllowCollapse = true;
            this.BarraHerramienta.OptionsBar.AllowQuickCustomization = false;
            this.BarraHerramienta.OptionsBar.AllowRename = true;
            this.BarraHerramienta.OptionsBar.DisableClose = true;
            this.BarraHerramienta.OptionsBar.DisableCustomization = true;
            this.BarraHerramienta.OptionsBar.DrawDragBorder = false;
            this.BarraHerramienta.OptionsBar.MultiLine = true;
            this.BarraHerramienta.OptionsBar.UseWholeRow = true;
            this.BarraHerramienta.Text = "Tools";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem1.Caption = "FECHA:";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.barStaticItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // BSIFecha
            // 
            this.BSIFecha.Caption = "2014-12-01";
            this.BSIFecha.Id = 1;
            this.BSIFecha.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.BSIFecha.ItemAppearance.Normal.Options.UseFont = true;
            this.BSIFecha.Name = "BSIFecha";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem2.Caption = "ESTABLECIMIENTO:";
            this.barStaticItem2.Id = 2;
            this.barStaticItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.barStaticItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // BSIEstablecimiento
            // 
            this.BSIEstablecimiento.Caption = "Establecimiento";
            this.BSIEstablecimiento.Id = 3;
            this.BSIEstablecimiento.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.BSIEstablecimiento.ItemAppearance.Normal.Options.UseFont = true;
            this.BSIEstablecimiento.Name = "BSIEstablecimiento";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem3.Caption = "VENDEDOR:";
            this.barStaticItem3.Id = 4;
            this.barStaticItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.barStaticItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // BSIVendedorNombre
            // 
            this.BSIVendedorNombre.Caption = "USUARIOO";
            this.BSIVendedorNombre.Id = 5;
            this.BSIVendedorNombre.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.BSIVendedorNombre.ItemAppearance.Normal.Options.UseFont = true;
            this.BSIVendedorNombre.Name = "BSIVendedorNombre";
            this.BSIVendedorNombre.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // barStaticItem9
            // 
            this.barStaticItem9.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem9.Caption = "TIPO DE CAMBIO:";
            this.barStaticItem9.Id = 14;
            this.barStaticItem9.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.barStaticItem9.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem9.Name = "barStaticItem9";
            // 
            // BSITipoCambio
            // 
            this.BSITipoCambio.Caption = "1.000";
            this.BSITipoCambio.Id = 15;
            this.BSITipoCambio.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BSITipoCambio.ItemAppearance.Normal.Options.UseFont = true;
            this.BSITipoCambio.Name = "BSITipoCambio";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Teclas Cortas";
            this.barSubItem1.Id = 20;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btncobrarteclacorta),
            new DevExpress.XtraBars.LinkPersistInfo(this.btncancelarteclacorta),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnmovcajateclacorta),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnmodificarteclacorta),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnquitarteclacorta),
            new DevExpress.XtraBars.LinkPersistInfo(this.btncierretotalcajateclacorta)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btncobrarteclacorta
            // 
            this.btncobrarteclacorta.Caption = "Cobrar";
            this.btncobrarteclacorta.Id = 21;
            this.btncobrarteclacorta.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btncobrarteclacorta.Name = "btncobrarteclacorta";
            this.btncobrarteclacorta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncobrarteclacorta_ItemClick);
            // 
            // btncancelarteclacorta
            // 
            this.btncancelarteclacorta.Caption = "Cancelar";
            this.btncancelarteclacorta.Id = 23;
            this.btncancelarteclacorta.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.btncancelarteclacorta.Name = "btncancelarteclacorta";
            this.btncancelarteclacorta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncancelarteclacorta_ItemClick);
            // 
            // btnmovcajateclacorta
            // 
            this.btnmovcajateclacorta.Caption = "Mov Caja";
            this.btnmovcajateclacorta.Id = 24;
            this.btnmovcajateclacorta.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmovcajateclacorta.Name = "btnmovcajateclacorta";
            this.btnmovcajateclacorta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnmovcajateclacorta_ItemClick);
            // 
            // btnmodificarteclacorta
            // 
            this.btnmodificarteclacorta.Caption = "Modificar";
            this.btnmodificarteclacorta.Id = 25;
            this.btnmodificarteclacorta.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btnmodificarteclacorta.Name = "btnmodificarteclacorta";
            this.btnmodificarteclacorta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnmodificarteclacorta_ItemClick);
            // 
            // btnquitarteclacorta
            // 
            this.btnquitarteclacorta.Caption = "Quitar";
            this.btnquitarteclacorta.Id = 26;
            this.btnquitarteclacorta.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnquitarteclacorta.Name = "btnquitarteclacorta";
            this.btnquitarteclacorta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnquitarteclacorta_ItemClick);
            // 
            // btncierretotalcajateclacorta
            // 
            this.btncierretotalcajateclacorta.Caption = "Cierre Total Caja";
            this.btncierretotalcajateclacorta.Id = 27;
            this.btncierretotalcajateclacorta.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btncierretotalcajateclacorta.Name = "btncierretotalcajateclacorta";
            this.btncierretotalcajateclacorta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncierretotalcajateclacorta_ItemClick);
            // 
            // BSIPuntoVenta
            // 
            this.BSIPuntoVenta.Caption = "Pdv";
            this.BSIPuntoVenta.Id = 22;
            this.BSIPuntoVenta.Name = "BSIPuntoVenta";
            this.BSIPuntoVenta.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1191, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 564);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1191, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 538);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1191, 26);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 538);
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "                                                                                 " +
    "  \r\n";
            this.barStaticItem4.Id = 6;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "COMPROBANTE DE VENTA";
            this.barStaticItem5.Id = 7;
            this.barStaticItem5.Name = "barStaticItem5";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Caption = "barStaticItem6";
            this.barStaticItem6.Id = 8;
            this.barStaticItem6.Name = "barStaticItem6";
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "barLargeButtonItem1";
            this.barLargeButtonItem1.Id = 9;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            // 
            // barStaticItem7
            // 
            this.barStaticItem7.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem7.Caption = "barStaticItem7";
            this.barStaticItem7.Id = 10;
            this.barStaticItem7.Name = "barStaticItem7";
            // 
            // barHeaderItem1
            // 
            this.barHeaderItem1.Caption = "barHeaderItem1";
            this.barHeaderItem1.Id = 11;
            this.barHeaderItem1.Name = "barHeaderItem1";
            // 
            // barStaticItem10
            // 
            this.barStaticItem10.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem10.Caption = "SERIE:";
            this.barStaticItem10.Id = 16;
            this.barStaticItem10.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.barStaticItem10.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem10.Name = "barStaticItem10";
            // 
            // barStaticItem11
            // 
            this.barStaticItem11.Caption = "NUMERO:";
            this.barStaticItem11.Id = 18;
            this.barStaticItem11.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.barStaticItem11.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem11.Name = "barStaticItem11";
            // 
            // btncobrar
            // 
            this.btncobrar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncobrar.Appearance.Options.UseFont = true;
            this.btncobrar.ImageOptions.Image = global::Contable.Properties.Resources.cobrarpos;
            this.btncobrar.Location = new System.Drawing.Point(174, 14);
            this.btncobrar.Name = "btncobrar";
            this.btncobrar.Size = new System.Drawing.Size(151, 40);
            this.btncobrar.TabIndex = 4;
            this.btncobrar.Text = "Cobrar [F5]";
            this.btncobrar.Click += new System.EventHandler(this.btncobrar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PopControlConModVenta);
            this.groupBox2.Controls.Add(this.textEdit1);
            this.groupBox2.Controls.Add(this.BSITipoDocumento);
            this.groupBox2.Controls.Add(this.DDBtnModVenta);
            this.groupBox2.Controls.Add(this.BSINumeroDocum);
            this.groupBox2.Controls.Add(this.BSISerieDocum);
            this.groupBox2.Controls.Add(this.txtpdvdesc);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(0, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(353, 186);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // PopControlConModVenta
            // 
            this.PopControlConModVenta.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PopControlConModVenta.Controls.Add(this.CheckBtnTicketFac);
            this.PopControlConModVenta.Controls.Add(this.CheckBtnTicketBol);
            this.PopControlConModVenta.Controls.Add(this.CheckBtnNotSal);
            this.PopControlConModVenta.Location = new System.Drawing.Point(12, 56);
            this.PopControlConModVenta.Manager = this.barManager1;
            this.PopControlConModVenta.Name = "PopControlConModVenta";
            this.PopControlConModVenta.Size = new System.Drawing.Size(331, 111);
            this.PopControlConModVenta.TabIndex = 16;
            this.PopControlConModVenta.Visible = false;
            this.PopControlConModVenta.Paint += new System.Windows.Forms.PaintEventHandler(this.PopControlConModVenta_Paint);
            // 
            // CheckBtnTicketFac
            // 
            this.CheckBtnTicketFac.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.CheckBtnTicketFac.Appearance.Options.UseFont = true;
            this.CheckBtnTicketFac.GroupIndex = 1;
            this.CheckBtnTicketFac.ImageOptions.ImageToTextIndent = 1;
            this.CheckBtnTicketFac.Location = new System.Drawing.Point(4, 77);
            this.CheckBtnTicketFac.Name = "CheckBtnTicketFac";
            this.CheckBtnTicketFac.Size = new System.Drawing.Size(324, 30);
            this.CheckBtnTicketFac.TabIndex = 0;
            this.CheckBtnTicketFac.TabStop = false;
            this.CheckBtnTicketFac.Text = "Factura";
            this.CheckBtnTicketFac.CheckedChanged += new System.EventHandler(this.CheckBtnTicketFac_CheckedChanged);
            // 
            // CheckBtnTicketBol
            // 
            this.CheckBtnTicketBol.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBtnTicketBol.Appearance.Options.UseFont = true;
            this.CheckBtnTicketBol.GroupIndex = 1;
            this.CheckBtnTicketBol.ImageOptions.Image = global::Contable.Properties.Resources.Ticket;
            this.CheckBtnTicketBol.Location = new System.Drawing.Point(4, 41);
            this.CheckBtnTicketBol.Name = "CheckBtnTicketBol";
            this.CheckBtnTicketBol.Size = new System.Drawing.Size(324, 30);
            this.CheckBtnTicketBol.TabIndex = 0;
            this.CheckBtnTicketBol.TabStop = false;
            this.CheckBtnTicketBol.Text = "Boleta";
            this.CheckBtnTicketBol.CheckedChanged += new System.EventHandler(this.CheckBtnTicketBol_CheckedChanged);
            // 
            // CheckBtnNotSal
            // 
            this.CheckBtnNotSal.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBtnNotSal.Appearance.Options.UseFont = true;
            this.CheckBtnNotSal.GroupIndex = 1;
            this.CheckBtnNotSal.ImageOptions.Image = global::Contable.Properties.Resources.Ticket;
            this.CheckBtnNotSal.Location = new System.Drawing.Point(4, 5);
            this.CheckBtnNotSal.Name = "CheckBtnNotSal";
            this.CheckBtnNotSal.Size = new System.Drawing.Size(324, 30);
            this.CheckBtnNotSal.TabIndex = 0;
            this.CheckBtnNotSal.TabStop = false;
            this.CheckBtnNotSal.Text = "Ticket Interno";
            this.CheckBtnNotSal.CheckedChanged += new System.EventHandler(this.CheckBtnNotSal_CheckedChanged);
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "COMPROBANTE";
            this.textEdit1.Location = new System.Drawing.Point(12, 78);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(335, 22);
            this.textEdit1.TabIndex = 16;
            // 
            // BSITipoDocumento
            // 
            this.BSITipoDocumento.EditValue = "COMPROBANTE";
            this.BSITipoDocumento.Location = new System.Drawing.Point(12, 106);
            this.BSITipoDocumento.Name = "BSITipoDocumento";
            this.BSITipoDocumento.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BSITipoDocumento.Properties.Appearance.Options.UseFont = true;
            this.BSITipoDocumento.Properties.Appearance.Options.UseTextOptions = true;
            this.BSITipoDocumento.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BSITipoDocumento.Properties.ReadOnly = true;
            this.BSITipoDocumento.Size = new System.Drawing.Size(335, 22);
            this.BSITipoDocumento.TabIndex = 16;
            // 
            // DDBtnModVenta
            // 
            this.DDBtnModVenta.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DDBtnModVenta.Appearance.Options.UseFont = true;
            this.DDBtnModVenta.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.DDBtnModVenta.DropDownControl = this.PopControlConModVenta;
            this.DDBtnModVenta.ImageOptions.Image = global::Contable.Properties.Resources.Tipo_venta;
            this.DDBtnModVenta.Location = new System.Drawing.Point(12, 37);
            this.DDBtnModVenta.MenuManager = this.barManager1;
            this.DDBtnModVenta.Name = "DDBtnModVenta";
            this.DDBtnModVenta.Size = new System.Drawing.Size(335, 32);
            this.DDBtnModVenta.TabIndex = 0;
            this.DDBtnModVenta.Text = "Tipo Documento";
            // 
            // BSINumeroDocum
            // 
            this.BSINumeroDocum.EditValue = "NUMERO";
            this.BSINumeroDocum.Location = new System.Drawing.Point(150, 156);
            this.BSINumeroDocum.Name = "BSINumeroDocum";
            this.BSINumeroDocum.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BSINumeroDocum.Properties.Appearance.Options.UseFont = true;
            this.BSINumeroDocum.Properties.Appearance.Options.UseTextOptions = true;
            this.BSINumeroDocum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BSINumeroDocum.Properties.ReadOnly = true;
            this.BSINumeroDocum.Size = new System.Drawing.Size(194, 22);
            this.BSINumeroDocum.TabIndex = 16;
            // 
            // BSISerieDocum
            // 
            this.BSISerieDocum.EditValue = "SERIE";
            this.BSISerieDocum.Location = new System.Drawing.Point(10, 156);
            this.BSISerieDocum.Name = "BSISerieDocum";
            this.BSISerieDocum.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BSISerieDocum.Properties.Appearance.Options.UseFont = true;
            this.BSISerieDocum.Properties.Appearance.Options.UseTextOptions = true;
            this.BSISerieDocum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BSISerieDocum.Properties.ReadOnly = true;
            this.BSISerieDocum.Size = new System.Drawing.Size(134, 22);
            this.BSISerieDocum.TabIndex = 16;
            // 
            // txtpdvdesc
            // 
            this.txtpdvdesc.EditValue = "PUNTO DE VENTA";
            this.txtpdvdesc.Location = new System.Drawing.Point(12, 11);
            this.txtpdvdesc.MenuManager = this.barManager1;
            this.txtpdvdesc.Name = "txtpdvdesc";
            this.txtpdvdesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpdvdesc.Properties.Appearance.Options.UseFont = true;
            this.txtpdvdesc.Properties.Appearance.Options.UseTextOptions = true;
            this.txtpdvdesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtpdvdesc.Properties.ReadOnly = true;
            this.txtpdvdesc.Size = new System.Drawing.Size(335, 22);
            this.txtpdvdesc.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SteelBlue;
            this.label7.Location = new System.Drawing.Point(227, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "NUMERO";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(64, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "SERIE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.txtproductodesc);
            this.groupBox1.Location = new System.Drawing.Point(9, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 46);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Contable.Properties.Resources.barra5;
            this.pictureBox1.Location = new System.Drawing.Point(7, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(90, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtproductodesc.EditValue = "PRODUCTO";
            this.txtproductodesc.Location = new System.Drawing.Point(103, 12);
            this.txtproductodesc.MenuManager = this.barManager1;
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtproductodesc.Properties.Appearance.Options.UseFont = true;
            this.txtproductodesc.Size = new System.Drawing.Size(383, 22);
            this.txtproductodesc.TabIndex = 17;
            this.txtproductodesc.Enter += new System.EventHandler(this.txtproductodesc_Enter);
            this.txtproductodesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtproductodesc_KeyPress);
            this.txtproductodesc.Leave += new System.EventHandler(this.txtproductodesc_Leave);
            // 
            // gbxBotones
            // 
            this.gbxBotones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbxBotones.Controls.Add(this.BtnCorte);
            this.gbxBotones.Controls.Add(this.BtnCerrar);
            this.gbxBotones.Controls.Add(this.BtnCancelar);
            this.gbxBotones.Controls.Add(this.btnvistapreviaImpresion);
            this.gbxBotones.Controls.Add(this.btnmovcaja);
            this.gbxBotones.Controls.Add(this.btncobrar);
            this.gbxBotones.Location = new System.Drawing.Point(6, 403);
            this.gbxBotones.Name = "gbxBotones";
            this.gbxBotones.Size = new System.Drawing.Size(347, 152);
            this.gbxBotones.TabIndex = 22;
            this.gbxBotones.TabStop = false;
            // 
            // BtnCorte
            // 
            this.BtnCorte.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCorte.Appearance.Options.UseFont = true;
            this.BtnCorte.Appearance.Options.UseTextOptions = true;
            this.BtnCorte.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BtnCorte.ImageOptions.Image = global::Contable.Properties.Resources.Cierrepos;
            this.BtnCorte.Location = new System.Drawing.Point(10, 59);
            this.BtnCorte.Name = "BtnCorte";
            this.BtnCorte.Size = new System.Drawing.Size(151, 40);
            this.BtnCorte.TabIndex = 163;
            this.BtnCorte.Text = "Cierre\r\nparcial";
            this.BtnCorte.Click += new System.EventHandler(this.BtnCorte_Click);
            // 
            // BtnCerrar
            // 
            this.BtnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCerrar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCerrar.Appearance.Options.UseFont = true;
            this.BtnCerrar.ImageOptions.Image = global::Contable.Properties.Resources.Cierrepos;
            this.BtnCerrar.Location = new System.Drawing.Point(174, 59);
            this.BtnCerrar.Name = "BtnCerrar";
            this.BtnCerrar.Size = new System.Drawing.Size(151, 40);
            this.BtnCerrar.TabIndex = 4;
            this.BtnCerrar.Text = "Cierre \r\ntotal [F6]";
            this.BtnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.Appearance.Options.UseFont = true;
            this.BtnCancelar.ImageOptions.Image = global::Contable.Properties.Resources.Cancelarpos;
            this.BtnCancelar.Location = new System.Drawing.Point(12, 14);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(151, 40);
            this.BtnCancelar.TabIndex = 4;
            this.BtnCancelar.Text = "Cancelar [F1]";
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnvistapreviaImpresion
            // 
            this.btnvistapreviaImpresion.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvistapreviaImpresion.Appearance.Options.UseFont = true;
            this.btnvistapreviaImpresion.ImageOptions.Image = global::Contable.Properties.Resources.vistaPreviaImpresion;
            this.btnvistapreviaImpresion.Location = new System.Drawing.Point(174, 105);
            this.btnvistapreviaImpresion.Name = "btnvistapreviaImpresion";
            this.btnvistapreviaImpresion.Size = new System.Drawing.Size(151, 40);
            this.btnvistapreviaImpresion.TabIndex = 4;
            this.btnvistapreviaImpresion.Text = "Impresion \r\nVista Previa";
            this.btnvistapreviaImpresion.Click += new System.EventHandler(this.btnvistapreviaImpresion_Click);
            // 
            // btnmovcaja
            // 
            this.btnmovcaja.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmovcaja.Appearance.Options.UseFont = true;
            this.btnmovcaja.ImageOptions.Image = global::Contable.Properties.Resources.MovCajapos;
            this.btnmovcaja.Location = new System.Drawing.Point(10, 105);
            this.btnmovcaja.Name = "btnmovcaja";
            this.btnmovcaja.Size = new System.Drawing.Size(151, 40);
            this.btnmovcaja.TabIndex = 4;
            this.btnmovcaja.Text = "Mov.Caja [F2]";
            this.btnmovcaja.Click += new System.EventHandler(this.btnmovcaja_Click);
            // 
            // gbdatoscliente
            // 
            this.gbdatoscliente.Controls.Add(this.btnmodificarcliente);
            this.gbdatoscliente.Controls.Add(this.txtdireccion);
            this.gbdatoscliente.Controls.Add(this.txtclientedesc);
            this.gbdatoscliente.Controls.Add(this.label3);
            this.gbdatoscliente.Controls.Add(this.txtrucdni);
            this.gbdatoscliente.Controls.Add(this.label4);
            this.gbdatoscliente.Controls.Add(this.label5);
            this.gbdatoscliente.Location = new System.Drawing.Point(6, 228);
            this.gbdatoscliente.Name = "gbdatoscliente";
            this.gbdatoscliente.Size = new System.Drawing.Size(347, 169);
            this.gbdatoscliente.TabIndex = 20;
            this.gbdatoscliente.TabStop = false;
            this.gbdatoscliente.Text = "Cliente";
            this.gbdatoscliente.Enter += new System.EventHandler(this.gbdatoscliente_Enter);
            // 
            // btnmodificarcliente
            // 
            this.btnmodificarcliente.Location = new System.Drawing.Point(11, 143);
            this.btnmodificarcliente.Name = "btnmodificarcliente";
            this.btnmodificarcliente.Size = new System.Drawing.Size(91, 23);
            this.btnmodificarcliente.TabIndex = 20;
            this.btnmodificarcliente.Text = "Modificar cliente";
            this.btnmodificarcliente.Click += new System.EventHandler(this.btnmodificarcliente_Click);
            // 
            // txtdireccion
            // 
            this.txtdireccion.EditValue = "";
            this.txtdireccion.Enabled = false;
            this.txtdireccion.Location = new System.Drawing.Point(10, 120);
            this.txtdireccion.MenuManager = this.barManager1;
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdireccion.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtdireccion.Properties.Appearance.Options.UseFont = true;
            this.txtdireccion.Properties.Appearance.Options.UseForeColor = true;
            this.txtdireccion.Size = new System.Drawing.Size(327, 22);
            this.txtdireccion.TabIndex = 19;
            // 
            // txtclientedesc
            // 
            this.txtclientedesc.EditValue = "";
            this.txtclientedesc.Enabled = false;
            this.txtclientedesc.Location = new System.Drawing.Point(10, 76);
            this.txtclientedesc.MenuManager = this.barManager1;
            this.txtclientedesc.Name = "txtclientedesc";
            this.txtclientedesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclientedesc.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtclientedesc.Properties.Appearance.Options.UseFont = true;
            this.txtclientedesc.Properties.Appearance.Options.UseForeColor = true;
            this.txtclientedesc.Size = new System.Drawing.Size(328, 22);
            this.txtclientedesc.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 16);
            this.label3.TabIndex = 16;
            this.label3.Text = "R.U.C. / D.N.I.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtrucdni
            // 
            this.txtrucdni.EditValue = "";
            this.txtrucdni.EnterMoveNextControl = true;
            this.txtrucdni.Location = new System.Drawing.Point(10, 32);
            this.txtrucdni.MenuManager = this.barManager1;
            this.txtrucdni.Name = "txtrucdni";
            this.txtrucdni.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrucdni.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtrucdni.Properties.Appearance.Options.UseFont = true;
            this.txtrucdni.Properties.Appearance.Options.UseForeColor = true;
            this.txtrucdni.Size = new System.Drawing.Size(327, 22);
            this.txtrucdni.TabIndex = 17;
            this.txtrucdni.TextChanged += new System.EventHandler(this.txtrucdni_TextChanged);
            this.txtrucdni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrucdni_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Nombres ó Razón Social";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Dirección";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxAgregarDetalle
            // 
            this.gbxAgregarDetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxAgregarDetalle.Controls.Add(this.dgvdatos);
            this.gbxAgregarDetalle.Controls.Add(this.BtnQuitar);
            this.gbxAgregarDetalle.Controls.Add(this.groupBox1);
            this.gbxAgregarDetalle.Controls.Add(this.BtnModificar);
            this.gbxAgregarDetalle.Location = new System.Drawing.Point(359, 39);
            this.gbxAgregarDetalle.Name = "gbxAgregarDetalle";
            this.gbxAgregarDetalle.Size = new System.Drawing.Size(832, 338);
            this.gbxAgregarDetalle.TabIndex = 27;
            this.gbxAgregarDetalle.TabStop = false;
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatos.Location = new System.Drawing.Point(9, 63);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(817, 268);
            this.dgvdatos.TabIndex = 5;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.dgvdatos.DoubleClick += new System.EventHandler(this.dgvdatos_DoubleClick);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.ColumnPanelRowHeight = 25;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn8,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 30;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Item";
            this.gridColumn1.FieldName = "Adm_Item";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 50;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Código";
            this.gridColumn2.FieldName = "Adm_Catalogo";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 80;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Descripción";
            this.gridColumn3.FieldName = "Adm_Catalogo_Desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 364;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.Caption = "Unm.";
            this.gridColumn4.FieldName = "Adm_Unm_Desc";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 127;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.Caption = "Lote";
            this.gridColumn8.FieldName = "Lot_Lote";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 91;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Cantidad";
            this.gridColumn5.DisplayFormat.FormatString = "n4";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "Adm_Cantidad";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 88;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Prec. Unit.";
            this.gridColumn6.DisplayFormat.FormatString = "n3";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "Adm_Valor_Unit";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 91;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.Caption = "Importe";
            this.gridColumn7.DisplayFormat.FormatString = "n2";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "Adm_Total";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowMove = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            this.gridColumn7.Width = 106;
            // 
            // BtnQuitar
            // 
            this.BtnQuitar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnQuitar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQuitar.Appearance.Options.UseFont = true;
            this.BtnQuitar.ImageOptions.Image = global::Contable.Properties.Resources.Quitarpos;
            this.BtnQuitar.Location = new System.Drawing.Point(664, 23);
            this.BtnQuitar.Name = "BtnQuitar";
            this.BtnQuitar.Size = new System.Drawing.Size(151, 28);
            this.BtnQuitar.TabIndex = 4;
            this.BtnQuitar.Text = "Quitar [F4]";
            this.BtnQuitar.Click += new System.EventHandler(this.BtnQuitar_Click);
            // 
            // BtnModificar
            // 
            this.BtnModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnModificar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnModificar.Appearance.Options.UseFont = true;
            this.BtnModificar.ImageOptions.Image = global::Contable.Properties.Resources.Modificarpos;
            this.BtnModificar.Location = new System.Drawing.Point(507, 23);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(151, 28);
            this.BtnModificar.TabIndex = 4;
            this.BtnModificar.Text = "Modificar [F3]";
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // PanelTotales
            // 
            this.PanelTotales.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelTotales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTotales.Controls.Add(this.TxtRsImporteTotal);
            this.PanelTotales.Controls.Add(this.label8);
            this.PanelTotales.Controls.Add(this.TxtRsIGVMonto);
            this.PanelTotales.Controls.Add(this.LblIGV);
            this.PanelTotales.Controls.Add(this.TxtRsOtrosTribCarg);
            this.PanelTotales.Controls.Add(this.TxtRsValExporta);
            this.PanelTotales.Controls.Add(this.label17);
            this.PanelTotales.Controls.Add(this.TxtRsISC);
            this.PanelTotales.Controls.Add(this.label15);
            this.PanelTotales.Controls.Add(this.TxtRsInAfecta);
            this.PanelTotales.Controls.Add(this.label13);
            this.PanelTotales.Controls.Add(this.TxtRsExonerada);
            this.PanelTotales.Controls.Add(this.label11);
            this.PanelTotales.Controls.Add(this.TxtRsBaseImponible);
            this.PanelTotales.Controls.Add(this.label9);
            this.PanelTotales.Controls.Add(this.label6);
            this.PanelTotales.Location = new System.Drawing.Point(3, 16);
            this.PanelTotales.Name = "PanelTotales";
            this.PanelTotales.Size = new System.Drawing.Size(824, 145);
            this.PanelTotales.TabIndex = 32;
            // 
            // TxtRsImporteTotal
            // 
            this.TxtRsImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsImporteTotal.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsImporteTotal.Location = new System.Drawing.Point(693, 90);
            this.TxtRsImporteTotal.Name = "TxtRsImporteTotal";
            this.TxtRsImporteTotal.Size = new System.Drawing.Size(150, 33);
            this.TxtRsImporteTotal.TabIndex = 17;
            this.TxtRsImporteTotal.Text = "0.00";
            this.TxtRsImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(628, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 19);
            this.label8.TabIndex = 16;
            this.label8.Text = "Total:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRsIGVMonto
            // 
            this.TxtRsIGVMonto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsIGVMonto.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsIGVMonto.Location = new System.Drawing.Point(693, 54);
            this.TxtRsIGVMonto.Name = "TxtRsIGVMonto";
            this.TxtRsIGVMonto.Size = new System.Drawing.Size(150, 35);
            this.TxtRsIGVMonto.TabIndex = 17;
            this.TxtRsIGVMonto.Text = "0.00";
            this.TxtRsIGVMonto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblIGV
            // 
            this.LblIGV.AutoSize = true;
            this.LblIGV.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblIGV.Location = new System.Drawing.Point(573, 61);
            this.LblIGV.Name = "LblIGV";
            this.LblIGV.Size = new System.Drawing.Size(113, 19);
            this.LblIGV.TabIndex = 16;
            this.LblIGV.Text = "I.G.V.(18%):";
            this.LblIGV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtRsOtrosTribCarg
            // 
            this.TxtRsOtrosTribCarg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsOtrosTribCarg.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsOtrosTribCarg.Location = new System.Drawing.Point(133, 62);
            this.TxtRsOtrosTribCarg.Name = "TxtRsOtrosTribCarg";
            this.TxtRsOtrosTribCarg.Size = new System.Drawing.Size(150, 33);
            this.TxtRsOtrosTribCarg.TabIndex = 17;
            this.TxtRsOtrosTribCarg.Text = "0.00";
            this.TxtRsOtrosTribCarg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRsValExporta
            // 
            this.TxtRsValExporta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsValExporta.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsValExporta.Location = new System.Drawing.Point(133, 20);
            this.TxtRsValExporta.Name = "TxtRsValExporta";
            this.TxtRsValExporta.Size = new System.Drawing.Size(150, 33);
            this.TxtRsValExporta.TabIndex = 17;
            this.TxtRsValExporta.Text = "0.00";
            this.TxtRsValExporta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(40, 69);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 19);
            this.label17.TabIndex = 16;
            this.label17.Text = "O. Cargos";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtRsISC
            // 
            this.TxtRsISC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsISC.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsISC.Location = new System.Drawing.Point(415, 90);
            this.TxtRsISC.Name = "TxtRsISC";
            this.TxtRsISC.Size = new System.Drawing.Size(150, 33);
            this.TxtRsISC.TabIndex = 17;
            this.TxtRsISC.Text = "0.00";
            this.TxtRsISC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 19);
            this.label15.TabIndex = 16;
            this.label15.Text = "Exportación";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtRsInAfecta
            // 
            this.TxtRsInAfecta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsInAfecta.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsInAfecta.Location = new System.Drawing.Point(415, 54);
            this.TxtRsInAfecta.Name = "TxtRsInAfecta";
            this.TxtRsInAfecta.Size = new System.Drawing.Size(150, 33);
            this.TxtRsInAfecta.TabIndex = 17;
            this.TxtRsInAfecta.Text = "0.00";
            this.TxtRsInAfecta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(310, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 19);
            this.label13.TabIndex = 16;
            this.label13.Text = "I.S.C.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtRsExonerada
            // 
            this.TxtRsExonerada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsExonerada.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsExonerada.Location = new System.Drawing.Point(415, 20);
            this.TxtRsExonerada.Name = "TxtRsExonerada";
            this.TxtRsExonerada.Size = new System.Drawing.Size(150, 33);
            this.TxtRsExonerada.TabIndex = 17;
            this.TxtRsExonerada.Text = "0.00";
            this.TxtRsExonerada.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(310, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 19);
            this.label11.TabIndex = 16;
            this.label11.Text = "Inafecta:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtRsBaseImponible
            // 
            this.TxtRsBaseImponible.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRsBaseImponible.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRsBaseImponible.Location = new System.Drawing.Point(692, 20);
            this.TxtRsBaseImponible.Name = "TxtRsBaseImponible";
            this.TxtRsBaseImponible.Size = new System.Drawing.Size(150, 33);
            this.TxtRsBaseImponible.TabIndex = 17;
            this.TxtRsBaseImponible.Text = "0.00";
            this.TxtRsBaseImponible.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(289, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "Exonerada:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(603, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 19);
            this.label6.TabIndex = 16;
            this.label6.Text = "Gravada:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnuevo
            // 
            this.btnuevo.Location = new System.Drawing.Point(983, 10);
            this.btnuevo.Name = "btnuevo";
            this.btnuevo.Size = new System.Drawing.Size(88, 20);
            this.btnuevo.TabIndex = 37;
            this.btnuevo.Text = "Nuevo";
            this.btnuevo.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.PanelTotales);
            this.groupBox3.Location = new System.Drawing.Point(361, 383);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(830, 164);
            this.groupBox3.TabIndex = 42;
            this.groupBox3.TabStop = false;
            // 
            // frm_pos_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 564);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnuevo);
            this.Controls.Add(this.gbdatoscliente);
            this.Controls.Add(this.gbxAgregarDetalle);
            this.Controls.Add(this.gbxBotones);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.Name = "frm_pos_edicion";
            this.Text = "Punto de venta";
            this.Load += new System.EventHandler(this.frm_pos_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PopControlConModVenta)).EndInit();
            this.PopControlConModVenta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSITipoDocumento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSINumeroDocum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSISerieDocum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            this.gbxBotones.ResumeLayout(false);
            this.gbdatoscliente.ResumeLayout(false);
            this.gbdatoscliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdireccion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtclientedesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrucdni.Properties)).EndInit();
            this.gbxAgregarDetalle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            this.PanelTotales.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem BSIFecha;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem BSIEstablecimiento;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem BSIVendedorNombre;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        public DevExpress.XtraBars.BarManager barManager1;
        public DevExpress.XtraBars.Bar BarraHerramienta;
        private DevExpress.XtraBars.BarStaticItem barStaticItem7;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem9;
        private DevExpress.XtraBars.BarStaticItem BSITipoCambio;
        private DevExpress.XtraBars.BarStaticItem barStaticItem10;
 
        private DevExpress.XtraBars.BarStaticItem barStaticItem11;
 
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btncobrarteclacorta;
        private DevExpress.XtraEditors.SimpleButton btncobrar;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.TextEdit txtpdvdesc;
        private DevExpress.XtraEditors.DropDownButton DDBtnModVenta;
        private DevExpress.XtraEditors.CheckButton CheckBtnTicketFac;
        private DevExpress.XtraEditors.CheckButton CheckBtnTicketBol;
        private DevExpress.XtraEditors.CheckButton CheckBtnNotSal;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private System.Windows.Forms.GroupBox gbxBotones;
        private DevExpress.XtraEditors.SimpleButton BtnCancelar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtdireccion;
        private DevExpress.XtraEditors.TextEdit txtclientedesc;
        private DevExpress.XtraEditors.TextEdit txtrucdni;
        private DevExpress.XtraEditors.SimpleButton BtnCerrar;
        private System.Windows.Forms.GroupBox gbxAgregarDetalle;
        private DevExpress.XtraEditors.SimpleButton BtnQuitar;
        private DevExpress.XtraEditors.SimpleButton BtnModificar;
        private DevExpress.XtraEditors.SimpleButton btnmovcaja;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private System.Windows.Forms.Panel PanelTotales;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label TxtRsBaseImponible;
        private System.Windows.Forms.Label TxtRsIGVMonto;
        private System.Windows.Forms.Label LblIGV;
        private System.Windows.Forms.Label TxtRsImporteTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gbdatoscliente;
        private DevExpress.XtraEditors.SimpleButton btnuevo;
        private DevExpress.XtraBars.BarButtonItem BSIPuntoVenta;
        private DevExpress.XtraBars.BarButtonItem btncancelarteclacorta;
        public DevExpress.XtraBars.PopupControlContainer PopControlConModVenta;
        private DevExpress.XtraBars.BarButtonItem btnmovcajateclacorta;
        private DevExpress.XtraBars.BarButtonItem btnmodificarteclacorta;
        private DevExpress.XtraBars.BarButtonItem btnquitarteclacorta;
        private DevExpress.XtraBars.BarButtonItem btncierretotalcajateclacorta;
        internal DevExpress.XtraEditors.SimpleButton BtnCorte;
        private DevExpress.XtraEditors.TextEdit BSINumeroDocum;
        private DevExpress.XtraEditors.TextEdit BSISerieDocum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit BSITipoDocumento;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label TxtRsValExporta;
        private System.Windows.Forms.Label TxtRsISC;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label TxtRsInAfecta;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label TxtRsExonerada;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label TxtRsOtrosTribCarg;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton btnvistapreviaImpresion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnmodificarcliente;
    }
}