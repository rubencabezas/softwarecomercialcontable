﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Pos
{
    public partial class frm_nota_credito : frm_fuente
    {
        public frm_nota_credito()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_nota_credito_edicion f = new frm_nota_credito_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";
                f.Id_Periodo = Actual_Conexion.PeriodoSelect;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                         Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        private void frm_nota_credito_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.ctb_pdv_codigo = null;
            Ent.Id_Movimiento = null;
            try
            {
                Lista = log.Listar_NC(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            using (frm_nota_credito_edicion f = new frm_nota_credito_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.ctb_pdv_codigo = Entidad.ctb_pdv_codigo;
                f.Id_Movimiento = Entidad.Id_Movimiento;


                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
        private void bandedGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & bandedGridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;

                    Entidad = Lista[bandedGridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Listar();
        }

        private void btnanular_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                if (Entidad.Estado == "0002")
                {
                    Accion.Advertencia("El comprobante ya fue anulado,verifique su estado.");
                    return;
                }

                string DBObject = "La venta a anular tiene la siguientes caracteristicas:";
                //DateTime pFecha;
                DateTime? pFecha = null;
                //Nullable<DateTime> pFecha = null;

                string pRazon = "";
                bool pInformarBaja = false;

                if (Accion.ShowAnulacionRazon(DBObject ))
                {


                    pFecha = Accion.pFecha;
                    pRazon = Accion.pRazon;
                    pInformarBaja = Accion.pInformarBaja;

                    List<DocumentoElectronico> DocList = new List<DocumentoElectronico>();
                    DocumentoElectronico docElec = new DocumentoElectronico();


                    docElec.Empresa = Entidad.Id_Empresa;
                    docElec.Anio = Entidad.Id_Anio;
                    docElec.Periodo = Entidad.Id_Periodo;
                    docElec.Tipo_Doc_Sistema = Entidad.Ctb_Tipo_Doc.Trim();
                    docElec.Emisor_Documento_Tipo = "6";
                    docElec.Emisor_Documento_Numero = Actual_Conexion.RucEmpresa.Trim();
                    docElec.TipoDocumento_Codigo = Entidad.Ctb_Tipo_Doc_Sunat.Trim();
                    docElec.TipoDocumento_Serie = Entidad.Ctb_Serie.Trim();
                    docElec.TipoDocumento_Numero = Entidad.Ctb_Numero.Trim();
                    docElec.RazonAnulacion = pRazon;


                    string ws_RptaEst = Facte.Consultar_Estado(docElec);

                    string[] datosRptaEst = ws_RptaEst.Split(Convert.ToChar("#"));

                    if (Verificar_ComElec_Antes_De_Anular(datosRptaEst))
                    {
                        string ws_Rpta = Facte.Dar_Baja(docElec);
                        string[] datosRpta = ws_Rpta.Split(Convert.ToChar("#"));
                        string DataVar;
                        DataVar = datosRpta[0];
             
                        if (datosRpta[0].ToString().Trim() != "0" & datosRpta[0].ToString().Trim() != "-")
                        {

                            //Anular_adm_Ventas
                            Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab
                            {
                                Id_Empresa = Entidad.Id_Empresa,
                                Id_Anio = Entidad.Id_Anio,
                                Id_Periodo = Entidad.Id_Periodo,
                                ctb_pdv_codigo = Entidad.ctb_pdv_codigo,
                                Id_Movimiento = Entidad.Id_Movimiento
                            };

                            Logica_Movimiento_Cab logi = new Logica_Movimiento_Cab();
                            logi.Anular_adm_Nota_Credito(Enti);

                            Accion.ExitoGuardar();
                            Estado = Estados.Ninguno;
                        }
                        else
                        {
                        //Tools.ShowWarnig();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public bool Verificar_ComElec_Antes_De_Anular(string[] datosRpta)
        {
            if (datosRpta[1].Trim().Contains("ACEPTADO") == false)
            {
                Accion.Advertencia("COMPROBANTE NO ACEPTADO POR SUNAT ,El comprobante aun tiene el estado " + datosRpta[1] + ", solo puede Anular comprobantes Aceptados");
                return false;
            }
            else if (datosRpta[5].Trim() == "1")
            {
                Accion.Advertencia("DOCUMENTO YA ANULADO ,El comprobante de venta ya tiene una comunicación de baja anterior, comuniquese con el area de TI (Sistemas)");
                return false;
            }
            else if (Convert.ToDouble(datosRpta[9].Trim()) > 3)
            {
                Accion.Advertencia("FECHA DE ANULACION NO VALIDA ,El comprobante de venta solo puede ser anulado 3 dias despues de ser aceptada por SUNAT.");
                return false;
            }

            return true;
        }



    }
}
