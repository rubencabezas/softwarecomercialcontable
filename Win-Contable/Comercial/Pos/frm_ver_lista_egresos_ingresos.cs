﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Pos
{
    public partial class frm_ver_lista_egresos_ingresos : frm_fuente
    {
        public frm_ver_lista_egresos_ingresos()
        {
            InitializeComponent();
        }

        public string Pdv_Codigo;
        public string Cja_Codigo;
        public int Cja_Aper_Cierre_Codigo;

        private void frm_ver_lista_egresos_ingresos_Load(object sender, EventArgs e)
        {
                Listar();
        }

        public List<Entidad_Movimiento_Caja> Lista = new List<Entidad_Movimiento_Caja>();
        public void Listar()
        {
            Entidad_Movimiento_Caja Ent = new Entidad_Movimiento_Caja();
            Logica_Movimiento_Caja log = new Logica_Movimiento_Caja();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Pdv_Codigo = Pdv_Codigo;
            Ent.Cja_Codigo = Cja_Codigo;
            Ent.Cja_Aper_Cierre_Codigo = Cja_Aper_Cierre_Codigo;

            try
            {
                Lista = log.Listar_Por_Pdv(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
