﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_ingresar_cantidad :  frm_fuente
    {
        public frm_ingresar_cantidad()
        {
            InitializeComponent();
        }

        //    Public VentaManual As Boolean
        //Public MonCodigo As String
        //Public Fecha As String
        public Entidad_Movimiento_Cab entii = new Entidad_Movimiento_Cab();
        Entidad_Lista_Precio ItemEdi = new Entidad_Lista_Precio();
        List<Entidad_Lista_Precio> Lista_Precio = new List<Entidad_Lista_Precio>();

        public int VariableTag;
        public string AlmacenCod;

        public decimal StockTotal;
        public decimal Resultado;
        public string Est_Codigo;
        public DateTime Fecha;
        decimal Igv_Porcentaje;

        private bool Acepta_lotes=false;
        bool Es_Precio_fijo = false;
        public string Establecimiento;
        public string tipo;

        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(Convert.ToString(Fecha)) == true))
                {
                    if ((DateTime.Parse(Convert.ToString(Fecha)).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(Convert.ToString(Fecha));

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            Igv_Porcentaje = Lista[0].Imd_Tasa;
        
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public void BuscarStock()
        {
            try
            {
                Entidad_Catalogo Producto = new Entidad_Catalogo();
                Logica_Catalogo Log = new Logica_Catalogo();
                List<Entidad_Catalogo> ListaFiltrada = new List<Entidad_Catalogo>();

                Producto.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Producto.Id_Anio = Actual_Conexion.AnioSelect;
                Producto.Id_Tipo = entii.Adm_Tipo_BSA;// "0031";//BIEN
                Producto.Id_Almacen = AlmacenCod;
                Producto.Id_Catalogo = entii.Adm_Catalogo;
                ListaFiltrada.Clear();

                ListaFiltrada = Log.Consultar_Stock(Producto);
       
                if (ListaFiltrada.Count == 1)
                {
                    StockTotal = ListaFiltrada[0].Stock;
                }
                else
                {
                    StockTotal = 0;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_ingresar_cantidad_Load(object sender, EventArgs e)
        {
            if (VariableTag == 1)
            {
                ActualizaIGV();
                LblTotalStock.Text = Convert.ToString(StockTotal);
                LblDescripcionProducto.Text = entii.Adm_Catalogo_Desc;
                TxtItPrecUnit.Text = Convert.ToDecimal(entii.Adm_Valor_Unit).ToString("0.00");
                TxtCantidad.Text = Convert.ToDecimal(entii.Adm_Cantidad).ToString("0.00");
                TxtMonto.Text = Convert.ToDecimal(entii.Adm_Total).ToString("0.00");
                lblunidadmedida.Text= entii.Adm_Unm_Desc;

                Acepta_lotes = entii.Acepta_lotes;


                //Buscamos El tipo de Operacion Por Producto para definir Si Esta afecto u Otro Regimen del IGV
                Entidad_Operaciones oper = new Entidad_Operaciones();
                Logica_Operaciones logoper = new Logica_Operaciones();
                List<Entidad_Operaciones> operlist = new List<Entidad_Operaciones>();
                oper.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                oper.Id_Tipo = entii.Adm_Tipo_BSA;// item.Adm_Tipo_BSA;
                oper.Id_Catalogo = entii.Adm_Catalogo;// item.Adm_Catalogo;
                operlist = logoper.Traer_Operacion_venta_Por_Producto(oper);


                Buscar_Precio_por_presentacion(operlist[0].AfecIGV_Tabla);

                txtprecfinal.Text = Convert.ToDecimal(entii.Adm_Valor_Unit).ToString("0.00");

                TxtCantidad.Select();
            }
        }


        void Buscar_Precio_por_presentacion(string DvtD_OperCodigo)
        {
            try
            {

                if (DvtD_OperCodigo == "0565")
                {
                    ActualizaIGV();
                }
                else
                {
                    Igv_Porcentaje = 0;
                }


                Logica_Lista_Precio log = new Logica_Lista_Precio();
                Entidad_Lista_Precio ent = new Entidad_Lista_Precio();


                ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                ent.Est_Codigo = Establecimiento;
                ent.Id_Tipo = entii.Adm_Tipo_BSA;//BIEN:"0031"
                ent.Id_Catalogo = entii.Adm_Catalogo;
                ent.Pre_Tipo = "0006";//CONTADO
                ent.Pre_Id_Unm = entii.Adm_Unm_Id;//ListaFiltrada_Por_Presentacion[gridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                ent.Pre_Moneda_cod = "001"; //SOLES
                ent.Pre_Fecha_Ini = Convert.ToDateTime(Fecha);
                Lista_Precio = log.Traer_Precio_Procuto_Presentacion_Por_Unm(ent);

                Es_Precio_fijo = false;

                if (Lista_Precio.Count > 0)
                {
                    Es_Precio_fijo = Lista_Precio[0].Es_Precio_Variable;

                    if (Es_Precio_fijo == true)
                    {


                        //===============================
                        btncodAutoriza.Enabled = true;

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigvMin = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4) * Math.Round(Igv_Porcentaje, 2);

                        txtprecminimo.Text = Convert.ToString(Math.Round(precigvMin + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2), 2));

                        decimal precigvMax = Math.Round(Convert.ToDecimal(txtprecigv.Text), 4);

                        decimal IgvPrecMaximo = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);
                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));

                        txtprecfinal.Text = Convert.ToString(Math.Round(precigvMax + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2), 2));

                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecfinal.Enabled = true;



                        Calcula_Importe_Final();

                        txtprecfinal.Enabled = false;
                        txtprecfinal.Focus();
                    }
                    else
                    {
                        btncodAutoriza.Enabled = false;

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4,MidpointRounding.AwayFromZero) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);

                        decimal PrecMin = Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2, MidpointRounding.AwayFromZero);

                        txtprecminimo.Text = Convert.ToString(precigv + PrecMin);
                        txtprecmaximo.Text = Convert.ToString(precigv + PrecMin);
                        txtprecfinal.Text = Convert.ToString(precigv + PrecMin);

                        txtprecunitario.Text = txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecfinal.Enabled = false;

                        Calcula_Importe_Final();

                    }


                }
                else
                {
                    Accion.Advertencia("Debe configurar un precio para este producto.");
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void Calcula_Importe_Final()
        {
            try
            {
                if (TxtCantidad.Text != "")
                {
                    //  decimal Precio_Final = Convert.ToDecimal(txtprecfinal.Text) + Convert.ToDecimal(txtprecigv.Text);
                    decimal Precio_Final = Convert.ToDecimal(txtprecfinal.Text);
                    TxtMonto.Text = Convert.ToString(Math.Round(Convert.ToDecimal(TxtCantidad.Text) * Precio_Final, 2));


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            try {
                if (TxtCantidad.Text ==Convert.ToString(0.0))
                {
                    Accion.Advertencia("DEBE INGRESAR UNA CANTIDAD");
                    TxtCantidad.Focus();
                }
                else
                {
                    if (StockTotal < Convert.ToDecimal(TxtCantidad.Text))
                    {
                        Accion.Advertencia("NO HAY STOCK PARA ESTE PRODUCTO");
                        TxtCantidad.Focus();
                    }
                    else
                    {
                        Resultado = Convert.ToDecimal(TxtMonto.Text) / Convert.ToDecimal(TxtItPrecUnit.Text);

                        DialogResult = DialogResult.OK;
                    }
                }
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        bool ObtenerValorVenta(string Product_Cod)
        {
            try
            {
                if (string.IsNullOrEmpty(Product_Cod))//Product.Id_Catalogo
                {
                    Accion.Advertencia("DEBE INGRESAR UN PRODUCTO");
                    return false;
                }

                Logica_Lista_Precio log = new Logica_Lista_Precio();
                Entidad_Lista_Precio ent = new Entidad_Lista_Precio();


                ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                ent.Est_Codigo = Est_Codigo;
                ent.Id_Tipo = entii.Adm_Tipo_BSA; //"0031";//BIEN
                ent.Id_Catalogo = Product_Cod;//Product.Id_Catalogo;
                ent.Pre_Tipo = "0006";//CONTADO
                ent.Pre_Moneda_cod = "001"; //SOLES
                ent.Pre_Fecha_Ini =Fecha;

                Lista_Precio = log.Traer_Precio_Procuto(ent);

                //ItemEdi.PrecioPrincipal = log.Traer_Precio_Procuto(ent);

                //if (ItemEdi.PrecioPrincipal[0].Id_Catalogo == null)
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }


        void IngresarCantidadOtrosProductos(string Product_Cod, decimal Cantidad)
        {
            try
            {
                if (ObtenerValorVenta(Product_Cod))
                {
                    ItemEdi.PrecioPrincipal = ItemEdi.PrecioPrincipal;

                    Entidad_Venta_Cab item = new Entidad_Venta_Cab();

 
                    //decimal IgvPrecioValor = Convert.ToDecimal(ItemEdi.PrecioPrincipal[0].Pre_Precio_Valor) * Convert.ToDecimal(Igv_Porcentaje);
                    //item.Adm_Valor_Unit = Convert.ToDecimal(ItemEdi.PrecioPrincipal[0].Pre_Precio_Valor) + IgvPrecioValor;


                    foreach (Entidad_Lista_Precio Prec in Lista_Precio)
                    {
                        //if ((Convert.ToDecimal(TxtCantidad.Text) >= Prec.Pre_Cantidad_Mayoreo) & Prec.Precio_Principal != 0)
                        //{

                        decimal IgvPrecioValor = Convert.ToDecimal(Prec.Pre_Precio_Minimo) * Convert.ToDecimal(Igv_Porcentaje);

                        item.Adm_Valor_Venta = Convert.ToDecimal(Prec.Pre_Precio_Minimo);
                        item.Adm_Valor_Unit = Convert.ToDecimal(Prec.Pre_Precio_Minimo) + IgvPrecioValor;

                        TxtItPrecUnit.Text = Convert.ToDecimal(item.Adm_Valor_Unit).ToString("0.000");
                        TxtMonto.Text = Convert.ToString(Math.Round((Convert.ToDecimal(TxtCantidad.Text) * Convert.ToDecimal(TxtItPrecUnit.Text)), 3).ToString("0.00"));

                        //}
                    }

                }
                else
                {
                    Accion.Advertencia("ESTE PRODUCTO NO TIENE PRECIO CONFIGURADO");
                }
           }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtCantidad_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                //if (BtnCancelar.Tag != null)
                //{
                    if ((TxtCantidad.Text) == "0.00")
                    {
                        TxtMonto.Text = "0.00";
                    }
                    else
                    {
                        if (StockTotal < (Convert.ToDecimal(TxtCantidad.Text)/ entii.Unm_Base))
                        {
                            Accion.Advertencia("NO HAY STOCK PARA ESTE PRODUCTO");
                            TxtCantidad.Focus();
                        }
                        else
                        {
                            Calcula_Importe_Final();
                        }
                    }
                //}

            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtMonto_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (TxtMonto.Text == "")
                {
                    TxtMonto.Text = "0.00";
                    TxtCantidad.Text = "0.0";
                }
            }catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtCantidad_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(TxtCantidad.Text))
                {
                    TxtMonto.Text = "0.00";
                    TxtCantidad.Text = "0.00";

                }
                else if ((!string.IsNullOrWhiteSpace(TxtCantidad.Text)  || (TxtCantidad.Text != "0.00")))
                {
                    //TxtMonto.Text =Convert.ToString(Math.Round((Convert.ToDecimal(TxtCantidad.Text) * Convert.ToDecimal(TxtItPrecUnit.Text)), 3).ToString("0.00"));


                    //PRIMERO DEBEMOS OBTENER EL TOTAL DE LA CANTIDAD DEL PRODUCTO 
                    //PARA SACAR EL PRECIO ...SE SACARA EL PRECIO SEGUN CANTIDAD
                    //SACAREMOS EL PRECIO SEGUN LA CANTIDAD DE MAYOREO

                    IngresarCantidadOtrosProductos(entii.Adm_Catalogo, Convert.ToDecimal(TxtCantidad.Text));



                }
                else
                {
                    TxtMonto.Text = "0.00";
                    TxtCantidad.Text = "0.00";
                 }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void TxtCantidad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (((TxtCantidad.Text == "") && !TxtCantidad.Focused))
                {
                    TxtMonto.Text ="0.00";
                    TxtCantidad.Text = "0.00";
     
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtprecfinal_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (TxtCantidad.Text != "")
                {
                    if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
                    {
                        Accion.Advertencia("El precio final no debe ser menor al precio base");

                        //txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
                        //decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                        //txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                        //txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigvMin = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4) * Math.Round(Igv_Porcentaje, 2);
                        txtprecminimo.Text = Convert.ToString(Math.Round(precigvMin + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2), 2));
                        decimal precigvMax = Math.Round(Convert.ToDecimal(txtprecigv.Text), 4);
                        decimal IgvPrecMaximo = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);
                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));
                        txtprecfinal.Text = Convert.ToString(Math.Round(precigvMax + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2), 2));
                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        Calcula_Importe_Final();

                        txtprecfinal.Focus();
                        txtprecfinal.Select();
                        return;
                    }
                    else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
                    {
                        Accion.Advertencia("El precio final no debe ser mayor al valor máximo");

                        //txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
                        //decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                        //txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                        //txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

                        txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2));
                        decimal precigvMin = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 4) * Math.Round(Igv_Porcentaje, 2);
                        txtprecminimo.Text = Convert.ToString(Math.Round(precigvMin + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2), 2));
                        decimal precigvMax = Math.Round(Convert.ToDecimal(txtprecigv.Text), 4);
                        decimal IgvPrecMaximo = Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Maximo), 4) * Math.Round(Igv_Porcentaje, 2);
                        decimal precigv2 = Math.Round(IgvPrecMaximo, 2, MidpointRounding.AwayFromZero);
                        txtprecmaximo.Text = Convert.ToString(precigv2 + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2));
                        txtprecfinal.Text = Convert.ToString(Math.Round(precigvMax + Math.Round(Lista_Precio[0].Pre_Precio_Maximo, 2), 2));
                        txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));


                        Calcula_Importe_Final();

                        txtprecfinal.Focus();
                        txtprecfinal.Select();
                        return;
                    }
                    else
                    {
                        if (Es_Precio_fijo == true)
                        {

                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));


                            Calcula_Importe_Final();

                            TxtCantidad.Focus();
                        }
                        else
                        {
                            txtprecminimo.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
                            txtprecmaximo.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));

                            txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
                            decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
                            txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo + precigv, 2));
                            txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));



                            txtprecfinal.Enabled = true;
                            TxtMonto.Text = Convert.ToString(Convert.ToDecimal(TxtCantidad.Text) * Convert.ToDecimal(txtprecfinal.Text));
                            TxtCantidad.Focus();
                            TxtCantidad.Select();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);

            }
        }

        //private void txtprecfinal_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (TxtCantidad.Text != "")
        //        {
        //            if (Convert.ToDecimal(txtprecminimo.Text) > Convert.ToDecimal(txtprecfinal.Text))
        //            {
        //                Accion.Advertencia("El precio final no debe ser menor al precio base");

        //                txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
        //                decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
        //                txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
        //                txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

        //                Calcula_Importe_Final();

        //                txtprecfinal.Focus();
        //                txtprecfinal.Select();
        //                return;
        //            }
        //            else if (Convert.ToDecimal(txtprecmaximo.Text) < Convert.ToDecimal(txtprecfinal.Text))
        //            {
        //                Accion.Advertencia("El precio final no debe ser mayor al valor máximo");

        //                txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
        //                decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
        //                txtprecfinal.Text = Convert.ToString(precigv + Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
        //                txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));

        //                Calcula_Importe_Final();

        //                txtprecfinal.Focus();
        //                txtprecfinal.Select();
        //                return;
        //            }
        //            else
        //            {
        //                if (Es_Precio_fijo == true)
        //                {

        //                    txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));


        //                    Calcula_Importe_Final();

        //                    TxtCantidad.Focus();
        //                }
        //                else
        //                {
        //                    //txtprecminimo.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));
        //                    //txtprecmaximo.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo, 2));

        //                    txtprecigv.Text = Convert.ToString(Math.Round(Convert.ToDecimal(Lista_Precio[0].Pre_Precio_Minimo), 2) * Math.Round(Igv_Porcentaje, 2));
        //                    decimal precigv = Math.Round(Convert.ToDecimal(txtprecigv.Text), 2, MidpointRounding.AwayFromZero);
        //                    txtprecfinal.Text = Convert.ToString(Math.Round(Lista_Precio[0].Pre_Precio_Minimo + precigv, 2));
        //                    txtprecunitario.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtprecfinal.Text), 2));



        //                    txtprecfinal.Enabled = true;
        //                    TxtMonto.Text = Convert.ToString(Convert.ToDecimal(TxtCantidad.Text) * Convert.ToDecimal(txtprecfinal.Text));
        //                    TxtCantidad.Focus();
        //                    TxtCantidad.Select();
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Accion.ErrorSistema(ex.Message);

        //    }
        //}

        private void btncodAutoriza_Click(object sender, EventArgs e)
        {
            using (frm_valida_cod_autorizacion f = new frm_valida_cod_autorizacion())
            {

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
                    Logica_Lista_Precio LogUsu = new Logica_Lista_Precio();
                    Entidad_Lista_Precio ent = new Entidad_Lista_Precio();



                    ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    ent.Est_Codigo = Establecimiento;
                    ent.Id_Tipo = entii.Adm_Tipo_BSA; //"0031";//BIEN
                    ent.Id_Catalogo = entii.Adm_Catalogo;
                    ent.Pre_Tipo = "0006";//CONTADO
                    ent.Pre_Id_Unm = entii.Adm_Unm_Id;// ListaFiltrada_Por_Presentacion[bandedGridView1.GetFocusedDataSourceRowIndex()].Unm_Cod_Det;
                    ent.Pre_Moneda_cod = "001"; //SOLES
                    ent.Pre_Fecha_Ini = Convert.ToDateTime(Fecha);
                    ent.Pre_Codigo_Autoriza = Accion.Encriptar(f.txtcodautoriza.Text.Trim());

                    Ent = LogUsu.Valida_Codigo_Autorizacion(ent);
                    if (Ent.Respuesta == "1")
                    {
                        txtprecfinal.Enabled = true;
                        txtprecfinal.Focus();
                    }
                    else
                    {
                        Accion.Advertencia("El código de autorización es incorrecto");
                        txtprecfinal.Enabled = false;
                    }
                }



            }
        }
    }
}
