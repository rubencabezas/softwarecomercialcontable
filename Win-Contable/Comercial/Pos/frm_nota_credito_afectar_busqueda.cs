﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Pos
{
    public partial class frm_nota_credito_afectar_busqueda : Form
    {
        public frm_nota_credito_afectar_busqueda()
        {
            InitializeComponent();
        }

        private void frm_nota_credito_afectar_busqueda_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public string ctb_pdv_codigo;
        public string Ctb_Ruc_dni;

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.ctb_pdv_codigo = ctb_pdv_codigo;
            Ent.Ctb_Ruc_dni = Ctb_Ruc_dni;
            try
            {
                Lista = log.Listar_Afectar_comprobante_NC_(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnseleccionar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bandedGridView1.RowCount > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void dgvdatos_DoubleClick(object sender, EventArgs e)
        {
            btnseleccionar.PerformClick();
        }

        private void dgvdatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnseleccionar.PerformClick();
                e.SuppressKeyPress = true;
            }
        }
    }
}
