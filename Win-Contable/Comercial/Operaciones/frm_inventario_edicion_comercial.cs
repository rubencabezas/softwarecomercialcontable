﻿using Comercial.Operaciones;
using Contable;
using Contable._1_Busquedas_Generales;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_inventario_edicion_comercial : frm_fuente
    {
        public frm_inventario_edicion_comercial()
        {
            InitializeComponent();
        }


        public string Estado_Ven_Boton;
        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Tipo_Mov, Id_Almacen, Id_Movimiento;

        List<Entidad_Movimiento_Inventario> Detalles = new List<Entidad_Movimiento_Inventario>();

        public bool Se_Genero_En_Compras = false;
        public bool Es_traspaso = false;
        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Inventario(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    txtlibro.Tag = Generales[0].Ini_Inventario;
                    txtlibro.Text = Generales[0].Ini_Inventario_Desc;
                }
                else
                {
                    Accion.Advertencia("No existe el libro configurado");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BloquearDetalles()
        {
            
            txtimporte.Enabled = false;
            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtvalorunit.Enabled = false;
            txtcantidad.Enabled = false;
            txtunmcod.Enabled = false;
           
        }


        public void HabilitarDetalles()
        {
      
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtimporte.Enabled = true;
            txtcantidad.Enabled = true;
            txtvalorunit.Enabled = true;
            txtunmcod.Enabled = true;
            txttipobsacod.Focus();
        
          
        }

        public void LimpiarCab()
        {
            txtlibro.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();
           
            txtfechadoc.ResetText();
            //txtrucdni.ResetText();
            //txtentidad.ResetText();
            txtmotivocod.ResetText();
            txtmotivodesc.ResetText();
            txttipoopercod.ResetText();
            txttipooperdes.ResetText();
            //txttipoentcod.ResetText();
            //txttipoentdesc.ResetText();
            txtalmacencod.ResetText();
            txtalmacendesc.ResetText();

            txtresponsable.ResetText();
            txtresponsabledesc.ResetText();

            FNecesitaCC = false;
            FNecesitaCG = false;
            FGeneraAsiento = false;
            FIsValorizado = false;
            FNecesitaDocumento = false;
            IsSaldoInicial = false;
            FTipoDocDefecto = "";
            FEstransferenciaAlma = false;

        }

        public void LimpiarDet()
        {
           
            //txttipoopedesc.ResetText();

            txtimporte.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtvalorunit.ResetText();
            txtcantidad.ResetText();
            txtunmcod.ResetText();
            txtunmdesc.Reset();

            chklote.Checked = false;

            //Detalles.Clear();

        }
        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();
                LimpiarDet();
                Detalles.Clear();
                dgvdatos.DataSource = null;
                BloquearDetalles();
                TraerLibro();
                txtglosa.Focus();
            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }
        int Id_Item;

        public override void CambiandoEstadoDetalle()
        {
            if (EstadoDetalle == Estados.Nuevo)
            {
                HabilitarDetalles();
                LimpiarDet();
                Id_Item = Detalles.Count + 1;

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Ninguno)
            {
                LimpiarDet();
                BloquearDetalles();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Modificar)
            {

                HabilitarDetalles();


                btnnuevodet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = true;

            }
            else if (EstadoDetalle == Estados.Guardado)
            {

                LimpiarDet();
                BloquearDetalles();
                btnnuevodet.Focus();

                btnnuevodet.Enabled = true;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.Consulta)
            {
                HabilitarDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;

            }
            else if (EstadoDetalle == Estados.SoloLectura)
            {
                BloquearDetalles();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = false;
            }
        }

        private void frm_inventario_edicion_Load(object sender, EventArgs e)
        {
            TraerLibro();
            BloquearDetalles();
            BuscarMotivo_Salida();

            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;

                ListarModificar();

                if (txtmotivocod.Tag.ToString() == "0034")//entrada
                {
                    lblO_D.Text = "Almacen Origen";
                }
                else if (txtmotivocod.Tag.ToString() == "0035") //salida
                {
                    lblO_D.Text = "Almacen Destino";
                }
                else
                {
                    lblO_D.Text = "A. Origen/Destino";
                }

                if (Se_Genero_En_Compras == true)
                {
                    btnguardar.Enabled = false;
                }

                if (Es_traspaso == true)
                {
                    btnguardar.Enabled = false;
                }
            }
        }

        public List<Entidad_Movimiento_Inventario> Lista_Modificar = new List<Entidad_Movimiento_Inventario>();
        string Inv_Tipo_Entidad;
        public void ListarModificar()
        {
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
            Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

            //Estado = Estados.Ninguno;
  
            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Tipo_Mov = Id_Tipo_Mov;
            Ent.Id_Almacen = Id_Almacen;
            Ent.Id_Movimiento = Id_Movimiento;
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Inventario Enti = new Entidad_Movimiento_Inventario();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Tipo_Mov = Enti.Id_Tipo_Mov;
                    Id_Almacen = Enti.Id_Almacen;
                    Id_Movimiento = Enti.Id_Movimiento;

                    Inv_Tipo_Entidad = Enti.Inv_Tipo_Entidad;
                    //txttipoentdesc.Text = Enti.Inv_Tipo_Entidad_Desc;

                    txtmotivocod.Tag = Enti.Id_Tipo_Mov;
                    txtmotivocod.Text = Enti.Id_Tipo_Mov_Det;
                    txtmotivodesc.Text = Enti.Id_Tipo_Mov_Desc;

                    txttipoopercod.Text = Enti.Id_Tipo_Operacion;
                    txttipooperdes.Text = Enti.Id_Tipo_Operacion_Desc;

                    txtalmacencod.Text = Enti.Id_Almacen;
                    txtalmacendesc.Text = Enti.Id_Almacen_Desc;

                    //txtrucdni.Text = Enti.Inv_Ruc_Dni;
                    //txtentidad.Text = Enti.Inv_Ruc_Dni_Desc;

                    txttipodoc.Text = Enti.Inv_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Inv_Tipo_Doc_Desc;

                    txtserie.Text = Enti.Inv_Serie +"-"+ Enti.Inv_Numero;

                   //txtnumero.Text = Enti.Inv_Numero ;


                   txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Inv_Fecha);

                   txtglosa.Text  =Enti.Glosa  ;

                    txtalmacentrancod.Text=  Enti.Inv_Almacen_O_D ;
                    txtresponsable.Text =Enti.Inv_Responsable_Ruc_Dni ;
                    txtresponsabledesc.Text = Enti.Inv_Responsable_Ruc_Dni_Desc;
                  


                    Logica_Movimiento_Inventario log_det = new Logica_Movimiento_Inventario();
                    dgvdatos.DataSource = null;
                    Detalles = log_det.Listar_Det(Enti);

                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                    }

                    Logica_Movimiento_Inventario log_Doc_Ref = new Logica_Movimiento_Inventario();

                    Documentos_Provisionados = log_det.Buscar_Doc_Referenciados(Enti);


                    ListaLotesFinal = log_det.Listar_Lotes_Inventario(Enti);

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void txtmotivocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmotivocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmotivocod.Text.Substring(txtmotivocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0012";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtmotivocod.Tag = Entidad.Id_General_Det;
                                txtmotivocod.Text = Entidad.Gen_Codigo_Interno;
                                txtmotivodesc.Text = Entidad.Gen_Descripcion_Det;

                                if (txtmotivocod.Tag.ToString() == "0034")//entrada
                                {
                                    lblO_D.Text = "Almacen Origen";
                                }else if (txtmotivocod.Tag.ToString() == "0035") //salida
                                {
                                    lblO_D.Text = "Almacen Destino";
                                }
                                else
                                {
                                    lblO_D.Text = "A. Origen/Destino";
                                }

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmotivocod.Text) & string.IsNullOrEmpty(txtmotivodesc.Text))
                    {
                        BuscarMotivo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarMotivo()
        {
            try
            {
                txtmotivocod.Text = Accion.Formato(txtmotivocod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0012",
                    Gen_Codigo_Interno = txtmotivocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txtmotivocod.Text.Trim().ToUpper())
                        {
                            txtmotivocod.Tag = T.Id_General_Det;
                            txtmotivocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtmotivodesc.Text = T.Gen_Descripcion_Det;


                            if (txtmotivocod.Tag.ToString() == "0034")//entrada
                            {
                                lblO_D.Text = "Almacen Origen";
                            }
                            else if (txtmotivocod.Tag.ToString() == "0035") //salida
                            {
                                lblO_D.Text = "Almacen Destino";
                            }
                            else
                            {
                                lblO_D.Text = "A. Origen/Destino";
                            }

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void BuscarMotivo_Salida()
        {
            try
            {
               
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0012",
                    Gen_Codigo_Interno = "01"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == "01")
                        {
                            txtmotivocod.Tag = T.Id_General_Det;
                            txtmotivocod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtmotivodesc.Text = T.Gen_Descripcion_Det;


                            if (txtmotivocod.Tag.ToString() == "0034")//entrada
                            {
                                lblO_D.Text = "Almacen Origen";
                            }
                            else if (txtmotivocod.Tag.ToString() == "0035") //salida
                            {
                                lblO_D.Text = "Almacen Destino";
                            }
                            else
                            {
                                lblO_D.Text = "A. Origen/Destino";
                            }

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void txtmotivocod_TextChanged(object sender, EventArgs e)
        {
            if (txtmotivocod.Focus() == false)
            {
                txtmotivodesc.ResetText();
            }
        }

        //private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (String.IsNullOrEmpty(txtrucdni.Text) == false)
        //    {
        //        try
        //        {
        //            if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
        //            {
        //                using (frm_entidades_busqueda f = new frm_entidades_busqueda())
        //                {


        //                    if (f.ShowDialog(this) == DialogResult.OK)
        //                    {
        //                        Entidad_Entidad Entidad = new Entidad_Entidad();

        //                        Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

        //                        txtrucdni.Text = Entidad.Ent_RUC_DNI;
        //                        txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
        //                    }
        //                }
        //            }
        //            else
        //                if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
        //            {
        //                BuscarEntidad();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Accion.ErrorSistema(ex.Message);
        //        }
        //    }
        //}


        //public void BuscarEntidad()
        //{
        //    try
        //    {
        //        //txtrucdni.Text = Accion.Formato(txt.Text, 3);
        //        Logica_Entidad log = new Logica_Entidad();

        //        List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

        //        Generales = log.Listar(new Entidad_Entidad
        //        {
        //            Ent_RUC_DNI = txtrucdni.Text
        //        });

        //        if (Generales.Count > 0)
        //        {

        //            foreach (Entidad_Entidad T in Generales)
        //            {
        //                if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
        //                {
        //                    txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
        //                    txtentidad.Text = T.Ent_Ape_Materno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;

        //                }
        //            }

        //        }
        //        else
        //        {
        //            Accion.Advertencia("No se encontro ningun dato");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Accion.ErrorSistema(ex.Message);
        //    }
        //}

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_tipo_documento_busqueda f = new frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 3);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_Comprobante = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_Comprobante).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtserie_Leave(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(txtserie.Text))
                {
                    string Serie_Numero = txtserie.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        //txtserie.Text = Accion.Formato(txtserie.Text, 4);
                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtserie.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtnumero_Leave(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtnumero.Text))
            //{
            //    txtnumero.Text = Accion.Formato(txtnumero.Text, 8);
            //}
        }

        //private void txtrucdni_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtrucdni.Focus() == false)
        //    {
        //        txtentidad.ResetText();
        //    }
        //}



        private void txtccgcod_KeyDown(object sender, KeyEventArgs e)
        {

            //if (String.IsNullOrEmpty(txtccgcod.Text) == false)
            //{
            //    try
            //    {
            //        if (e.KeyCode == Keys.Enter & txtccgcod.Text.Substring(txtccgcod.Text.Length - 1, 1) == "*")
            //        {
            //            using (frm_centro_costo_gasto_busqueda f = new frm_centro_costo_gasto_busqueda())
            //            {


            //                if (f.ShowDialog(this) == DialogResult.OK)
            //                {
            //                    Entidad_Centro_Costo_Gasto Entidad = new Entidad_Centro_Costo_Gasto();

            //                    Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

            //                    txtccgcod.Text = Entidad.Id_Centro_cg;
            //                    txtccgdesc.Text = Entidad.Ccg_Descripcion;
            //                }
            //            }
            //        }
            //        else
            //            if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtccgcod.Text) & string.IsNullOrEmpty(txtccgdesc.Text))
            //        {
            //            BuscarCentroCostogasto();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Accion.ErrorSistema(ex.Message);
            //    }
            //}
        }




        public void BuscarCentroCostogasto()
        {
            //try
            //{
            //    txtccgcod.Text = Accion.Formato(txtccgcod.Text, 3);

            //    Logica_Centro_Costo_Gasto log = new Logica_Centro_Costo_Gasto();

            //    List<Entidad_Centro_Costo_Gasto> Generales = new List<Entidad_Centro_Costo_Gasto>();

            //    Generales = log.Listar(new Entidad_Centro_Costo_Gasto
            //    {
            //        Id_Centro_cg = txtccgcod.Text
            //    });

            //    if (Generales.Count > 0)
            //    {

            //        foreach (Entidad_Centro_Costo_Gasto T in Generales)
            //        {
            //            if ((T.Id_Centro_cg).ToString().Trim().ToUpper() == txtccgcod.Text.Trim().ToUpper())
            //            {
            //                txtccgcod.Text = (T.Id_Centro_cg).ToString().Trim();
            //                txtccgdesc.Text = T.Ccg_Descripcion;

            //            }
            //        }

            //    }
            //    else
            //    {
            //        Accion.Advertencia("No se encontro ningun dato");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Accion.ErrorSistema(ex.Message);
            //}
        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_catalogo_busqueda f = new frm_catalogo_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;

                                chklote.Checked = Entidad.Acepta_Lote;

                                codMarca = Entidad.Id_Marca.Trim();
                                desMarca = Entidad.Mar_Descripcion.Trim();

                                BuscarUnm_Defecto_Producto();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                        BuscarUnm_Defecto_Producto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {

                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;

                            chklote.Checked = T.Acepta_Lote;

                            codMarca = T.Id_Marca.Trim();
                            desMarca = T.Mar_Descripcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }







        private void txtccgcod_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void txttipobsacod_TextChanged(object sender, EventArgs e)
        {
            if (txttipobsacod.Focus() == false)
            {
                txttipobsadesc.ResetText();
            }
        }

        private void txtproductocod_TextChanged(object sender, EventArgs e)
        {
            if (txtproductocod.Focus() == false)
            {
                txtproductodesc.ResetText();
                txtunmcod.ResetText();
                txtunmdesc.ResetText();
            }
        }



        private void txtalmacencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacencod.Text.Substring(txtalmacencod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacencod.Text = Entidad.Id_Almacen.Trim();
                                txtalmacendesc.Text = Entidad.Alm_Descrcipcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacencod.Text) & string.IsNullOrEmpty(txtalmacendesc.Text))
                    {
                        BuscarAlmacen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarAlmacen()
        {
            try
            {
                txtalmacencod.Text = Accion.Formato(txtalmacencod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen=txtalmacencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacencod.Text.Trim().ToUpper())
                        {
                            txtalmacencod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtalmacencod_TextChanged(object sender, EventArgs e)
        {
            if (txtalmacencod.Focus() == false)
            {
                txtalmacendesc.ResetText();
            }
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {
                    Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
                    Logica_Movimiento_Inventario Log = new Logica_Movimiento_Inventario();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Id_Periodo;
                    Ent.Id_Tipo_Mov = txtmotivocod.Tag.ToString();
                    Ent.Id_Almacen = txtalmacencod.Text;
                    Ent.Id_Movimiento = Id_Movimiento;
                    Ent.Id_Tipo_Operacion = txttipoopercod.Text;

                    //Ent.Inv_Tipo_Entidad = txttipoentcod.Text;
                    //Ent.Inv_Ruc_Dni = txtrucdni.Text;

                    Ent.Glosa = txtglosa.Text;
                    Ent.Inv_Tipo_Doc = txttipodoc.Text;

                    //Ent.Inv_Serie = txtserie.Text;
                    //Ent.Inv_Numero = txtnumero.Text;

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Inv_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Inv_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();

                    Ent.Inv_Fecha = Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Glosa = txtglosa.Text;

                    Ent.Inv_Almacen_O_D = txtalmacentrancod.Text;
                    Ent.Inv_Responsable_Ruc_Dni = txtresponsable.Text;

                    Ent.Inv_Libro = txtlibro.Tag.ToString();

                    Ent.Detalle = Detalles;
                    Ent.DetalleDoc_Referencia = Documentos_Provisionados;


                    Ent.DetalleLotes = ListaLotesFinal;
 



                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            LimpiarCab();
                            LimpiarDet();
                            Detalles.Clear();
                            ListaLotesFinal.Clear();
                            dgvdatos.DataSource = null;
                            BloquearDetalles();
                            TraerLibro();
                            txtmotivocod.Focus();
                        }
                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }


            if (Detalles.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

 


            //if (Detalles.Count > 0)
            //{

            //    for (int i = 0; (i <= (Detalles.Count - 1)); i++)//Detalles_NS
            //    {
            //        if (Detalles[i].Acepta_lotes == true)
            //        {
            //            if (ListaLotesFinal.Count>0 )
            //            {
            //                List<Entidad_Movimiento_Inventario> ListaLotes = new List<Entidad_Movimiento_Inventario>();

            //                var PrivView = from item in ListaLotesFinal
            //                               where item.Lot_Catalogo == Detalles[i].Invd_Catalogo && item.Lot_Adm_item == Detalles[i].Id_Item
            //                               orderby item.Id_Anio descending
            //                               select item;
            //                ListaLotes = PrivView.ToList();

            //                if (PrivView.Count() > 0)
            //                {
            //                     if (ListaLotes[0].Lot_Catalogo == Detalles[i].Invd_Catalogo && ListaLotes[0].Lot_Adm_item == Detalles[i].Id_Item)
            //                        {
            //                            if (Detalles[i].Invd_Cantidad != ListaLotes[0].Lot_Cantidad)
            //                            {
            //                                Accion.Advertencia("Las cantidades para el producto:" + Detalles[i].Invd_Catalogo_Desc + " en el registro de lotes deben ser iguales");
            //                                return false;
            //                            }
            //                        }
            //                }
            //                else
            //                    {
            //                        Accion.Advertencia("Uno de los productos acepta el registro con lotes,verificar y llenar su lote correspondiente.");
            //                        return false;
            //                    }
            //            }
            //            else
            //            {
            //                Accion.Advertencia("Uno de los productos acepta el registro con lotes,verificar y llenar su lote correspondiente.");
            //                return false;
            //            }
            //        }
            //    }
            //}

            return true;
        }

        private void txtalmacentrancod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacentrancod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacentrancod.Text.Substring(txtalmacentrancod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacentrancod.Text = Entidad.Id_Almacen.Trim();
                                txtalmacentrandesc.Text = Entidad.Alm_Descrcipcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacentrancod.Text) & string.IsNullOrEmpty(txtalmacentrandesc.Text))
                    {
                        BuscarAlmacenTran();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarAlmacenTran()
        {
            try
            {
                txtalmacentrancod.Text = Accion.Formato(txtalmacentrancod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen = txtalmacentrancod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacentrancod.Text.Trim().ToUpper())
                        {
                            txtalmacentrancod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacentrandesc.Text = T.Alm_Descrcipcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipoopercod_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipoopercod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipoopercod.Text.Substring(txttipoopercod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_tipo_operacion_busqueda f = new frm_tipo_operacion_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Tipo_Operacion Entidad = new Entidad_Tipo_Operacion();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipoopercod.Text = Entidad.Id_Operacion;
                                txttipooperdes.Text = Entidad.Top_Descripcion;
                                FNecesitaCC = Entidad.Top_NecesitaCC;
                                FNecesitaCG = Entidad.Top_NecesitaCG;
                                FGeneraAsiento = Entidad.Top_Genera_Asiento;
                                FIsValorizado = Entidad.Top_Es_Valorizado;
                                FNecesitaDocumento = Entidad.Top_Necesita_Doc;
                                IsSaldoInicial = Entidad.Top_Es_Saldo_Inicial;
                                FTipoDocDefecto = Entidad.Top_Tipo_Doc_Defecto;
                                FEstransferenciaAlma = Entidad.Top_Requiere_Transferecia_Almacen;
                                FRequiereEntidad = Entidad.Top_Requiere_Tipo_Entidad;
                                FTipoEntidad = Entidad.Top_Tipo_Entidad_Cod;
                                Verificar_Tipo_Operacion();

                                if (txttipoopercod.Text == "14") // Saldo Inicial
                                {
                                    btnnuevodet.Enabled = false;
                                    btneditardet.Enabled = false;
                                    btnquitardet.Enabled = false;
                                    btnanadirdet.Enabled = false;
                                    btnsaldoinicial.Enabled = true;
                                }
                                else
                                {
                                    //btnnuevodet.Enabled = false;
                                    //btneditardet.Enabled = false;
                                    //btnquitardet.Enabled = false;
                                    //btnanadirdet.Enabled = false;
                                    btnsaldoinicial.Enabled = false;
                                }

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoopercod.Text) & string.IsNullOrEmpty(txttipooperdes.Text))
                    {
                        BuscarTipoOperacion();
                        Verificar_Tipo_Operacion();

                        if (txttipoopercod.Text == "14") // Saldo Inicial
                        {
                            btnnuevodet.Enabled = false;
                            btneditardet.Enabled = false;
                            btnquitardet.Enabled = false;
                            btnanadirdet.Enabled = false;
                            btnsaldoinicial.Visible = true;
                        }
                        else
                        {
                            //btnnuevodet.Enabled = false;
                            //btneditardet.Enabled = false;
                            //btnquitardet.Enabled = false;
                            //btnanadirdet.Enabled = false;
                            btnsaldoinicial.Visible = false;
                        }

                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public Boolean FNecesitaCC, FNecesitaCG, FGeneraAsiento, FIsValorizado,FEstransferenciaAlma,FRequiereEntidad;


        void Verificar_Tipo_Operacion()
        {
            Verifica_Documento();
            VerificaCentro_Costo_Gasto();
            Verifica_Transferencia();
            VerificaEntidad();
            txttipoopercod.Focus();
        }

        void Verifica_Documento()
        {
            if (FNecesitaDocumento == true)
            {
                BuscarDoc_Defecto();
                txttipodoc.Enabled = true;
             txttipodoc.Enabled = false;
            }
            else
            {
                txttipodoc.Enabled = true;
                txttipodoc.ResetText();
                txtserie.ResetText();
         
            }
        }
        void VerificaCentro_Costo_Gasto()
        {
            //FNecesitaCC = Entidad.Top_NecesitaCC;
            //FNecesitaCG = Entidad.Top_NecesitaCG;

            if (FNecesitaCC==true != FNecesitaCG == true)
            {
                //txtccgcod.Enabled = true;
            }
            else
            {
                //txtccgcod.Enabled = false;
            }
        }

        void Verifica_Transferencia()
        {
            if (FEstransferenciaAlma == true)
            {
            gbtransferencia.Enabled=true;
            }
            else
            {
                gbtransferencia.Enabled = false;
            }
        }

        void VerificaEntidad()
        {
            //FRequiereEntidad = Entidad.Top_Requiere_Tipo_Entidad;
            //FTipoEntidad = Entidad.Top_Tipo_Entidad_Cod;
            if (FRequiereEntidad == true)
            {
                BuscarTipoEntidad_Requiere();
                //txttipoentcod.Enabled = false;
            }
            else
            {
                //txttipoentcod.Enabled = true;
            }
        }
        public void BuscarDoc_Defecto()
        {
            try
            {
                //txttipodoc.Text = Accion.Formato(txttipodoc.Text, 3);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_Comprobante = FTipoDocDefecto
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_Comprobante).ToString().Trim().ToUpper() == FTipoDocDefecto.ToUpper())
                        {
                            txttipodoc.Text = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void BuscarTipoEntidad_Requiere()
        {
            try
            {
            
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar_tipo_entidad(new Entidad_Entidad
                {
                    Id_Tipo_Ent = FTipoEntidad
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Id_Tipo_Ent).ToString().Trim().ToUpper() == FTipoEntidad.Trim().ToUpper())
                        {
                            //txttipoentcod.Text = (T.Id_Tipo_Ent).ToString().Trim();
                            //txttipoentdesc.Text = T.Ent_Descripcion;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtresponsable_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtresponsable.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtresponsable.Text.Substring(txtresponsable.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtresponsable.Text = Entidad.Ent_RUC_DNI;
                                txtresponsabledesc.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtresponsable.Text) & string.IsNullOrEmpty(txtresponsabledesc.Text))
                    {
                        BuscarResponsable();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarResponsable()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtresponsable.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtresponsable.Text.Trim().ToUpper())
                        {
                            txtresponsable.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtresponsabledesc.Text = T.Ent_Ape_Materno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        string codMarca;
        string desMarca;
        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarDetalle())
                {
                    Entidad_Movimiento_Inventario ItemDetalle = new Entidad_Movimiento_Inventario();

                    ItemDetalle.Id_Item = Id_Item;

                    ItemDetalle.Invd_TipoBSA = txttipobsacod.Tag.ToString();
                    ItemDetalle.Invd_TipoBSA_Det = txttipobsacod.Text.Trim().ToString();
                    ItemDetalle.Invd_TipoBSA_Descipcion = txttipobsadesc.Text.Trim().ToString();
                    
                    ItemDetalle.Invd_Catalogo = txtproductocod.Text.Trim().ToString();
                    ItemDetalle.Invd_Catalogo_Desc = txtproductodesc.Text.Trim().ToString();

                    ItemDetalle.Invd_Unm = txtunmcod.Text.Trim().ToString();
                    ItemDetalle.Invd_Unm_Desc = txtunmdesc.Text.Trim().ToString();

                    ItemDetalle.Invd_Cantidad = Convert.ToDecimal(txtcantidad.Text);
                    ItemDetalle.Invd_Valor_Unit = Convert.ToDecimal(txtvalorunit.Text);
                    ItemDetalle.Invd_Total = Convert.ToDecimal(txtimporte.Text);

                    ItemDetalle.Acepta_lotes = chklote.Checked;

                    ItemDetalle.Invd_Marca = codMarca;
                    ItemDetalle.Invd_Marca_desc = desMarca;


                    if (EstadoDetalle == Estados.Nuevo)
                    {
                        Detalles.Add(ItemDetalle);
                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;
                    }
                    else if (EstadoDetalle == Estados.Modificar)
                    {

                        Detalles[Convert.ToInt32(Id_Item) - 1] = ItemDetalle;
                        UpdateGrilla();
                        EstadoDetalle = Estados.Guardado;

                    }

                    btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdatos.DataSource = Detalles;
            }
        }


        public bool VerificarDetalle()
        {

            if (string.IsNullOrEmpty(txttipobsadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo");
                txttipobsacod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmotivodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar motivo");
                txtmotivocod.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipooperdes.Text))
            {
                //tools.ShowWarnig("FALTA EL PRODUCTO", "Primero debe de seleccionar el producto antes de grabar");
                Accion.Advertencia("Debe ingresar un tipo operacion");
                txttipoopercod.Focus();
                return false;
            }

            //if (string.IsNullOrEmpty(txtalmacendesc.Text))
            //{
            //    //tools.ShowWarnig("VALOR INCORRECTO EN CANTIDAD", "Debe de ingresar un valor numérico mayor a CERO en el campo cantidad antes de grabar");
            //    Accion.Advertencia("Debe ingresar un almacen");
            //    txtalmacencod.Focus();
            //    return false;
            //}



            return true;
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Modificar;

        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    //Detalles.RemoveAt[gridView1.GetFocusedDataSourceRowIndex];

                    Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());
                 
                    dgvdatos.DataSource = null;
                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;
                        RefreshNumeral();
                    }

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        public void RefreshNumeral()
        {
            int NumOrden = 1;
            foreach (Entidad_Movimiento_Inventario Det in Detalles)
            {
                Det.Id_Item = NumOrden;
                NumOrden += 1;
            }
        }


        public List<Entidad_Movimiento_Inventario> Lista_Doc_ref_Lista = new List<Entidad_Movimiento_Inventario>();

        Entidad_Movimiento_Inventario EntDoc = new Entidad_Movimiento_Inventario();


        public List<Entidad_Movimiento_Inventario> Documentos_Provisionados = new List<Entidad_Movimiento_Inventario>();


        bool ExisteOrden(Entidad_Movimiento_Inventario op)
        {
            foreach (Entidad_Movimiento_Inventario Ent in Documentos_Provisionados)
            {
                if (((op.Doc_Periodo == Ent.Doc_Periodo) && ((op.Doc_Proceso == Ent.Doc_Proceso) && ((op.Doc_Folio == Ent.Doc_Folio)))))
                {
                    return true;
                }

            }

            return false;
        }


        List<Entidad_Movimiento_Inventario> Detalles_Ordee_Inventario= new List<Entidad_Movimiento_Inventario>();

        List<Entidad_Movimiento_Inventario> Detalles_Lotes_Inventario = new List<Entidad_Movimiento_Inventario>();
        private void btnbuscar_Click(object sender, EventArgs e)
        {


            if ((txtmotivodesc.Text == ""))
            {
                Accion.Advertencia("Debe seleccionar un motivo");
                txtmotivocod.Focus();
            }
            else if (txtalmacendesc.Text == "")
            {
                Accion.Advertencia("Debe seleccionar un almacen");
                txtalmacencod.Focus();
            }
            else if (txttipooperdes.Text == "")
            {
                Accion.Advertencia("Debe seleccionar un tipo de operacion");
                txttipoopercod.Focus();
            }
            else
            {
                using (frm_entrada_provision_buscar f = new frm_entrada_provision_buscar())
                {
                    f.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    f.Id_Anio = Actual_Conexion.AnioSelect;
                    f.Id_Periodo = Actual_Conexion.PeriodoSelect;
                    f.Id_Tipo_Mov = "0035";// txtmotivocod.Tag.ToString();
                    f.Id_Almacen = txtalmacentrancod.Text;

                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        foreach (int Item in f.gridView1.GetSelectedRows())
                        {
                            //Entidad_Movimiento_Inventario
                            Entidad_Movimiento_Inventario Ord = new Entidad_Movimiento_Inventario();
                            Entidad_Movimiento_Inventario Compra_Ord2 = new Entidad_Movimiento_Inventario();
                            Ord = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                            Compra_Ord2.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            Compra_Ord2.Doc_Proceso = Ord.Proceso_Cod;
                            Compra_Ord2.Doc_Anio = Ord.Id_Anio;
                            Compra_Ord2.Doc_Periodo = Ord.Id_Periodo;
                            Compra_Ord2.Doc_Folio = Ord.Id_Movimiento;
                            Compra_Ord2.Id_Almacen = Ord.Id_Almacen;
                            Compra_Ord2.Id_Tipo_Mov = Ord.Id_Tipo_Mov;
                            Compra_Ord2.Doc_Item = Documentos_Provisionados.Count + 1;


                            if (ExisteOrden(Compra_Ord2) == false)
                            {
                                Documentos_Provisionados.Add(Compra_Ord2);

                                //Buscamos los detalles

                                Entidad_Movimiento_Inventario Det = new Entidad_Movimiento_Inventario();

                                Det.Id_Empresa = Compra_Ord2.Id_Empresa;
                                Det.Doc_Anio = Compra_Ord2.Doc_Anio;
                                Det.Doc_Periodo = Compra_Ord2.Doc_Periodo;
                                Det.Id_Tipo_Mov = Compra_Ord2.Id_Tipo_Mov;
                                Det.Id_Almacen = Compra_Ord2.Id_Almacen;
                                Det.Doc_Proceso = Compra_Ord2.Doc_Proceso;
                                Det.Doc_Folio = Compra_Ord2.Doc_Folio;


                                Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

                                if (Det.Doc_Proceso == "0049")
                                {
                                    Detalles_Ordee_Inventario = log.Buscar_Detalles_Orden(Det);
                                }
                                if (Det.Doc_Proceso == "0065")//0050 asi estaba lo cambie por 0065
                                {
                                    Detalles_Ordee_Inventario = log.Buscar_Detalles_Inventario(Det);

                                    Detalles_Lotes_Inventario = log.Buscar_Detalles_Inventario_Lotes(Det);

                                }

                                foreach (Entidad_Movimiento_Inventario T in Detalles_Ordee_Inventario)
                                {
                                    Entidad_Movimiento_Inventario Enti = new Entidad_Movimiento_Inventario();


                                    Enti.Id_Item = Detalles.Count + 1;//Documentos_Provisionados.Count + 1;

                                    Enti.Invd_TipoBSA = T.Invd_TipoBSA;
                                    Enti.Invd_TipoBSA_Det = T.Invd_TipoBSA_Det;
                                    Enti.Invd_TipoBSA_Descipcion = T.Invd_TipoBSA_Descipcion;


                                    Enti.Invd_Catalogo = T.Invd_Catalogo;
                                    Enti.Invd_Catalogo_Desc = T.Invd_Catalogo_Desc;

                                    Enti.Invd_Cantidad = T.Invd_Cantidad;
                                    Enti.Invd_Valor_Unit = T.Invd_Valor_Unit;

                                    Enti.Invd_Total = T.Invd_Total;

                                    Enti.Invd_Centro_CG = T.Invd_Centro_CG;
                                    Enti.Invd_Centro_CG_Desc = T.Invd_Centro_CG_Desc;

                                    Enti.Acepta_lotes = T.Acepta_lotes;


                                    Detalles.Add(Enti);

                                }

                                foreach (Entidad_Movimiento_Inventario T in Detalles_Lotes_Inventario)
                                {

                                    Entidad_Movimiento_Inventario Enti = new Entidad_Movimiento_Inventario();

                                    Enti.Lot_Adm_item = T.Lot_Adm_item;
                                    Enti.Lot_item = T.Lot_item;
                                    Enti.Lot_Catalogo = T.Lot_Catalogo;
                                    Enti.Lot_Lote = T.Lot_Lote;
                                    Enti.Lot_FechaFabricacion = T.Lot_FechaFabricacion;
                                    Enti.Lot_FechaVencimiento = T.Lot_FechaVencimiento;
                                    Enti.Lot_Cantidad = T.Lot_Cantidad;

                                    ListaLotesFinal.Add(Enti);
                                }




                                    if (Detalles.Count > 0)
                                {
                                    dgvdatos.DataSource = null;
                                    dgvdatos.DataSource = Detalles;

                                }
                            }
                        }
                    }
                }
            }


        }

        private void btnlistadoDocs_Click(object sender, EventArgs e)
        {

            //try
            //{
            //    if (Documentos_Provisionados.Count > 0)
            //    {
            //        using (frm_entrada_provision f = new frm_entrada_provision())
            //        {

            //            f.buscar2(Documentos_Provisionados);
            //            if (f.ShowDialog() == DialogResult.OK)
            //            {

            //            }
            //        }

            //    }
            //    else
            //    {
            //        Accion.Advertencia("Noe existe ninguna orden");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Accion.ErrorSistema(ex.Message);
            //}

       
        }

        //private void txttipoentcod_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (String.IsNullOrEmpty(txttipoentcod.Text) == false)
        //    {
        //        try
        //        {
        //            if (e.KeyCode == Keys.Enter & txttipoentcod.Text.Substring(txttipoentcod.Text.Length - 1, 1) == "*")
        //            {
        //                using (frm_tipo_entidad_busqueda f = new frm_tipo_entidad_busqueda())
        //                {


        //                    if (f.ShowDialog(this) == DialogResult.OK)
        //                    {
        //                        Entidad_Entidad Entidad = new Entidad_Entidad();

        //                        Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

        //                        txttipoentcod.Text = Entidad.Id_Tipo_Ent;
        //                        txttipoentdesc.Text = Entidad.Ent_Descripcion;
        //                    }
        //                }
        //            }
        //            else
        //                if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipoentcod.Text) & string.IsNullOrEmpty(txttipoentdesc.Text))
        //            {
        //                BuscarTipoEntidad();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Accion.ErrorSistema(ex.Message);
        //        }
        //    }
        //}

        private void txtvalorunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtvalorunit.Text.Trim()))
            {
                try
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        decimal Importe;
                        Importe = Convert.ToDecimal(txtvalorunit.Text) * Convert.ToDecimal(txtcantidad.Text);
                        txtimporte.Text = Convert.ToString(Importe);
                    }

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void gbtransferencia_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtunmcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtunmcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunmcod.Text.Substring(txtunmcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_unidad_medida_busqueda f = new frm_unidad_medida_busqueda())
                        {
                            f.Producto_Cod = txtproductocod.Text;
                            f.Producto_Unm = true;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunmcod.Text = Entidad.Id_Unidad_Medida;
                                txtunmdesc.Text = Entidad.Und_Descripcion;
                       
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtunmcod.Text) & string.IsNullOrEmpty(txtunmdesc.Text))
                    {
                        BuscarUnmPres();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarUnmPres()
        {
            try
            {
                txtunmcod.Text = Accion.Formato(txtunmcod.Text, 3);
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Unidad_Medida = txtunmcod.Text,
                    Id_Catalogo = txtproductocod.Text.Trim()
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Id_Unidad_Medida).ToString().Trim().ToUpper() == txtunmcod.Text.Trim().ToUpper())
                        {
                            txtunmcod.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdesc.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void BuscarUnm_Defecto_Producto()
        {
            try
            {
             
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Empresa=Actual_Conexion.CodigoEmpresa,
                    Id_Catalogo = txtproductocod.Text.Trim(),
                    Unm_Defecto=true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Unm_Defecto)==true)
                        {
                            txtunmcod.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdesc.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        private void txtalmacencod_TextChanged_1(object sender, EventArgs e)
        {
            if (txtalmacencod.Focus() == false)
            {
                txtalmacendesc.ResetText();
            }
        }

        private void txtresponsable_TextChanged(object sender, EventArgs e)
        {
            if (txtresponsable.Focus() == false)
            {
                txtresponsabledesc.ResetText();
            }
        }

        private void txtalmacentrancod_TextChanged(object sender, EventArgs e)
        {
            if (txtalmacentrancod.Focus() == false)
            {
                txtalmacentrandesc.ResetText();
            }
        }

        private void txtunmcod_TextChanged(object sender, EventArgs e)
        {
            if (txtunmcod.Focus() == false)
            {
                txtunmdesc.ResetText();
            }
        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        List<Entidad_Movimiento_Inventario> ListaLotesFinal = new List<Entidad_Movimiento_Inventario>();
        private void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Movimiento_Inventario parametros = new Entidad_Movimiento_Inventario();

                    parametros = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    if (parametros.Acepta_lotes == true)
                    {

                        using (frm_lotes_edicion_inv f = new frm_lotes_edicion_inv())
                        {

                            f.codProducto = parametros.Invd_Catalogo;
                            f.desProducto = parametros.Invd_Catalogo_Desc;
                            f.codMarca = parametros.Invd_Marca;
                            f.desMarca = parametros.Invd_Marca_desc;
                            f.txtcantidad.Text = Convert.ToString(parametros.Invd_Cantidad);
                            f.Adm_Item = parametros.Id_Item;

                            f.ListaLotesCompleta = ListaLotesFinal;

                            if (f.ShowDialog() == DialogResult.OK)
                            {

                                ListaLotesFinal = f.ListaLotesCompleta;
 

                            }

                        }


                    }
                    else
                    {
                        //aqui bloquearemos cuando no tiene lotes
                        //MessageBox.Show("ccc");
                        repositoryItemButtonEdit1.ReadOnly = false;
                    }


                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

            GridView View = (GridView)sender;
            if (e.RowHandle >= 0)
            {
                string Acepta = View.GetRowCellDisplayText(e.RowHandle, View.Columns["Acepta_lotes"]);
                string codProducto = View.GetRowCellDisplayText(e.RowHandle, View.Columns["Invd_Catalogo"]);
                int Adm_Item = Convert.ToInt32(View.GetRowCellDisplayText(e.RowHandle, View.Columns["Id_Item"]));

                if (Acepta == "True")
                {
                    var PrivView = from item in ListaLotesFinal
                                   where item.Lot_Catalogo == codProducto && item.Lot_Adm_item == Adm_Item
                                   orderby item.Id_Anio descending
                                   select item;

                    if (PrivView.Count() == 0)
                    {
                        e.Appearance.BackColor = Color.FromArgb(204, 240, 112);
                    }


                }

            }
        }

        private void btnsaldoinicial_Click(object sender, EventArgs e)
        {
            try
            {
                //Entidad_Movimiento_Inventario ItemDetalle = new Entidad_Movimiento_Inventario();
                Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

                List<Entidad_Movimiento_Inventario> Lista_saldo_Inicial = new List<Entidad_Movimiento_Inventario>();
                List<Entidad_Movimiento_Inventario> Lista_saldo_Inicial_Lote = new List<Entidad_Movimiento_Inventario>();

                Lista_saldo_Inicial = log.Traer_Saldo_Inicial(new Entidad_Movimiento_Inventario
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect,
                    Id_Almacen = txtalmacencod.Text.Trim()
                });

                if (Lista_saldo_Inicial.Count > 0)
                {
                    dgvdatos.DataSource = null;
                    dgvdatos.DataSource = Lista_saldo_Inicial;
                    Detalles = Lista_saldo_Inicial;


                    foreach (Entidad_Movimiento_Inventario T in Lista_saldo_Inicial)
                    {
                        if ((T.Acepta_lotes) == true)
                        {
                            Lista_saldo_Inicial_Lote = log.Traer_Saldo_Inicial_Lotes(new Entidad_Movimiento_Inventario
                            {
                                Id_Empresa = Actual_Conexion.CodigoEmpresa,
                                Id_Anio = Actual_Conexion.AnioSelect,
                                Id_Almacen = txtalmacencod.Text.Trim(),
                                Invd_Catalogo=T.Invd_Catalogo

                            });

                            if (Lista_saldo_Inicial_Lote.Count > 0)
                            {
                                foreach (Entidad_Movimiento_Inventario TT in Lista_saldo_Inicial_Lote)
                                {

                                    Entidad_Movimiento_Inventario Enti = new Entidad_Movimiento_Inventario();

                                    Enti.Lot_Adm_item = T.Id_Item;
                                    Enti.Lot_item = TT.Lot_item;
                                    Enti.Lot_Catalogo = TT.Lot_Catalogo;
                                    Enti.Lot_Lote = TT.Lot_Lote;
                                    Enti.Lot_FechaFabricacion = TT.Lot_FechaFabricacion;
                                    Enti.Lot_FechaVencimiento = TT.Lot_FechaVencimiento;
                                    Enti.Lot_Cantidad = TT.Lot_Cantidad;

                                    ListaLotesFinal.Add(Enti);
                                }
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        //public void BuscarTipoEntidad()
        //{
        //    try
        //    {
        //        //txtrucdni.Text = Accion.Formato(txt.Text, 3);
        //        Logica_Entidad log = new Logica_Entidad();

        //        List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

        //        Generales = log.Listar(new Entidad_Entidad
        //        {
        //            Id_Tipo_Ent = txttipoentcod.Text
        //        });

        //        if (Generales.Count > 0)
        //        {

        //            foreach (Entidad_Entidad T in Generales)
        //            {
        //                if ((T.Id_Tipo_Ent).ToString().Trim().ToUpper() == txttipoentcod.Text.Trim().ToUpper())
        //                {
        //                    txttipoentcod.Text = (T.Ent_RUC_DNI).ToString().Trim();
        //                    txttipoentdesc.Text = T.Ent_Descripcion;

        //                }
        //            }

        //        }
        //        else
        //        {
        //            Accion.Advertencia("No se encontro ningun dato");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Accion.ErrorSistema(ex.Message);
        //    }
        //}

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    Entidad_Movimiento_Inventario Entidad = new Entidad_Movimiento_Inventario();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                    Id_Item= Entidad.Id_Item;
                                       
                    txttipobsacod.Tag = Entidad.Invd_TipoBSA;
                    txttipobsacod.Text = Entidad.Invd_TipoBSA_Det;
                    txttipobsadesc.Text = Entidad.Invd_TipoBSA_Descipcion;

                    txtunmcod.Text = Entidad.Invd_Unm;
                    txtunmdesc.Text = Entidad.Invd_Unm_Desc;

                    txtproductocod.Text = Entidad.Invd_Catalogo;
                    txtproductodesc.Text = Entidad.Invd_Catalogo_Desc;

                    txtcantidad.Text = Convert.ToString(Entidad.Invd_Cantidad);
                    txtvalorunit.Text = Convert.ToString(Entidad.Invd_Valor_Unit);
                    txtimporte.Text= Convert.ToString(Entidad.Invd_Total);

                    txtunmcod.Text = Entidad.Invd_Unm;
                    txtunmdesc.Text = Entidad.Invd_Unm_Desc;


                    chklote.Checked = Entidad.Acepta_lotes;

                    codMarca = Entidad.Invd_Marca;
                    desMarca = Entidad.Invd_Marca_desc;

                    if (Estado_Ven_Boton == "1")
                    {
                        //EstadoDetalle = Estados.Consulta;
                        EstadoDetalle = Estados.SoloLectura;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle = Estados.SoloLectura;


                    }


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public Boolean FNecesitaEntidad, FNecesitaDocumento, IsSaldoInicial;
        public string FEntidadTipo, FTipoDocDefecto,FTipoEntidad;



        public void BuscarTipoOperacion()
        {
            try
            {
                txttipoopercod.Text = Accion.Formato(txttipoopercod.Text, 2);

                Logica_Tipo_Operacion log = new Logica_Tipo_Operacion();

                List<Entidad_Tipo_Operacion> Generales = new List<Entidad_Tipo_Operacion>();

                Generales = log.Listar(new Entidad_Tipo_Operacion
                {
                        Id_Operacion = txttipoopercod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Tipo_Operacion T in Generales)
                    {
                        if ((T.Id_Operacion).ToString().Trim().ToUpper() == txttipoopercod.Text.Trim().ToUpper())
                        {
                            txttipoopercod.Text = (T.Id_Operacion).ToString().Trim();
                            txttipooperdes.Text = T.Top_Descripcion;
                            FNecesitaCC = T.Top_NecesitaCC;
                            FNecesitaCG = T.Top_NecesitaCG;
                            FGeneraAsiento = T.Top_Genera_Asiento;
                            FIsValorizado = T.Top_Es_Valorizado;
                            FNecesitaDocumento = T.Top_Necesita_Doc;
                            IsSaldoInicial = T.Top_Es_Saldo_Inicial;
                            FTipoDocDefecto = T.Top_Tipo_Doc_Defecto;
                            FEstransferenciaAlma = T.Top_Requiere_Transferecia_Almacen;
                            FRequiereEntidad = T.Top_Requiere_Tipo_Entidad;
                            FTipoEntidad = T.Top_Tipo_Entidad_Cod;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txttipoopercod_TextChanged(object sender, EventArgs e)
        {
            if (txttipoopercod.Focus() == false)
            {
                txttipooperdes.ResetText();
            }
        }

        private void txttipoentcod_TextChanged(object sender, EventArgs e)
        {
            //if (txttipoentcod.Focus() == false)
            //{
            //    txttipoentdesc.ResetText();
            //}
        }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            EstadoDetalle = Estados.Nuevo;

            HabilitarDetalles();
            VerificaCentro_Costo_Gasto();

        }


        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}
