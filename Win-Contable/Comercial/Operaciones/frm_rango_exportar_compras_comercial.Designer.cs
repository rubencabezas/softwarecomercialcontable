﻿namespace Contable.Comercial.Operaciones
{
    partial class frm_rango_exportar_compras_comercial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txthasta = new DevExpress.XtraEditors.TextEdit();
            this.txtdesde = new DevExpress.XtraEditors.TextEdit();
            this.btncancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btngenerar = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txthasta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdesde.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.txthasta);
            this.groupBox1.Controls.Add(this.txtdesde);
            this.groupBox1.Location = new System.Drawing.Point(3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(320, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(150, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(32, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Hasta:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(6, 23);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(33, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "desde:";
            // 
            // txthasta
            // 
            this.txthasta.EnterMoveNextControl = true;
            this.txthasta.Location = new System.Drawing.Point(216, 20);
            this.txthasta.Name = "txthasta";
            this.txthasta.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txthasta.Properties.Mask.BeepOnError = true;
            this.txthasta.Properties.Mask.EditMask = "d";
            this.txthasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txthasta.Properties.Mask.SaveLiteral = false;
            this.txthasta.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txthasta.Properties.MaxLength = 10;
            this.txthasta.Size = new System.Drawing.Size(89, 20);
            this.txthasta.TabIndex = 3;
            // 
            // txtdesde
            // 
            this.txtdesde.EnterMoveNextControl = true;
            this.txtdesde.Location = new System.Drawing.Point(49, 20);
            this.txtdesde.Name = "txtdesde";
            this.txtdesde.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtdesde.Properties.Mask.BeepOnError = true;
            this.txtdesde.Properties.Mask.EditMask = "d";
            this.txtdesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtdesde.Properties.Mask.SaveLiteral = false;
            this.txtdesde.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtdesde.Properties.MaxLength = 10;
            this.txtdesde.Size = new System.Drawing.Size(89, 20);
            this.txtdesde.TabIndex = 1;
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(52, 60);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(89, 28);
            this.btncancelar.TabIndex = 3;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // btngenerar
            // 
            this.btngenerar.Location = new System.Drawing.Point(170, 60);
            this.btngenerar.Name = "btngenerar";
            this.btngenerar.Size = new System.Drawing.Size(89, 28);
            this.btngenerar.TabIndex = 2;
            this.btngenerar.Text = "Generar";
            this.btngenerar.Click += new System.EventHandler(this.btngenerar_Click);
            // 
            // frm_rango_exportar_compras_comercial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 125);
            this.Controls.Add(this.btngenerar);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_rango_exportar_compras_comercial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Parametros";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txthasta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdesde.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        public DevExpress.XtraEditors.TextEdit txthasta;
        public DevExpress.XtraEditors.TextEdit txtdesde;
        private DevExpress.XtraEditors.SimpleButton btncancelar;
        private DevExpress.XtraEditors.SimpleButton btngenerar;
    }
}