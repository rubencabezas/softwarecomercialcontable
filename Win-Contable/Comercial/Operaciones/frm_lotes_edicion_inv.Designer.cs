﻿namespace Comercial.Operaciones
{
    partial class frm_lotes_edicion_inv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbxdetalle = new System.Windows.Forms.GroupBox();
            this.txtcantidad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtlote = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechavencimiento = new DevExpress.XtraEditors.TextEdit();
            this.txtfechafabricacion = new DevExpress.XtraEditors.TextEdit();
            this.txtdescmarca = new DevExpress.XtraEditors.TextEdit();
            this.txtcodmarca = new DevExpress.XtraEditors.TextEdit();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.gbxdetalle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechafabricacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescmarca.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodmarca.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxdetalle
            // 
            this.gbxdetalle.Controls.Add(this.txtcantidad);
            this.gbxdetalle.Controls.Add(this.labelControl6);
            this.gbxdetalle.Controls.Add(this.labelControl5);
            this.gbxdetalle.Controls.Add(this.txtlote);
            this.gbxdetalle.Controls.Add(this.labelControl2);
            this.gbxdetalle.Controls.Add(this.labelControl4);
            this.gbxdetalle.Controls.Add(this.labelControl3);
            this.gbxdetalle.Controls.Add(this.txtfechavencimiento);
            this.gbxdetalle.Controls.Add(this.txtfechafabricacion);
            this.gbxdetalle.Controls.Add(this.txtdescmarca);
            this.gbxdetalle.Controls.Add(this.txtcodmarca);
            this.gbxdetalle.Controls.Add(this.txtproductodesc);
            this.gbxdetalle.Controls.Add(this.txtproductocod);
            this.gbxdetalle.Controls.Add(this.labelControl1);
            this.gbxdetalle.Location = new System.Drawing.Point(1, 39);
            this.gbxdetalle.Name = "gbxdetalle";
            this.gbxdetalle.Size = new System.Drawing.Size(702, 91);
            this.gbxdetalle.TabIndex = 1;
            this.gbxdetalle.TabStop = false;
            this.gbxdetalle.Text = "Datos";
            // 
            // txtcantidad
            // 
            this.txtcantidad.Enabled = false;
            this.txtcantidad.EnterMoveNextControl = true;
            this.txtcantidad.Location = new System.Drawing.Point(614, 65);
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Properties.Mask.EditMask = "n2";
            this.txtcantidad.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtcantidad.Size = new System.Drawing.Size(82, 20);
            this.txtcantidad.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(565, 67);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(43, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Cantidad";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(393, 67);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(89, 13);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Fecha vencimiento";
            // 
            // txtlote
            // 
            this.txtlote.EnterMoveNextControl = true;
            this.txtlote.Location = new System.Drawing.Point(56, 64);
            this.txtlote.Name = "txtlote";
            this.txtlote.Size = new System.Drawing.Size(168, 20);
            this.txtlote.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Nro Lote";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(230, 64);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(84, 13);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Fecha fabricacion";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(17, 45);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(33, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Marca:";
            // 
            // txtfechavencimiento
            // 
            this.txtfechavencimiento.EnterMoveNextControl = true;
            this.txtfechavencimiento.Location = new System.Drawing.Point(486, 65);
            this.txtfechavencimiento.Name = "txtfechavencimiento";
            this.txtfechavencimiento.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechavencimiento.Properties.Mask.BeepOnError = true;
            this.txtfechavencimiento.Properties.Mask.EditMask = "d";
            this.txtfechavencimiento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechavencimiento.Properties.Mask.SaveLiteral = false;
            this.txtfechavencimiento.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechavencimiento.Properties.MaxLength = 10;
            this.txtfechavencimiento.Size = new System.Drawing.Size(73, 20);
            this.txtfechavencimiento.TabIndex = 11;
            // 
            // txtfechafabricacion
            // 
            this.txtfechafabricacion.EnterMoveNextControl = true;
            this.txtfechafabricacion.Location = new System.Drawing.Point(318, 64);
            this.txtfechafabricacion.Name = "txtfechafabricacion";
            this.txtfechafabricacion.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechafabricacion.Properties.Mask.BeepOnError = true;
            this.txtfechafabricacion.Properties.Mask.EditMask = "d";
            this.txtfechafabricacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechafabricacion.Properties.Mask.SaveLiteral = false;
            this.txtfechafabricacion.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechafabricacion.Properties.MaxLength = 10;
            this.txtfechafabricacion.Size = new System.Drawing.Size(68, 20);
            this.txtfechafabricacion.TabIndex = 9;
            // 
            // txtdescmarca
            // 
            this.txtdescmarca.Location = new System.Drawing.Point(94, 42);
            this.txtdescmarca.Name = "txtdescmarca";
            this.txtdescmarca.Properties.ReadOnly = true;
            this.txtdescmarca.Size = new System.Drawing.Size(608, 20);
            this.txtdescmarca.TabIndex = 5;
            // 
            // txtcodmarca
            // 
            this.txtcodmarca.Location = new System.Drawing.Point(56, 42);
            this.txtcodmarca.Name = "txtcodmarca";
            this.txtcodmarca.Properties.ReadOnly = true;
            this.txtcodmarca.Size = new System.Drawing.Size(35, 20);
            this.txtcodmarca.TabIndex = 4;
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Location = new System.Drawing.Point(159, 19);
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Properties.ReadOnly = true;
            this.txtproductodesc.Size = new System.Drawing.Size(543, 20);
            this.txtproductodesc.TabIndex = 2;
            // 
            // txtproductocod
            // 
            this.txtproductocod.Location = new System.Drawing.Point(56, 19);
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Properties.ReadOnly = true;
            this.txtproductocod.Size = new System.Drawing.Size(100, 20);
            this.txtproductocod.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Producto";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.Appearance.Options.UseFont = true;
            this.btnAceptar.Location = new System.Drawing.Point(615, 131);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(82, 29);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.Visible = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnanadirdet.Appearance.Options.UseFont = true;
            this.btnanadirdet.Location = new System.Drawing.Point(244, 136);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(75, 20);
            this.btnanadirdet.TabIndex = 2;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnquitardet.Appearance.Options.UseFont = true;
            this.btnquitardet.Location = new System.Drawing.Point(82, 136);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(75, 20);
            this.btnquitardet.TabIndex = 24;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneditardet.Appearance.Options.UseFont = true;
            this.btneditardet.Location = new System.Drawing.Point(163, 136);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(75, 20);
            this.btneditardet.TabIndex = 25;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevodet.Appearance.Options.UseFont = true;
            this.btnnuevodet.Location = new System.Drawing.Point(1, 136);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(75, 20);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(1, 165);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(702, 157);
            this.dgvdatos.TabIndex = 21;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Item";
            this.gridColumn5.FieldName = "Lot_item";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Lote";
            this.gridColumn1.FieldName = "Lot_Lote";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 92;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Fecha Fab.";
            this.gridColumn2.FieldName = "Lot_FechaFabricacion";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 98;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Fecha Ven.";
            this.gridColumn3.FieldName = "Lot_FechaVencimiento";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 103;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Cantidad";
            this.gridColumn4.FieldName = "Lot_Cantidad";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 126;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(710, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 327);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(710, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 293);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(710, 34);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 293);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // frm_lotes_edicion_inv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 327);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnanadirdet);
            this.Controls.Add(this.btnquitardet);
            this.Controls.Add(this.btneditardet);
            this.Controls.Add(this.btnnuevodet);
            this.Controls.Add(this.dgvdatos);
            this.Controls.Add(this.gbxdetalle);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_lotes_edicion_inv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lotes";
            this.Load += new System.EventHandler(this.frm_lotes_edicion_inv_Load);
            this.gbxdetalle.ResumeLayout(false);
            this.gbxdetalle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechavencimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechafabricacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescmarca.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodmarca.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxdetalle;
        public DevExpress.XtraEditors.TextEdit txtcantidad;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.TextEdit txtlote;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        public DevExpress.XtraEditors.TextEdit txtfechavencimiento;
        public DevExpress.XtraEditors.TextEdit txtfechafabricacion;
        public DevExpress.XtraEditors.TextEdit txtdescmarca;
        public DevExpress.XtraEditors.TextEdit txtcodmarca;
        public DevExpress.XtraEditors.TextEdit txtproductodesc;
        public DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.SimpleButton btnAceptar;
        public DevExpress.XtraEditors.SimpleButton btnanadirdet;
        public DevExpress.XtraEditors.SimpleButton btnquitardet;
        public DevExpress.XtraEditors.SimpleButton btneditardet;
        public DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}