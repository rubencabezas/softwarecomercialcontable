﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Contable
{
    public partial class frml_lotes_edicion : frm_fuente
    {
        public frml_lotes_edicion()
        {
            InitializeComponent();
        }

        public string codProducto ;
        public string desProducto;
        public string codMarca;
        public string desMarca;
        public int Adm_Item;

        private int Lot_Item;

        private bool Nuevo=false;
        private bool Editar = false;

        public string Estado_Ven_Boton ;

        public List<Entidad_Movimiento_Cab> ListaLotesCompleta = new List<Entidad_Movimiento_Cab>();
        public List<Entidad_Movimiento_Cab> ListaLotes = new List<Entidad_Movimiento_Cab>();

        private void frml_lotes_edicion_Load(object sender, EventArgs e)
        {
            gbxdetalle.Enabled = false;
            txtproductocod.Text = codProducto;
            txtproductodesc.Text = desProducto;
            txtcodmarca.Text = codMarca;
            txtdescmarca.Text = desMarca;


            UpdateGrilla_Correspondiente();

            btnanadirdet.Enabled = false;
            btnnuevodet.Select();
        }

        void UpdateGrilla_Correspondiente()
        {

            var PrivView = from item in ListaLotesCompleta
                           where item.Lot_Catalogo == codProducto && item.Lot_Adm_item == Adm_Item
                           orderby item.Id_Anio descending
                           select item;

            ListaLotes = PrivView.ToList();

            dgvdatos.DataSource = null;
            if (ListaLotes.Count > 0)
            {
                dgvdatos.DataSource = ListaLotes;
            }
        }
       
        private void btnaceptar_Click(object sender, EventArgs e)
        {
            if (ListaLotes.Count== 0)
            {
                Accion.Advertencia("Debe contener un lote");
            }
            else
            {
                DialogResult = DialogResult.OK;
            }

        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {
            try
            {
                if (validar())
                {
                        Entidad_Movimiento_Cab Lote = new Entidad_Movimiento_Cab();

                        Lote.Lot_Adm_item = Adm_Item;
                        Lote.Lot_item = Lot_Item;
                        Lote.Lot_Catalogo = txtproductocod.Text;
                        Lote.Lot_Lote = txtlote.Text;
                        Lote.Lot_FechaFabricacion = Convert.ToDateTime(txtfechafabricacion.Text);
                        Lote.Lot_FechaVencimiento = Convert.ToDateTime(txtfechavencimiento.Text);
                        Lote.Lot_Cantidad = Convert.ToDecimal(txtcantidad.Text);

                        //if (ExistsLoteExiste(Lote.Lot_Catalogo, Lote.Lot_Lote, Lote.Lot_FechaFabricacion, Lote.Lot_FechaVencimiento))
                        //{
                        //    Accion.Advertencia("Este lote para este producto ya existe en la lista");

                        //}
                        //else
                        //{
                            if (Nuevo==true)
                            {
                                ListaLotesCompleta.Add(Lote);
                                UpdateGrilla_Correspondiente();
                                Limpiar();
                                Nuevo = false;
                            }
                            else if (Editar == true)
                            {
                                ListaLotesCompleta[Convert.ToInt32(Lot_Item) - 1] = Lote;
                                UpdateGrilla_Correspondiente();
                                //UpdateGrilla();
                                Editar = false;
                            }
                     
                        //}

                        Bloquear();
                        btnnuevodet.Select();
                }
            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

    

        void Limpiar()
        {
             
            txtlote.ResetText();
            txtfechafabricacion.ResetText();
            txtfechavencimiento.ResetText();
        
        }
   


        bool ExistsLoteExiste(string codProducto, string Lote, DateTime FechaFab, DateTime FechaVenci)
        {
            foreach (Entidad_Movimiento_Cab Item in ListaLotes)
            {
                if ((Item.Lot_Catalogo == codProducto) && (Item.Lot_Lote == Lote) && (Item.Lot_FechaFabricacion == FechaFab) && (Item.Lot_FechaVencimiento == FechaVenci))
                {
                    return true;
                }

            }

            return false;
        }

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            Nuevo = true;
            gbxdetalle.Enabled = true;
            Lot_Item = ListaLotesCompleta.Count() + 1;
            Limpiar();
            Desbloquear();
            btnanadirdet.Enabled = true;
            txtlote.Select();
        }


        bool validar()
        {
            //  if (String.IsNullOrEmpty(txtlote.Text))
            //   {
            //  Accion.Advertencia("Debe ingresar un lote");
            //  txtlote.Focus();
            //  return false;
            // }

            //  if (String.IsNullOrEmpty(txtfechafabricacion.Text))
            //  {
            // Accion.Advertencia("Debe ingresar una fecha de fabricacion");
            // txtfechafabricacion.Focus();
            // return false;
            // }

            // if (string.IsNullOrEmpty(txtfechavencimiento.Text))
            // {
            // Accion.Advertencia("Debe ingresar una fecha de vencimiento");
            //txtfechavencimiento.Focus();
            //  return false;
            // }

            decimal cantidad =0;
          foreach(Entidad_Movimiento_Cab t in ListaLotes)
            {

                cantidad += t.Lot_Cantidad;

            }

             if (cantidad> Convert.ToDecimal(txtcantidad.Text)){
            Accion.Advertencia("Esta ingresando una cantidad mayor al detalle del producto");
                return false;
                 }

                return true;
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ListaLotes.Count == 0)
            {
                Accion.Advertencia("Debe contener un lote");
            }
            else
            {
                decimal cantidad = 0;
                foreach (Entidad_Movimiento_Cab t in ListaLotes)
                {
                    cantidad += t.Lot_Cantidad;
                }

                if (cantidad > Convert.ToDecimal(txtcantidad.Text))
                {
                    Accion.Advertencia("Esta ingresando una cantidad mayor al detalle del producto");
                    return ;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }

             
            }
        }

        void Bloquear()
        {
            txtlote.Enabled = false;
            txtfechafabricacion.Enabled = false;
            txtfechavencimiento.Enabled = false;
        }

        void Desbloquear()
        {
            txtlote.Enabled = true;
            txtfechafabricacion.Enabled = true;
            txtfechavencimiento.Enabled = true;
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
           
            try
            {
                if (ListaLotes.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
            
                    Entidad = ListaLotes[gridView1.GetFocusedDataSourceRowIndex()];

                    Lot_Item = Entidad.Lot_item;
                    txtlote.Text = Entidad.Lot_Lote;
                    txtfechafabricacion.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Lot_FechaFabricacion);//Convert.ToString(Entidad.Lot_FechaFabricacion);
                    txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Lot_FechaVencimiento);//Convert.ToString(Entidad.Lot_FechaVencimiento);

                    Bloquear();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            gbxdetalle.Enabled = true;
            Desbloquear();
            btnanadirdet.Enabled = true;
            txtlote.Select();
            Editar = true;
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListaLotes.Count > 0)
                {

                    ListaLotesCompleta.RemoveAll(x => x.Lot_Catalogo == txtproductocod.Text.Trim() && x.Lot_Adm_item == Adm_Item && x.Lot_item == Lot_Item);


                    ListaLotes.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());

                    dgvdatos.DataSource = null;
                    if (ListaLotes.Count > 0)
                    {
                        dgvdatos.DataSource = ListaLotes;
                        RefreshNumeral();
                    }
 
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Movimiento_Cab Det in ListaLotesCompleta)
                {
                    Det.Lot_item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btncancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (Estado_Ven_Boton == "1"){

            //    ListaLotesFinal.RemoveAll(x => x.Lot_Catalogo == txtproductocod.Text.Trim() && x.Lot_Adm_item == Adm_Item);

            //}
            //else
            //{

            //}
        }
    }
}
