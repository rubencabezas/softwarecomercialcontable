﻿namespace Comercial
{
    partial class frm_inventario_edicion_comercial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_inventario_edicion_comercial));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtalmacendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtresponsabledesc = new DevExpress.XtraEditors.TextEdit();
            this.txtresponsable = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacencod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtglosa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechadoc = new DevExpress.XtraEditors.TextEdit();
            this.txttipooperdes = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txttipoopercod = new DevExpress.XtraEditors.TextEdit();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.txtmotivodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmotivocod = new DevExpress.XtraEditors.TextEdit();
            this.txtlibro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.chklote = new System.Windows.Forms.CheckBox();
            this.txtunmdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtunmcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnsaldoinicial = new DevExpress.XtraEditors.SimpleButton();
            this.btnanadirdet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnquitardet = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Lote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtimporte = new DevExpress.XtraEditors.TextEdit();
            this.txtcantidad = new DevExpress.XtraEditors.TextEdit();
            this.txtvalorunit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.gbtransferencia = new System.Windows.Forms.GroupBox();
            this.txtalmacentrandesc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacentrancod = new DevExpress.XtraEditors.TextEdit();
            this.lblO_D = new DevExpress.XtraEditors.LabelControl();
            this.btnbuscar = new DevExpress.XtraEditors.SimpleButton();
            this.btnlistadoDocs = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsabledesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperdes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopercod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorunit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            this.gbtransferencia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacentrandesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacentrancod.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(925, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 550);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(925, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 518);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(925, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 518);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtalmacendesc);
            this.groupControl1.Controls.Add(this.txtresponsabledesc);
            this.groupControl1.Controls.Add(this.txtresponsable);
            this.groupControl1.Controls.Add(this.txtalmacencod);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.txtglosa);
            this.groupControl1.Controls.Add(this.labelControl15);
            this.groupControl1.Controls.Add(this.txtfechadoc);
            this.groupControl1.Controls.Add(this.txttipooperdes);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txttipoopercod);
            this.groupControl1.Controls.Add(this.txtserie);
            this.groupControl1.Controls.Add(this.txttipodocdesc);
            this.groupControl1.Controls.Add(this.txttipodoc);
            this.groupControl1.Controls.Add(this.txtmotivodesc);
            this.groupControl1.Controls.Add(this.txtmotivocod);
            this.groupControl1.Controls.Add(this.txtlibro);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(2, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(816, 136);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Datos Generales";
            // 
            // txtalmacendesc
            // 
            this.txtalmacendesc.Enabled = false;
            this.txtalmacendesc.EnterMoveNextControl = true;
            this.txtalmacendesc.Location = new System.Drawing.Point(254, 45);
            this.txtalmacendesc.MenuManager = this.barManager1;
            this.txtalmacendesc.Name = "txtalmacendesc";
            this.txtalmacendesc.Size = new System.Drawing.Size(220, 20);
            this.txtalmacendesc.TabIndex = 7;
            // 
            // txtresponsabledesc
            // 
            this.txtresponsabledesc.Enabled = false;
            this.txtresponsabledesc.EnterMoveNextControl = true;
            this.txtresponsabledesc.Location = new System.Drawing.Point(148, 89);
            this.txtresponsabledesc.MenuManager = this.barManager1;
            this.txtresponsabledesc.Name = "txtresponsabledesc";
            this.txtresponsabledesc.Size = new System.Drawing.Size(662, 20);
            this.txtresponsabledesc.TabIndex = 28;
            // 
            // txtresponsable
            // 
            this.txtresponsable.EnterMoveNextControl = true;
            this.txtresponsable.Location = new System.Drawing.Point(64, 89);
            this.txtresponsable.MenuManager = this.barManager1;
            this.txtresponsable.Name = "txtresponsable";
            this.txtresponsable.Size = new System.Drawing.Size(83, 20);
            this.txtresponsable.TabIndex = 27;
            this.txtresponsable.TextChanged += new System.EventHandler(this.txtresponsable_TextChanged);
            this.txtresponsable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtresponsable_KeyDown);
            // 
            // txtalmacencod
            // 
            this.txtalmacencod.EnterMoveNextControl = true;
            this.txtalmacencod.Location = new System.Drawing.Point(223, 45);
            this.txtalmacencod.MenuManager = this.barManager1;
            this.txtalmacencod.Name = "txtalmacencod";
            this.txtalmacencod.Size = new System.Drawing.Size(29, 20);
            this.txtalmacencod.TabIndex = 6;
            this.txtalmacencod.TextChanged += new System.EventHandler(this.txtalmacencod_TextChanged_1);
            this.txtalmacencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtalmacencod_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(177, 47);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(40, 13);
            this.labelControl16.TabIndex = 5;
            this.labelControl16.Text = "Almacen";
            // 
            // txtglosa
            // 
            this.txtglosa.EnterMoveNextControl = true;
            this.txtglosa.Location = new System.Drawing.Point(64, 111);
            this.txtglosa.MenuManager = this.barManager1;
            this.txtglosa.Name = "txtglosa";
            this.txtglosa.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtglosa.Size = new System.Drawing.Size(746, 20);
            this.txtglosa.TabIndex = 30;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(1, 92);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(61, 13);
            this.labelControl15.TabIndex = 26;
            this.labelControl15.Text = "Responsable";
            // 
            // txtfechadoc
            // 
            this.txtfechadoc.EnterMoveNextControl = true;
            this.txtfechadoc.Location = new System.Drawing.Point(720, 68);
            this.txtfechadoc.MenuManager = this.barManager1;
            this.txtfechadoc.Name = "txtfechadoc";
            this.txtfechadoc.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechadoc.Properties.Mask.BeepOnError = true;
            this.txtfechadoc.Properties.Mask.EditMask = "d";
            this.txtfechadoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechadoc.Properties.Mask.SaveLiteral = false;
            this.txtfechadoc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechadoc.Properties.MaxLength = 10;
            this.txtfechadoc.Size = new System.Drawing.Size(90, 20);
            this.txtfechadoc.TabIndex = 19;
            // 
            // txttipooperdes
            // 
            this.txttipooperdes.Enabled = false;
            this.txttipooperdes.EnterMoveNextControl = true;
            this.txttipooperdes.Location = new System.Drawing.Point(605, 44);
            this.txttipooperdes.MenuManager = this.barManager1;
            this.txttipooperdes.Name = "txttipooperdes";
            this.txttipooperdes.Size = new System.Drawing.Size(205, 20);
            this.txttipooperdes.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(482, 48);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(76, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Tipo Operacion:";
            // 
            // txttipoopercod
            // 
            this.txttipoopercod.EnterMoveNextControl = true;
            this.txttipoopercod.Location = new System.Drawing.Point(564, 44);
            this.txttipoopercod.MenuManager = this.barManager1;
            this.txttipoopercod.Name = "txttipoopercod";
            this.txttipoopercod.Size = new System.Drawing.Size(39, 20);
            this.txttipoopercod.TabIndex = 9;
            this.txttipoopercod.TextChanged += new System.EventHandler(this.txttipoopercod_TextChanged);
            this.txttipoopercod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipoopercod_KeyDown);
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(441, 67);
            this.txtserie.MenuManager = this.barManager1;
            this.txtserie.Name = "txtserie";
            this.txtserie.Size = new System.Drawing.Size(214, 20);
            this.txtserie.TabIndex = 15;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(96, 67);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(307, 20);
            this.txttipodocdesc.TabIndex = 13;
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(63, 67);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Size = new System.Drawing.Size(32, 20);
            this.txttipodoc.TabIndex = 12;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // txtmotivodesc
            // 
            this.txtmotivodesc.Enabled = false;
            this.txtmotivodesc.EnterMoveNextControl = true;
            this.txtmotivodesc.Location = new System.Drawing.Point(96, 45);
            this.txtmotivodesc.Name = "txtmotivodesc";
            this.txtmotivodesc.Size = new System.Drawing.Size(78, 20);
            this.txtmotivodesc.TabIndex = 4;
            // 
            // txtmotivocod
            // 
            this.txtmotivocod.Enabled = false;
            this.txtmotivocod.EnterMoveNextControl = true;
            this.txtmotivocod.Location = new System.Drawing.Point(64, 45);
            this.txtmotivocod.MenuManager = this.barManager1;
            this.txtmotivocod.Name = "txtmotivocod";
            this.txtmotivocod.Size = new System.Drawing.Size(31, 20);
            this.txtmotivocod.TabIndex = 3;
            this.txtmotivocod.TextChanged += new System.EventHandler(this.txtmotivocod_TextChanged);
            this.txtmotivocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmotivocod_KeyDown);
            // 
            // txtlibro
            // 
            this.txtlibro.Enabled = false;
            this.txtlibro.EnterMoveNextControl = true;
            this.txtlibro.Location = new System.Drawing.Point(64, 21);
            this.txtlibro.MenuManager = this.barManager1;
            this.txtlibro.Name = "txtlibro";
            this.txtlibro.Size = new System.Drawing.Size(746, 20);
            this.txtlibro.TabIndex = 1;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(31, 114);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(30, 13);
            this.labelControl10.TabIndex = 29;
            this.labelControl10.Text = "Glosa:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(657, 70);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(58, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Fecha Doc.:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(20, 69);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "T. Doc.:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(409, 70);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(28, 13);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Serie:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(25, 49);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Motivo:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(34, 24);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Libro:";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(9, 26);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(54, 13);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "Tipo B/S/A:";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(194, 27);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(47, 13);
            this.labelControl14.TabIndex = 3;
            this.labelControl14.Text = "Producto:";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.chklote);
            this.groupControl2.Controls.Add(this.txtunmdesc);
            this.groupControl2.Controls.Add(this.txtunmcod);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.btnsaldoinicial);
            this.groupControl2.Controls.Add(this.btnanadirdet);
            this.groupControl2.Controls.Add(this.btneditardet);
            this.groupControl2.Controls.Add(this.btnquitardet);
            this.groupControl2.Controls.Add(this.btnnuevodet);
            this.groupControl2.Controls.Add(this.dgvdatos);
            this.groupControl2.Controls.Add(this.txtimporte);
            this.groupControl2.Controls.Add(this.txtcantidad);
            this.groupControl2.Controls.Add(this.txtvalorunit);
            this.groupControl2.Controls.Add(this.labelControl19);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.txtproductodesc);
            this.groupControl2.Controls.Add(this.txtproductocod);
            this.groupControl2.Controls.Add(this.txttipobsadesc);
            this.groupControl2.Controls.Add(this.txttipobsacod);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Location = new System.Drawing.Point(2, 222);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(925, 328);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "Detalles:";
            this.groupControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl2_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(240)))), ((int)(((byte)(112)))));
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 283);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Falta asignar lote";
            // 
            // chklote
            // 
            this.chklote.AutoSize = true;
            this.chklote.Enabled = false;
            this.chklote.Location = new System.Drawing.Point(194, 72);
            this.chklote.Name = "chklote";
            this.chklote.Size = new System.Drawing.Size(91, 17);
            this.chklote.TabIndex = 51;
            this.chklote.Text = "Acepta lotes?";
            this.chklote.UseVisualStyleBackColor = true;
            // 
            // txtunmdesc
            // 
            this.txtunmdesc.Enabled = false;
            this.txtunmdesc.EnterMoveNextControl = true;
            this.txtunmdesc.Location = new System.Drawing.Point(111, 46);
            this.txtunmdesc.MenuManager = this.barManager1;
            this.txtunmdesc.Name = "txtunmdesc";
            this.txtunmdesc.Size = new System.Drawing.Size(211, 20);
            this.txtunmdesc.TabIndex = 8;
            // 
            // txtunmcod
            // 
            this.txtunmcod.EnterMoveNextControl = true;
            this.txtunmcod.Location = new System.Drawing.Point(67, 46);
            this.txtunmcod.MenuManager = this.barManager1;
            this.txtunmcod.Name = "txtunmcod";
            this.txtunmcod.Size = new System.Drawing.Size(43, 20);
            this.txtunmcod.TabIndex = 7;
            this.txtunmcod.TextChanged += new System.EventHandler(this.txtunmcod_TextChanged);
            this.txtunmcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtunmcod_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(35, 49);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(27, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "U.M.:";
            // 
            // btnsaldoinicial
            // 
            this.btnsaldoinicial.Location = new System.Drawing.Point(825, 24);
            this.btnsaldoinicial.Name = "btnsaldoinicial";
            this.btnsaldoinicial.Size = new System.Drawing.Size(85, 61);
            this.btnsaldoinicial.TabIndex = 15;
            this.btnsaldoinicial.Text = "Saldo Inicial";
            this.btnsaldoinicial.Visible = false;
            this.btnsaldoinicial.Click += new System.EventHandler(this.btnsaldoinicial_Click);
            // 
            // btnanadirdet
            // 
            this.btnanadirdet.Location = new System.Drawing.Point(744, 58);
            this.btnanadirdet.Name = "btnanadirdet";
            this.btnanadirdet.Size = new System.Drawing.Size(75, 28);
            this.btnanadirdet.TabIndex = 15;
            this.btnanadirdet.Text = "Añadir";
            this.btnanadirdet.Click += new System.EventHandler(this.btnanadirdet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.Location = new System.Drawing.Point(663, 56);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(75, 28);
            this.btneditardet.TabIndex = 39;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // btnquitardet
            // 
            this.btnquitardet.Location = new System.Drawing.Point(744, 23);
            this.btnquitardet.Name = "btnquitardet";
            this.btnquitardet.Size = new System.Drawing.Size(75, 28);
            this.btnquitardet.TabIndex = 38;
            this.btnquitardet.Text = "Quitar";
            this.btnquitardet.Click += new System.EventHandler(this.btnquitardet_Click);
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Location = new System.Drawing.Point(663, 23);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(75, 28);
            this.btnnuevodet.TabIndex = 0;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // dgvdatos
            // 
            this.dgvdatos.Location = new System.Drawing.Point(6, 92);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.dgvdatos.Size = new System.Drawing.Size(914, 230);
            this.dgvdatos.TabIndex = 35;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn7,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.Lote,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Item";
            this.gridColumn1.FieldName = "Id_Item";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tipo Bien";
            this.gridColumn2.FieldName = "Invd_TipoBSA_Descipcion";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 88;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Unidad de medida";
            this.gridColumn7.FieldName = "Invd_Unm_Desc";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 165;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Producto";
            this.gridColumn3.FieldName = "Invd_Catalogo_Desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 284;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Cantidad";
            this.gridColumn4.FieldName = "Invd_Cantidad";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Valor Unit";
            this.gridColumn5.FieldName = "Invd_Valor_Unit";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Total";
            this.gridColumn6.FieldName = "Invd_Total";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Invd_Total", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            // 
            // Lote
            // 
            this.Lote.Caption = "Lote";
            this.Lote.ColumnEdit = this.repositoryItemButtonEdit1;
            this.Lote.FieldName = "Acepta_lotes";
            this.Lote.Name = "Lote";
            this.Lote.Visible = true;
            this.Lote.VisibleIndex = 7;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.Click += new System.EventHandler(this.repositoryItemButtonEdit1_Click);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Invd_Catalogo";
            this.gridColumn8.FieldName = "Invd_Catalogo";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Id_Item";
            this.gridColumn9.FieldName = "Id_Item";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // txtimporte
            // 
            this.txtimporte.EnterMoveNextControl = true;
            this.txtimporte.Location = new System.Drawing.Point(67, 69);
            this.txtimporte.MenuManager = this.barManager1;
            this.txtimporte.Name = "txtimporte";
            this.txtimporte.Properties.Mask.EditMask = "n2";
            this.txtimporte.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtimporte.Size = new System.Drawing.Size(100, 20);
            this.txtimporte.TabIndex = 14;
            // 
            // txtcantidad
            // 
            this.txtcantidad.EnterMoveNextControl = true;
            this.txtcantidad.Location = new System.Drawing.Point(378, 46);
            this.txtcantidad.MenuManager = this.barManager1;
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Properties.Mask.EditMask = "n2";
            this.txtcantidad.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtcantidad.Size = new System.Drawing.Size(119, 20);
            this.txtcantidad.TabIndex = 10;
            // 
            // txtvalorunit
            // 
            this.txtvalorunit.EnterMoveNextControl = true;
            this.txtvalorunit.Location = new System.Drawing.Point(557, 46);
            this.txtvalorunit.MenuManager = this.barManager1;
            this.txtvalorunit.Name = "txtvalorunit";
            this.txtvalorunit.Properties.Mask.EditMask = "n2";
            this.txtvalorunit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtvalorunit.Size = new System.Drawing.Size(100, 20);
            this.txtvalorunit.TabIndex = 12;
            this.txtvalorunit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtvalorunit_KeyDown);
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(19, 72);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(42, 13);
            this.labelControl19.TabIndex = 13;
            this.labelControl19.Text = "Importe:";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(327, 49);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(47, 13);
            this.labelControl18.TabIndex = 9;
            this.labelControl18.Text = "Cantidad:";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(500, 49);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(50, 13);
            this.labelControl17.TabIndex = 11;
            this.labelControl17.Text = "Valor Unit.";
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(325, 23);
            this.txtproductodesc.MenuManager = this.barManager1;
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Size = new System.Drawing.Size(333, 20);
            this.txtproductodesc.TabIndex = 5;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(246, 23);
            this.txtproductocod.MenuManager = this.barManager1;
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Size = new System.Drawing.Size(76, 20);
            this.txtproductocod.TabIndex = 4;
            this.txtproductocod.TextChanged += new System.EventHandler(this.txtproductocod_TextChanged);
            this.txtproductocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproductocod_KeyDown);
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(111, 23);
            this.txttipobsadesc.MenuManager = this.barManager1;
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(75, 20);
            this.txttipobsadesc.TabIndex = 2;
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(67, 23);
            this.txttipobsacod.MenuManager = this.barManager1;
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Size = new System.Drawing.Size(43, 20);
            this.txttipobsacod.TabIndex = 1;
            this.txttipobsacod.TextChanged += new System.EventHandler(this.txttipobsacod_TextChanged);
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // gbtransferencia
            // 
            this.gbtransferencia.Controls.Add(this.txtalmacentrandesc);
            this.gbtransferencia.Controls.Add(this.txtalmacentrancod);
            this.gbtransferencia.Controls.Add(this.lblO_D);
            this.gbtransferencia.Enabled = false;
            this.gbtransferencia.Location = new System.Drawing.Point(2, 174);
            this.gbtransferencia.Name = "gbtransferencia";
            this.gbtransferencia.Size = new System.Drawing.Size(657, 45);
            this.gbtransferencia.TabIndex = 1;
            this.gbtransferencia.TabStop = false;
            this.gbtransferencia.Text = "Transferencia";
            this.gbtransferencia.Enter += new System.EventHandler(this.gbtransferencia_Enter);
            // 
            // txtalmacentrandesc
            // 
            this.txtalmacentrandesc.Enabled = false;
            this.txtalmacentrandesc.Location = new System.Drawing.Point(152, 15);
            this.txtalmacentrandesc.MenuManager = this.barManager1;
            this.txtalmacentrandesc.Name = "txtalmacentrandesc";
            this.txtalmacentrandesc.Size = new System.Drawing.Size(499, 20);
            this.txtalmacentrandesc.TabIndex = 2;
            // 
            // txtalmacentrancod
            // 
            this.txtalmacentrancod.Location = new System.Drawing.Point(104, 15);
            this.txtalmacentrancod.MenuManager = this.barManager1;
            this.txtalmacentrancod.Name = "txtalmacentrancod";
            this.txtalmacentrancod.Size = new System.Drawing.Size(45, 20);
            this.txtalmacentrancod.TabIndex = 1;
            this.txtalmacentrancod.TextChanged += new System.EventHandler(this.txtalmacentrancod_TextChanged);
            this.txtalmacentrancod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtalmacentrancod_KeyDown);
            // 
            // lblO_D
            // 
            this.lblO_D.Location = new System.Drawing.Point(15, 18);
            this.lblO_D.Name = "lblO_D";
            this.lblO_D.Size = new System.Drawing.Size(86, 13);
            this.lblO_D.TabIndex = 0;
            this.lblO_D.Text = "A. Origen/Destino";
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(7, 15);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(49, 24);
            this.btnbuscar.TabIndex = 6;
            this.btnbuscar.Text = "Buscar";
            this.btnbuscar.Click += new System.EventHandler(this.btnbuscar_Click);
            // 
            // btnlistadoDocs
            // 
            this.btnlistadoDocs.Location = new System.Drawing.Point(110, 15);
            this.btnlistadoDocs.Name = "btnlistadoDocs";
            this.btnlistadoDocs.Size = new System.Drawing.Size(49, 24);
            this.btnlistadoDocs.TabIndex = 11;
            this.btnlistadoDocs.Text = "Ver";
            this.btnlistadoDocs.Click += new System.EventHandler(this.btnlistadoDocs_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.simpleButton1);
            this.groupBox1.Controls.Add(this.btnlistadoDocs);
            this.groupBox1.Controls.Add(this.btnbuscar);
            this.groupBox1.Location = new System.Drawing.Point(665, 172);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 44);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(59, 15);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(49, 24);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "Quitar";
            // 
            // frm_inventario_edicion_comercial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(925, 550);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbtransferencia);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_inventario_edicion_comercial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Inventario";
            this.Load += new System.EventHandler(this.frm_inventario_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsabledesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresponsable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtglosa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechadoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipooperdes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipoopercod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmotivocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlibro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimporte.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcantidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorunit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            this.gbtransferencia.ResumeLayout(false);
            this.gbtransferencia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacentrandesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacentrancod.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtfechadoc;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.TextEdit txttipooperdes;
        private DevExpress.XtraEditors.TextEdit txttipoopercod;
        private DevExpress.XtraEditors.TextEdit txtmotivodesc;
        private DevExpress.XtraEditors.TextEdit txtlibro;
        private DevExpress.XtraEditors.TextEdit txtglosa;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txttipobsadesc;
        private DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.TextEdit txtproductocod;
  
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtvalorunit;
        private DevExpress.XtraEditors.TextEdit txtimporte;
        private DevExpress.XtraEditors.TextEdit txtcantidad;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraEditors.SimpleButton btnanadirdet;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.SimpleButton btnquitardet;
        private System.Windows.Forms.GroupBox gbtransferencia;
        private DevExpress.XtraEditors.TextEdit txtresponsabledesc;
        private DevExpress.XtraEditors.TextEdit txtresponsable;
        private DevExpress.XtraEditors.TextEdit txtalmacentrandesc;
        private DevExpress.XtraEditors.TextEdit txtalmacentrancod;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lblO_D;
        private DevExpress.XtraEditors.SimpleButton btnbuscar;
        private DevExpress.XtraEditors.TextEdit txtalmacendesc;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        public DevExpress.XtraEditors.TextEdit txtmotivocod;
        public DevExpress.XtraEditors.TextEdit txtalmacencod;
        private DevExpress.XtraEditors.SimpleButton btnlistadoDocs;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit txtunmdesc;
        private DevExpress.XtraEditors.TextEdit txtunmcod;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private System.Windows.Forms.CheckBox chklote;
        private DevExpress.XtraGrid.Columns.GridColumn Lote;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnsaldoinicial;
    }
}
