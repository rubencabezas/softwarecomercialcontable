﻿using Contable;
using Contable._2_Alertas;
using Contable.Comercial.Operaciones;
using DevExpress.Compression;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_inventario_comercial : frm_fuente
    {
        public frm_inventario_comercial()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            {
                        using (frm_inventario_edicion_comercial f = new frm_inventario_edicion_comercial())
                    {


                        Estado = Estados.Nuevo;
                        f.Estado_Ven_Boton = "1";
                        f.Id_Periodo = Actual_Conexion.PeriodoSelect;

                        if (f.ShowDialog() == DialogResult.OK)
                        {


                            try
                            {
                                Listar();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                        }else
                        {
                            Listar();
                        }

                    }
            }

        }


        Entidad_Movimiento_Inventario Entidad = new Entidad_Movimiento_Inventario();
        public List<Entidad_Movimiento_Inventario> Lista = new List<Entidad_Movimiento_Inventario>();
        public void Listar()
        {
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
            Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.Id_Tipo_Mov = "0034";//INGRESOS NO MA
            Ent.Id_Almacen = null;
            Ent.Id_Movimiento = null;
            Ent.Inv_Es_Venta_Sin_Ticket = false;
            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_inventario_Load(object sender, EventArgs e)
        {
            Listar();
        }


        private void gridView1_Click(object sender, EventArgs e)
        {

            if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
            {
                Estado = Estados.Ninguno;
                Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_inventario_edicion_comercial f = new frm_inventario_edicion_comercial())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Tipo_Mov = Entidad.Id_Tipo_Mov;
                f.Id_Almacen = Entidad.Id_Almacen;
                f.Id_Movimiento=Entidad.Id_Movimiento;
                f.txtmotivocod.Enabled = false;
                f.txtalmacencod.Enabled = false;
                f.Se_Genero_En_Compras = Entidad.Inv_Generado_En_Compra;
                f.Es_traspaso = Entidad.Inv_Es_Traspaso;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        private void btnimprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Inventario" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (Entidad.Inv_Generado_En_Compra == false)
            {
                if (Entidad.Inv_Es_Traspaso == false)
                {
                            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Almacen_Desc + " " + Entidad.Id_Tipo_Mov_Desc + " " + Entidad.Id_Movimiento ;
                            if (Accion.ShowDeleted(DocDelted))
                            {
                                try
                                {
                                    Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Entidad.Id_Empresa,
                                        Id_Anio = Entidad.Id_Anio,
                                        Id_Periodo = Entidad.Id_Periodo,
                                        Id_Tipo_Mov = Entidad.Id_Tipo_Mov,
                                        Id_Almacen = Entidad.Id_Almacen,
                                        Id_Movimiento=Entidad.Id_Movimiento
                                    };

                                    Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

                                   if (Entidad.Id_Tipo_Operacion == "09")
                                    {
                                        log.Eliminar_Adm_Ingresos_Inv_Libera_Trabsaferencia(Ent);
                                        Accion.ExitoGuardar();
                                    }
                                    else
                                    {
                                        log.Eliminar_Adm_Ingresos_Inv(Ent);
                                        Accion.ExitoGuardar();
                                    }


                                    Listar();

                                }
                                catch (Exception ex)
                                {
                                    Accion.ErrorSistema(ex.Message);
                                }

                            }
                }
                else
                {
                    Accion.Advertencia("Este registro se generó desde traspasos,para eliminar , debe hacerlo desde el traspaso.");
                }

            }
            else
            {
                Accion.Advertencia("Este registro se generó desde compras,para eliminar , debe hacerlo desde el registro de compras.");
            }


        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (frm_rango_exportar_compras_comercial ff = new frm_rango_exportar_compras_comercial())
                {

                    if (ff.ShowDialog() == DialogResult.OK)
                    {


                        System.Threading.Thread p = new System.Threading.Thread((this.Progreso));
                        try
                        {

                            p.Start();


                            Logica_Exportar_Importar log = new Logica_Exportar_Importar();
                            Entidad_Exportar entidad = new Entidad_Exportar();
                            entidad.Empresa = Actual_Conexion.CodigoEmpresa;
                            entidad.desde = Convert.ToDateTime(ff.txtdesde.Text);
                            entidad.hasta = Convert.ToDateTime(ff.txthasta.Text);

                            //CATALOGO
                            DataSet CTB_MOVIMIENTO_INVENTARIO_CAB = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_DET = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_LOTE = new DataSet();
                    
                            CTB_MOVIMIENTO_INVENTARIO_CAB = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_DET = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_LOTE = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(entidad);
                          


                            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                            string Nombre = "INVENTARIO";// + EstCod.ToUpper.Trim + " " + FechaServer();
                            string Ruta;
                            Ruta = desktopPath + @"\" + Nombre;

                            File.Delete(Ruta + ".rar");
                            if (!Directory.Exists(Ruta))
                            {
                                Directory.CreateDirectory(Ruta);
                            }
                            else
                            {
                                foreach (var f in Directory.GetFiles(Ruta))
                                    File.Delete(Conversions.ToString(f));
                            }
 
                            string inventario_cab = "CTB_MOVIMIENTO_INVENTARIO_CAB";
                            string inventario_det = "CTB_MOVIMIENTO_INVENTARIO_DET";
                            string inventario_lote = "CTB_MOVIMIENTO_INVENTARIO_LOTE";
              

                            File.WriteAllText(Ruta + @"\" + inventario_cab + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_CAB.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_det + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_DET.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_lote + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_LOTE.Tables[0]));
                        
                            var files = Directory.GetFiles(Ruta);
                            string Password = Actual_Conexion.RucEmpresa.Trim();
                            Comprimir(files, Ruta, Accion.Encriptar(Password));



                            p.Abort();
                        }
                        catch (Exception ex)
                        {
                            p.Abort();
                            Accion.ErrorSistema(ex.Message);
                        }




                    }


                }
            }
            catch (Exception ex)
            {

            }
        }


        public void Progreso()
        {
            WaitForm1 dlg = new WaitForm1();
            dlg.ShowDialog();
        }


        public static void Comprimir(string[] Archivos, string Ruta, string password)
        {
            using (var archive = new ZipArchive())
            {
                foreach (string file in Archivos)
                {
                    ZipFileItem zipFI = archive.AddFile(file, "/");
                    zipFI.EncryptionType = EncryptionType.Aes128;
                    zipFI.Password = password;
                }

                archive.Save(Ruta + ".rar");
                // File.Move(Ruta + ".rar", Ruta + ".xml");
                archive.Dispose();
            }
        }


        private static string ConvertToCSV(DataTable oDt)
        {
            var sb = new StringBuilder();
            var columnNames = oDt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
            sb.AppendLine(string.Join("|", columnNames));
            foreach (DataRow row in oDt.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                sb.AppendLine(string.Join("|", fields));
            }

            return sb.ToString();
        }

        string RutaArchivo = "";

        private void btnimportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {


                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "ZIP Folder (.rar)|*.rar";
                openFileDialog1.ShowDialog();
                openFileDialog1.Multiselect = true;
                RutaArchivo = openFileDialog1.FileName;

                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Nombre = Path.GetFileNameWithoutExtension(RutaArchivo);
                string Destino;
                Destino = desktopPath + @"\" + Nombre;

                p.Start();

                if (!Directory.Exists(Destino))
                {
                    Directory.CreateDirectory(Destino);
                }

                if (Directory.Exists(Destino))
                {
                    foreach (var f in Directory.GetFiles(Destino))

                        File.Delete(Conversions.ToString(f));
                    string Password = Actual_Conexion.RucEmpresa.Trim();
                    if (!Descomprimir(RutaArchivo, Destino, Accion.Encriptar(Password)))
                    {
                        Directory.Delete(Destino, true);
                    }
                }


                if (Directory.Exists(Destino))
                {
                    // Aki empesamos a importar los archivos
                    Importar_(Destino);

                }

                p.Abort();
            }
            catch (Exception ex)
            {
                p.Abort();
                Accion.ErrorSistema(ex.Message);
            }
        }



        public void Importar_(string Destino)
        {
            
            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_CAB.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_CAB.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DET.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DET.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_LOTE.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_LOTE.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(Dt);
            }

            



        }


        private static DataTable GetDataTableFromCsv(string path__1, bool isFirstRowHeader)
        {
            var csvData = new DataTable();
            using (var csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(path__1))
            {
                csvReader.SetDelimiters(new string[] { "|" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                var colFields = csvReader.ReadFields();
                foreach (var Column in colFields)
                {
                    var datecolumn = new DataColumn(Column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }

                int Cont = 2;
                while (!csvReader.EndOfData)
                {
                    var fieldData = csvReader.ReadFields();
                    // Making empty value as null
                    if (fieldData.Count() != csvData.Columns.Count)
                    {
                        Accion.Advertencia("ARCHIVO CON FALLAS,El archivo " + Path.GetFileName(path__1) + " En la linea " + Cont + " la cantidad de columnas de cabecera es " + csvData.Columns.Count + " la fila presenta " + fieldData.Count());
                    }

                    for (int i = 0, loopTo = fieldData.Length - 1; i <= loopTo; i++)
                    {
                        if (string.IsNullOrEmpty(fieldData[i]))
                        {
                            fieldData[i] = null;
                        }
                    }

                    csvData.Rows.Add(fieldData);
                    Cont = Cont + 1;
                }
            }

            return csvData;
        }


        public static bool Descomprimir(string ArchivoRar, string RutaDescomprimir, string password)
        {
            using (ZipArchive archive = ZipArchive.Read(ArchivoRar))
            {
                foreach (ZipItem item in archive)
                {
                    try
                    {
                        item.Password = password;
                        item.Extract(RutaDescomprimir);
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Listar();
        }
    }
}
