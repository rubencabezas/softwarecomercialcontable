﻿using Contable;
using Contable._2_Alertas;
using Contable.Comercial.Operaciones;
using DevExpress.Compression;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_compras_comercial : frm_fuente
    {
        public frm_compras_comercial()
        {
            InitializeComponent();
        }



        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

      


            if (Accion.Valida_Anio_Actual_Ejercicio(Actual_Conexion.CodigoEmpresa, Actual_Conexion.AnioSelect))
            {
                    using (frm_compras_edicion_comercial f = new frm_compras_edicion_comercial())
                    {


                        Estado = Estados.Nuevo;
                        f.Estado_Ven_Boton = "1";
                        f.Id_Periodo = Actual_Conexion.PeriodoSelect;

                        if (f.ShowDialog() == DialogResult.OK)
                        {


                            try
                            {
                                Listar();
                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }
                        }
                        else
                        {
                            Listar();
                        }

                    }
            }
            
            


        }

        private void frm_compras_Load(object sender, EventArgs e)
        {
            TraerLibro();
            Listar();
        }

        

        string Id_Libro;
        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Compra(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_Compra;

                }
                else
                {
                    Accion.Advertencia("Debe configurar libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;// Actual_Conexion.PeriodoSelect;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;
            try
            {
                Lista = log.Listar_adm_Logistica(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_compras_edicion_comercial f = new frm_compras_edicion_comercial())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Libro = Entidad.Id_Libro;
                f.Voucher = Entidad.Id_Voucher;
          

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

        private void bandedGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & bandedGridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;

                    Entidad = Lista[bandedGridView1.GetFocusedDataSourceRowIndex()];
                    //frm_principal.cb
                    //frm_principal f = new frm_principal();
                    //f.comboperiodo.ValueMember = Entidad.Id_Periodo;
                    //f.comboperiodo.DisplayMember = Entidad.Id_Periodo_Desc;

                    //comboperiodo.ValueMember = "Id_Periodo";
                    //comboperiodo.DisplayMember = "Descripcion_Periodo";
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Anio = Entidad.Id_Anio,
                        Id_Periodo = Entidad.Id_Periodo,
                        Id_Voucher = Entidad.Id_Voucher,
                        Id_Libro = Entidad.Id_Libro
                    };

                    Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

                    log.Eliminar_Adm_Compra(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void btnanular_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ANULAR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Anio = Entidad.Id_Anio,
                        Id_Periodo = Entidad.Id_Periodo,
                        Id_Voucher = Entidad.Id_Voucher,
                        Id_Libro = Entidad.Id_Libro
                    };

                    Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

                    log.Anular_Adm_Compra(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void btnrevertiranulado_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a REVERTIR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Anio = Entidad.Id_Anio,
                        Id_Periodo = Entidad.Id_Periodo,
                        Id_Voucher = Entidad.Id_Voucher,
                        Id_Libro = Entidad.Id_Libro
                    };

                    Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

                    log.Revertir(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Compras" + ".Xlsx");
                bandedGridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnexportardatos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using(frm_rango_exportar_compras_comercial ff = new frm_rango_exportar_compras_comercial())
                {

                    if (ff.ShowDialog() == DialogResult.OK)
                    {


                        System.Threading.Thread p = new System.Threading.Thread((this.Progreso));
                        try
                        {

                            p.Start();


                            Logica_Exportar_Importar log = new Logica_Exportar_Importar();
                            Entidad_Exportar entidad = new Entidad_Exportar();
                            entidad.Empresa = Actual_Conexion.CodigoEmpresa;
                            entidad.desde = Convert.ToDateTime(ff.txtdesde.Text);
                            entidad.hasta = Convert.ToDateTime(ff.txthasta.Text);

                            //CATALOGO
                            DataSet GNL_ENTIDAD = new DataSet();
                            DataSet CTB_MOVIMIENTO_CONTABLE_CAB = new DataSet();
                            DataSet CTB_MOVIMIENTO_ADM_CONTABLE_DET = new DataSet();
                            DataSet CTB_MOVIMIENTO_ADM_COMPRA_LOTE = new DataSet();
                            DataSet CTB_MOVIMIENTO_CONTABLE_DET = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_CAB = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_DET = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_LOTE = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA = new DataSet();

                            GNL_ENTIDAD = log.EXPORTAR_GNL_ENTIDAD_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_CONTABLE_CAB = log.EXPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_ADM_CONTABLE_DET = log.EXPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_ADM_COMPRA_LOTE = log.EXPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_CONTABLE_DET = log.EXPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_CAB = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_DET = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_LOTE = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(entidad);



                            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                            string Nombre = "COMPRAS";// + EstCod.ToUpper.Trim + " " + FechaServer();
                            string Ruta;
                            Ruta = desktopPath + @"\" + Nombre;

                            File.Delete(Ruta + ".rar");
                            if (!Directory.Exists(Ruta))
                            {
                                Directory.CreateDirectory(Ruta);
                            }
                            else
                            {
                                foreach (var f in Directory.GetFiles(Ruta))
                                    File.Delete(Conversions.ToString(f));
                            }

                            string gen_entidad = "GNL_ENTIDAD";
                            string movimiento_cab = "CTB_MOVIMIENTO_CONTABLE_CAB";
                            string adm_movimiento = "CTB_MOVIMIENTO_ADM_CONTABLE_DET";
                            string adm_movimiento_lote = "CTB_MOVIMIENTO_ADM_COMPRA_LOTE";
                            string adm_movimiento_det = "CTB_MOVIMIENTO_CONTABLE_DET";
                            string inventario_cab = "CTB_MOVIMIENTO_INVENTARIO_CAB";
                            string inventario_det = "CTB_MOVIMIENTO_INVENTARIO_DET";
                            string inventario_lote = "CTB_MOVIMIENTO_INVENTARIO_LOTE";
                            string inventario_doc_ref = "CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA";

                            File.WriteAllText(Ruta + @"\" + gen_entidad + ".csv", ConvertToCSV(GNL_ENTIDAD.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + movimiento_cab + ".csv", ConvertToCSV(CTB_MOVIMIENTO_CONTABLE_CAB.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + adm_movimiento + ".csv", ConvertToCSV(CTB_MOVIMIENTO_ADM_CONTABLE_DET.Tables[0]));

                            File.WriteAllText(Ruta + @"\" + adm_movimiento_lote + ".csv", ConvertToCSV(CTB_MOVIMIENTO_ADM_COMPRA_LOTE.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + adm_movimiento_det + ".csv", ConvertToCSV(CTB_MOVIMIENTO_CONTABLE_DET.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_cab + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_CAB.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_det + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_DET.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_lote + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_LOTE.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_doc_ref + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA.Tables[0]));

                            var files = Directory.GetFiles(Ruta);
                            string Password = Actual_Conexion.RucEmpresa.Trim();
                            Comprimir(files, Ruta, Accion.Encriptar(Password));



                            p.Abort();
                        }
                        catch (Exception ex)
                        {
                            p.Abort();
                            Accion.ErrorSistema(ex.Message);
                        }




                    }


                }
            }catch(Exception ex)
            {

            }
        }


        public void Progreso()
        {
            WaitForm1 dlg = new WaitForm1();
            dlg.ShowDialog();
        }


        public static void Comprimir(string[] Archivos, string Ruta, string password)
        {
            using (var archive = new ZipArchive())
            {
                foreach (string file in Archivos)
                {
                    ZipFileItem zipFI = archive.AddFile(file, "/");
                    zipFI.EncryptionType = EncryptionType.Aes128;
                    zipFI.Password = password;
                }

                archive.Save(Ruta + ".rar");
                // File.Move(Ruta + ".rar", Ruta + ".xml");
                archive.Dispose();
            }
        }


        private static string ConvertToCSV(DataTable oDt)
        {
            var sb = new StringBuilder();
            var columnNames = oDt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
            sb.AppendLine(string.Join("|", columnNames));
            foreach (DataRow row in oDt.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                sb.AppendLine(string.Join("|", fields));
            }

            return sb.ToString();
        }

        string RutaArchivo = "";
        private void btnimportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {


                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "ZIP Folder (.rar)|*.rar";
                openFileDialog1.ShowDialog();
                openFileDialog1.Multiselect = true;
                RutaArchivo = openFileDialog1.FileName;

                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Nombre = Path.GetFileNameWithoutExtension(RutaArchivo);
                string Destino;
                Destino = desktopPath + @"\" + Nombre;

                p.Start();

                if (!Directory.Exists(Destino))
                {
                    Directory.CreateDirectory(Destino);
                }

                if (Directory.Exists(Destino))
                {
                    foreach (var f in Directory.GetFiles(Destino))

                        File.Delete(Conversions.ToString(f));
                    string Password = Actual_Conexion.RucEmpresa.Trim();
                    if (!Descomprimir(RutaArchivo, Destino, Accion.Encriptar(Password)))
                    {
                        Directory.Delete(Destino, true);
                    }
                }


                if (Directory.Exists(Destino))
                {
                    // Aki empesamos a importar los archivos
                    Importar_Compras(Destino);

                }

                p.Abort();
            }
            catch (Exception ex)
            {
                 p.Abort();
                Accion.ErrorSistema(ex.Message);
            }
        }



        public void Importar_Compras(string Destino)
        {
            if (File.Exists(Destino + @"\" + "GNL_ENTIDAD.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "GNL_ENTIDAD.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_GNL_ENTIDAD_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_CONTABLE_CAB.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_CONTABLE_CAB.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_ADM_CONTABLE_DET.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_ADM_CONTABLE_DET.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_ADM_COMPRA_LOTE.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_ADM_COMPRA_LOTE.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_CONTABLE_DET.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_CONTABLE_DET.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_CAB.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_CAB.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DET.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DET.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_LOTE.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_LOTE.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(Dt);
            }



        }


        private static DataTable GetDataTableFromCsv(string path__1, bool isFirstRowHeader)
        {
            var csvData = new DataTable();
            using (var csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(path__1))
            {
                csvReader.SetDelimiters(new string[] { "|" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                var colFields = csvReader.ReadFields();
                foreach (var Column in colFields)
                {
                    var datecolumn = new DataColumn(Column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }

                int Cont = 2;
                while (!csvReader.EndOfData)
                {
                    var fieldData = csvReader.ReadFields();
                    // Making empty value as null
                    if (fieldData.Count() != csvData.Columns.Count)
                    {
                        Accion.Advertencia("ARCHIVO CON FALLAS,El archivo " + Path.GetFileName(path__1) + " En la linea " + Cont + " la cantidad de columnas de cabecera es " + csvData.Columns.Count + " la fila presenta " + fieldData.Count());
                    }

                    for (int i = 0, loopTo = fieldData.Length - 1; i <= loopTo; i++)
                    {
                        if (string.IsNullOrEmpty(fieldData[i]))
                        {
                            fieldData[i] = null;
                        }
                    }

                    csvData.Rows.Add(fieldData);
                    Cont = Cont + 1;
                }
            }

            return csvData;
        }


        public static bool Descomprimir(string ArchivoRar, string RutaDescomprimir, string password)
        {
            using (ZipArchive archive = ZipArchive.Read(ArchivoRar))
            {
                foreach (ZipItem item in archive)
                {
                    try
                    {
                        item.Password = password;
                        item.Extract(RutaDescomprimir);
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void bntmactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TraerLibro();
            Listar();
        }
    }
}
