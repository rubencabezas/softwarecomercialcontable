﻿using Contable;
using Contable._1_Busquedas_Generales;
using DevExpress.Utils.Extensions;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_compras_edicion_comercial : frm_fuente
    {
        public frm_compras_edicion_comercial()
        {
            InitializeComponent();
        }


        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;
        public bool Es_traspaso=false;

        public List<Entidad_Movimiento_Cab> Detalles_ADM = new List<Entidad_Movimiento_Cab>();

        //string Igv_Tasa_texto;
        //decimal Igv_Porcentaje;
        //string Igv_TasaImpresion;

        string Id_Analisis;
        List<Entidad_Movimiento_Cab> Detalles_CONT = new List<Entidad_Movimiento_Cab>();

        public void ResetearImportes()
        {
            txtbaseimponible.Text = "0.00";
            txtigv.Text = "0.00";
            txtimporteisc.Text = "0.00";
            txtotrostribuimporte.Text = "0.00";
            txtvadquinograba.Text = "0.00";
            txtimportetotal.Text = "0.00";
            chkisc.Checked = false;
            chkotrostri.Checked = false;

        }

        public void LimpiarCab()
        {
            txtlibro.ResetText();
            txtglosa.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();

            txtfechadoc.ResetText();
            txtrucdni.ResetText();
            txtentidad.ResetText();
            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txttipocambiocod.ResetText();
            txttipocambiodesc.ResetText();

            txtpdc.ResetText();
            txtpdcdesc.ResetText();
            txtfechavencimiento.ResetText();




        }

        public void LimpiarDet2()
        {
 
            txtimporte.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
            txtalmacencod.ResetText();
            txtalmacendesc.ResetText();
            txtvalorunit.ResetText();
            txtcantidad.ResetText();
            txtunmcod.ResetText();
            txtunmdesc.ResetText();

            chklote.Checked = false;
            btnlotes.Enabled = false;
        }

        public void HabilitarDetalles()
        {
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtimporte.Enabled = true;
            txtalmacencod.Enabled = true;
            txtvalorunit.Enabled = true;
            txtcantidad.Enabled = true;
            txtunmcod.Enabled = true;
            txttipobsacod.Focus();
        }

        public override void CambiandoEstado()
        {

            if (Estado == Estados.Nuevo)
            {
                //Botones
                LimpiarCab();


                Detalles_ADM.Clear();
                Dgvdetalles.DataSource = null;

                TraerLibro();
                BuscarMoneda_Inicial();
                ResetearImportes();

            }
            else if (Estado == Estados.Ninguno)
            {
                //Botones

                EstadoDetalle = Estados.Ninguno;
                EstadoDetalle2 = Estados.Ninguno;

            }
            else if (Estado == Estados.Modificar)
            {
                //Botones


            }
            else if (Estado == Estados.Guardado)
            {
                //Botones

            }
            else if (Estado == Estados.SoloLectura)
            {

            }
            else if (Estado == Estados.Consulta)
            {

            }
        }

        int Adm_Item;
        public override void CambiandoEstadoDetalle2()
        {
            if (EstadoDetalle2 == Estados.Nuevo)
            {
                HabilitarDetalles2();
                LimpiarDet2();
                Adm_Item = Detalles_ADM.Count + 1;

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Ninguno)
            {
                LimpiarDet2();
                BloquearDetalles2();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Modificar)
            {

                HabilitarDetalles2();


                btnanadirdet.Enabled = true;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = true;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Guardado)
            {

                LimpiarDet2();
                BloquearDetalles2();
                btnanadirdet.Focus();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = true;
                btnnuevodet.Enabled = true;

            }
            else if (EstadoDetalle2 == Estados.Consulta)
            {
                HabilitarDetalles2();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = false;

            }
            else if (EstadoDetalle2 == Estados.SoloLectura)
            {
                BloquearDetalles2();

                btnanadirdet.Enabled = false;
                btneditardet.Enabled = true;
                btnquitardet.Enabled = false;
                btnnuevodet.Enabled = false;


            }
        }


        public void BloquearDetalles2()
        {
            txttipobsacod.Enabled = false;
            txtproductocod.Enabled = false;
            txtalmacencod.Enabled = false;
            txtcantidad.Enabled = false;
            txtvalorunit.Enabled = false;
            txtimporte.Enabled = false;
            txtunmcod.Enabled = false;
            txttipooperacion.Enabled = false;
        }

        public void HabilitarDetalles2()
        {
            txttipobsacod.Enabled = true;
            txtproductocod.Enabled = true;
            txtalmacencod.Enabled = true;
            txtcantidad.Enabled = true;
            txtvalorunit.Enabled = true;
            txtimporte.Enabled = true;
            txtunmcod.Enabled = true;
            txttipooperacion.Enabled = true;
        }

        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Compra(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    txtlibro.Tag = Generales[0].Ini_Compra;
                    txtlibro.Text = Generales[0].Ini_Compra_Desc;
                }
                else
                {
                    Accion.Advertencia("Debe configurar un libro contable para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        void ActualizaIGV()
        {
            try
            {
                if ((IsDate(txtfechadoc.Text) == true))
                {
                    if ((DateTime.Parse(txtfechadoc.Text).Year > 2000))
                    {
                        Entidad_Impuesto_Det ent = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        ent.Imd_Fecha_Inicio = Convert.ToDateTime(txtfechadoc.Text);

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(ent);
                        if ((Lista.Count > 0))
                        {
                            txtigvporcentaje.Tag = Lista[0].Imd_Tasa;//0.18
                            txtigvporcentaje.Text = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));//18.00

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        public void BuscarMoneda_Inicial()
        {
            try
            {

                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = "1"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == "1")
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;

                        }

                    }
                    VerificarMoneda();
                    //txtglosa.Focus();
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void txtserie_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtserie.Text))
                {
                    string Serie_Numero = txtserie.Text.Trim();

                    if (Serie_Numero.Contains("-") == true)
                    {
                        string SerNum = txtserie.Text;
                        string[] Datos = SerNum.Split(Convert.ToChar("-"));
                        //string DataVar;
                        txtserie.Text = Accion.Formato(Datos[0].Trim(), 4).ToString() + "-" + Accion.Formato(Datos[1].Trim(), 8).ToString();

                        //txtserie.Text = Accion.Formato(txtserie.Text, 4);
                        txtserie.EnterMoveNextControl = true;
                    }
                    else
                    {
                        Accion.Advertencia("Se debe separar por un '-' para poder registrar la serie y numero");
                        txtserie.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_moneda_busqueda f = new frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                Es_moneda_nac = Entidad.Es_nacional;
                                txtmonedacod.Tag = Entidad.Id_Moneda.Trim();

                                txtmonedacod.Text = Entidad.Id_Sunat.Trim();
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;

                                VerificarMoneda();
                                Traer_Venta_Publicacion();

                                //txtmonedacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();
                        VerificarMoneda();
                        Traer_Venta_Publicacion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = txtmonedacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == txtmonedacod.Text.Trim().ToUpper())
                        {
                            Es_moneda_nac = T.Es_nacional;
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public bool Es_moneda_nac;

        public void VerificarMoneda()
        {
            if (!string.IsNullOrEmpty(txtmonedacod.Text.Trim()))
            {

                if (Es_moneda_nac == true)
                {
                    txttipocambiocod.Enabled = false;
                    txttipocambiocod.Text = "SCV";
                    txttipocambiodesc.Text = "SIN CONVERSION";
                    txttipocambiovalor.Enabled = false;
                    txttipocambiovalor.Text = "1.000";

                }
                else
                {
                    Traer_Venta_Publicacion();
                }
            }
        }

        void Traer_Venta_Publicacion()
        {
            try
            {
                if (txtmonedacod.Text == "2")
                {
                    txttipocambiocod.Text = "VEP";
                    txttipocambiodesc.Text = "VENTA PUBLICACION";
                    txttipocambiovalor.Enabled = true;
                    txttipocambiocod.Enabled = false;
                    //txttipocambiovalor.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtrucdni_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtrucdni.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtrucdni.Text.Substring(txtrucdni.Text.Length - 1, 1) == "*")
                    {
                        using (frm_entidades_busqueda f = new frm_entidades_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Entidad Entidad = new Entidad_Entidad();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtrucdni.Text = Entidad.Ent_RUC_DNI;
                                txtentidad.Text = Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Ape_Materno + " " + Entidad.Ent_Razon_Social_Nombre;
                                txtrucdni.EnterMoveNextControl = true;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtrucdni.Text) & string.IsNullOrEmpty(txtentidad.Text))
                    {
                        BuscarEntidad();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public void BuscarEntidad()
        {
            try
            {
                //txtrucdni.Text = Accion.Formato(txt.Text, 3);
                Logica_Entidad log = new Logica_Entidad();

                List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

                Generales = log.Listar(new Entidad_Entidad
                {
                    Ent_RUC_DNI = txtrucdni.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Entidad T in Generales)
                    {
                        if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                        {
                            txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                            txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                            txtrucdni.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    try
                    {
                            using (frm_entidad_edicion_comercial f = new frm_entidad_edicion_comercial())
                            {

                                f.txtnumero.Text = txtrucdni.Text;

                                f.Estado_Ven_Boton = "1";
                                if (f.ShowDialog(this) == DialogResult.OK)
                                {

                                }
                                else
                                {
                                    Logica_Entidad logi = new Logica_Entidad();

                                    List<Entidad_Entidad> Generalesi = new List<Entidad_Entidad>();

                                    Generalesi = logi.Listar(new Entidad_Entidad
                                    {
                                        Ent_RUC_DNI = txtrucdni.Text
                                    });

                                    if (Generalesi.Count > 0)
                                    {

                                        foreach (Entidad_Entidad T in Generalesi)
                                        {
                                            if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == txtrucdni.Text.Trim().ToUpper())
                                            {
                                                txtrucdni.Text = (T.Ent_RUC_DNI).ToString().Trim();
                                                txtentidad.Text = T.Ent_Ape_Paterno + " " + T.Ent_Ape_Materno + " " + T.Ent_Razon_Social_Nombre;
                                                txtrucdni.EnterMoveNextControl = true;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        txtrucdni.EnterMoveNextControl = false;
                                        txtrucdni.ResetText();
                                        txtrucdni.Focus();
                                    }
                                }
                            }
                    }catch(Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtfechavencimiento_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //0006    01  CONTADO
                    //0007    02  CREDITO

                    if (txtfechavencimiento.Text == "01/01/1900" || txtfechavencimiento.Text == "01/01/0001" || txtfechavencimiento.Text != "")
                    {
                        BuscarCondicion_Por_Fecha("02");
                    }
                    else
                    {
                        BuscarCondicion_Por_Fecha("01");
                    }
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarCondicion_Por_Fecha(string Codigo)
        {
            try
            {

                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = Codigo
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == Codigo)
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");

                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_compras_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                EstadoDetalle2 = Estados.Ninguno;

                LimpiarCab();
                Detalles_ADM.Clear();
                Dgvdetalles.DataSource = null;
                TraerLibro();
                BuscarMoneda_Inicial();
                ResetearImportes();
                BloquearDetalles2();
                txtpdc.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                TraerLibro();
                BloquearDetalles2();
                ListarModificar();

            }

        }

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            try
            {
                Lista_Modificar = log.Listar_adm(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = txtlibro.Tag.ToString();
                    Voucher = Enti.Id_Voucher;
                  
                   txtpdc.Text = Enti.ctb_pdc_codigo;
                   txtpdcdesc.Text  =Enti.ctb_pdc_Nombre;

                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;


                    txtserie.Text = Enti.Ctb_Serie + "-" + Enti.Ctb_Numero;

                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));

                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtcondicioncod.Tag = Enti.Ctb_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ctb_Condicion_Interno;
                    txtcondiciondesc.Text = Enti.Ctb_Condicion_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();

                    Es_moneda_nac = Enti.Ctb_Es_moneda_nac;



                    //MBase1 = Enti.Ctb_Base_Imponible;
                    //MBase2 = Enti.Ctb_Base_Imponible2;
                    //MBase3 = Enti.Ctb_Base_Imponible3;
                    //MIgv1 = Enti.Ctb_Igv;
                    //MIgv2 = Enti.Ctb_Igv2;
                    //MIgv3 = Enti.Ctb_Igv3;
                    //MValorAdqNoGrav = Enti.Ctb_No_Gravadas;
                    //MIsc = Enti.Ctb_Isc_Importe;
                    //MOtrosTributos = Enti.Ctb_Otros_Tributos_Importe;
                    //MImporteTotal = Enti.Ctb_Importe_Total;

                    txtbaseimponible.Text = Convert.ToDecimal(Enti.Ctb_Base_Imponible).ToString("0.00");
                    txtigv.Text = Convert.ToDecimal(Enti.Ctb_Igv).ToString("0.00");
                    txtimportetotal.Text = Convert.ToDecimal(Enti.Ctb_Importe_Total).ToString("0.00");

                    //txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");
                    //txtotrostribuimporte.Text = Convert.ToDecimal(MOtrosTributos).ToString("0.00");
                    //txtvadquinograba.Text = Convert.ToDecimal(MValorAdqNoGrav).ToString("0.00");
                    





                    //txtidanalisis.Text = Enti.Ctb_Analisis;
                    //txtanalisisdesc.Text = Enti.Ctb_Analisis_Desc;

                    if (String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/0001" || String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/1900")
                    {
                        txtfechavencimiento.Text = "";
                    }
                    else

                    {


                        txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Vencimiento);
                    }



                    //listar cnotable
                    Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                    Dgvdetalles.DataSource = null;
                    Detalles_ADM = log_det_con.Listar_Det_Administrativo(Enti);
                    if (Detalles_ADM.Count > 0)
                    {
                        Dgvdetalles.DataSource = Detalles_ADM;
                    }

                    //listar cnotable
                    Logica_Movimiento_Cab log_det_contable = new Logica_Movimiento_Cab();
   
                    Detalles_CONT = log_det_contable.Listar_Det(Enti);

                    ListaLotesFinal =log_det_contable.Listar_Lotes_Compra(Enti);

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        public bool VerificarCabecera()
        {

            if (string.IsNullOrEmpty(txtpdcdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un punto de compra");
                txtglosa.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una glosa");
                txtglosa.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtfechadoc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtfechadoc.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtentidad.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un proveedor");
                txtentidad.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtcondiciondesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una condicion");
                txtcondicioncod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtmonedadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una moneda");
                txtmonedacod.Focus();
                return false;
            }


            if (Detalles_ADM.Count == 0)
            {
                Accion.Advertencia("No existe ningun detalle");
                return false;
            }

            //MessageBox.Show(Convert.ToString(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Debe"].SummaryItem.SummaryValue)));
            //if (Math.Round(Convert.ToDouble(gridView2.Columns["Ctb_Importe_Debe"].SummaryItem.SummaryValue), 2) != Math.Round(Convert.ToDouble(gridView1.Columns["Ctb_Importe_Haber"].SummaryItem.SummaryValue), 2))
            //{
            //    Accion.Advertencia("Los Montos Totales del Debe y el Haber no son iguales");
            //    return false;
            //}



            //Validar si el detalle que esta afecto a Lote tiene un registro

            if (Detalles_ADM.Count > 0)
            {

                for (int i = 0; (i <= (Detalles_ADM.Count - 1)); i++)
                {
                    if (Detalles_ADM[i].Acepta_lotes == true)
                    {
                        if (ListaLotesFinal.Count > 0)
                        {

                          
                            List<Entidad_Movimiento_Cab> ListaLotes = new List<Entidad_Movimiento_Cab>();

                            var PrivView = from item in ListaLotesFinal
                                           where item.Lot_Catalogo == Detalles_ADM[i].Adm_Catalogo && item.Lot_Adm_item == Detalles_ADM[i].Adm_Item
                                           orderby item.Id_Anio descending
                                           select item;

                            ListaLotes = PrivView.ToList();


                            if (PrivView.Count() > 0)
                            {
                              if (ListaLotes[0].Lot_Catalogo == Detalles_ADM[i].Adm_Catalogo && ListaLotes[0].Lot_Adm_item == Detalles_ADM[i].Adm_Item)
                                {
                                    if (Detalles_ADM[i].Adm_Cantidad != ListaLotes[0].Lot_Cantidad)
                                    {
                                        Accion.Advertencia("Las cantidades para el producto:" + Detalles_ADM[i].Adm_Catalogo_Desc + " en el registro de lotes deben ser iguales");
                                        return false;
                                    }
                                }
                            }
                            else
                            {
                                Accion.Advertencia("Uno de los productos acepta el registro con lotes,verificar y llenar su lote correspondiente.");
                                return false;
                            }
                        }
                        else
                        {
                            Accion.Advertencia("Uno de los productos acepta el registro con lotes,verificar y llenar su lote correspondiente.");
                            return false;
                        }
                    }
                }
            }

            return true;




        }


        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
                    Logica_Movimiento_Cab Log = new Logica_Movimiento_Cab();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                    Ent.Id_Periodo = Id_Periodo;
                    Ent.Id_Libro = txtlibro.Tag.ToString();
                    Ent.Id_Voucher = Voucher;

                    Ent.ctb_pdc_codigo = txtpdc.Text;
                    Ent.Ctb_Glosa = txtglosa.Text;
                    Ent.Ctb_Tipo_Doc = txttipodoc.Tag.ToString();

                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Ent.Ctb_Serie = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Ent.Ctb_Numero = Accion.Formato(Datos[1].Trim(), 8).ToString();

                    Ent.Ctb_Fecha_Movimiento = Convert.ToDateTime(txtfechadoc.Text);
                    Ent.Ctb_Tipo_Ent = "P";
                    Ent.Ctb_Ruc_dni = txtrucdni.Text;
                    Ent.Ctb_Condicion_Cod = txtcondicioncod.Tag.ToString();
                    Ent.Ctn_Moneda_Cod = txtmonedacod.Tag.ToString();
                    Ent.Ctb_Tipo_Cambio_Cod = txttipocambiocod.Text;

                    Ent.Ctb_Tipo_Cambio_desc = txttipocambiodesc.Text;
                    Ent.Ctb_Tipo_Cambio_Valor = Convert.ToDecimal(txttipocambiovalor.Text);

                    Ent.Ctb_Base_Imponible = Convert.ToDecimal(txtbaseimponible.Text);
                    Ent.Ctb_Igv = Convert.ToDecimal(txtigv.Text);
                    Ent.Ctb_Importe_Total = Convert.ToDecimal(txtimportetotal.Text);


                    //Ent.Ctb_Base_Imponible2 = MBase2;
                    //Ent.Ctb_Base_Imponible3 = MBase3;

                    //Ent.Ctb_Igv2 = MIgv2;
                    //Ent.Ctb_Igv3 = MIgv3;

                    //Ent.Ctb_Isc = chkisc.Checked;
                    //Ent.Ctb_Isc_Importe = MIsc;
                    //Ent.Ctb_Otros_Tributos = chkotrostri.Checked;
                    //Ent.Ctb_Otros_Tributos_Importe = MOtrosTributos;
                    //Ent.Ctb_No_Gravadas = MValorAdqNoGrav;
                


                    if (txtfechavencimiento.Text == "" || txtfechavencimiento.Text == "01/01/1900")
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime("01/01/1900");
                    }
                    else
                    {
                        Ent.Ctb_Fecha_Vencimiento = Convert.ToDateTime(txtfechavencimiento.Text);
                    }


                    //Ent.Ctb_Afecto_RE = Ctb_Afecto_RE;
                    //Ent.Ctb_Afecto_Tipo = Ctb_Afecto_Tipo;
                    //Ent.Ctb_Afecto_Tipo_Doc = Ctb_Afecto_Tipo_Doc;
                    //Ent.Ctb_Afecto_Serie = Ctb_Afecto_Serie;
                    //Ent.Ctb_Afecto_Numero = Ctb_Afecto_Numero;
                    //Ent.Ctb_Afecto_Fecha = Ctb_Afecto_Fecha;
                    //Ent.Ctb_Afecto_Porcentaje = Ctb_Afecto_Porcentaje;
                    //Ent.Ctb_Afecto_Monto = Ctb_Afecto_Monto;


                    //Ent.Ctb_Tasa_IGV = Convert.ToDecimal(txtigvporcentaje.Text);
                    //Ent.Ctb_Analisis = txtidanalisis.Text.Trim();

                    //detalle Contable
                    Ent.DetalleAsiento = Detalles_CONT;
                    //Detalle Administrativo
                    Ent.DetalleADM = Detalles_ADM;
                    ////Detalle Documento Referencia (Nota Debito - Credito)
                    //Ent.DetalleDoc_Ref = Lista_Doc_ref_Lista;
                    //// Detalle Orden de Compra.
                    //Ent.DetalleDoc_Orden_CS = Detalles_Orden_Compra;

                    Ent.DetalleLotes = ListaLotesFinal;

                    Ent.Ctb_Es_Compra_Comercial = true;
                    Ent.Ctb_Es_Movimiento_Comercial = true;
                    Ent.Ctb_Es_Movimiento_Comercial = false;

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar_adm(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;

                            LimpiarCab();
                            LimpiarDet2();
                            Detalles_ADM.Clear();
                            Detalles_CONT.Clear();
                            ListaLotesFinal.Clear();
                            Dgvdetalles.DataSource = null;
                            BloquearDetalles2();
                            TraerLibro();
                            BuscarMoneda_Inicial();
                            ResetearImportes();
                            txtpdc.Focus();
                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar_adm(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }


        //void ValidaSiUnProductoEstaAfectoALote()
        //{
        //    foreach (Entidad_Movimiento_Cab Item in ListaLotes)
        //    {
        //        if ((Item.Lot_Catalogo == codProducto) && (Item.Lot_Lote == Lote) && (Item.Lot_FechaFabricacion == FechaFab) && (Item.Lot_FechaVencimiento == FechaVenci))
        //        {
        //            return true;
        //        }

        //    }

        //    return false;
        //}

        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            if (VerificarNuevoDet())
            {
                EstadoDetalle2 = Estados.Nuevo;

                HabilitarDetalles();
                LimpiarDet2();
                Adm_Item = Detalles_ADM.Count + 1;

                txtvalorunit.Text = "0.00";
                txtimporte.Text = "0.00";
                txtcantidad.Text = "0.00";

                BuscarPuntoCompra_Almacen();

                btnnuevodet.Enabled = false;
                btneditardet.Enabled = false;
                btnquitardet.Enabled = false;
                btnanadirdet.Enabled = true;

                txtproductocod.Select();
            }
        }


        public bool VerificarNuevoDet()
        {
            //if (string.IsNullOrEmpty(txtglosa.Text.Trim()))
            //{
            //    Accion.Advertencia("Debe ingresar una glosa");
            //    txtglosa.Focus();
            //    return false;
            //}


            return true;
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {
            EstadoDetalle2 = Estados.Modificar;
            txttipobsacod.Focus();
        }

        private void btnanadirdet_Click(object sender, EventArgs e)
        {

            try
            {
                if (VerificarDetalle())
                {
                    Entidad_Movimiento_Cab ItemDetalle = new Entidad_Movimiento_Cab();

                    ItemDetalle.Adm_Item = Adm_Item;

                    if (txttipobsacod.Tag.ToString() == null)
                    {
                        ItemDetalle.Adm_Tipo_BSA = "";
                    }
                    else
                    {
                        ItemDetalle.Adm_Tipo_BSA = txttipobsacod.Tag.ToString();
                    }

                    ItemDetalle.Adm_Tipo_BSA_Interno = txttipobsacod.Text;
                    ItemDetalle.Adm_Tipo_BSA_Desc = txttipobsadesc.Text;

                    ItemDetalle.Adm_Catalogo = txtproductocod.Text.Trim();
                    ItemDetalle.Adm_Catalogo_Desc = txtproductodesc.Text;

                    ItemDetalle.Adm_Unm_Id = txtunmcod.Text.Trim();
                    ItemDetalle.Adm_Unm_Desc = txtunmdesc.Text.Trim();



                    ItemDetalle.Adm_Almacen = txtalmacencod.Text;
                    ItemDetalle.Adm_Almacen_desc = txtalmacendesc.Text;

                    ItemDetalle.Adm_Cantidad = Convert.ToDecimal(txtcantidad.Text);
                    ItemDetalle.Adm_Valor_Unit = Convert.ToDecimal(txtvalorunit.Text);

                    ItemDetalle.Adm_Total = Convert.ToDecimal(txtimporte.Text);

                    ItemDetalle.Acepta_lotes = chklote.Checked;

                    ItemDetalle.Adm_Marca = codMarca;
                    ItemDetalle.Adm_Marca_Desc = desMarca;

                    //AddRepository(chklote.Checked);


           

                    if (txttipooperacion.Tag.ToString() == null)
                    {
                        ItemDetalle.Adm_Tipo_Operacion_Interno = "";
                    }
                    else
                    {
                        ItemDetalle.Adm_Tipo_Operacion_Interno = txttipooperacion.Tag.ToString();
                    }

                    ItemDetalle.Adm_Tipo_Operacion = txttipooperacion.Text.Trim();
                   ItemDetalle.Adm_Tipo_Operacion_Desc = txttipooperaciondesc.Text.Trim();


                    if (EstadoDetalle2 == Estados.Nuevo)
                    {
                        Detalles_ADM.Add(ItemDetalle);
                        UpdateGrilla();
                        EstadoDetalle2 = Estados.Guardado;
                    }
                    else if (EstadoDetalle2 == Estados.Modificar)
                    {

                        Detalles_ADM[Convert.ToInt32(Adm_Item) - 1] = ItemDetalle;

                        UpdateGrilla();
                        EstadoDetalle2 = Estados.Guardado;

                    }

                    btnnuevodet.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void UpdateGrilla()
        {
            Dgvdetalles.DataSource = null;
            if (Detalles_ADM.Count > 0)
            {
                Dgvdetalles.DataSource = Detalles_ADM;
            }

            MostrarResumen();
        }

        private void AddRepository(bool Acepta_lotes=false)
        {
            if (Acepta_lotes == true)
            {
                RepositoryItemButtonEdit edit = new RepositoryItemButtonEdit();
                edit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                edit.ButtonClick += edit_ButtonClick;
                edit.Buttons[6].Caption = "Button";
                edit.Buttons[6].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
                gridView2.Columns["Button"].ColumnEdit = edit;
            }

        }


        void edit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            MessageBox.Show("The button from the " + gridView2.FocusedRowHandle + " row has been clicked!");
        }

        void MostrarResumen()
        {
            decimal MBase1 = 0, MValor_Fac_Export = 0;
            decimal MExonerada = 0, MInafecta, MIgv1 = 0;
            decimal MIsc = 0;
            decimal MOtrosTributos = 0, MImporteTotal = 0;

  
                foreach (Entidad_Movimiento_Cab T in Detalles_ADM)
                {
                    MBase1 += Math.Round((T.Adm_Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + 1)),2);
                    MIgv1 += Math.Round((T.Adm_Total / (Convert.ToDecimal(txtigvporcentaje.Tag) + 1)) * Convert.ToDecimal(txtigvporcentaje.Tag),2);
                   

                }

            MImporteTotal = Math.Round(MBase1 + MIgv1,2);

            txtbaseimponible.Text = Convert.ToString(Math.Round(MBase1, 2));
            txtigv.Text = Convert.ToString(Math.Round(MIgv1, 2));
            txtimportetotal.Text = Convert.ToString(Math.Round(MImporteTotal, 2));

            BuscarTipoAnalisisDefecto();


        }

        void BuscarTipoAnalisisDefecto()
        {
            Entidad_Analisis_Contable Ent = new Entidad_Analisis_Contable();
            Logica_Analisis_Contable log = new Logica_Analisis_Contable();
            List<Entidad_Analisis_Contable> Detalles_Analisi = new List<Entidad_Analisis_Contable>();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Analisis = Id_Analisis;
            Ent.Ana_Venta = false;
            Ent.Ana_Asiento_Defecto = true;
            Ent.Ana_Compra = true;


            Detalles_Analisi = log.Traer_Analisis_Defecto(Ent);
            Detalles_CONT.Clear();

            if (Detalles_Analisi.Count > 0)
            {
                Detalles_CONT.Clear();

                foreach (Entidad_Analisis_Contable T in Detalles_Analisi)
                {
                    Entidad_Movimiento_Cab Item = new Entidad_Movimiento_Cab();

                    Item.Id_Item = Detalles_CONT.Count + 1;
                    Item.Ctb_Cuenta = T.Ana_Cuenta;
                    Item.Ctb_Cuenta_Desc = T.Ana_Cuenta_Desc;
                    Item.Ctb_Operacion_Cod = T.Ana_Operacion_Cod;
                    Item.Ctb_Operacion_Cod_Interno = T.Ana_Operacion_Det;
                    Item.Ctb_Operacion_Desc = T.Ana_Operacion_Desc;
                    Item.Ctb_Tipo_DH = T.Ana_Tipo;
                    Item.CCtb_Tipo_DH_Interno = T.Ana_Tipo_Det;
                    Item.Ctb_Tipo_DH_Desc = T.Ana_Tipo_Desc;

                    Item.Ctb_Fecha_Mov_det = Convert.ToDateTime(txtfechadoc.Text);
                    Item.Ctb_Tipo_Ent_det = "P";
                    Item.Ctb_Ruc_dni_det = txtrucdni.Text.ToString().Trim();

                    Item.Ctb_Tipo_Doc_det = txttipodoc.Tag.ToString();

                    Item.Ctb_moneda_cod_det = txtmonedacod.Tag.ToString();

                    Item.Ctb_Tipo_Cambio_Cod_Det = txttipocambiocod.Text;// "SVC"
                    Item.Ctb_Tipo_Cambio_Desc_Det = txttipocambiodesc.Text;// "SIN CONVERSION"

                    Item.Ctb_Tipo_Cambio_Valor_Det = Convert.ToDecimal(txttipocambiovalor.Text); // 1

                    //Item.Ctb_Serie_det = BSISerieDocum.Caption;
                    //Item.Ctb_Numero_det = BSINumeroDocum.Caption;
                    string SerNum = txtserie.Text;
                    string[] Datos = SerNum.Split(Convert.ToChar("-"));

                    Item.Ctb_Serie_det = Accion.Formato(Datos[0].Trim(), 4).ToString();
                    Item.Ctb_Numero_det = Accion.Formato(Datos[1].Trim(), 8).ToString();


                    if (Item.Ctb_Tipo_DH == "0004")
                    {
                        if (Es_moneda_nac == true)
                        {
                            Item.Ctb_Importe_Debe = 0;
                        }
                        else
                        {
                            Item.Ctb_Importe_Debe_Extr = 0;
                        }
                    }
                    else if (Item.Ctb_Tipo_DH == "0005")
                    {
                        if (Es_moneda_nac == true)
                        {
                            Item.Ctb_Importe_Haber = 0;
                        }
                        else
                        {
                            Item.Ctb_Importe_Haber_Extr = 0;
                        }
                    }

                    Detalles_CONT.Add(Item);
                }
            }
            Calcular(Detalles_CONT, Es_moneda_nac);
  
        }

        public void Calcular(List<Entidad_Movimiento_Cab> Detalles, bool Es_moneda_nac)
        {

            try
            {
                decimal Base1 = 0, Valor_Fac_Export = 0;
                decimal Exonerada = 0, Inafecta = 0, Igv1 = 0;
                decimal Isc = 0;
                decimal OtrosTributos = 0, ImporteTotal = 0;


                foreach (Entidad_Movimiento_Cab itenms in Detalles_CONT)
                {
                    Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();

                    Ent = Detalles_CONT[itenms.Id_Item - 1];

                    Ent.Id_Item = itenms.Id_Item;

                    Ent.Ctb_Cuenta = itenms.Ctb_Cuenta;
                    Ent.Ctb_Cuenta_Desc = itenms.Ctb_Cuenta_Desc;
                    Ent.Ctb_Operacion_Cod = itenms.Ctb_Operacion_Cod;
                    Ent.Ctb_Operacion_Cod_Interno = itenms.Ctb_Operacion_Cod_Interno;
                    Ent.Ctb_Operacion_Desc = itenms.Ctb_Operacion_Desc;
                    Ent.Ctb_Tipo_DH = itenms.Ctb_Tipo_DH;
                    Ent.CCtb_Tipo_DH_Interno = itenms.CCtb_Tipo_DH_Interno;
                    Ent.Ctb_Tipo_DH_Desc = itenms.Ctb_Tipo_DH_Desc;

                    Ent.Ctb_Fecha_Mov_det = itenms.Ctb_Fecha_Mov_det;
                    Ent.Ctb_Tipo_Ent_det = "P";
                    Ent.Ctb_Ruc_dni_det = txtrucdni.Text;
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;
                    Ent.Ctb_moneda_cod_det = itenms.Ctb_moneda_cod_det;
                    Ent.Ctb_Tipo_Cambio_Cod_Det = itenms.Ctb_Tipo_Cambio_Cod_Det;
                    Ent.Ctb_Tipo_Cambio_Desc_Det = itenms.Ctb_Tipo_Cambio_Desc_Det;
                    Ent.Ctb_Tipo_Cambio_Valor_Det = itenms.Ctb_Tipo_Cambio_Valor_Det;
                    Ent.Ctb_Tipo_Doc_det = itenms.Ctb_Tipo_Doc_det;
                    Ent.Ctb_Serie_det = itenms.Ctb_Serie_det;
                    Ent.Ctb_Numero_det = itenms.Ctb_Numero_det;

                    if ((itenms.Ctb_Operacion_Cod == "0020"))//BASE IMPONIBLE
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Base_Imponible += Convert.ToDecimal(txtbaseimponible.Text);
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                            }
                            else
                            {
                                Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Debe_Extr;//(itenms.Ctb_Importe_Debe_Extr);
                                Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                            }
                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Base_Imponible += Convert.ToDecimal(txtbaseimponible.Text);//(itenms.Ctb_Importe_Haber);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                        }
                        else
                        {
                            Ent.Ctb_Base_Imponible += itenms.Ctb_Importe_Haber_Extr;//(itenms.Ctb_Importe_Haber_Extr);
                            Base1 = Convert.ToDecimal(Ent.Ctb_Base_Imponible);
                        }
                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0021")) // IGV
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Igv += Convert.ToDecimal(txtigv.Text);//();
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                            }
                            else
                            {
                                Ent.Ctb_Igv += itenms.Ctb_Importe_Debe_Extr;//(itenms.Ctb_Importe_Debe_Extr);
                                Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                            }
                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Igv += Convert.ToDecimal(txtigv.Text);//(itenms.Ctb_Importe_Haber);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                        }
                        else
                        {
                            Ent.Ctb_Igv += itenms.Ctb_Importe_Haber_Extr;// (itenms.Ctb_Importe_Haber_Extr);
                            Igv1 = Convert.ToDecimal(Ent.Ctb_Igv);
                        }
                    }
                    else if ((itenms.Ctb_Operacion_Cod == "0040")) // IMPORTE TOTAL
                    {
                        if ((itenms.Ctb_Importe_Debe > 0 || itenms.Ctb_Importe_Debe_Extr > 0))
                        {
                            if ((Es_moneda_nac == true))
                            {
                                Ent.Ctb_Importe_Total += Convert.ToDecimal(txtimportetotal.Text);// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                            else
                            {
                                Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Debe_Extr;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Debe_Extr);
                                ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                            }
                        }
                        else if ((Es_moneda_nac == true))
                        {
                            Ent.Ctb_Importe_Total += Convert.ToDecimal(txtimportetotal.Text);// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                        else
                        {
                            Ent.Ctb_Importe_Total += itenms.Ctb_Importe_Haber_Extr;// (Ent.Ctb_Importe_Total + itenms.Ctb_Importe_Haber_Extr);
                            ImporteTotal = Convert.ToDecimal(Ent.Ctb_Importe_Total);
                        }
                    }



                    ////PAAR EL DEBE Y EL HABER
                    ////
                    if (Ent.Ctb_Tipo_DH == "0004")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Base1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Base1 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Debe_Extr = Base1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = Igv1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = Igv1 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Debe_Extr = Igv1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// Total
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Debe = ImporteTotal;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Debe = ImporteTotal * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Debe_Extr = ImporteTotal;
                            }
                        }
                    }
                    else if (Ent.Ctb_Tipo_DH == "0005")
                    {
                        if (itenms.Ctb_Operacion_Cod == "0020") //Base 1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Base1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Base1 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = Base1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0021")//igv1
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = Igv1;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = Igv1 * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = Igv1;
                            }
                        }
                        if (itenms.Ctb_Operacion_Cod == "0040")// 
                        {
                            if (Es_moneda_nac == true)
                            {
                                Ent.Ctb_Importe_Haber = ImporteTotal;
                            }
                            else
                            {
                                Ent.Ctb_Importe_Haber = ImporteTotal * Convert.ToDecimal(txttipocambiovalor.Text);
                                Ent.Ctb_Importe_Haber_Extr = ImporteTotal;
                            }
                        }
                    }

                    //Detalles_CONT[itenms.Id_Item] = Ent;

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public bool VerificarDetalle()
        {

            // if (Convert.ToDouble(txtimporte.Text) <= 0)
            //{
            // Accion.Advertencia("Debe ingresar un importe mayor a cero");
            //  txtimporte.Focus();
            //  return false;
            // }
            //  if (string.IsNullOrEmpty(txttipooperaciondesc.Text.Trim()))
            // {
            // Accion.Advertencia("Debe ingresar un importe mayor a cero");
            //  txtimporte.Focus();
            //  return false;
            // }


            return true;
        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipobsacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        BuscarBSA();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA()
        {
            try
            {
                txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = txttipobsacod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim().ToUpper())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;
                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.ResetText();
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        string codMarca;
        string desMarca;
        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        //using (frm_catalogo_busqueda f = new frm_catalogo_busqueda())
                        //{
                        //    f.Tipo = txttipobsacod.Tag.ToString();

                        //    if (f.ShowDialog(this) == DialogResult.OK)
                        //    {
                        //        Entidad_Catalogo Entidad = new Entidad_Catalogo();

                        //        Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                        //        txtproductocod.Text = Entidad.Id_Catalogo;
                        //        txtproductodesc.Text = Entidad.Cat_Descripcion;

                        //        txtunmcod.Text = Entidad.Id_Unidad_Medida;
                        //        txtunmdesc.Text = Entidad.Und_Descripcion;
                        //        txtunmabrev.Text = Entidad.Und_Abreviado;


                        //        codMarca = Entidad.Id_Marca.Trim();
                        //        desMarca = Entidad.Mar_Descripcion.Trim();

                        //        chklote.Checked = Entidad.Acepta_Lote;
                        //        if (chklote.Checked)
                        //        {
                        //            btnlotes.Enabled = true;

                        //        }

                        //        txtproductocod.EnterMoveNextControl = true;

                        //        BuscarUnm_Defecto_Producto();
                        //    }
                        //}

                        using (frm_catalogo f = new frm_catalogo())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();
                            //f.Desde_Catalogo = "2";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;

                                txtunmcod.Text = Entidad.Id_Unidad_Medida;
                                txtunmdesc.Text = Entidad.Und_Descripcion;
                                txtunmabrev.Text = Entidad.Und_Abreviado;


                                codMarca = Entidad.Id_Marca.Trim();
                                desMarca = Entidad.Mar_Descripcion.Trim();

                                chklote.Checked = Entidad.Acepta_Lote;
                                if (chklote.Checked)
                                {
                                    btnlotes.Enabled = true;

                                }

                                txtproductocod.EnterMoveNextControl = true;

                                BuscarUnm_Defecto_Producto();
                            }
                        }

                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                        BuscarUnm_Defecto_Producto();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {
                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;

                            txtunmcod.Text = T.Id_Unidad_Medida;
                            txtunmdesc.Text = T.Und_Descripcion;
                            txtunmabrev.Text = T.Und_Abreviado;
                            chklote.Checked = T.Acepta_Lote;

                            codMarca = T.Id_Marca.Trim();
                            desMarca = T.Mar_Descripcion.Trim();

                            if (chklote.Checked)
                            {
                                btnlotes.Enabled = true;
                            }


                            txtproductocod.EnterMoveNextControl = true;
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.ResetText();
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void BuscarUnm_Defecto_Producto()
        {
            try
            {

                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Catalogo = txtproductocod.Text.Trim(),
                    Unm_Defecto = true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Unm_Defecto) == true)
                        {
                            txtunmcod.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdesc.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void txtalmacencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacencod.Text.Substring(txtalmacencod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacencod.Text = Entidad.Id_Almacen;
                                txtalmacendesc.Text = Entidad.Alm_Descrcipcion;
                                txtalmacencod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacencod.Text) & string.IsNullOrEmpty(txtalmacendesc.Text))
                    {
                        BuscarAlmacen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }




        public void BuscarAlmacen()
        {
            try
            {
                txtalmacencod.Text = Accion.Formato(txtalmacencod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen = txtalmacencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacencod.Text.Trim().ToUpper())
                        {
                            txtalmacencod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion;
                            txtalmacencod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtalmacencod.EnterMoveNextControl = false;
                    txtalmacencod.ResetText();
                    txtalmacencod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtvalorunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtvalorunit.Text.Trim()))
            {
                try
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        decimal Importe;
                        Importe = Convert.ToDecimal(txtvalorunit.Text) * Convert.ToDecimal(txtcantidad.Text);
                        txtimporte.Text = Convert.ToString(Importe);
                    }

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void gridView2_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                if (Detalles_ADM.Count > 0)
                {
                    Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();

                    EstadoDetalle = Estados.Ninguno;
                    Entidad = Detalles_ADM[gridView2.GetFocusedDataSourceRowIndex()];

                    Adm_Item = Entidad.Adm_Item;

                    txttipobsacod.Tag = Entidad.Adm_Tipo_BSA;
                    txttipobsacod.Text = Entidad.Adm_Tipo_BSA_Interno;
                    txttipobsadesc.Text = Entidad.Adm_Tipo_BSA_Desc;

                    txtunmcod.Text = Entidad.Adm_Unm_Id;
                    txtunmdesc.Text = Entidad.Adm_Unm_Desc;


                    txtproductocod.Text = Entidad.Adm_Catalogo;
                    txtproductodesc.Text = Entidad.Adm_Catalogo_Desc;


                    txtalmacencod.Text = Entidad.Adm_Almacen;
                    txtalmacendesc.Text = Entidad.Adm_Almacen_desc;

                    txtcantidad.Text = Entidad.Adm_Cantidad.ToString();
                    txtvalorunit.Text = Entidad.Adm_Valor_Unit.ToString();
                    txtimporte.Text = Entidad.Adm_Total.ToString();

                    chklote.Checked = Entidad.Acepta_lotes;

                    codMarca = Entidad.Adm_Marca;
                    desMarca = Entidad.Adm_Marca_Desc;


                    txttipooperacion.Tag = Entidad.Adm_Tipo_Operacion_Interno;
                    txttipooperacion.Text = Entidad.Adm_Tipo_Operacion ;
                    txttipooperaciondesc.Text   = Entidad.Adm_Tipo_Operacion_Desc ;



                    if (chklote.Checked)
                    {
                        btnlotes.Enabled = true;
                    }

                    if (Estado_Ven_Boton == "1")
                    {
                        EstadoDetalle2 = Estados.Consulta;
                    }
                    else if (Estado_Ven_Boton == "2")
                    {
                        EstadoDetalle2 = Estados.SoloLectura;
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnquitardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles_ADM.Count > 0)
                {

                    //Primero Eliminar los detalles de Lotes Quitar de la lista
                    //var PrivView = (from item in ListaLotesFinal
                    //                where item.Lot_Catalogo == txtproductocod.Text.Trim() && item.Lot_Adm_item == Adm_Item
                    //                select item).FirstOrDefault();
                    //PrivView.Employees.Remove(x);
                    //PrivView.SaveChanges();

                    ListaLotesFinal.RemoveAll(x => x.Lot_Catalogo == txtproductocod.Text.Trim() && x.Lot_Adm_item == Adm_Item);


                    Detalles_ADM.RemoveAt(gridView2.GetFocusedDataSourceRowIndex());

                    Dgvdetalles.DataSource = null;
                    if (Detalles_ADM.Count > 0)
                    {
                        Dgvdetalles.DataSource = Detalles_ADM;
                        RefreshNumeral();                        
                    }

                    MostrarResumen();

                    EstadoDetalle = Estados.Ninguno;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void RefreshNumeral()
        {
            try
            {
                int NumOrden = 1;
                foreach (Entidad_Movimiento_Cab Det in Detalles_ADM)
                {
                    Det.Id_Item = NumOrden;
                    NumOrden += 1;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void txtfechadoc_Leave(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txtfechadoc_TextChanged(object sender, EventArgs e)
        {
            ActualizaIGV();
        }

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_tipo_documento_busqueda f = new frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        private void txtpdc_TextChanged(object sender, EventArgs e)
        {
            if (txtpdc.Focus() == false)
            {
                txtpdcdesc.ResetText();
            }
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {

            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txtmonedacod_TextChanged(object sender, EventArgs e)
        {
            if (txtmonedacod.Focus() == false)
            {
                txtmonedadesc.ResetText();
                txttipocambiocod.ResetText();
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
                txtmonedacod.Focus();

            }
        }

        private void txttipocambiocod_TextChanged(object sender, EventArgs e)
        {
            if (txttipocambiocod.Focus() == false)
            {
                txttipocambiodesc.ResetText();
                txttipocambiovalor.Text = "0.000";
            }
        }

        private void txtrucdni_TextChanged(object sender, EventArgs e)
        {
            if (txtrucdni.Focus() == false)
            {
                txtentidad.ResetText();
            }
        }

        private void txttipobsacod_TextChanged(object sender, EventArgs e)
        {
            if (txttipobsacod.Focus() == false)
            {
                txttipobsadesc.ResetText();
            }
        }

        private void txtproductocod_TextChanged(object sender, EventArgs e)
        {
            if (txtproductocod.Focus() == false)
            {
                txtproductodesc.ResetText();
                //txtunmcod.ResetText();
                //txtunmdesc.ResetText();
            }
        }

        private void txtunmcod_TextChanged(object sender, EventArgs e)
        {
            if (txtunmcod.Focus() == false)
            {
                txtunmdesc.ResetText();
            }
        }

        private void txtalmacencod_TextChanged(object sender, EventArgs e)
        {
            if (txtalmacencod.Focus() == false)
            {
                txtalmacendesc.ResetText();
            }
        }

        private void txtunmcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtunmcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunmcod.Text.Substring(txtunmcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_unidad_medida_busqueda f = new frm_unidad_medida_busqueda())
                        {
                            f.Producto_Cod = txtproductocod.Text;
                            f.Producto_Unm = true;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunmcod.Text = Entidad.Id_Unidad_Medida;
                                txtunmdesc.Text = Entidad.Und_Descripcion;

                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtunmcod.Text) & string.IsNullOrEmpty(txtunmdesc.Text))
                    {
                        BuscarUnmPres();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarUnmPres()
        {
            try
            {
                txtunmcod.Text = Accion.Formato(txtunmcod.Text, 3);
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Unidad_Medida = txtunmcod.Text,
                    Id_Catalogo = txtproductocod.Text.Trim()
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Id_Unidad_Medida).ToString().Trim().ToUpper() == txtunmcod.Text.Trim().ToUpper())
                        {
                            txtunmcod.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdesc.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        List<Entidad_Movimiento_Cab> ListaLotesFinal = new List<Entidad_Movimiento_Cab>();
        private void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles_ADM.Count > 0)
                {
                    Entidad_Movimiento_Cab parametros = new Entidad_Movimiento_Cab();

                    parametros = Detalles_ADM[gridView2.GetFocusedDataSourceRowIndex()];

                    if (parametros.Acepta_lotes == true)
                    {
 
                        using (frml_lotes_edicion f = new frml_lotes_edicion())
                        {

                            f.codProducto = parametros.Adm_Catalogo;
                            f.desProducto = parametros.Adm_Catalogo_Desc;
                            f.codMarca = parametros.Adm_Marca;
                            f.desMarca = parametros.Adm_Marca_Desc;
                            f.txtcantidad.Text = Convert.ToString(parametros.Adm_Cantidad);
                            f.Adm_Item = parametros.Adm_Item;
                            
                            f.Estado_Ven_Boton = Estado_Ven_Boton;

                            f.ListaLotesCompleta = ListaLotesFinal;

                            if (f.ShowDialog() == DialogResult.OK)
                            {

                                ListaLotesFinal = f.ListaLotesCompleta ;

                                //foreach (Entidad_Movimiento_Cab i in f.ListaLotes)
                                //{
                                //    Entidad_Movimiento_Cab Doc = new Entidad_Movimiento_Cab();
                                //    Doc = f.Lista [i];

                             }

                        }


                        }
                    else
                    {
                        //aqui bloquearemos cuando no tiene lotes
                        //MessageBox.Show("ccc");
                        repositoryItemButtonEdit1.ReadOnly = false;
                    }


                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        bool ExistsLoteExiste(string codProducto, string Lote, DateTime FechaFab, DateTime FechaVenci)
        {
            foreach (Entidad_Movimiento_Cab Item in ListaLotesFinal)
            {
                if ((Item.Lot_Catalogo == codProducto) && (Item.Lot_Lote == Lote) && (Item.Lot_FechaFabricacion == FechaFab) && (Item.Lot_FechaVencimiento == FechaVenci))
                {
                    return true;
                }

            }

            return false;
        }

        private void btnlotes_Click(object sender, EventArgs e)
        {

        }

        private void gridView2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            //Dim View As GridView = sender
            //If(e.RowHandle >= 0) Then
            //   Dim Diferencia As Int16 = View.GetRowCellDisplayText(e.RowHandle, View.Columns("RptItem"))
            //    If Diferencia = 1 Then
            //        e.Appearance.BackColor = Color.FromArgb(128, 153, 195)
            //    End If
            //End If

            //if (e.Column == gridView2.Columns[4])
            //    e.RepositoryItem = GetRepItemButtonEdit(gridView2.GetDataSourceRowIndex(e.RowHandle));

            GridView View = (GridView)sender;
            if (e.RowHandle >= 0)
            {
                string Acepta = View.GetRowCellDisplayText(e.RowHandle, View.Columns["Acepta_lotes"]);
                string codProducto = View.GetRowCellDisplayText(e.RowHandle, View.Columns["Adm_Catalogo"]);
                int Adm_Item = Convert.ToInt32(View.GetRowCellDisplayText(e.RowHandle, View.Columns["Adm_Item"]));

                if (Acepta == "True")
                {
                    var PrivView = from item in ListaLotesFinal
                                   where item.Lot_Catalogo == codProducto && item.Lot_Adm_item == Adm_Item
                                   orderby item.Id_Anio descending
                                   select item;

                    if (PrivView.Count() == 0)
                    {
                      e.Appearance.BackColor = Color.FromArgb(204 , 240, 112);
                    }

                  
                }

            }


            //ColumnView view = (ColumnView)sender;
            //if (view.IsValidRowHandle(e.RowHandle))
            //{
            //    string Acepta = View.GetRowCellDisplayText(e.RowHandle, View.Columns["Acepta_lotes"]);

            //    if (Acepta == "True")
            //    {
                   
            //        //int colHandle = e.Column.ColumnHandle;
            //        //int rowHandle = e.RowHandle;
            //        //result = SomeHeavyCalculation(colHandle, rowHandle) 
            //        e.Appearance.BackColor = Color.DarkRed;
            //    }
               
                   

            //}
        }

        private void txttipooperacion_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipooperacion.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipooperacion.Text.Substring(txttipooperacion.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "1009";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipooperacion.Tag = Entidad.Id_General_Det;
                                txttipooperacion.Text = Entidad.Gen_Codigo_Interno;
                                txttipooperaciondesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipooperacion.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipooperacion.Text) & string.IsNullOrEmpty(txttipooperaciondesc.Text))
                    {
                        BuscarTipoOperacion();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarTipoOperacion()
        {
            try
            {
                txttipooperacion.Text = Accion.Formato(txttipooperacion.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "1009",
                    Gen_Codigo_Interno = txttipooperacion.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipooperacion.Text.Trim().ToUpper())
                        {
                            txttipooperacion.Tag = T.Id_General_Det;
                            txttipooperacion.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipooperaciondesc.Text = T.Gen_Descripcion_Det;
                            txttipooperacion.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipooperacion.EnterMoveNextControl = false;
                    txttipooperacion.ResetText();
                    txttipooperacion.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipooperacion_TextChanged(object sender, EventArgs e)
        {
            if (txttipooperacion.Focus() == false)
            {
                txttipooperaciondesc.ResetText();
            }
        }

        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtpdc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpdc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpdc.Text.Substring(txtpdc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_punto_compra_busqueda f = new frm_punto_compra_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Punto_Compra Entidad = new Entidad_Punto_Compra();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpdc.Text = Entidad.Pdc_Codigo.Trim();
                                txtpdcdesc.Text = Entidad.Pdc_Nombre.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpdc.Text) & string.IsNullOrEmpty(txtpdcdesc.Text))
                    {
                        BuscarPuntoCompra();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarPuntoCompra()
        {
            try
            {
                txtpdc.Text = Accion.Formato(txtpdc.Text, 2);

                Logica_Punto_Compra log = new Logica_Punto_Compra();

                List<Entidad_Punto_Compra> Generales = new List<Entidad_Punto_Compra>();

                Generales = log.Listar(new Entidad_Punto_Compra
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,

                    Pdc_Codigo = txtpdc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Punto_Compra T in Generales)
                    {
                        if ((T.Pdc_Codigo).ToString().Trim().ToUpper() == txtpdc.Text.Trim().ToUpper())
                        {
                            txtpdc.Text = (T.Pdc_Codigo).ToString().Trim();
                            txtpdcdesc.Text = T.Pdc_Nombre.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarPuntoCompra_Almacen()
        {
            try
            {
                txtpdc.Text = Accion.Formato(txtpdc.Text, 2);
               Logica_Punto_Compra log = new Logica_Punto_Compra();
                List<Entidad_Punto_Compra> Generales = new List<Entidad_Punto_Compra>();

                Generales = log.Listar(new Entidad_Punto_Compra
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Pdc_Codigo = txtpdc.Text
                });

                if (Generales.Count > 0)
                {
                    foreach (Entidad_Punto_Compra T in Generales)
                    {
                        if ((T.Pdc_Codigo).ToString().Trim().ToUpper() == txtpdc.Text.Trim().ToUpper())
                        {
                            //Tipo BIEN-SERVICIO
                            txttipobsacod.Tag = T.Pdc_Almacen_BSA_Cod_;
                            txttipobsacod.Text = (T.Pdc_Almacen_Cod_BSA_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Pdc_Almacen_BSA_Interno_Desc;
                            //ALMACEN
                            txttipobsacod.Enabled = false;

                            txtalmacencod.Text = (T.Pdc_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion;
                            txtalmacencod.Enabled = false;

                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



    }
}
