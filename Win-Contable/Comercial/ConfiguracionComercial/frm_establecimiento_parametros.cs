﻿using Comercial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.ConfiguracionComercial
{
    public partial class frm_establecimiento_parametros : frm_fuente
    {
        public frm_establecimiento_parametros()
        {
            InitializeComponent();
        }

        private void btngenerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtestnombre.Text.Trim()))
                {
                    Accion.Advertencia("Debe seleccionar un establecimiento");
                }
                else
                {
                 DialogResult = DialogResult.OK;
                }
            
            }
            catch (Exception ex)
            {

            }
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtestcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtestcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtestcod.Text.Substring(txtestcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_establecimiento_busqueda f = new frm_establecimiento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Establecimiento Entidad = new Entidad_Establecimiento();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtestcod.Text = Entidad.Est_Codigo.Trim();
                                txtestnombre.Text = Entidad.Est_Descripcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtestcod.Text) & string.IsNullOrEmpty(txtestnombre.Text))
                    {
                        BuscarEstablecimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEstablecimiento()
        {
            try
            {
                txtestcod.Text = Accion.Formato(txtestcod.Text, 2);

                Logica_Establecimiento log = new Logica_Establecimiento();

                List<Entidad_Establecimiento> Generales = new List<Entidad_Establecimiento>();

                Generales = log.Listar(new Entidad_Establecimiento
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Est_Codigo = txtestcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Establecimiento T in Generales)
                    {
                        if ((T.Est_Codigo).ToString().Trim().ToUpper() == txtestcod.Text.Trim().ToUpper())
                        {
                            txtestcod.Text = (T.Est_Codigo).ToString().Trim();
                            txtestnombre.Text = T.Est_Descripcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtestcod_TextChanged(object sender, EventArgs e)
        {
            if (txtestcod.Focus() == false)
            {
                txtestnombre.ResetText();
            }
        }
    }
}
