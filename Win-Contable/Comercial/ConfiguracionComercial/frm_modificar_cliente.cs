﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.ConfiguracionComercial
{
    public partial class frm_modificar_cliente : frm_fuente
    {
        public frm_modificar_cliente()
        {
            InitializeComponent();
        }

        public string rucdni;
        public string Ent_Tipo_Doc_Cod_Interno;

        void traer_cliente()
        {
            Logica_Entidad log = new Logica_Entidad();

            List<Entidad_Entidad> Generales = new List<Entidad_Entidad>();

            Generales = log.Listar(new Entidad_Entidad
            {
                Ent_RUC_DNI = rucdni
            });

            if (Generales.Count > 0)
            {

                foreach (Entidad_Entidad T in Generales)
                {
                    if ((T.Ent_RUC_DNI).ToString().Trim().ToUpper() == rucdni.Trim().ToUpper())
                    {
                        TxtTipDocNumer.Text = (T.Ent_RUC_DNI).ToString().Trim();
                        TxtEntNombre.Text =  T.Ent_Razon_Social_Nombre.Trim();
                        TxtEnt1Ape.Text = T.Ent_Ape_Paterno.Trim();
                        TxtEnt2Ape.Text = T.Ent_Ape_Materno.Trim();
                        TxtDirecionCompleta.Text = T.Ent_Domicilio_Fiscal.Trim();
                        Ent_Tipo_Doc_Cod_Interno = T.Ent_Tipo_Doc_Cod_Interno.Trim();
                    }
                }

            }
        }

        private void frm_modificar_cliente_Load(object sender, EventArgs e)
        {
            Estado = Estados.Modificar;
            traer_cliente();
        }

        private void btnaceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validar())
                {
                    Entidad_Entidad Ent = new Entidad_Entidad();
                    Logica_Entidad Log = new Logica_Entidad();

 
                    Ent.Ent_RUC_DNI = TxtTipDocNumer.Text;
                    Ent.Ent_Razon_Social_Nombre = TxtEntNombre.Text;
                    Ent.Ent_Ape_Paterno = TxtEnt1Ape.Text;
                    Ent.Ent_Ape_Materno = TxtEnt2Ape.Text;
 
                    Ent.Ent_Domicilio_Fiscal = TxtDirecionCompleta.Text;
                    Ent.Ent_Correo = "";
 

                    try
                    {
                     if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar_RAZON_DIRECCION(Ent))
                            {
                                Accion.ExitoModificar();
                                this.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool Validar()
        {
            if (string.IsNullOrEmpty(TxtTipDocNumer.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero para el documento");
                TxtTipDocNumer.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(TxtEntNombre.Text.Trim()))
            {
                Accion.Advertencia("Debe asignar un nombre");
                TxtEntNombre.Focus();
                return false;
            }


            return true;
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
