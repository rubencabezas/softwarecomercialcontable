﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_registrar_cliente : frm_fuente
    {
        public frm_registrar_cliente()
        {
            InitializeComponent();
        }

        public string TipoPersona;
        public string TipoDoc;
        public string RUCDNI_NoExist="";
        private void frm_registrar_cliente_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(RUCDNI_NoExist))
            {
                Estado = Estados.Nuevo;
                TxtTipDocNumer.Text = RUCDNI_NoExist;
                TxtEnt1Ape.Text = TxtEnt1Ape.Text;
                TxtEnt2Ape.Text = TxtEnt2Ape.Text;
                TxtEntNombre.Text = TxtEntNombre.Text;
                TxtDirecionCompleta.Text = TxtDirecionCompleta.Text;
            }
            CarGarTipoPersona();
            CargarTipoDoc();

        }




        public void CarGarTipoPersona()
        {
            try
            {
                TxtTipPerCodigo.Text = TipoPersona;
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0004",
                    Gen_Codigo_Interno=TipoPersona
                });

                if (Generales.Count > 0)
                {
                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == TxtTipPerCodigo.Text.Trim().ToUpper())
                        {
                            TxtTipPerCodigo.Tag = (T.Id_General_Det).ToString().Trim();
                            TxtTipPerCodigo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            TxtTipPerDescrip.Text = T.Gen_Descripcion_Det;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public void CargarTipoDoc()
        {
            try
            {
                TxtTipDocCodigo.Text = TipoDoc;
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0005",
                    Gen_Codigo_Interno=TipoDoc
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == TxtTipDocCodigo.Text.Trim().ToUpper())
                        {
                            TxtTipDocCodigo.Tag = (T.Id_General_Det).ToString().Trim();
                            TxtTipDocCodigo.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            TxtTipDocDescrip.Text = T.Gen_Descripcion_Det;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);//Tools.ShowError(ex.Message);
            }
        }

        private void btnaceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validar())
                {
                    Entidad_Entidad Ent = new Entidad_Entidad();
                    Logica_Entidad Log = new Logica_Entidad();

                    Ent.Ent_Tipo_Persona = TxtTipPerCodigo.Tag.ToString();
                    Ent.Ent_Tipo_Doc = TxtTipDocCodigo.Tag.ToString();
                    Ent.Ent_RUC_DNI = TxtTipDocNumer.Text;
                    Ent.Ent_Razon_Social_Nombre = TxtEntNombre.Text;
                    Ent.Ent_Ape_Paterno = TxtEnt1Ape.Text;
                    Ent.Ent_Ape_Materno = TxtEnt2Ape.Text;
                    Ent.Ent_Telefono = "";
                    Ent.Ent_Repre_Legal = "";
                    Ent.Ent_Telefono_movil = "";
                    Ent.Ent_Domicilio_Fiscal = TxtDirecionCompleta.Text;
                    Ent.Ent_Correo = "";
                    Ent.Ent_Pagina_Web = "";

                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {
                            if (Log.Insertar(Ent))
                            {
                                Accion.ExitoGuardar();
                                Estado = Estados.Nuevo;
                                this.Close();
                            }
                        }
                        else if (Estado == Estados.Modificar)
                        {
                            if (Log.Modificar(Ent))
                            {
                                Accion.ExitoModificar();
                                this.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public bool Validar()
        {
            if (string.IsNullOrEmpty(TxtTipDocNumer.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero para el documento");
                TxtTipDocNumer.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(TxtEntNombre.Text.Trim()))
            {
                Accion.Advertencia("Debe asignar un nombre");
                TxtEntNombre.Focus();
                return false;
            }


            return true;
        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
