﻿namespace Comercial
{
    partial class frm_asignacion_caja_monto_pdv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtsaldoinicial = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.btncancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnaceptar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtsaldoinicial.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(79, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "APERTURA DE CAJA";
            // 
            // txtsaldoinicial
            // 
            this.txtsaldoinicial.Location = new System.Drawing.Point(205, 79);
            this.txtsaldoinicial.Name = "txtsaldoinicial";
            this.txtsaldoinicial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsaldoinicial.Properties.Appearance.Options.UseFont = true;
            this.txtsaldoinicial.Properties.Mask.EditMask = "n2";
            this.txtsaldoinicial.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtsaldoinicial.Size = new System.Drawing.Size(258, 42);
            this.txtsaldoinicial.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 39);
            this.label2.TabIndex = 2;
            this.label2.Text = "Saldo Inicial";
            // 
            // btncancelar
            // 
            this.btncancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancelar.Appearance.Options.UseFont = true;
            this.btncancelar.Appearance.Options.UseTextOptions = true;
            this.btncancelar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btncancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btncancelar.Location = new System.Drawing.Point(86, 144);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(135, 51);
            this.btncancelar.TabIndex = 163;
            this.btncancelar.Text = "Cancelar";
            // 
            // btnaceptar
            // 
            this.btnaceptar.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaceptar.Appearance.Options.UseFont = true;
            this.btnaceptar.Appearance.Options.UseTextOptions = true;
            this.btnaceptar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnaceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnaceptar.Location = new System.Drawing.Point(276, 144);
            this.btnaceptar.Name = "btnaceptar";
            this.btnaceptar.Size = new System.Drawing.Size(135, 51);
            this.btnaceptar.TabIndex = 163;
            this.btnaceptar.Text = "Aceptar";
            // 
            // frm_asignacion_caja_monto_pdv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(475, 207);
            this.Controls.Add(this.btnaceptar);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtsaldoinicial);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_asignacion_caja_monto_pdv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Caja Importe";
            this.Load += new System.EventHandler(this.frm_asignacion_caja_monto_pdv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtsaldoinicial.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.SimpleButton btncancelar;
        internal DevExpress.XtraEditors.SimpleButton btnaceptar;
        public DevExpress.XtraEditors.TextEdit txtsaldoinicial;
    }
}
