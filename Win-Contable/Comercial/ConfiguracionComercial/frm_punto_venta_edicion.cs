﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_punto_venta_edicion : frm_fuente
    {
        public frm_punto_venta_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Pdv_Codigo;
        void Limpiar()
        {
            Pdv_Codigo=null;

            txtestcod.ResetText();
            txtnombrepdv.ResetText();
            txtimpresora.ResetText();

            txtnombrepc.ResetText();
            txtnummaq.ResetText();
            txtalmacencod.ResetText();

        }
        private void txtalmacencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacencod.Text.Substring(txtalmacencod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacencod.Text = Entidad.Id_Almacen.Trim();
                                txtalmacendesc.Text = Entidad.Alm_Descrcipcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacencod.Text) & string.IsNullOrEmpty(txtalmacendesc.Text))
                    {
                        BuscarAlmacen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtnombrepdv.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un nombre");
                txtnombrepdv.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtestnombre.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un establecimiento");
                txtestcod.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtalmacendesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un almacen");
                txtalmacencod.Focus();
                return false;
            }


            return true;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Punto_Venta Ent = new Entidad_Punto_Venta();
                    Logica_Punto_Venta Log = new Logica_Punto_Venta();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

                    Ent.Pdv_Codigo = Pdv_Codigo;

                    Ent.Est_Codigo = txtestcod.Text;
                    Ent.Pdv_Nombre = txtnombrepdv.Text;
                    Ent.Pdv_Impresora = txtimpresora.Text;

                    Ent.Pdv_Nombre_PC = txtnombrepc.Text;
                    Ent.Pdv_Num_Maquina_Registradora = txtnummaq.Text;
                    Ent.Pdv_Almacen = txtalmacencod.Text;



                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();


                            txtnombrepdv.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtestcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtestcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtestcod.Text.Substring(txtestcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_establecimiento_busqueda f = new frm_establecimiento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Establecimiento Entidad = new Entidad_Establecimiento();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtestcod.Text = Entidad.Est_Codigo.Trim();
                                txtestnombre.Text = Entidad.Est_Descripcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtestcod.Text) & string.IsNullOrEmpty(txtestnombre.Text))
                    {
                        BuscarEstablecimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEstablecimiento()
        {
            try
            {
                txtestcod.Text = Accion.Formato(txtestcod.Text, 2);

                Logica_Establecimiento log = new Logica_Establecimiento();

                List<Entidad_Establecimiento> Generales = new List<Entidad_Establecimiento>();

                Generales = log.Listar(new Entidad_Establecimiento
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Est_Codigo = txtestcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Establecimiento T in Generales)
                    {
                        if ((T.Est_Codigo).ToString().Trim().ToUpper() == txtestcod.Text.Trim().ToUpper())
                        {
                            txtestcod.Text = (T.Est_Codigo).ToString().Trim();
                            txtestnombre.Text = T.Est_Descripcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnimpresora_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Using;
            //    ((void)(f));
            //    new FrmImpresoras();
            //    if ((f.ShowDialog == DialogResult.OK))
            //    {
            //        TxtNombreImpresora.Text = f.DgvDetalle.CurrentRow.Cells[0].Value;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Tools.ShowError(ex.Message);
            //}
            using (frm_impresora_instalada_busqueda f = new frm_impresora_instalada_busqueda())
            {


                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        txtimpresora.Text = Convert.ToString(f.dgvdatos.CurrentRow.Cells[0].Value);
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
              }
        }

        private void btnpc_Click(object sender, EventArgs e)
        {
            txtnombrepc.Text = Actual_Conexion.Maquina;
        }

        private void frm_punto_venta_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Limpiar();

                txtnombrepdv.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();

            }
        }

        public List<Entidad_Punto_Venta> Lista_Modificar = new List<Entidad_Punto_Venta>();
        public void ListarModificar()
        {
            Entidad_Punto_Venta Ent = new Entidad_Punto_Venta();
            Logica_Punto_Venta log = new Logica_Punto_Venta();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Pdv_Codigo = Pdv_Codigo;

            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Punto_Venta Enti = new Entidad_Punto_Venta();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Pdv_Codigo = Enti.Pdv_Codigo;

                    txtnombrepdv.Text = Enti.Pdv_Nombre;
                    txtestcod.Text = Enti.Est_Codigo;
                    txtestnombre.Text = Enti.Est_Descripcion;
                    txtimpresora.Text = Enti.Pdv_Impresora;
                    txtnombrepc.Text = Enti.Pdv_Nombre_PC;
                    txtnummaq.Text = Enti.Pdv_Num_Maquina_Registradora;
                    txtalmacencod.Text = Enti.Pdv_Almacen;
                    txtalmacendesc.Text = Enti.Alm_Descrcipcion;


                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }
        public void BuscarAlmacen()
        {
            try
            {
                txtalmacencod.Text = Accion.Formato(txtalmacencod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen = txtalmacencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacencod.Text.Trim().ToUpper())
                        {
                            txtalmacencod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtestcod_TextChanged(object sender, EventArgs e)
        {
            if (txtestcod.Focus() == false)
            {
                txtestnombre.ResetText();
            }
        }

        private void txtalmacencod_TextChanged(object sender, EventArgs e)
        {
            if (txtalmacencod.Focus() == false)
            {
                txtalmacendesc.ResetText();
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }
    }
}
