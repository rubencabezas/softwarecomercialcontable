﻿using Contable;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_series_comprobantes_edicion : frm_fuente
    {
        public frm_series_comprobantes_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Ser_Tipo_Doc, Ser_Serie, Ser_Numero_Ini, Ser_Numero_Fin;
        void Limpiar()
        {
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txtserie.ResetText();
            txtnuminicio.ResetText();
            txtnumfin.ResetText();
            txtnumactual.ResetText();
            txtpdvcod.ResetText();
            txtpdvdesc.ResetText();
            txtfechaimpr.ResetText();
            txtfechabaja.ResetText();

        }
        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_tipo_documento_busqueda f = new frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtpdvcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpdvcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpdvcod.Text.Substring(txtpdvcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_punto_venta_busqueda f = new frm_punto_venta_busqueda())
                        {
                           

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpdvcod.Text = Entidad.Pdv_Codigo.Trim();
                                txtpdvdesc.Text = Entidad.Pdv_Nombre.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpdvcod.Text) & string.IsNullOrEmpty(txtpdvdesc.Text))
                    {
                        BuscarPuntoVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarPuntoVenta()
        {
            try
            {
                txtpdvcod.Text = Accion.Formato(txtpdvcod.Text, 2);

                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Generales = new List<Entidad_Punto_Venta>();

                Generales = log.Listar(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                   
                    Pdv_Codigo = txtpdvcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Punto_Venta T in Generales)
                    {
                        if ((T.Pdv_Codigo).ToString().Trim().ToUpper() == txtpdvcod.Text.Trim().ToUpper())
                        {
                            txtpdvcod.Text = (T.Pdv_Codigo).ToString().Trim();
                            txtpdvdesc.Text = T.Pdv_Nombre.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtserie_Leave(object sender, EventArgs e)
        {
            txtserie.Text = Accion.Formato(txtserie.Text, 4);
        }

        private void txtnuminicio_Leave(object sender, EventArgs e)
        {
            txtnuminicio.Text = Accion.Formato(txtnuminicio.Text,8);
        }

        private void txtnumfin_Leave(object sender, EventArgs e)
        {
            txtnumfin.Text = Accion.Formato(txtnumfin.Text, 8);
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtserie.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una serie");
                txtserie.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txtnuminicio.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero de inicio");
                txtnuminicio.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtnumfin.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero de fin");
                txtnumfin.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtpdvcod.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un punto de venta");
                txtpdvcod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtfechaimpr.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha");
                txtpdvcod.Focus();
                return false;
            }

            return true;
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Serie_Comprobante Ent = new Entidad_Serie_Comprobante();
                    Logica_Serie_Comprobante Log = new Logica_Serie_Comprobante();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;


                    Ent.Ser_Tipo_Doc = txttipodoc.Tag.ToString();
                    Ent.Ser_Serie = txtserie.Text;
                    Ent.Ser_Numero_Ini = txtnuminicio.Text;
                    Ent.Ser_Numero_Fin = txtnumfin.Text;
                    Ent.Ser_Numero_Actual = txtnumactual.Text;
                    Ent.Ser_Pdv_Codigo = txtpdvcod.Text;

                    Ent.Es_Electronico = chkelectronico.Checked;

  
                    if (txtfechaimpr.Text == "" || txtfechaimpr.Text == "01/01/1900")
                    {
                        Ent.Ser_Fecha_Impresion = Convert.ToDateTime("01/01/1900");
                    }
                    else
                    {
                        Ent.Ser_Fecha_Impresion = Convert.ToDateTime(txtfechaimpr.Text);
                    }


                    if (txtfechabaja.Text == "" || txtfechabaja.Text == "01/01/1900")
                    {
                        Ent.Ser_Fecha_Baja = Convert.ToDateTime("01/01/1900");
                    }
                    else
                    {
                        Ent.Ser_Fecha_Baja = Convert.ToDateTime(txtfechabaja.Text);
                    }

                    Ent.Ser_Cod_Autorizacion_SUNAT = txtcodsunat.Text;
                    Ent.Ser_Max_Lineas =Convert.ToInt32(txtmaxlineas.Text);

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();


                            txttipodoc.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        private void txtpdvcod_TextChanged(object sender, EventArgs e)
        {
            if (txtpdvcod.Focus() == false)
            {
                txtpdvdesc.ResetText();
            }
        }

        private void frm_series_comprobantes_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Limpiar();

                txttipodoc.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();

            }
        }

        public List<Entidad_Serie_Comprobante> Lista_Modificar = new List<Entidad_Serie_Comprobante>();
        public void ListarModificar()
        {
            Entidad_Serie_Comprobante Ent = new Entidad_Serie_Comprobante();
            Logica_Serie_Comprobante log = new Logica_Serie_Comprobante();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Ser_Tipo_Doc = Ser_Tipo_Doc;
            Ent.Ser_Serie = Ser_Serie;
            Ent.Ser_Numero_Ini = Ser_Numero_Ini;
            Ent.Ser_Numero_Fin = Ser_Numero_Ini;

            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Serie_Comprobante Enti = new Entidad_Serie_Comprobante();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;

                    txttipodoc.Text = Enti.Id_Sunat;
                    txttipodoc.Tag = Enti.Ser_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;

                    txtserie.Text   = Enti.Ser_Serie;
                    txtnuminicio.Text  = Enti.Ser_Numero_Ini ;
                    txtnumfin.Text = Enti.Ser_Numero_Fin ;
                    txtnumactual.Text  = Enti.Ser_Numero_Actual ;
                    txtpdvcod.Text = Enti.Ser_Pdv_Codigo ;
                    txtpdvdesc.Text = Enti.Pdv_Nombre;
                    txtfechaimpr.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ser_Fecha_Impresion);
                    txtfechabaja.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ser_Fecha_Baja);

                    txtcodsunat.Text = Enti.Ser_Cod_Autorizacion_SUNAT;
                    txtmaxlineas.Text =Convert.ToString(Enti.Ser_Max_Lineas);
                    chkelectronico.Checked = Enti.Es_Electronico;

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

    }
}
