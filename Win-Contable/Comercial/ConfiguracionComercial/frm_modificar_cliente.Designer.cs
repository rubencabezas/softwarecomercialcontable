﻿namespace Contable.Comercial.ConfiguracionComercial
{
    partial class frm_modificar_cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnaceptar = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDirecionCompleta = new DevExpress.XtraEditors.TextEdit();
            this.TxtEnt2Ape = new DevExpress.XtraEditors.TextEdit();
            this.TxtEnt1Ape = new DevExpress.XtraEditors.TextEdit();
            this.TxtEntNombre = new DevExpress.XtraEditors.TextEdit();
            this.TxtTipDocNumer = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirecionCompleta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt2Ape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt1Ape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocNumer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btncancelar
            // 
            this.btncancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.btncancelar.Appearance.Options.UseFont = true;
            this.btncancelar.Location = new System.Drawing.Point(366, 142);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(127, 51);
            this.btncancelar.TabIndex = 29;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // btnaceptar
            // 
            this.btnaceptar.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaceptar.Appearance.Options.UseFont = true;
            this.btnaceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnaceptar.Location = new System.Drawing.Point(232, 142);
            this.btnaceptar.Name = "btnaceptar";
            this.btnaceptar.Size = new System.Drawing.Size(127, 51);
            this.btnaceptar.TabIndex = 28;
            this.btnaceptar.Text = "Aceptar";
            this.btnaceptar.Click += new System.EventHandler(this.btnaceptar_Click);
            // 
            // TxtDirecionCompleta
            // 
            this.TxtDirecionCompleta.EnterMoveNextControl = true;
            this.TxtDirecionCompleta.Location = new System.Drawing.Point(97, 100);
            this.TxtDirecionCompleta.Name = "TxtDirecionCompleta";
            this.TxtDirecionCompleta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtDirecionCompleta.Properties.Appearance.Options.UseFont = true;
            this.TxtDirecionCompleta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtDirecionCompleta.Size = new System.Drawing.Size(782, 36);
            this.TxtDirecionCompleta.TabIndex = 27;
            // 
            // TxtEnt2Ape
            // 
            this.TxtEnt2Ape.EnterMoveNextControl = true;
            this.TxtEnt2Ape.Location = new System.Drawing.Point(447, 59);
            this.TxtEnt2Ape.Name = "TxtEnt2Ape";
            this.TxtEnt2Ape.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtEnt2Ape.Properties.Appearance.Options.UseFont = true;
            this.TxtEnt2Ape.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEnt2Ape.Size = new System.Drawing.Size(432, 36);
            this.TxtEnt2Ape.TabIndex = 25;
            // 
            // TxtEnt1Ape
            // 
            this.TxtEnt1Ape.EnterMoveNextControl = true;
            this.TxtEnt1Ape.Location = new System.Drawing.Point(97, 59);
            this.TxtEnt1Ape.Name = "TxtEnt1Ape";
            this.TxtEnt1Ape.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtEnt1Ape.Properties.Appearance.Options.UseFont = true;
            this.TxtEnt1Ape.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEnt1Ape.Size = new System.Drawing.Size(245, 36);
            this.TxtEnt1Ape.TabIndex = 23;
            // 
            // TxtEntNombre
            // 
            this.TxtEntNombre.EnterMoveNextControl = true;
            this.TxtEntNombre.Location = new System.Drawing.Point(444, 17);
            this.TxtEntNombre.Name = "TxtEntNombre";
            this.TxtEntNombre.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtEntNombre.Properties.Appearance.Options.UseFont = true;
            this.TxtEntNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEntNombre.Size = new System.Drawing.Size(435, 36);
            this.TxtEntNombre.TabIndex = 21;
            // 
            // TxtTipDocNumer
            // 
            this.TxtTipDocNumer.Enabled = false;
            this.TxtTipDocNumer.EnterMoveNextControl = true;
            this.TxtTipDocNumer.Location = new System.Drawing.Point(97, 18);
            this.TxtTipDocNumer.Name = "TxtTipDocNumer";
            this.TxtTipDocNumer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtTipDocNumer.Properties.Appearance.Options.UseFont = true;
            this.TxtTipDocNumer.Size = new System.Drawing.Size(245, 36);
            this.TxtTipDocNumer.TabIndex = 19;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(26, 111);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(65, 19);
            this.labelControl7.TabIndex = 26;
            this.labelControl7.Text = "Direccion";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(348, 70);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(93, 19);
            this.labelControl6.TabIndex = 24;
            this.labelControl6.Text = "Apellido Mat.";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(349, 28);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(92, 19);
            this.labelControl5.TabIndex = 20;
            this.labelControl5.Text = "RS / Nombre";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(34, 29);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(57, 19);
            this.labelControl4.TabIndex = 18;
            this.labelControl4.Text = "Numero";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(1, 70);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 19);
            this.labelControl3.TabIndex = 22;
            this.labelControl3.Text = "Apellido Pat.";
            // 
            // frm_modificar_cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 211);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.btnaceptar);
            this.Controls.Add(this.TxtDirecionCompleta);
            this.Controls.Add(this.TxtEnt2Ape);
            this.Controls.Add(this.TxtEnt1Ape);
            this.Controls.Add(this.TxtEntNombre);
            this.Controls.Add(this.TxtTipDocNumer);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Name = "frm_modificar_cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar";
            this.Load += new System.EventHandler(this.frm_modificar_cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirecionCompleta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt2Ape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt1Ape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocNumer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btncancelar;
        private DevExpress.XtraEditors.SimpleButton btnaceptar;
        public DevExpress.XtraEditors.TextEdit TxtDirecionCompleta;
        public DevExpress.XtraEditors.TextEdit TxtEnt2Ape;
        public DevExpress.XtraEditors.TextEdit TxtEnt1Ape;
        public DevExpress.XtraEditors.TextEdit TxtEntNombre;
        public DevExpress.XtraEditors.TextEdit TxtTipDocNumer;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
    }
}