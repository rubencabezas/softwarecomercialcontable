﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_punto_venta : frm_fuente
    {
        public frm_punto_venta()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_punto_venta_edicion f = new frm_punto_venta_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        public List<Entidad_Punto_Venta> Lista = new List<Entidad_Punto_Venta>();
        public void Listar()
        {
            Entidad_Punto_Venta Ent = new Entidad_Punto_Venta();
            Logica_Punto_Venta log = new Logica_Punto_Venta();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Pdv_Codigo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_punto_venta_edicion f = new frm_punto_venta_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Pdv_Codigo = Entidad.Pdv_Codigo;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }
        }
        Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

        private void dgvdatos_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        private void frm_punto_venta_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Pdv_Codigo + " " + Entidad.Pdv_Nombre;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Punto_Venta Ent = new Entidad_Punto_Venta
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Pdv_Codigo = Entidad.Pdv_Codigo
                    };

                    Logica_Punto_Venta log = new Logica_Punto_Venta();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        private void btnExportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                //string path = "C:\\ROFELIO PAUCAR";
                string Ruta;
                Ruta = path + ("\\punto_venta" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
