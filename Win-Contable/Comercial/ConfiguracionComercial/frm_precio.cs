﻿using Comercial.Busquedas_Generales;
using Contable;
using Contable._1_Busquedas_Generales;
using Contable._2_Alertas;
using Contable.Comercial.ConfiguracionComercial;
using DevExpress.Compression;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_precio : frm_fuente
    {
        public frm_precio()
        {
            InitializeComponent();
        }

        bool Es_Nuevo_Det=false;
        bool Es_Modificar_Det = false;
        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
            gbxcabecera.Enabled = true;
            gbxbotones.Enabled = true;

   
            Estado = Estados.Nuevo;
            btnmodificar.Enabled = false;//bloqeuado
            Btnguardar.Enabled = true;//bloqeuado
            btnnuevo.Enabled = false;

            Limpiar_Cabecera();
            Limpiar_Detalle();
            Detalles.Clear();
            dgvdatos.DataSource = null;   
            
            BuscarBSA("01");
            
            txtestcod.Select();
        }


        public List<Entidad_Lista_Precio> Lista = new List<Entidad_Lista_Precio>();
 

        Entidad_Lista_Precio Entidad = new Entidad_Lista_Precio();
        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {
                    //Estado = Estados.Ninguno;
                    Entidad = Detalles[gridView1.GetFocusedDataSourceRowIndex()];

                  
                    Id_Item= Entidad.Pre_Correltivo ;

                    txtunmcodpres.Text = Entidad.Pre_Id_Unm;
                    txtunmdescpres.Text = Entidad.Pre_Unm_Desc;

                    txtcondicioncod.Text = Entidad.Pre_Mod_Venta;
                    txtcondicioncod.Tag = Entidad.Pre_Id_Mod_Venta;
                    txtcondiciondesc.Text = Entidad.Pre_Mod_Venta_Desc;

                    txtmonedacod.Text = Entidad.Pre_Moneda_Sunat;
                    txtmonedacod.Tag = Entidad.Pre_Moneda_cod;
                    txtmonedadesc.Text = Entidad.Pre_Moneda_Desc;


                    txtfechaIni.Text = String.Format("{0:yyyy-MM-dd}", Entidad.Pre_Fecha_Ini);  //Convert.ToDateTime(Entidad.Pre_Fecha_Ini).ToString();
                   
                    txtfechaFin.Text = Convert.ToString(Entidad.Pre_Fecha_Fin);

                    chkesvariable.Checked = Entidad.Es_Precio_Variable;
                    txtpreciominimo.Text =Convert.ToString(Entidad.Pre_Precio_Minimo);
                    txtpreciomaximo.Text =Convert.ToString(Entidad.Pre_Precio_Maximo);

                    // Bloquear_Desbloquear_Det(false);

                    chkRequiereCodigoAutoriza.Checked = Entidad.Pre_Requiere_Codigo_Autoriza;
                    if (chkRequiereCodigoAutoriza.Checked)
                    {      
                        txtcodigoautoriza.Text = Accion.Desencriptar(Entidad.Pre_Codigo_Autoriza);

                    }
           


                    Es_Modificar_Det = false;
                    Es_Nuevo_Det = false;
                    gbxpresentacion.Enabled = false;

                }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Btnguardar.Enabled = true;
            btnnuevo.Enabled = false;
            Estado = Estados.Modificar;
            btnmodificar.Enabled = false;
            gbxbotones.Enabled = true;
        }

        private void frm_precio_Load(object sender, EventArgs e)
        {
           
            gbxcabecera.Enabled = false;
            gbxpresentacion.Enabled = false;
            gbxbotones.Enabled = false;

            txtpreciomaximo.Enabled = false;
            Es_Nuevo_Det = false;
            btnmodificar.Enabled = false;//bloqeuado
            Btnguardar.Enabled = false;//bloqeuado
            txtpreciominimo.Text = "0.00";
            txtpreciomaximo.Text = "0.00";
            chkRequiereCodigoAutoriza.Enabled = false;
            txtcodigoautoriza.Enabled = false;

        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Tipo_Descripcion + " " + Entidad.Id_Catalogo_Desc;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Est_Codigo = Entidad.Est_Codigo,
                        Id_Tipo=Entidad.Id_Tipo,
                        Id_Catalogo=Entidad.Id_Catalogo,
                        Pre_Tipo=Entidad.Pre_Tipo,
                        Pre_Correltivo=Entidad.Pre_Correltivo,
                        Pre_Moneda_cod=Entidad.Pre_Moneda_cod
                    };

                    Logica_Lista_Precio log = new Logica_Lista_Precio();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    //Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }



        private void txtestcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtestcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtestcod.Text.Substring(txtestcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_establecimiento_busqueda f = new frm_establecimiento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Establecimiento Entidad = new Entidad_Establecimiento();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtestcod.Text = Entidad.Est_Codigo.Trim();
                                txtestnombre.Text = Entidad.Est_Descripcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtestcod.Text) & string.IsNullOrEmpty(txtestnombre.Text))
                    {
                        BuscarEstablecimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEstablecimiento()
        {
            try
            {
                txtestcod.Text = Accion.Formato(txtestcod.Text, 2);

                Logica_Establecimiento log = new Logica_Establecimiento();

                List<Entidad_Establecimiento> Generales = new List<Entidad_Establecimiento>();

                Generales = log.Listar(new Entidad_Establecimiento
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Est_Codigo = txtestcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Establecimiento T in Generales)
                    {
                        if ((T.Est_Codigo).ToString().Trim().ToUpper() == txtestcod.Text.Trim().ToUpper())
                        {
                            txtestcod.Text = (T.Est_Codigo).ToString().Trim();
                            txtestnombre.Text = T.Est_Descripcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipobsacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipobsacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipobsacod.Text.Substring(txttipobsacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0011";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipobsacod.Tag = Entidad.Id_General_Det;
                                txttipobsacod.Text = Entidad.Gen_Codigo_Interno;
                                txttipobsadesc.Text = Entidad.Gen_Descripcion_Det;

                                txttipobsacod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipobsacod.Text) & string.IsNullOrEmpty(txttipobsadesc.Text))
                    {
                        txttipobsacod.Text = Accion.Formato(txttipobsacod.Text, 2);
                        BuscarBSA(txttipobsacod.Text.Trim());
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarBSA(string codigo)
        {
            try
            {
               
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0011",
                    Gen_Codigo_Interno = codigo
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == txttipobsacod.Text.Trim())
                        {
                            txttipobsacod.Tag = T.Id_General_Det;
                            txttipobsacod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txttipobsadesc.Text = T.Gen_Descripcion_Det;
                            txttipobsacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipobsacod.EnterMoveNextControl = false;
                    txttipobsacod.ResetText();
                    txttipobsacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_catalogo_busqueda f = new frm_catalogo_busqueda())
                        {
                            f.Tipo = txttipobsacod.Tag.ToString();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;
                                txtproductocod.EnterMoveNextControl = true;

                                Logica_Catalogo log = new Logica_Catalogo();
                                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                                Generales = log.Comprobar_Producto_ya_Registrado(new Entidad_Catalogo
                                {
                                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                                    Est_Codigo = txtestcod.Text.Trim(),
                                    Id_Tipo = txttipobsacod.Tag.ToString(),
                                    Id_Catalogo = txtproductocod.Text.Trim()
                                }) ;

                                if (Generales[0].Cantidad >= 1)
                                {
                                    Accion.Advertencia("Este producto ya fue registrado en la lista de precio,consultar en busqueda avanzada y luego añadir un nuevo precio");
                                       btncancelar.PerformClick();
                                }
                                else
                                {

                                    BuscarUnm_Defecto_Producto();
                                }
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = txttipobsacod.Tag.ToString(),
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {

                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;

                            Logica_Catalogo logi = new Logica_Catalogo();
                            List<Entidad_Catalogo> Generaless = new List<Entidad_Catalogo>();
                            Generaless = logi.Comprobar_Producto_ya_Registrado(new Entidad_Catalogo
                            {
                                Id_Empresa = Actual_Conexion.CodigoEmpresa,
                                Est_Codigo = txtestcod.Text.Trim(),
                                Id_Tipo = txttipobsacod.Tag.ToString(),
                                Id_Catalogo = txtproductocod.Text.Trim()
                            });

                            if (Generaless[0].Cantidad >=1)
                            {
                                Accion.Advertencia("Este producto ya fue registrado en la lista de precio,consultar en busqueda avanzada y luego añadir un nuevo precio");
                                btncancelar.PerformClick();
                            }
                            else
                            {

                                BuscarUnm_Defecto_Producto();
                                txtproductocod.EnterMoveNextControl = true;
                            }


                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.ResetText();
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void BuscarUnm_Defecto_Producto()
        {
            try
            {

                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Catalogo = txtproductocod.Text.Trim(),
                    Unm_Defecto = true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Unm_Defecto) == true)
                        {
                            txtunmcodpres.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdescpres.Text = T.Und_Descripcion;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtmonedacod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtmonedacod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtmonedacod.Text.Substring(txtmonedacod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_moneda_busqueda f = new frm_moneda_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Moneda Entidad = new Entidad_Moneda();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtmonedacod.Tag = Entidad.Id_Moneda.Trim();

                                txtmonedacod.Text = Entidad.Id_Sunat.Trim();
                                txtmonedadesc.Text = Entidad.Nombre_Moneda;


                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtmonedacod.Text) & string.IsNullOrEmpty(txtmonedadesc.Text))
                    {
                        BuscarMoneda();

                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarMoneda()
        {
            try
            {
                //txtmonedacod.Text = Accion.Formato(txtmonedacod.Text, 1);
                Logica_Moneda log = new Logica_Moneda();

                List<Entidad_Moneda> Generales = new List<Entidad_Moneda>();
                Generales = log.Listar(new Entidad_Moneda
                {
                    Id_Sunat = "1"
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Moneda T in Generales)
                    {
                        if ((T.Id_Sunat).ToString().Trim().ToUpper() == "1")
                        {
                            txtmonedacod.Text = (T.Id_Sunat).ToString().Trim();
                            txtmonedacod.Tag = (T.Id_Moneda).ToString().Trim();
                            txtmonedadesc.Text = T.Nombre_Moneda;
                            txtmonedacod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtmonedacod.EnterMoveNextControl = false;
                    txtmonedacod.ResetText();
                    txtmonedacod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtcondicioncod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtcondicioncod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtcondicioncod.Text.Substring(txtcondicioncod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_generales_busqueda f = new frm_generales_busqueda())
                        {
                            f.Id_General = "0003";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_General Entidad = new Entidad_General();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtcondicioncod.Tag = Entidad.Id_General_Det;
                                txtcondicioncod.Text = Entidad.Gen_Codigo_Interno;
                                txtcondiciondesc.Text = Entidad.Gen_Descripcion_Det;

                                txtcondicioncod.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtcondicioncod.Text) & string.IsNullOrEmpty(txtcondiciondesc.Text))
                    {
                        BuscarCondicion();

                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCondicion()
        {
            try
            {
                //txtcondicioncod.Text = Accion.Formato(txtcondicioncod.Text, 2);
                Logica_General log = new Logica_General();

                List<Entidad_General> Generales = new List<Entidad_General>();
                Generales = log.Listar(new Entidad_General
                {
                    Id_General = "0003",
                    Gen_Codigo_Interno = "01"
                });

                if (Generales.Count > 0)
                {
                    foreach (Entidad_General T in Generales)
                    {
                        if ((T.Gen_Codigo_Interno).ToString().Trim().ToUpper() == "01")
                        {
                            txtcondicioncod.Tag = T.Id_General_Det;
                            txtcondicioncod.Text = (T.Gen_Codigo_Interno).ToString().Trim();
                            txtcondiciondesc.Text = T.Gen_Descripcion_Det;
                            txtcondicioncod.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtcondicioncod.EnterMoveNextControl = false;
                    txtcondicioncod.ResetText();
                    txtcondicioncod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtestcod_TextChanged(object sender, EventArgs e)
        {
            if (txtestcod.Focus() == false)
            {
                txtestnombre.ResetText();
            }
        }

        private void txttipobsacod_TextChanged(object sender, EventArgs e)
        {
            if (txttipobsacod.Focus() == false)
            {
                txttipobsadesc.ResetText();
            }
        }

        private void txtproductocod_TextChanged(object sender, EventArgs e)
        {
            if (txtproductocod.Focus() == false)
            {
                txtproductodesc.ResetText();
            }
        }

        int Id_Item;
        private void btnnuevodet_Click(object sender, EventArgs e)
        {
            Limpiar_Detalle();
           gbxpresentacion.Enabled = true;
           
            BuscarCondicion();
            BuscarMoneda();
            Id_Item = Detalles.Count + 1;
            Es_Nuevo_Det = true;
            Bloquear_Desbloquear_Det(true);
             txtunmcodpres.Select();

        }

        public bool VerificarCabecera()
        {
            
            if (string.IsNullOrEmpty(txtestnombre.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un punto de venta");
                txtestcod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtproductodesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un punto de venta");
                txtproductocod.Focus();
                return false;
            }

 
            return true;
        }
        private void Btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                if (VerificarCabecera())
                {
                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
                    Logica_Lista_Precio Log = new Logica_Lista_Precio();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Est_Codigo = txtestcod.Text;
                    Ent.Id_Tipo = txttipobsacod.Tag.ToString();
                    Ent.Id_Catalogo = txtproductocod.Text;

                    Ent.Detalles_Precios = Detalles;

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {
                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;

                            gbxcabecera.Enabled = false;
                            Btnguardar.Enabled = false;
                            btnnuevo.Enabled = true;
                            btnmodificar.Enabled = true;

                        }
                    }else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoGuardar();

                            Logica_Lista_Precio Logi = new Logica_Lista_Precio();
                            Detalles.Clear();
                            Detalles = Logi.Listar_Producto_Con_Precio_Det(Ent);
                            if (Detalles.Count > 0)
                            {
                                dgvdatos.DataSource = Detalles;

                                gbxbotones.Enabled = false;
                                Btnguardar.Enabled = false;
                                btnnuevo.Enabled = true;
                                btnmodificar.Enabled = true;
                            }

                        }

                    }


                }


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtunmcodpres_KeyDown(object sender, KeyEventArgs e)
        {

            if (String.IsNullOrEmpty(txtunmcodpres.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtunmcodpres.Text.Substring(txtunmcodpres.Text.Length - 1, 1) == "*")
                    {
                        using (frm_unidad_medida_busqueda f = new frm_unidad_medida_busqueda())
                        {
                            f.Producto_Cod = txtproductocod.Text;
                            f.Producto_Unm = true;
                            f.tipo = txttipobsacod.Tag.ToString().Trim();

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Unidad_Medida Entidad = new Entidad_Unidad_Medida();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtunmcodpres.Text = Entidad.Id_Unidad_Medida;
                                txtunmdescpres.Text = Entidad.Und_Descripcion;
                                txtunmcodpres.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtunmcodpres.Text) & string.IsNullOrEmpty(txtunmdescpres.Text))
                    {
                        BuscarUnmPres();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarUnmPres()
        {
            try
            {
                txtunmcodpres.Text = Accion.Formato(txtunmcodpres.Text, 3);
                Logica_Unidad_Medida log = new Logica_Unidad_Medida();

                List<Entidad_Unidad_Medida> Generales = new List<Entidad_Unidad_Medida>();
                Generales = log.Listar_Unm_Producto(new Entidad_Unidad_Medida
                {
                    Id_Empresa=Actual_Conexion.CodigoEmpresa,
                    Id_Unidad_Medida = txtunmcodpres.Text,
                    Id_Catalogo = txtproductocod.Text.Trim(),
                    Id_Tipo=txttipobsacod.Tag.ToString().Trim(),
                    Unm_Defecto=true
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Unidad_Medida T in Generales)
                    {
                        if ((T.Id_Unidad_Medida).ToString().Trim().ToUpper() == txtunmcodpres.Text.Trim().ToUpper())
                        {
                            txtunmcodpres.Text = (T.Id_Unidad_Medida).ToString().Trim();
                            txtunmdescpres.Text = T.Und_Descripcion;
                            txtunmcodpres.EnterMoveNextControl = true;
                        }
                    }

                }else
                {
                    Accion.Advertencia("No existe este codigo de unidad de medida configurado");
                    txtunmcodpres.EnterMoveNextControl = false;
                    txtunmcodpres.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void chkesvariable_CheckedChanged(object sender, EventArgs e)
        {
            if (chkesvariable.Checked)
            {
                txtpreciomaximo.Enabled = true;
                //txtpreciomaximo.Text="0.00";
                chkRequiereCodigoAutoriza.Enabled = true;
                //txtcodigoautoriza.Enabled = true;
            }
            else
            {
                txtpreciomaximo.Enabled = false;
                txtpreciomaximo.Text = "0.00";

                chkRequiereCodigoAutoriza.Enabled = false;
                txtcodigoautoriza.Enabled = false;
                chkRequiereCodigoAutoriza.Checked = false;
                txtcodigoautoriza.ResetText();
            }
        }
        List<Entidad_Lista_Precio> Detalles = new List<Entidad_Lista_Precio>();
   



        public void UpdateGrilla()
        {
            dgvdatos.DataSource = null;
            if (Detalles.Count > 0)
            {
                dgvdatos.DataSource = Detalles;
            }
        }


        public bool VerificarDetalle()
        {

            if (string.IsNullOrEmpty(txtunmdescpres.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una unidad de medida");
                txtunmcodpres.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txttipobsadesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo");
                txttipobsacod.Focus();
                return false;
            }


         if (chkesvariable.Checked)
            {
                if (Convert.ToDecimal(txtpreciomaximo.Text) == 0)
                {
                    Accion.Advertencia("Debe ingresar un precio maximo");
                    txtpreciomaximo.Focus();
                    return false;
                }
            }

         if (chkRequiereCodigoAutoriza.Checked)
            {
                if (string.IsNullOrEmpty(txtcodigoautoriza.Text.Trim()))
                {
                    Accion.Advertencia("Debe ingresar un código de autorización,");
                    txtcodigoautoriza.Focus();
                    return false;
                }
            }

            return true;
        }

        private void btnagregardet_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarDetalle())
                {

                    //Validar y verificar antes de añadir a la lista 
                    //si existe 

                    Entidad_Lista_Precio Entidad = new Entidad_Lista_Precio();
                    Logica_Lista_Precio Log = new Logica_Lista_Precio();
                    Entidad.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Entidad.Est_Codigo = txtestcod.Text;
                    Entidad.Id_Tipo = txttipobsacod.Tag.ToString();
                    Entidad.Id_Catalogo = txtproductocod.Text;
                    Entidad.Pre_Id_Unm = txtunmcodpres.Text;
                    Entidad.Pre_Id_Mod_Venta = txtcondicioncod.Tag.ToString();
                    Entidad.Pre_Moneda_cod = txtmonedacod.Tag.ToString();
                    Entidad.Pre_Fecha_Ini = Convert.ToDateTime(txtfechaIni.Text);

                    //if (Log.Verificar_producto_Fecha_Modalidad(Entidad))
                    //{


                        //if (Entidad.Mensaje == "")
                        //{

                            Entidad_Lista_Precio ItemDetalle = new Entidad_Lista_Precio();
                            
                            ItemDetalle.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                            ItemDetalle.Pre_Correltivo = Id_Item;
                        
                            ItemDetalle.Est_Codigo= txtestcod.Text;
                            ItemDetalle.Id_Tipo = txttipobsacod.Tag.ToString();
                            ItemDetalle.Id_Catalogo = txtproductocod.Text;

                            ItemDetalle.Pre_Id_Unm = txtunmcodpres.Text.ToString();
                            ItemDetalle.Pre_Unm_Desc = txtunmdescpres.Text.Trim().ToString();


                            ItemDetalle.Pre_Mod_Venta = txtcondicioncod.Text.Trim().ToString();
                            ItemDetalle.Pre_Id_Mod_Venta = txtcondicioncod.Tag.ToString();
                            ItemDetalle.Pre_Mod_Venta_Desc = txtcondiciondesc.Text.Trim().ToString();



                            ItemDetalle.Pre_Moneda_Sunat = txtmonedacod.Text.Trim().ToString();
                            ItemDetalle.Pre_Moneda_cod = txtmonedacod.Tag.ToString();
                            ItemDetalle.Pre_Moneda_Desc = txtmonedadesc.Text.Trim().ToString();

                            ItemDetalle.Pre_Fecha_Ini = Convert.ToDateTime(txtfechaIni.Text);

                            if (txtfechaFin.Text == "" || txtfechaFin.Text == "01/01/1900")
                            {
                                ItemDetalle.Pre_Fecha_Fin = Convert.ToDateTime("01/01/1900");
                            }
                            else
                            {
                                ItemDetalle.Pre_Fecha_Fin = Convert.ToDateTime(txtfechaFin.Text);
                            }

                            ItemDetalle.Es_Precio_Variable = chkesvariable.Checked;

                            ItemDetalle.Pre_Precio_Minimo = Convert.ToDecimal(txtpreciominimo.Text);
                            ItemDetalle.Pre_Precio_Maximo = Convert.ToDecimal(IIf(txtpreciomaximo.Text == "", 0, txtpreciomaximo.Text));


                            ItemDetalle.Pre_Requiere_Codigo_Autoriza = chkRequiereCodigoAutoriza.Checked;
                            string codAutoriza = Accion.Encriptar(txtcodigoautoriza.Text.Trim());
                            ItemDetalle.Pre_Codigo_Autoriza = codAutoriza;


                            if (Es_Nuevo_Det == true)
                            {
                                Detalles.Add(ItemDetalle);
                                UpdateGrilla();
                                EstadoDetalle = Estados.Guardado;
                            }
                            else if (Es_Modificar_Det == true)
                            {

                                Detalles[Convert.ToInt32(Id_Item) - 1] = ItemDetalle;
                                UpdateGrilla();
                                EstadoDetalle = Estados.Guardado;

                            }

                            Limpiar_Detalle();

                            Bloquear_Desbloquear_Det(false);
                            Es_Modificar_Det = false;
                            Es_Nuevo_Det = false;
                            btnnuevodet.Select();
                //        }
                //else
                //{
                //    Accion.Advertencia(Entidad.Mensaje);
                //}
            //}




                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }
        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        void Limpiar_Detalle()
        {
            txtunmcodpres.ResetText();
            txtunmdescpres.ResetText();
            txtcondicioncod.ResetText();
            txtcondiciondesc.ResetText();
            txtmonedacod.ResetText();
            txtmonedadesc.ResetText();
            txtfechaIni.ResetText();
            txtfechaFin.ResetText();
            chkesvariable.Checked=false;
            txtpreciominimo.Text = "0.00";
            txtpreciomaximo.Text = "0.00";

        }

        void Limpiar_Cabecera()
        {
            txtestcod.ResetText();
            txtestnombre.ResetText();
            txttipobsacod.ResetText();
            txttipobsadesc.ResetText();
            txtproductocod.ResetText();
            txtproductodesc.ResetText();
        }


        void Bloquear_Desbloquear_Det(bool Valor)
        {
            txtunmcodpres.Enabled=Valor;
            //txtcondicioncod.Enabled = Valor;
            //txtmonedacod.Enabled = Valor;
           
            txtfechaIni.Enabled = Valor;
            chkesvariable.Enabled = Valor;
            txtpreciominimo.Enabled = Valor;
            if (chkesvariable.Checked)
            {
                txtpreciomaximo.Enabled = true;
            }
            else
            {
                txtpreciomaximo.Enabled = false;
            }
        }

        private void btneditardet_Click(object sender, EventArgs e)
        {

            try
            {
                // if (Convert.ToString(Entidad.Pre_Fecha_Fin) == "01/01/1900 00:00:00")
                //{
                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
                    Logica_Lista_Precio Log = new Logica_Lista_Precio();
                    List<Entidad_Lista_Precio> Lista_Prec_Config_Posterior = new List<Entidad_Lista_Precio>();
                    Lista_Prec_Config_Posterior = Log.Buscar_Config_Precios_Posteriores(Entidad);
          
                    if (Lista_Prec_Config_Posterior[0].Cant_Precio_Posterior == 1)
                    {
                            gbxpresentacion.Enabled = true;
                            Bloquear_Desbloquear_Det(true);
                            Es_Modificar_Det = true;
                    }
                    else
                    {
                        Accion.Advertencia("Existen precios posteriores configurados ,No puede editar");
                    }
                //} else
                //    {
                //        Accion.Advertencia("Existen precios posteriores configurados ,No puede editar");
                //    }
           


            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


 


        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            try
            {
                if (Detalles.Count > 0)
                {

                    Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
                    Logica_Lista_Precio Log = new Logica_Lista_Precio();
                    List<Entidad_Lista_Precio> Lista_Prec_Config_Posterior = new List<Entidad_Lista_Precio>();
                    Lista_Prec_Config_Posterior = Log.Buscar_Config_Precios_Posteriores(Entidad);
                    if (Lista_Prec_Config_Posterior[0].Cant_Precio_Posterior == 1)
                    {
                            Detalles.RemoveAt(gridView1.GetFocusedDataSourceRowIndex());

                            dgvdatos.DataSource = null;
                            if (Detalles.Count > 0)
                            {
                                dgvdatos.DataSource = Detalles;
                                RefreshNumeral();
                            }

                            Limpiar_Detalle();
                    }else
                    {
                        Accion.Advertencia("Existen precios posteriores configurados ,debe eliminar el ultimo");

                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        public void RefreshNumeral()
        {
            int NumOrden = 1;
            foreach (Entidad_Lista_Precio Det in Detalles)
            {
                Det.Pre_Correltivo = NumOrden;
                NumOrden += 1;
            }
        }

        private void btnbusquedaavanzada_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {
                   //p.Start();

                    Buscar();

                    //p.Abort();
            }catch(Exception ex)
            {
                p.Abort();
            }
 
        }

        void Buscar()
        {
            using (frm_producto_con_precio f = new frm_producto_con_precio())
            {

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Lista_Precio Entidad = new Entidad_Lista_Precio();

                    Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                    txtestcod.Text = Entidad.Est_Codigo;

                    txtestnombre.Text = Entidad.Est_Descripcion;
                    txttipobsacod.Text = Entidad.Id_Tipo_Interno;
                    txttipobsacod.Tag = Entidad.Id_Tipo;
                    txttipobsadesc.Text = Entidad.Id_Tipo_Descripcion;
                    txtproductocod.Text = Entidad.Id_Catalogo;
                    txtproductodesc.Text = Entidad.Id_Catalogo_Desc;

                    Logica_Lista_Precio Log = new Logica_Lista_Precio();

                    Detalles = Log.Listar_Producto_Con_Precio_Det(Entidad);

                    btnmodificar.Enabled = true;//desbloqeuado

                    if (Detalles.Count > 0)
                    {
                        dgvdatos.DataSource = Detalles;

                        
                        Btnguardar.Enabled = false;//desbloqeuado
                        btnnuevo.Enabled = false;
                    }
                }
            }
        }

        private void btncancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnmodificar.Enabled = false;//bloqeuado
            Btnguardar.Enabled = false;//bloqeuado
            btnnuevo.Enabled = true;//bloqeuado
            gbxcabecera.Enabled = false;
            gbxpresentacion.Enabled = false;
            gbxbotones.Enabled = false;

            Limpiar_Cabecera();
            Limpiar_Detalle();
            dgvdatos.DataSource = null;
            Detalles.Clear();
        }

        private void txtunmcodpres_TextChanged(object sender, EventArgs e)
        {
            if (txtunmcodpres.Focus() == false)
            {
                txtunmdescpres.ResetText();
            }
        }

        private void btnexportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\establcimiento" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void Progreso()
        {
            WaitForm1 dlg = new WaitForm1();
            dlg.ShowDialog();
        }

        private void txtproductocod_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtpreciominimo_KeyDown(object sender, KeyEventArgs e)
        {
            //SendKeys.Send("+{TAB}");
            //e.Handled = true;
        }

        private void txtpreciomaximo_KeyDown(object sender, KeyEventArgs e)
        {
            //SendKeys.Send("+{TAB}");
            //e.Handled = true;
        }

        private void txtpreciominimo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (txtpreciomaximo.Enabled == false)
                {

                    btnagregardet.Select();
                }
                else
                {
                    txtpreciomaximo.Select();
                }
            }
        }

        private void txtpreciomaximo_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtpreciomaximo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (chkesvariable.Checked)
                {
                    if (txtpreciomaximo.Enabled == false)
                    {
                        chkRequiereCodigoAutoriza.Select();
                    }
                }
               // btnagregardet.Select();

            }
        }

        private void chkRequiereCodigoAutoriza_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (txtcodigoautoriza.Enabled == false)
                {

                    btnagregardet.Select();
                }
                else
                {
                    txtcodigoautoriza.Select();
                }
            }
        }

        private void txtcodigoautoriza_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {

                btnagregardet.Select();

            }
        }

        private void chkRequiereCodigoAutoriza_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRequiereCodigoAutoriza.Checked)
            {
                txtcodigoautoriza.Enabled = true;
                txtcodigoautoriza.Focus();
            }
            else
            {
                txtcodigoautoriza.ResetText();
                txtcodigoautoriza.Enabled = false;
            }
        }

        private void btnexportardata_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (frm_establecimiento_parametros ff = new frm_establecimiento_parametros())
                {

                    if (ff.ShowDialog() == DialogResult.OK)
                    {


                        System.Threading.Thread p = new System.Threading.Thread((this.Progreso));
                        try
                        {

                            //p.Start();


                            Logica_Exportar_Importar log = new Logica_Exportar_Importar();
                            Entidad_Exportar entidad = new Entidad_Exportar();
                            entidad.Empresa = Actual_Conexion.CodigoEmpresa;
                            entidad.establecimiento = ff.txtestcod.Text.Trim();
               

                            //CATALOGO
                            DataSet VEN_LISTA_PRECIO_CAB = new DataSet();
                            DataSet VEN_LISTA_PRECIO_DET = new DataSet();
            

                            VEN_LISTA_PRECIO_CAB = log.EXPORTAR_VEN_LISTA_PRECIO_CAB_XML(entidad);
                            VEN_LISTA_PRECIO_DET = log.EXPORTAR_VEN_LISTA_PRECIO_DET_XML(entidad);
                  

                            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                            string Nombre = "PRECIOS";// + EstCod.ToUpper.Trim + " " + FechaServer();
                            string Ruta;
                            Ruta = desktopPath + @"\" + Nombre;

                            File.Delete(Ruta + ".rar");
                            if (!Directory.Exists(Ruta))
                            {
                                Directory.CreateDirectory(Ruta);
                            }
                            else
                            {
                                foreach (var f in Directory.GetFiles(Ruta))
                                    File.Delete(Conversions.ToString(f));
                            }

                            string precio_cab = "VEN_LISTA_PRECIO_CAB";
                            string precio_det = "VEN_LISTA_PRECIO_DET";
 

                            File.WriteAllText(Ruta + @"\" + precio_cab + ".csv", ConvertToCSV(VEN_LISTA_PRECIO_CAB.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + precio_det + ".csv", ConvertToCSV(VEN_LISTA_PRECIO_DET.Tables[0]));
 

                            var files = Directory.GetFiles(Ruta);
                            string Password = Actual_Conexion.RucEmpresa.Trim();
                            Comprimir(files, Ruta, Accion.Encriptar(Password));



                            //p.Abort();
                        }
                        catch (Exception ex)
                        {
                            //p.Abort();
                            Accion.ErrorSistema(ex.Message);
                        }




                    }


                }
            }
            catch (Exception ex)
            {

            }
        }


 


        public static void Comprimir(string[] Archivos, string Ruta, string password)
        {
            using (var archive = new ZipArchive())
            {
                foreach (string file in Archivos)
                {
                    ZipFileItem zipFI = archive.AddFile(file, "/");
                    zipFI.EncryptionType = EncryptionType.Aes128;
                    zipFI.Password = password;
                }

                archive.Save(Ruta + ".rar");
                // File.Move(Ruta + ".rar", Ruta + ".xml");
                archive.Dispose();
            }
        }


        private static string ConvertToCSV(DataTable oDt)
        {
            var sb = new StringBuilder();
            var columnNames = oDt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
            sb.AppendLine(string.Join("|", columnNames));
            foreach (DataRow row in oDt.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                sb.AppendLine(string.Join("|", fields));
            }

            return sb.ToString();
        }

        string RutaArchivo = "";

        private void btnimportardata_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {


                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "ZIP Folder (.rar)|*.rar";
                openFileDialog1.ShowDialog();
                openFileDialog1.Multiselect = true;
                RutaArchivo = openFileDialog1.FileName;

                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Nombre = Path.GetFileNameWithoutExtension(RutaArchivo);
                string Destino;
                Destino = desktopPath + @"\" + Nombre;

                p.Start();

                if (!Directory.Exists(Destino))
                {
                    Directory.CreateDirectory(Destino);
                }

                if (Directory.Exists(Destino))
                {
                    foreach (var f in Directory.GetFiles(Destino))

                        File.Delete(Conversions.ToString(f));
                    string Password = Actual_Conexion.RucEmpresa.Trim();
                    if (!Descomprimir(RutaArchivo, Destino, Accion.Encriptar(Password)))
                    {
                        Directory.Delete(Destino, true);
                    }
                }


                if (Directory.Exists(Destino))
                {
                    // Aki empesamos a importar los archivos
                    Importar_Compras(Destino);

                }

                p.Abort();
            }
            catch (Exception ex)
            {
                p.Abort();
                Accion.ErrorSistema(ex.Message);
            }
        }



        public void Importar_Compras(string Destino)
        {
            if (File.Exists(Destino + @"\" + "VEN_LISTA_PRECIO_CAB.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "VEN_LISTA_PRECIO_CAB.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_VEN_LISTA_PRECIO_CAB_PRECIO_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "VEN_LISTA_PRECIO_DET.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "VEN_LISTA_PRECIO_DET.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_VEN_LISTA_PRECIO_DET_PRECIO_XML(Dt);
            }

        


        }


        private static DataTable GetDataTableFromCsv(string path__1, bool isFirstRowHeader)
        {
            var csvData = new DataTable();
            using (var csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(path__1))
            {
                csvReader.SetDelimiters(new string[] { "|" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                var colFields = csvReader.ReadFields();
                foreach (var Column in colFields)
                {
                    var datecolumn = new DataColumn(Column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }

                int Cont = 2;
                while (!csvReader.EndOfData)
                {
                    var fieldData = csvReader.ReadFields();
                    // Making empty value as null
                    if (fieldData.Count() != csvData.Columns.Count)
                    {
                        Accion.Advertencia("ARCHIVO CON FALLAS,El archivo " + Path.GetFileName(path__1) + " En la linea " + Cont + " la cantidad de columnas de cabecera es " + csvData.Columns.Count + " la fila presenta " + fieldData.Count());
                    }

                    for (int i = 0, loopTo = fieldData.Length - 1; i <= loopTo; i++)
                    {
                        if (string.IsNullOrEmpty(fieldData[i]))
                        {
                            fieldData[i] = null;
                        }
                    }

                    csvData.Rows.Add(fieldData);
                    Cont = Cont + 1;
                }
            }

            return csvData;
        }


        public static bool Descomprimir(string ArchivoRar, string RutaDescomprimir, string password)
        {
            using (ZipArchive archive = ZipArchive.Read(ArchivoRar))
            {
                foreach (ZipItem item in archive)
                {
                    try
                    {
                        item.Password = password;
                        item.Extract(RutaDescomprimir);
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            return true;
        }




    }
}
