﻿using Contable;
using Contable._1_Busquedas_Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_documentos_pdv_edicion :frm_fuente
    {
        public frm_documentos_pdv_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa, Pdv_Codigo, Doc_Codigo;

        void Limpiar()
        {
            txtestcod.ResetText();
            txtestnombre.ResetText();
            txtpdvcod.ResetText();
            txtpdvdesc.ResetText();
            txttipodoc.ResetText();
            txttipodocdesc.ResetText();
            txttipodocAsoc.ResetText();
            chkcreditofiscal.Checked = false;
            rbdocdefecto.Checked = false;
            rbfactura.Checked = false;
            rbboleta.Checked = false;
            rbnota.Checked = false;

        }
        private void txtestcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtestcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtestcod.Text.Substring(txtestcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_establecimiento_busqueda f = new frm_establecimiento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Establecimiento Entidad = new Entidad_Establecimiento();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtestcod.Text = Entidad.Est_Codigo.Trim();
                                txtestnombre.Text = Entidad.Est_Descripcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtestcod.Text) & string.IsNullOrEmpty(txtestnombre.Text))
                    {
                        BuscarEstablecimiento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarEstablecimiento()
        {
            try
            {
                txtestcod.Text = Accion.Formato(txtestcod.Text, 2);

                Logica_Establecimiento log = new Logica_Establecimiento();

                List<Entidad_Establecimiento> Generales = new List<Entidad_Establecimiento>();

                Generales = log.Listar(new Entidad_Establecimiento
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Est_Codigo = txtestcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Establecimiento T in Generales)
                    {
                        if ((T.Est_Codigo).ToString().Trim().ToUpper() == txtestcod.Text.Trim().ToUpper())
                        {
                            txtestcod.Text = (T.Est_Codigo).ToString().Trim();
                            txtestnombre.Text = T.Est_Descripcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtpdvcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpdvcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpdvcod.Text.Substring(txtpdvcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_punto_venta_busqueda f = new frm_punto_venta_busqueda())
                        {
                            f.Est_Codigo = txtestcod.Text;

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpdvcod.Text = Entidad.Pdv_Codigo.Trim();
                                txtpdvdesc.Text = Entidad.Pdv_Nombre.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpdvcod.Text) & string.IsNullOrEmpty(txtpdvdesc.Text))
                    {
                        BuscarPuntoVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarPuntoVenta()
        {
            try
            {
                txtpdvcod.Text = Accion.Formato(txtpdvcod.Text, 2);

                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Generales = new List<Entidad_Punto_Venta>();

                Generales = log.Listar(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Est_Codigo = txtestcod.Text,
                    Pdv_Codigo=txtpdvcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Punto_Venta T in Generales)
                    {
                        if ((T.Pdv_Codigo).ToString().Trim().ToUpper() == txtpdvcod.Text.Trim().ToUpper())
                        {
                            txtpdvcod.Text = (T.Pdv_Codigo).ToString().Trim();
                            txtpdvdesc.Text = T.Pdv_Nombre.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodoc.Text.Substring(txttipodoc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_tipo_documento_busqueda f = new frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodoc.Text = Entidad.Id_SUnat;
                                txttipodoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdesc.Text = Entidad.Nombre_Comprobante;
                                txttipodoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodoc.Text) & string.IsNullOrEmpty(txttipodocdesc.Text))
                    {
                        BuscarTipoDocumento();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }



        public void BuscarTipoDocumento()
        {
            try
            {
                txttipodoc.Text = Accion.Formato(txttipodoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodoc.Text.Trim().ToUpper())
                        {
                            txttipodoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdesc.Text = T.Nombre_Comprobante;
                            txttipodoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodoc.EnterMoveNextControl = false;
                    txttipodoc.ResetText();
                    txttipodoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txttipodocAsoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txttipodocAsoc.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txttipodocAsoc.Text.Substring(txttipodocAsoc.Text.Length - 1, 1) == "*")
                    {
                        using (frm_tipo_documento_busqueda f = new frm_tipo_documento_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Comprobantes Entidad = new Entidad_Comprobantes();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txttipodocAsoc.Text = Entidad.Id_SUnat;
                                txttipodocAsoc.Tag = Entidad.Id_Comprobante;
                                txttipodocdescAsoc.Text = Entidad.Nombre_Comprobante;
                                txttipodocAsoc.EnterMoveNextControl = true;
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txttipodocAsoc.Text) & string.IsNullOrEmpty(txttipodocdescAsoc.Text))
                    {
                        BuscarTipoDocumentoAsoc();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }


        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtestnombre.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un establecimiento");
                txtestcod.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtpdvdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un punto de venta");
                txtpdvcod.Focus();
                return false;
            }


            if (string.IsNullOrEmpty(txttipodocdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un tipo de documento");
                txttipodoc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttipodocdescAsoc.Text.Trim()))
            {
                Accion.Advertencia("Debe asociar a un tipo de documento");
                txttipodocAsoc.Focus();
                return false;
            }

            return true;
        }

        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Documentos_Punto_Venta Ent = new Entidad_Documentos_Punto_Venta();
                    Logica_Documentos_Punto_Venta Log = new Logica_Documentos_Punto_Venta();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Doc_Codigo = Doc_Codigo;
                    Ent.Pdv_Codigo = txtpdvcod.Text;

                    Ent.Est_Codigo = txtestcod.Text;
                    Ent.Pdv_Nombre = txtpdvcod.Text;
                    Ent.Doc_Tipo_Doc = txttipodoc.Tag.ToString();

                    Ent.Doc_Tipo_Doc_Asoc = txttipodocAsoc.Tag.ToString();
                    Ent.Doc_Credito_Fiscal = chkcreditofiscal.Checked;
                    Ent.Doc_Por_Defecto = rbdocdefecto.Checked;
                    Ent.Doc_Es_Factura = rbfactura.Checked;
                    Ent.Doc_Es_Boleta = rbboleta.Checked;
                    Ent.Doc_Es_Nota_Salida = rbnota.Checked;



                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();


                            txtestcod.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public void BuscarTipoDocumentoAsoc()
        {
            try
            {
                txttipodocAsoc.Text = Accion.Formato(txttipodocAsoc.Text, 2);
                Logica_Comprobante log = new Logica_Comprobante();

                List<Entidad_Comprobantes> Generales = new List<Entidad_Comprobantes>();
                Generales = log.Listar(new Entidad_Comprobantes
                {
                    Id_SUnat = txttipodocAsoc.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Comprobantes T in Generales)
                    {
                        if ((T.Id_SUnat).ToString().Trim().ToUpper() == txttipodocAsoc.Text.Trim().ToUpper())
                        {
                            txttipodocAsoc.Text = (T.Id_SUnat).ToString().Trim();
                            txttipodocAsoc.Tag = (T.Id_Comprobante).ToString().Trim();
                            txttipodocdescAsoc.Text = T.Nombre_Comprobante;
                            txttipodocAsoc.EnterMoveNextControl = true;
                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txttipodocAsoc.EnterMoveNextControl = false;
                    txttipodocAsoc.ResetText();
                    txttipodocAsoc.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_documentos_pdv_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Limpiar();

                txtestcod.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();

            }
        }


        public List<Entidad_Documentos_Punto_Venta> Lista_Modificar = new List<Entidad_Documentos_Punto_Venta>();

        private void txtestcod_TextChanged(object sender, EventArgs e)
        {
            if (txtestcod.Focus() == false)
            {
                txtestnombre.ResetText();
            }
        }

        private void txtpdvcod_TextChanged(object sender, EventArgs e)
        {
            if (txtpdvcod.Focus() == false)
            {
                txtpdvdesc.ResetText();
            }
        }

        private void txttipodoc_Validated(object sender, EventArgs e)
        {
            //if (txttipodoc.Focus() == false)
            //{
            //    txttipodocdesc.ResetText();
            //}
        }

        private void txttipodocAsoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodocAsoc.Focus() == false)
            {
                txttipodocdescAsoc.ResetText();
            }
        }

        private void txttipodoc_TextChanged(object sender, EventArgs e)
        {
            if (txttipodoc.Focus() == false)
            {
                txttipodocdesc.ResetText();
            }
        }

        public void ListarModificar()
        {
            Entidad_Documentos_Punto_Venta Ent = new Entidad_Documentos_Punto_Venta();
            Logica_Documentos_Punto_Venta log = new Logica_Documentos_Punto_Venta();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Pdv_Codigo = Pdv_Codigo;
            Ent.Doc_Codigo = Doc_Codigo;

            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Documentos_Punto_Venta Enti = new Entidad_Documentos_Punto_Venta();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Pdv_Codigo = Enti.Pdv_Codigo;
                    Doc_Codigo = Enti.Doc_Codigo;

             
                    txtestcod.Text = Enti.Est_Codigo;
                    txtestnombre.Text = Enti.Est_Descripcion;

                    txtpdvcod.Text = Enti.Pdv_Codigo;
                    txtpdvdesc.Text = Enti.Pdv_Nombre;

                    txttipodoc.Text = Enti.Id_Sunat;
                    txttipodoc.Tag = Enti.Doc_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;

                    txttipodocAsoc.Text = Enti.Id_Sunat_Asoc;
                    txttipodocAsoc.Tag = Enti.Doc_Tipo_Doc_Asoc;
                    txttipodocdescAsoc.Text = Enti.Nombre_Comprobante_Asoc;

                    chkcreditofiscal.Checked = Enti.Doc_Credito_Fiscal;
                    rbdocdefecto.Checked = Enti.Doc_Por_Defecto;
                    rbfactura.Checked = Enti.Doc_Es_Factura;
                    rbboleta.Checked = Enti.Doc_Es_Boleta;
                    rbnota.Checked = Enti.Doc_Es_Nota_Salida;



                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

    }
}
