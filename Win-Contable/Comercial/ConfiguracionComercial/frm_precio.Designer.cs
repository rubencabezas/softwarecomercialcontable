﻿namespace Comercial
{
    partial class frm_precio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnnuevo = new DevExpress.XtraBars.BarButtonItem();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.Btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.btncancelar = new DevExpress.XtraBars.BarButtonItem();
            this.btneliminar = new DevExpress.XtraBars.BarButtonItem();
            this.btnexportar = new DevExpress.XtraBars.BarButtonItem();
            this.btnbusquedaavanzada = new DevExpress.XtraBars.BarButtonItem();
            this.btnexportardata = new DevExpress.XtraBars.BarButtonItem();
            this.btnimportardata = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnanular = new DevExpress.XtraBars.BarButtonItem();
            this.btnrevertiranulado = new DevExpress.XtraBars.BarButtonItem();
            this.dgvdatos = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.gbxcabecera = new System.Windows.Forms.GroupBox();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsadesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipobsacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtestnombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtestcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.gbxpresentacion = new System.Windows.Forms.GroupBox();
            this.chkRequiereCodigoAutoriza = new DevExpress.XtraEditors.CheckEdit();
            this.chkesvariable = new DevExpress.XtraEditors.CheckEdit();
            this.txtunmdescpres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtpreciomaximo = new DevExpress.XtraEditors.TextEdit();
            this.txtpreciominimo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtunmcodpres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtcodigoautoriza = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtfechaFin = new DevExpress.XtraEditors.TextEdit();
            this.txtfechaIni = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.btnnuevodet = new DevExpress.XtraEditors.SimpleButton();
            this.btneditardet = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnagregardet = new DevExpress.XtraEditors.SimpleButton();
            this.gbxbotones = new System.Windows.Forms.GroupBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.gbxcabecera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.gbxpresentacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequiereCodigoAutoriza.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkesvariable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescpres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpreciomaximo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpreciominimo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodpres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodigoautoriza.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaIni.Properties)).BeginInit();
            this.gbxbotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnnuevo,
            this.btnmodificar,
            this.btneliminar,
            this.btnanular,
            this.btnrevertiranulado,
            this.btnexportar,
            this.Btnguardar,
            this.btnbusquedaavanzada,
            this.btncancelar,
            this.btnexportardata,
            this.btnimportardata});
            this.barManager1.MaxItemId = 11;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnnuevo),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnmodificar),
            new DevExpress.XtraBars.LinkPersistInfo(this.Btnguardar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btncancelar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btneliminar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnexportar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnbusquedaavanzada),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnexportardata),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnimportardata)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnnuevo
            // 
            this.btnnuevo.Caption = "Nuevo [F1]";
            this.btnnuevo.Id = 0;
            this.btnnuevo.ImageOptions.Image = global::Contable.Properties.Resources.Add_File_24px;
            this.btnnuevo.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevo.ItemAppearance.Normal.Options.UseFont = true;
            this.btnnuevo.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnnuevo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnnuevo_ItemClick);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Modificar [F2]";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ImageOptions.Image = global::Contable.Properties.Resources.Edit_File_24px;
            this.btnmodificar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodificar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnmodificar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnmodificar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnmodificar_ItemClick);
            // 
            // Btnguardar
            // 
            this.Btnguardar.Caption = "Guardar";
            this.Btnguardar.Id = 6;
            this.Btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.Btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.Btnguardar.Name = "Btnguardar";
            this.Btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.Btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Btnguardar_ItemClick);
            // 
            // btncancelar
            // 
            this.btncancelar.Caption = "Cancelar";
            this.btncancelar.Id = 8;
            this.btncancelar.ImageOptions.Image = global::Contable.Properties.Resources.cancelar;
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btncancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btncancelar_ItemClick);
            // 
            // btneliminar
            // 
            this.btneliminar.Caption = "Eliminar [F3]";
            this.btneliminar.Id = 2;
            this.btneliminar.ImageOptions.Image = global::Contable.Properties.Resources.Delete_File_24px;
            this.btneliminar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneliminar.ItemAppearance.Normal.Options.UseFont = true;
            this.btneliminar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btneliminar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btneliminar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btneliminar_ItemClick);
            // 
            // btnexportar
            // 
            this.btnexportar.Caption = "Exportar";
            this.btnexportar.Id = 5;
            this.btnexportar.ImageOptions.Image = global::Contable.Properties.Resources.Microsoft_Excel_24px;
            this.btnexportar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexportar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnexportar.Name = "btnexportar";
            this.btnexportar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnexportar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnexportar_ItemClick);
            // 
            // btnbusquedaavanzada
            // 
            this.btnbusquedaavanzada.Caption = "Busqueda Avanzada";
            this.btnbusquedaavanzada.Id = 7;
            this.btnbusquedaavanzada.ImageOptions.Image = global::Contable.Properties.Resources.busquedaavanzada;
            this.btnbusquedaavanzada.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnbusquedaavanzada.ItemAppearance.Normal.Options.UseFont = true;
            this.btnbusquedaavanzada.Name = "btnbusquedaavanzada";
            this.btnbusquedaavanzada.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnbusquedaavanzada.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnbusquedaavanzada_ItemClick);
            // 
            // btnexportardata
            // 
            this.btnexportardata.Caption = "Exportar Data";
            this.btnexportardata.Id = 9;
            this.btnexportardata.ImageOptions.Image = global::Contable.Properties.Resources.exportdata;
            this.btnexportardata.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnexportardata.ItemAppearance.Normal.Options.UseFont = true;
            this.btnexportardata.Name = "btnexportardata";
            this.btnexportardata.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnexportardata.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnexportardata_ItemClick);
            // 
            // btnimportardata
            // 
            this.btnimportardata.Caption = "Importar Data";
            this.btnimportardata.Id = 10;
            this.btnimportardata.ImageOptions.Image = global::Contable.Properties.Resources.importdata;
            this.btnimportardata.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnimportardata.ItemAppearance.Normal.Options.UseFont = true;
            this.btnimportardata.Name = "btnimportardata";
            this.btnimportardata.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnimportardata.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnimportardata_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(990, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 508);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(990, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(990, 32);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // btnanular
            // 
            this.btnanular.Caption = "Anular [F7]";
            this.btnanular.Id = 3;
            this.btnanular.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F7);
            this.btnanular.Name = "btnanular";
            this.btnanular.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnrevertiranulado
            // 
            this.btnrevertiranulado.Caption = "Revertir Anulado [F8]";
            this.btnrevertiranulado.Id = 4;
            this.btnrevertiranulado.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F8);
            this.btnrevertiranulado.Name = "btnrevertiranulado";
            this.btnrevertiranulado.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // dgvdatos
            // 
            this.dgvdatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.RelationName = "Level1";
            this.dgvdatos.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.dgvdatos.Location = new System.Drawing.Point(0, 273);
            this.dgvdatos.MainView = this.gridView1;
            this.dgvdatos.MenuManager = this.barManager1;
            this.dgvdatos.Name = "dgvdatos";
            this.dgvdatos.Size = new System.Drawing.Size(990, 233);
            this.dgvdatos.TabIndex = 4;
            this.dgvdatos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.GridControl = this.dgvdatos;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Unidad de medida";
            this.gridColumn1.FieldName = "Pre_Unm_Desc";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 127;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tipo ";
            this.gridColumn2.FieldName = "Pre_Mod_Venta_Desc";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 118;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Moneda";
            this.gridColumn3.FieldName = "Pre_Moneda_Desc";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 107;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Fecha Inicial";
            this.gridColumn4.FieldName = "Pre_Fecha_Ini";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Fecha Final";
            this.gridColumn5.FieldName = "Pre_Fecha_Fin";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Es precio variable?";
            this.gridColumn6.FieldName = "Es_Precio_Variable";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 112;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Precio minimo";
            this.gridColumn7.FieldName = "Pre_Precio_Minimo";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 104;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Precio maximo";
            this.gridColumn8.FieldName = "Pre_Precio_Maximo";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            this.gridColumn8.Width = 104;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Tiene Codigo de Autorización?";
            this.gridColumn9.FieldName = "Pre_Requiere_Codigo_Autoriza";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 8;
            this.gridColumn9.Width = 184;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 32);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(990, 32);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl3.Location = new System.Drawing.Point(0, 508);
            this.barDockControl3.Manager = this.barManager1;
            this.barDockControl3.Size = new System.Drawing.Size(990, 0);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 32);
            this.barDockControl4.Manager = this.barManager1;
            this.barDockControl4.Size = new System.Drawing.Size(990, 0);
            // 
            // gbxcabecera
            // 
            this.gbxcabecera.Controls.Add(this.labelControl21);
            this.gbxcabecera.Controls.Add(this.txtproductodesc);
            this.gbxcabecera.Controls.Add(this.txtproductocod);
            this.gbxcabecera.Controls.Add(this.txttipobsadesc);
            this.gbxcabecera.Controls.Add(this.txttipobsacod);
            this.gbxcabecera.Controls.Add(this.labelControl15);
            this.gbxcabecera.Controls.Add(this.txtestnombre);
            this.gbxcabecera.Controls.Add(this.labelControl6);
            this.gbxcabecera.Controls.Add(this.txtestcod);
            this.gbxcabecera.Location = new System.Drawing.Point(2, 63);
            this.gbxcabecera.Name = "gbxcabecera";
            this.gbxcabecera.Size = new System.Drawing.Size(634, 61);
            this.gbxcabecera.TabIndex = 1;
            this.gbxcabecera.TabStop = false;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(44, 40);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(47, 13);
            this.labelControl21.TabIndex = 5;
            this.labelControl21.Text = "Producto:";
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(172, 38);
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Size = new System.Drawing.Size(449, 20);
            this.txtproductodesc.TabIndex = 7;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(94, 38);
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Size = new System.Drawing.Size(77, 20);
            this.txtproductocod.TabIndex = 6;
            this.txtproductocod.EditValueChanged += new System.EventHandler(this.txtproductocod_EditValueChanged);
            this.txtproductocod.TextChanged += new System.EventHandler(this.txtproductocod_TextChanged);
            this.txtproductocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproductocod_KeyDown);
            // 
            // txttipobsadesc
            // 
            this.txttipobsadesc.Enabled = false;
            this.txttipobsadesc.EnterMoveNextControl = true;
            this.txttipobsadesc.Location = new System.Drawing.Point(459, 15);
            this.txttipobsadesc.Name = "txttipobsadesc";
            this.txttipobsadesc.Size = new System.Drawing.Size(162, 20);
            this.txttipobsadesc.TabIndex = 4;
            // 
            // txttipobsacod
            // 
            this.txttipobsacod.EnterMoveNextControl = true;
            this.txttipobsacod.Location = new System.Drawing.Point(414, 15);
            this.txttipobsacod.Name = "txttipobsacod";
            this.txttipobsacod.Size = new System.Drawing.Size(44, 20);
            this.txttipobsacod.TabIndex = 3;
            this.txttipobsacod.TextChanged += new System.EventHandler(this.txttipobsacod_TextChanged);
            this.txttipobsacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipobsacod_KeyDown);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(356, 21);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(54, 13);
            this.labelControl15.TabIndex = 3;
            this.labelControl15.Text = "Tipo B/S/A:";
            // 
            // txtestnombre
            // 
            this.txtestnombre.Enabled = false;
            this.txtestnombre.EnterMoveNextControl = true;
            this.txtestnombre.Location = new System.Drawing.Point(131, 15);
            this.txtestnombre.Name = "txtestnombre";
            this.txtestnombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestnombre.Size = new System.Drawing.Size(213, 20);
            this.txtestnombre.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(14, 15);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Establecimiento";
            // 
            // txtestcod
            // 
            this.txtestcod.EnterMoveNextControl = true;
            this.txtestcod.Location = new System.Drawing.Point(94, 15);
            this.txtestcod.Name = "txtestcod";
            this.txtestcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestcod.Size = new System.Drawing.Size(37, 20);
            this.txtestcod.TabIndex = 1;
            this.txtestcod.TextChanged += new System.EventHandler(this.txtestcod_TextChanged);
            this.txtestcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtestcod_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(445, 74);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(87, 13);
            this.labelControl8.TabIndex = 25;
            this.labelControl8.Text = "Precio máximo:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(352, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(24, 13);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "Tipo:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(47, 47);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(42, 13);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Moneda:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(481, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(64, 13);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Fecha Hasta:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(270, 74);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(83, 13);
            this.labelControl5.TabIndex = 25;
            this.labelControl5.Text = "Precio minimo:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(285, 49);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 13);
            this.labelControl3.TabIndex = 21;
            this.labelControl3.Text = "Fecha Vigencia:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelControl14);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Controls.Add(this.labelControl8);
            this.groupBox2.Controls.Add(this.labelControl5);
            this.groupBox2.Controls.Add(this.textEdit1);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Location = new System.Drawing.Point(3, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(658, 98);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Presentacion";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(0, 25);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(89, 13);
            this.labelControl14.TabIndex = 12;
            this.labelControl14.Text = "Unidad de medida:";
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "0.000";
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(538, 69);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Mask.EditMask = "n3";
            this.textEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit1.Size = new System.Drawing.Size(83, 20);
            this.textEdit1.TabIndex = 26;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(6, 44);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(201, 14);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "Valor de venta - no incluye I.G.V.";
            // 
            // gbxpresentacion
            // 
            this.gbxpresentacion.Controls.Add(this.chkRequiereCodigoAutoriza);
            this.gbxpresentacion.Controls.Add(this.chkesvariable);
            this.gbxpresentacion.Controls.Add(this.txtunmdescpres);
            this.gbxpresentacion.Controls.Add(this.labelControl9);
            this.gbxpresentacion.Controls.Add(this.labelControl10);
            this.gbxpresentacion.Controls.Add(this.labelControl11);
            this.gbxpresentacion.Controls.Add(this.labelControl13);
            this.gbxpresentacion.Controls.Add(this.txtpreciomaximo);
            this.gbxpresentacion.Controls.Add(this.txtpreciominimo);
            this.gbxpresentacion.Controls.Add(this.labelControl16);
            this.gbxpresentacion.Controls.Add(this.labelControl17);
            this.gbxpresentacion.Controls.Add(this.txtcondicioncod);
            this.gbxpresentacion.Controls.Add(this.txtcondiciondesc);
            this.gbxpresentacion.Controls.Add(this.txtunmcodpres);
            this.gbxpresentacion.Controls.Add(this.labelControl18);
            this.gbxpresentacion.Controls.Add(this.txtcodigoautoriza);
            this.gbxpresentacion.Controls.Add(this.txtmonedacod);
            this.gbxpresentacion.Controls.Add(this.txtmonedadesc);
            this.gbxpresentacion.Controls.Add(this.txtfechaFin);
            this.gbxpresentacion.Controls.Add(this.txtfechaIni);
            this.gbxpresentacion.Location = new System.Drawing.Point(2, 127);
            this.gbxpresentacion.Name = "gbxpresentacion";
            this.gbxpresentacion.Size = new System.Drawing.Size(634, 126);
            this.gbxpresentacion.TabIndex = 9;
            this.gbxpresentacion.TabStop = false;
            this.gbxpresentacion.Text = "Presentacion";
            // 
            // chkRequiereCodigoAutoriza
            // 
            this.chkRequiereCodigoAutoriza.EnterMoveNextControl = true;
            this.chkRequiereCodigoAutoriza.Location = new System.Drawing.Point(95, 101);
            this.chkRequiereCodigoAutoriza.Name = "chkRequiereCodigoAutoriza";
            this.chkRequiereCodigoAutoriza.Properties.Caption = "Requiere Código de autorización?";
            this.chkRequiereCodigoAutoriza.Size = new System.Drawing.Size(176, 20);
            this.chkRequiereCodigoAutoriza.TabIndex = 22;
            this.chkRequiereCodigoAutoriza.CheckedChanged += new System.EventHandler(this.chkRequiereCodigoAutoriza_CheckedChanged);
            this.chkRequiereCodigoAutoriza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkRequiereCodigoAutoriza_KeyPress);
            // 
            // chkesvariable
            // 
            this.chkesvariable.EnterMoveNextControl = true;
            this.chkesvariable.Location = new System.Drawing.Point(95, 70);
            this.chkesvariable.Name = "chkesvariable";
            this.chkesvariable.Properties.Caption = "Es precio variable?";
            this.chkesvariable.Size = new System.Drawing.Size(122, 20);
            this.chkesvariable.TabIndex = 17;
            this.chkesvariable.CheckedChanged += new System.EventHandler(this.chkesvariable_CheckedChanged);
            // 
            // txtunmdescpres
            // 
            this.txtunmdescpres.Enabled = false;
            this.txtunmdescpres.EnterMoveNextControl = true;
            this.txtunmdescpres.Location = new System.Drawing.Point(129, 20);
            this.txtunmdescpres.Name = "txtunmdescpres";
            this.txtunmdescpres.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmdescpres.Properties.Appearance.Options.UseFont = true;
            this.txtunmdescpres.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmdescpres.Size = new System.Drawing.Size(217, 22);
            this.txtunmdescpres.TabIndex = 6;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(0, 25);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(89, 13);
            this.labelControl9.TabIndex = 4;
            this.labelControl9.Text = "Unidad de medida:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(352, 23);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(24, 13);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "Tipo:";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(445, 74);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(87, 13);
            this.labelControl11.TabIndex = 20;
            this.labelControl11.Text = "Precio máximo:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Location = new System.Drawing.Point(277, 74);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(83, 13);
            this.labelControl13.TabIndex = 18;
            this.labelControl13.Text = "Precio minimo:";
            // 
            // txtpreciomaximo
            // 
            this.txtpreciomaximo.EditValue = "0.0000";
            this.txtpreciomaximo.EnterMoveNextControl = true;
            this.txtpreciomaximo.Location = new System.Drawing.Point(544, 69);
            this.txtpreciomaximo.Name = "txtpreciomaximo";
            this.txtpreciomaximo.Properties.Mask.EditMask = "n4";
            this.txtpreciomaximo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtpreciomaximo.Size = new System.Drawing.Size(77, 20);
            this.txtpreciomaximo.TabIndex = 21;
            this.txtpreciomaximo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpreciomaximo_KeyDown);
            this.txtpreciomaximo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpreciomaximo_KeyPress);
            this.txtpreciomaximo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtpreciomaximo_KeyUp);
            // 
            // txtpreciominimo
            // 
            this.txtpreciominimo.EditValue = "0.0000";
            this.txtpreciominimo.EnterMoveNextControl = true;
            this.txtpreciominimo.Location = new System.Drawing.Point(362, 70);
            this.txtpreciominimo.Name = "txtpreciominimo";
            this.txtpreciominimo.Properties.Mask.EditMask = "n4";
            this.txtpreciominimo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtpreciominimo.Size = new System.Drawing.Size(77, 20);
            this.txtpreciominimo.TabIndex = 19;
            this.txtpreciominimo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpreciominimo_KeyDown);
            this.txtpreciominimo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpreciominimo_KeyPress);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(47, 47);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(42, 13);
            this.labelControl16.TabIndex = 10;
            this.labelControl16.Text = "Moneda:";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(468, 47);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(64, 13);
            this.labelControl17.TabIndex = 15;
            this.labelControl17.Text = "Fecha Hasta:";
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.Enabled = false;
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(378, 20);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Size = new System.Drawing.Size(58, 20);
            this.txtcondicioncod.TabIndex = 8;
            this.txtcondicioncod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcondicioncod_KeyDown);
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(438, 20);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Size = new System.Drawing.Size(183, 20);
            this.txtcondiciondesc.TabIndex = 9;
            // 
            // txtunmcodpres
            // 
            this.txtunmcodpres.EnterMoveNextControl = true;
            this.txtunmcodpres.Location = new System.Drawing.Point(94, 20);
            this.txtunmcodpres.Name = "txtunmcodpres";
            this.txtunmcodpres.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmcodpres.Properties.Appearance.Options.UseFont = true;
            this.txtunmcodpres.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmcodpres.Size = new System.Drawing.Size(34, 22);
            this.txtunmcodpres.TabIndex = 5;
            this.txtunmcodpres.TextChanged += new System.EventHandler(this.txtunmcodpres_TextChanged);
            this.txtunmcodpres.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtunmcodpres_KeyDown);
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(285, 49);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(75, 13);
            this.labelControl18.TabIndex = 13;
            this.labelControl18.Text = "Fecha Vigencia:";
            // 
            // txtcodigoautoriza
            // 
            this.txtcodigoautoriza.Enabled = false;
            this.txtcodigoautoriza.EnterMoveNextControl = true;
            this.txtcodigoautoriza.Location = new System.Drawing.Point(277, 101);
            this.txtcodigoautoriza.Name = "txtcodigoautoriza";
            this.txtcodigoautoriza.Properties.MaxLength = 10;
            this.txtcodigoautoriza.Properties.PasswordChar = '*';
            this.txtcodigoautoriza.Size = new System.Drawing.Size(162, 20);
            this.txtcodigoautoriza.TabIndex = 23;
            this.txtcodigoautoriza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcodigoautoriza_KeyPress);
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.Enabled = false;
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(95, 44);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Size = new System.Drawing.Size(34, 20);
            this.txtmonedacod.TabIndex = 11;
            this.txtmonedacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmonedacod_KeyDown);
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(130, 44);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(141, 20);
            this.txtmonedadesc.TabIndex = 12;
            // 
            // txtfechaFin
            // 
            this.txtfechaFin.Enabled = false;
            this.txtfechaFin.EnterMoveNextControl = true;
            this.txtfechaFin.Location = new System.Drawing.Point(544, 44);
            this.txtfechaFin.Name = "txtfechaFin";
            this.txtfechaFin.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechaFin.Properties.Mask.BeepOnError = true;
            this.txtfechaFin.Properties.Mask.EditMask = "d";
            this.txtfechaFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechaFin.Properties.Mask.SaveLiteral = false;
            this.txtfechaFin.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechaFin.Properties.MaxLength = 10;
            this.txtfechaFin.Size = new System.Drawing.Size(77, 20);
            this.txtfechaFin.TabIndex = 16;
            // 
            // txtfechaIni
            // 
            this.txtfechaIni.EnterMoveNextControl = true;
            this.txtfechaIni.Location = new System.Drawing.Point(362, 44);
            this.txtfechaIni.Name = "txtfechaIni";
            this.txtfechaIni.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechaIni.Properties.Mask.BeepOnError = true;
            this.txtfechaIni.Properties.Mask.EditMask = "d";
            this.txtfechaIni.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechaIni.Properties.Mask.SaveLiteral = false;
            this.txtfechaIni.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechaIni.Properties.MaxLength = 10;
            this.txtfechaIni.Size = new System.Drawing.Size(77, 20);
            this.txtfechaIni.TabIndex = 14;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(6, 44);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(201, 14);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "Valor de venta - no incluye I.G.V.";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Location = new System.Drawing.Point(4, 47);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(201, 14);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "Valor de venta - no incluye I.G.V.";
            // 
            // btnnuevodet
            // 
            this.btnnuevodet.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevodet.Appearance.Options.UseFont = true;
            this.btnnuevodet.Location = new System.Drawing.Point(6, 16);
            this.btnnuevodet.Name = "btnnuevodet";
            this.btnnuevodet.Size = new System.Drawing.Size(64, 28);
            this.btnnuevodet.TabIndex = 3;
            this.btnnuevodet.Text = "Nuevo";
            this.btnnuevodet.Click += new System.EventHandler(this.btnnuevodet_Click);
            // 
            // btneditardet
            // 
            this.btneditardet.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneditardet.Appearance.Options.UseFont = true;
            this.btneditardet.Location = new System.Drawing.Point(6, 53);
            this.btneditardet.Name = "btneditardet";
            this.btneditardet.Size = new System.Drawing.Size(64, 28);
            this.btneditardet.TabIndex = 56;
            this.btneditardet.Text = "Editar";
            this.btneditardet.Click += new System.EventHandler(this.btneditardet_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(76, 16);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(64, 28);
            this.simpleButton3.TabIndex = 60;
            this.simpleButton3.Text = "Quitar";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // btnagregardet
            // 
            this.btnagregardet.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnagregardet.Appearance.Options.UseFont = true;
            this.btnagregardet.Location = new System.Drawing.Point(76, 53);
            this.btnagregardet.Name = "btnagregardet";
            this.btnagregardet.Size = new System.Drawing.Size(64, 28);
            this.btnagregardet.TabIndex = 24;
            this.btnagregardet.Text = "Agregar";
            this.btnagregardet.Click += new System.EventHandler(this.btnagregardet_Click);
            // 
            // gbxbotones
            // 
            this.gbxbotones.Controls.Add(this.btnagregardet);
            this.gbxbotones.Controls.Add(this.btnnuevodet);
            this.gbxbotones.Controls.Add(this.simpleButton3);
            this.gbxbotones.Controls.Add(this.btneditardet);
            this.gbxbotones.Location = new System.Drawing.Point(642, 161);
            this.gbxbotones.Name = "gbxbotones";
            this.gbxbotones.Size = new System.Drawing.Size(148, 87);
            this.gbxbotones.TabIndex = 2;
            this.gbxbotones.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frm_precio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(990, 508);
            this.Controls.Add(this.gbxbotones);
            this.Controls.Add(this.gbxpresentacion);
            this.Controls.Add(this.labelControl20);
            this.Controls.Add(this.gbxcabecera);
            this.Controls.Add(this.dgvdatos);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frm_precio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Precio";
            this.Load += new System.EventHandler(this.frm_precio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.gbxcabecera.ResumeLayout(false);
            this.gbxcabecera.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipobsacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.gbxpresentacion.ResumeLayout(false);
            this.gbxpresentacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequiereCodigoAutoriza.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkesvariable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescpres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpreciomaximo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpreciominimo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodpres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodigoautoriza.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaIni.Properties)).EndInit();
            this.gbxbotones.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnnuevo;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem btneliminar;
        private DevExpress.XtraBars.BarButtonItem btnexportar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnanular;
        private DevExpress.XtraBars.BarButtonItem btnrevertiranulado;
        private DevExpress.XtraGrid.GridControl dgvdatos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private System.Windows.Forms.GroupBox gbxcabecera;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.TextEdit txttipobsadesc;
        private DevExpress.XtraEditors.TextEdit txttipobsacod;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtestnombre;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtestcod;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.GroupBox gbxpresentacion;
        private DevExpress.XtraEditors.CheckEdit chkesvariable;
        private DevExpress.XtraEditors.TextEdit txtunmdescpres;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtpreciomaximo;
        private DevExpress.XtraEditors.TextEdit txtpreciominimo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        private DevExpress.XtraEditors.TextEdit txtunmcodpres;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtfechaFin;
        private DevExpress.XtraEditors.TextEdit txtfechaIni;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.SimpleButton btnagregardet;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton btneditardet;
        private DevExpress.XtraEditors.SimpleButton btnnuevodet;
        private DevExpress.XtraBars.BarButtonItem Btnguardar;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraBars.BarButtonItem btnbusquedaavanzada;
        private DevExpress.XtraBars.BarButtonItem btncancelar;
        private System.Windows.Forms.GroupBox gbxbotones;
        private DevExpress.XtraEditors.CheckEdit chkRequiereCodigoAutoriza;
        private DevExpress.XtraEditors.TextEdit txtcodigoautoriza;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraBars.BarButtonItem btnexportardata;
        private DevExpress.XtraBars.BarButtonItem btnimportardata;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
