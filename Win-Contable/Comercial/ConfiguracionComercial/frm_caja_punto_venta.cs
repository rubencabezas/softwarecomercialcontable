﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_caja_punto_venta : frm_fuente
    {
        public frm_caja_punto_venta()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_caja_punto_venta_edicion f = new frm_caja_punto_venta_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        public List<Entidad_Caja> Lista = new List<Entidad_Caja>();
        public void Listar()
        {
            Entidad_Caja Ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Pdv_Codigo = null;
            Ent.Cja_Codigo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_caja_punto_venta_edicion f = new frm_caja_punto_venta_edicion())
            {

                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Pdv_Codigo = Entidad.Pdv_Codigo;
                f.Cja_Codigo = Entidad.Cja_Codigo;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Cja_Codigo + " " + Entidad.Cja_Descripcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Caja Ent = new Entidad_Caja()
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Pdv_Codigo = Entidad.Pdv_Codigo,
                        Cja_Codigo = Entidad.Cja_Codigo
                    };

                    Logica_Caja log = new Logica_Caja();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }


        Entidad_Caja Entidad = new Entidad_Caja();

        private void dgvdatos_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void btnExportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                //string path = "C:\\ROFELIO PAUCAR";
                string Ruta;
                Ruta = path + ("\\caja" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_caja_punto_venta_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

    }
}
