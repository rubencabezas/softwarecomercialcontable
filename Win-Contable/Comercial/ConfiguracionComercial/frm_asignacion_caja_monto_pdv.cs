﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_asignacion_caja_monto_pdv : frm_fuente
    {
        public frm_asignacion_caja_monto_pdv()
        {
            InitializeComponent();
        }

        private void frm_asignacion_caja_monto_pdv_Load(object sender, EventArgs e)
        {
            txtsaldoinicial.Text = "0.00";
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }
    }
}
