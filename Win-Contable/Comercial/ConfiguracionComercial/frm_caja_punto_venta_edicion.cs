﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_caja_punto_venta_edicion : frm_fuente
    {
        public frm_caja_punto_venta_edicion()
        {
            InitializeComponent();
        }


        public string Estado_Ven_Boton;

        public string Id_Empresa, Pdv_Codigo,Cja_Codigo;

        void Limpiar()
        {
            txtpdvcod.ResetText();
            txtpdvdesc.ResetText();
            txtdescripcion.ResetText();
        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Caja Ent = new Entidad_Caja();
                    Logica_Caja Log = new Logica_Caja();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Pdv_Codigo = txtpdvcod.Text;
                    Ent.Cja_Codigo = Cja_Codigo;
                    Ent.Cja_Descripcion = txtdescripcion.Text;
  



                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();


                            txtpdvcod.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtpdvdesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un punto de venta");
                txtpdvcod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtdescripcion.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una descripcion");
                txtdescripcion.Focus();
                return false;
            }

            return true;
        }

        private void frm_caja_punto_venta_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Limpiar();

                txtpdvcod.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();

            }
        }

        private void txtpdvcod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtpdvcod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtpdvcod.Text.Substring(txtpdvcod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_punto_venta_busqueda f = new frm_punto_venta_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Punto_Venta Entidad = new Entidad_Punto_Venta();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtpdvcod.Text = Entidad.Pdv_Codigo.Trim();
                                txtpdvdesc.Text = Entidad.Pdv_Nombre.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtpdvcod.Text) & string.IsNullOrEmpty(txtpdvdesc.Text))
                    {
                        BuscarPuntoVenta();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarPuntoVenta()
        {
            try
            {
                txtpdvcod.Text = Accion.Formato(txtpdvcod.Text, 2);

                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Generales = new List<Entidad_Punto_Venta>();

                Generales = log.Listar(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,

                    Pdv_Codigo = txtpdvcod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Punto_Venta T in Generales)
                    {
                        if ((T.Pdv_Codigo).ToString().Trim().ToUpper() == txtpdvcod.Text.Trim().ToUpper())
                        {
                            txtpdvcod.Text = (T.Pdv_Codigo).ToString().Trim();
                            txtpdvdesc.Text = T.Pdv_Nombre.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }


        public List<Entidad_Caja> Lista_Modificar = new List<Entidad_Caja>();

        private void txtpdvcod_TextChanged(object sender, EventArgs e)
        {
            if (txtpdvcod.Focus() == false)
            {
                txtpdvdesc.ResetText();
            }
        }

        public void ListarModificar()
        {
            Entidad_Caja Ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Pdv_Codigo = Pdv_Codigo;
            Ent.Cja_Codigo = Cja_Codigo;

            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Caja Enti = new Entidad_Caja();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Cja_Codigo = Enti.Cja_Codigo;

                    txtpdvcod.Text = Enti.Pdv_Codigo;
                    txtpdvdesc.Text = Enti.Pdv_Nombre;
                    txtdescripcion.Text = Enti.Cja_Descripcion;

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

    }
}
