﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_establecimiento_edicion : frm_fuente
    {
        public frm_establecimiento_edicion()
        {
            InitializeComponent();
        }

        public string Estado_Ven_Boton;

        public string Id_Empresa,Est_Codigo;

        void Limpiar()
        {
            txtnombre.ResetText();
            txtdireccion.ResetText();
            txttelefono.ResetText();
        }

        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtnombre.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un nombre");
                txtnombre.Focus();
                return false;
            }

            return true;
        }

        private void frm_establecimiento_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Limpiar();
 
                txtnombre.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                ListarModificar();
             
            }
        }


        public List<Entidad_Establecimiento> Lista_Modificar = new List<Entidad_Establecimiento>();
        public void ListarModificar()
        {
            Entidad_Establecimiento Ent = new Entidad_Establecimiento();
            Logica_Establecimiento log = new Logica_Establecimiento();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Est_Codigo = Est_Codigo;
   
            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Establecimiento Enti = new Entidad_Establecimiento();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Est_Codigo = Enti.Est_Codigo;

                    txtnombre.Text = Enti.Est_Descripcion;
                    txtdireccion.Text = Enti.Est_Direccion;
                    txttelefono.Text = Enti.Est_Telefono;

                                    }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Establecimiento Ent = new Entidad_Establecimiento();
                    Logica_Establecimiento Log = new Logica_Establecimiento();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                   
                    Ent.Est_Codigo = Est_Codigo;
          
                    Ent.Est_Descripcion = txtnombre.Text;
                    Ent.Est_Direccion = txtdireccion.Text;
                    Ent.Est_Telefono = txttelefono.Text;
           

                 

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();
                          
                                   
                            txtnombre.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

    }
}
