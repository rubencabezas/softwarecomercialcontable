﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_almacen : frm_fuente
    {
        public frm_almacen()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_almacen_edicion f = new frm_almacen_edicion())
            {


                Estado = Estados.Nuevo;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Almacen Ent = new Entidad_Almacen();
                    Logica_Almacen Log = new Logica_Almacen();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Alm_Descrcipcion = f.txtdescripcion.Text.Trim();
                    Ent.Alm_TipoBSA = f.txttipobsacod.Tag.ToString();
             
                    try
                    {
                        if (Estado == Estados.Nuevo)
                        {

                            if (Log.Insertar(Ent))
                            {
                                Listar();

                            }
                            else
                            {
                                Accion.Advertencia("");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }



        public List<Entidad_Almacen> Lista = new List<Entidad_Almacen>();
        public void Listar()
        {
            Entidad_Almacen Ent = new Entidad_Almacen();
            Logica_Almacen log = new Logica_Almacen();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


        Entidad_Almacen Entidad = new Entidad_Almacen();
     
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Estado = Estados.Modificar;

            using (frm_almacen_edicion f = new frm_almacen_edicion())
            {

              
                f.txtdescripcion.Tag = Entidad.Id_Almacen;
                f.txtdescripcion.Text = Entidad.Alm_Descrcipcion.Trim();
                f.txttipobsacod.Tag = Entidad.Alm_TipoBSA;
                f.txttipobsacod.Text = Entidad.Alm_TipoBSA_cod_interno;
                f.txttipobsadesc.Text = Entidad.Alm_TipoBSA_Descripcion_Det;

                if (f.ShowDialog() == DialogResult.OK)
                {

                    Entidad_Almacen Ent = new Entidad_Almacen();
                    Logica_Almacen Log = new Logica_Almacen();


                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    Ent.Id_Almacen = f.txtdescripcion.Tag.ToString().Trim();
                    Ent.Alm_Descrcipcion = f.txtdescripcion.Text.ToString().Trim();
                    Ent.Alm_TipoBSA = f.txttipobsacod.Tag.ToString();
                    
                    try
                    {
                        if (Estado == Estados.Modificar)
                        {

                            if (Log.Modificar(Ent))
                            {
                                Listar();
                            }
                            else
                            {
                                Accion.Advertencia("");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
            }
        }

        private void frm_almacen_Load(object sender, EventArgs e)
        {
            Listar();
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Almacen + " " + Entidad.Alm_Descrcipcion;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Almacen Ent = new Entidad_Almacen
                    {
                        Id_Empresa = Entidad.Id_Empresa,
                        Id_Almacen = Entidad.Id_Almacen
                    };

                    Logica_Almacen log = new Logica_Almacen();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
        private void btnExportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                //string path = "C:\\ROFELIO PAUCAR";
                string Ruta;
                Ruta = path + ("\\almacen" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
