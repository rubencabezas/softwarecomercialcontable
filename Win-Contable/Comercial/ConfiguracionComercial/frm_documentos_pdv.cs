﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_documentos_pdv : frm_fuente
    {
        public frm_documentos_pdv()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_documentos_pdv_edicion f = new frm_documentos_pdv_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        public List<Entidad_Documentos_Punto_Venta> Lista = new List<Entidad_Documentos_Punto_Venta>();
        public void Listar()
        {
            Entidad_Documentos_Punto_Venta Ent = new Entidad_Documentos_Punto_Venta();
            Logica_Documentos_Punto_Venta log = new Logica_Documentos_Punto_Venta();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Pdv_Codigo = null;
            Ent.Doc_Codigo = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        Entidad_Documentos_Punto_Venta Entidad = new Entidad_Documentos_Punto_Venta();
        private void dgvdatos_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_documentos_pdv_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_documentos_pdv_edicion f = new frm_documentos_pdv_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Pdv_Codigo = Entidad.Pdv_Codigo;
                f.Doc_Codigo = Entidad.Doc_Codigo;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

    }
}
