﻿namespace Contable.Comercial.ConfiguracionComercial
{
    partial class frm_establecimiento_parametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtestnombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtestcod = new DevExpress.XtraEditors.TextEdit();
            this.btngenerar = new DevExpress.XtraEditors.SimpleButton();
            this.btncancelar = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtestnombre);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.txtestcod);
            this.groupBox1.Location = new System.Drawing.Point(1, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(469, 62);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtestnombre
            // 
            this.txtestnombre.Enabled = false;
            this.txtestnombre.EnterMoveNextControl = true;
            this.txtestnombre.Location = new System.Drawing.Point(128, 20);
            this.txtestnombre.Name = "txtestnombre";
            this.txtestnombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestnombre.Size = new System.Drawing.Size(332, 20);
            this.txtestnombre.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(8, 20);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "Establecimiento";
            // 
            // txtestcod
            // 
            this.txtestcod.EnterMoveNextControl = true;
            this.txtestcod.Location = new System.Drawing.Point(88, 20);
            this.txtestcod.Name = "txtestcod";
            this.txtestcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestcod.Size = new System.Drawing.Size(37, 20);
            this.txtestcod.TabIndex = 4;
            this.txtestcod.TextChanged += new System.EventHandler(this.txtestcod_TextChanged);
            this.txtestcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtestcod_KeyDown);
            // 
            // btngenerar
            // 
            this.btngenerar.Location = new System.Drawing.Point(244, 78);
            this.btngenerar.Name = "btngenerar";
            this.btngenerar.Size = new System.Drawing.Size(89, 28);
            this.btngenerar.TabIndex = 4;
            this.btngenerar.Text = "Generar";
            this.btngenerar.Click += new System.EventHandler(this.btngenerar_Click);
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(126, 78);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(89, 28);
            this.btncancelar.TabIndex = 5;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // frm_establecimiento_parametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 123);
            this.Controls.Add(this.btngenerar);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_establecimiento_parametros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Generar Data";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton btngenerar;
        private DevExpress.XtraEditors.SimpleButton btncancelar;
        public DevExpress.XtraEditors.TextEdit txtestnombre;
        public DevExpress.XtraEditors.TextEdit txtestcod;
    }
}