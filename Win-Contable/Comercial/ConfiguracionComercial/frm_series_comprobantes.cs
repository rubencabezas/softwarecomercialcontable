﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Contable;
using Microsoft.VisualBasic;

namespace Comercial
{
    public partial class frm_series_comprobantes : frm_fuente
    {
        public frm_series_comprobantes()
        {
            InitializeComponent();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_series_comprobantes_edicion f = new frm_series_comprobantes_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }
        }

        public List<Entidad_Serie_Comprobante> Lista = new List<Entidad_Serie_Comprobante>();
        public void Listar()
        {
            Entidad_Serie_Comprobante Ent = new Entidad_Serie_Comprobante();
            Logica_Serie_Comprobante log = new Logica_Serie_Comprobante();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Ser_Tipo_Doc = null;
            Ent.Ser_Serie = null;

            try
            {
                Lista = log.Listar_principal(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_series_comprobantes_edicion f = new frm_series_comprobantes_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Ser_Tipo_Doc = Entidad.Ser_Tipo_Doc;
                f.Ser_Serie = Entidad.Ser_Serie;
                f.Ser_Numero_Ini = Entidad.Ser_Numero_Ini;
                f.Ser_Numero_Fin = Entidad.Ser_Numero_Fin;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }
        }
        Entidad_Serie_Comprobante Entidad = new Entidad_Serie_Comprobante();

        private void frm_series_comprobantes_Load(object sender, EventArgs e)
        {
            Listar();

        }

        private void dgvdatos_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btndarbaja_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a Dar de Baja tiene la caraterisctica:" + Entidad.Nombre_Comprobante + " " + Entidad.Ser_Serie;
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    string fecha_Baja="";
                

                    fecha_Baja = Microsoft.VisualBasic.Interaction.InputBox("Ingrese una fecha ", "Fecha", "",500,300);

                    if (IsDate(fecha_Baja))
                    {
                        Entidad_Serie_Comprobante Ent = new Entidad_Serie_Comprobante
                        {
                            Id_Empresa = Entidad.Id_Empresa,
                            Ser_Tipo_Doc = Entidad.Ser_Tipo_Doc,
                            Ser_Serie = Entidad.Ser_Serie,
                            Ser_Numero_Ini = Entidad.Ser_Numero_Ini,
                            Ser_Numero_Fin = Entidad.Ser_Numero_Fin,
                            Ser_Fecha_Baja = Convert.ToDateTime(fecha_Baja)
                        };

                        Logica_Serie_Comprobante log = new Logica_Serie_Comprobante();

                        log.Dar_Baja(Ent);
                        Accion.ExitoGuardar();
                        Listar();
                    }


                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public static DialogResult InputBox(string title, string promptText,  string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }


    }
}
