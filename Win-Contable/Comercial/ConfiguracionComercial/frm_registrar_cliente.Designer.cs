﻿namespace Comercial
{
    partial class frm_registrar_cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.TxtTipPerCodigo = new DevExpress.XtraEditors.TextEdit();
            this.TxtTipPerDescrip = new DevExpress.XtraEditors.TextEdit();
            this.TxtTipDocCodigo = new DevExpress.XtraEditors.TextEdit();
            this.TxtTipDocDescrip = new DevExpress.XtraEditors.TextEdit();
            this.TxtTipDocNumer = new DevExpress.XtraEditors.TextEdit();
            this.TxtEntNombre = new DevExpress.XtraEditors.TextEdit();
            this.TxtEnt1Ape = new DevExpress.XtraEditors.TextEdit();
            this.TxtEnt2Ape = new DevExpress.XtraEditors.TextEdit();
            this.TxtDirecionCompleta = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnaceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btncancelar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipPerCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipPerDescrip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocDescrip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocNumer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt1Ape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt2Ape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirecionCompleta.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(72, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo persona";
            this.labelControl1.Visible = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(303, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Tipo doc.";
            this.labelControl2.Visible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(4, 93);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 19);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Apellido Pat.";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(37, 52);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(57, 19);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Numero";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(352, 51);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(92, 19);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "RS / Nombre";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(351, 93);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(93, 19);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Apellido Mat.";
            // 
            // TxtTipPerCodigo
            // 
            this.TxtTipPerCodigo.EnterMoveNextControl = true;
            this.TxtTipPerCodigo.Location = new System.Drawing.Point(141, 3);
            this.TxtTipPerCodigo.Name = "TxtTipPerCodigo";
            this.TxtTipPerCodigo.Size = new System.Drawing.Size(34, 20);
            this.TxtTipPerCodigo.TabIndex = 1;
            this.TxtTipPerCodigo.Visible = false;
            // 
            // TxtTipPerDescrip
            // 
            this.TxtTipPerDescrip.EnterMoveNextControl = true;
            this.TxtTipPerDescrip.Location = new System.Drawing.Point(176, 3);
            this.TxtTipPerDescrip.Name = "TxtTipPerDescrip";
            this.TxtTipPerDescrip.Size = new System.Drawing.Size(100, 20);
            this.TxtTipPerDescrip.TabIndex = 2;
            this.TxtTipPerDescrip.Visible = false;
            // 
            // TxtTipDocCodigo
            // 
            this.TxtTipDocCodigo.EnterMoveNextControl = true;
            this.TxtTipDocCodigo.Location = new System.Drawing.Point(353, 3);
            this.TxtTipDocCodigo.Name = "TxtTipDocCodigo";
            this.TxtTipDocCodigo.Size = new System.Drawing.Size(33, 20);
            this.TxtTipDocCodigo.TabIndex = 4;
            this.TxtTipDocCodigo.Visible = false;
            // 
            // TxtTipDocDescrip
            // 
            this.TxtTipDocDescrip.EnterMoveNextControl = true;
            this.TxtTipDocDescrip.Location = new System.Drawing.Point(388, 3);
            this.TxtTipDocDescrip.Name = "TxtTipDocDescrip";
            this.TxtTipDocDescrip.Size = new System.Drawing.Size(100, 20);
            this.TxtTipDocDescrip.TabIndex = 5;
            this.TxtTipDocDescrip.Visible = false;
            // 
            // TxtTipDocNumer
            // 
            this.TxtTipDocNumer.Enabled = false;
            this.TxtTipDocNumer.EnterMoveNextControl = true;
            this.TxtTipDocNumer.Location = new System.Drawing.Point(100, 41);
            this.TxtTipDocNumer.Name = "TxtTipDocNumer";
            this.TxtTipDocNumer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtTipDocNumer.Properties.Appearance.Options.UseFont = true;
            this.TxtTipDocNumer.Size = new System.Drawing.Size(245, 36);
            this.TxtTipDocNumer.TabIndex = 7;
            // 
            // TxtEntNombre
            // 
            this.TxtEntNombre.EnterMoveNextControl = true;
            this.TxtEntNombre.Location = new System.Drawing.Point(447, 40);
            this.TxtEntNombre.Name = "TxtEntNombre";
            this.TxtEntNombre.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtEntNombre.Properties.Appearance.Options.UseFont = true;
            this.TxtEntNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEntNombre.Size = new System.Drawing.Size(435, 36);
            this.TxtEntNombre.TabIndex = 9;
            // 
            // TxtEnt1Ape
            // 
            this.TxtEnt1Ape.EnterMoveNextControl = true;
            this.TxtEnt1Ape.Location = new System.Drawing.Point(100, 82);
            this.TxtEnt1Ape.Name = "TxtEnt1Ape";
            this.TxtEnt1Ape.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtEnt1Ape.Properties.Appearance.Options.UseFont = true;
            this.TxtEnt1Ape.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEnt1Ape.Size = new System.Drawing.Size(245, 36);
            this.TxtEnt1Ape.TabIndex = 11;
            // 
            // TxtEnt2Ape
            // 
            this.TxtEnt2Ape.EnterMoveNextControl = true;
            this.TxtEnt2Ape.Location = new System.Drawing.Point(450, 82);
            this.TxtEnt2Ape.Name = "TxtEnt2Ape";
            this.TxtEnt2Ape.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtEnt2Ape.Properties.Appearance.Options.UseFont = true;
            this.TxtEnt2Ape.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEnt2Ape.Size = new System.Drawing.Size(432, 36);
            this.TxtEnt2Ape.TabIndex = 13;
            // 
            // TxtDirecionCompleta
            // 
            this.TxtDirecionCompleta.EnterMoveNextControl = true;
            this.TxtDirecionCompleta.Location = new System.Drawing.Point(100, 123);
            this.TxtDirecionCompleta.Name = "TxtDirecionCompleta";
            this.TxtDirecionCompleta.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.TxtDirecionCompleta.Properties.Appearance.Options.UseFont = true;
            this.TxtDirecionCompleta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtDirecionCompleta.Size = new System.Drawing.Size(782, 36);
            this.TxtDirecionCompleta.TabIndex = 15;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(29, 134);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(65, 19);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Direccion";
            // 
            // btnaceptar
            // 
            this.btnaceptar.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaceptar.Appearance.Options.UseFont = true;
            this.btnaceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnaceptar.Location = new System.Drawing.Point(235, 165);
            this.btnaceptar.Name = "btnaceptar";
            this.btnaceptar.Size = new System.Drawing.Size(127, 51);
            this.btnaceptar.TabIndex = 16;
            this.btnaceptar.Text = "Aceptar";
            this.btnaceptar.Click += new System.EventHandler(this.btnaceptar_Click);
            // 
            // btncancelar
            // 
            this.btncancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.btncancelar.Appearance.Options.UseFont = true;
            this.btncancelar.Location = new System.Drawing.Point(369, 165);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(127, 51);
            this.btncancelar.TabIndex = 17;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // frm_registrar_cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(894, 218);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.btnaceptar);
            this.Controls.Add(this.TxtDirecionCompleta);
            this.Controls.Add(this.TxtEnt2Ape);
            this.Controls.Add(this.TxtEnt1Ape);
            this.Controls.Add(this.TxtEntNombre);
            this.Controls.Add(this.TxtTipDocNumer);
            this.Controls.Add(this.TxtTipDocDescrip);
            this.Controls.Add(this.TxtTipDocCodigo);
            this.Controls.Add(this.TxtTipPerDescrip);
            this.Controls.Add(this.TxtTipPerCodigo);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_registrar_cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cliente";
            this.Load += new System.EventHandler(this.frm_registrar_cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipPerCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipPerDescrip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocDescrip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTipDocNumer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt1Ape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnt2Ape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirecionCompleta.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit TxtTipPerCodigo;
        private DevExpress.XtraEditors.TextEdit TxtTipPerDescrip;
        private DevExpress.XtraEditors.TextEdit TxtTipDocCodigo;
        private DevExpress.XtraEditors.TextEdit TxtTipDocDescrip;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        public DevExpress.XtraEditors.TextEdit TxtTipDocNumer;
        public DevExpress.XtraEditors.TextEdit TxtEntNombre;
        public DevExpress.XtraEditors.TextEdit TxtEnt1Ape;
        public DevExpress.XtraEditors.TextEdit TxtEnt2Ape;
        public DevExpress.XtraEditors.TextEdit TxtDirecionCompleta;
        private DevExpress.XtraEditors.SimpleButton btnaceptar;
        private DevExpress.XtraEditors.SimpleButton btncancelar;
    }
}
