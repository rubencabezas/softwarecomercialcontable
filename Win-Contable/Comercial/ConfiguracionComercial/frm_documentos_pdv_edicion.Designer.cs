﻿namespace Comercial
{
    partial class frm_documentos_pdv_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbdocdefecto = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbnota = new System.Windows.Forms.RadioButton();
            this.rbboleta = new System.Windows.Forms.RadioButton();
            this.rbfactura = new System.Windows.Forms.RadioButton();
            this.chkcreditofiscal = new System.Windows.Forms.CheckBox();
            this.txttipodocdescAsoc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocAsoc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.txtpdvdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpdvcod = new DevExpress.XtraEditors.TextEdit();
            this.txtestnombre = new DevExpress.XtraEditors.TextEdit();
            this.txtestcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdescAsoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocAsoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(652, 36);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 144);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(652, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 36);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 108);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(652, 36);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 108);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbdocdefecto);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.chkcreditofiscal);
            this.groupBox1.Controls.Add(this.txttipodocdescAsoc);
            this.groupBox1.Controls.Add(this.txttipodocAsoc);
            this.groupBox1.Controls.Add(this.txttipodocdesc);
            this.groupBox1.Controls.Add(this.txttipodoc);
            this.groupBox1.Controls.Add(this.txtpdvdesc);
            this.groupBox1.Controls.Add(this.txtpdvcod);
            this.groupBox1.Controls.Add(this.txtestnombre);
            this.groupBox1.Controls.Add(this.txtestcod);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(652, 108);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // rbdocdefecto
            // 
            this.rbdocdefecto.AutoSize = true;
            this.rbdocdefecto.Location = new System.Drawing.Point(197, 76);
            this.rbdocdefecto.Name = "rbdocdefecto";
            this.rbdocdefecto.Size = new System.Drawing.Size(107, 17);
            this.rbdocdefecto.TabIndex = 15;
            this.rbdocdefecto.Text = "Doc. por defecto";
            this.rbdocdefecto.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbnota);
            this.groupBox2.Controls.Add(this.rbboleta);
            this.groupBox2.Controls.Add(this.rbfactura);
            this.groupBox2.Location = new System.Drawing.Point(310, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(336, 38);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Operaciones";
            // 
            // rbnota
            // 
            this.rbnota.AutoSize = true;
            this.rbnota.Location = new System.Drawing.Point(139, 15);
            this.rbnota.Name = "rbnota";
            this.rbnota.Size = new System.Drawing.Size(93, 17);
            this.rbnota.TabIndex = 17;
            this.rbnota.TabStop = true;
            this.rbnota.Text = "Nota de salida";
            this.rbnota.UseVisualStyleBackColor = true;
            // 
            // rbboleta
            // 
            this.rbboleta.AutoSize = true;
            this.rbboleta.Location = new System.Drawing.Point(78, 15);
            this.rbboleta.Name = "rbboleta";
            this.rbboleta.Size = new System.Drawing.Size(55, 17);
            this.rbboleta.TabIndex = 16;
            this.rbboleta.TabStop = true;
            this.rbboleta.Text = "Boleta";
            this.rbboleta.UseVisualStyleBackColor = true;
            // 
            // rbfactura
            // 
            this.rbfactura.AutoSize = true;
            this.rbfactura.Location = new System.Drawing.Point(10, 15);
            this.rbfactura.Name = "rbfactura";
            this.rbfactura.Size = new System.Drawing.Size(62, 17);
            this.rbfactura.TabIndex = 15;
            this.rbfactura.TabStop = true;
            this.rbfactura.Text = "Factura";
            this.rbfactura.UseVisualStyleBackColor = true;
            // 
            // chkcreditofiscal
            // 
            this.chkcreditofiscal.AutoSize = true;
            this.chkcreditofiscal.Location = new System.Drawing.Point(95, 76);
            this.chkcreditofiscal.Name = "chkcreditofiscal";
            this.chkcreditofiscal.Size = new System.Drawing.Size(88, 17);
            this.chkcreditofiscal.TabIndex = 12;
            this.chkcreditofiscal.Text = "Credito fiscal";
            this.chkcreditofiscal.UseVisualStyleBackColor = true;
            // 
            // txttipodocdescAsoc
            // 
            this.txttipodocdescAsoc.Enabled = false;
            this.txttipodocdescAsoc.EnterMoveNextControl = true;
            this.txttipodocdescAsoc.Location = new System.Drawing.Point(436, 43);
            this.txttipodocdescAsoc.MenuManager = this.barManager1;
            this.txttipodocdescAsoc.Name = "txttipodocdescAsoc";
            this.txttipodocdescAsoc.Size = new System.Drawing.Size(210, 20);
            this.txttipodocdescAsoc.TabIndex = 11;
            // 
            // txttipodocAsoc
            // 
            this.txttipodocAsoc.EnterMoveNextControl = true;
            this.txttipodocAsoc.Location = new System.Drawing.Point(400, 43);
            this.txttipodocAsoc.MenuManager = this.barManager1;
            this.txttipodocAsoc.Name = "txttipodocAsoc";
            this.txttipodocAsoc.Size = new System.Drawing.Size(34, 20);
            this.txttipodocAsoc.TabIndex = 10;
            this.txttipodocAsoc.TextChanged += new System.EventHandler(this.txttipodocAsoc_TextChanged);
            this.txttipodocAsoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodocAsoc_KeyDown);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(126, 43);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(188, 20);
            this.txttipodocdesc.TabIndex = 8;
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(94, 43);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Size = new System.Drawing.Size(31, 20);
            this.txttipodoc.TabIndex = 7;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            this.txttipodoc.Validated += new System.EventHandler(this.txttipodoc_Validated);
            // 
            // txtpdvdesc
            // 
            this.txtpdvdesc.Enabled = false;
            this.txtpdvdesc.EnterMoveNextControl = true;
            this.txtpdvdesc.Location = new System.Drawing.Point(436, 19);
            this.txtpdvdesc.MenuManager = this.barManager1;
            this.txtpdvdesc.Name = "txtpdvdesc";
            this.txtpdvdesc.Size = new System.Drawing.Size(210, 20);
            this.txtpdvdesc.TabIndex = 5;
            // 
            // txtpdvcod
            // 
            this.txtpdvcod.EnterMoveNextControl = true;
            this.txtpdvcod.Location = new System.Drawing.Point(400, 19);
            this.txtpdvcod.MenuManager = this.barManager1;
            this.txtpdvcod.Name = "txtpdvcod";
            this.txtpdvcod.Size = new System.Drawing.Size(34, 20);
            this.txtpdvcod.TabIndex = 4;
            this.txtpdvcod.TextChanged += new System.EventHandler(this.txtpdvcod_TextChanged);
            this.txtpdvcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpdvcod_KeyDown);
            // 
            // txtestnombre
            // 
            this.txtestnombre.Enabled = false;
            this.txtestnombre.EnterMoveNextControl = true;
            this.txtestnombre.Location = new System.Drawing.Point(126, 20);
            this.txtestnombre.MenuManager = this.barManager1;
            this.txtestnombre.Name = "txtestnombre";
            this.txtestnombre.Size = new System.Drawing.Size(188, 20);
            this.txtestnombre.TabIndex = 2;
            // 
            // txtestcod
            // 
            this.txtestcod.EnterMoveNextControl = true;
            this.txtestcod.Location = new System.Drawing.Point(95, 20);
            this.txtestcod.MenuManager = this.barManager1;
            this.txtestcod.Name = "txtestcod";
            this.txtestcod.Size = new System.Drawing.Size(30, 20);
            this.txtestcod.TabIndex = 1;
            this.txtestcod.TextChanged += new System.EventHandler(this.txtestcod_TextChanged);
            this.txtestcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtestcod_KeyDown);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(355, 46);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Asociar:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(41, 50);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(49, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Tipo Doc.:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(320, 22);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(74, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Punto de Venta";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 24);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(78, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Establecimiento:";
            // 
            // frm_documentos_pdv_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(652, 144);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_documentos_pdv_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Documentos por punto de venta";
            this.Load += new System.EventHandler(this.frm_documentos_pdv_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdescAsoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocAsoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkcreditofiscal;
        private DevExpress.XtraEditors.TextEdit txttipodocdescAsoc;
        private DevExpress.XtraEditors.TextEdit txttipodocAsoc;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.TextEdit txtpdvdesc;
        private DevExpress.XtraEditors.TextEdit txtpdvcod;
        private DevExpress.XtraEditors.TextEdit txtestnombre;
        private DevExpress.XtraEditors.TextEdit txtestcod;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbnota;
        private System.Windows.Forms.RadioButton rbboleta;
        private System.Windows.Forms.RadioButton rbfactura;
        private System.Windows.Forms.CheckBox rbdocdefecto;
    }
}
