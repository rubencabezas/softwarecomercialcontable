﻿namespace Comercial
{
    partial class frm_series_comprobantes_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkelectronico = new DevExpress.XtraEditors.CheckEdit();
            this.txtmaxlineas = new DevExpress.XtraEditors.TextEdit();
            this.txtcodsunat = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechabaja = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechaimpr = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtpdvdesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpdvcod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtnumactual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtnumfin = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtnuminicio = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtserie = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkelectronico.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaxlineas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodsunat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechabaja.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaimpr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumactual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumfin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnuminicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(672, 36);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 153);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(672, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 36);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 117);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(672, 36);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 117);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkelectronico);
            this.groupBox1.Controls.Add(this.txtmaxlineas);
            this.groupBox1.Controls.Add(this.txtcodsunat);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Controls.Add(this.labelControl9);
            this.groupBox1.Controls.Add(this.txtfechabaja);
            this.groupBox1.Controls.Add(this.labelControl8);
            this.groupBox1.Controls.Add(this.txtfechaimpr);
            this.groupBox1.Controls.Add(this.labelControl7);
            this.groupBox1.Controls.Add(this.txtpdvdesc);
            this.groupBox1.Controls.Add(this.txtpdvcod);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.txtnumactual);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.txtnumfin);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.txtnuminicio);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.txtserie);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.txttipodocdesc);
            this.groupBox1.Controls.Add(this.txttipodoc);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(672, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // chkelectronico
            // 
            this.chkelectronico.Location = new System.Drawing.Point(563, 92);
            this.chkelectronico.MenuManager = this.barManager1;
            this.chkelectronico.Name = "chkelectronico";
            this.chkelectronico.Properties.Caption = "Es electronico?";
            this.chkelectronico.Size = new System.Drawing.Size(103, 19);
            this.chkelectronico.TabIndex = 22;
            // 
            // txtmaxlineas
            // 
            this.txtmaxlineas.Location = new System.Drawing.Point(424, 88);
            this.txtmaxlineas.Name = "txtmaxlineas";
            this.txtmaxlineas.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmaxlineas.Size = new System.Drawing.Size(73, 20);
            this.txtmaxlineas.TabIndex = 21;
            // 
            // txtcodsunat
            // 
            this.txtcodsunat.Location = new System.Drawing.Point(158, 88);
            this.txtcodsunat.MenuManager = this.barManager1;
            this.txtcodsunat.Name = "txtcodsunat";
            this.txtcodsunat.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcodsunat.Size = new System.Drawing.Size(185, 20);
            this.txtcodsunat.TabIndex = 19;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(349, 91);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(69, 13);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "Max. de lineas";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(21, 91);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(131, 13);
            this.labelControl9.TabIndex = 18;
            this.labelControl9.Text = "Codigo Autorizacion SUNAT";
            // 
            // txtfechabaja
            // 
            this.txtfechabaja.Enabled = false;
            this.txtfechabaja.EnterMoveNextControl = true;
            this.txtfechabaja.Location = new System.Drawing.Point(567, 66);
            this.txtfechabaja.MenuManager = this.barManager1;
            this.txtfechabaja.Name = "txtfechabaja";
            this.txtfechabaja.Properties.Mask.EditMask = "t";
            this.txtfechabaja.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechabaja.Properties.MaxLength = 10;
            this.txtfechabaja.Size = new System.Drawing.Size(100, 20);
            this.txtfechabaja.TabIndex = 17;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(527, 69);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(34, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "F. Baja";
            // 
            // txtfechaimpr
            // 
            this.txtfechaimpr.EnterMoveNextControl = true;
            this.txtfechaimpr.Location = new System.Drawing.Point(415, 66);
            this.txtfechaimpr.MenuManager = this.barManager1;
            this.txtfechaimpr.Name = "txtfechaimpr";
            this.txtfechaimpr.Properties.Mask.EditMask = "d";
            this.txtfechaimpr.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechaimpr.Properties.MaxLength = 10;
            this.txtfechaimpr.Size = new System.Drawing.Size(82, 20);
            this.txtfechaimpr.TabIndex = 15;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(349, 69);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 13);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "F. Impresion";
            // 
            // txtpdvdesc
            // 
            this.txtpdvdesc.Enabled = false;
            this.txtpdvdesc.EnterMoveNextControl = true;
            this.txtpdvdesc.Location = new System.Drawing.Point(130, 65);
            this.txtpdvdesc.MenuManager = this.barManager1;
            this.txtpdvdesc.Name = "txtpdvdesc";
            this.txtpdvdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpdvdesc.Size = new System.Drawing.Size(213, 20);
            this.txtpdvdesc.TabIndex = 13;
            // 
            // txtpdvcod
            // 
            this.txtpdvcod.EnterMoveNextControl = true;
            this.txtpdvcod.Location = new System.Drawing.Point(90, 65);
            this.txtpdvcod.MenuManager = this.barManager1;
            this.txtpdvcod.Name = "txtpdvcod";
            this.txtpdvcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpdvcod.Size = new System.Drawing.Size(37, 20);
            this.txtpdvcod.TabIndex = 12;
            this.txtpdvcod.TextChanged += new System.EventHandler(this.txtpdvcod_TextChanged);
            this.txtpdvcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpdvcod_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(10, 65);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Punto de venta";
            // 
            // txtnumactual
            // 
            this.txtnumactual.Enabled = false;
            this.txtnumactual.EnterMoveNextControl = true;
            this.txtnumactual.Location = new System.Drawing.Point(567, 43);
            this.txtnumactual.MenuManager = this.barManager1;
            this.txtnumactual.Name = "txtnumactual";
            this.txtnumactual.Size = new System.Drawing.Size(100, 20);
            this.txtnumactual.TabIndex = 10;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(503, 46);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(58, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Num. Actual";
            // 
            // txtnumfin
            // 
            this.txtnumfin.EnterMoveNextControl = true;
            this.txtnumfin.Location = new System.Drawing.Point(397, 43);
            this.txtnumfin.MenuManager = this.barManager1;
            this.txtnumfin.Name = "txtnumfin";
            this.txtnumfin.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnumfin.Size = new System.Drawing.Size(100, 20);
            this.txtnumfin.TabIndex = 8;
            this.txtnumfin.Leave += new System.EventHandler(this.txtnumfin_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(349, 46);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Num. Fin";
            // 
            // txtnuminicio
            // 
            this.txtnuminicio.EnterMoveNextControl = true;
            this.txtnuminicio.Location = new System.Drawing.Point(243, 43);
            this.txtnuminicio.MenuManager = this.barManager1;
            this.txtnuminicio.Name = "txtnuminicio";
            this.txtnuminicio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnuminicio.Size = new System.Drawing.Size(100, 20);
            this.txtnuminicio.TabIndex = 6;
            this.txtnuminicio.Leave += new System.EventHandler(this.txtnuminicio_Leave);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(184, 46);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Num. Inicio";
            // 
            // txtserie
            // 
            this.txtserie.EnterMoveNextControl = true;
            this.txtserie.Location = new System.Drawing.Point(90, 43);
            this.txtserie.MenuManager = this.barManager1;
            this.txtserie.Name = "txtserie";
            this.txtserie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtserie.Size = new System.Drawing.Size(88, 20);
            this.txtserie.TabIndex = 4;
            this.txtserie.Leave += new System.EventHandler(this.txtserie_Leave);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(56, 46);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(24, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Serie";
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(130, 20);
            this.txttipodocdesc.MenuManager = this.barManager1;
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodocdesc.Size = new System.Drawing.Size(537, 20);
            this.txttipodocdesc.TabIndex = 2;
            // 
            // txttipodoc
            // 
            this.txttipodoc.EnterMoveNextControl = true;
            this.txttipodoc.Location = new System.Drawing.Point(90, 20);
            this.txttipodoc.MenuManager = this.barManager1;
            this.txttipodoc.Name = "txttipodoc";
            this.txttipodoc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttipodoc.Size = new System.Drawing.Size(37, 20);
            this.txttipodoc.TabIndex = 1;
            this.txttipodoc.TextChanged += new System.EventHandler(this.txttipodoc_TextChanged);
            this.txttipodoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoc_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(36, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(44, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo doc.";
            // 
            // frm_series_comprobantes_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(672, 153);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_series_comprobantes_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Series de comprobantes";
            this.Load += new System.EventHandler(this.frm_series_comprobantes_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkelectronico.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaxlineas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcodsunat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechabaja.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaimpr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpdvcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumactual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumfin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnuminicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtserie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoc.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtnumactual;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtnumfin;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtnuminicio;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtserie;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.TextEdit txttipodoc;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtfechabaja;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtfechaimpr;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtpdvdesc;
        private DevExpress.XtraEditors.TextEdit txtpdvcod;
        private DevExpress.XtraEditors.TextEdit txtcodsunat;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtmaxlineas;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.CheckEdit chkelectronico;
    }
}
