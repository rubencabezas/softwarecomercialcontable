﻿namespace Comercial
{
    partial class frm_punto_venta_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnpc = new DevExpress.XtraEditors.SimpleButton();
            this.btnimpresora = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtnombrepc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacencod = new DevExpress.XtraEditors.TextEdit();
            this.txtnummaq = new DevExpress.XtraEditors.TextEdit();
            this.txtimpresora = new DevExpress.XtraEditors.TextEdit();
            this.txtestnombre = new DevExpress.XtraEditors.TextEdit();
            this.txtestcod = new DevExpress.XtraEditors.TextEdit();
            this.txtnombrepdv = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombrepc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnummaq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpresora.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombrepdv.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(512, 36);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 192);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(512, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 36);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 156);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(512, 36);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 156);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnpc);
            this.groupBox1.Controls.Add(this.btnimpresora);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.txtnombrepc);
            this.groupBox1.Controls.Add(this.txtalmacendesc);
            this.groupBox1.Controls.Add(this.txtalmacencod);
            this.groupBox1.Controls.Add(this.txtnummaq);
            this.groupBox1.Controls.Add(this.txtimpresora);
            this.groupBox1.Controls.Add(this.txtestnombre);
            this.groupBox1.Controls.Add(this.txtestcod);
            this.groupBox1.Controls.Add(this.txtnombrepdv);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(512, 156);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // btnpc
            // 
            this.btnpc.Location = new System.Drawing.Point(466, 84);
            this.btnpc.Name = "btnpc";
            this.btnpc.Size = new System.Drawing.Size(35, 23);
            this.btnpc.TabIndex = 14;
            this.btnpc.Text = "+";
            this.btnpc.Click += new System.EventHandler(this.btnpc_Click);
            // 
            // btnimpresora
            // 
            this.btnimpresora.Location = new System.Drawing.Point(466, 59);
            this.btnimpresora.Name = "btnimpresora";
            this.btnimpresora.Size = new System.Drawing.Size(35, 23);
            this.btnimpresora.TabIndex = 10;
            this.btnimpresora.Text = "+";
            this.btnimpresora.Click += new System.EventHandler(this.btnimpresora_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(16, 88);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(65, 13);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Nombre P.C.:";
            // 
            // txtnombrepc
            // 
            this.txtnombrepc.EnterMoveNextControl = true;
            this.txtnombrepc.Location = new System.Drawing.Point(86, 85);
            this.txtnombrepc.MenuManager = this.barManager1;
            this.txtnombrepc.Name = "txtnombrepc";
            this.txtnombrepc.Size = new System.Drawing.Size(374, 20);
            this.txtnombrepc.TabIndex = 13;
            // 
            // txtalmacendesc
            // 
            this.txtalmacendesc.Enabled = false;
            this.txtalmacendesc.EnterMoveNextControl = true;
            this.txtalmacendesc.Location = new System.Drawing.Point(133, 130);
            this.txtalmacendesc.MenuManager = this.barManager1;
            this.txtalmacendesc.Name = "txtalmacendesc";
            this.txtalmacendesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtalmacendesc.Size = new System.Drawing.Size(373, 20);
            this.txtalmacendesc.TabIndex = 12;
            // 
            // txtalmacencod
            // 
            this.txtalmacencod.EnterMoveNextControl = true;
            this.txtalmacencod.Location = new System.Drawing.Point(88, 130);
            this.txtalmacencod.MenuManager = this.barManager1;
            this.txtalmacencod.Name = "txtalmacencod";
            this.txtalmacencod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtalmacencod.Size = new System.Drawing.Size(42, 20);
            this.txtalmacencod.TabIndex = 11;
            this.txtalmacencod.TextChanged += new System.EventHandler(this.txtalmacencod_TextChanged);
            this.txtalmacencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtalmacencod_KeyDown);
            // 
            // txtnummaq
            // 
            this.txtnummaq.EnterMoveNextControl = true;
            this.txtnummaq.Location = new System.Drawing.Point(132, 107);
            this.txtnummaq.MenuManager = this.barManager1;
            this.txtnummaq.Name = "txtnummaq";
            this.txtnummaq.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnummaq.Size = new System.Drawing.Size(374, 20);
            this.txtnummaq.TabIndex = 10;
            // 
            // txtimpresora
            // 
            this.txtimpresora.EnterMoveNextControl = true;
            this.txtimpresora.Location = new System.Drawing.Point(86, 62);
            this.txtimpresora.MenuManager = this.barManager1;
            this.txtimpresora.Name = "txtimpresora";
            this.txtimpresora.Size = new System.Drawing.Size(374, 20);
            this.txtimpresora.TabIndex = 9;
            // 
            // txtestnombre
            // 
            this.txtestnombre.Enabled = false;
            this.txtestnombre.EnterMoveNextControl = true;
            this.txtestnombre.Location = new System.Drawing.Point(131, 39);
            this.txtestnombre.MenuManager = this.barManager1;
            this.txtestnombre.Name = "txtestnombre";
            this.txtestnombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestnombre.Size = new System.Drawing.Size(375, 20);
            this.txtestnombre.TabIndex = 8;
            // 
            // txtestcod
            // 
            this.txtestcod.EnterMoveNextControl = true;
            this.txtestcod.Location = new System.Drawing.Point(86, 39);
            this.txtestcod.MenuManager = this.barManager1;
            this.txtestcod.Name = "txtestcod";
            this.txtestcod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtestcod.Size = new System.Drawing.Size(42, 20);
            this.txtestcod.TabIndex = 7;
            this.txtestcod.TextChanged += new System.EventHandler(this.txtestcod_TextChanged);
            this.txtestcod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtestcod_KeyDown);
            // 
            // txtnombrepdv
            // 
            this.txtnombrepdv.EnterMoveNextControl = true;
            this.txtnombrepdv.Location = new System.Drawing.Point(86, 16);
            this.txtnombrepdv.MenuManager = this.barManager1;
            this.txtnombrepdv.Name = "txtnombrepdv";
            this.txtnombrepdv.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnombrepdv.Size = new System.Drawing.Size(420, 20);
            this.txtnombrepdv.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(37, 133);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(44, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Almacen:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(4, 110);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(120, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "N. maquina registradora:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(28, 65);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Impresora:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 42);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Establecimiento:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(40, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(41, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nombre:";
            // 
            // frm_punto_venta_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 192);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_punto_venta_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Punto de venta";
            this.Load += new System.EventHandler(this.frm_punto_venta_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombrepc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnummaq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtimpresora.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestnombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtestcod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombrepdv.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtalmacendesc;
        private DevExpress.XtraEditors.TextEdit txtalmacencod;
        private DevExpress.XtraEditors.TextEdit txtnummaq;
        private DevExpress.XtraEditors.TextEdit txtimpresora;
        private DevExpress.XtraEditors.TextEdit txtestnombre;
        private DevExpress.XtraEditors.TextEdit txtestcod;
        private DevExpress.XtraEditors.TextEdit txtnombrepdv;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtnombrepc;
        private DevExpress.XtraEditors.SimpleButton btnimpresora;
        private DevExpress.XtraEditors.SimpleButton btnpc;
    }
}