﻿namespace Comercial
{
    partial class frm_precio_edicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechaIni = new DevExpress.XtraEditors.TextEdit();
            this.txtfechaFin = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtmonedacod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtunmcodpres = new DevExpress.XtraEditors.TextEdit();
            this.txtcondiciondesc = new DevExpress.XtraEditors.TextEdit();
            this.txtcondicioncod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtvalorventa = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtunmdescpres = new DevExpress.XtraEditors.TextEdit();
            this.chkpreciovariable = new DevExpress.XtraEditors.CheckEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaIni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodpres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorventa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescpres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkpreciovariable.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;

            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(738, 38);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 326);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(738, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 38);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 288);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(738, 38);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 288);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(6, 44);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(201, 14);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "Valor de venta - no incluye I.G.V.";
            // 
            // txtfechaIni
            // 
            this.txtfechaIni.EnterMoveNextControl = true;
            this.txtfechaIni.Location = new System.Drawing.Point(362, 44);
            this.txtfechaIni.Name = "txtfechaIni";
            this.txtfechaIni.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechaIni.Properties.Mask.BeepOnError = true;
            this.txtfechaIni.Properties.Mask.EditMask = "d";
            this.txtfechaIni.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechaIni.Properties.Mask.SaveLiteral = false;
            this.txtfechaIni.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechaIni.Properties.MaxLength = 10;
            this.txtfechaIni.Size = new System.Drawing.Size(77, 20);
            this.txtfechaIni.TabIndex = 22;
            // 
            // txtfechaFin
            // 
            this.txtfechaFin.Enabled = false;
            this.txtfechaFin.EnterMoveNextControl = true;
            this.txtfechaFin.Location = new System.Drawing.Point(551, 44);
            this.txtfechaFin.Name = "txtfechaFin";
            this.txtfechaFin.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechaFin.Properties.Mask.BeepOnError = true;
            this.txtfechaFin.Properties.Mask.EditMask = "d";
            this.txtfechaFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechaFin.Properties.Mask.SaveLiteral = false;
            this.txtfechaFin.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechaFin.Properties.MaxLength = 10;
            this.txtfechaFin.Size = new System.Drawing.Size(70, 20);
            this.txtfechaFin.TabIndex = 24;
            // 
            // txtmonedadesc
            // 
            this.txtmonedadesc.Enabled = false;
            this.txtmonedadesc.EnterMoveNextControl = true;
            this.txtmonedadesc.Location = new System.Drawing.Point(130, 44);
            this.txtmonedadesc.Name = "txtmonedadesc";
            this.txtmonedadesc.Size = new System.Drawing.Size(141, 20);
            this.txtmonedadesc.TabIndex = 20;
            // 
            // txtmonedacod
            // 
            this.txtmonedacod.Enabled = false;
            this.txtmonedacod.EnterMoveNextControl = true;
            this.txtmonedacod.Location = new System.Drawing.Point(95, 44);
            this.txtmonedacod.Name = "txtmonedacod";
            this.txtmonedacod.Size = new System.Drawing.Size(34, 20);
            this.txtmonedacod.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(285, 49);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 13);
            this.labelControl3.TabIndex = 21;
            this.labelControl3.Text = "Fecha Vigencia:";
            // 
            // txtunmcodpres
            // 
            this.txtunmcodpres.EnterMoveNextControl = true;
            this.txtunmcodpres.Location = new System.Drawing.Point(95, 20);
            this.txtunmcodpres.Name = "txtunmcodpres";
            this.txtunmcodpres.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmcodpres.Properties.Appearance.Options.UseFont = true;
            this.txtunmcodpres.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmcodpres.Size = new System.Drawing.Size(34, 22);
            this.txtunmcodpres.TabIndex = 13;
            // 
            // txtcondiciondesc
            // 
            this.txtcondiciondesc.Enabled = false;
            this.txtcondiciondesc.EnterMoveNextControl = true;
            this.txtcondiciondesc.Location = new System.Drawing.Point(438, 20);
            this.txtcondiciondesc.Name = "txtcondiciondesc";
            this.txtcondiciondesc.Size = new System.Drawing.Size(183, 20);
            this.txtcondiciondesc.TabIndex = 17;
            // 
            // txtcondicioncod
            // 
            this.txtcondicioncod.Enabled = false;
            this.txtcondicioncod.EnterMoveNextControl = true;
            this.txtcondicioncod.Location = new System.Drawing.Point(378, 20);
            this.txtcondicioncod.Name = "txtcondicioncod";
            this.txtcondicioncod.Size = new System.Drawing.Size(58, 20);
            this.txtcondicioncod.TabIndex = 16;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(481, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(64, 13);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Fecha Hasta:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(47, 47);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(42, 13);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Moneda:";
            // 
            // txtvalorventa
            // 
            this.txtvalorventa.EditValue = "0.000";
            this.txtvalorventa.EnterMoveNextControl = true;
            this.txtvalorventa.Location = new System.Drawing.Point(356, 70);
            this.txtvalorventa.Name = "txtvalorventa";
            this.txtvalorventa.Properties.Mask.EditMask = "n3";
            this.txtvalorventa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtvalorventa.Size = new System.Drawing.Size(83, 20);
            this.txtvalorventa.TabIndex = 26;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "0.000";
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(538, 69);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Mask.EditMask = "n3";
            this.textEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit1.Size = new System.Drawing.Size(83, 20);
            this.textEdit1.TabIndex = 26;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(270, 74);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(83, 13);
            this.labelControl5.TabIndex = 25;
            this.labelControl5.Text = "Precio minimo:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(445, 74);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(87, 13);
            this.labelControl8.TabIndex = 25;
            this.labelControl8.Text = "Precio máximo:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(352, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(24, 13);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "Tipo:";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(0, 25);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(89, 13);
            this.labelControl14.TabIndex = 12;
            this.labelControl14.Text = "Unidad de medida:";
            // 
            // txtunmdescpres
            // 
            this.txtunmdescpres.Enabled = false;
            this.txtunmdescpres.EnterMoveNextControl = true;
            this.txtunmdescpres.Location = new System.Drawing.Point(129, 20);
            this.txtunmdescpres.Name = "txtunmdescpres";
            this.txtunmdescpres.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunmdescpres.Properties.Appearance.Options.UseFont = true;
            this.txtunmdescpres.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtunmdescpres.Size = new System.Drawing.Size(217, 22);
            this.txtunmdescpres.TabIndex = 14;
            // 
            // chkpreciovariable
            // 
            this.chkpreciovariable.Location = new System.Drawing.Point(95, 70);
            this.chkpreciovariable.MenuManager = this.barManager1;
            this.chkpreciovariable.Name = "chkpreciovariable";
            this.chkpreciovariable.Properties.Caption = "Es precio variable?";
            this.chkpreciovariable.Size = new System.Drawing.Size(122, 19);
            this.chkpreciovariable.TabIndex = 27;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkpreciovariable);
            this.groupBox2.Controls.Add(this.txtunmdescpres);
            this.groupBox2.Controls.Add(this.labelControl14);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Controls.Add(this.labelControl8);
            this.groupBox2.Controls.Add(this.labelControl5);
            this.groupBox2.Controls.Add(this.textEdit1);
            this.groupBox2.Controls.Add(this.txtvalorventa);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Controls.Add(this.txtcondicioncod);
            this.groupBox2.Controls.Add(this.txtcondiciondesc);
            this.groupBox2.Controls.Add(this.txtunmcodpres);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Controls.Add(this.txtmonedacod);
            this.groupBox2.Controls.Add(this.txtmonedadesc);
            this.groupBox2.Controls.Add(this.txtfechaFin);
            this.groupBox2.Controls.Add(this.txtfechaIni);
            this.groupBox2.Location = new System.Drawing.Point(3, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(658, 98);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Presentacion";
            // 
            // frm_precio_edicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(738, 326);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_precio_edicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Precio";
            this.Load += new System.EventHandler(this.frm_precio_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaIni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechaFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmonedacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmcodpres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondiciondesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcondicioncod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalorventa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtunmdescpres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkpreciovariable.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.CheckEdit chkpreciovariable;
        public DevExpress.XtraEditors.TextEdit txtunmdescpres;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit txtvalorventa;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtcondicioncod;
        private DevExpress.XtraEditors.TextEdit txtcondiciondesc;
        public DevExpress.XtraEditors.TextEdit txtunmcodpres;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtmonedacod;
        private DevExpress.XtraEditors.TextEdit txtmonedadesc;
        private DevExpress.XtraEditors.TextEdit txtfechaFin;
        private DevExpress.XtraEditors.TextEdit txtfechaIni;
    }
}
