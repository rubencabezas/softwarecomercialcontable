﻿namespace Comercial
{
    partial class frm_entidad_edicion_comercial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnguardar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtnumero = new DevExpress.XtraEditors.TextEdit();
            this.txttipodocdesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtcorreo = new DevExpress.XtraEditors.TextEdit();
            this.txttipodoccod = new DevExpress.XtraEditors.TextEdit();
            this.txtmovil = new DevExpress.XtraEditors.TextEdit();
            this.txttipopersonacod = new DevExpress.XtraEditors.TextEdit();
            this.txttelefono = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtapematerno = new DevExpress.XtraEditors.TextEdit();
            this.txttipopersonadesc = new DevExpress.XtraEditors.TextEdit();
            this.txtpaginaweb = new DevExpress.XtraEditors.TextEdit();
            this.txtdomifiscal = new DevExpress.XtraEditors.TextEdit();
            this.txtrepresentantelegal = new DevExpress.XtraEditors.TextEdit();
            this.txtappaterno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtrazonsocial = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcorreo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoccod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmovil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipopersonacod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttelefono.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtapematerno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipopersonadesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpaginaweb.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdomifiscal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrepresentantelegal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtappaterno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrazonsocial.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnguardar,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnguardar)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnguardar
            // 
            this.btnguardar.Caption = "Guardar [F4]";
            this.btnguardar.Id = 0;
            this.btnguardar.ImageOptions.Image = global::Contable.Properties.Resources.Save_24px;
            this.btnguardar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnguardar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnguardar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnguardar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(834, 36);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 219);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(834, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 36);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 183);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(834, 36);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 183);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem2.Caption = "Cancelar";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtnumero);
            this.groupBox1.Controls.Add(this.txttipodocdesc);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Controls.Add(this.txtcorreo);
            this.groupBox1.Controls.Add(this.txttipodoccod);
            this.groupBox1.Controls.Add(this.txtmovil);
            this.groupBox1.Controls.Add(this.txttipopersonacod);
            this.groupBox1.Controls.Add(this.txttelefono);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.txtapematerno);
            this.groupBox1.Controls.Add(this.txttipopersonadesc);
            this.groupBox1.Controls.Add(this.txtpaginaweb);
            this.groupBox1.Controls.Add(this.txtdomifiscal);
            this.groupBox1.Controls.Add(this.txtrepresentantelegal);
            this.groupBox1.Controls.Add(this.txtappaterno);
            this.groupBox1.Controls.Add(this.labelControl11);
            this.groupBox1.Controls.Add(this.labelControl9);
            this.groupBox1.Controls.Add(this.labelControl8);
            this.groupBox1.Controls.Add(this.labelControl12);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Controls.Add(this.txtrazonsocial);
            this.groupBox1.Controls.Add(this.labelControl7);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(834, 183);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // txtnumero
            // 
            this.txtnumero.EnterMoveNextControl = true;
            this.txtnumero.Location = new System.Drawing.Point(122, 28);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnumero.Properties.MaxLength = 11;
            this.txtnumero.Size = new System.Drawing.Size(140, 20);
            this.txtnumero.TabIndex = 27;
            this.txtnumero.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnumero_KeyDown);
            // 
            // txttipodocdesc
            // 
            this.txttipodocdesc.Enabled = false;
            this.txttipodocdesc.EnterMoveNextControl = true;
            this.txttipodocdesc.Location = new System.Drawing.Point(625, 28);
            this.txttipodocdesc.Name = "txttipodocdesc";
            this.txttipodocdesc.Size = new System.Drawing.Size(198, 20);
            this.txttipodocdesc.TabIndex = 33;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(69, 31);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(47, 13);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "Nnumero:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(265, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 13);
            this.labelControl1.TabIndex = 28;
            this.labelControl1.Text = "Tipo Persona:";
            // 
            // txtcorreo
            // 
            this.txtcorreo.EnterMoveNextControl = true;
            this.txtcorreo.Location = new System.Drawing.Point(530, 119);
            this.txtcorreo.Name = "txtcorreo";
            this.txtcorreo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcorreo.Size = new System.Drawing.Size(293, 20);
            this.txtcorreo.TabIndex = 49;
            // 
            // txttipodoccod
            // 
            this.txttipodoccod.EnterMoveNextControl = true;
            this.txttipodoccod.Location = new System.Drawing.Point(586, 28);
            this.txttipodoccod.Name = "txttipodoccod";
            this.txttipodoccod.Size = new System.Drawing.Size(35, 20);
            this.txttipodoccod.TabIndex = 32;
            this.txttipodoccod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipodoccod_KeyDown);
            // 
            // txtmovil
            // 
            this.txtmovil.EnterMoveNextControl = true;
            this.txtmovil.Location = new System.Drawing.Point(628, 97);
            this.txtmovil.Name = "txtmovil";
            this.txtmovil.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtmovil.Size = new System.Drawing.Size(195, 20);
            this.txtmovil.TabIndex = 45;
            // 
            // txttipopersonacod
            // 
            this.txttipopersonacod.EnterMoveNextControl = true;
            this.txttipopersonacod.Location = new System.Drawing.Point(334, 28);
            this.txttipopersonacod.MenuManager = this.barManager1;
            this.txttipopersonacod.Name = "txttipopersonacod";
            this.txttipopersonacod.Size = new System.Drawing.Size(39, 20);
            this.txttipopersonacod.TabIndex = 29;
            this.txttipopersonacod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttipopersonacod_KeyDown);
            // 
            // txttelefono
            // 
            this.txttelefono.EnterMoveNextControl = true;
            this.txttelefono.Location = new System.Drawing.Point(628, 75);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Properties.Mask.EditMask = "n0";
            this.txttelefono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txttelefono.Size = new System.Drawing.Size(195, 20);
            this.txttelefono.TabIndex = 41;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(535, 31);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 13);
            this.labelControl2.TabIndex = 31;
            this.labelControl2.Text = "Tipo Doc.";
            // 
            // txtapematerno
            // 
            this.txtapematerno.EnterMoveNextControl = true;
            this.txtapematerno.Location = new System.Drawing.Point(384, 74);
            this.txtapematerno.Name = "txtapematerno";
            this.txtapematerno.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtapematerno.Size = new System.Drawing.Size(164, 20);
            this.txtapematerno.TabIndex = 39;
            // 
            // txttipopersonadesc
            // 
            this.txttipopersonadesc.Enabled = false;
            this.txttipopersonadesc.EnterMoveNextControl = true;
            this.txttipopersonadesc.Location = new System.Drawing.Point(376, 28);
            this.txttipopersonadesc.Name = "txttipopersonadesc";
            this.txttipopersonadesc.Size = new System.Drawing.Size(151, 20);
            this.txttipopersonadesc.TabIndex = 30;
            // 
            // txtpaginaweb
            // 
            this.txtpaginaweb.EnterMoveNextControl = true;
            this.txtpaginaweb.Location = new System.Drawing.Point(124, 142);
            this.txtpaginaweb.Name = "txtpaginaweb";
            this.txtpaginaweb.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpaginaweb.Size = new System.Drawing.Size(357, 20);
            this.txtpaginaweb.TabIndex = 51;
            // 
            // txtdomifiscal
            // 
            this.txtdomifiscal.EnterMoveNextControl = true;
            this.txtdomifiscal.Location = new System.Drawing.Point(124, 119);
            this.txtdomifiscal.Name = "txtdomifiscal";
            this.txtdomifiscal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdomifiscal.Size = new System.Drawing.Size(357, 20);
            this.txtdomifiscal.TabIndex = 47;
            // 
            // txtrepresentantelegal
            // 
            this.txtrepresentantelegal.EnterMoveNextControl = true;
            this.txtrepresentantelegal.Location = new System.Drawing.Point(124, 97);
            this.txtrepresentantelegal.Name = "txtrepresentantelegal";
            this.txtrepresentantelegal.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrepresentantelegal.Size = new System.Drawing.Size(424, 20);
            this.txtrepresentantelegal.TabIndex = 43;
            // 
            // txtappaterno
            // 
            this.txtappaterno.EnterMoveNextControl = true;
            this.txtappaterno.Location = new System.Drawing.Point(124, 74);
            this.txtappaterno.Name = "txtappaterno";
            this.txtappaterno.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtappaterno.Size = new System.Drawing.Size(164, 20);
            this.txtappaterno.TabIndex = 37;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(487, 123);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(37, 13);
            this.labelControl11.TabIndex = 48;
            this.labelControl11.Text = "Correo:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(554, 101);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(73, 13);
            this.labelControl9.TabIndex = 44;
            this.labelControl9.Text = "Telefono Movil:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(581, 77);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(46, 13);
            this.labelControl8.TabIndex = 40;
            this.labelControl8.Text = "Telefono:";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(61, 145);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(61, 13);
            this.labelControl12.TabIndex = 50;
            this.labelControl12.Text = "Pagina Web:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(300, 77);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(84, 13);
            this.labelControl6.TabIndex = 38;
            this.labelControl6.Text = "Apellido Materno:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(49, 122);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(73, 13);
            this.labelControl10.TabIndex = 46;
            this.labelControl10.Text = "Domicilio Fiscal:";
            // 
            // txtrazonsocial
            // 
            this.txtrazonsocial.EnterMoveNextControl = true;
            this.txtrazonsocial.Location = new System.Drawing.Point(122, 52);
            this.txtrazonsocial.Name = "txtrazonsocial";
            this.txtrazonsocial.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrazonsocial.Size = new System.Drawing.Size(701, 20);
            this.txtrazonsocial.TabIndex = 35;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(18, 101);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(104, 13);
            this.labelControl7.TabIndex = 42;
            this.labelControl7.Text = "Representante Legal:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(40, 77);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(82, 13);
            this.labelControl5.TabIndex = 36;
            this.labelControl5.Text = "Apellido Paterno:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(11, 55);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(111, 13);
            this.labelControl4.TabIndex = 34;
            this.labelControl4.Text = "Razon Social / Nombre:";
            // 
            // frm_entidad_edicion_comercial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(834, 219);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_entidad_edicion_comercial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Entidad";
            this.Load += new System.EventHandler(this.frm_entidad_edicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnumero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodocdesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcorreo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipodoccod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmovil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipopersonacod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttelefono.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtapematerno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttipopersonadesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpaginaweb.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdomifiscal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrepresentantelegal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtappaterno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrazonsocial.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnguardar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.GroupBox groupBox1;
        public DevExpress.XtraEditors.TextEdit txtnumero;
        public DevExpress.XtraEditors.TextEdit txttipodocdesc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.TextEdit txtcorreo;
        public DevExpress.XtraEditors.TextEdit txttipodoccod;
        public DevExpress.XtraEditors.TextEdit txtmovil;
        public DevExpress.XtraEditors.TextEdit txttipopersonacod;
        public DevExpress.XtraEditors.TextEdit txttelefono;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.TextEdit txtapematerno;
        public DevExpress.XtraEditors.TextEdit txttipopersonadesc;
        public DevExpress.XtraEditors.TextEdit txtpaginaweb;
        public DevExpress.XtraEditors.TextEdit txtdomifiscal;
        public DevExpress.XtraEditors.TextEdit txtrepresentantelegal;
        public DevExpress.XtraEditors.TextEdit txtappaterno;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        public DevExpress.XtraEditors.TextEdit txtrazonsocial;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
    }
}
