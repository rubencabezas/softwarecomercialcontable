﻿using Contable;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_roles_01 :  frm_fuente
    {
        public frm_roles_01()
        {
            InitializeComponent();
        }

        public List<Entidad_Usuario> Lista = new List<Entidad_Usuario>();
        public void Listar_Usuarios()
        {
            Entidad_Usuario Ent = new Entidad_Usuario();
            Logica_Usuario log = new Logica_Usuario();


            Ent.Usuario_dni = null;

            try
            {
                Lista = log.Listar(Ent);

                dgvdatos.DataSource = null;

                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        private List<Entidad_Empresa> ListEmp = new List<Entidad_Empresa>();
        public void Listar_Empresas()
        {
            try
            {
                Logica_Empresa LogEmp = new Logica_Empresa();
                Entidad_Empresa Ent = new Entidad_Empresa();

                ListEmp = LogEmp.Listar_Emp(Ent);

                //TreeListColumn newColumnDescription = treeListEmpresas.Columns.Add();
                //newColumnDescription.Caption = "Empresas";
                //newColumnDescription.FieldName = "Emp_Nombre";
                //newColumnDescription.VisibleIndex = 0;
                //TreeListNode Nodo = treeListEmpresas.Nodes.Add();

                GenerarColumnas(treeListEmpresas);
                foreach (Entidad_Empresa empre in ListEmp)
                {
                    TreeListNode father = null;
                    TreeListNode boton;
                    boton = treeListEmpresas.AppendNode(new object[] { empre.Emp_Nombre, empre.Id_Empresa }, father);
                    treeListEmpresas.OptionsView.ShowCheckBoxes = true;

                    foreach(Entidad_Modulo Emp in ChekList_Empresas)
                    {
                        if (Emp.Id_Empresas == empre.Id_Empresa)
                        {
                            boton.Checked = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        void GenerarColumnas(DevExpress.XtraTreeList.TreeList tree)
        {
            tree.OptionsView.ShowHorzLines = false;
            tree.OptionsView.ShowIndicator = false;
            tree.OptionsView.ShowVertLines = true;
            tree.OptionsView.ShowColumns = true;
            tree.OptionsView.AutoWidth = true;
            tree.OptionsBehavior.Editable = true;

            tree.BeginUpdate();
            tree.Columns.Add();
            tree.Columns[0].Caption = "EMPRESAS";
            tree.Columns[0].VisibleIndex = 0;
            tree.Columns[0].OptionsColumn.AllowEdit = false;
            tree.Columns[0].Width = 200;
            tree.Columns.Add();
            tree.Columns[1].Caption = "";
            tree.Columns[1].VisibleIndex = 1;
            tree.Columns[1].OptionsColumn.AllowEdit = false;
            tree.Columns[1].Width = 60;
            tree.Columns[1].Visible = false;
            tree.EndUpdate();
        }

        private List<Entidad_Modulo> Lista_Modulo_Grupo_Opciones = new List<Entidad_Modulo>();

        private List<Entidad_Modulo> Lista_Solo_Modulos = new List<Entidad_Modulo>();

        private List<Entidad_Modulo> ListaOpciones = new List<Entidad_Modulo>();
        void Listar_Modulos()
        {

            Entidad_Modulo Ent = new Entidad_Modulo();
            Logica_Modulo log = new Logica_Modulo();

            Ent.Id_modulo = null;

            try
            {
                //Lista_Solo_Modulos = log.Listar(Ent);

                Lista_Modulo_Grupo_Opciones = log.Listar_Todos_Modulos(Ent);

                 foreach (Entidad_Modulo Op in Lista_Modulo_Grupo_Opciones)
                    {
                        if (!ExisteModulo(Op))
                        {
                            soloModulo.Add(Op);
                        }
                    }

                foreach(Entidad_Modulo Item in soloModulo)
                {
                    foreach (Entidad_Modulo Op in Lista_Modulo_Grupo_Opciones)
                    {
                        if (!Existe(Op))
                        {
                            SoloGrupos.Add(Op);
                        }
                    }

                    foreach (Entidad_Modulo Op in Lista_Modulo_Grupo_Opciones)
                    {
                        if (!Existe(Op))
                        {
                            SoloGrupos.Add(Op);
                        }
                    }

                    foreach (Entidad_Modulo Op in Lista_Modulo_Grupo_Opciones)
                    {
                        if (!Existe_Opciones(Op))
                        {
                            SoloOpciones.Add(Op);
                        }
                    }

                    GenerarColumnas_Modulos(treeListModulos);

                    foreach(Entidad_Modulo modulo in soloModulo)
                    {
                        TreeListNode father = null;
                        TreeListNode boton;
                        boton = treeListModulos.AppendNode(new object[] { modulo.Mod_Descripcion, modulo.Id_modulo }, father);
                        treeListModulos.OptionsView.ShowCheckBoxes = true;

                        foreach(Entidad_Modulo grup in SoloGrupos)
                        {

                            TreeListNode nuevochil;
                            nuevochil = treeListModulos.AppendNode(new object[] { grup.Gru_Descripcion, grup.Id_Grupo }, boton);

                            foreach (Entidad_Modulo opc in SoloOpciones)
                            {
                                if (grup.Id_Grupo == opc.Id_Grupo)
                                {
                                     TreeListNode nuevochilOpciones;
                                nuevochilOpciones = treeListModulos.AppendNode(new object[] { opc.Opc_Descripcion, opc.Id_Opcion}, nuevochil);

                                //aqui buscamos si las opciones han sido asignadas a un usuario
                                //solo para ponerle el chek 
            
                                foreach(Entidad_Modulo Select in ChekList_Asignados)
                                {
                                        if (opc.Id_Opcion == Select.Id_Opcion)
                                        {
                                            nuevochilOpciones.Checked = true;
                                             nuevochil.Checked = true;
                                             boton.Checked = true;
                                            Entidad_Modulo entOpc = new Entidad_Modulo();
                                            Ent.Id_Opcion = Select.Id_Opcion;
                                            Ent.Usuario_dni = User_DNI;
                                            Usuario_Opciones.Add(Ent);
                                        }
                                }
                                }
                          
                            }
                        }
                        treeListModulos.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }



        ArrayList ExisteSubGrupo(Entidad_Modulo Gr, Entidad_Modulo SGr)
        {
            ArrayList Retorno = new ArrayList();
            foreach (Entidad_Modulo op in Gr.ListaOpciones)
            {
                if ((SGr.Id_Opcion == op.Id_Opcion))
                {
                    Retorno.Add(true);
                    Retorno.Add(op);
                    return Retorno;
                }

            }

            Retorno.Add(false);
            return Retorno;
        }
        void GenerarColumnas_Modulos(DevExpress.XtraTreeList.TreeList tree)
        {
            tree.OptionsView.ShowHorzLines = false;
            tree.OptionsView.ShowIndicator = false;
            tree.OptionsView.ShowVertLines = true;
            tree.OptionsView.ShowColumns = true;
            tree.OptionsView.AutoWidth = true;
            tree.OptionsBehavior.Editable = true;

            tree.BeginUpdate();
            tree.Columns.Add();
            tree.Columns[0].Caption = "MODULOS";
            tree.Columns[0].VisibleIndex = 0;
            tree.Columns[0].OptionsColumn.AllowEdit = false;
            tree.Columns[0].Width = 200;
            tree.Columns.Add();
            tree.Columns[1].Caption = "";
            tree.Columns[1].VisibleIndex = 1;
            tree.Columns[1].OptionsColumn.AllowEdit = false;
            tree.Columns[1].Width = 60;
            tree.Columns[1].Visible = false;
            tree.EndUpdate();
        }

        List<Entidad_Modulo> soloModulo =new List<Entidad_Modulo>();
        bool ExisteModulo(Entidad_Modulo Gr)
        {
            foreach (Entidad_Modulo op in soloModulo)
            {
                if ((Gr.Id_modulo == op.Id_modulo))
                {
                    return true;
                }
            }
            return false;
        }

        List<Entidad_Modulo> SoloGrupos = new List<Entidad_Modulo>();
        bool Existe(Entidad_Modulo op)
        {
            foreach (Entidad_Modulo Grup in SoloGrupos)
            {
                if (((op.Id_Grupo == Grup.Id_Grupo) && (op.Id_modulo == Grup.Id_modulo)))
                {
                    return true;
                }
            }
            return false;
        }

        List<Entidad_Modulo> SoloOpciones = new List<Entidad_Modulo>();
        bool Existe_Opciones(Entidad_Modulo op)
        {
            foreach (Entidad_Modulo opc in SoloOpciones)
            {
                if (( (op.Id_modulo == opc.Id_modulo) && (op.Id_Grupo == opc.Id_Grupo)&& (op.Id_Opcion == opc.Id_Opcion)))
                {
                    return true;
                }
            }
            return false;
        }
        private void frm_roles_01_Load(object sender, EventArgs e)
        {
            treeListEmpresas.ClearNodes();
            treeListModulos.ClearNodes();

            Listar_Usuarios();
            //Listar_Empresas();
            //Listar_Modulos();
        }

        private void treeListModulos_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            SetCheckedChildNodes(e.Node, e.Node.CheckState);
            SetCheckedParentNodes(e.Node, e.Node.CheckState);
        }
        private void SetCheckedChildNodes(TreeListNode node, CheckState check)
        {
            for (int i = 0; (i <= (node.Nodes.Count - 1)); i++)
            {
                node.Nodes[i].CheckState = check;
                SetCheckedChildNodes(node.Nodes[i], check);


            }

        }
        private void SetCheckedParentNodes(TreeListNode node, CheckState check)
        {
            if (node.ParentNode != null)
            {
                bool b = false;
                CheckState state;
                for (int i = 0; (i <= (node.ParentNode.Nodes.Count - 1)); i++)
                {
                    state = ((CheckState)(node.ParentNode.Nodes[i].CheckState));
                    if (!check.Equals(state))
                    {
                        b = !b;
                        break;
                    }
                }

                if (b)
                {
                    node.ParentNode.CheckState = CheckState.Checked;
                }
                else
                {
                    node.ParentNode.CheckState = check;
                }
                SetCheckedParentNodes(node.ParentNode, check);
            }
        }

        List<Entidad_Modulo> Usuario_Opciones = new List<Entidad_Modulo>();
        List<Entidad_Modulo> Usuario_Empresas = new List<Entidad_Modulo>();
        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                Entidad_Modulo enti = new Entidad_Modulo();
                Logica_Modulo logi = new Logica_Modulo();

                enti.Usuario_dni = User_DNI;

                Usuario_Opciones.Clear();
                Usuario_Empresas.Clear();

                foreach (TreeListNode A in treeListModulos.Nodes)
                {
                    foreach(TreeListNode B in A.Nodes)
                    {
                        foreach(TreeListNode C in B.Nodes)
                        {
                            if (C.Checked)
                            {
                                Entidad_Modulo entOpc = new Entidad_Modulo();
                                entOpc.Id_Opcion = Convert.ToString(C.GetValue(1));
                                entOpc.Usuario_dni = User_DNI;
                                Usuario_Opciones.Add(entOpc);
                            }
                        }
                    }
                }


                enti.ListaOpciones = Usuario_Opciones;

                foreach (DevExpress.XtraTreeList.Nodes.TreeListNode n in treeListEmpresas.Nodes)
                {
                    if (n.Checked)
                    {
                        Entidad_Modulo entEmpre = new Entidad_Modulo();
                        entEmpre.Id_Empresas = Convert.ToString(n.GetValue(1));
                        entEmpre.Usuario_dni = User_DNI;
                        Usuario_Empresas.Add(entEmpre);
                    }
                }

                enti.ListaEmpresas = Usuario_Empresas;

                if (Estado == Estados.Nuevo)
                {
                    if (logi.Insertar(enti))
                    {
                        Accion.ExitoGuardar();
                    }

                }else if (Estado == Estados.Modificar)
                {
                    if (logi.Modificar(enti))
                    {
                        Accion.ExitoModificar();
                    }
                }

            }
            catch(Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }




        }

        string User_DNI;
        List<Entidad_Modulo> ChekList_Asignados = new List<Entidad_Modulo>();
        List<Entidad_Modulo> ChekList_Empresas = new List<Entidad_Modulo>();

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if ((dgvdatos.DataSource!=null) & (gridView1.GetFocusedDataSourceRowIndex() > -1)){

                    if (Lista.Count > 0)
                    {
                        treeListEmpresas.ClearNodes();
                        treeListModulos.ClearNodes();

                        Entidad_Usuario ent = new Entidad_Usuario();

                        ent = Lista[gridView1.GetFocusedDataSourceRowIndex()];

                        User_DNI = ent.Usuario_dni;
                        //PARA DEFINIR SI ES UNO NUEVO O SI ES MODIFICAR(DEBE ENCONTRAR AL MENOS MAS DE UNO ASIGNADO PARA QUE SEA MODIFICAR0
                        Entidad_Modulo Enti = new Entidad_Modulo();
                        Logica_Modulo log = new Logica_Modulo();
                        List<Entidad_Modulo> Cantidad_Asignada_Opciones = new List<Entidad_Modulo>();
                        List<Entidad_Modulo> Cantidad_Asignada_Empresas= new List<Entidad_Modulo>();

                        Enti.Usuario_dni = User_DNI;
                        Cantidad_Asignada_Opciones = log.Contar_Opcion(Enti);
                        Cantidad_Asignada_Empresas = log.Contar_Empre(Enti);


                        ChekList_Asignados = log.Opciones_Asignadas_X_Usuario(Enti);

                        ChekList_Empresas = log.Empresas_Asignadas_X_Usuario(Enti);

                        if (Cantidad_Asignada_Opciones[0].contar == 0 || Cantidad_Asignada_Empresas[0].contar_Empre==0)
                        {
                            Estado = Estados.Nuevo;
                        }
                        else
                        {
                            Estado = Estados.Modificar;
                        }
                        Listar_Modulos();
                        Listar_Empresas();
                    }
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            treeListEmpresas.ClearNodes();
            treeListModulos.ClearNodes();

            Listar_Usuarios();
        }
    }
}
