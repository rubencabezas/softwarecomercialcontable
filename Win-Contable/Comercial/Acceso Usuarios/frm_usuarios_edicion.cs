﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_usuarios_edicion :  frm_fuente
    {
        public frm_usuarios_edicion()
        {
            InitializeComponent();
        }
        public string Estado_Ven_Boton,Usuario_dni;
        void Limpiar()
        {
            txtrucdni.ResetText();
            txtnombres.ResetText();
            txtpass.ResetText();
            txtpassconfirmar.ResetText();
        }
        public bool VerificarCabecera()
        {
            if (string.IsNullOrEmpty(txtrucdni.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un numero de DNI");
                txtrucdni.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtnombres.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar el nombre del usuario");
                txtrucdni.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtpass.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una contraseña");
                txtpass.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtpassconfirmar.Text.Trim()))
            {
                Accion.Advertencia("Debe Ingresar una contraseña");
                txtpassconfirmar.Focus();
                return false;
            }
            return true;
        }

        private void frm_usuarios_edicion_Load(object sender, EventArgs e)
        {
            if (Estado_Ven_Boton == "1")
            {
                Estado = Estados.Nuevo;
                Limpiar();

                txtrucdni.Select();

            }
            else if (Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
                txtrucdni.Enabled = false;
                ListarModificar();

            }
        }


        public List<Entidad_Usuario> Lista_Modificar = new List<Entidad_Usuario>();
        public void ListarModificar()
        {
            Entidad_Usuario Ent = new Entidad_Usuario();
            Logica_Usuario log = new Logica_Usuario();

            Ent.Usuario_dni = Usuario_dni;

            try
            {
                Lista_Modificar = log.Listar(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Usuario Enti = new Entidad_Usuario();
                    Enti = Lista_Modificar[0];
                    txtrucdni.Text = Enti.Usuario_dni;

                    txtnombres.Text = Enti.Usuario_Entidad;

                    //TxtPassword.Text = Tools.DecryptPasswordMD5(Ent_User.contrase, "12345678901")
                    txtpass.Text = Accion.Desencriptar(Enti.Usuario_Pass);
                    txtpassconfirmar.Text =Accion.Desencriptar(Enti.Usuario_Confirmar_Pass);

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }


        private void btnguardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                if (VerificarCabecera())
                {

                    Entidad_Usuario Ent = new Entidad_Usuario();
                    Logica_Usuario Log = new Logica_Usuario();


                    Ent.Usuario_dni = txtrucdni.Text.Trim();

                    string ret = Accion.Encriptar(txtpass.Text.Trim());
                    Ent.Usuario_Pass = ret;
                    Ent.Usuario_Entidad = txtnombres.Text.Trim();

                    Ent.Usuario_Confirmar_Pass = ret;

                    if (Estado == Estados.Nuevo)
                    {
                        if (Log.Insertar(Ent))
                        {

                            Accion.ExitoGuardar();
                            Estado = Estados.Nuevo;
                            Limpiar();


                            txtrucdni.Focus();


                        }

                    }
                    else if (Estado == Estados.Modificar)
                    {
                        if (Log.Modificar(Ent))
                        {
                            Accion.ExitoModificar();
                            this.Close();
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }


    }
    }

