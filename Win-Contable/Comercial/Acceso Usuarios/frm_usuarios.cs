﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_usuarios :  frm_fuente
    {
        public frm_usuarios()
        {
            InitializeComponent();
        }

        public List<Entidad_Usuario> Lista = new List<Entidad_Usuario>();
        public void Listar()
        {
            Entidad_Usuario Ent = new Entidad_Usuario();
            Logica_Usuario log = new Logica_Usuario();


            Ent.Usuario_dni = null;

            try
            {
                Lista = log.Listar(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void frm_usuarios_Load(object sender, EventArgs e)
        {
            Listar();

        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_usuarios_edicion f = new frm_usuarios_edicion())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Usuario_dni = Entidad.Usuario_dni;


                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }
            }
        }

        Entidad_Usuario Entidad = new Entidad_Usuario();

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lista.Count > 0)
                {
                    Estado = Estados.Ninguno;
                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_usuarios_edicion f = new frm_usuarios_edicion())
            {


                Estado = Estados.Nuevo;
                f.Estado_Ven_Boton = "1";

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }
                }
                else
                {
                    Listar();
                }

            }

        }

        private void btneliminar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Usuario_dni + ":" + Entidad.Usuario_Entidad + "  si hace en el boton ACEPTAR no podra revertir los cambios." ;
          
            if (Accion.ShowDeleted(DocDelted))
            {
                try
                {
                    Entidad_Usuario Ent = new Entidad_Usuario
                    {
                        Usuario_dni = Entidad.Usuario_dni
                    };

                    Logica_Usuario log = new Logica_Usuario();

                    log.Eliminar(Ent);
                    Accion.ExitoGuardar();
                    Listar();

                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }

            }
        }
    }
}
