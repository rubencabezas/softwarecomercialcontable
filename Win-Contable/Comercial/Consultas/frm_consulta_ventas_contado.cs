﻿using Contable;
using Contable._2_Alertas;
using DevExpress.XtraPrinting.Native;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_consulta_ventas_contado : frm_fuente
    {
        public frm_consulta_ventas_contado()
        {
            InitializeComponent();
        }
        string Ruta = (Application.StartupPath + "\\DOCUMENTOS\\");
        string Id_Libro;
        //string BuscarArchivo;
        //string Buffer;
        public void TraerLibro()
        {
            try
            {
                Logica_Parametro_Inicial log = new Logica_Parametro_Inicial();

                List<Entidad_Parametro_Inicial> Generales = new List<Entidad_Parametro_Inicial>();
                Generales = log.Traer_Libro_Venta(new Entidad_Parametro_Inicial
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Anio = Actual_Conexion.AnioSelect
                });

                if (Generales.Count > 0)
                {
                    Id_Libro = Generales[0].Ini_Venta;

                }
                else
                {
                    Accion.Advertencia("Debe configurar un libro para este proceso");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {
            
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = null;
            try
            {
                Lista = log.Listar_adm_ventas(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void frm_consulta_ventas_contado_Load(object sender, EventArgs e)
        {
            
            //System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {
                 //p.Start();
                dgvdatos.Dock = DockStyle.Fill;
                TraerLibro();
                Listar();
              
                //p.Abort();
            }
            catch (Exception ex)
            {
                //p.Abort();
            }

        }

        public void Progreso()
        {
            WaitForm1 dlg = new WaitForm1();
            dlg.ShowDialog();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            using (frm_vista_previa f = new frm_vista_previa())
            {
               f.Serie = Entidad.Ctb_Serie;
                f.Numero = Entidad.Ctb_Numero;
                f.Nombre_Carpeta = "VENTAS AL CONTADO";
                if (f.ShowDialog() == DialogResult.OK)
                {
          
                }
            }
        }

        public string UbicarArchivo(string Ruta, string Documento)
        {
            string ArchivoEncontrado = "";
            string[] files = System.IO.Directory.GetFiles(Ruta, Documento);
            foreach (string file in files)
            {
                ArchivoEncontrado = file;
            }
            return ArchivoEncontrado;
        }


        public string Copia()
        {
            string Txt;
            Txt = (new string('*', 40) + ("\r\n" + ("*" + (new string(' ', 17) + ("COPIA" + (new string(' ', 16) + ("*" + ("\r\n" + (new string('*', 40) + "\r\n")))))))));
            return Txt;
        }

        Entidad_Movimiento_Cab Entidad = new Entidad_Movimiento_Cab();
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {//     if (ListaFiltrada.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;

                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Entidad_Caja ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();
            List<Entidad_Caja> list_Caja_Aper = new List<Entidad_Caja>();

            ent.Id_Empresa = Entidad.Id_Empresa;
            ent.Pdv_Codigo = Entidad.ctb_pdv_codigo;
            ent.Cja_Codigo = Entidad.Cja_Codigo;
            ent.Cja_Aper_Cierre_Codigo = Entidad.Cja_Aper_Cierre_Codigo;

            list_Caja_Aper = log.Listar_Caja_Aperturado(ent);

            if (list_Caja_Aper.Count > 0)
            {
                if (list_Caja_Aper[0].Cja_Estado == "0058")//APERTURADO //SI A UN ESTA APERTURADO SERVIRA ELIMINAR
                {
                    string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
                    if (Accion.ShowDeleted(DocDelted))
                    {
                        try
                        {
                            Entidad_Movimiento_Cab EntI = new Entidad_Movimiento_Cab
                            {
                                Id_Empresa = Entidad.Id_Empresa,
                                Id_Anio = Entidad.Id_Anio,
                                Id_Periodo = Entidad.Id_Periodo,
                                Id_Voucher = Entidad.Id_Voucher,
                                Id_Libro = Entidad.Id_Libro
                            };

                            Logica_Movimiento_Cab logI = new Logica_Movimiento_Cab();

                            logI.Eliminar_Venta(EntI);
                            Accion.ExitoGuardar();
                            Listar();

                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                        }

                    }
                }
                else
                {
                    Accion.Advertencia("YA NO PUEDE ELIMINAR ESTE REGISTRO,YA QUE LA CAJA FUE CERRADO");
                }

            }else {
                Accion.Advertencia("ESTE REGISTRO NO SE ENCUENTRA EN UNA CAJA APERTURADA,NO SE PUEDE ELIMINAR");
            }




        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Entidad_Caja ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();
            List<Entidad_Caja> list_Caja_Aper = new List<Entidad_Caja>();

            ent.Id_Empresa = Entidad.Id_Empresa;
            ent.Pdv_Codigo = Entidad.ctb_pdv_codigo;
            ent.Cja_Codigo = Entidad.Cja_Codigo;
            ent.Cja_Aper_Cierre_Codigo = Entidad.Cja_Aper_Cierre_Codigo;

            list_Caja_Aper = log.Listar_Caja_Aperturado(ent);

            //if (Entidad.Estado == "0002")
            //{
                    if (list_Caja_Aper.Count > 0)
                    {
                        if (list_Caja_Aper[0].Cja_Estado == "0058")//APERTURADO //SI A UN ESTA APERTURADO SERVIRA ELIMINAR
                        {

                            string DocDelted = "EL registro a ANULAR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
                            if (Accion.ShowDeleted(DocDelted))
                            {
                                try
                                {
                                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab
                                    {
                                        Id_Empresa = Entidad.Id_Empresa,
                                        Id_Anio = Entidad.Id_Anio,
                                        Id_Periodo = Entidad.Id_Periodo,
                                        Id_Voucher = Entidad.Id_Voucher,
                                        Id_Libro = Entidad.Id_Libro
                                    };

                                    Logica_Movimiento_Cab logi = new Logica_Movimiento_Cab();

                                    logi.Anular_Venta(Enti);
                                    Accion.ExitoGuardar();
                                    Listar();

                                }
                                catch (Exception ex)
                                {
                                    Accion.ErrorSistema(ex.Message);
                                }

                            }


                        }
                        else
                        {
                            Accion.Advertencia("NO PUEDE ANULAR ESTE REGISTRO,YA QUE LA CAJA FUE CERRADO");
                        }

                    }
                    else
                    {
                        Accion.Advertencia("ESTE REGISTRO NO SE ENCUENTRA EN UNA CAJA APERTURADA,NO SE PUEDE ELIMINAR");
                    }
            //}
            //else
            //{
            //    Accion.Advertencia("NO SE PUEDE REVERTIR,ESTE REGISTRO NO ESTA ANULADO");
            //}

        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
   

            Entidad_Caja ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();
            List<Entidad_Caja> list_Caja_Aper = new List<Entidad_Caja>();

            ent.Id_Empresa = Entidad.Id_Empresa;
            ent.Pdv_Codigo = Entidad.ctb_pdv_codigo;
            ent.Cja_Codigo = Entidad.Cja_Codigo;
            ent.Cja_Aper_Cierre_Codigo = Entidad.Cja_Aper_Cierre_Codigo;

            list_Caja_Aper = log.Listar_Caja_Aperturado(ent);

            if (Entidad.Estado_Fila == "ANULADO")
            {
                    if (list_Caja_Aper.Count > 0)
                    {
                        if (list_Caja_Aper[0].Cja_Estado == "0058")//APERTURADO //SI A UN ESTA APERTURADO SERVIRA ELIMINAR
                        {

                            string DocDelted = "EL registro a REVERTIR tiene la caraterisctica:" + Entidad.Id_Voucher + " " + Entidad.Nombre_Comprobante + " " + Entidad.Ctb_Serie + " " + Entidad.Ctb_Numero;
                            if (Accion.ShowDeleted(DocDelted))
                            {
                                try
                                {
                                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab
                                    {
                                        Id_Empresa = Entidad.Id_Empresa,
                                        Id_Anio = Entidad.Id_Anio,
                                        Id_Periodo = Entidad.Id_Periodo,
                                        Id_Voucher = Entidad.Id_Voucher,
                                        Id_Libro = Entidad.Id_Libro
                                    };

                                    Logica_Movimiento_Cab logi = new Logica_Movimiento_Cab();

                                    logi.Revertir(Enti);
                                    Accion.ExitoGuardar();
                                    Listar();

                                }
                                catch (Exception ex)
                                {
                                    Accion.ErrorSistema(ex.Message);
                                }

                            }


                        }
                        else
                        {
                            Accion.Advertencia("NO PUEDE REVERTIR ESTE REGISTRO,YA QUE LA CAJA FUE CERRADO");
                        }

                    }

            }
            else
            {
                Accion.Advertencia("SU ESTADO ACTUAL ESTA ACTIVO");
            }



        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TraerLibro();
            Listar();
        }

        private void btnVistaMovimiento_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_ventas_edicion_comercial_consulta f = new frm_ventas_edicion_comercial_consulta())
            {


                Estado = Estados.Modificar;
                f.Estado_Ven_Boton = "2";

                f.Id_Empresa = Entidad.Id_Empresa;
                f.Id_Anio = Entidad.Id_Anio;
                f.Id_Periodo = Entidad.Id_Periodo;
                f.Id_Libro = Entidad.Id_Libro;
                f.Voucher = Entidad.Id_Voucher;

                if (f.ShowDialog() == DialogResult.OK)
                {


                    try
                    {
                        Listar();
                    }
                    catch (Exception ex)
                    {
                        Accion.ErrorSistema(ex.Message);
                    }



                }
                else
                {
                    Listar();
                }
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\ConsultaVentasContado" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

 
    }
}
