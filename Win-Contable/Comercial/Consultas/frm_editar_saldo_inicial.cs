﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial.Consultas
{
    public partial class frm_editar_saldo_inicial : frm_fuente
    {
        public frm_editar_saldo_inicial()
        {
            InitializeComponent();
        }

        public string pdv, cja_codigo;

        public int Cja_Codigo_Apertura;
        public decimal Cja_Saldo_Inicial;

        private void frm_editar_saldo_inicial_Load(object sender, EventArgs e)
        {
            txtsaldoinicial.Text = Convert.ToString(Cja_Saldo_Inicial);
        }

        private void btnaceptar_Click(object sender, EventArgs e)
        {
            try
            {

                Entidad_Caja Ent = new Entidad_Caja();
                Logica_Caja log = new Logica_Caja();

                Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                Ent.Pdv_Codigo = pdv;
                Ent.Cja_Codigo = cja_codigo;
                Ent.Cja_Aper_Cierre_Codigo = Cja_Codigo_Apertura;
                Ent.Cja_Saldo_Inicial = Convert.ToDecimal(txtsaldoinicial.Text);

                if (log.Modificar_Saldo_Inicial(Ent))
                {
                    Accion.ExitoModificar();
                }


             }
            catch (Exception ex)
            {

            }
        }



    }
}
