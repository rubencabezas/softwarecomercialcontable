﻿using Contable;
using Contable.Comercial.Pos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial.Consultas
{
    public partial class frm_consulta_cajas : frm_fuente
    {
        public frm_consulta_cajas()
        {
            InitializeComponent();
        }

        private void frm_consulta_cajas_Load(object sender, EventArgs e)
        {
            Listar_todas_Cajas();
        }

        List<Entidad_Caja> lista_cajas = new List<Entidad_Caja>();
        void Listar_todas_Cajas()
        {

            try
            {

                    Logica_Caja Log = new Logica_Caja();
                    Entidad_Caja Ent = new Entidad_Caja();

                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    lista_cajas.Clear();
                    dgvdatos.DataSource = null;

                    lista_cajas = Log.Listado_de_Todas_las_Cajas(Ent);

                    if (lista_cajas.Count > 0)
                    {
                        dgvdatos.DataSource = lista_cajas;

                    }
                    else
                    {
                        Accion.Advertencia("No se encontro movimientos");
                    }
                
           }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Listar_todas_Cajas();
        }

        private void btnvistaprevia_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_vista_previa_caja f = new frm_vista_previa_caja())
            {
                f.pdv = Entidad.Pdv_Codigo;
                f.cja_codigo = Entidad.Cja_Codigo;
                f.Cja_Codigo_Apertura = Entidad.Cja_Aper_Cierre_Codigo;

                f.Nombre_Carpeta = "CIERRETOTAL";
                if (f.ShowDialog() == DialogResult.OK)
                {

                }
            }

        }

        Entidad_Caja Entidad = new Entidad_Caja();
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {//     if (ListaFiltrada.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                if (lista_cajas.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                   Entidad = lista_cajas[gridView1.GetFocusedDataSourceRowIndex()];
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void btnsaldoInicial_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (Entidad.Cja_Estado== "0058") // 0058 : abierto
            {

                using (frm_editar_saldo_inicial f = new frm_editar_saldo_inicial())
                {


                    f.pdv = Entidad.Pdv_Codigo;
                    f.cja_codigo = Entidad.Cja_Codigo;
                    f.Cja_Codigo_Apertura = Entidad.Cja_Aper_Cierre_Codigo;
                    f.Cja_Saldo_Inicial = Entidad.Cja_Saldo_Inicial;

                    if (f.ShowDialog() == DialogResult.OK)
                    {


                        try
                        {
                            Listar_todas_Cajas();
                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                        }



                    }
                    else
                    {
                        Listar_todas_Cajas();
                    }
                }
            }
            else
            {
                Accion.Advertencia("Esta caja ya se encuentra cerrado.");
            }




        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\Cajas" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                using (frm_ver_lista_egresos_ingresos f = new frm_ver_lista_egresos_ingresos())
                {

                    f.Pdv_Codigo = Entidad.Pdv_Codigo;
                    f.Cja_Codigo = Entidad.Cja_Codigo;
                    f.Cja_Aper_Cierre_Codigo = Entidad.Cja_Aper_Cierre_Codigo;

                    if (f.ShowDialog() == DialogResult.OK)
                    {

                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
