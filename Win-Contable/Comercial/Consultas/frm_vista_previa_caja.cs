﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial.Consultas
{
    public partial class frm_vista_previa_caja : frm_fuente
    {
        public frm_vista_previa_caja()
        {
            InitializeComponent();
        }

        public string pdv, cja_codigo;
        public int Cja_Codigo_Apertura;

        string Ruta = (Application.StartupPath + "\\DOCUMENTOS\\");
        public string Nombre_Carpeta = "";
        string BuscarArchivo;

        private void frm_vista_previa_caja_Load(object sender, EventArgs e)
        {
            string Datos = pdv + "-" + cja_codigo +"-" + Cja_Codigo_Apertura + ".pdf";
            string carpeta = (Ruta + Nombre_Carpeta + "\\");
            BuscarArchivo = UbicarArchivo(carpeta, Datos);

            if (!string.IsNullOrWhiteSpace(BuscarArchivo))
            {
                pdfViewer1.LoadDocument(carpeta + Datos);
            }
            else
            {
                Accion.Advertencia(" Es posible que no tenga el formato de vista previa");
                this.Close();
            }
        }

        public string UbicarArchivo(string Ruta, string Documento)
        {
            string ArchivoEncontrado = "";
            string[] files = System.IO.Directory.GetFiles(Ruta, Documento);
            foreach (string file in files)
            {
                ArchivoEncontrado = file;
            }
            return ArchivoEncontrado;
        }


    }
}
