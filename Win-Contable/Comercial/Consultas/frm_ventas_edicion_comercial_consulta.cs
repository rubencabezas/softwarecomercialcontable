﻿using Contable;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_ventas_edicion_comercial_consulta : frm_fuente
    {
        public frm_ventas_edicion_comercial_consulta()
        {
            InitializeComponent();
        }


        public string Estado_Ven_Boton;

        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;

         
        List<Entidad_Movimiento_Cab> Detalles_CONT = new List<Entidad_Movimiento_Cab>();


        List<Entidad_Movimiento_Cab> Detalles = new List<Entidad_Movimiento_Cab>();


        List<Entidad_Movimiento_Cab> Detalles_Orden_Compra = new List<Entidad_Movimiento_Cab>();
        List<Entidad_Movimiento_Cab> Detalles_ADM = new List<Entidad_Movimiento_Cab>();
       

        private void frm_ventas_edicion_Load(object sender, EventArgs e)
        {
            if(Estado_Ven_Boton == "2")
            {
                Estado = Estados.Modificar;
             
                ListarModificar();
        
            }
        }


        decimal MBase1 = 0, MValor_Fac_Export = 0;
        decimal MExonerada = 0, MInafecta, MIgv1 = 0;
        decimal MIsc = 0;
        decimal MOtrosTributos = 0, MImporteTotal = 0;

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public void ListarModificar()
        {
            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            //Estado = Estados.Ninguno;

            Ent.Id_Empresa = Id_Empresa;
            Ent.Id_Anio = Id_Anio;
            Ent.Id_Periodo = Id_Periodo;
            Ent.Id_Libro = Id_Libro;
            Ent.Id_Voucher = Voucher;
            try
            {
                Lista_Modificar = log.Listar_adm(Ent);
                if (Lista_Modificar.Count > 0)
                {
                    Entidad_Movimiento_Cab Enti = new Entidad_Movimiento_Cab();
                    Enti = Lista_Modificar[0];


                    Id_Empresa = Enti.Id_Empresa;
                    Id_Anio = Enti.Id_Anio;
                    Id_Periodo = Enti.Id_Periodo;
                    Id_Libro = Enti.Id_Libro;
                    Voucher = Enti.Id_Voucher;

                    txtglosa.Text = Enti.Ctb_Glosa;

                    txttipodoc.Text = Enti.Ctb_Tipo_Doc_Sunat;
                    txttipodoc.Tag = Enti.Ctb_Tipo_Doc;
                    txttipodocdesc.Text = Enti.Nombre_Comprobante;

                    //txtserie.Text = Enti.Ctb_Serie;
                    //txtnumero.Text = Enti.Ctb_Numero;

                    txtserie.Text = Enti.Ctb_Serie + "-" + Enti.Ctb_Numero;


                    //DateTime Fecha;
                    //Fecha = Convert.ToDateTime(Enti.Ctb_Fecha_Movimiento);
                    txtfechadoc.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Movimiento);//Convert.ToString(Fecha.ToString("dd/mm/yyyy"));
                    txtrucdni.Text = Enti.Ctb_Ruc_dni;
                    txtentidad.Text = Enti.Entidad;
                    txtcondicioncod.Tag = Enti.Ctb_Condicion_Cod;
                    txtcondicioncod.Text = Enti.Ctb_Condicion_Interno;
                    txtcondiciondesc.Text = Enti.Ctb_Condicion_Desc;

                    txtmonedacod.Text = Enti.Ctn_Moneda_Sunat;
                    txtmonedacod.Tag = Enti.Ctn_Moneda_Cod;
                    txtmonedadesc.Text = Enti.Ctn_Moneda_Desc;

                    txttipocambiocod.Text = Enti.Ctb_Tipo_Cambio_Cod;
                    txttipocambiodesc.Text = Enti.Ctb_Tipo_Cambio_desc;
                    txttipocambiovalor.Text = Convert.ToDecimal(Enti.Ctb_Tipo_Cambio_Valor).ToString();
 


                    MBase1 = Enti.Ctb_Base_Imponible;
                    MValor_Fac_Export = Enti.Ctb_Valor_Fact_Exonerada;
                    MIgv1 = Enti.Ctb_Igv;
                    MIsc = Enti.Ctb_Isc_Importe;
                    MOtrosTributos = Enti.Ctb_Otros_Tributos_Importe;
                    MExonerada = Enti.Ctb_Exonerada;
                    MInafecta = Enti.Ctb_Inafecta;
                    MImporteTotal = Enti.Ctb_Importe_Total;

                    //  { 0:00.00}         ", value)
                    txtbaseimponible.Text = Convert.ToDecimal(MBase1).ToString("0.00");
                    txtvalorfactexport.Text = Convert.ToDecimal(MValor_Fac_Export).ToString("0.00");
                    txtigv.Text = Convert.ToDecimal(MIgv1).ToString("0.00");
                    txtimporteisc.Text = Convert.ToDecimal(MIsc).ToString("0.00");
                    txtotrostribuimporte.Text = Convert.ToDecimal(MOtrosTributos).ToString("0.00");
                    txtexonerada.Text = Convert.ToDecimal(MExonerada).ToString("0.00");
                    txtinafecta.Text = Convert.ToDecimal(MInafecta).ToString("0.00");
                    txtimportetotal.Text = Convert.ToDecimal(MImporteTotal).ToString("0.00");


 

                    txtidanalisis.Text = Enti.Ctb_Analisis;
                    txtanalisisdesc.Text = Enti.Ctb_Analisis_Desc;

                    if (String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/0001" || String.Format("{0:dd/MM/yyyy}", Enti.Ctb_Fecha_Vencimiento) == "01/01/1900")
                    {
                        txtfechavencimiento.Text = "";
                    }
                    else
                    {
                        txtfechavencimiento.Text = String.Format("{0:yyyy-MM-dd}", Enti.Ctb_Fecha_Vencimiento);
                    }


                    //listar cnotable
                    Logica_Movimiento_Cab log_det_con = new Logica_Movimiento_Cab();
                    Dgvdetalles.DataSource = null;
                    Detalles_CONT = log_det_con.Listar_Det_Administrativo(Enti);

                    if (Detalles_CONT.Count > 0)
                    {

                        Dgvdetalles.DataSource = Detalles_CONT;

                    }

 

                }
            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }



        }


  

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
