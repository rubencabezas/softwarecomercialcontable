﻿using Contable;
using Contable._1_Busquedas_Generales;
using DevExpress.XtraPrinting.Preview;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_consulta_stock_kardex : frm_fuente
    {
        public frm_consulta_stock_kardex()
        {
            InitializeComponent();
        }

        private void txtalmacencod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtalmacencod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtalmacencod.Text.Substring(txtalmacencod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_almacen_busqueda f = new frm_almacen_busqueda())
                        {


                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Almacen Entidad = new Entidad_Almacen();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];

                                txtalmacencod.Text = Entidad.Id_Almacen.Trim();
                                txtalmacendesc.Text = Entidad.Alm_Descrcipcion.Trim();
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtalmacencod.Text) & string.IsNullOrEmpty(txtalmacendesc.Text))
                    {
                        BuscarAlmacen();
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarAlmacen()
        {
            try
            {
                txtalmacencod.Text = Accion.Formato(txtalmacencod.Text, 2);

                Logica_Almacen log = new Logica_Almacen();

                List<Entidad_Almacen> Generales = new List<Entidad_Almacen>();

                Generales = log.Listar(new Entidad_Almacen
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Almacen = txtalmacencod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Almacen T in Generales)
                    {
                        if ((T.Id_Almacen).ToString().Trim().ToUpper() == txtalmacencod.Text.Trim().ToUpper())
                        {
                            txtalmacencod.Text = (T.Id_Almacen).ToString().Trim();
                            txtalmacendesc.Text = T.Alm_Descrcipcion.Trim();

                        }
                    }

                }
                else
                {
                    Accion.Advertencia("No se encontro ningun dato");
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtalmacencod_TextChanged(object sender, EventArgs e)
        {
            if (txtalmacencod.Focus() == false)
            {
                txtalmacendesc.ResetText();
            }
        }

        private void txtproductocod_KeyDown(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(txtproductocod.Text) == false)
            {
                try
                {
                    if (e.KeyCode == Keys.Enter & txtproductocod.Text.Substring(txtproductocod.Text.Length - 1, 1) == "*")
                    {
                        using (frm_catalogo_busqueda f = new frm_catalogo_busqueda())
                        {
                            f.Tipo = "0031";

                            if (f.ShowDialog(this) == DialogResult.OK)
                            {
                                Entidad_Catalogo Entidad = new Entidad_Catalogo();

                                Entidad = f.Lista[f.gridView1.GetFocusedDataSourceRowIndex()];


                                txtproductocod.Text = Entidad.Id_Catalogo;
                                txtproductodesc.Text = Entidad.Cat_Descripcion;

                          

                                txtproductocod.EnterMoveNextControl = true;
 
                            }
                        }
                    }
                    else
                        if (e.KeyCode == Keys.Enter & !string.IsNullOrEmpty(txtproductocod.Text) & string.IsNullOrEmpty(txtproductodesc.Text))
                    {
                        BuscarCatalogo();
 
                    }
                }
                catch (Exception ex)
                {
                    Accion.ErrorSistema(ex.Message);
                }
            }
        }

        public void BuscarCatalogo()
        {
            try
            {
                txtproductocod.Text = Accion.Formato(txtproductocod.Text, 10);

                Logica_Catalogo log = new Logica_Catalogo();

                List<Entidad_Catalogo> Generales = new List<Entidad_Catalogo>();
                Generales = log.Listar(new Entidad_Catalogo
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Id_Tipo = "0031",
                    Id_Catalogo = txtproductocod.Text
                });

                if (Generales.Count > 0)
                {

                    foreach (Entidad_Catalogo T in Generales)
                    {
                        if ((T.Id_Catalogo).ToString().Trim().ToUpper() == txtproductocod.Text.Trim().ToUpper())
                        {
                            txtproductocod.Text = (T.Id_Catalogo).ToString().Trim();
                            txtproductodesc.Text = T.Cat_Descripcion;

           

                            txtproductocod.EnterMoveNextControl = true;
                        }
                    }
                }
                else
                {
                    Accion.Advertencia("No se encontro ningun elemento con este codigo");
                    txtproductocod.EnterMoveNextControl = false;
                    txtproductocod.ResetText();
                    txtproductocod.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        List<Entidad_Movimiento_Inventario> ListCta = new List<Entidad_Movimiento_Inventario>();
        List<Entidad_Movimiento_Inventario> ListCtaResumen = new List<Entidad_Movimiento_Inventario>();

        public bool ValidaPrimerReporte()
        {

            if (string.IsNullOrEmpty(txtalmacendesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un almacen");
                txtalmacencod.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtfechainicio.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha de inicio");
                txtfechainicio.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtfechafin.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar una fecha de fin");
                txtfechafin.Focus();
                return false;
            }

            return true;
        }

        public bool ValidaTercerReporte()
        {

            if (string.IsNullOrEmpty(txtalmacendesc.Text.Trim()))
            {
                Accion.Advertencia("Debe ingresar un almacen");
                txtalmacencod.Focus();
                return false;
            }
 

            return true;
        }


        private void btnvistaprevia_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
         
                    try {

                        if (ValidaPrimerReporte())
                             {

                                    Logica_Movimiento_Inventario Log = new Logica_Movimiento_Inventario();
                                    Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();

                                    Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                                    Ent.Id_Anio = Actual_Conexion.AnioSelect;
                                    Ent.Id_Almacen = txtalmacencod.Text.Trim();
                                    Ent.Invd_Catalogo = txtproductocod.Text.Trim();
                                    Ent.Fecha_inicio = Convert.ToDateTime(txtfechainicio.Text.Trim());
                                    Ent.Fecha_Fin = Convert.ToDateTime(txtfechafin.Text.Trim());



                                    ListCta = Log.Reporte_Kardex(Ent);

                                    if (ListCta.Count > 0)
                                    {
                                       Reporte_Inventario_Kardex f = new Reporte_Inventario_Kardex();

                                        f.lblperiodo.Text = Actual_Conexion.AnioSelect;
                                        f.lblruc.Text = Actual_Conexion.RucEmpresa;
                                        f.lblrazonsocial.Text = Actual_Conexion.EmpresaNombre;
                                        f.DataSource = ListCta;
                                        //f.PaperKind = PaperKind.A4;
                                        f.Landscape = true;

                                        dynamic ribbonPreview = new PrintPreviewFormEx();
                                        f.CreateDocument();
                                        f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                                        ribbonPreview.PrintingSystem = f.PrintingSystem;
                                        // ribbonPreview.MdiParent = frm_principal;
                                        ribbonPreview.Show();

                                    }
                                    else
                                    {
                                        Accion.Advertencia("No se encontro movimientos");
                                    }
                        }



                    }

                    catch (Exception ex)
                    {
                       Accion.ErrorSistema(ex.Message);
                    }
            
            

        }

        private void btnresumen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                if (ValidaPrimerReporte())
                {

                        Logica_Movimiento_Inventario Log = new Logica_Movimiento_Inventario();
                        Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();

                        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        Ent.Id_Anio = Actual_Conexion.AnioSelect;
                        Ent.Id_Almacen = txtalmacencod.Text.Trim();
                        Ent.Invd_Catalogo = txtproductocod.Text.Trim();
                        Ent.Fecha_inicio = Convert.ToDateTime(txtfechainicio.Text.Trim());
                        Ent.Fecha_Fin = Convert.ToDateTime(txtfechafin.Text.Trim());



                        ListCtaResumen = Log.Reporte_Kardex_Resumen(Ent);

                        if (ListCtaResumen.Count > 0)
                        {
                            Reporte_Inventario_Kardex_Resumen f = new Reporte_Inventario_Kardex_Resumen();

                            f.lblperiodo.Text = Actual_Conexion.AnioSelect;
                            f.lblruc.Text = Actual_Conexion.RucEmpresa;
                            f.lblrazonsocial.Text = Actual_Conexion.EmpresaNombre;
                            f.DataSource = ListCtaResumen;
                            f.PaperKind = PaperKind.A4;
                            f.Landscape = false;

                            dynamic ribbonPreview = new PrintPreviewFormEx();
                            f.CreateDocument();
                            f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                            ribbonPreview.PrintingSystem = f.PrintingSystem;
                            // ribbonPreview.MdiParent = frm_principal;
                            ribbonPreview.Show();

                        }
                        else
                        {
                            Accion.Advertencia("No se encontro movimientos");
                        }
                }



            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                if (ValidaTercerReporte())
                {
                        Logica_Movimiento_Inventario Log = new Logica_Movimiento_Inventario();
                        Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();

                        Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                        Ent.Id_Anio = Actual_Conexion.AnioSelect;
                        Ent.Id_Almacen = txtalmacencod.Text.Trim();
                        Ent.Invd_Catalogo = txtproductocod.Text.Trim();
                      //  Ent.Fecha_inicio = Convert.ToDateTime(txtfechainicio.Text.Trim());
                      //  Ent.Fecha_Fin = Convert.ToDateTime(txtfechafin.Text.Trim());



                        ListCtaResumen = Log.Reporte_Kardex_Lote_Resumen(Ent);

                        if (ListCtaResumen.Count > 0)
                        {
                            Reporte_Inventario_Kardex_Lote f = new Reporte_Inventario_Kardex_Lote();

                            f.lblperiodo.Text = Actual_Conexion.AnioSelect;
                            f.lblruc.Text = Actual_Conexion.RucEmpresa;
                            f.lblrazonsocial.Text = Actual_Conexion.EmpresaNombre;
                            f.DataSource = ListCtaResumen;
                            f.PaperKind = PaperKind.A4;
                            f.Landscape = false;

                            dynamic ribbonPreview = new PrintPreviewFormEx();
                            f.CreateDocument();
                            f.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                            ribbonPreview.PrintingSystem = f.PrintingSystem;
                            // ribbonPreview.MdiParent = frm_principal;
                            ribbonPreview.Show();

                        }
                        else
                        {
                            Accion.Advertencia("No se encontro movimientos");
                        }
                }



            }

            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void txtproductocod_TextChanged(object sender, EventArgs e)
        {
            if (txtproductocod.Focus() == false)
            {
                txtproductodesc.ResetText();
            }
        }
    }
}
