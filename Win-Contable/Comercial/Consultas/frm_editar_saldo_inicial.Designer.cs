﻿namespace Comercial.Consultas
{
    partial class frm_editar_saldo_inicial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtsaldoinicial = new DevExpress.XtraEditors.TextEdit();
            this.btnaceptar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtsaldoinicial.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 39);
            this.label2.TabIndex = 4;
            this.label2.Text = "Saldo Inicial";
            // 
            // txtsaldoinicial
            // 
            this.txtsaldoinicial.Location = new System.Drawing.Point(193, 12);
            this.txtsaldoinicial.Name = "txtsaldoinicial";
            this.txtsaldoinicial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsaldoinicial.Properties.Appearance.Options.UseFont = true;
            this.txtsaldoinicial.Properties.Mask.EditMask = "n2";
            this.txtsaldoinicial.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtsaldoinicial.Size = new System.Drawing.Size(226, 42);
            this.txtsaldoinicial.TabIndex = 3;
            // 
            // btnaceptar
            // 
            this.btnaceptar.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaceptar.Appearance.Options.UseFont = true;
            this.btnaceptar.Appearance.Options.UseTextOptions = true;
            this.btnaceptar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnaceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnaceptar.Location = new System.Drawing.Point(284, 60);
            this.btnaceptar.Name = "btnaceptar";
            this.btnaceptar.Size = new System.Drawing.Size(135, 51);
            this.btnaceptar.TabIndex = 164;
            this.btnaceptar.Text = "Aceptar";
            this.btnaceptar.Click += new System.EventHandler(this.btnaceptar_Click);
            // 
            // frm_editar_saldo_inicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 122);
            this.Controls.Add(this.btnaceptar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtsaldoinicial);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_editar_saldo_inicial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edicion de saldo inicial";
            this.Load += new System.EventHandler(this.frm_editar_saldo_inicial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtsaldoinicial.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit txtsaldoinicial;
        internal DevExpress.XtraEditors.SimpleButton btnaceptar;
    }
}