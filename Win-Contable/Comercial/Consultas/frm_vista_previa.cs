﻿using Contable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_vista_previa : frm_fuente
    {
        public frm_vista_previa()
        {
            InitializeComponent();
        }

        public string Serie, Numero;
        string Ruta = (Application.StartupPath + "\\DOCUMENTOS\\");
       public string Nombre_Carpeta = "";
        string BuscarArchivo;
        string Buffer;


        public string Id_Empresa, Id_Anio, Id_Periodo, Id_Libro, Voucher;
        int NumCaracteresFila = 42;
        string DireccionDescripcionPuntoVenta, Numero_maquina_registradora;
        string Codigo_Autorizacion_Sunat, NombreImpresora;
        string Igv_TasaImpresion;
        //bool EsEpson = false;

        public Boolean VAR_BOLETA;
        public Boolean VAR_FACTURA;
        public Boolean VAR_NOTA_SALIDA;

        public List<Entidad_Movimiento_Cab> Lista_Modificar = new List<Entidad_Movimiento_Cab>();
        public List<Entidad_Movimiento_Cab> Detalles_ADM = new List<Entidad_Movimiento_Cab>();
   
        public string TextoCentro(string StrC)
        {
            string StrPrint = "";
            string VNext = "";
            if ((StrC.Length > NumCaracteresFila))
            {
                string[] TxtWords = StrC.Split(Convert.ToChar(" "));
                string CurrentSub = "";
                string AfterSub = "";
                bool FoundLength = false;
                foreach (string Txt in TxtWords)
                {
                    if ((((CurrentSub + (" " + Txt)).Length <= NumCaracteresFila) && (FoundLength == false)))
                    {
                        CurrentSub = (CurrentSub + (" " + Txt));
                    }
                    else
                    {
                        FoundLength = true;
                        AfterSub = (AfterSub + (" " + Txt));
                    }

                }

                StrPrint = CurrentSub;
                VNext = TextoCentro(AfterSub);
            }
            else
            {
                StrPrint = StrC;
            }

            if ((StrPrint.Trim().Length == NumCaracteresFila))
            {
                return (StrPrint + ("\n" + VNext));
            }
            else
            {
                int espc = ((NumCaracteresFila - StrPrint.Trim().Length) / 2);
                StrPrint = (new string(' ', espc) + StrPrint.Trim());
                // & StrDup(espc, " ")
                return (StrPrint + ("\n" + VNext));
            }

        }


        string FormatearLimite(string StrF)
        {
            string StrPrint = "";
            string VNext = "";
            if ((StrF.Length > NumCaracteresFila))
            {
                string[] TxtWords = StrF.Split(Convert.ToChar(" "));
                string CurrentSub = "";
                string AfterSub = "";
                bool FoundLength = false;
                foreach (string Txt in TxtWords)
                {
                    if ((((CurrentSub + (" " + Txt)).Length <= NumCaracteresFila) && (FoundLength == false)))
                    {
                        CurrentSub = (CurrentSub + (" " + Txt)).Trim();
                    }
                    else
                    {
                        FoundLength = true;
                        AfterSub = (AfterSub + (" " + Txt));
                    }

                }

                StrPrint = CurrentSub;
                VNext = FormatearLimite(AfterSub.Trim());
            }
            else
            {
                StrPrint = StrF.Trim();
            }

            return (StrPrint + ("\n" + VNext));
        }




        public string AgregaDetalle(string cant, string par1, string precio, string total)
        {
            string StrPrint = "";
            string DescripLarga = "";
            StrPrint = ("        ".Substring(0, (8 - cant.ToString().Length)) + (cant.ToString() + " "));
            if ((par1.Trim().Length > 17))
            {
                // par1 = par1.Remove(0, 17)
                StrPrint = (StrPrint + (par1.Substring(0, 17) + " "));
                DescripLarga = DescripcionLarga(par1.Substring(17));
            }
            else
            {
                StrPrint = (StrPrint + par1.PadRight(18));
            }

            // ----Prec. Unit.
            StrPrint = (StrPrint + (precio.PadLeft(7) + " "));
            if ((NumCaracteresFila == 42))
            {
                StrPrint = (StrPrint + (total.PadLeft(7) + "\n"));
                // (StrDup(7, "-") & total).Substring((StrDup(7, "-") & total).Length - (total.Length)) '& " "
            }
            else
            {
                StrPrint = (StrPrint + (total.PadLeft(5) + "\n"));
            }

            if (!string.IsNullOrWhiteSpace(DescripLarga))
            {
                StrPrint = (StrPrint + DescripLarga);
            }

            return StrPrint;
        }

        string DescripcionLarga(string strr)
        {
            string strRestante = new string(' ', 9);
            if ((strr.Length > 17))
            {
                string StrNext = strr.Substring(17);
                return (strRestante + (strr.Substring(0, 17) + ("\n" + DescripcionLarga(StrNext))));
            }
            else
            {
                return (strRestante + (strr + "\n"));
            }

        }

        public List<Entidad_Entidad> Entidad_Direccion = new List<Entidad_Entidad>();
        public void Entidad_Direcion()
        {
            Entidad_Entidad Ent = new Entidad_Entidad();
            Logica_Entidad log = new Logica_Entidad();

            try
            {
                Entidad_Direccion = log.Listar(Ent);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Direccion_Punto_Venta()
        {
        

            try
            {
                Entidad_Punto_Venta ent = new Entidad_Punto_Venta();
                Logica_Punto_Venta log = new Logica_Punto_Venta();

                List<Entidad_Punto_Venta> Punto_Venta_PC = new List<Entidad_Punto_Venta>();

                Punto_Venta_PC = log.Buscar_Pdv_por_PC(new Entidad_Punto_Venta
                {
                    Id_Empresa = Actual_Conexion.CodigoEmpresa,
                    Pdv_Nombre_PC = Actual_Conexion.Maquina
                });

                DireccionDescripcionPuntoVenta = Punto_Venta_PC[0].Est_Direccion;
                Numero_maquina_registradora = Punto_Venta_PC[0].Pdv_Num_Maquina_Registradora;
                NombreImpresora = Punto_Venta_PC[0].Pdv_Impresora;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void frm_vista_previa_Load(object sender, EventArgs e)
        {
            
            string Datos = Serie + "-" + Numero + ".pdf" ;
            string carpeta = (Ruta + Nombre_Carpeta+"\\");
            BuscarArchivo = UbicarArchivo(carpeta, Datos );

            if (!string.IsNullOrWhiteSpace(BuscarArchivo))
            {
                pdfViewer1.LoadDocument(carpeta+Datos);
            }
            else
            {
                Accion.Advertencia(" SERIE:" + Serie + "\n" + " NUMERO:" + Numero + "\n" + " es posible que no tenga el formato de vista previa");
                this.Close();
            }

        }

        public string UbicarArchivo(string Ruta, string Documento)
        {
            string ArchivoEncontrado = "";
            string[] files = System.IO.Directory.GetFiles(Ruta, Documento);
            foreach (string file in files)
            {
                ArchivoEncontrado = file;
            }
            return ArchivoEncontrado;
        }

        public string Copia()
        {
            string Txt;
            Txt = (new string('*', 40) + ("\r\n" + ("*" + (new string(' ', 17) + ("COPIA" + (new string(' ', 16) + ("*" + ("\r\n" + (new string('*', 40) + "\r\n")))))))));
            return Txt;
        }

        void AutorizacionSUNAT(Entidad_Movimiento_Cab ent)
        {
            Logica_Serie_Comprobante LogSerieNum = new Logica_Serie_Comprobante();
            List<Entidad_Serie_Comprobante> SerieNumeros = new List<Entidad_Serie_Comprobante>();

            SerieNumeros = LogSerieNum.Listar(new Entidad_Serie_Comprobante
            {
                Id_Empresa = Actual_Conexion.CodigoEmpresa,
                Ser_Tipo_Doc = ent.Ctb_Tipo_Doc,
                Ser_Pdv_Codigo = ent.ctb_pdv_codigo
            });
            Codigo_Autorizacion_Sunat = SerieNumeros[0].Ser_Cod_Autorizacion_SUNAT;
        }
        void ActualizaIGV(Entidad_Movimiento_Cab ent)
        {
            try
            {
                if ((IsDate(Convert.ToString(ent.Ctb_Fecha_Movimiento)) == true))
                {
                    if ((DateTime.Parse(Convert.ToString(ent.Ctb_Fecha_Movimiento)).Year > 2000))
                    {
                        Entidad_Impuesto_Det enti = new Entidad_Impuesto_Det();
                        Logica_Impuesto_Det log = new Logica_Impuesto_Det();
                        enti.Imd_Fecha_Inicio = Convert.ToDateTime(Convert.ToString(ent.Ctb_Fecha_Movimiento));

                        List<Entidad_Impuesto_Det> Lista = new List<Entidad_Impuesto_Det>();
                        Lista = log.Igv_Actual(enti);
                        if ((Lista.Count > 0))
                        {
                            //Igv_Porcentaje = Lista[0].Imd_Tasa;
                            //Igv_Tasa_texto = Convert.ToString(Math.Round(Lista[0].Imd_Tasa * 100, 2));
                            Igv_TasaImpresion = Lista[0].Imd_Tasa_Impresion;
                            //LblIGV.Text = "I.G.V." + "(" + Math.Round(Lista[0].Imd_Tasa * 100, 2) + "):";
                            //LblIGV.Tag = Lista[0].Imd_Tasa;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        public static bool IsDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        List<Entidad_Documentos_Punto_Venta> DocumentosXPuntoVenta = new List<Entidad_Documentos_Punto_Venta>();
        List<Entidad_Documentos_Punto_Venta> DocumentosXModVenta = new List<Entidad_Documentos_Punto_Venta>();
        void Buscar_Comprabante_Punto_Venta(Entidad_Movimiento_Cab Ent)
        {


            try
            {
                DocumentosXPuntoVenta.Clear();
                if (((Ent.ctb_pdv_Nombre != "")))
                {
                    Entidad_Documentos_Punto_Venta Documento = new Entidad_Documentos_Punto_Venta();
                    Logica_Documentos_Punto_Venta Log_Documento = new Logica_Documentos_Punto_Venta();

                    Documento.Id_Empresa = Actual_Conexion.CodigoEmpresa;
                    //Documento.Est_Codigo = BSIEstablecimiento.Tag.ToString();
                    Documento.Pdv_Codigo = Ent.ctb_pdv_codigo;

                    DocumentosXPuntoVenta = Log_Documento.Listar(Documento);

                    DocumentosXModVenta.Clear();
                    if (DocumentosXPuntoVenta.Count > 0)
                    {


                        foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXPuntoVenta)
                        {
                            if (Ent_Doc.Doc_Es_Nota_Salida)
                            {
                             
                                DocumentosXModVenta.Add(Ent_Doc);
                                break;
                            }

                        }

                        if (DocumentosXModVenta.Count == 0)
                        {
                            Entidad_Documentos_Punto_Venta Ent_ = new Entidad_Documentos_Punto_Venta();
                            Ent_.Doc_Es_Nota_Salida = false;
                            DocumentosXModVenta.Add(Ent_);
                        }

                        foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXPuntoVenta)
                        {
                            if (Ent_Doc.Doc_Es_Boleta)
                            {
                               
                                DocumentosXModVenta.Add(Ent_Doc);
                                break;
                            }

                        }

                        if (DocumentosXModVenta.Count == 1)
                        {
                            Entidad_Documentos_Punto_Venta Ent_ = new Entidad_Documentos_Punto_Venta();
                            Ent_.Doc_Es_Boleta = false;
                            DocumentosXModVenta.Add(Ent_);
                        }



                        foreach (Entidad_Documentos_Punto_Venta Ent_Doc in DocumentosXPuntoVenta)
                        {
                            if (Ent_Doc.Doc_Es_Factura)
                            {
                               
                                DocumentosXModVenta.Add(Ent_Doc);
                                break;
                            }

                        }

                        if (DocumentosXModVenta.Count == 2)
                        {
                            Entidad_Documentos_Punto_Venta Ent_ = new Entidad_Documentos_Punto_Venta();
                            Ent_.Doc_Es_Factura = false;
                            DocumentosXModVenta.Add(Ent_);
                        }

                        VAR_BOLETA = DocumentosXModVenta[0].Doc_Es_Boleta;
                        VAR_FACTURA = DocumentosXModVenta[0].Doc_Es_Factura;
                        VAR_NOTA_SALIDA = DocumentosXModVenta[0].Doc_Es_Nota_Salida;

                    }
                    else
                    {
                        //Accion.Advertencia("FALTA CONFIGURAR LOS COMPROBANTES DE VENTA PARA ESTE PUNTO DE VENTA " + BSIPuntoVenta.Caption);
                    
                    }

                }

            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

    }
}
