﻿using Contable;
using Contable._2_Alertas;
using Contable.Comercial.Operaciones;
using DevExpress.Compression;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Comercial
{
    public partial class frm_consulta_ventas_sin_ticket : frm_fuente
    {
        public frm_consulta_ventas_sin_ticket()
        {
            InitializeComponent();
        }

        Entidad_Movimiento_Inventario Entidad = new Entidad_Movimiento_Inventario();
        public List<Entidad_Movimiento_Inventario> Lista = new List<Entidad_Movimiento_Inventario>();
        public void Listar()
        {
            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario();
            Logica_Movimiento_Inventario log = new Logica_Movimiento_Inventario();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
            Ent.Id_Anio = Actual_Conexion.AnioSelect;
            Ent.Id_Periodo = null;
            Ent.Id_Tipo_Mov = "0035";//SALIDAS PERO CON EL TIPO DE ES_VENTAS SIN TICKET NO MA
            Ent.Id_Almacen = null;
            Ent.Id_Movimiento = null;
            Ent.Inv_Es_Venta_Sin_Ticket = true;
            try
            {
                Lista = log.Listar_Ventas_Sin_Ticket(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
        private void frm_consulta_ventas_sin_ticket_Load(object sender, EventArgs e)
        {
            dgvdatos.Dock = DockStyle.Fill;
            // dgvdatos.Anchor = (AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top);
            Listar();
        }

        private void btnnuevo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frm_vista_previa f = new frm_vista_previa())
            {
                f.Serie = Entidad.Inv_Serie;
                f.Numero = Entidad.Inv_Numero;
                f.Nombre_Carpeta = "VENTAS SIN COMPROBANTE";
                if (f.ShowDialog() == DialogResult.OK)
                {

                }
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Entidad_Caja ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();
            List<Entidad_Caja> list_Caja_Aper = new List<Entidad_Caja>();

            ent.Id_Empresa = Entidad.Id_Empresa;
            ent.Pdv_Codigo = Entidad.Inv_pdv_codigo;
            ent.Cja_Codigo = Entidad.Cja_Codigo;
            ent.Cja_Aper_Cierre_Codigo = Entidad.Cja_Aper_Cierre_Codigo;

            list_Caja_Aper = log.Listar_Caja_Aperturado(ent);

            //if (Entidad.Estado == "0002")
            //{
            if (list_Caja_Aper.Count > 0)
            {
                if (list_Caja_Aper[0].Cja_Estado == "0058")//APERTURADO //SI A UN ESTA APERTURADO SERVIRA ELIMINAR
                {

                    string DocDelted = "EL registro a ANULAR tiene la caraterisctica:" + Entidad.Id_Almacen_Desc + " " + Entidad.Id_Tipo_Mov_Desc + " " + Entidad.Id_Movimiento;
                    if (Accion.ShowDeleted(DocDelted))
                    {
                        try
                        {
                            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario
                            {
                                Id_Empresa = Entidad.Id_Empresa,
                                Id_Anio = Entidad.Id_Anio,
                                Id_Periodo = Entidad.Id_Periodo,
                                Id_Tipo_Mov = Entidad.Id_Tipo_Mov,
                                Id_Almacen = Entidad.Id_Almacen,
                                Id_Movimiento = Entidad.Id_Movimiento
                            };

                            Logica_Movimiento_Inventario logi = new Logica_Movimiento_Inventario();

                            logi.Anular_Adm_Ingresos_Inv(Ent);
                            Accion.ExitoGuardar();
                            Listar();

                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                        }

                    }


                }
                else
                {
                    Accion.Advertencia("NO PUEDE ANULAR ESTE REGISTRO,YA QUE LA CAJA FUE CERRADO");
                }

            }
            else
            {
                Accion.Advertencia("ESTE REGISTRO NO SE ENCUENTRA EN UNA CAJA APERTURADA,NO SE PUEDE ELIMINAR");
            }

        }

        private void btnmodificar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            Entidad_Caja ent = new Entidad_Caja();
            Logica_Caja log = new Logica_Caja();
            List<Entidad_Caja> list_Caja_Aper = new List<Entidad_Caja>();

            ent.Id_Empresa = Entidad.Id_Empresa;
            ent.Pdv_Codigo = Entidad.Inv_pdv_codigo;
            ent.Cja_Codigo = Entidad.Cja_Codigo;
            ent.Cja_Aper_Cierre_Codigo = Entidad.Cja_Aper_Cierre_Codigo;

            list_Caja_Aper = log.Listar_Caja_Aperturado(ent);

            if (list_Caja_Aper.Count > 0)
            {
                if (list_Caja_Aper[0].Cja_Estado == "0058")//APERTURADO //SI A UN ESTA APERTURADO SERVIRA ELIMINAR
                {
                    string DocDelted = "EL registro a ELIMINAR tiene la caraterisctica:" + Entidad.Id_Almacen_Desc + " " + Entidad.Id_Tipo_Mov_Desc + " " + Entidad.Id_Movimiento;
                    if (Accion.ShowDeleted(DocDelted))
                    {
                        try
                        {
                            Entidad_Movimiento_Inventario Ent = new Entidad_Movimiento_Inventario
                            {
                                Id_Empresa = Entidad.Id_Empresa,
                                Id_Anio = Entidad.Id_Anio,
                                Id_Periodo = Entidad.Id_Periodo,
                                Id_Tipo_Mov = Entidad.Id_Tipo_Mov,
                                Id_Almacen = Entidad.Id_Almacen,
                                Id_Movimiento = Entidad.Id_Movimiento
                            };

                            Logica_Movimiento_Inventario logi = new Logica_Movimiento_Inventario();

                            logi.Eliminar_Adm_Ingresos_Inv(Ent);
                            Accion.ExitoGuardar();
                            Listar();

                        }
                        catch (Exception ex)
                        {
                            Accion.ErrorSistema(ex.Message);
                        }

                    }
                }
                else
                {
                    Accion.Advertencia("YA NO PUEDE ELIMINAR ESTE REGISTRO,YA QUE LA CAJA FUE CERRADO");
                }

            }
            else
            {
                Accion.Advertencia("ESTE REGISTRO NO SE ENCUENTRA EN UNA CAJA APERTURADA,NO SE PUEDE ELIMINAR");
            }
        }

        List<Entidad_Movimiento_Inventario> Detalles = new List<Entidad_Movimiento_Inventario>();
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (Lista.Count > 0 & gridView1.GetFocusedDataSourceRowIndex() > -1)
                {
                    Estado = Estados.Ninguno;

                    Entidad = Lista[gridView1.GetFocusedDataSourceRowIndex()];


                    Logica_Movimiento_Inventario log_det = new Logica_Movimiento_Inventario();
                    dgvdetalles.DataSource = null;
                    Detalles = log_det.Listar_Det(Entidad);

                    if (Detalles.Count > 0)
                    {
                        dgvdetalles.DataSource = Detalles;
                    }

                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //TraerLibro();
            Listar();
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\ConsultaVentasSinTicket" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }

        private void BtnVerDetalle_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (BtnVerDetalle.Checked == true)
            {
                dgvdatos.Dock = DockStyle.None;
                //dgvdatos.Anchor = (AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top );
                dgvdatos.Dock = DockStyle.Left;
             
                dgvdetalles.Visible = true;
            }
            else
            {
                dgvdatos.Dock = DockStyle.Fill;
                dgvdetalles.Visible = false;
            }
        }

        private void btnexportardatos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (frm_rango_exportar_compras_comercial ff = new frm_rango_exportar_compras_comercial())
                {

                    if (ff.ShowDialog() == DialogResult.OK)
                    {


                        System.Threading.Thread p = new System.Threading.Thread((this.Progreso));
                        try
                        {

                            p.Start();


                            Logica_Exportar_Importar log = new Logica_Exportar_Importar();
                            Entidad_Exportar entidad = new Entidad_Exportar();
                            entidad.Empresa = Actual_Conexion.CodigoEmpresa;
                            entidad.desde = Convert.ToDateTime(ff.txtdesde.Text);
                            entidad.hasta = Convert.ToDateTime(ff.txthasta.Text);

                            //CATALOGO
                            DataSet GNL_ENTIDAD = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_CAB = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_DET = new DataSet();
                            DataSet CTB_MOVIMIENTO_INVENTARIO_LOTE = new DataSet();
 

                            GNL_ENTIDAD = log.EXPORTAR_GNL_ENTIDAD_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_CAB = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBO_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_DET = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(entidad);
                            CTB_MOVIMIENTO_INVENTARIO_LOTE = log.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(entidad);
 

                            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                            string Nombre = "VENTAS_AMBOS";// + EstCod.ToUpper.Trim + " " + FechaServer();
                            string Ruta;
                            Ruta = desktopPath + @"\" + Nombre;

                            File.Delete(Ruta + ".rar");
                            if (!Directory.Exists(Ruta))
                            {
                                Directory.CreateDirectory(Ruta);
                            }
                            else
                            {
                                foreach (var f in Directory.GetFiles(Ruta))
                                    File.Delete(Conversions.ToString(f));
                            }

                            string gen_entidad = "GNL_ENTIDAD";
                            string inventario_cab = "CTB_MOVIMIENTO_INVENTARIO_CAB";
                            string inventario_det = "CTB_MOVIMIENTO_INVENTARIO_DET";
                            string inventario_lote = "CTB_MOVIMIENTO_INVENTARIO_LOTE";


                            File.WriteAllText(Ruta + @"\" + gen_entidad + ".csv", ConvertToCSV(GNL_ENTIDAD.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_cab + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_CAB.Tables[0]));
                           File.WriteAllText(Ruta + @"\" + inventario_det + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_DET.Tables[0]));
                            File.WriteAllText(Ruta + @"\" + inventario_lote + ".csv", ConvertToCSV(CTB_MOVIMIENTO_INVENTARIO_LOTE.Tables[0]));

                            var files = Directory.GetFiles(Ruta);
                            string Password = Actual_Conexion.RucEmpresa.Trim();
                            Comprimir(files, Ruta, Accion.Encriptar(Password));



                            p.Abort();
                        }
                        catch (Exception ex)
                        {
                            p.Abort();
                            Accion.ErrorSistema(ex.Message);
                        }




                    }


                }
            }
            catch (Exception ex)
            {

            }
        }


        public void Progreso()
        {
            WaitForm1 dlg = new WaitForm1();
            dlg.ShowDialog();
        }


        public static void Comprimir(string[] Archivos, string Ruta, string password)
        {
            using (var archive = new ZipArchive())
            {
                foreach (string file in Archivos)
                {
                    ZipFileItem zipFI = archive.AddFile(file, "/");
                    zipFI.EncryptionType = EncryptionType.Aes128;
                    zipFI.Password = password;
                }

                archive.Save(Ruta + ".rar");
                // File.Move(Ruta + ".rar", Ruta + ".xml");
                archive.Dispose();
            }
        }


        private static string ConvertToCSV(DataTable oDt)
        {
            var sb = new StringBuilder();
            var columnNames = oDt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
            sb.AppendLine(string.Join("|", columnNames));
            foreach (DataRow row in oDt.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                sb.AppendLine(string.Join("|", fields));
            }

            return sb.ToString();
        }

        string RutaArchivo = "";

        private void btnimportardatos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Threading.Thread p = new System.Threading.Thread((this.Progreso));

            try
            {


                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "ZIP Folder (.rar)|*.rar";
                openFileDialog1.ShowDialog();
                openFileDialog1.Multiselect = true;
                RutaArchivo = openFileDialog1.FileName;

                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Nombre = Path.GetFileNameWithoutExtension(RutaArchivo);
                string Destino;
                Destino = desktopPath + @"\" + Nombre;

                p.Start();

                if (!Directory.Exists(Destino))
                {
                    Directory.CreateDirectory(Destino);
                }

                if (Directory.Exists(Destino))
                {
                    foreach (var f in Directory.GetFiles(Destino))

                        File.Delete(Conversions.ToString(f));
                    string Password = Actual_Conexion.RucEmpresa.Trim();
                    if (!Descomprimir(RutaArchivo, Destino, Accion.Encriptar(Password)))
                    {
                        Directory.Delete(Destino, true);
                    }
                }


                if (Directory.Exists(Destino))
                {
                    // Aki empesamos a importar los archivos
                    Importar_(Destino);

                }

                p.Abort();
            }
            catch (Exception ex)
            {
                p.Abort();
                Accion.ErrorSistema(ex.Message);
            }
        }



        public void Importar_(string Destino)
        {
            if (File.Exists(Destino + @"\" + "GNL_ENTIDAD.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "GNL_ENTIDAD.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_GNL_ENTIDAD_XML(Dt);
            }

          

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_CAB.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_CAB.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBOS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DET.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_DET.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(Dt);
            }

            if (File.Exists(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_LOTE.csv"))
            {
                var Dt = new DataTable();
                Dt = GetDataTableFromCsv(Destino + @"\" + "CTB_MOVIMIENTO_INVENTARIO_LOTE.csv", true);
                Logica_Exportar_Importar Log = new Logica_Exportar_Importar();
                Log.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(Dt);
            }
 


        }


        private static DataTable GetDataTableFromCsv(string path__1, bool isFirstRowHeader)
        {
            var csvData = new DataTable();
            using (var csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(path__1))
            {
                csvReader.SetDelimiters(new string[] { "|" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                var colFields = csvReader.ReadFields();
                foreach (var Column in colFields)
                {
                    var datecolumn = new DataColumn(Column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }

                int Cont = 2;
                while (!csvReader.EndOfData)
                {
                    var fieldData = csvReader.ReadFields();
                    // Making empty value as null
                    if (fieldData.Count() != csvData.Columns.Count)
                    {
                        Accion.Advertencia("ARCHIVO CON FALLAS,El archivo " + Path.GetFileName(path__1) + " En la linea " + Cont + " la cantidad de columnas de cabecera es " + csvData.Columns.Count + " la fila presenta " + fieldData.Count());
                    }

                    for (int i = 0, loopTo = fieldData.Length - 1; i <= loopTo; i++)
                    {
                        if (string.IsNullOrEmpty(fieldData[i]))
                        {
                            fieldData[i] = null;
                        }
                    }

                    csvData.Rows.Add(fieldData);
                    Cont = Cont + 1;
                }
            }

            return csvData;
        }


        public static bool Descomprimir(string ArchivoRar, string RutaDescomprimir, string password)
        {
            using (ZipArchive archive = ZipArchive.Read(ArchivoRar))
            {
                foreach (ZipItem item in archive)
                {
                    try
                    {
                        item.Password = password;
                        item.Extract(RutaDescomprimir);
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            return true;
        }











    }
}
