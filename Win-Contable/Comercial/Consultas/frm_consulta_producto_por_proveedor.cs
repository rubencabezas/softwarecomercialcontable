﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contable.Comercial.Consultas
{
    public partial class frm_consulta_producto_por_proveedor : frm_fuente
    {
        public frm_consulta_producto_por_proveedor()
        {
            InitializeComponent();
        }

        private void frm_consulta_producto_por_proveedor_Load(object sender, EventArgs e)
        {
            Listar();
        }

        public List<Entidad_Movimiento_Cab> Lista = new List<Entidad_Movimiento_Cab>();
        public void Listar()
        {

            Entidad_Movimiento_Cab Ent = new Entidad_Movimiento_Cab();
            Logica_Movimiento_Cab log = new Logica_Movimiento_Cab();

            Ent.Id_Empresa = Actual_Conexion.CodigoEmpresa;
 
            try
            {
                Lista = log.Listar_adm_Productos_Por_Proveedor(Ent);
                if (Lista.Count > 0)
                {
                    dgvdatos.DataSource = Lista;
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }


        }

        private void btnactualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Listar();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string Ruta;
                Ruta = path + ("\\ConsultaProveedorProducto" + ".Xlsx");
                gridView1.ExportToXlsx(Ruta);
                System.Diagnostics.Process.Start(Ruta);
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }
        }
    }
}
