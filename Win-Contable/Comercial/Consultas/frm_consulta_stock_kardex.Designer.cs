﻿namespace Comercial
{
    partial class frm_consulta_stock_kardex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnvistaprevia = new DevExpress.XtraBars.BarButtonItem();
            this.btnresumen = new DevExpress.XtraBars.BarButtonItem();
            this.btnresumenporlote = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl6 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl7 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            this.btnmodificar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.txtalmacendesc = new DevExpress.XtraEditors.TextEdit();
            this.txtalmacencod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtproductodesc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtfechainicio = new DevExpress.XtraEditors.TextEdit();
            this.txtproductocod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtfechafin = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechainicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechafin.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControl5);
            this.barManager1.DockControls.Add(this.barDockControl6);
            this.barManager1.DockControls.Add(this.barDockControl7);
            this.barManager1.DockControls.Add(this.barDockControl8);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnvistaprevia,
            this.btnmodificar,
            this.barButtonItem1,
            this.barButtonItem2,
            this.btnresumen,
            this.btnresumenporlote});
            this.barManager1.MaxItemId = 6;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnvistaprevia),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnresumen),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnresumenporlote)});
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnvistaprevia
            // 
            this.btnvistaprevia.Caption = "Vista Previa Detallado";
            this.btnvistaprevia.Id = 0;
            this.btnvistaprevia.ImageOptions.Image = global::Contable.Properties.Resources.vistaprevia;
            this.btnvistaprevia.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvistaprevia.ItemAppearance.Normal.Options.UseFont = true;
            this.btnvistaprevia.Name = "btnvistaprevia";
            this.btnvistaprevia.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnvistaprevia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnvistaprevia_ItemClick);
            // 
            // btnresumen
            // 
            this.btnresumen.Caption = "Vista Previa Resumen";
            this.btnresumen.Id = 4;
            this.btnresumen.ImageOptions.Image = global::Contable.Properties.Resources.busquedaavanzada;
            this.btnresumen.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnresumen.ItemAppearance.Normal.Options.UseFont = true;
            this.btnresumen.Name = "btnresumen";
            this.btnresumen.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnresumen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnresumen_ItemClick);
            // 
            // btnresumenporlote
            // 
            this.btnresumenporlote.Caption = "Vista Previa Resumen por lote";
            this.btnresumenporlote.Id = 5;
            this.btnresumenporlote.ImageOptions.Image = global::Contable.Properties.Resources.vistapreviadetallado;
            this.btnresumenporlote.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnresumenporlote.ItemAppearance.Normal.Options.UseFont = true;
            this.btnresumenporlote.Name = "btnresumenporlote";
            this.btnresumenporlote.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnresumenporlote.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl5.Location = new System.Drawing.Point(0, 0);
            this.barDockControl5.Manager = this.barManager1;
            this.barDockControl5.Size = new System.Drawing.Size(800, 32);
            // 
            // barDockControl6
            // 
            this.barDockControl6.CausesValidation = false;
            this.barDockControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl6.Location = new System.Drawing.Point(0, 450);
            this.barDockControl6.Manager = this.barManager1;
            this.barDockControl6.Size = new System.Drawing.Size(800, 0);
            // 
            // barDockControl7
            // 
            this.barDockControl7.CausesValidation = false;
            this.barDockControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl7.Location = new System.Drawing.Point(0, 32);
            this.barDockControl7.Manager = this.barManager1;
            this.barDockControl7.Size = new System.Drawing.Size(0, 418);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl8.Location = new System.Drawing.Point(800, 32);
            this.barDockControl8.Manager = this.barManager1;
            this.barDockControl8.Size = new System.Drawing.Size(0, 418);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Caption = "Eliminar";
            this.btnmodificar.Id = 1;
            this.btnmodificar.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodificar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Anular";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Revertir anulado";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barButtonItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // txtalmacendesc
            // 
            this.txtalmacendesc.Enabled = false;
            this.txtalmacendesc.EnterMoveNextControl = true;
            this.txtalmacendesc.Location = new System.Drawing.Point(98, 19);
            this.txtalmacendesc.MenuManager = this.barManager1;
            this.txtalmacendesc.Name = "txtalmacendesc";
            this.txtalmacendesc.Size = new System.Drawing.Size(538, 20);
            this.txtalmacendesc.TabIndex = 2;
            // 
            // txtalmacencod
            // 
            this.txtalmacencod.EnterMoveNextControl = true;
            this.txtalmacencod.Location = new System.Drawing.Point(58, 19);
            this.txtalmacencod.MenuManager = this.barManager1;
            this.txtalmacencod.Name = "txtalmacencod";
            this.txtalmacencod.Size = new System.Drawing.Size(39, 20);
            this.txtalmacencod.TabIndex = 1;
            this.txtalmacencod.TextChanged += new System.EventHandler(this.txtalmacencod_TextChanged);
            this.txtalmacencod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtalmacencod_KeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 22);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(44, 13);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Almacen:";
            // 
            // txtproductodesc
            // 
            this.txtproductodesc.Enabled = false;
            this.txtproductodesc.EnterMoveNextControl = true;
            this.txtproductodesc.Location = new System.Drawing.Point(164, 42);
            this.txtproductodesc.Name = "txtproductodesc";
            this.txtproductodesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproductodesc.Size = new System.Drawing.Size(472, 20);
            this.txtproductodesc.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(9, 68);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(59, 13);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Fecha inicio:";
            // 
            // txtfechainicio
            // 
            this.txtfechainicio.EnterMoveNextControl = true;
            this.txtfechainicio.Location = new System.Drawing.Point(74, 65);
            this.txtfechainicio.Name = "txtfechainicio";
            this.txtfechainicio.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechainicio.Properties.Mask.BeepOnError = true;
            this.txtfechainicio.Properties.Mask.EditMask = "d";
            this.txtfechainicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechainicio.Properties.Mask.SaveLiteral = false;
            this.txtfechainicio.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechainicio.Properties.MaxLength = 10;
            this.txtfechainicio.Size = new System.Drawing.Size(89, 20);
            this.txtfechainicio.TabIndex = 7;
            // 
            // txtproductocod
            // 
            this.txtproductocod.EnterMoveNextControl = true;
            this.txtproductocod.Location = new System.Drawing.Point(58, 42);
            this.txtproductocod.Name = "txtproductocod";
            this.txtproductocod.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproductocod.Size = new System.Drawing.Size(105, 20);
            this.txtproductocod.TabIndex = 4;
            this.txtproductocod.TextChanged += new System.EventHandler(this.txtproductocod_TextChanged);
            this.txtproductocod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproductocod_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(9, 45);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(47, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Producto:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtalmacendesc);
            this.groupBox1.Controls.Add(this.txtfechafin);
            this.groupBox1.Controls.Add(this.txtfechainicio);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Controls.Add(this.txtalmacencod);
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.txtproductocod);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.txtproductodesc);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(639, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametros";
            // 
            // txtfechafin
            // 
            this.txtfechafin.EnterMoveNextControl = true;
            this.txtfechafin.Location = new System.Drawing.Point(244, 65);
            this.txtfechafin.Name = "txtfechafin";
            this.txtfechafin.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.txtfechafin.Properties.Mask.BeepOnError = true;
            this.txtfechafin.Properties.Mask.EditMask = "d";
            this.txtfechafin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtfechafin.Properties.Mask.SaveLiteral = false;
            this.txtfechafin.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtfechafin.Properties.MaxLength = 10;
            this.txtfechafin.Size = new System.Drawing.Size(89, 20);
            this.txtfechafin.TabIndex = 9;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(179, 68);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Fecha fin:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 32);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 418);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 392);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Reporte";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // frm_consulta_stock_kardex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.barDockControl7);
            this.Controls.Add(this.barDockControl8);
            this.Controls.Add(this.barDockControl6);
            this.Controls.Add(this.barDockControl5);
            this.Name = "frm_consulta_stock_kardex";
            this.Text = "esta desactualizado";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacendesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtalmacencod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductodesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechainicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtproductocod.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfechafin.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnvistaprevia;
        private DevExpress.XtraBars.BarButtonItem btnmodificar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarDockControl barDockControl6;
        private DevExpress.XtraBars.BarDockControl barDockControl7;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraEditors.TextEdit txtalmacendesc;
        private DevExpress.XtraEditors.TextEdit txtalmacencod;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.TextEdit txtproductodesc;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        public DevExpress.XtraEditors.TextEdit txtfechainicio;
        public DevExpress.XtraEditors.TextEdit txtproductocod;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.GroupBox groupBox1;
        public DevExpress.XtraEditors.TextEdit txtfechafin;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevExpress.XtraBars.BarButtonItem btnresumen;
        private DevExpress.XtraBars.BarButtonItem btnresumenporlote;
    }
}