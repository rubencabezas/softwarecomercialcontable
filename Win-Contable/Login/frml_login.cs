﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace Contable.Login
{
    public partial class frml_login : MetroFramework.Forms.MetroForm
    {
        public frml_login()
        {
            InitializeComponent();
        }

        private void frml_login_Load(object sender, EventArgs e)
        {
            this.Width = 416;
            this.Height = 149;
            txtusuario.Focus();
      
            //416; 149
        }

        private void btniniciar_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtusuario.Text.Trim()))
            {
                MessageBox.Show("Ingrese su nombre");
               txtusuario.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtcontrasena.Text.Trim()))
            {
                MessageBox.Show("Ingrese su Contraseña");
                txtcontrasena.Focus();
                return;
            }


            try
            {
                Entidad_Inicio_Sesion Ent = new Entidad_Inicio_Sesion();
                Logico_Inicio_Sesion LogUsu = new Logico_Inicio_Sesion();
                Entidad_Inicio_Sesion enti = new Entidad_Inicio_Sesion();

                Actual_Conexion.UserID = txtusuario.Text;
                Actual_Conexion.UserName = txtusuario.Text;

                enti.Usuario_dni = txtusuario.Text.Trim();
                //enti.Usuario_pass = txtcontrasena.Text.Trim();
                //enti.Contrasenia = Tools.EncryptPasswordMD5(TxtContrasenia.Text.Trim, "12345678901");
                enti.Usuario_pass = Accion.Encriptar(txtcontrasena.Text.Trim());

                Ent = LogUsu.Iniciar(enti);
                if (Ent.sms == "1")
                {
                    this.Size = new Size(416, 241);
                    IniciarBarra();
                    if (ListEmp.Count == 0)
                    {
                       // BtnCrearEmpresa.Enabled = true;
                    }
                    //Actual_Conexion.Maquina = TxtUsuario.Text;
                    btningresar.Focus();
                }
                else
                {
                    MessageBox.Show("Nombre de Usuario o Contraseña inconrrectos", "Inicio de sesion incorrecto", MessageBoxButtons.OK);
                    txtusuario.Focus();
                }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }




        }

        private List<Entidad_Empresa> ListEmp = new List<Entidad_Empresa>();
        public void IniciarBarra()
        {
            try
            {
                Logica_Empresa LogEmp = new Logica_Empresa();
                Entidad_Empresa Ent = new Entidad_Empresa();
                cbxempresa.ValueMember = "Id_Empresa";
                cbxempresa.DisplayMember = "Emp_Nombre";
                Ent.Usuario_dni = txtusuario.Text.Trim();

                ListEmp = LogEmp.Listar(Ent);

                cbxempresa.DataSource = ListEmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Inicio de sesion incorrecto", MessageBoxButtons.OK);
            }
        }

        private void btningresar_Click(object sender, EventArgs e)
        {
            try
            {
          if (cbxempresa.SelectedIndex >= 0)
            {
                Logico_Inicio_Sesion LogUsu = new Logico_Inicio_Sesion();
                Actual_Conexion.UserID = txtusuario.Text.Trim();

                    //DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Hide();
                    using (frm_main_master f = new frm_main_master())
                    {

                        f.EmpresaNombre = Actual_Conexion.EmpresaNombre;
                        f.CodigoEmpresa = Actual_Conexion.CodigoEmpresa;
                        f.RucEmpresa = Actual_Conexion.RucEmpresa;

                        f.Emp_DireccionCorta = Actual_Conexion.Emp_DireccionCorta;
                        f.Emp_Urbanizacion = Actual_Conexion.Emp_Urbanizacion;
                        f.Emp_Direccion_Departamento = Actual_Conexion.Emp_Direccion_Departamento;
                        f.Emp_Direccion_Provincia = Actual_Conexion.Emp_Direccion_Provincia;
                        f.Emp_Direccion_Distrito = Actual_Conexion.Emp_Direccion_Distrito;

                        f.UserID = Actual_Conexion.UserID;
                        f.UserName = Actual_Conexion.UserName;
                        f.UserNameEmployee = Actual_Conexion.UserNameEmployee;
                        f.lblusuario.Caption = Actual_Conexion.UserID;



                        if (f.ShowDialog() == DialogResult.OK)
                        {


                            try
                            {

                            }
                            catch (Exception ex)
                            {
                                Accion.ErrorSistema(ex.Message);
                            }
                        }

                    }
                }
            else
            {
                MessageBox.Show("Seleccionar la Empresa", "Aviso", MessageBoxButtons.OK);
            }
            }
            catch (Exception ex)
            {
                Accion.ErrorSistema(ex.Message);
            }

  

        }

        private void cbxempresa_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbxempresa.Items.Count > 0)
            {
                Actual_Conexion.CodigoEmpresa = cbxempresa.SelectedValue.ToString();

                var PrivView = from item in ListEmp
                               where item.Id_Empresa == cbxempresa.SelectedValue.ToString()
                select item;
                List<Entidad_Empresa> Empresa = PrivView.ToList();

  
                Actual_Conexion.RucEmpresa = Empresa[0].Emp_Ruc;
                Actual_Conexion.EmpresaNombre = Empresa[0].Emp_Nombre;

                Actual_Conexion.Emp_DireccionCorta = Empresa[0].Emp_DireccionCorta;
                Actual_Conexion.Emp_Urbanizacion = Empresa[0].Emp_Urbanizacion;
                Actual_Conexion.Emp_Direccion_Departamento = Empresa[0].Emp_Direccion_Departamento;
                Actual_Conexion.Emp_Direccion_Provincia = Empresa[0].Emp_Direccion_Provincia;
                Actual_Conexion.Emp_Direccion_Distrito = Empresa[0].Emp_Direccion_Distrito;

                //ListarAnios();
                //DatosActualConexion.AnioSelect = CbxAnio.SelectedValue;
                //ListarPeriodos();
                // ActualizarIGV()
            }

        }

        private void txtusuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtcontrasena.Focus();
            }
        }

        private void txtcontrasena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btniniciar.Focus();
            }
        }

        private void btniniciar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btningresar.Focus();
            }
        }
    }
}
