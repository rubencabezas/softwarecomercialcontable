﻿namespace Contable.Login
{
    partial class frml_login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtusuario = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtcontrasena = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.btniniciar = new MetroFramework.Controls.MetroButton();
            this.cbxempresa = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.btningresar = new MetroFramework.Controls.MetroButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(228, 38);
            this.txtusuario.MaxLength = 8;
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(175, 23);
            this.txtusuario.TabIndex = 1;
            this.txtusuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtusuario_KeyDown);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(148, 36);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(74, 25);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Usuario:";
            // 
            // txtcontrasena
            // 
            this.txtcontrasena.Location = new System.Drawing.Point(228, 67);
            this.txtcontrasena.MaxLength = 11;
            this.txtcontrasena.Name = "txtcontrasena";
            this.txtcontrasena.PasswordChar = '*';
            this.txtcontrasena.Size = new System.Drawing.Size(175, 23);
            this.txtcontrasena.TabIndex = 3;
            this.txtcontrasena.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcontrasena_KeyDown);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.Location = new System.Drawing.Point(121, 64);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(102, 25);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Contraseña:";
            // 
            // btniniciar
            // 
            this.btniniciar.Location = new System.Drawing.Point(328, 108);
            this.btniniciar.Name = "btniniciar";
            this.btniniciar.Size = new System.Drawing.Size(75, 23);
            this.btniniciar.TabIndex = 4;
            this.btniniciar.Text = "Iniciar sesion";
            this.btniniciar.Click += new System.EventHandler(this.btniniciar_Click);
            this.btniniciar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btniniciar_KeyDown);
            // 
            // cbxempresa
            // 
            this.cbxempresa.FormattingEnabled = true;
            this.cbxempresa.ItemHeight = 23;
            this.cbxempresa.Location = new System.Drawing.Point(23, 171);
            this.cbxempresa.Name = "cbxempresa";
            this.cbxempresa.Size = new System.Drawing.Size(380, 29);
            this.cbxempresa.TabIndex = 5;
            this.cbxempresa.SelectedIndexChanged += new System.EventHandler(this.cbxempresa_SelectedIndexChanged);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(138, 149);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(139, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Seleccione la empresa";
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(228, 108);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(75, 23);
            this.metroButton2.TabIndex = 4;
            this.metroButton2.Text = "Salir";
            // 
            // btningresar
            // 
            this.btningresar.Location = new System.Drawing.Point(328, 206);
            this.btningresar.Name = "btningresar";
            this.btningresar.Size = new System.Drawing.Size(75, 23);
            this.btningresar.TabIndex = 6;
            this.btningresar.Text = "Ingresar";
            this.btningresar.Click += new System.EventHandler(this.btningresar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Contable.Properties.Resources.iconoContailidad2;
            this.pictureBox1.Location = new System.Drawing.Point(13, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(102, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // frml_login
            // 
            this.ClientSize = new System.Drawing.Size(416, 241);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.cbxempresa);
            this.Controls.Add(this.btningresar);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.btniniciar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtcontrasena);
            this.Controls.Add(this.txtusuario);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frml_login";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Load += new System.EventHandler(this.frml_login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtusuario;
        public MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public MetroFramework.Controls.MetroLabel metroLabel2;
        public MetroFramework.Controls.MetroTextBox txtcontrasena;
        private MetroFramework.Controls.MetroButton btniniciar;
        private MetroFramework.Controls.MetroComboBox cbxempresa;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton btningresar;
    }
}
