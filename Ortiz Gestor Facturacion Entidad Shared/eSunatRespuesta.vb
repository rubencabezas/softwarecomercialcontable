﻿Public Class eSunatRespuesta
    Public Property Codigo As String

    'Public Property DigestValue As String

    Public Property Estado As EstadoDocumento

    Public Property Mensaje As String
 
    'Public Property RutaXML_DLL As String

    'Public Property RutaXML_SNT As String

    Public Property Ticket As String

    Public Property Fecha_Respuesta As Date

End Class
Public Enum EstadoDocumento
    ' Fields
    Aceptado = 2
    [Error] = 0
    Generado = 4
    Invalido = 3
    Rechazado = 1
End Enum