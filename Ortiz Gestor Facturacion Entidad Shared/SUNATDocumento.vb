﻿Public Class SUNATDocumento
    Shared ReadOnly Property Factura As String
        Get
            Return "01"
        End Get
    End Property
    Shared ReadOnly Property Boleta As String
        Get
            Return "03"
        End Get
    End Property
    Shared ReadOnly Property NotaCredito As String
        Get
            Return "07"
        End Get
    End Property
    Shared ReadOnly Property NotaDebito As String
        Get
            Return "08"
        End Get
    End Property
    Shared ReadOnly Property GuiaRemisionRemitente As String
        Get
            Return "09"
        End Get
    End Property
    Shared ReadOnly Property ComprobanteRetencion As String
        Get
            Return "20"
        End Get
    End Property
    Shared ReadOnly Property ComprobantePercepcion As String
        Get
            Return "40"
        End Get
    End Property
End Class
