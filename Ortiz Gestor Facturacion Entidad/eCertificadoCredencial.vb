﻿Public Class eCertificadoCredencial
    Property Emisor_RUC As String
    Property Correlativo As Integer
    Property Codigo As String
    Property EsPropio As Boolean
    Property Propietario_RUC As String
    Property Propietario_RazonSocial As String
    Property Ruta As String
    Property Contrasena As String
    Property Sunat_Usuario As String
    Property Sunat_Contrasena As String
    Property Sunat_Autorizacion As String
    Property Sunat_Servicio_URL As String
    Property Correo_Usuario As String
    Property Correo_Contrasena As String
    Property Correo_Servidor_SMTP As String
    Property Correo_Servidor_Puerto As String
    Property Correo_Servidor_SSL As Boolean
    Property Emisor_Web_Visualizacion As String
    Property Etapa_Codigo As String
    Property Path_Root_App As String
    'Property PathServer As String
    Property FTP_Direccion As String
    Property FTP_Carpeta As String
    Property FTP_Usuario As String
    Property FTP_Contrasena As String
    Property Firma_DigestValue As String 'FirmaValorResumen
    Property Firma_SignatureValue As String 'FirmaValorFirma
    Property Valido_Desde As Date
    Property Valido_Hasta As Date

End Class
