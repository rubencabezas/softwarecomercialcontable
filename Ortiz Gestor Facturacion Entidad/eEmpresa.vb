﻿Public Class eEmpresa
    Property Emp_Codigo As String
    Property Emp_NumRuc As String
    Property Emp_NombreRazonSocial As String
    Property Emp_EnviaResumenBoletas As Boolean
    Property Sunat_Usuario As String
    Property Sunat_Contrasena As String
    Property Sunat_Autorizacion As String
    Property Emisor_Web_Visualizacion As String
    Property Correo_Usuario As String
    Property Correo_Contrasena As String
    Property Correo_ServidorSMTP As String
    Property Correo_ServidorPuerto As String
    Property Correo_ServidorSSL As Boolean
    Property Etapa_Codigo As String
    Property Etapa_Descripcion As String
    Property Path_Root_App As String
    'Property PathLocal As String
    'Property PathServer As String
    Property FTP_Direccion As String
    Property FTP_Carpeta As String
    Property FTP_Usuario As String
    Property FTP_Contrasena As String
    Property ActualizarClientes As Boolean
    Sub New()
    End Sub

End Class
