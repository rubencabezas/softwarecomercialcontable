﻿Public Class eProvisionFacturacion
    Property Emisor_RUC As String
    Property Emisor_RazonSocial_Nombre As String
    Property Emisor_Direccion_Ubigeo As String
    Property Emisor_Direccion_Calle As String
    Property Emisor_Direccion_Urbanizacion As String
    Property Emisor_Direccion_Distrito As String
    Property Emisor_Direccion_Provincia As String
    Property Emisor_Direccion_Departamento As String
    Property Emisor_Direccion_PaisCodigo As String 'Catalogo 04
    ReadOnly Property Emisor_Direccion_Departamento_Provincia_Distrito As String 'Catalogo 04
        Get
            Return Emisor_Direccion_Departamento & " - " & Emisor_Direccion_Provincia & " - " & Emisor_Direccion_Distrito
        End Get
    End Property
    ReadOnly Property Emisor_Direccion_Calle_Urbanizacion As String 'Catalogo 04
        Get
            Return Emisor_Direccion_Calle & " " & Emisor_Direccion_Urbanizacion
        End Get
    End Property

    Property Dvt_VTSerie As String
    Property Dvt_VTNumer As String
    Property Dvt_Fecha_Emision As Date
    Property Dvt_Fecha_Vencimiento As Date
    Property Dvt_Fecha_Generacion As Date
    Property Dvt_Moneda_Internacional As String
    Property Cliente_Documento_Tipo As String
    Property Cliente_Documento_Numero As String
    Property Cliente_RazonSocial_Nombre As String
    Property Cliente_Apellido_Primero As String
    Property Cliente_Apellido_Segundo As String
    ReadOnly Property Cliente_RazonSocial_Nombre_Completo As String
        Get
            Dim Apellidos As String = (IIf(String.IsNullOrEmpty(Cliente_Apellido_Primero), "", Cliente_Apellido_Primero) & " " & IIf(String.IsNullOrEmpty(Cliente_Apellido_Segundo), "", Cliente_Apellido_Segundo)).ToString.Trim
            Return (IIf(String.IsNullOrEmpty(Apellidos), "", Apellidos & " ")) & Cliente_RazonSocial_Nombre
        End Get
    End Property
    Property Cliente_Correo_Electronico As String
    Property Dvt_Total_IGV As Double
    Property Dvt_Total_Importe As Double
    ReadOnly Property Dvt_Total_Importe_Letras As String
        Get
            Return (Imprimir_Letras.NroEnLetras(Dvt_Total_Importe) & " " & Dvt_Des_Moneda)
        End Get
    End Property
    ReadOnly Property Dvt_Total_Importe_Letras2 As String
        Get
            Return "IMPORTE EN LETRAS: " & (Imprimir_Letras.NroEnLetras(Dvt_Total_Importe) & " " & Dvt_Des_Moneda)
        End Get
    End Property
    'FIN REQUERIDO

    Property Dvt_Origen_Codigo As String
    Property ProvisionEnviado As Boolean
    Property SoloVenta As Boolean = False
    Property IncluirAniosAnteriores As Boolean = False
    Property Seleccionado As Boolean = False
    Property Item As Integer
    Property Emisor_Codigo As String
    Property Pan_cAnio As String
    Property Per_cPeriodo As String
    Property Dvt_Codigo As String
    Property Dvt_TipoBSA As String
    Property Pdv_Codigo As String
    Property Pdv_Descripcion As String 'Descripcion del punto de venta
    Property Alm_Codigo As String
    Property Alm_Descripcion As String

    Property TpDc_Codigo_St As String

    Property TpDc_Descripcion As String 'Nombre del documento

    Property Ent_cCodEntidad As String
    Property Ten_cTipoEntidad As String
   
    Property Cliente_Direccion As String

    Property Dir_Codigo As String 'cODIGO DE DIRECCION
    Property Dir_Descripcion As String


    Property Con_cCondicion As String
    Property Con_cDescripcion As String 'Descripcion de la condicion
    Property Dvt_DiasCondic As Integer

    Property Dvt_Cod_TipoPrc As String
    Property Dvt_Des_TipoPrc As String

    Property Dvt_Cod_Moneda As String
    Property Dvt_Des_Moneda As String

    Property Dvt_Abrev_Moneda As String

    Property Dvt_Simbol_Moneda As String

    Property Dvt_TipCambCod As String
    Property Dvt_TipCambMont As Double
    ReadOnly Property Dvt_TipCambDesc As String
        Get
            If Dvt_TipCambCod = "COM" Then
                Return "COMPRA VIGENTE"
            ElseIf Dvt_TipCambCod = "VEN" Then
                Return "VENTA VIGENTE"
            ElseIf Dvt_TipCambCod = "VEP" Then
                Return "VENTA PUBLICACION"
            ElseIf Dvt_TipCambCod = "SCV" Then
                Return "SIN CONVERSION"
            Else
                Return "OTROS"
            End If
        End Get
    End Property
    'Solo pedido

    Property Dvt_Est_Fact_Ds As String
    Property Vnt_EstadoEnvio As String
    Property Detalle As New List(Of eProvisionFacturacionDetalle)
    Property ReferenciaDocumento As New List(Of eProvisionFacturacionReferenciaDocumento)
    Property CertificadoCredencial As eCertificadoCredencial

    Property Vnt_Observacion As String
    '---CREDENCIALES
    Property Vnt_IGV_Porcentaje As Double
    ReadOnly Property Vnt_IGV_Porcentaje_Texto As String
        Get
            Return (Vnt_IGV_Porcentaje.ToString & " %")
        End Get
    End Property
    Property Dvt_Total_Gravado As Double
    Property Dvt_Total_Exonerado As Double
    Property Dvt_Total_Inafecto As Double
    Property Dvt_Total_Gratuitas As Double
    Property Dvt_Total_ISC As Double
    Property Dvt_Total_Otros As Double
    'Para imprimir
    Property Vnt_Cert_Resumen As String
    Property Vnt_Cert_Firma_PDF417 As String
    Property Rs_Correlativo As String
    Property Rs_TotalResumen As Double
    Property Rs_NumeroInicio As String
    Property Rs_NumeroFin As String
    Property Rs_NumeroLinea As String

    Property Ret_TipoCodigo As String
    Property Ret_Porcentaje As Double
    Property Ret_ImporteRetenidoTotal As Double 'Siempre en soles
    Property Ret_ImportePagadoTotal As Double 'Siempre en soles

    Property Ref_Documento_St As String
    Property Ref_Documento_Serie As String
    Property Ref_Documento_Numero As String
    Property Ref_Documento_FechaEmision As Date
    Property Ref_Documento_ImporteTotal As Double 'Importe total del documento
    Property Ref_Documento_MonedaInternacional As String 'Moneda del documento

    Property Ref_Pag_Codigo As Integer
    Property Ref_Pag_Fecha As Date
    Property Ref_Pag_ImporteSinRetencion As Double

    Property Ref_Ret_ImporteRetenido As Double '--siempre en soles
    Property Ref_Ret_ImporteTotalInclRetencion As Double '--moneda nacional

    Property Ref_Ret_TC_Aplicado As Double
    'Property Ref_Ret_TC_Fecha As Date

    Property EstadoElectronico As String
    Property Estado_Descripcion As String
    Property EstadoDoc_Codigo As String
    Property EstadoDoc_Descripcion As String
    Property Doc_NombreArchivoWeb As String
    ReadOnly Property ActionModel As String
        Get
            If TpDc_Codigo_St = SUNATDocumento.Boleta Then
                Return "Invoice"
            ElseIf TpDc_Codigo_St = SUNATDocumento.Factura Then
                Return "Invoice"
            ElseIf TpDc_Codigo_St = SUNATDocumento.NotaCredito Then
                Return "CreditNote"
            ElseIf TpDc_Codigo_St = SUNATDocumento.NotaDebito Then
                Return "DebitNote"
            ElseIf TpDc_Codigo_St = SUNATDocumento.GuiaRemisionRemitente Then
                Return "DespatchAdvice"
            ElseIf TpDc_Codigo_St = SUNATDocumento.ComprobantePercepcion Then
                Return "Perception"
            ElseIf TpDc_Codigo_St = SUNATDocumento.ComprobanteRetencion Then
                Return "Retention"
            End If
        End Get
    End Property
    ReadOnly Property PostIDActionModel As String
        Get
            Return (ActionModel & "/" & Doc_NombreArchivoWeb)
        End Get
    End Property
    Property SPOT_Afecto As Boolean
    Property SPOT_Reg_Codigo As String
    Property SPOT_Monto As Double
    Property SPOT_Porcentaje As Double
    Property Vnt_AnulacionRazon As String
    Property Vnt_AnulacionFecha As Date
    ''
    Property Etapa_Codigo As String
    Property Busq_Fecha_Inicio As Date
    Property Busq_Fecha_Fin As Date
    Property Path_Root_App As String
    Property Nombre_XML As String
    Property Nombre_PDF As String
    Property Doc_Correo_Enviado As Boolean
    Property Doc_Correo_EstaLeido As Boolean
    Property TipoXML_Codigo As String
    Property XML_POS As DocumentoElectronico
    Property Sunat_Respuesta_Codigo As String
    Property Sunat_Respuesta_Descripcion As String
    Property Sunat_Respuesta_Ticket As String
    Property Sunat_Respuesta_Fecha As Date
    Property Version_Correlativo As Integer
    Property DigestValue As String
    Property SignatureValue As String
    Property RootPath_Enterprise As String
    Property Modif_Doc_Serie_Numero As String
    Property Modif_Doc_Tipo As String
    Property Grupo_Codigo As String
    'Property Baja_Motivo As String
    Property SQL_XML_Nombre As String

    Sub New()
    End Sub
End Class
