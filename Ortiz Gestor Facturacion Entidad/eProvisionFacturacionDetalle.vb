﻿Public Class eProvisionFacturacionDetalle
    Property Item_CodigoProducto As String
    Property Item_Orden As String
    Property Item_Unidad_Codigo As String
    Property Item_Cantidad As Double
    Property Item_Unitario_ValorVenta As Double
    Property Item_Unitario_Precio As Double
    Property Item_Unitario_Precio_Tipo As String 'CATALOGO 16:---- 01-PRECIO UNIT ---- 02 -VALOR REFERENCIAL UNITARIO EN OPERACIONES NO ONEROSAS
    Property Item_Unitario_IGV As Double
    Property Item_Afectacion_IGV As String 'CATALOGO 07:----

    Property Item_Trib_IGV_Codigo As String = "1000" 'CATALOGO 05:---- 1000-IGV ---- 2000-ISC ---- 9999-OTROS
    Property Item_Trib_IGV_Nombre As String = "IGV"
    Property Item_Trib_IGV_Tipo As String = "VAT"
    Property Item_Trib_IGV_Porcentaje As String

    Property Item_Trib_ISC_Valor As Double
    Property Item_Trib_ISC_Codigo As String = "2000" 'CATALOGO 05
    Property Item_Trib_ISC_Nombre As String = "ISC IMPUESTO SELECTIVO AL CONSUMO"
    Property Item_Trib_ISC_Tipo As String = "EXC"

    Property Item_Trib_Otros_Valor As Double
    Property Item_Trib_Otros_Codigo As String = "2000" 'CATALOGO 05
    Property Item_Trib_Otros_Nombre As String = "ISC IMPUESTO SELECTIVO AL CONSUMO"
    Property Item_Trib_Otros_Tipo As String = "OTH"

    '--ana becerra
    Property Item_Descripcion As String

    Property Item_Unitario_Importe As Double

End Class
