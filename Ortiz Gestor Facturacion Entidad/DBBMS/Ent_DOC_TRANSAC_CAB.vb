﻿Public Class Ent_DOC_TRANSAC_CAB
    Property Establecimiento_Codigo As String
    Property IsConta As Boolean
    Property EstadoCambio As String
    Property Per_cPeriodoDesc As String
    Property Dcm_Codigo As String

    Property DocTipoOrden As String
    Property Doc_NumItem As String
    Property DocPer_cPeriodo As String
    Property DocORD_Codigo As String

    Property IsCostoVinculado As Boolean
    Property IsNotaCredito As Boolean
    Property IsNotaDebito As Boolean

    Property Dcm_NumItem As String

    Property Pdc_Codigo As String
    Property Pdc_Descripcion As String 'Descripcion del punto de venta
    Property WithAlmacen As Boolean

    Property Pdc_Motivo_Cod As String
    Property Pdc_Motivo_Ofi As String
    Property Pdc_Motivo_Des As String

    ''Property TpDc_Codigo As String
    Property TpDc_Codigo_Sunat As String
    'Property TpDc_Descripcion As String 'Nombre del documento
    Property Dcm_VTSerie As String
    Property Dcm_VTNumer As String
    Property Dcm_VTFecha As Date
    'Cliente

    Property Dcm_DiasCondic As Integer
    Property Dcm_FechaVenci As Date

    Property Dcm_Cod_Moneda As String
    Property Dcm_Des_Moneda As String

    Property Dcm_TipCambCod As String
    Property Dcm_TipCambMont As Double
    ReadOnly Property Dcm_TipCambDesc As String
        Get
            If Dcm_TipCambCod = "COM" Then
                Return "COMPRA VIGENTE"
            ElseIf Dcm_TipCambCod = "VEN" Then
                Return "VENTA VIGENTE"
            ElseIf Dcm_TipCambCod = "VEP" Then
                Return "VENTA PUBLICACION"
            ElseIf Dcm_TipCambCod = "SCV" Then
                Return "SIN CONVERSION"
            Else
                Return "OTROS"
            End If
        End Get
    End Property
    'Solo pedido
    Property Dcm_AtencFecha As Date
    Property Dcm_AtencHora As String
    Property Dcm_PersEntrega As String
    Property Dcm_PersNumDocu As String
    Property Dcm_PersVehicul As String
    Property Dcm_PersVehiPla As String
    Property Dcm_PersVehiKmAn As String
    Property Dcm_PersVehiKmAc As String
    Property Dcm_PersAreaDest As String
    Property Dcm_PersActivi As String
    Property Dcm_PersCntCsto As String
    Property Dcm_PersMotivo As String
    'Datos Factura anticipada
    Property Dcm_EsAnticipada As Boolean
    Property Dcm_FacAnt_Anio As String
    Property Dcm_FacAnt_Periodo As String
    Property Dcm_FacAnt_Correlativo As String
    Property Dcm_FacAnt_TpDc As String
    Property Dcm_FacAnt_DcSerie As String
    Property Dcm_FacAnt_DcNumero As String
    'Vendedor
    Property Dcm_VencCoddEnt As String
    Property Dcm_VenCodTipoEnti As String
    Property Dcm_VenEnt_DNIRUC As String
    Property Dcm_VenEnt_NombreRzS As String
    Property Dcm_VenEnt_ApelliPri As String
    Property Dcm_VenEnt_ApelliSeg As String
    ReadOnly Property Dcm_VenEnt_NombreCompleto As String
        Get
            Dim Apellidos As String = (IIf(String.IsNullOrEmpty(Dcm_VenEnt_ApelliPri), "", Dcm_VenEnt_ApelliPri) & " " & IIf(String.IsNullOrEmpty(Dcm_VenEnt_ApelliSeg), "", Dcm_VenEnt_ApelliSeg)).ToString.Trim
            Return (IIf(String.IsNullOrEmpty(Apellidos), "", Apellidos & ", ")) & Dcm_VenEnt_NombreRzS
        End Get
    End Property

    Property Dcm_Observaciones As String
    'REGIMEN ESPECIAL
    Property Dcm_IsAfectoRegEsp As Boolean
    Property Dcm_AfectoRegEspCod As String
    Property Dcm_AfectoRegEspOfi As String
    Property Dcm_AfectoRegEspDesc As String

    Property Dcm_IGV_PerIncluyDoc_Compra As Boolean
    Property Dcm_IGV_PerTpDc_Codigo As String
    Property Dcm_IGV_PerTpDc_Codigo_Snt As String
    Property Dcm_IGV_PerTpDc_Desc As String
    Property Dcm_IGV_PerSerie As String
    Property Dcm_IGV_PerNumero As String
    Property Dcm_IGV_Detr_TipoOperaci As String
    Property Dcm_IGV_Detr_TipoOperaci_Desc As String
    Property Dcm_IGV_Detr_TipoProduct As String
    Property Dcm_IGV_Detr_TipoProduct_Desc As String
    Property Dcm_IGV_Detr_Porcentaje As Double
    Property Dcm_IGV_Rete_Porcentaje As Double
    Property Dcm_IGV_Monto_Afecto As Double
    '------------------------------

    Property Dcm_Otros As String


    Property Dcm_SubTotal As Double
    Property Dcm_IncluyeIGV As Boolean

    Property Dcm_Est_Fact As String
    Property Dcm_Est_Fact_Ds As String
    Property Dcm_Est_Fila As String
    Property Dcm_Est_Fila_Ds As String

    Property Dcm_VoucherContable As String
    Property Dcm_InternoContable As String
    Property Dcm_GlosaContable As String
    Property Dcm_Libro As String


    Property Dcm_DetalleDoc As List(Of Ent_DOC_TRANSAC_CAB)

    ReadOnly Property DcmD_BaseImpobleOperacionTotal As Double
        Get
            Return Dcm_MntoBaseImponible01 + Dcm_MntoBaseImponible02 + Dcm_MntoBaseImponible03
        End Get
    End Property
    ReadOnly Property DcmD_IGV_IPMTotal As Double
        Get
            Return Dcm_MntoIGV01 + Dcm_MntoIGV02 + Dcm_MntoIGV03
        End Get
    End Property
    'Nuevo Resumen
    Property Dcm_PrDstoGeneral As Double
    Property Dcm_MntoValAdqNoGrav As Double
    Property Dcm_MntoBaseImponible01 As Double
    Property Dcm_MntoBaseImponible02 As Double
    Property Dcm_MntoBaseImponible03 As Double
    Property Dcm_IsMntoISC As Boolean
    Property Dcm_MntoISC As Double
    Property Dcm_IsMntoOtr As Boolean
    Property Dcm_MntoOtr As Double
    Property Dcm_PrcIGV As Double
    Property Dcm_MntoIGV01 As Double
    Property Dcm_MntoIGV02 As Double
    Property Dcm_MntoIGV03 As Double
    Property Dcm_MntoTotal As Double
    Property Dcm_MntoTotalAlterno As Double




    Property Seleccionado As Boolean = False
    Property Item As Integer
    Property Es2012 As Boolean
    Property Emp_cCodigo As String
    Property Pan_cAnio As String
    Property Per_cPeriodo As String
    ReadOnly Property Per_Descripcion As String
        Get
            If Per_cPeriodo = "01" Then
                Return Per_cPeriodo & " Enero"
            ElseIf Per_cPeriodo = "02" Then
                Return Per_cPeriodo & " Febrero"
            ElseIf Per_cPeriodo = "03" Then
                Return Per_cPeriodo & " Marzo"
            ElseIf Per_cPeriodo = "04" Then
                Return Per_cPeriodo & " Abril"
            ElseIf Per_cPeriodo = "05" Then
                Return Per_cPeriodo & " Mayo"
            ElseIf Per_cPeriodo = "06" Then
                Return Per_cPeriodo & " Junio"
            ElseIf Per_cPeriodo = "07" Then
                Return Per_cPeriodo & " Julio"
            ElseIf Per_cPeriodo = "08" Then
                Return Per_cPeriodo & " Agosto"
            ElseIf Per_cPeriodo = "09" Then
                Return Per_cPeriodo & " Setiembre"
            ElseIf Per_cPeriodo = "10" Then
                Return Per_cPeriodo & " Octubre"
            ElseIf Per_cPeriodo = "11" Then
                Return Per_cPeriodo & " Noviembre"
            ElseIf Per_cPeriodo = "12" Then
                Return Per_cPeriodo & " Diciembre"
            ElseIf Per_cPeriodo = "00" Then
                Return Per_cPeriodo & " Apertura"
            Else
                Return Per_cPeriodo & " Otros"
            End If
        End Get
    End Property
    Property Dvt_Codigo As String
    Property Dvt_TipoBSA As String
    Property Dvt_TipoBSAOfi As String
    Property Dvt_TipoBSADes As String
    Property Pdv_Codigo As String
    Property Pdv_Lado As Int32
    Property Pdv_Descripcion As String 'Descripcion del punto de venta

    Property Alm_Codigo As String
    Property Alm_Descripcion As String

    Property Pdv_Motivo_Cod As String
    Property Pdv_Motivo_Ofi As String
    Property Pdv_Motivo_Des As String

    Property TpDc_Codigo As String
    Property TpDc_Codigo_St As String
    Property TpDc_Descripcion As String 'Nombre del documento
    Property Dvt_VTSerie As String
    Property Dvt_VTNumer As String
    Property Dvt_VTFecha As Date
    Property Dvt_VTFecha_Anular As Date
    'Cliente
    Property Ent_cCodEntidad As String
    Property Ten_cTipoEntidad As String
    Property Ent_TpDcEnt_SUNAT As String
    Property Ent_DNIRUC As String
    Property Ent_NombreRzS As String
    Property Ent_ApelliPri As String
    Property Ent_ApelliSeg As String
    ReadOnly Property Ent_NombreCompleto As String
        Get
            Dim Apellidos As String = (IIf(String.IsNullOrEmpty(Ent_ApelliPri), "", Ent_ApelliPri) & " " & IIf(String.IsNullOrEmpty(Ent_ApelliSeg), "", Ent_ApelliSeg)).ToString.Trim
            Return (IIf(String.IsNullOrEmpty(Apellidos), "", Apellidos & ", ")) & Ent_NombreRzS
        End Get
    End Property
    Property Dir_Codigo As String 'cODIGO DE DIRECCION
    Property Dir_Descripcion As String
    Property Cont_Codigo As String 'Codigo de contacto
    Property Cont_Nombres As String
    Property Cont_ApelliPri As String
    Property Cont_ApelliSeg As String
    ReadOnly Property Ent_NombreCompletoContacto As String
        Get
            Dim Apellidos As String = (IIf(String.IsNullOrEmpty(Cont_ApelliPri), "", Cont_ApelliPri) & " " & IIf(String.IsNullOrEmpty(Cont_ApelliSeg), "", Cont_ApelliSeg)).ToString.Trim
            Return (IIf(String.IsNullOrEmpty(Apellidos), "", Apellidos & ", ")) & Cont_Nombres
        End Get
    End Property

    Property Con_cCondicion As String
    Property Con_cDescripcion As String 'Descripcion de la condicion
    Property Dvt_DiasCondic As Integer
    Property Dvt_FechaVenci As Date
    Property Dvt_Cod_TipoPrc As String
    Property Dvt_Des_TipoPrc As String
    Property Dvt_Cod_Moneda As String
    Property Dvt_Des_Moneda As String

    Property Dvt_TipCambCod As String
    Property Dvt_TipCambMont As Double
    ReadOnly Property Dvt_TipCambDesc As String
        Get
            If Dvt_TipCambCod = "COM" Then
                Return "COMPRA VIGENTE"
            ElseIf Dvt_TipCambCod = "VEN" Then
                Return "VENTA VIGENTE"
            ElseIf Dvt_TipCambCod = "VEP" Then
                Return "VENTA PUBLICACION"
            ElseIf Dvt_TipCambCod = "SCV" Then
                Return "SIN CONVERSION"
            Else
                Return "OTROS"
            End If
        End Get
    End Property
    'Solo pedido
    Property Dvt_AtencFecha As Date
    Property Dvt_AtencHora As String = ""
    Property Dvt_PersEntrega As String
    Property Dvt_PersNumDocu As String
    Property Dvt_PersVehicul As String
    Property Dvt_PersVehiPla As String
    Property Dvt_PersVehiKmAn As String
    Property Dvt_PersVehiKmAc As String
    Property Dvt_PersAreaDest As String
    Property Dvt_PersActivi As String
    Property Dvt_PersCntCsto As String
    Property Dvt_PersMotivo As String
    'Datos Factura anticipada
    Property Dvt_EsAnticipada As Boolean
    Property Dvt_FacAnt_Anio As String
    Property Dvt_FacAnt_Periodo As String
    Property Dvt_FacAnt_Correlativo As String
    Property Dvt_FacAnt_TpDc As String
    Property Dvt_FacAnt_DcSerie As String
    Property Dvt_FacAnt_DcNumero As String
    'Vendedor
    Property Dvt_VencCoddEnt As String
    Property Dvt_VenCodTipoEnti As String
    Property Dvt_VenEnt_DNIRUC As String = ""
    Property Dvt_VenEnt_NombreRzS As String = ""
    Property Dvt_VenEnt_ApelliPri As String = ""
    Property Dvt_VenEnt_ApelliSeg As String = ""
    ReadOnly Property Dvt_VenEnt_NombreCompleto As String
        Get
            Dim Apellidos As String = (IIf(String.IsNullOrEmpty(Dvt_VenEnt_ApelliPri), "", Dvt_VenEnt_ApelliPri.Trim) & " " & IIf(String.IsNullOrEmpty(Dvt_VenEnt_ApelliSeg), "", Dvt_VenEnt_ApelliSeg)).ToString.Trim
            Return Dvt_VenEnt_NombreRzS & (IIf(String.IsNullOrEmpty(Apellidos), "", " " & Apellidos))
        End Get
    End Property

    Property Dvt_Observaciones As String
    Property Dvt_CorreoAEnviar As String

    Property Dvt_Otros As String

    Property Dvt_SubTotal As Double
    Property Dvt_IncluyeIGV As Boolean

    Property Dvt_Est_Fact As String
    Property Dvt_Est_Fact_Ds As String
    Property Dvt_Est_Fila As String

    Property Dvt_VoucherContable As String
    Property Dvt_InternoContable As String
    Property Dvt_GlosaContable As String
    Property Dvt_Libro As String

    Property Dvt_Detalle As List(Of Ent_DOC_VENTA_DET)
    Property Dvt_Medios_Pago As List(Of Ent_DOC_VENTA_DET)
    'Para notas de credito y debito
    Property Dvt_Comprobantes As New List(Of Ent_DOC_TRANSAC_CAB)
    'Nuevo Resumen
    Property Dvt_PrDstoGeneral As Double
    Property Dvt_MntoRedondeo As Double
    Property Dvt_MntoValFactExport As Double
    Property Dvt_MntoBaseImponible As Double
    Property Dvt_MntoExonerado As Double
    Property Dvt_MntoInafecta As Double
    Property Dvt_MntoISC As Double
    Property Dvt_PrcIGV As Double
    Property Dvt_MntoIGV As Double
    Property Dvt_MntoOtr As Double
    Property Dvt_MntoTotal As Double
    Property Dvt_MntoTotalAlterno As Double
    Property Hoy As Date

    Property Dvt_AfectoRegi As String
    '    -- inicio: Datos de regimen especial
    Property PVnt_IGV_Afecto As Boolean

    Property PVnt_IGV_Codigo As String
    Property PVnt_IGV_Codigo_Ofi As String
    Property PVnt_IGV_Descripcion As String

    Property PVnt_IGV_PerIncluyDoc_Venta As Boolean
    Property PVnt_IGV_PerTpDc_Codigo As String
    Property PVnt_IGV_PerTpDc_Codigo_Ofi As String
    Property PVnt_IGV_PerTpDc_Descripcion As String

    Property PVnt_IGV_PerSerie As String
    Property PVnt_IGV_PerNumero As String

    Property PVnt_IGV_Detr_TipoOperaci As String
    Property PVnt_IGV_Detr_TipoOperaci_Ds As String
    Property PVnt_IGV_Detr_TipoProduct As String
    Property PVnt_IGV_Detr_TipoProduct_Ds As String

    Property PVnt_IGV_Detr_Porcentaje As Double
    Property PVnt_IGV_Rete_Porcentaje As Double
    Property PVnt_IGV_Monto_Afecto As Double
    '-- fin: Datos de regimen especial

    Property Desde As Date
    Property Hasta As Date
    '----- Codigo del menu
    Property MyIDMenu As String
    Property MyOrigen As String = ""
    Sub New()
    End Sub
    Sub New(ByVal c_Emp_cCodigo As String, ByVal c_Pan_cAnio As String, ByVal c_Dvt_Codigo As String, ByVal c_Dvt_TipoBSA As String, ByVal c_Pdv_Codigo As String, ByVal c_TpDc_Codigo As String, ByVal c_Dvt_VTSerie As String, ByVal c_Dvt_VTNumer As String, ByVal c_Dvt_VTFecha As Date, ByVal c_Ent_cCodEntidad As String, ByVal c_Ten_cTipoEntidad As String, ByVal c_Dir_Codigo As String, ByVal c_Con_cCondicion As String, ByVal c_Dvt_DiasCondic As Integer, ByVal c_Dvt_FechaVenci As Date, ByVal c_Dvt_Cod_TipoPrc As String, ByVal c_Dvt_Cod_Moneda As String, ByVal c_Dvt_TipCambCod As String, ByVal c_Dvt_TipCambMont As Double, ByVal c_Dvt_VencCoddEnt As String, ByVal c_Dvt_VenCodTipoEnti As String, ByVal c_Dvt_TipoVenta As String, ByVal c_Dvt_EsAnticipada As Boolean, ByVal c_Dvt_TotalSinDesc As Double, ByVal c_Dvt_DescPorcent As Double, ByVal c_Dvt_DescMonto As Double, ByVal c_Dvt_RedondeoApli As Boolean, ByVal c_Dvt_RedondeoMont As Double, ByVal c_Dvt_SubTotal As Double, ByVal c_Dvt_TotInAfect As Double, ByVal c_Dvt_TotExoner As Double, ByVal c_Dvt_TotAfecto As Double, ByVal c_Dvt_ISC_Monto As Double, ByVal c_Dvt_PorcjIGV As Double, ByVal c_Dvt_MontoIGV As Double, ByVal c_Dvt_MontoOtrosCarg As Double, ByVal c_Dvt_MontoTotal As Double, ByVal c_Dvt_MontoIGV_MN As Double, ByVal c_Dvt_MontoTotal_MN As Double, ByVal c_Dvt_Est_Fact As String)
        Emp_cCodigo = c_Emp_cCodigo
        Pan_cAnio = c_Pan_cAnio
        Dvt_Codigo = c_Dvt_Codigo
        Dvt_TipoBSA = c_Dvt_TipoBSA
        Pdv_Codigo = c_Pdv_Codigo
        TpDc_Codigo = c_TpDc_Codigo
        Dvt_VTSerie = c_Dvt_VTSerie
        Dvt_VTNumer = c_Dvt_VTNumer
        Dvt_VTFecha = c_Dvt_VTFecha
        Ent_cCodEntidad = c_Ent_cCodEntidad
        Ten_cTipoEntidad = c_Ten_cTipoEntidad
        Dir_Codigo = c_Dir_Codigo
        Con_cCondicion = c_Con_cCondicion
        Dvt_DiasCondic = c_Dvt_DiasCondic
        Dvt_FechaVenci = c_Dvt_FechaVenci
        Dvt_Cod_TipoPrc = c_Dvt_Cod_TipoPrc
        Dvt_Cod_Moneda = c_Dvt_Cod_Moneda
        Dvt_TipCambCod = c_Dvt_TipCambCod
        Dvt_TipCambMont = c_Dvt_TipCambMont
        Dvt_VencCoddEnt = c_Dvt_VencCoddEnt
        Dvt_VenCodTipoEnti = c_Dvt_VenCodTipoEnti
        'Dvt_TipoVenta = c_Dvt_TipoVenta
        Dvt_EsAnticipada = c_Dvt_EsAnticipada
        'Dvt_TotalSinDesc = c_Dvt_TotalSinDesc
        'Dvt_DescPorcent = c_Dvt_DescPorcent
        'Dvt_DescMonto = c_Dvt_DescMonto
        'Dvt_RedondeoApli = c_Dvt_RedondeoApli
        'Dvt_RedondeoMont = c_Dvt_RedondeoMont
        'Dvt_SubTotal = c_Dvt_SubTotal
        'Dvt_TotInAfect = c_Dvt_TotInAfect
        'Dvt_TotExoner = c_Dvt_TotExoner
        'Dvt_TotAfecto = c_Dvt_TotAfecto
        'Dvt_ISC_Monto = c_Dvt_ISC_Monto
        'Dvt_PorcjIGV = c_Dvt_PorcjIGV
        'Dvt_MontoIGV = c_Dvt_MontoIGV
        'Dvt_MontoOtrosCarg = c_Dvt_MontoOtrosCarg
        'Dvt_MontoTotal = c_Dvt_MontoTotal
        'Dvt_MontoIGV_MN = c_Dvt_MontoIGV_MN
        'Dvt_MontoTotal_MN = c_Dvt_MontoTotal_MN
        Dvt_Est_Fact = c_Dvt_Est_Fact



    End Sub
    'REPORTE
    Property RptFecha As String
    Property RptEntidad As String
    Property RpteFechaInicio As Date
    Property RpteFechaFin As Date
    Property RptePuntoVenta As String
    Property RpteTipoCentroCG As String
    Property RpteCentroCG_Codigo As String
    Property RpteUnidad_Codigo As String

    Property RptVntD_NumItem As Int16
    Property RptCTC_Codigo As String
    Property RptCTC_DescripCorta As String
    Property RptCTC_Serie As String
    Property RptCTC_DescripcionItem As String
    Property RptCTC_CodUnidad As String
    Property RptUNM_Descripcion As String
    Property RptVntD_Cantidad As Decimal
    Property RptVntD_ValorVenta As Decimal
    Property RptVntD_PDstoUnitari As Decimal
    Property RptVntD_PDstoGeneral As Decimal
    Property RptVntD_IGVTasa As Decimal
    Property RptVntD_IGVMonto As Decimal
    Property RptVntD_PrecNeto As Decimal
    Property RptVntD_Importe As Decimal
    Property RptVntD_Operaci As String
    Property RptTAD_CODIGO_OFICIAL1 As String
    Property RptTAD_DESCRIPCION1 As String
    Property RptAlm_Codigo As String
    Property RptALM_Descripcion As String
    Property RptVntD_TipoBSA As String
    Property RptTAD_CODIGO_OFICIAL2 As String
    Property RptTAD_DESCRIPCION2 As String
    Property RptVntD_TipoCentro As String
    Property RptTAD_CODIGO_OFICIAL As String
    Property RptTAD_DESCRIPCION As String
    Property RptVntD_CodigoCentro As String
    Property RptCcg_cDescripcion As String
    Property RptVntD_UnidadMaquinaria As String
    Property RptINV_TipoUMINV_Serie As String
    Property RptUNM_CodigoSunat As String
    Property RptImpTotal As Decimal
    Property RptOK As Boolean = 0

    ''''''''''''''''''''''''

    Property CodigoCaja As String
    Property Cja_Codigo_Item As Integer
    Property Cja_Codigo_Apertura As Integer
    Property EstadoCaja As String
    Property MontoEntregado As Decimal
    Property MontoDevolver As String
    Property TpDc_Codigo_Asociada As String
    Property Correlativo_Linea_Credito As String

    Property Vnt_IsAperturado As Boolean
    Property Vnt_placa_Vehiculo As String
    Property Caja_Estado As String
    Property Serie_Ref As String
    Property Numero_Ref As String
    Property Vnt_IsVenta_Automatica As Boolean
    Property Vnt_IsVenta_Cuadre As Boolean
    'TRAER PARA ELECTRONICO

    Property Vnt_Cod_Autorizacion_Sunat As String
    Property Vnt_Es_Electronico As Boolean
    Property Vnt_Modelo_Impresion As String
    Property Vnt_EstadoEnvio As String

    Property SoloVenta As Boolean = False
    Property IncluirAniosAnteriores As Boolean = False
    Property Dvt_Abrev_Moneda As String
End Class
