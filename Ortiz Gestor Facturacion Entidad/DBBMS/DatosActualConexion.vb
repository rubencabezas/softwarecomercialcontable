﻿Public Class DatosActualConexion
    'Empresa
    'Public Shared OrigenPresupuesto As String = "47"
    'Public Shared OrigenHojaRequerimiento As String = "48"
    'Public Shared OrigenHojaProduccion As String = "49"

    'Public Shared EmpresaNombre As String
    Public Shared EmpresaRuc As String
    Public Shared EmpresaRazonSocial As String
    Public Shared CodigoEmpresa As String = "001"
    Public Shared DireccionEmpresa As String

    Public Shared Emp_DireccionCorta As String
    Public Shared Emp_Urbanizacion As String
    Public Shared Emp_Direccion_Departamento As String
    Public Shared Emp_Direccion_Provincia As String
    Public Shared Emp_Direccion_Distrito As String

    'Año
    Public Shared AnioSelect As Integer
    'Periodo
    Public Shared PeriodoSelect As String

    'Usuario
    Public Shared UserDNI As String = 1
    Public Shared UserAlias As String
    Public Shared UserName As String
    Public Shared CodEnt As String
    Public Shared Codigo_Autorizacion_Sunat As String

    Public Shared MachineCurrent As String = System.Environment.MachineName
    Public Shared ModuloReg As String = "0012"
    Public Shared Modulo As String = My.Application.Info.Title
    Public Shared VersionAplication As String = String.Format("Versión {0}", My.Application.Info.Version.ToString)
    Public Shared SOCurrent As String = My.Computer.Info.OSFullName
    Public Shared MyServer As String = ""
    'Aqui esta el cn q servirá para las consultas
    Public Shared CnSelect As String

    Public Shared ServerIp As String

    'Public Shared OrigenCompraCod As String = "01"
    'Public Shared OrigenCostoVinculado As String = "02"
    Public Shared OrigenCompraCod As String = "01"
    Public Shared OrigenCostoVinculado As String = "02"
    Public Shared OrigenNotaCredito As String = "22"
    Public Shared OrigenNotaDebito As String = "04"
    Public Shared ImagenesLocalizacion As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\LogoStore3.png"

    Public Shared CodigoBaseImponible = "0565"
    Public Shared IgvActual As Decimal

    Public Shared HabilitarTodasEstaciones As Boolean = False
    Public Shared ImpresionContinua As Boolean = True

    Public Shared Mensaje As String
End Class
