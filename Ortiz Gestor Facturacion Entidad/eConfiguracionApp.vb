﻿Public Class eConfiguracionApp
    Property App_Replicacion As Boolean
    Property App_Replicacion_Servidor As String
    Property App_Replicacion_BD As String
    Property App_Replicacion_Usuario As String
    Property App_Replicacion_Contrasenia As String
    Property App_Replicacion_Prog_Hora_Establecido As Boolean
    Property App_Replicacion_Prog_Hora_CadaRangoHoras As Boolean
    Property App_Replicacion_Prog_Hora As Integer
    Property App_Replicacion_Prog_Hora_Desde As TimeSpan
    Property App_Replicacion_Prog_Hora_Hasta As TimeSpan
    ReadOnly Property Progra_FrecuEnvio_Hora_Desde_12 As String
        Get
            Return New DateTime(App_Replicacion_Prog_Hora_Desde.Ticks).ToString("hh:mm:ss tt")
        End Get
    End Property
    ReadOnly Property Progra_FrecuEnvio_Hora_Hasta_12 As String
        Get
            Return New DateTime(App_Replicacion_Prog_Hora_Hasta.Ticks).ToString("hh:mm:ss tt")
        End Get
    End Property
    Property App_Replicacion_RutaPrincipalArchivos As String
    Property App_Es_Hora_Replicar As Boolean
End Class
