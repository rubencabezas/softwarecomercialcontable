﻿Imports Microsoft.VisualBasic.CompilerServices
Imports Microsoft.VisualBasic.FileIO


Public Class log
    ' Methods
    Private Sub abrirArchivo()
        Me.nombreLog = String.Concat(New String() {Me.path, Me.carpetaLog, Me.nombre, "000", Me.extension})
        If Me.archivoExiste(Me.nombreLog) Then
            Me.swEscritor = New IO.StreamWriter(Me.nombreLog, True)
        Else
            Me.swEscritor = New IO.StreamWriter(Me.nombreLog)
        End If
    End Sub

    Private Function archivoExiste(ByVal nombre As String) As Boolean
        Return IO.File.Exists(nombre)
    End Function

    Private Sub crearDirectorioLog()
        If Not Me.directorioExiste((Me.path & Me.carpetaLog)) Then
            IO.Directory.CreateDirectory((Me.path & Me.carpetaLog))
        End If
    End Sub

    Public Sub datosInicio(ByVal nombre As String, ByVal path As String)
        Me.path = path
        Me.nombre = nombre
        Me.crearDirectorioLog()
        Me.leerConfiguraciones()
        Me.abrirArchivo()
    End Sub

    Private Function directorioExiste(ByVal nombre As String) As Boolean
        Return IO.Directory.Exists(nombre)
    End Function

    Private Sub elimininarArchivo(ByVal nombre As String)
        Try
            My.Computer.FileSystem.DeleteFile(nombre)
        Catch exception1 As Exception
            ProjectData.SetProjectError(exception1)
            Dim exception As Exception = exception1
            Interaction.MsgBox(exception.Message.ToString, MsgBoxStyle.Critical, Nothing)
            ProjectData.ClearProjectError()
        End Try
    End Sub

    Public Sub escribirLinea(ByVal tipo As tipoLogEnum, ByVal donde As String, ByVal info As String)
        If (tipo > Me.logdesde) Then
            Dim type As Type = Me.GetType
            'Monitor.Enter(type)
            Try
                Dim str As String = String.Concat(New String() {Conversions.ToString(DateTime.Now), Me.sep, tipo.ToString, Me.sep, donde, Me.sep, info})
                Me.swEscritor.WriteLine(str)
                Me.swEscritor.Flush()
                Dim info2 As New IO.FileInfo(Me.nombreLog)
                If (info2.Length > (Me.tamaño * &H400)) Then
                    Me.swEscritor.Close()
                    Me.renombrarArchivos()
                    Me.abrirArchivo()
                End If
            Catch exception1 As Exception
                ProjectData.SetProjectError(exception1)
                Dim exception As Exception = exception1
                Interaction.MsgBox(exception.Message.ToString, MsgBoxStyle.Critical, Nothing)
                ProjectData.ClearProjectError()
            Finally
                'Monitor.Exit(type)
            End Try
        End If
    End Sub

    Private Sub leerConfiguraciones()
        Try
            Dim reader As New IO.StreamReader((Me.path & "\loginfo.ini"))
            Me.tamaño = Conversions.ToShort(reader.ReadLine)
            Me.cantArch = Conversions.ToShort(reader.ReadLine)
            Me.logdesde = Conversions.ToShort(reader.ReadLine)
            reader.Close()
            If (Me.cantArch > &H3E8) Then
                Me.cantArch = &H3E7
            End If
        Catch exception1 As Exception
            ProjectData.SetProjectError(exception1)
            Dim exception As Exception = exception1
            Me.tamaño = 100
            Me.cantArch = 10
            Me.logdesde = 0 'Todos los log se activa, cuando no se encuentra el configurador
            ProjectData.ClearProjectError()
        End Try
    End Sub

    Private Sub listarArchivos()
        Try
            Dim enumerator As IEnumerator(Of String)
            Me.htArchivos.Clear()
            Try
                enumerator = My.Computer.FileSystem.GetFiles((Me.path & Me.carpetaLog), SearchOption.SearchTopLevelOnly, New String() {("*" & Me.extension)}).GetEnumerator
                Do While enumerator.MoveNext
                    Dim current As String = enumerator.Current
                    Dim str As String = current.Substring((Me.path.Length + Me.carpetaLog.Length), ((Me.nombre.Length + 3) + Me.extension.Length))
                    Dim length As Short = CShort(Me.nombre.Length)
                    Dim key As Short = Conversions.ToShort(str.Substring(length, 3))
                    Me.htArchivos.Add(key, current)
                Loop
            Finally
                If (Not enumerator Is Nothing) Then
                    enumerator.Dispose()
                End If
            End Try
        Catch exception1 As Exception
            ProjectData.SetProjectError(exception1)
            Dim exception As Exception = exception1
            Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, Nothing)
            ProjectData.ClearProjectError()
        End Try
    End Sub

    Private Sub renombrarArchivos()
        Try
            Me.listarArchivos()
            Dim cantArch As Short = Me.cantArch
            If Conversions.ToBoolean(Operators.NotObject(Operators.CompareObjectEqual(Me.htArchivos.Item(cantArch), "", False))) Then
                My.Computer.FileSystem.DeleteFile(Conversions.ToString(Me.htArchivos.Item(cantArch)))
            End If
            Dim i As Short = Me.cantArch
            Do While (i >= 1)
                cantArch = CShort((i - 1))
                If Conversions.ToBoolean(Operators.NotObject(Operators.CompareObjectEqual(Me.htArchivos.Item(cantArch), "", False))) Then
                    Dim str As String
                    Dim num3 As Short = i
                    If (num3 < 10) Then
                        str = ("00" & Conversions.ToString(CInt(i)))
                    ElseIf (num3 < 100) Then
                        str = ("0" & Conversions.ToString(CInt(i)))
                    Else
                        str = Conversions.ToString(CInt(i))
                    End If
                    Dim file As String = Conversions.ToString(Me.htArchivos.Item(cantArch))
                    Dim newName As String = (Me.nombre & str & Me.extension)
                    newName = newName.Substring(1, (newName.Length - 1))
                    My.Computer.FileSystem.RenameFile(file, newName)
                End If
                i = CShort((i + -1))
            Loop
        Catch exception1 As Exception
            ProjectData.SetProjectError(exception1)
            Dim exception As Exception = exception1
            Interaction.MsgBox(exception.Message.ToString, MsgBoxStyle.Critical, Nothing)
            ProjectData.ClearProjectError()
        End Try
    End Sub


    ' Properties
    Public Shared ReadOnly Property Instancia As log
        Get
            If (log._instancia Is Nothing) Then
                log._instancia = New log
            End If
            Return log._instancia
        End Get
    End Property


    ' Fields
    Private Shared _instancia As log
    Private cantArch As Short
    Private carpetaLog As String = "\log"
    Private extension As String = ".log"
    Private htArchivos As Hashtable = New Hashtable
    Private logdesde As Short
    Private nombre As String
    Private nombreLog As String
    Private path As String
    Private sep As String = " | "
    Private swEscritor As IO.StreamWriter
    Private tamaño As Short

    ' Nested Types
    Public Enum tipoLogEnum
        ' Fields
        Debug_ = 1
        Info__ = 2
        Warn__ = 3
        Error_ = 4
        Fatal_ = 5
        Prog__ = 6
    End Enum
End Class
