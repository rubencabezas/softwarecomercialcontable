﻿Public Class BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO
    Property Doc_Emisor_RUC As String
    Property Doc_Tipo As String
    Property Doc_Serie As String
    Property Doc_Numero As String
    Property XML_Archivo_Item As Integer
    Property XML_Archivo_NombreLocal As String
    Property XML_Archivo_Nombre As String
    Property Aud_Registro_FechaHora As Date
    Property Aud_Registro_Equipo As String
    Property Aud_Registro_Usuario As String
    Property Path_Root_App As String 'Para saber donde estan
    ReadOnly Property Path_Root_App_Empresa As String
        Get
            Return Path_Root_App & "\" & Doc_Emisor_RUC
        End Get
    End Property
End Class
