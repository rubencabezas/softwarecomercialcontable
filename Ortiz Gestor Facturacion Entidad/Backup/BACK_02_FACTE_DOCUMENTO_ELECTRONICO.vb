﻿Public Class BACK_02_FACTE_DOCUMENTO_ELECTRONICO
    Property Doc_Emisor_RUC As String
    Property Doc_Tipo As String
    Property Doc_Serie As String
    Property Doc_Numero As String
    Property Doc_FechaEmision As Date
    Property Doc_Moneda As String
    Property Doc_Oper_Gravadas As Double
    Property Doc_Oper_Exoneradas As Double
    Property Doc_Oper_Inafectas As Double
    Property Doc_Oper_Gratuitas As Double
    Property Doc_ISC As Double
    Property Doc_Otros_Tributos As Double
    Property Doc_Otros_Conceptos As Double
    Property Doc_IGV As Double
    Property Doc_Importe As Double
    Property Doc_Certificado_Correlativo As Integer
    Property Doc_Cliente_TipoDocIdent_Codigo As String
    Property Doc_Cliente_TipoDocIdent_Numero As String
    Property Doc_Modif_Doc_Tipo As String
    Property Doc_Modif_Doc_Serie As String
    Property Doc_Modif_Doc_Numero As String
    Property Doc_NombreArchivoXML As String
    Property Doc_NombreArchivoPDF As String
    Property Doc_NombreArchivoWeb As String
    Property Doc_Resumen_FechaGeneracion As Date
    Property Doc_Resumen_Correlativo As Integer
    Property Doc_Baja_GrpTipo_Codigo As String
    Property Doc_Baja_FechaGeneracion As Date
    Property Doc_Baja_Correlativo As Integer
    Property Doc_Correo_Enviado As Boolean
    Property Doc_Correo_EstaLeido As Boolean
    Property Doc_Correo_ContadorLectura As Integer
    Property Doc_Correo_LecturaFechaPrimera As Date
    Property Doc_Correo_LecturaFechaUltima As Date
    Property EstDoc_Codigo As String
    Property Path_Root_App As String 'Para saber donde estan
    ReadOnly Property Path_Root_App_Empresa As String
        Get
            Return Path_Root_App & "\" & Doc_Emisor_RUC
        End Get
    End Property
    Property EstEnv_Codigo As String
End Class
