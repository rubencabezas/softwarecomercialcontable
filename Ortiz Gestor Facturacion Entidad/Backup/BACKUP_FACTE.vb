﻿Public Class BACKUP_FACTE
    <Xml.Serialization.XmlArrayItem("Socios")> _
    Property SociosNegocio() As BACK_01_FACTE_SOCIO_NEGOCIO()
    <Xml.Serialization.XmlArrayItem("Documentos")> _
    Property Documentos() As BACK_02_FACTE_DOCUMENTO_ELECTRONICO()
    <Xml.Serialization.XmlArrayItem("Adjuntos")> _
    Property DocumentosAdjuntos() As BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO()
    <Xml.Serialization.XmlArrayItem("ArchivoOrigen")> _
    Property DocumentosXMLCliente() As BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE()
End Class
