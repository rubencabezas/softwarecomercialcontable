﻿Public Class BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
    Property Doc_Emisor_RUC As String
    Property Doc_Tipo As String
    Property Doc_Serie As String
    Property Doc_Numero As String
    Property XML_Correlativo As Integer
    Property XML_Reciente As Boolean
    Property XML_Archivo As String
    Property TipXML_Codigo As String
    Property EstEnv_Codigo As String
    Property XML_DigestValue As String
    Property XML_SignatureValue As String
    Property SUNAT_Respuesta_Codigo As String
    Property XML_MotivoBaja As String
    Property XML_FilaActiva As Boolean
    Property XML_EnviadoFTP As Boolean
    Property XML_Revercion_Anulacion As Boolean
    Property XML_Revercion_Anulacion_FechaHora As Date
    Property XML_Revercion_Anulacion_Razon As String
    Property XML_Creado_FechaHora As Date
    Property XML_Enviado_FechaHora As Date
    Property Aud_Registro_FechaHora As Date
    Property Aud_Registro_Equipo As String
    Property Aud_Registro_Usuario As String
    Property Path_Root_App As String 'Para saber donde estan
    ReadOnly Property Path_Root_App_Empresa As String
        Get
            Return Path_Root_App & "\" & Doc_Emisor_RUC
        End Get
    End Property
End Class
