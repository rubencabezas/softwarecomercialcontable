﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Entidad_Reporte_Libro_Diario
    {
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Id_Libro { get; set; }
        public Boolean Acumulado { get; set; }
        public string Correlativo { get; set; }
        public DateTime FechaOperacion { get; set; }
        public string NumMov { get; set; }
        public string Glosa { get; set; }
        public string Lib_Codigo { get; set; }
        public string Lib_Sunat { get; set; }
        public string Voucher { get; set; }
        public string TipoDoc { get; set; }
        public string DocSunat { get; set; }
        public string SerieDoc { get; set; }
        public string NumeroDoc { get; set; }
        public string Cuenta { get; set; }
        public string Descripcion { get; set; }
        public decimal DebeNacional { get; set; }
        public decimal HaberNacional { get; set; }
        public Boolean Cta_Destino { get; set; }
        public string TipoDebeHaber { get; set; }

        public decimal XSaldoDebe { get; set; }
        public decimal XSaldoHaber { get; set; }
        public string VoucherVentas { get; set; }
        public string VoucherCompras { get; set; }
        public string VoucherConsignacion { get; set; }

        public string ple_V5 { get; set; }
        public string ple_V5_cuentas { get; set; }

        public int Nivel { get; set; }





        public string periodo_V5 { get; set; }
        public string cuo_V5 { get; set; }
        public string correlativo_V5 { get; set; }
        public string cod_plan_cuentas_V5 { get; set; }
        public string cuenta_ctb_V5 { get; set; }
        public string cod_unidad_operacion_V5 { get; set; }
        public string cod_centro_costos_V5 { get; set; }
        public string tipo_moneda_V5 { get; set; }
        public string tipo_doc_emisor_V5 { get; set; }
        public string numero_doc_emisor_V5 { get; set; }
        public string tipo_Cmoprobante_pago_V5 { get; set; }
        public string serie_V5 { get; set; }
        public string numero_V5 { get; set; }
        public string fecha_contable_V5 { get; set; }
        public string fecha_vencimiento_V5 { get; set; }
        public string fecha_operacion_V5 { get; set; }
        public string glosa_V5 { get; set; }
        public string glosa_referencial_V5 { get; set; }
        public string debe_V5 { get; set; }
        public string haber_V5 { get; set; }
        public string dato_estructurado_V5 { get; set; }
        public string estado_V5 { get; set; }

        public string Cta_Descripcion { get; set; }
     public string desc_Plan_cuenta { get; set; }

    }
}
