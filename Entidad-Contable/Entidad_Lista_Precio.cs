﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
  public  class Entidad_Lista_Precio
    {
        
        public string Id_Empresa { get; set; }
        public string Est_Codigo { get; set; }
        public string Est_Descripcion { get; set; }
        public string Id_Tipo { get; set; }
        public string Id_Tipo_Interno { get; set; }
        public string Id_Tipo_Descripcion { get; set; }
        public string Id_Catalogo { get; set; }
        public string Id_Catalogo_Desc { get; set; }




        //detalles

        public string Mensaje { get; set; }

        public Int32 Pre_Correltivo { get; set; }

        public string Id_Unidad_Medida { get; set; }
        public string Und_Descripcion { get; set; }
        public string Und_Abreviado { get; set; }

        public string Pre_Id_Mod_Venta { get; set; }
        public string Pre_Mod_Venta { get; set; }
        public string Pre_Mod_Venta_Desc { get; set; }

        public string Pre_Moneda_cod { get; set; }
        public string Pre_Moneda_Sunat { get; set; }
        public string Pre_Moneda_Desc { get; set; }

        public DateTime Pre_Fecha_Ini { get; set; }
        public DateTime Pre_Fecha_Fin { get; set; }
        public decimal Pre_Precio_Minimo { get; set; }
        public decimal Pre_Precio_Maximo { get; set; }
        public bool Es_Precio_Variable { get; set; }

        public bool Pre_Requiere_Codigo_Autoriza { get; set; }
        public string Pre_Codigo_Autoriza { get; set; }

        public List<Entidad_Lista_Precio> Detalles_Precios { get; set; }

        /// <summary>
        /// //
        /// </summary>
        public string Pre_Tipo { get; set; }
        public string Pre_Tipo_Interno { get; set; }
        public string Pre_Tipo_Desc { get; set; }




        public string Pre_Id_Unm { get; set; }
        public string Pre_Unm_Desc { get; set; }


        public List<Entidad_Lista_Precio> PrecioPrincipal { get; set; }

        public Int32 Cant_Precio_Posterior { get; set; }

        public string marca { get; set; }
        public string fabricante { get; set; }

        public string Respuesta { get; set; }

    }
}
