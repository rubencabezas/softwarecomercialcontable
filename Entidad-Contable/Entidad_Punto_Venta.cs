﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
  public  class Entidad_Punto_Venta
    {
        // ,
        // ,
        // ,
        // ,
        // ,
        // ,
        // ,
        //,

        public string Id_Empresa { get; set; }
        public string Pdv_Codigo { get; set; }
        public string Est_Codigo { get; set; }
        public string Est_Descripcion { get; set; }
        public string Est_Direccion { get; set; }
        public string Pdv_Nombre { get; set; }
        public string Pdv_Impresora { get; set; }
        public string Pdv_Nombre_PC { get; set; }
        public string Pdv_Num_Maquina_Registradora { get; set; }
        public string Pdv_Almacen { get; set; }
        public string Alm_Descrcipcion { get; set; }


        public string FechaActual_Servidor { get; set; }
        public string HoraActual_Servidor { get; set; }
    }
}
