﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
  public  class Entidad_Serie_Comprobante
    {


        public string Id_Empresa { get; set; }
        public string Ser_Tipo_Doc { get; set; }
        public string Id_Sunat { get; set; }
        public string Nombre_Comprobante { get; set; }
        public string Ser_Serie { get; set; }
        public string Ser_Numero_Ini { get; set; }
        public string Ser_Numero_Fin { get; set; }
        public string Ser_Numero_Actual { get; set; }
        public string Ser_Pdv_Codigo { get; set; }
        public string Pdv_Nombre { get; set; }
        public DateTime Ser_Fecha_Impresion { get; set; }
        public DateTime Ser_Fecha_Baja { get; set; }
public string Ser_Estado_Baja { get; set; }
        public string Ser_Estado_Baja_Desc { get; set; }
 public string Ser_Cod_Autorizacion_SUNAT { get; set; }
        public Int32 Ser_Max_Lineas { get; set; }

        public bool Es_Electronico { get; set; }

    }
}
