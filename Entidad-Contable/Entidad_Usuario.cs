﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
   public class Entidad_Usuario
    {
     
        public string Usuario_dni { get; set; }
        public string Usuario_Entidad { get; set; }
        public string Usuario_Entidad_Desc { get; set; }
        public string Usuario_Pass { get; set; }
        public string Usuario_Confirmar_Pass { get; set; }
        public string Usuario_Rol_Id { get; set; }
    }
}
