﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
public    class Entidad_Centro_Costo_Gasto
    {

        public string Id_Empresa { get; set; }
        public string Id_Centro_cg  { get; set; }
        public string Id_Unidad { get; set; }
        public string Und_Descripcion { get; set; }
        public string Ccg_Tipo_cg  { get; set; }
        public string Gen_Codigo_Interno { get; set; }
        public string Gen_Descripcion_Det { get; set; }
        public string Ccg_Descripcion { get; set; }
    }
}
