﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Entidad_Movimiento_Cab
    {
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Id_Periodo_Desc { get; set; }
        public string Id_Libro { get; set; }
        public string Id_Libro_Nombre { get; set; }
        public string Id_Libro_Sunat { get; set; }
        public string Id_Voucher { get; set; }
        public string Ctb_Glosa { get; set; }

        public string Ven_Motivo_Cod { get; set; }
        public string Ven_Motivo_Cod_Interno { get; set; }
        public string Ven_Motivo_Desc { get; set; }

        public string Ctb_Tipo_Doc_Sunat { get; set; }
        public string Ctb_Tipo_Doc { get; set; }
         public string Nombre_Comprobante { get; set; }
        public string Ctb_Serie { get; set; }
        public string Ctb_Numero { get; set; }
        public DateTime Ctb_Fecha_Movimiento { get; set; }
        public string Ctb_Tipo_Ent { get; set; }
        public string Ctb_Tipo_Ent_Desc { get; set; }
        public string Ctb_Ruc_dni { get; set; }
        public string Entidad { get; set; }

        public string Ctb_Condicion_Cod { get; set; }
        public string Ctb_Condicion_Interno { get; set; }
        public string Ctb_Condicion_Desc { get; set; }

        public string Est_Facturacion { get; set; }
        public string Est_Facturacion_Interno { get; set; }
        public string Est_Facturacion_Desc { get; set; }



        public string Ctn_Moneda_Sunat { get; set; }
        public string Ctn_Moneda_Cod { get; set; }
        
        public bool Ctb_Es_moneda_nac { get; set; }
        public string Ctn_Moneda_Desc { get; set; }
        public string Ctb_Tipo_Cambio_Cod { get; set; }
        public string Ctb_Tipo_Cambio_desc { get; set; }
        public decimal Ctb_Tipo_Cambio_Valor { get; set; }

        public decimal Ctb_Base_Imponible { get; set; }

        public decimal Ctb_Base_Imponible2 { get; set; }
        public decimal Amortizado { get; set; }

        public decimal Ctb_Base_Imponible3 { get; set; }
        public decimal Ctb_Igv { get; set; }
        public decimal Ctb_Igv2 { get; set; }
        public decimal Ctb_Igv3 { get; set; }
        public decimal Ctb_Importe_Total { get; set; }
        public Boolean Ctb_Isc { get; set; }
        public decimal Ctb_Isc_Importe { get; set; }
        public Boolean Ctb_Otros_Tributos { get; set; }
        public decimal Ctb_Otros_Tributos_Importe { get; set; }
        public decimal Ctb_No_Gravadas { get; set; }
        public decimal Ctb_Valor_Fact_Exonerada { get; set; }
        public decimal Ctb_Exonerada { get; set; }
        public decimal Ctb_Inafecta { get; set; }

        public bool Ctb_Es_Compra_Comercial { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }
        public Boolean Modif { get; set; }
        public List<Entidad_Movimiento_Cab> DetalleAsiento { get; set; }
        public List<Entidad_Movimiento_Cab> DetalleADM { get; set; }
        public List<Entidad_Movimiento_Cab> DetalleDoc_Ref { get; set; } 
       public List<Entidad_Movimiento_Cab> DetalleDoc_Orden_CS { get; set; } 

        public string Id_Anio_Ref { get; set; }
        public string Id_Periodo_Ref { get; set; }
        public string Id_Libro_Ref { get; set; }
        public string Id_Voucher_Ref { get; set; }

        public string Ven_Afecto_IGV_FE { get; set; }
        public string Ven_Afecto_IGV_FE_Desc { get; set; }




        public Boolean Ctb_Es_Dif_Cambio { get; set; }
        public decimal Ctb_Importe_Dif_Cambio { get; set; }
        public Boolean Ctb_Es_Redondeo { get; set; }
        public decimal Ctb_Importe_Redondeo { get; set; }
        public decimal Ctb_Importe_Diferencias { get; set; }


        public int Id_Item_Ref { get; set; }
        public Boolean Ctb_Es_Cuenta_Principal { get; set; }


        public Boolean Ctb_Afecto_RE { get; set; }
        public string Ctb_Afecto_Tipo { get; set; }
        public string Ctb_Afecto_Tipo_cod_Interno { get; set; }
        public string Ctb_Afecto_Tipo_Descripcion { get; set; }
        public string Ctb_Afecto_Tipo_Doc { get; set; }
        public string Ctb_Afecto_Tipo_Doc_DEsc { get; set; }
        public string Ctb_Afecto_Serie { get; set; }
        public string Ctb_Afecto_Numero { get; set; }
        public decimal Ctb_Afecto_Porcentaje { get; set; }
        public decimal Ctb_Afecto_Monto { get; set; }
        public DateTime Ctb_Afecto_Fecha { get; set; }
        public DateTime Ctb_Fecha_Vencimiento { get; set; }

        public decimal Ctb_Tasa_IGV { get; set; }
        public string Estado { get; set; }
        public string Estado_Fila { get; set; }
        public string Ven_FactElectronica_Estado { get; set; }
        public string Ven_FactElectronica_Estado_Desc { get; set; }
        public string XML_DigestValue { get; set; }
        public string XML_SignatureValue { get; set; }

        public string Ctb_Analisis { get; set; }
        public string Ctb_Analisis_Desc { get; set; }
        public string Ord_tipo { get; set; }
        public string Ord_Folio { get; set; }
        public string Ord_Origen { get; set; }
        //DETALLES

        public int Id_Item { get; set; }
        public string Ctb_Cuenta { get; set; }
        public string Ctb_Cuenta_Desc { get; set; }

        public string Ctb_Operacion_Cod { get; set; } = null;
        public string Ctb_Operacion_Cod_Interno { get; set; }
        public string Ctb_Operacion_Desc { get; set; }


        //public string Ctb_Igv_Cod { get; set; }
        public string Ctb_Centro_CG { get; set; }
        public string Ctb_Centro_CG_Desc { get; set; }

        public string Ctb_Medio_Pago_Det { get; set; }
        public string Ctb_Medio_Pago_Det_Desc { get; set; }
        public string Ctb_Tipo_DH { get; set; }
        public string CCtb_Tipo_DH_Interno { get; set; }
        public string Ctb_Tipo_DH_Desc { get; set; }

        public decimal Ctb_Importe_Debe { get; set; }
        public decimal Ctb_Importe_Haber { get; set; }
       public decimal Ctb_Importe_Debe_Extr { get; set; }
        public decimal Ctb_Importe_Haber_Extr { get; set; }
        public string Ctb_Tipo_BSA { get; set; } 
        public string Ctb_Tipo_BSA_Interno { get; set; }
        public string Ctb_Tipo_BSA_Desc { get; set; }

        public string Ctb_Catalogo { get; set; }
        public string Ctb_Catalogo_Desc { get; set; }

        public string Ctb_Almacen_cod { get; set; }

        public string Ctb_Almacen_desc { get; set; }

        public decimal Ctb_Valor_Unit { get; set; }
        public decimal Ctb_Cantidad { get; set; }

        public string Ctb_Tipo_Doc_det_Sunat { get; set; }
        public string Ctb_Tipo_Doc_det { get; set; }
        public string Ctb_Tipo_Doc_det_desc { get; set; }
      
        public string Ctb_Serie_det { get; set; }
        public string Ctb_Numero_det { get; set; }
        public DateTime Ctb_Fecha_Mov_det { get; set; }
        public string Ctb_Tipo_Ent_det { get; set; }
        public string Ctb_Tipo_Ent_det_Desc { get; set; }
        public string Ctb_Ruc_dni_det { get; set; }
        public string Entidad_det { get; set; }

        public string Ctb_moneda_cod_det_Sunat { get; set; }
        public string Ctb_moneda_cod_det { get; set; }
        public string Ctb_moneda_det_desc { get; set; }


        public string Ctb_Tipo_Cambio_Cod_Det { get; set; }
        public string Ctb_Tipo_Cambio_Desc_Det { get; set; }
        public decimal Ctb_Tipo_Cambio_Valor_Det { get; set; }
        public Boolean Cta_Destino { get; set; }

        //DETALLES ADMINISTRATIVO

        public string Adm_Unm_Id { get; set; }
        public string Adm_Unm_Desc { get; set; }
        public int Adm_Item { get; set; }
        public string Adm_Centro_CG { get; set; }
        public string Adm_Centro_CG_Desc { get; set; }
        public string Adm_Tipo_BSA { get; set; }
        public string Adm_Tipo_BSA_Interno { get; set; }
        public string Adm_Tipo_BSA_Desc { get; set; }
        public string Adm_Catalogo { get; set; }
        public string Adm_Catalogo_Desc { get; set; }
        public string Adm_Almacen { get; set; }
        public string Adm_Almacen_desc { get; set; }
        public decimal Adm_Cantidad { get; set; }
        public decimal Adm_CantidadAtendidaEnBaseAlCoefciente { get; set; }
        public decimal Adm_Valor_Unit { get; set; }
        public decimal Adm_Total { get; set; }
        public decimal Adm_Valor_Venta { get; set; }
        public string Adm_Unm_Abrev { get; set; }
        public string Adm_Marca { get; set; }
        public string Adm_Marca_Desc { get; set; }
        public string UNM_CoigodFE { get; set; }
        public decimal Unm_Base { get; set; }

        ////TESORERIA
        public decimal Ctb_Importe_Amortizado { get; set; }
        public decimal Ctb_Importe_Saldo { get; set; }

        //     , , , ,
        // ,	 ,
        // ,
        // ,
        // ,
        public string Ctb_Tipo_IE { get; set; }
        public string Ctb_Tipo_IE_Interno { get; set; }
        public string Ctb_Tipo_IE_Desc { get; set; }
        public string Ctb_Tipo_Movimiento { get; set; }
        public string Ctb_Tipo_Movimiento_Interno { get; set; }
        public string Ctb_Tipo_Movimiento_Desc { get; set; }

        public string Ctb_Medio_Pago { get; set; }
        public string Ctb_Medio_Pago_desc { get; set; }
        public string Ctb_Numero_Transaccion { get; set; }
        public string Ctb_Cuenta_Corriente { get; set; }
        public string Ctb_Entidad_Financiera { get; set; }
        public string Ctb_Entidad_Financiera_Desc { get; set; }
        //public decimal Ctb_Importe_MN { get; set; }
        public decimal Ctb_Importe { get; set; }
        public decimal Ctb_Importe_Rendido { get; set; }
        public decimal Ctb_Importe_Rendido_Saldo { get; set; }
        public decimal Ctb_Importe_MN
        {
            get
            {
                if ((Ctb_Es_moneda_nac == true))
                {
                    return Ctb_Importe;
                }
                else
                {
                    return (Ctb_Importe * Ctb_Tipo_Cambio_Valor);
                }

            }
        }

        public decimal Saldo_Por_Amortizar
        {
            get
            {
                       return Ctb_Importe_Total - Amortizado; 
            }

    
        }

        public bool Ctb_Es_moneda_nac_det { get; set; }
        public decimal Ctb_Importe_Det_MN_Aper_Caja_chica
        {
            get
            {
                if ((Ctb_Es_moneda_nac_det == true))
                {
                    return Ctb_Importe_Haber;
                }
                else
                {
                    return (Ctb_Importe_Haber * Ctb_Tipo_Cambio_Valor_Det);
                }

            }
        }

        public decimal Ctb_Importe_Det_MN_Rendicion
        {
            get
            {
                if ((Ctb_Es_moneda_nac_det == true))
                {
                    return Ctb_Importe_Debe;
                }
                else
                {
                    return (Ctb_Importe_Debe * Ctb_Tipo_Cambio_Valor_Det);
                }

            }
        }

        public decimal Ctb_Importe_Det_MN_Reembolso
        {
            get
            {
                if ((Ctb_Es_moneda_nac_det == true))
                {
                    return Ctb_Importe_Haber;
                }
                else
                {
                    return (Ctb_Importe_Haber * Ctb_Tipo_Cambio_Valor_Det);
                }

            }
        }

        public decimal Ctb_Importe_Det_MN_Cierre
        {
            get
            {
                if ((Ctb_Es_moneda_nac_det == true))
                {
                    return Ctb_Importe_Debe;
                }
                else
                {
                    return (Ctb_Importe_Debe * Ctb_Tipo_Cambio_Valor_Det);
                }

            }
        }

        public decimal Ctb_Importe_ME
        {
            get
            {
                if ((Ctb_Es_moneda_nac != true))
                {
                    return 0;
                }
                else
                {
                    return Ctb_Importe;
                }

            }
        }

        public string Flujo_Efectivo_Cod { get; set; }
        public string Flujo_Efectivo_Desc { get; set; }
        public string Ctb_Codigo_Caja_Chica { get; set; }
        public string Ctb_Codigo_Caja_Chica_Desc { get; set; }
        public string Ctb_Caja_CHica_Estado { get; set; }
        public string Ctb_Caja_CHica_Estado_Desc { get; set; }

        public Boolean Ctb_Es_Caja_Chica { get; set; }
        public Boolean Ctb_Rendicion_Caja_Chica { get; set; }
        public Boolean Ctb_Reembolso_Caja_Chica { get; set; }
        public Boolean Ctb_Cierre_Caja_Chica { get; set; }

        public string Tes_Cuenta { get; set; }
        public string Ctb_Cuenta_Caja_Chica { get; set; }
        public string Ctb_Cuenta_Caja_Chica_Desc { get; set; }
        public string Ctb_Caja_Chica_Responsable { get; set; }
        public string Ctb_Caja_Chica_Responsable_Desc { get; set; }

 public bool Cta_Dif { get; set; }
        public string Patri_Neto_Cod { get; set; }
        public string Patri_Neto_Desc { get; set; }

        public string Estado_Flujo_Efecctivo_Cod_Det { get; set; }
        public string Estado_Flujo_Efecctivo_Desc_Det { get; set; }

        public bool Tes_DifCam { get; set; }
        public decimal Tes_DifCambio { get; set; }
        public bool Tes_DifRed { get; set; }
        public decimal Tes_DifRedond { get; set; }

        // DETALLE LOTES

        public int Lot_Adm_item { get; set; }
        public int Lot_item { get; set; }
        public string Lot_Catalogo { get; set; }
        public string Lot_Lote { get; set; }
        public DateTime Lot_FechaFabricacion { get; set; }
        public DateTime Lot_FechaVencimiento { get; set; }
        public decimal Lot_Cantidad { get; set; }
        public string Lot_Estado { get; set; }


        public string ctb_pdc_codigo { get; set; }
        public string ctb_pdc_Nombre { get; set; }
        public string ctb_pdv_codigo { get; set; }
        public string ctb_pdv_codigo_Ref { get; set; }
        public string ctb_pdv_Nombre { get; set; }


        public List<Entidad_Movimiento_Cab> DetalleLotes { get; set; }
        public bool Acepta_lotes { get; set; }

        public string Adm_Tipo_Operacion { get; set; }
      public string Adm_Tipo_Operacion_Interno { get; set; }
      public string Adm_Tipo_Operacion_Desc { get; set; }


        public string Cja_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo_Item { get; set; }
        public string cja_estado { get; set; }

        public decimal DvtD_IGVTasaPorcentaje { get; set; }

        public string DvtD_OperCodigo { get; set; }

        public List<Entidad_Movimiento_Cab> DetalleComprobantes { get; set; }

        public decimal DvtD_IGVMonto
        {
            get
            {
                if ((DvtD_OperCodigo == "0565"))
                {
                    return Math.Round((Convert.ToDecimal(Adm_Total) / (1 + (Convert.ToDecimal(DvtD_IGVTasaPorcentaje) / 100))) * (Convert.ToDecimal(DvtD_IGVTasaPorcentaje) / 100), 2);

                }
                else
                {
                    return 0;
                }

            }
        }

  

        public decimal DvtD_SubTotal
        {
            get
            {
                return Math.Round(Convert.ToDecimal(Adm_Total) / (1 + (Convert.ToDecimal(DvtD_IGVTasaPorcentaje) / 100)), 2);
            }
        }



        public decimal DvtD_BaseImpobleOperacionGravada
        {
            get
            {
                if (DvtD_OperCodigo == "0564")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0565")
                {
                    return DvtD_SubTotal;
                }
                else if (DvtD_OperCodigo == "0566")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0567")
                {
                    return 0;
                }
                else
                {
                    return 0;
                }

            }
        }

        public decimal DvtD_IGV_IPM
        {
            get
            {
                if (DvtD_OperCodigo == "0564")
                {
                    return DvtD_SubTotal;
                }
                else if (DvtD_OperCodigo == "0565")
                {
                    return DvtD_IGVMonto;
                }
                else if (DvtD_OperCodigo == "0566")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0567")
                {
                    return 0;
                }
                else
                {
                    return 0;
                }

            }
        }


        public decimal DvtD_ValorFactExportacion
        {
            get
            {
                if (DvtD_OperCodigo == "0564")
                {
                    return DvtD_SubTotal;
                }
                else if (DvtD_OperCodigo == "0565")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0566")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0567")
                {
                    return 0;
                }
                else
                {
                    return 0;
                }

            }
        }


        public decimal DvtD_Exonerada
        {
            get
            {
                if (DvtD_OperCodigo == "0564")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0565")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0566")
                {
                    return DvtD_SubTotal;
                }
                else if (DvtD_OperCodigo == "0567")
                {
                    return 0;
                }
                else
                {
                    return 0;
                }

            }
        }

        public decimal DvtD_Inafecta
        {
            get
            {
                if (DvtD_OperCodigo == "0564")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0565")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0566")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0567")
                {
                    return DvtD_SubTotal;
                }
                else
                {
                    return 0;
                }

            }
        }


        public decimal DvtD_ISC
        {
            get
            {
                if (DvtD_OperCodigo == "0564")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0565")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0566")
                {
                    return 0;
                }
                else if (DvtD_OperCodigo == "0567")
                {
                    return 0;
                }
                else
                {
                    return 0;
                }

            }
        }



            public string Ent_TpDcEnt_SUNAT { get; set; }

            public string Correo_Cliente { get; set; }
            public string Cliente_Razon_Social { get; set; }
            public string Cliente_Direccion { get; set; }

            public string Id_Movimiento { get; set; }//para ventas
        public string Id_Movimiento_Ref { get; set; }
        public bool Ctb_Es_Movimiento_Comercial { get; set; }
            public bool Ctb_Tiene_Asiento { get; set; }

            public bool Es_Venta { get; set; }


    }
}
