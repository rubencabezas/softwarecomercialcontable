﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Entidad_Ctb_Libro
    {
        public string Id_Libro { get; set; }
        public string Nombre_Libro { get; set; }
        public string Id_SUNAT { get; set; }
        public Boolean Lib_CentralizacionMes { get; set; }
    }
}
