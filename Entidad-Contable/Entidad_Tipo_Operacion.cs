﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Entidad_Tipo_Operacion
    {

      
        public string Id_Operacion { get; set; }
        public Boolean Top_Entrada { get; set; }
        public Boolean Top_Salida { get; set; }
        public string Top_Descripcion { get; set; }
        public string Top_Sunat { get; set; }
        public Boolean Top_NecesitaCC { get; set; }
        public Boolean Top_NecesitaCG { get; set; }
        public Boolean Top_Genera_Asiento { get; set; }
        public Boolean Top_Necesita_Doc { get; set; }
        public string Top_Tipo_Doc_Defecto { get; set; }
        public string Top_Tipo_Doc_Defecto_Desc { get; set; }
        public Boolean Top_Es_Saldo_Inicial { get; set; }
        public Boolean Top_Es_Valorizado { get; set; }
        public Boolean Top_Es_Salida_Produccion { get; set; }
        public Boolean Top_ES_Venta { get; set; }
        public Boolean Top_Requiere_Transferecia_Almacen { get; set; }


        public Boolean Top_Requiere_Tipo_Entidad { get; set; }
        public string Top_Tipo_Entidad_Cod { get; set; }
      public string Ent_Descripcion { get; set; }   

    
    }
}
