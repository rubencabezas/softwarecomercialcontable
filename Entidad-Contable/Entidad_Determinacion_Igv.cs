﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Entidad_Determinacion_Igv
    {
        public string Empresa { get; set; }
        public string Anio { get; set; }
        public string Periodo { get; set; }
        public string Periodo_Desc { get; set; }
        public decimal Credito_Fiscal_Mes_Anterior { get; set; }
        public decimal Compensacion_IGV { get; set; }
        public decimal Devuelto_CNC_Exportad { get; set; }
        public decimal Per_Mes_Anterior { get; set; }
        public decimal Per_Aplicadas { get; set; }
        public decimal Ret_Mes_Anterior { get; set; }
        public decimal Ret_Aplicadas { get; set; }


    }
}
