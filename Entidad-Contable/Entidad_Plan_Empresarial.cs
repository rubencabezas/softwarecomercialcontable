﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Entidad_Plan_Empresarial
    {

        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }

        public string Id_Elemento { get; set; }
        public string Ele_Descripcion { get; set; }
        public string Id_Estructura { get; set; }
        public string Est_Descripcion { get; set; }
        public string Est_Num_Digitos { get; set; }

        public string Id_Cuenta { get; set; }
        public string Cta_Descripcion { get; set; }
        public Boolean Cta_Afecto_Doc { get; set; }
        public Boolean Cta_Area { get; set; }
        public Boolean Cta_Centro_Costo { get; set; }
        public Boolean Cta_Centro_Gasto { get; set; }
        public Boolean Cta_Funcion { get; set; }
        public Boolean Cta_Naturaleza { get; set; }
        public Boolean Cta_Inventario { get; set; }

        public string Cod_Proceso { get; set; }
        public string Tag_Proceso { get; set; }
        public string Proceso_Desc { get; set; }

        public string Cod_Obligacion { get; set; }
        public string Tag_Obligacion { get; set; }
        public string Obligacion_Desc { get; set; }


        public string Cta_Afecto_Regimen { get; set; }
        public string Cta_Afecto_Regimen_tag { get; set; }
        public string Cta_Afecto_Regimen_Desc { get; set; }

        public List<Entidad_Plan_Empresarial> DetalleCta_Destino { get; set; }
        public string Id_Cuenta_Destino { get; set; }
        public string Cta_Descripcion_Destino { get; set; }
        public decimal Porcen_Debe { get; set; }
        public decimal Porcen_Haber { get; set; }
        public string Ctb_Tipo_DH { get; set; }
        public string CCtb_Tipo_DH_Interno { get; set; }
        public string Ctb_Tipo_DH_Desc { get; set; }


//Cta_Cod_Bal_Tributario ,
//Cta_Cod_Bal_Rec_Tributario  ,
//Cta_Cod_Bal_Nif_Tributario  ,
//Cta_Cod_Bal_Rec_Nif_Tributario  ,
//Cta_Cod_Bal_Res_Nif_Tributario
        public string Cta_Cod_Bal_Tributario { get; set; }
        public string Cta_Cod_Bal_Tributario_desc { get; set; }
        public string Cta_Cod_Bal_Rec_Tributario { get; set; }
        public string Cta_Cod_Bal_Rec_Tributario_desc { get; set; }
        public string Cta_Cod_Bal_Nif_Tributario { get; set; }
        public string Cta_Cod_Bal_Nif_Tributario_desc { get; set; }
        public string Cta_Cod_Bal_Rec_Nif_Tributario { get; set; }
        public string Cta_Cod_Bal_Rec_Nif_Tributario_desc { get; set; }
        public string Cta_Cod_Bal_Res_Nif_Tributario { get; set; }
        public string Cta_Cod_Bal_Res_Nif_Tributario_desc { get; set; }
    }
}
