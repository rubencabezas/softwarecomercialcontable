﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
public    class Entidad_Caja_Chica_Config
    {

        // ,
        // ,
        // ,
        // ,
        // ,
        // ,
        // ,
        //,
        //Cja_Estado,
        //Cja_Estado_Caja
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Caja { get; set; }
        public string Cja_Descripcion { get; set; }
        public string Cja_Responsable { get; set; }
        public string Cja_Responsable_Desc { get; set; }
        public string Cja_Cuenta { get; set; }
        public string  Cja_Cuenta_Desc { get; set; }
       public Boolean Cja_Es_Caja_Chica { get; set; }
        public Boolean Cja_Es_Caja_Central { get; set; }

        public string Cja_Estado_Caja { get; set; }
        public string Cja_Estado_Caja_det { get; set; }
 public string Cja_Estado_Caja_Desc { get; set; }
    }
}
