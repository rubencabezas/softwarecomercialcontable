﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Entidad_Familia
    {
        //   FAM.Id_Empresa, 
        //FAM.Id_Grupo, GR.Gru_Descripcion,
        //FAM.Id_Familia, FAM.Fam_Descripcion, FAM.Id_Tipo

        public string Id_Empresa { get; set; }
        public string Id_Grupo { get; set; }
        public string Gru_Descripcion { get; set; }
        public string Id_Familia { get; set; }
        public string Fam_Descripcion { get; set; }
        public string Id_Tipo { get; set; }
        public string Id_TipoCod { get; set; }
        public string Id_Tipo_Descripcion { get; set; }

    }
}
