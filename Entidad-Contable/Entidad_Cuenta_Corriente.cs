﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
 public   class Entidad_Cuenta_Corriente
    {
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Cuenta_Corriente { get; set; }
        public string Cor_Ent_Financiera { get; set; }
        public string Fin_Descripcion { get; set; }
        public string Cor_Moneda_cod { get; set; }
        public string Nombre_Moneda { get; set; }
        public string Cor_Cuenta_Contable { get; set; }
        public string Cta_Descripcion { get; set; }


    }
}
