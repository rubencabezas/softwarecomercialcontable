﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Entidad_Tipo_Cambio
    {
        public DateTime Tic_Fecha { get; set; }
        public string Tic_Id_Moneda { get; set; }
        public string Moneda_des { get; set; }

        //public string Tic_Compra_Vig_Cod { get; set; }
        //public string Tic_Compra_Vig_01 { get; set; }
        //public string Tic_Compra_Vig_Desc { get; set; }
        public decimal Tic_Compra_Vigente { get; set; }


        //public string Tic_Venta_Vig_Cod { get; set; }
        //public string Tic_Venta_Vig_02 { get; set; }
        //public string Tic_Venta_Vig_Desc { get; set; }
        public decimal Tic_Venta_Vigente { get; set; }

        //public string Tic_Venta_Pub_Cod { get; set; }
        // public string Tic_Venta_Pub_03 { get; set; }
        //public string Tic_Venta_Pub_Desc { get; set; }
        public decimal Tic_Venta_Publicacion { get; set; }

        //    Property Codigo As String
        //Property Nombre As String
        //Property Valor As Double
        //Property Modif As Boolean
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }

        public Boolean Modif { get; set; }


        //public Entidad_Tipo_Cambio(string c_Codigo, string c_Nombre, decimal c_Valor, bool c_Modif, System.DateTime c_Fecha)
        //{
        //    Codigo = c_Codigo;
        //    Nombre = c_Nombre;
        //    Valor = c_Valor;
        //    Modif = c_Modif;
        //    Tic_Fecha = c_Fecha;
        //}




    }
}
