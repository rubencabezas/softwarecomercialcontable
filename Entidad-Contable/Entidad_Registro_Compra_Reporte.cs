﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
public    class Entidad_Registro_Compra_Reporte
    {
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }

        public string Id_Voucher { get; set; }
        public string Ctb_Fecha_Movimiento { get; set; }
        public string Ctb_Fecha_Venci { get; set; }
        public string Id_Sunat { get; set; }
        public string Ctb_Serie { get; set; }
        public string Ctb_Numero { get; set; }
        public string Ctb_Ruc_dni { get; set; }
        public string Entidad { get; set; }
        public decimal Ctb_Base_Imponible { get; set; }
        public decimal Ctb_Igv { get; set; }
        public decimal Ctb_Base_Imponible2 { get; set; }
        public decimal Ctb_Igv2 { get; set; }
        public decimal Ctb_Base_Imponible3 { get; set; }
        public decimal Ctb_Igv3 { get; set; }
        public decimal Ctb_No_Gravadas { get; set; }
        public decimal Ctb_Isc_Importe { get; set; }
        public decimal Ctb_Otros_Tributos_Importe { get; set; }
        public decimal Ctb_Importe_Total { get; set; }
        public string Ctb_Fecha_Detrac { get; set; }
        public string Ctb_Numero_Detrac { get; set; }
        public decimal Ctb_Tipo_Cambio { get; set; }
        public DateTime Ctb_Fecha_Doc_Ref { get; set; }
        public string Ctb_Serie_Doc_Ref { get; set; }
        public string Ctb_Numero_Doc_Ref { get; set; }

        public string Ple_V5 { get; set; }
    }
}
