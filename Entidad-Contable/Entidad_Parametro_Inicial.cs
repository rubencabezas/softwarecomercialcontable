﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Parametro_Inicial
    {

        //Id_Empresa ,
        //Id_Anio ,
        //Ini_Venta ,
        //Ini_Compra ,
        //Ini_Diario

        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Ini_Venta { get; set; }
        public string Ini_Venta_Desc { get; set; }
        public string Ini_Venta_sunat { get; set; }
        public string Ini_Compra { get; set; }
        public string Ini_Compra_Desc { get; set; }
        public string Ini_Compra_sunat { get; set; }
        public string Ini_Diario { get; set; }
        public string Ini_Diario_Desc { get; set; }
        public string Ini_Diario_sunat { get; set; }

        public string Ini_Inventario { get; set; }
        public string Ini_Inventario_Desc { get; set; }
        public string Ini_Inventario_sunat { get; set; }

        public string Ini_CajaBanco { get; set; }
        public string Ini_CajaBanco_Desc { get; set; }
        public string Ini_CajaBanco_sunat { get; set; }


        //        @ CHAR(3)=NULL,
        //@ CHAR(12)=NULL,
        //@ CHAR(12)=NULL,
        //@ CHAR(12)=NULL,
        //@ CHAR(12)=NULL,
        //@ CHAR(12)=NULL,

        public string Ini_Rendicion_Caja_Chica { get; set; }
        public string Ini_Rendicion_Caja_Chica_Desc { get; set; }
        public string Ini_Rendicion_Caja_Chica_Sunat { get; set; }
        public string Ini_Cta_Ganancia_Dif_Cambio { get; set; }
        public string Ini_Cta_Ganancia_Dif_Cambio_Desc { get; set; }
        public string Ini_Cta_Perdida_Dif_Cambio { get; set; }
        public string Ini_Cta_Perdida_Dif_Cambio_Desc { get; set; }
        public string Ini_Cta_Ganancia_Redondeo { get; set; }
        public string Ini_Cta_Ganancia_Redondeo_Desc { get; set; }
        public string Ini_Cta_Perdida_Redondeo { get; set; }
        public string Ini_Cta_Perdida_Redondeo_Desc { get; set; }
        public string Ini_Cta_ITF { get; set; }
        public string Ini_Cta_ITF_Desc { get; set; }
        public string Ini_Cta_Caja { get; set; }
        public string Ini_Cta_Caja_Desc { get; set; }


 
        public string Ini_Cta_Percepcion { get; set; }
        public string Ini_Cta_Percepcion_Desc { get; set; }

        public string Ini_Cta_Detraccion { get; set; }
        public string Ini_Cta_Detraccion_Desc { get; set; }

        public string Ini_Cta_Retencion { get; set; }
        public string Ini_Cta_Retencion_Desc { get; set; }

        public string Ini_Cta_Igv { get; set; }

        public string Ini_Cta_Igv_Desc { get; set; }
    }
}
