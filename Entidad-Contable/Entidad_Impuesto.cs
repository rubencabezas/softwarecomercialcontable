﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Impuesto
    {
        public string Imc_Codigo { get; set; }
        public string Imc_Descripcion { get; set; }
        public string Imc_Abreviado { get; set; }
        public Boolean Imc_Es_Vigente { get; set; }

    }
}
