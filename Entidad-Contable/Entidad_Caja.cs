﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
  public  class Entidad_Caja
    {

        public string Id_Empresa { get; set; }
        public string Pdv_Codigo { get; set; }
        public string Pdv_Nombre { get; set; }
        public string Cja_Codigo { get; set; }
        public string Cja_Descripcion { get; set; }


        //CAJA USUARIO cab

        public int Cja_Aper_Cierre_Codigo { get; set; }
        public decimal Cja_Saldo_Inicial { get; set; }
        public decimal Cja_Total_Efectivo { get; set; }
        public decimal Cja_Total_Credito { get; set; }
        public decimal Cja_Total_Tarjeta { get; set; }
        public string Cja_Estado { get; set; }
        public string Cja_Estado_Desc { get; set; }
        public string Cja_Autor_Apertura { get; set; }
        public string Cja_Maquina_Apertura { get; set; }
        public string Cja_Responsable { get; set; }

        public int Cja_Aper_Cierre_Codigo_Item { get; set; }

        //
        

        public string Catalogo_Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Importe { get; set; }

        public List<Entidad_Caja> VentasDepartamento_CierreTotal { get; set; }
        public List<Entidad_Caja> VentasSin_Documento { get; set; }
        public List<Entidad_Caja> Ventas_Otros_Ingresos { get; set; }
        public List<Entidad_Caja> Ventas_Otros_Egresos { get; set; }

        public DateTime Cja_Fecha_Apertura { get; set; }
        public DateTime Cja_Fecha_Cierre { get; set; }

        public string Cja_Autor_Cierre { get; set; }
        public string Cja_Autor_Responsable { get; set; }
        public string Cja_Maquina_Cierre { get; set; }
    }
}
