﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Entidad_Unidad_Medida
    {
        // ,
        //    ,
        //    ,
        //   
        public string Id_Empresa { get; set; }
        public string Id_Tipo { get; set; }
        public string Id_Unidad_Medida { get; set; }
        public string Und_Descripcion { get; set; }
        public string Und_Abreviado { get; set; }
        public string Und_SunaT { get; set; }
        public string Id_Catalogo { get; set; }
        public bool Unm_Defecto { get; set; }
        public string Unm_Cod_Det { get; set; }
        public decimal Unm_Base { get; set; }
        public string UnmFE { get; set; }
        public int Id_Item_Det { get; set; }
    }
}
