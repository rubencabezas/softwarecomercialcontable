﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Entidad_Balance_Comprobacion_Reporte
    {
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Es_Num_Digitos { get; set; }
        public string Id_Moneda { get; set; }
        public Boolean IsFuncion { get; set; }
        public Boolean IsNaturaleza  { get; set; }
        public Boolean IsInventario { get; set; }
        public Boolean Acumulado { get; set; }
        public Boolean Mensual { get; set; }
        public string Cuenta { get; set; }
        public string Descripcion { get; set; }
        public decimal Debe { get; set; }
        public decimal Haber { get; set; }
        public decimal SaldoDebe { get; set; }
        public decimal SaldoHaber { get; set; }
        public decimal InvenDebe { get; set; }
        public decimal InvenHaber { get; set; }
        public decimal FuncionDebe { get; set; }
        public decimal FuncionHaber { get; set; }
        public decimal NatuDebe { get; set; }
        public decimal NatuHaber { get; set; }


        //DETALLE DE LA CUENTA

        public string Ctb_Cuenta { get; set; }
        public string Id_Voucher { get; set; }
        public string Ctb_Tipo_Doc_det { get; set; }
        public string Nombre_Comprobante { get; set; }
        public string Doc_Id_Sunat { get; set; }
        public string Ctb_Serie_det { get; set; }
        public string Ctb_Numero_det { get; set; }
        public string Ctb_Ruc_dni_det { get; set; }
        public string Entidad { get; set; }
        public DateTime Ctb_Fecha_Mov_det { get; set; }
        public string Ctb_Fecha_Mov_det_ { get; set; }

        public string Nombre_Moneda { get; set; }
        public string Mon_Id_Sunat { get; set; }
        public string Ctb_Tipo_Cambio_Cod_Det { get; set; }
        public decimal Ctb_Tipo_Cambio_Valor_Det { get; set; }
        public decimal Ctb_Importe_Debe { get; set; }
        public decimal Ctb_Importe_Haber { get; set; }
        public decimal Ctb_Importe_Debe_Extr { get; set; }
        public decimal Ctb_Importe_Haber_Extr { get; set; }
        public string Nombre_Libro { get; set; }

    }
}
