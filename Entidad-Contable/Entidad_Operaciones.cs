﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
  public  class Entidad_Operaciones
    {
 

        public string AfecIGV_Codigo { get; set; }
        public string AfecIGV_Descripcion { get; set; }
        public string AfecIGV_Tabla { get; set; }
        public string Gen_Codigo_Interno { get; set; }
        public string Gen_Descripcion_Det { get; set; }


        public string Id_Empresa { get; set; }
        public string Id_Tipo { get; set; }
        public string Id_Catalogo { get; set; }
 


    }
}
