﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Orden_C_S
    {

        //, , , , , 
        //, , , , ,
        //, , , , ,
        //, , , , , 
        //Ord_Estado, , , Usuario_Crea, Fecha_Crea, Maquina_Crea, 
        //Usuario_Modi, Fecha_Modi, Maquina_Modi

        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Id_Movimiento { get; set; }
        public string Id_Tipo_Orden { get; set; }
        public string Id_Tipo_Orden_det { get; set; }
        public string Id_Tipo_Orden_des { get; set; }
        public string Ord_Tipo_Doc { get; set; }
        public string Ord_Tipo_Doc_desc { get; set; }
        public string Ord_Serie { get; set; }
        public string Ord_Numero { get; set; }
        public string Ord_Proveedor_Ruc_dni { get; set; }
        public string Ord_Proveedor_Ruc_dni_desc { get; set; }

        public string Ord_Responsable_Ruc_dni { get; set; }
        public string Ord_Responsable_Ruc_dni_desc { get; set; }
        public string Ord_Observacion { get; set; }
        public string Ord_Condicion_Cod { get; set; }
        public string Ord_Condicion_det { get; set; }
        public string Ord_Condicion_desc { get; set; }
        public DateTime Ord_Fecha { get; set; }
        public int Ord_Dias { get; set; }
        public DateTime Ord_Fecha_Venci { get; set; }
        public string Ord_Moneda_Cod { get; set; }
        public string Ord_Moneda_desc { get; set; }
        public string Ord_Tipo_Cambio_Cod { get; set; }
        public string Ord_Tipo_Cambio_Desc { get; set; }
        public decimal Ord_Tipo_Cambio_Valor { get; set; }
        public decimal Ord_Sub_Total { get; set; }
        public decimal Ord_Igv { get; set; }
        public decimal Ord_Importe_Total { get; set; }
        public string Ord_Ubicaion { get; set; }
        public Boolean Ord_Inventario { get; set; }
   public decimal Ord_Igv_Tasa { get; set; }

        //
        public int Doc_Item { get; set; }
        public string Doc_Proceso { get; set; }
        public string Doc_Anio { get; set; }
        public string Doc_Periodo { get; set; }
       
        public string Doc_Folio { get; set; }

        //detalles
        public List<Entidad_Orden_C_S> Detalle { get; set; }
        public List<Entidad_Orden_C_S> Detalle_Requerimiento { get; set; }

        //
        public int Ord_Item { get; set; }
        public string Ord_Tipo_BSA { get; set; }
        public string Ord_Tipo_BSA_Interno { get; set; }
        public string Ord_Tipo_BSA_Desc { get; set; }

        public string Ord_Catalogo { get; set; }
        public string Ord_Catalogo_desc { get; set; }
        public string Ord_Descripcion { get; set; }
        public string Ord_Almacen { get; set; }
        public string Ord_Almacen_desc { get; set; }

        public decimal Ord_Cantidad { get; set; }
        public decimal Ord_Precio_Unitario { get; set; }
        public Boolean Ord_Incluye_Igv { get; set; }
        public decimal Ord_Igv_Porcentaje { get; set; }

        public decimal Ord_Sub_Total_item { get; set; }
        public decimal Ord_Igv_item { get; set; }
        public decimal Ord_Importe_Total_item { get; set; }

    }
}
