﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
  public  class Entidad_Venta_Cab
    {
        //Ven_Estado, , , Usuario_Crea, Fecha_Crea, Maquina_Crea, Usuario_Modi, Fecha_Modi, Maquina_Modi

        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Id_Pdv_Codigo { get; set; }
        public string Id_Movimiento { get; set; }
        public string Ven_Glosa { get; set; }
        public string Ven_Tipo_Doc { get; set; }
        public string Ven_Serie { get; set; }
        public string Ven_Numero { get; set; }
        public DateTime Ven_Fecha_Movimiento { get; set; }
        public DateTime Ven_Fecha_Vencimiento { get; set; }
        public string Ven_Tipo_Ent { get; set; }
        public string Ven_Ruc_dni { get; set; }
        public string Ven_Condicion_Cod { get; set; }
        public string Ven_Moneda_Cod { get; set; }
        public string Ven_Tipo_Cambio_Cod { get; set; }
        public string Ven_Tipo_Cambio_Desc { get; set; }

        public decimal Ven_Tipo_Cambio_Valor { get; set; }
        public decimal Ven_Igv_Tasa { get; set; }
        public decimal Ven_Base_Imponible { get; set; }
        public decimal Ven_Igv { get; set; }
        public decimal Ven_Valor_Fact_Exonerada { get; set; }
        public decimal Ven_Exonerada { get; set; }
        public decimal Ven_Inafecta { get; set; }
        public bool Ven_Isc { get; set; }
        public decimal Ven_Isc_Importe { get; set; }
        public bool Ven_Otros_Tributos { get; set; }
        public decimal Ven_Otros_Tributos_Importe { get; set; }
        public decimal Ven_Importe_Total { get; set; }
        public string Ven_Analisis { get; set; }
        public string Cja_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo_Item { get; set; }
        public string Cja_Estado { get; set; }
        public string Id_Libro { get; set; }
        public string Id_Voucher { get; set; }

        public List<Entidad_Venta_Cab> DetalleADM { get; set; }

        //DETALLES

        public int Adm_Item { get; set; }
        public string Adm_Centro_CG { get; set; }
        public string Adm_Centro_CG_Desc { get; set; }
        public string Adm_Tipo_BSA { get; set; }
        public string Adm_Tipo_BSA_Interno { get; set; }
        public string Adm_Tipo_BSA_Desc { get; set; }
        public string Adm_Catalogo { get; set; }
        public string Adm_Catalogo_Desc { get; set; }
        public string Adm_Unm_Id { get; set; }
        public string Adm_Unm_Desc { get; set; }
        public string Adm_Unm_Abrev { get; set; }
        public string Adm_Almacen { get; set; }
        public string Adm_Almacen_desc { get; set; }
        public decimal Adm_Cantidad { get; set; }
        public decimal Adm_Valor_Venta { get; set; }
        public decimal Adm_Valor_Unit { get; set; }
        public decimal Adm_Total { get; set; }

    }
}
