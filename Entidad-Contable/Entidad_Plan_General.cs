﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
public    class Entidad_Plan_General
    {
        public string Id_Cuenta { get; set; }
        public string Cta_Descripcion { get; set; }
        public string Id_Elemento { get; set; }
        public string Ele_Descripcion { get; set; }
        public string Id_Estructura { get; set; }
         public string Est_Descripcion { get; set; }
        public string Est_Num_Digitos { get; set; }

        public List<Entidad_Plan_General> DetallesCuentas { get; set; }

        public Entidad_Plan_General()
        {
        }

    }
}
