﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Requerimiento
    {
       
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Id_Movimiento { get; set; }
        public string Req_Solicitante_Ruc_dni { get; set; }
        public string Req_Solicitante_Ruc_dni_desc { get; set; }
        public string Rec_Id_Area { get; set; }
        public string Rec_Id_Area_desc { get; set; }
        public string Req_Id_Cargo { get; set; }
        public string Req_Id_Cargo_desc { get; set; }
        public string Req_Encardado_Ruc_Dni { get; set; }
        public string Req_Encardado_Ruc_Dni_desc { get; set; }
        public string Req_Descripcion { get; set; }
        public string Req_Tipo_Doc { get; set; }
        public string Req_Tipo_Doc_desc { get; set; }
        public string Req_Serie { get; set; }
        public string Req_Numero { get; set; }
        public DateTime Req_Fecha_Envio { get; set; }
        public TimeSpan Req_Hora_Envio { get; set; }

        public string Asist_Hora_Envio
        {
            get { return new DateTime(Req_Hora_Envio.Ticks).ToString("hh:mm:ss tt"); }
        }

        public string Req_O_C { get; set; }
        public string Req_Codigo_Proyecto { get; set; }

        public List<Entidad_Requerimiento> DetalleS { get; set; }

        public int Id_Item { get; set; }
        public string Req_Tipo_BSA { get; set; }
        public string Req_Tipo_BSA_Det { get; set; }
        public string Req_Tipo_BSA_Descripcion { get; set; }
        public string Req_Catalogo { get; set; }
        public string Req_Catalogo_Desc { get; set; }
        public string Req_Detalle { get; set; }
        public string Req_Observacion { get; set; }
        public Decimal Req_Cantidad { get; set; }
        /*ublic int MyProperty { get; set; }*/
        //public int MyProperty { get; set; }
        //public int MyProperty { get; set; }
        //public int MyProperty { get; set; }
        //public int MyProperty { get; set; }
        //public int MyProperty { get; set; }
        //public int MyProperty { get; set; }
        //public int MyProperty { get; set; }
    }
}
