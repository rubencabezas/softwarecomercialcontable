﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Entidad_Comprobantes
    {
        public string Id_Comprobante { get; set; }
        public string Nombre_Comprobante { get; set; }
        public string Id_SUnat { get; set; }
        public Boolean Es_Documento_Interno { get; set; }

    }
}
