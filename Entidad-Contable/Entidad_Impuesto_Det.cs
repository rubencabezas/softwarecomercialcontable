﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Entidad_Impuesto_Det
    {

        public string Imd_Codigo { get; set; }
        public string Imc_Descripcion { get; set; }
        public DateTime Imd_Fecha_Inicio { get; set; }
        public DateTime Imd_Fecha_Fin { get; set; }
        public decimal Imd_Tasa { get; set; }
        public string Imd_Descripcion_Impresion { get; set; }
        public string Imd_Tasa_Impresion { get; set; }

    }
}
