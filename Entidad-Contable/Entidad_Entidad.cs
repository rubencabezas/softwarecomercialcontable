﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Entidad
    {
        //Ent_RUC_DNI, Ent_Tipo_Persona, Ent_Tipo_Doc, Ent_Razon_Social_Nombre, Ent_Ape_Paterno, Ent_Ape_Materno, 
        //Ent_Telefono, Ent_Repre_Legal, Ent_Telefono_movil, Ent_Domicilio_Fiscal, Ent_Correo, 
        //Ent_Pagina_Web, Ent_Estado, Usuario_Crea, Fecha_Crea, Maquina_Crea, Usuario_Modi, Fecha_Modi, Maquina_Modi
        public string Ent_RUC_DNI { get; set; }
        public string Ent_Tipo_Persona { get; set; }
        public string Ent_Tipo_Persona_Cod_Interno { get; set; }
        public string Ent_Tipo_Persona_Descripcion { get; set; }
        public string Ent_Tipo_Doc { get; set; }
        public string Ent_Tipo_Doc_Cod_Interno { get; set; }
        public string Ent_Tipo_Doc_Descripcion { get; set; }
        public string Ent_Razon_Social_Nombre { get; set; }
        public string Ent_Ape_Paterno { get; set; }
        public string Ent_Ape_Materno { get; set; }
        public string Ent_Telefono { get; set; }
        public string Ent_Repre_Legal { get; set; }
        public string Ent_Telefono_movil { get; set; }
        public string Ent_Domicilio_Fiscal { get; set; }
        public string Ent_Correo { get; set; }
        public string Ent_Pagina_Web { get; set; }


        public string Id_Tipo_Ent { get; set; }

        public string Ent_Descripcion { get; set; }

        public string Estado { get; set; }
        public string EstadoContr { get; set; }
        public Boolean Estado_De_Consulta { get; set; }
 public Boolean Estado_No_Existe { get; set; }
    }
}
