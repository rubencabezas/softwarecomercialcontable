﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Entidad_Moneda
    {
        public string Id_Moneda { get; set; }

        public string Nombre_Moneda { get; set; }
        public string Id_Sunat { get; set; }
        public bool Es_nacional { get; set; }

    }
}
