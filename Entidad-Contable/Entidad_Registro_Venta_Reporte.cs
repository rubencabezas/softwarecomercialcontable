﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Registro_Venta_Reporte
    {

        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }

        public string Id_Voucher { get; set; }
        public string Ctb_Fecha_Movimiento { get; set; }
        public string Id_Sunat { get; set; }
        public string Ctb_Serie { get; set; }
        public string Ctb_Numero { get; set; }
        public string TipoDoc_Entidad_sunat { get; set; }
        public string Ctb_Ruc_dni { get; set; }
        public string Entidad { get; set; }
        public decimal Ctb_Valor_Fact_Exonerada { get; set; }
       public decimal Ctb_Base_Imponible { get; set; }
        public decimal Ctb_Exonerada { get; set; }
        public decimal Ctb_Inafecta { get; set; }
        public decimal Ctb_Isc_Importe { get; set; }
        public decimal Ctb_Igv { get; set; }
        public decimal Ctb_Otros_Tributos_Importe { get; set; }
        public decimal Ctb_Importe_Total { get; set; }

        public string Ple_V5 { get; set; }

    }
}
