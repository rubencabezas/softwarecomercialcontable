﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
 public   class Entidad_Cedula_Determinacion_IGV
    {
        public string Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Periodo { get; set; }
        public decimal EXPORTACIONES_FACTURADAS { get; set; }
        public decimal VENTAS_NO_GRABADAS { get; set; }
        public decimal VENTAS_NETAS { get; set; }
        public decimal DEBITO_FISCAL { get; set; }
        public decimal DESC_CONCED_DEV_VENTAS { get; set; }
        public decimal DEB_FISCAL_FISCAL_DESC_CONCED { get; set; }

        public decimal TOTAL_DEBITO_FISCAL
        {
            get
            {
               return Math.Round(DEBITO_FISCAL- DEB_FISCAL_FISCAL_DESC_CONCED,2);
            }
        }

        public decimal BASE_IMPONIBLE_NACIONAL { get; set; }
        public decimal IGV_NACIONAL { get; set; }
        public decimal BASE_IMPONIBLE_2_NACIONAL { get; set; }
        public decimal IGV_2_NACIONAL { get; set; }
        public decimal BASE_IMPONIBLE_3_NACIONAL { get; set; }
        public decimal IGV_3_NACIONAL { get; set; }
        public decimal ADQU_NO_GRADAS { get; set; }
        public decimal IMPUESTO_SELECTIVO_CONSUMO { get; set; }
        public decimal OTROS_TRIBUTOS_CARGO { get; set; }

        public decimal CREDITO_FISCAL_MES_ANTERIOR_X { get; set; }
        public decimal CREDITO_FISCAL_MES_ANTERIOR { get; set; }

        public decimal CREDITO_FISCAL_DISPONIBLE
        {
            get
            {
                return Math.Round(IGV_NACIONAL + IGV_2_NACIONAL+ CREDITO_FISCAL_MES_ANTERIOR, 2);
            }
        }

        public decimal CREDITO_FISCAL_MES_SIGUIENTE
        {
            get { 
            
                //if (CREDITO_FISCAL_DISPONIBLE != 0)
                //{
                //  return Math.Round(CREDITO_FISCAL_DISPONIBLE, 2);
                //}
                //else
                //{
                //  return 0;
                //}
                if (DEVUELTO_CON_NCN_EXPORTADO > 0)
                {
                    if ((CREDITO_FISCAL_DISPONIBLE - TOTAL_DEBITO_FISCAL - COMPENSACION_DEL_IGV)> 0){
                        return Math.Round(CREDITO_FISCAL_DISPONIBLE -TOTAL_DEBITO_FISCAL - COMPENSACION_DEL_IGV - DEVUELTO_CON_NCN_EXPORTADO , 2);
                    }
                    else
                    {
                        return 0;
                    }
                }else
                {
                    if ((CREDITO_FISCAL_DISPONIBLE - TOTAL_DEBITO_FISCAL - COMPENSACION_DEL_IGV) > 0)
                    {
                        return Math.Round(CREDITO_FISCAL_DISPONIBLE - TOTAL_DEBITO_FISCAL - COMPENSACION_DEL_IGV - DEVUELTO_CON_NCN_EXPORTADO, 2);
                    }
                    else
                    {
                        return 0;
                    }
                }

            }
        }

        public decimal IMPUESTO_DETERMINADO
        {
            get
            {
                if (TOTAL_DEBITO_FISCAL - CREDITO_FISCAL_DISPONIBLE <0)
                {
                    return 0;
                }
                else
                {
                    return Math.Round(TOTAL_DEBITO_FISCAL - CREDITO_FISCAL_DISPONIBLE, 2);
                }

            }
        }


        public decimal COMPENSACION_DEL_IGV { get; set; }
        public decimal DEVUELTO_CON_NCN_EXPORTADO { get; set; }

        //PERCEPCIONES
        public decimal PERCEPCIONES_DEL_MES { get; set; }
        public decimal PERCEPCION_MES_ANTERIOR_X { get; set; }
        public decimal PERCEPCION_MES_ANTERIOR { get; set; }
        public decimal PERCEPCIONES_DEVUELTO_O_COMPESADO { get; set; }
        public decimal PERCEPCIONES_APLICADAS { get; set; }
       //RETENCIONES
        public decimal RETENCIONES_DEL_MES { get; set; }
        public decimal RETENCION_MES_ANTERIOR_X { get; set; }
        public decimal RETENCION_MES_ANTERIOR { get; set; }
        public decimal RETENCION_DEVUELTO_O_COMPESADO { get; set; }
        public decimal RETENCION_APLICADAS { get; set; }


        public decimal TRIBUTO_POR_PAGAR
        {
            get
            {
              return Math.Round(IMPUESTO_DETERMINADO - PERCEPCIONES_APLICADAS - RETENCION_APLICADAS, 2);
            }
        }
    }
}
