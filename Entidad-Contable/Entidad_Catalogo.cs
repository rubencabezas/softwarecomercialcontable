﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Entidad_Catalogo
    {

        //cat.Id_Empresa, cat.Id_Grupo,gr.Gru_Descripcion, 
        //cat.Id_Familia,fam.Fam_Descripcion, 
        //cat.Id_Tipo, cat.Id_Catalogo, 
        //cat.Cat_Descripcion, 

        //ISNULL(cat.Cat_Serie,'') AS Cat_Serie,
        //ISNULL(cat.Cat_Barra, '' )AS Cat_Barra,
        //ISNULL(cat.Id_Marca, '') AS Id_Marca,
        //ISNULL(mar.Mar_Descripcion, '') AS Mar_Descripcion,
        //ISNULL(cat.Id_Unidad_Medida, '') AS Id_Unidad_Medida,
        //ISNULL(unm.Und_Descripcion, '') AS Und_Descripcion,
        //ISNULL(cat.Id_Tipo_Existencia, '') AS Id_Tipo_Existencia,
        //ISNULL(ex.Exs_Nombre, '') AS Exs_Nombre


        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Grupo { get; set; }
        public string Gru_Descripcion { get; set; }
        public string Id_Familia { get; set; }
        public string Fam_Descripcion { get; set; }
        public string Id_Tipo { get; set; }
        public string Id_TipoCod { get; set; }
        public string Id_TipoDescripcion { get; set; }


        public string Id_Catalogo { get; set; }
        public string Cat_Descripcion { get; set; }
        public string Cat_Serie { get; set; }
        public string Cat_Barra { get; set; }
        public string Id_Marca { get; set; }
        public string Mar_Descripcion { get; set; }
        public string Id_Unidad_Medida { get; set; }
        public string Und_Descripcion { get; set; }
        public string Und_Abreviado { get; set; }
        public string Id_Tipo_Existencia { get; set; }
        public string Exs_Nombre { get; set; }

        public string Id_Almacen { get; set; }
        public decimal Stock { get; set; }
        public decimal Cat_Unidades_Presentacion { get; set; }
        public string Cat_Comentarios { get; set; }
        public string Id_Fabricante { get; set; }
        public string Fab_Descripcion { get; set; }
        public bool Acepta_Lote { get; set; }
        public bool Facturar_Sin_Existencia { get; set; }
        //////////////
        public int Id_Item { get; set; }
        public string Esp_Codigo { get; set; }
        public string Esp_Descripcion { get; set; }

        public string Esp_Desc { get; set; }

        public List<Entidad_Catalogo> Detalle_especificaciones { get; set; }

        public List<Entidad_Catalogo> Detalle_UNM { get; set; }

        public int Id_Item_Det { get; set; }
        public string Unm_Cod_Det { get; set; }
        public string Unm_Desc_Det { get; set; }
        public decimal Unm_Base { get; set; }
        public bool Unm_Defecto { get; set; }
        public string Unm_Sunat { get; set; }
        public string Unm_FE { get; set; }
        //
        public string Pre_Id_Mod_Venta { get; set; }
        public string Gen_Descripcion_Det { get; set; }

        public string Unm_Coef_Base_Unidad_Medida { get; set; }
        public string Unm_Coef_Base_Unidad_Medida_Descripcion { get; set; }

        //public string Presentacion_Descripcion { get; set; }
        public string Est_Codigo { get; set; }
        public decimal Cantidad { get; set; }
        public string Presentacion_Descripcion
        {
            get
            {
                return Convert.ToString(Unm_Desc_Det) + " x " + Convert.ToString(Unm_Base) + " " + Convert.ToString(Unm_Coef_Base_Unidad_Medida_Descripcion);


            }
        }


        public string Lot_Lote { get; set; }
        public DateTime Lot_FechaFabricacion { get; set; }
        public DateTime Lot_FechaVencimiento { get; set; }
        public decimal Stock_lote { get; set; }


        public string Id_Afecto_Venta { get; set; }
        public string Afecto_Venta_Descripcion { get; set; }

        public string Id_Analisis { get; set; }
        public string Id_Analisis_Desc { get; set; }

        public string Id_Analisis_Compra { get; set; }
        public string Id_Analisis_Compra_Desc { get; set; }
        public bool Es_Compra { get; set; }
        public bool Es_Venta { get; set; }

        public decimal PrecUnit { get; set; }
        public decimal Total { get; set; }

        public decimal Adm_Valor_Venta { get; set; }
        public decimal Adm_Valor_Unit { get; set; }

        public List<Entidad_Catalogo> Detalle_Cuenta { get; set; }

        public string Id_Cuenta_Venta { get; set; }
        public string Id_Cuenta_Venta_desc { get; set; }
        public string Id_Cuenta_Compra { get; set; }
        public string Id_Cuenta_Compra_desc { get; set; }

        public decimal Invd_CantidadAtendidaEnBaseAlCoefciente { get; set; }
    }
}
