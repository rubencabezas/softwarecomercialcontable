﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Entidad_Flujo_Efectivo
    {

        public int ID { get; set; }
        public string Empresa { get; set; }
        public string Anio { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal Capital { get; set; }
        public decimal Capital_Adicional { get; set; }
        public decimal Acciones_Inversion { get; set; }
        public decimal Exedente_Revaluacion { get; set; }
        public decimal Reserva_Legal { get; set; }
        public decimal Otras_Reservas { get; set; }
        public decimal Resultados_Acumulados { get; set; }

        public decimal Total_Patrimonio_Neto
        {
            get
            {
             
                    return (Capital + Capital_Adicional+ Acciones_Inversion+ Exedente_Revaluacion+ Reserva_Legal+ Otras_Reservas+ Resultados_Acumulados);
          
            }
        }


        //
        public string Periodo01 { get; set; }
        public string Periodo02 { get; set; }
        public bool Acumulado { get; set; }
        public bool Acumulado2 { get; set; }
        public string Flujo_Efectivo_Cod { get; set; }
        public string Flujo_Efectivo_Desc { get; set; }
        public string Flujo_Efectivo_Cod_conasev { get; set; }
        public int Flujo_Efectivo_Grupo { get; set; }
        public decimal Montos { get; set; }
        public decimal Montos2 { get; set; }
    }
}
