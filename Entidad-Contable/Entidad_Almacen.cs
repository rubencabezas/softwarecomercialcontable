﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Entidad_Almacen
    {
        //   AL.Id_Empresa,AL.Id_Empresa,AL.Alm_Descrcipcion,
        //AL.Alm_TipoBSA,tp.Gen_Codigo_Interno,tp.Gen_Descripcion_Det
        public string Id_Empresa { get; set; }
        public string Id_Almacen { get; set; }
        public string Alm_Descrcipcion { get; set; }
        public string Alm_TipoBSA { get; set; }
        public string Alm_TipoBSA_cod_interno { get; set; }
        public string Alm_TipoBSA_Descripcion_Det { get; set; }



    }
}
