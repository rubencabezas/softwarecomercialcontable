﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
   public class Entidad_Movimiento_Caja
    {
        //, , , , , , , 
        public string Id_Empresa { get; set; }
        public string Pdv_Codigo { get; set; }
        public string Cja_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo { get; set; }
        public int Cja_Movimiento_Item { get; set; }
        public string Cja_Tipo { get; set; }
        public string Cja_Tipo_desc { get; set; }
        public decimal Cja_Monto { get; set; }
        public string Cja_Motivo { get; set; }
        public int Cja_Aper_Cierre_Codigo_Item { get; set; }

        ///
        //@Item INT,
        //@Anio_Ref CHAR(4)   , 
        //@Periodo_Ref CHAR(2), 
        //@Id_Tipo_Mov_Ref CHAR(4), 
        //@Id_Almacen_Ref CHAR(2), 
        //@Id_Movimiento_Ref CHAR(10), 
        //@Es_Venta_Sin_Ticket BIT,
        //@Ruc_DNI VARCHAR(15) NULL,
        //@Total DECIMAL(18,4) NULL,
       // @TipoDoc ,
        //@Serie  ,
        //@Numero
        public int Item { get; set; }
        public string Anio_Ref { get; set; }
        public string Periodo_Ref { get; set; }
        public string Id_Tipo_Mov_Ref { get; set; }
        public string Id_Almacen_Ref { get; set; }
        public string Id_Movimiento_Ref { get; set; }
        public bool Es_Venta_Sin_Ticket { get; set; }
        public string Ruc_DNI { get; set; }
        public decimal Total { get; set; }
        public string TipoDoc { get; set; }
        public string TipoDocNombre { get; set; }
        public string Serie { get; set; }
        public string Numero { get; set; }
        public decimal Amortizado { get; set; }
        public List<Entidad_Movimiento_Caja> DetalleCobroCaja { get; set; }



    }       
}
