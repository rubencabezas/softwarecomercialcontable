﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Analisis_Contable
    {

        //, , , , , , 
        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Analisis { get; set; }
        public string Ana_Descripcion { get; set; }
        public Boolean Ana_Compra { get; set; }
        public Boolean Ana_Venta { get; set; }
        public Boolean Ana_Inventario { get; set; }
        public Boolean Ana_Asiento_Defecto { get; set; }

        //Detalles
        public List<Entidad_Analisis_Contable> Detalles{ get; set; }

        public int Id_Item { get; set; }
        public string Ana_Operacion_Cod { get; set; }
        public string Ana_Operacion_Det { get; set; }
        public string Ana_Operacion_Desc { get; set; }
        public string Ana_Cuenta { get; set; }
        public string Ana_Cuenta_Desc { get; set; }
        public string Ana_Tipo { get; set; }
        public string Ana_Tipo_Det { get; set; }
        public string Ana_Tipo_Desc { get; set; }

    }
}
