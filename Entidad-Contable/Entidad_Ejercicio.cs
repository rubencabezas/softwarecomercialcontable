﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Entidad_Ejercicio
    {

        public string Id_Empresa { get; set; }
        public string Id_Anio { get; set; }
        public string Emp_Nombre { get; set; }
       
         public string Id_Periodo { get; set; }
        public string Descripcion_Periodo { get; set; }
        public string Id_Empresa_Destino { get; set; }
        public string Id_Anio_Destino { get; set; }

        public bool Es_Plan_contable { get; set; }
        public bool Es_Analisis { get; set; }
        public bool Es_Cuenta_Corriente { get; set; }
        public bool Es_Parametro_inicial { get; set; }

    }
}
