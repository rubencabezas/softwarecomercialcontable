﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Entidad_Series_Tesorerira
    {
        public string Id_Empresa { get; set; }
        public string Ser_Tipo_Doc { get; set; }
        public string Ser_Tipo_Doc_Des { get; set; }
        public string Ser_Tipo_Doc_Sunat { get; set; }
        public string Ser_Tip_IE { get; set; }
        public string Ser_Tip_IE_Interno { get; set; }
        public string Ser_Tip_IE_Desc { get; set; }
        public string Ser_Tipo_Mov { get; set; }
        public string Ser_Tipo_Mov_Interno { get; set; }
        public string Ser_Tipo_Mov_Desc { get; set; }
        public string Ser_Serie { get; set; }
        public string Ser_Numero_Inicio { get; set; }
        public string Ser_Numero_Fin { get; set; }
        public string Ser_Numero_Actual { get; set; }
        public Boolean Ser_Emitiendose { get; set; }
        public Boolean Ser_Es_CtaCorriente { get; set; }
        public string Ser_Cta_Corriente { get; set; }

        public Boolean Ser_Es_CajaChica { get; set; }
       public string Ser_Codigo_Caja { get; set; }
        public string Ser_Caja_Desc { get; set; }


        public Boolean Ser_Rendicion_Caja_Chica { get; set; }
        public Boolean Ser_Reembolso_Caja_Chica { get; set; }
        public Boolean Ser_Cierre_Caja_Chica { get; set; }

        //        @ BIT = NULL,
        //@ VARCHAR(30)

    }
}
