﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Movimiento_Inventario
    {
      
    
        public string Id_Empresa { get; set; }
        public string Empresa_desc { get; set; }

        public string Empresa_ruc { get; set; }
        public string Id_Anio { get; set; }
        public string Id_Periodo { get; set; }
        public string Id_Tipo_Mov { get; set; }
        public string Id_Tipo_Mov_Det { get; set; }
        public string Id_Tipo_Mov_Desc { get; set; }
        public string Id_Almacen { get; set; }
        public string Id_Almacen_Desc { get; set; }
        public string Id_Movimiento { get; set; }
        public string Id_Movimiento_Desc { get; set; }
        public string Id_Tipo_Operacion { get; set; }
        public string Id_Tipo_Operacion_Desc { get; set; }
        public string Inv_Tipo_Entidad { get; set; }
        public string Inv_Tipo_Entidad_Desc { get; set; }
        public string Inv_Ruc_Dni { get; set; }
        public string Inv_Ruc_Dni_Desc { get; set; }
        public string Inv_Tipo_Doc { get; set; }
        public string Inv_Tipo_Doc_Desc { get; set; }
        public string Inv_Serie { get; set; }
        public string Inv_Numero { get; set; }
        public DateTime Inv_Fecha { get; set; }
        public string Inv_Almacen_O_D { get; set; }
        public string Inv_Almacen_O_D_Desc { get; set; }
        public string Inv_Responsable_Ruc_Dni { get; set; }
        public string Inv_Responsable_Ruc_Dni_Desc { get; set; }
        public string Inv_Libro { get; set; }
        public string Inv_Voucher { get; set; }
        public string Glosa { get; set; }

        public string Invd_Unm_sunat { get; set; }

        //
        //public string Id_Anio_Ref { get; set; }
        //public string Id_Periodo_Ref { get; set; }


        //detalle

        // ,
        //   ,
        //   ,
        //   ,
        //   ,
        //   ,
        //  
        public List<Entidad_Movimiento_Inventario> DetalleAsiento { get; set; }

        public int Id_Item { get; set; }
        public string Invd_TipoBSA { get; set; }
        public string Invd_TipoBSA_Det { get; set; }
        public string Invd_TipoBSA_Descipcion { get; set; }
        public string Invd_Catalogo { get; set; }
        public string Invd_Catalogo_Desc { get; set; }
        public decimal Invd_Cantidad { get; set; }
        public decimal Invd_Valor_Unit { get; set; }
        public decimal Invd_Total { get; set; }
        public string Invd_Centro_CG { get; set; }
        public string Invd_Centro_CG_Desc { get; set; }
        ///////////////
        /////////////////
        public string Proceso_Cod { get; set; }
        public string Proceso_Desc { get; set; }
        public string Periodo_Desc { get; set; }
        public string Tipo_Doc_Sunat { get; set; }


        public List<Entidad_Movimiento_Inventario> DetalleDoc_Referencia{ get; set; }

        public int Doc_Item { get; set; }
        public string Doc_Proceso { get; set; }
    public string Doc_Anio { get; set; }
    public string Doc_Periodo { get; set; }
    public string Doc_Folio { get; set; }




        public bool Acepta_lotes { get; set; }

        // DETALLE LOTES

        public int Lot_Adm_item { get; set; }
        public int Lot_item { get; set; }
        public string Lot_Catalogo { get; set; }
        public string Lot_Catalogo_Desc { get; set; }
        public string Lot_Lote { get; set; }
        public DateTime Lot_FechaFabricacion { get; set; }
        public DateTime Lot_FechaVencimiento { get; set; }
        public decimal Lot_Cantidad { get; set; }
        public string Lot_Estado { get; set; }
        //
        public string Invd_Unm { get; set; }
        public string Invd_Unm_Desc { get; set; }
        public string Invd_Coef_Presentacion_Desc { get; set; }
        public decimal Stock { get; set; }

        public string Invd_Marca { get; set; }
        public string Invd_Marca_desc { get; set; }
        public bool Inv_Es_Venta_Sin_Ticket { get; set; }
        public bool Inv_Es_Venta { get; set; }

        public string Inv_Condicion { get; set; }
        public string Inv_Condicion_Interno { get; set; }
        public string Inv_Condicion_Desc { get; set; }

        public bool Inv_Generado_En_Compra { get; set; }

        public List<Entidad_Movimiento_Inventario> Detalle { get; set; }
        public List<Entidad_Movimiento_Inventario> DetalleLotes { get; set; }

        public bool Es_Atendido_Traspaso { get; set; }
      public bool Inv_Es_Traspaso { get; set; }
        public decimal Inv_Base { get; set; }
        public decimal Inv_Igv { get; set; }
        public decimal Inv_Total { get; set; }

        public decimal Inv_Valor_Fact_Exonerada { get; set; }
        public decimal Inv_Exonerada { get; set; }
        public decimal Inv_Inafecta { get; set; }
        public decimal Inv_Isc_Importe { get; set; }
        public decimal Inv_Otros_Tributos_Importe { get; set; }

        public string Inv_pdc_codigo { get; set; }
        public string Inv_pdv_codigo { get; set; }

        public string Est_Facturacion { get; set; }
        public string Est_Facturacion_Interno { get; set; }
        public string Est_Facturacion_Desc { get; set; }

        public string Cja_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo { get; set; }
        public int Cja_Aper_Cierre_Codigo_Item { get; set; }
        public string cja_estado { get; set; }

        public string Inv_pdv_Nombre { get; set; }

        public string Estado_Fila { get; set; }

        public DateTime Fecha_inicio { get; set; }
        public DateTime Fecha_Fin { get; set; }

        public decimal CantidadEntrada { get; set; }
        public decimal CostoUnitarioEntrada { get; set; }
        public decimal CostoTotalEntrada { get; set; }

        public decimal CantidadSalida { get; set; }
        public decimal CostoUnitarioSalida { get; set; }
        public decimal CostoTotalSalida { get; set; }

        public string Invd_Serie { get; set; }

        public decimal Unm_Base { get; set; }
        public decimal Invd_CantidadAtendidaEnBaseAlCoefciente { get; set; }

        public string Usuario { get; set; }
        public DateTime FechaOperacionVendedor { get; set; }

    }
}
