﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
 public   class Entidad_Documentos_Punto_Venta
    {

        public string Id_Empresa { get; set; }
        public string Pdv_Codigo { get; set; }
        public string Pdv_Nombre { get; set; }
        public string Doc_Codigo { get; set; }
        public string Est_Codigo { get; set; }
        public string Est_Descripcion { get; set; }
        public string Doc_Tipo_Doc { get; set; }
        public string Id_Sunat { get; set; }
        public string Nombre_Comprobante { get; set; }
        public string Doc_Tipo_Doc_Asoc { get; set; }
        public string Id_Sunat_Asoc { get; set; }
        public string Nombre_Comprobante_Asoc { get; set; }
        public Boolean Doc_Credito_Fiscal { get; set; }
        public Boolean Doc_Por_Defecto { get; set; }
        public Boolean Doc_Es_Factura { get; set; }
        public Boolean Doc_Es_Boleta { get; set; }
        public Boolean Doc_Es_Nota_Salida { get; set; }

    }
}
