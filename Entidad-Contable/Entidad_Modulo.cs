﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
  public  class Entidad_Modulo
    {
        public string Id_Empresas { get; set; }
        public string Usuario_dni { get; set; }
        public string Id_modulo { get; set; }
        public string Mod_Descripcion { get; set; }

        public string Id_Grupo { get; set; }
        public string Gru_Descripcion { get; set; }
        public string Id_Opcion { get; set; }
        public string Opc_Descripcion { get; set; }

        public List<Entidad_Modulo> ListaOpciones { get; set; }
        public List<Entidad_Modulo> ListaEmpresas { get; set; }
        public int contar { get; set; }
        public int contar_Empre { get; set; }
    }
}
