﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Entidad_Formato_Tributario
    {
        public string bat_codigo { get; set; }
        public string bat_codigo_pdt { get; set; }
        public string bat_descripcion { get; set; }
        public string bat_formula { get; set; }

        public string bat_tipo { get; set; } // SI ES ,ACTIVO , PASIVO, NATURALEZA O FUNCION
    }
}
