﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Entidad_Empresa
    {
        public string Id_Empresa { get; set; }
        public string Emp_Nombre { get; set; }
        public string Emp_Ruc { get; set; }
        public string Emp_Direccion { get; set; }
        public string Emp_Repres1 { get; set; }
        public string Usuario_dni { get; set; }

        public string Emp_DireccionCorta { get; set; }
        public string Emp_Urbanizacion { get; set; }
        public string Emp_Direccion_Departamento { get; set; }
        public string Emp_Direccion_Provincia { get; set; }
        public string Emp_Direccion_Distrito { get; set; }

        public string Id_Anio  { get; set; }



    }
}
