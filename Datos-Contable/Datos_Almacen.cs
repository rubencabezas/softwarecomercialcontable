﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Almacen
    {
        public void Insertar(Entidad_Almacen Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ALMACEN_INSERTAR", Cn))
                    {

                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Alm_Descrcipcion", System.Data.SqlDbType.Char, 200).Value = Clas_Enti.Alm_Descrcipcion;
                        Cmd.Parameters.Add("@Alm_TipoBSA", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Alm_TipoBSA;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Almacen = Cmd.Parameters["@Id_Almacen"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Almacen Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ALMACEN_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Alm_Descrcipcion", System.Data.SqlDbType.Char, 200).Value = Clas_Enti.Alm_Descrcipcion;
                        Cmd.Parameters.Add("@Alm_TipoBSA", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Alm_TipoBSA;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Entidad_Almacen Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ALMACEN_ELIMINAR", Cn))
                    {

                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public void Eliminar(Entidad_Empresa Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Almacen> Listar(Entidad_Almacen Cls_Enti)
        {
            List<Entidad_Almacen> ListaItems = new List<Entidad_Almacen>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ALMACEN_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Almacen
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Almacen = Dr.GetString(1).Trim(),
                                    Alm_Descrcipcion = Dr.GetString(2).Trim(),
                                    Alm_TipoBSA = Dr.GetString(3).Trim(),
                                    Alm_TipoBSA_cod_interno = Dr.GetString(4).Trim(),
                                    Alm_TipoBSA_Descripcion_Det = Dr.GetString(5).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
