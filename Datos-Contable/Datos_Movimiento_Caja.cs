﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
 public   class Datos_Movimiento_Caja
    {
        public void Insertar(Entidad_Movimiento_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_MOVIMIENTO_CAJA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Tipo", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Cja_Tipo;
                        Cmd.Parameters.Add("@Cja_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Monto;
                        Cmd.Parameters.Add("@Cja_Motivo", System.Data.SqlDbType.Char).Value = Clas_Enti.Cja_Motivo;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Movimiento_Caja ent in Clas_Enti.DetalleCobroCaja)
                        {
                            Cmd.Parameters.Clear();


                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_CAJA_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Codigo;
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Codigo;
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                            Cmd.Parameters.Add("@Cja_Movimiento_Item", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Movimiento_Item;
                            Cmd.Parameters.Add("@Item", System.Data.SqlDbType.Int).Value = ent.Item;
                            Cmd.Parameters.Add("@Anio_Ref", System.Data.SqlDbType.VarChar).Value = ent.Anio_Ref;
                            Cmd.Parameters.Add("@Periodo_Ref", System.Data.SqlDbType.VarChar).Value = ent.Periodo_Ref;
                            Cmd.Parameters.Add("@Id_Tipo_Mov_Ref", System.Data.SqlDbType.VarChar).Value = ent.Id_Tipo_Mov_Ref;
                            Cmd.Parameters.Add("@Id_Almacen_Ref", System.Data.SqlDbType.VarChar).Value = ent.Id_Almacen_Ref;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Id_Movimiento_Ref", System.Data.SqlDbType.VarChar).Value = ent.Id_Movimiento_Ref;
                            Cmd.Parameters.Add("@Es_Venta_Sin_Ticket", System.Data.SqlDbType.VarChar).Value = ent.Es_Venta_Sin_Ticket;
                            Cmd.Parameters.Add("@Ruc_DNI", System.Data.SqlDbType.VarChar).Value = ent.Ruc_DNI;
                            Cmd.Parameters.Add("@Total", System.Data.SqlDbType.Decimal).Value = ent.Amortizado;
                            Cmd.Parameters.Add("@TipoDoc", System.Data.SqlDbType.VarChar).Value = ent.TipoDoc;
                            Cmd.Parameters.Add("@Serie", System.Data.SqlDbType.VarChar).Value = ent.Serie;
                            Cmd.Parameters.Add("@Numero", System.Data.SqlDbType.VarChar).Value = ent.Numero;
                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.ExecuteNonQuery();
                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Caja> Listar_Por_Pdv(Entidad_Movimiento_Caja Clas_Enti)
        {
            List<Entidad_Movimiento_Caja> ListaItems = new List<Entidad_Movimiento_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_MOVIMIENTO_CAJA_INSERTAR_LISTAR_POR_PDV", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Caja
                                {
                                    Id_Empresa=Dr.GetString(0),
                                    Pdv_Codigo= Dr.GetString(1),
                                    Cja_Codigo = Dr.GetString(2),
                                    Cja_Aper_Cierre_Codigo = Dr.GetInt32(3),
                                    Cja_Movimiento_Item = Dr.GetInt32(4),
                                    Cja_Tipo = Dr.GetString(5),
                                    Cja_Monto = Dr.GetDecimal(6),
                                    Cja_Motivo = Dr.GetString(7),
                                    Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(8),
                                    Cja_Tipo_desc = Dr.GetString(9),
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }




















    }
}
