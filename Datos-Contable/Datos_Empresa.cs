﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Empresa
    {

        public void Insertar(Entidad_Empresa Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Emp_Nombre", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Emp_Nombre;
                        Cmd.Parameters.Add("@Emp_RUC", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Emp_Ruc;
                        Cmd.Parameters.Add("@Emp_Direccion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Emp_Direccion;
                        Cmd.Parameters.Add("@Emp_Repres1", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Emp_Repres1;
                        Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = "";
                        Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = "";

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Empresa = Cmd.Parameters["@Id_Empresa"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Empresa Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Emp_Nombre", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Emp_Nombre;
                        Cmd.Parameters.Add("@Emp_RUC", System.Data.SqlDbType.Char,11).Value = Clas_Enti.Emp_Ruc;
                        Cmd.Parameters.Add("@Emp_Direccion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Emp_Direccion;
                        Cmd.Parameters.Add("@Emp_Repres1", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Emp_Repres1;
                        Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = "";
                        Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = "";
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Empresa Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Empresa> Listar(Entidad_Empresa Cls_Enti)
        {
            List<Entidad_Empresa> ListaItems = new List<Entidad_Empresa>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Cls_Enti.Usuario_dni;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Empresa
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Emp_Nombre = Dr.GetString(1),
                                    Emp_Ruc = Dr.GetString(2),
                                    Emp_Direccion = Dr.GetString(3),
                                    Emp_Repres1 = Dr.GetString(4),
                                    Emp_DireccionCorta = Dr.GetString(6),
                                    Emp_Urbanizacion = Dr.GetString(7),
                                    Emp_Direccion_Departamento = Dr.GetString(8),
                                    Emp_Direccion_Provincia = Dr.GetString(9),
                                    Emp_Direccion_Distrito = Dr.GetString(10)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Empresa> Listar_Emp(Entidad_Empresa Cls_Enti)
        {
            List<Entidad_Empresa> ListaItems = new List<Entidad_Empresa>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_LISTAR_EMP", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Empresa
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Emp_Nombre = Dr.GetString(1),
                                    Emp_Ruc = Dr.GetString(2),
                                    Emp_Direccion = Dr.GetString(3),
                                    Emp_Repres1 = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public List<Entidad_Empresa> Traer_Anio_Actual_Server(Entidad_Empresa Cls_Enti)
        {
            List<Entidad_Empresa> ListaItems = new List<Entidad_Empresa>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ANIO_ACTUAL_SERVIDOR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Empresa
                                {
                                    Id_Anio=Dr.GetString(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }










    }
}
