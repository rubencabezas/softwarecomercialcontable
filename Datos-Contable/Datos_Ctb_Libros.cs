﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace Contable
{
    public class Datos_Ctb_Libros
    {

        public void Insertar(Entidad_Ctb_Libro Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_LIBRO_CONTABLE_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Nombre_Libro", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Nombre_Libro;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_SUNAT;
                        Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Lib_CentralizacionMes", System.Data.SqlDbType.Bit).Value = Clas_Enti.Lib_CentralizacionMes;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Libro = Cmd.Parameters["@Id_Libro"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Ctb_Libro Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_LIBRO_CONTABLE_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Nombre_Libro", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Nombre_Libro;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_SUNAT;
                        Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Lib_CentralizacionMes", System.Data.SqlDbType.Bit).Value = Clas_Enti.Lib_CentralizacionMes;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Ctb_Libro Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_LIBRO_CONTABLE_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public Entidad_Ctb_Libro Listar(Entidad_Ctb_Libro Clas_Enti)
        //{
        //    //Entidad_Ctb_Libro ListaItems = new Entidad_Ctb_Libro();

        //    List<Entidad_Ctb_Libro> ListaItems = new List<Entidad_Ctb_Libro>();
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_CTB_LIBRO_CONTABLE_LISTAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
        //                Cn.Open();
        //                using (SqlDataReader Dr = Cmd.ExecuteReader())
        //                {
        //                    while (Dr.Read())
        //                    {
        //                        ListaItems.Id_Libro = Dr.GetString(0);
        //                        ListaItems.Nombre_Libro = Dr.GetString(1);
        //                        ListaItems.Id_SUNAT = Dr.GetString(2);
        //                    }
        //                }
        //            }
        //        }
        //        return ListaItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}




        public List<Entidad_Ctb_Libro> Listar(Entidad_Ctb_Libro Cls_Enti)
        {
            List<Entidad_Ctb_Libro> ListaItems = new List<Entidad_Ctb_Libro>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_LIBRO_CONTABLE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Ctb_Libro
                                {
                                    Id_Libro = Dr.GetString(0),
                                    Nombre_Libro = Dr.GetString(1),
                                    Id_SUNAT = Dr.GetString(2),
                                    Lib_CentralizacionMes=Dr.GetBoolean(3)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }















        }
    }

}
