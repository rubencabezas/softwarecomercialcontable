﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
   public class Datos_Registro_Compra_Reporte
    {
        public List<Entidad_Registro_Compra_Reporte> Traer_reporte(Entidad_Registro_Compra_Reporte Cls_Enti)
        {
            List<Entidad_Registro_Compra_Reporte> ListaItems = new List<Entidad_Registro_Compra_Reporte>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_REGISTRO_COMPRAS_REPORTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Id_Periodo) ? null : Cls_Enti.Id_Periodo);



                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Registro_Compra_Reporte
                                {
                                    Id_Voucher = Dr.GetString(0),
                                    Ctb_Fecha_Movimiento= Dr.GetString(1),
                                    Ctb_Fecha_Venci = Dr.GetString(2),
                                    Id_Sunat = Dr.GetString(3),
                                    Ctb_Serie = Dr.GetString(4),
                                    Ctb_Numero = Dr.GetString(5),
                                    Ctb_Ruc_dni = Dr.GetString(6),
                                    Entidad = Dr.GetString(7),
                                    Ctb_Base_Imponible=Dr.GetDecimal(8),
                                    Ctb_Igv=Dr.GetDecimal(9),
                                    Ctb_Base_Imponible2= Dr.GetDecimal(10),
                                    Ctb_Igv2= Dr.GetDecimal(11),
                                    Ctb_Base_Imponible3= Dr.GetDecimal(12),
                                    Ctb_Igv3= Dr.GetDecimal(13),
                                    Ctb_No_Gravadas= Dr.GetDecimal(14),
                                    Ctb_Isc_Importe= Dr.GetDecimal(15),
                                    Ctb_Otros_Tributos_Importe= Dr.GetDecimal(16),
                                    Ctb_Importe_Total= Dr.GetDecimal(17),
                                    Ctb_Fecha_Detrac=Dr.GetString(18),
                                    Ctb_Numero_Detrac=Dr.GetString(19),
                                    Ctb_Tipo_Cambio=Dr.GetDecimal(20),
                                    Ctb_Fecha_Doc_Ref=Dr.GetDateTime(21),
                                    Ctb_Serie_Doc_Ref=Dr.GetString(22),
                                    Ctb_Numero_Doc_Ref=Dr.GetString(23)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Registro_Compra_Reporte> Ple_V5(Entidad_Registro_Compra_Reporte Cls_Enti)
        {
            List<Entidad_Registro_Compra_Reporte> ListaItems = new List<Entidad_Registro_Compra_Reporte>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PLE_5_REGISTRO_COMPRAS_TXT", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;



                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Registro_Compra_Reporte
                                {
                                    Ple_V5 = Dr.GetString(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
