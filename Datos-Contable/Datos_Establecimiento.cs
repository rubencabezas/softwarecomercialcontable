﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
  public  class Datos_Establecimiento
    {

        public void Insertar(Entidad_Establecimiento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_ESTABLECIMIENTO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value=Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Est_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Descripcion;
                        Cmd.Parameters.Add("@Est_Direccion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Direccion;
                        Cmd.Parameters.Add("@Est_Telefono", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Telefono;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Empresa = Cmd.Parameters["@Est_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Establecimiento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_ESTABLECIMIENTO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value=Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Est_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Descripcion;
                        Cmd.Parameters.Add("@Est_Direccion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Direccion;
                        Cmd.Parameters.Add("@Est_Telefono", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Telefono;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Establecimiento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_ESTABLECIMIENTO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Establecimiento> Listar(Entidad_Establecimiento Cls_Enti)
        {
            List<Entidad_Establecimiento> ListaItems = new List<Entidad_Establecimiento>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_ESTABLECIMIENTO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Est_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Establecimiento
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Est_Codigo = Dr.GetString(1),
                                    Est_Descripcion = Dr.GetString(2),
                                    Est_Direccion = Dr.GetString(3),
                                    Est_Telefono = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



    }
}
