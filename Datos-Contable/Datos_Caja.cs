﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
   public class Datos_Caja
    {

        public void Insertar(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_CONFIGURACION_CAJA_INSERTAR", Cn))
                    {

                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Cja_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Descripcion;
                            Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Cja_Codigo = Cmd.Parameters["@Cja_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_CONFIGURACION_CAJA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Descripcion;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public void Eliminar(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_CONFIGURACION_CAJA_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Caja> Listar(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_CONFIGURACION_CAJA_lISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1).Trim(),
                                    Pdv_Nombre = Dr.GetString(2).Trim(),
                                    Cja_Codigo = Dr.GetString(3).Trim(),
                                    Cja_Descripcion = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Caja> Listar_Caja_Aperturado(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_APERTURA_CIERRE_CAJA_CAB_BUSCAR_APERTURADO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Estado", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Cja_Estado;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1).Trim(),
                                    Cja_Codigo = Dr.GetString(2).Trim(),
                                    Cja_Aper_Cierre_Codigo = Dr.GetInt32(3),
                                    Cja_Saldo_Inicial=Dr.GetDecimal(4),
                                    Cja_Total_Efectivo=Dr.GetDecimal(5),
                                    Cja_Total_Credito=Dr.GetDecimal(6),
                                    Cja_Total_Tarjeta=Dr.GetDecimal(7),
                                    Cja_Estado=Dr.GetString(8)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Listar_Caja_Aperturado_DET(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paGRF_APERTURA_CIERRE_CAJA_DET_BUSCAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                     
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Aper_Cierre_Codigo_Item=Dr.GetInt32(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void Insertar_Aperturando_Caja(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_APERTURA_CIERRE_CAJA_CAB_INSERTAR", Cn))
                    {

                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;

                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Cja_Saldo_Inicial", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Saldo_Inicial;
                        Cmd.Parameters.Add("@Cja_Total_Efectivo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Efectivo;

                        Cmd.Parameters.Add("@Cja_Total_Credito", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Credito;
                        Cmd.Parameters.Add("@Cja_Total_Tarjeta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Tarjeta;
                        Cmd.Parameters.Add("@Cja_Estado", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Cja_Estado;

                        Cmd.Parameters.Add("@Cja_Autor_Apertura", System.Data.SqlDbType.Char,50).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Cja_Maquina_Apertura", System.Data.SqlDbType.VarChar, 50).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cja_Responsable", System.Data.SqlDbType.Char, 11).Value = Actual_Conexion.UserID;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Cja_Aper_Cierre_Codigo = Convert.ToInt32(Cmd.Parameters["@Cja_Aper_Cierre_Codigo"].Value);
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    public void Insertar_Apertura_Det(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_APERTURA_CIERRE_CAJA_DET_INSERTAR", Cn))
                    {

                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Total_Efectivo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Efectivo;
                        Cmd.Parameters.Add("@Cja_Total_Credito", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Credito;
                        Cmd.Parameters.Add("@Cja_Total_Tarjeta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Tarjeta;

                        Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.Char,50).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar, 50).Value = Actual_Conexion.Maquina;
     

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja>Ventas_Contado_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_VENTAS_CAJA_CONTADO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo_Item);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@cja_estado", System.Data.SqlDbType.Char).Value = Cls_Enti.Cja_Estado;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Ventas_Credito_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_VENTAS_CAJA_CREDITO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo_Item);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@cja_estado", System.Data.SqlDbType.Char).Value = Cls_Enti.Cja_Estado;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Ventas_Contado_Inventario_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_VENTAS_INVENTARIO_CAJA_CONTADO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo_Item);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@cja_estado", System.Data.SqlDbType.Char).Value = Cls_Enti.Cja_Estado;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Ventas_Credito_Inventario_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_VENTAS_INVENTARIO_CAJA_CREDITO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo_Item);//Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@cja_estado", System.Data.SqlDbType.Char).Value = Cls_Enti.Cja_Estado;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Caja> Listar_Cortes_Totales(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paGRF_APERTURA_CIERRE_CAJA_DET_LISTAR_SUMAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = (Cls_Enti.Cja_Aper_Cierre_Codigo == 0 ? 0 : Cls_Enti.Cja_Aper_Cierre_Codigo);//Cls_Enti.Cja_Aper_Cierre_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0),
                                    Cja_Total_Credito=Dr.GetDecimal(1),
                                    Cja_Total_Tarjeta=Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void Actualizar_Cierre_Total_caja(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paGRF_APERTURA_CIERRE_CAJA_CAB_CERRAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Total_Efectivo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Efectivo;
                        Cmd.Parameters.Add("@Cja_Total_Credito", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Credito;
                        Cmd.Parameters.Add("@Cja_Total_Tarjeta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Total_Tarjeta;
                        Cmd.Parameters.Add("@Cja_Estado", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Estado;
                        Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value =Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Buscar_Ventas_Departamento_CierreTotal(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_VEN_REPORTE_CIERRE_TOTAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                    Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                    Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                    Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                    Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion=Dr.GetString(0),
                                    Cantidad=Dr.GetDecimal(1),
                                    Importe=Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Ventas_Contado_Credito_Corte_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_VEN_REPORTE_CORTE_CONTADO_CREDITO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo_Item;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0),
                                    Cja_Total_Credito = Dr.GetDecimal(1),
                                    Cja_Total_Tarjeta = Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
      public List<Entidad_Caja> Buscar_Ventas_Contado_Credito_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_VEN_REPORTE_CIERRE_CONTADO_CREDITO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                       
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Cja_Total_Efectivo = Dr.GetDecimal(0),
                                    Cja_Total_Credito = Dr.GetDecimal(1),
                                    Cja_Total_Tarjeta = Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Ventas_Departamento_Corte_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_VEN_REPORTE_CORTE_TOTAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo_Item;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Cantidad = Dr.GetDecimal(1),
                                    Importe = Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public List<Entidad_Caja> Saldo_Inicial_Apertura_Caja(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_VEN_SALDO_INICIAL_CAJA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Importe=Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Ventas_Sin_Documentos_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VENTAS_SIN_DOCUMENTOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Cantidad = Dr.GetDecimal(1),
                                    Importe = Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Buscar_Ventas_Sin_Documentos_Corte_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VENTAS_SIN_DOCUMENTOS_CORTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo_Item;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Cantidad = Dr.GetDecimal(1),
                                    Importe = Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Buscar_Otros_Ingresos_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_INGRESOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char,2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Otros_Ingresos_Corte_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_INGRESOS_CORTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0, DBNull.Value, Cls_Enti.Cja_Aper_Cierre_Codigo_Item);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Otros_Ingresos_Corte_Total_Before(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_INGRESOS_CORTE_BEFORE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0, DBNull.Value, Cls_Enti.Cja_Aper_Cierre_Codigo_Item);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    //Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Otros_Ingresos_Corte_Total_Todo(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_INGRESOS_CORTE_TODO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0, DBNull.Value, Cls_Enti.Cja_Aper_Cierre_Codigo_Item);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                   // Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Otros_Egresos_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_EGRESOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        public List<Entidad_Caja> Buscar_Otros_Egresos_Corte_Total(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_EGRESOS_CORTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0, DBNull.Value, Cls_Enti.Cja_Aper_Cierre_Codigo_Item);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Otros_Egresos_Corte_Total_Before(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_EGRESOS_CORTE_BEFORE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0, DBNull.Value, Cls_Enti.Cja_Aper_Cierre_Codigo_Item);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    //Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Otros_Egresos_Corte_Total_Todo(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_OTROS_EGRESOS_CORTE_TODO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@EmpresaCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PdvCodigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo_Apertura", System.Data.SqlDbType.Int).Value = Cls_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.Cja_Aper_Cierre_Codigo_Item == 0, DBNull.Value, Cls_Enti.Cja_Aper_Cierre_Codigo_Item);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    //Catalogo_Descripcion = Dr.GetString(0),
                                    Importe = Dr.GetDecimal(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja>Listado_de_Todas_las_Cajas(Entidad_Caja Cls_Enti)
        {
            List<Entidad_Caja> ListaItems = new List<Entidad_Caja>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_APERTURA_CIERRE_CAJA_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
        
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1),
                                    Pdv_Nombre=Dr.GetString(2),
                                    Cja_Codigo= Dr.GetString(3),
                                    Cja_Aper_Cierre_Codigo=Dr.GetInt32(4),
                                    Cja_Saldo_Inicial=Dr.GetDecimal(5),
                                    Cja_Total_Efectivo=Dr.GetDecimal(6),
                                    Cja_Total_Credito=Dr.GetDecimal(7),
                                    Cja_Total_Tarjeta=Dr.GetDecimal(8),
                                    Cja_Estado=Dr.GetString(9),
                                    Cja_Estado_Desc=Dr.GetString(10),
                                    Cja_Fecha_Apertura=Dr.GetDateTime(11),
                                    Cja_Autor_Apertura=Dr.GetString(12),
                                    Cja_Maquina_Apertura=Dr.GetString(13),
                                    Cja_Fecha_Cierre=Dr.GetDateTime(14),
                                    Cja_Autor_Cierre=Dr.GetString(15),
                                    Cja_Maquina_Cierre=Dr.GetString(16),
                                    Cja_Responsable=Dr.GetString(17),

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar_Saldo_Inicial(Entidad_Caja Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_APERTURA_CIERRE_CAJA_CAB_MODIFICAR_SALDOINICIAL", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cja_Codigo;
                       Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                       Cmd.Parameters.Add("@Cja_Saldo_Inicial", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cja_Saldo_Inicial;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
