﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
public    class Datos_Impuesto_Det
    {
        public void Insertar(Entidad_Impuesto_Det Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_IMPUESTO_DET_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Imd_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Imd_Codigo;
                        Cmd.Parameters.Add("@Imd_Fecha_Inicio", System.Data.SqlDbType.Date).Value = Clas_Enti.Imd_Fecha_Inicio;
                        Cmd.Parameters.Add("@Imd_Fecha_Fin", System.Data.SqlDbType.Date).Value = Clas_Enti.Imd_Fecha_Fin;
                        Cmd.Parameters.Add("@Imd_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Imd_Tasa;
                        Cmd.Parameters.Add("@Imd_Descripcion_Impresion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Imd_Descripcion_Impresion;
                        Cmd.Parameters.Add("@Imd_Tasa_Impresion", System.Data.SqlDbType.VarChar,50).Value = Clas_Enti.Imd_Tasa_Impresion;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Modificar(Entidad_Impuesto_Det Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_IMPUESTO_DET_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Imd_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Imd_Codigo;
                        Cmd.Parameters.Add("@Imd_Fecha_Inicio", System.Data.SqlDbType.Date).Value = Clas_Enti.Imd_Fecha_Inicio;
                        Cmd.Parameters.Add("@Imd_Fecha_Fin", System.Data.SqlDbType.Date).Value = Clas_Enti.Imd_Fecha_Fin;
                        Cmd.Parameters.Add("@Imd_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Imd_Tasa;
                        Cmd.Parameters.Add("@Imd_Descripcion_Impresion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Imd_Descripcion_Impresion;
                        Cmd.Parameters.Add("@Imd_Tasa_Impresion", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Imd_Tasa_Impresion;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public List<Entidad_Impuesto_Det> Listar(Entidad_Impuesto_Det Cls_Enti)
        {
            List<Entidad_Impuesto_Det> ListaItems = new List<Entidad_Impuesto_Det>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_IMPUESTO_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Imd_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Imd_Codigo;
                        //Cmd.Parameters.Add("@Imd_Fecha_Inicio", System.Data.SqlDbType.Date).Value = (Cls_Enti.Imd_Fecha_Inicio == Convert.ToDateTime("01/01/0001").Date ? null : String.Format("{0:yyyy-MM-dd}", Cls_Enti.Imd_Fecha_Inicio));

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Impuesto_Det
                                {
                                    Imd_Codigo = Dr.GetString(0),
                                    Imc_Descripcion = Dr.GetString(1).Trim(),
                                    Imd_Fecha_Inicio = Dr.GetDateTime(2),
                                    Imd_Fecha_Fin=Dr.GetDateTime(3),
                                    Imd_Tasa=Dr.GetDecimal(4),
                                    Imd_Descripcion_Impresion=Dr.GetString(5),
                                    Imd_Tasa_Impresion=Dr.GetString(6)
                                });
                            }
                        }
                    }
                }

                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Impuesto_Det> Igv_Actual(Entidad_Impuesto_Det Cls_Enti)
        {
            List<Entidad_Impuesto_Det> ListaItems = new List<Entidad_Impuesto_Det>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IGV_ACTUAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                      Cmd.Parameters.Add("@Imd_Fecha_Inicio", System.Data.SqlDbType.Date).Value = (Cls_Enti.Imd_Fecha_Inicio == Convert.ToDateTime("01/01/0001").Date ? null : String.Format("{0:yyyy-MM-dd}", Cls_Enti.Imd_Fecha_Inicio));

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Impuesto_Det
                                {
                                    Imd_Fecha_Inicio = Dr.GetDateTime(0),
                                    Imd_Fecha_Fin = Dr.GetDateTime(1),
                                    Imd_Tasa = Dr.GetDecimal(2)
                                });
                            }
                        }
                    }
                }

                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


    }


}

