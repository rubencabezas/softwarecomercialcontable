﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
 public   class Datos_Usuario
    {

        public void Insertar(Entidad_Usuario Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Usuario_dni;
                        Cmd.Parameters.Add("@Usuario_Pass", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Pass;
                        Cmd.Parameters.Add("@Usuario_Confirmar_Pass", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Confirmar_Pass;
                        Cmd.Parameters.Add("@Usuario_Rol_Id", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Rol_Id;
                        Cmd.Parameters.Add("@Usuario_Desc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Entidad;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Usuario Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Usuario_dni;
                        Cmd.Parameters.Add("@Usuario_Pass", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Pass;
                        Cmd.Parameters.Add("@Usuario_Confirmar_Pass", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Confirmar_Pass;
                        Cmd.Parameters.Add("@Usuario_Rol_Id", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Rol_Id;
                        Cmd.Parameters.Add("@Usuario_Desc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_Entidad;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Entidad_Usuario Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Usuario_dni;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Usuario> Listar(Entidad_Usuario Cls_Enti)
        {
            List<Entidad_Usuario> ListaItems = new List<Entidad_Usuario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 11).Value = Cls_Enti.Usuario_dni;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Usuario
                                {
                                    Usuario_dni=Dr.GetString(0),
                                    Usuario_Entidad=Dr.GetString(1),
                                    Usuario_Pass=Dr.GetString(2),
                                    Usuario_Confirmar_Pass=Dr.GetString(3),
                                    Usuario_Rol_Id=Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        //public List<Entidad_Entidad> Listar_Tipo_Ent(Entidad_Entidad Cls_Enti)
        //{
        //    List<Entidad_Entidad> ListaItems = new List<Entidad_Entidad>();
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_ENTIDAD_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
        //            {
        //                Cmd.Parameters.Add("@Id_Tipo_Ent", System.Data.SqlDbType.Char, 1).Value = Cls_Enti.Id_Tipo_Ent;
        //                Cn.Open();
        //                using (SqlDataReader Dr = Cmd.ExecuteReader())
        //                {
        //                    while (Dr.Read())
        //                    {
        //                        ListaItems.Add(new Entidad_Entidad
        //                        {
        //                            Id_Tipo_Ent = Dr.GetString(0),
        //                            Ent_Descripcion = Dr.GetString(1)
        //                        });
        //                    }
        //                }
        //            }
        //        }
        //        return ListaItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }


        //}
    }
}
