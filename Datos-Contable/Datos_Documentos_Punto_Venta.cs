﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
 public   class Datos_Documentos_Punto_Venta
    {
        public void Insertar(Entidad_Documentos_Punto_Venta Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_PUNTO_VENTA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Doc_Codigo", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Doc_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Doc_Tipo_Doc;
                        Cmd.Parameters.Add("@Doc_Tipo_Doc_Asoc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Doc_Tipo_Doc_Asoc;
                        Cmd.Parameters.Add("@Doc_Credito_Fiscal", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Credito_Fiscal;
                        Cmd.Parameters.Add("@Doc_Por_Defecto", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Por_Defecto;
                        Cmd.Parameters.Add("@Doc_Es_Factura", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Es_Factura;
                        Cmd.Parameters.Add("@Doc_Es_Boleta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Es_Boleta;
                        Cmd.Parameters.Add("@Doc_Es_Nota_Salida", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Es_Nota_Salida;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Empresa = Cmd.Parameters["@Doc_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Documentos_Punto_Venta Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_PUNTO_VENTA_MODIFICA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Doc_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Doc_Codigo;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Doc_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Doc_Tipo_Doc;
                        Cmd.Parameters.Add("@Doc_Tipo_Doc_Asoc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Doc_Tipo_Doc_Asoc;
                        Cmd.Parameters.Add("@Doc_Credito_Fiscal", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Credito_Fiscal;
                        Cmd.Parameters.Add("@Doc_Por_Defecto", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Por_Defecto;
                        Cmd.Parameters.Add("@Doc_Es_Factura", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Es_Factura;
                        Cmd.Parameters.Add("@Doc_Es_Boleta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Es_Boleta;
                        Cmd.Parameters.Add("@Doc_Es_Nota_Salida", System.Data.SqlDbType.Bit).Value = Clas_Enti.Doc_Es_Nota_Salida;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public void Eliminar(Entidad_Empresa Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Documentos_Punto_Venta> Listar(Entidad_Documentos_Punto_Venta Cls_Enti)
        {
            List<Entidad_Documentos_Punto_Venta> ListaItems = new List<Entidad_Documentos_Punto_Venta>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_PUNTO_VENTA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Pdv_Codigo) ? null : Cls_Enti.Pdv_Codigo); //Cls_Enti.@Pdv_Codigo;
                        Cmd.Parameters.Add("@Doc_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Doc_Codigo) ? null : Cls_Enti.Doc_Codigo); //Cls_Enti.@Pdv_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Documentos_Punto_Venta
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1),
                                    Pdv_Nombre = Dr.GetString(2),
                                    Doc_Codigo = Dr.GetString(3),
                                    Est_Codigo = Dr.GetString(4),
                                    Est_Descripcion = Dr.GetString(5),
                                    Doc_Tipo_Doc = Dr.GetString(6),
                                    Id_Sunat = Dr.GetString(7),
                                    Nombre_Comprobante = Dr.GetString(8),
                                    Doc_Tipo_Doc_Asoc = Dr.GetString(9),
                                    Id_Sunat_Asoc = Dr.GetString(10),
                                    Nombre_Comprobante_Asoc = Dr.GetString(11),
                                    Doc_Credito_Fiscal = Dr.GetBoolean(12),
                                    Doc_Por_Defecto = Dr.GetBoolean(13),
                                    Doc_Es_Factura = Dr.GetBoolean(14),
                                    Doc_Es_Boleta = Dr.GetBoolean(15),
                                    Doc_Es_Nota_Salida = Dr.GetBoolean(16)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
