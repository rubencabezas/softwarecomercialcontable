﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
 public   class Datos_Parametro_Inicial
    {
        public void Insertar(Entidad_Parametro_Inicial Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Actual_Conexion.AnioSelect;
                        Cmd.Parameters.Add("@Ini_Venta", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ini_Venta.Trim();
                        Cmd.Parameters.Add("@Ini_Compra", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ini_Compra.Trim();
                        Cmd.Parameters.Add("@Ini_Diario", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ini_Diario.Trim();
                        Cmd.Parameters.Add("@Ini_Inventario", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ini_Inventario.Trim();
                        Cmd.Parameters.Add("@Ini_CajaBanco", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ini_CajaBanco.Trim();

                        Cmd.Parameters.Add("@Ini_Rendicion_Caja_Chica", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ini_Rendicion_Caja_Chica.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_Ganancia_Dif_Cambio", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Ganancia_Dif_Cambio.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_Perdida_Dif_Cambio", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Perdida_Dif_Cambio.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_Ganancia_Redondeo", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Ganancia_Redondeo.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_Perdida_Redondeo", System.Data.SqlDbType.Char,12).Value = Clas_Enti.Ini_Cta_Perdida_Redondeo.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_ITF", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_ITF.Trim();

                        Cmd.Parameters.Add("@Ini_Cta_Caja", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Caja.Trim();
 
                        Cmd.Parameters.Add("@Ini_Cta_Percepcion", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Percepcion.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_Detraccion", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Detraccion.Trim();
                        Cmd.Parameters.Add("@Ini_Cta_Retencion", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Retencion.Trim();

                        Cmd.Parameters.Add("@Ini_Cta_Igv", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Ini_Cta_Igv.Trim();

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Parametro_Inicial> Listar(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_Venta = Dr.GetString(2).Trim(),
                                    Ini_Venta_Desc = Dr.GetString(3).Trim(),
                                    Ini_Venta_sunat = Dr.GetString(4).Trim(),
                                    Ini_Compra = Dr.GetString(5).Trim(),
                                    Ini_Compra_Desc = Dr.GetString(6).Trim(),
                                    Ini_Compra_sunat = Dr.GetString(7).Trim(),
                                    Ini_Diario = Dr.GetString(8).Trim(),
                                    Ini_Diario_Desc = Dr.GetString(9).Trim(),
                                    Ini_Diario_sunat = Dr.GetString(10).Trim(),
                                    Ini_Inventario = Dr.GetString(11).Trim(),
                                    Ini_Inventario_Desc=Dr.GetString(12).Trim(),
                                    Ini_Inventario_sunat=Dr.GetString(13).Trim(),
                                    Ini_CajaBanco = Dr.GetString(14).Trim(),
                                    Ini_CajaBanco_Desc = Dr.GetString(15).Trim(),
                                    Ini_CajaBanco_sunat = Dr.GetString(16).Trim(),
                                    Ini_Rendicion_Caja_Chica = Dr.GetString(17).Trim(),
                                    Ini_Rendicion_Caja_Chica_Desc = Dr.GetString(18).Trim(),
                                    Ini_Rendicion_Caja_Chica_Sunat = Dr.GetString(19).Trim(),
                                    Ini_Cta_Ganancia_Dif_Cambio = Dr.GetString(20).Trim(),
                                    Ini_Cta_Ganancia_Dif_Cambio_Desc = Dr.GetString(21).Trim(),
                                    Ini_Cta_Perdida_Dif_Cambio = Dr.GetString(22).Trim(),
                                    Ini_Cta_Perdida_Dif_Cambio_Desc = Dr.GetString(23).Trim(),
                                    Ini_Cta_Ganancia_Redondeo = Dr.GetString(24).Trim(),
                                    Ini_Cta_Ganancia_Redondeo_Desc = Dr.GetString(25).Trim(),
                                    Ini_Cta_Perdida_Redondeo = Dr.GetString(26).Trim(),
                                    Ini_Cta_Perdida_Redondeo_Desc = Dr.GetString(27).Trim(),
                                    Ini_Cta_ITF = Dr.GetString(28).Trim(),
                                    Ini_Cta_ITF_Desc = Dr.GetString(29).Trim(),
                                    Ini_Cta_Caja=Dr.GetString(30).Trim(),
                                    Ini_Cta_Caja_Desc=Dr.GetString(31).Trim(),
                                    Ini_Cta_Percepcion = Dr.GetString(32).Trim(),
                                    Ini_Cta_Percepcion_Desc = Dr.GetString(33).Trim(),
                                    Ini_Cta_Detraccion = Dr.GetString(34).Trim(),
                                    Ini_Cta_Detraccion_Desc = Dr.GetString(35).Trim(),
                                    Ini_Cta_Retencion = Dr.GetString(36).Trim(),
                                    Ini_Cta_Retencion_Desc = Dr.GetString(37).Trim(),
                                    Ini_Cta_Igv=Dr.GetString(38).Trim(),
                                    Ini_Cta_Igv_Desc=Dr.GetString(39).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public List<Entidad_Parametro_Inicial> Traer_Libro_Compra(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_TRAER_LIBRO_COMPRAS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_Compra = Dr.GetString(2).Trim(),
                                    Ini_Compra_Desc = Dr.GetString(3).Trim(),
                                    Ini_Compra_sunat = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Parametro_Inicial> Traer_Libro_Venta(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_TRAER_LIBRO_VENTA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_Venta = Dr.GetString(2).Trim(),
                                    Ini_Venta_Desc = Dr.GetString(3).Trim(),
                                    Ini_Venta_sunat = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_CajaBanco(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_TRAER_CAJA_BANCO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_CajaBanco = Dr.GetString(2).Trim(),
                                    Ini_CajaBanco_Desc = Dr.GetString(3).Trim(),
                                    Ini_CajaBanco_sunat = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_RendicionesCajaChica(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_TRAER_RENDCION_CAJA_CHICA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_Rendicion_Caja_Chica= Dr.GetString(2).Trim(),
                                    Ini_Rendicion_Caja_Chica_Desc = Dr.GetString(3).Trim(),
                                    Ini_Rendicion_Caja_Chica_Sunat = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Parametro_Inicial> Traer_Libro_Diario(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_TRAER_LIBRO_DIARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_Diario = Dr.GetString(2).Trim(),
                                    Ini_Diario_Desc = Dr.GetString(3).Trim(),
                                    Ini_Diario_sunat = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Parametro_Inicial> Traer_Libro_Inventario(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PARAMETROS_INICIAL_TRAER_LIBRO_INVENTARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Ini_Inventario = Dr.GetString(2).Trim(),
                                    Ini_Inventario_Desc = Dr.GetString(3).Trim(),
                                    Ini_Inventario_sunat = Dr.GetString(4).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public List<Entidad_Parametro_Inicial> Traer_Cuenta_Caja(Entidad_Parametro_Inicial Cls_Enti)
        {
            List<Entidad_Parametro_Inicial> ListaItems = new List<Entidad_Parametro_Inicial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paCont_ListCtaCaja_CTB_PARAMETRO_INI", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Parametro_Inicial
                                {
                                    Ini_Cta_Caja=Dr.GetString(0),
                                    Ini_Cta_Caja_Desc=Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
    }
}
