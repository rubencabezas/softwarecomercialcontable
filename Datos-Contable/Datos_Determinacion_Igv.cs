﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Datos_Determinacion_Igv
    {

        public void Eliminar_Anual(Entidad_Determinacion_Igv Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_DETERMINACION_IGV_ELIMINAR_ANUAL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Clas_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Clas_Enti.Anio;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }

        public void Eliminar_Mensual(Entidad_Determinacion_Igv Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_DETERMINACION_IGV_ELIMINAR_MENSUAL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Clas_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Clas_Enti.Anio;
                        command.Parameters.Add("@Periodo", SqlDbType.Char, 4).Value = Clas_Enti.Periodo;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }


        public void Insertar_Anual(Entidad_Determinacion_Igv Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_DETERMINACION_IGV_INSERTAR_ANUAL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Clas_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Clas_Enti.Anio;
                        command.Parameters.Add("@Credito_Fiscal_Mes_Anterior", SqlDbType.Decimal).Value = Clas_Enti.Credito_Fiscal_Mes_Anterior;
                        command.Parameters.Add("@Per_Mes_Anterior", SqlDbType.Decimal).Value = Clas_Enti.Per_Mes_Anterior;
                        command.Parameters.Add("@Ret_Mes_Anterior", SqlDbType.Decimal).Value = Clas_Enti.Ret_Mes_Anterior;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }

        public void Insertar_Mensual(Entidad_Determinacion_Igv Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_DETERMINACION_IGV_INSERTAR_MENSUAL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Clas_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Clas_Enti.Anio;
                        command.Parameters.Add("@Periodo", SqlDbType.Char, 2).Value = Clas_Enti.Periodo;
                        command.Parameters.Add("@Per_Aplicadas", SqlDbType.Decimal).Value = Clas_Enti.Per_Aplicadas;
                        command.Parameters.Add("@Ret_Aplicadas", SqlDbType.Decimal).Value = Clas_Enti.Ret_Aplicadas;
                        command.Parameters.Add("@Compensacion_IGV", SqlDbType.Decimal).Value = Clas_Enti.Compensacion_IGV;
                        command.Parameters.Add("@Devuelto_CNC_Exportad", SqlDbType.Decimal).Value = Clas_Enti.Devuelto_CNC_Exportad;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }


        public List<Entidad_Determinacion_Igv> Listar_Anual(Entidad_Determinacion_Igv Cls_Enti)
        {
            List<Entidad_Determinacion_Igv> list2;
            List<Entidad_Determinacion_Igv> list = new List<Entidad_Determinacion_Igv>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("pa_CTB_DETERMINACION_IGV_LISTAR_ANUAL", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Cls_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Cls_Enti.Anio;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Determinacion_Igv item = new Entidad_Determinacion_Igv();
                                item.Empresa = reader.GetString(0);
                                item.Anio = reader.GetString(1);
                                item.Credito_Fiscal_Mes_Anterior = reader.GetDecimal(2);
                                item.Per_Mes_Anterior = reader.GetDecimal(3);
                                item.Ret_Mes_Anterior = reader.GetDecimal(4);
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }

        public List<Entidad_Determinacion_Igv> Listar_Mensual(Entidad_Determinacion_Igv Cls_Enti)
        {
            List<Entidad_Determinacion_Igv> list2;
            List<Entidad_Determinacion_Igv> list = new List<Entidad_Determinacion_Igv>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("pa_CTB_DETERMINACION_IGV_LISTAR_MENSUAL", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Cls_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Cls_Enti.Anio;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Determinacion_Igv item = new Entidad_Determinacion_Igv();
                                item.Empresa = reader.GetString(0);
                                item.Anio = reader.GetString(1);
                                item.Periodo = reader.GetString(2);
                                item.Periodo_Desc = reader.GetString(3);
                                item.Per_Aplicadas = reader.GetDecimal(4);
                                item.Ret_Aplicadas = reader.GetDecimal(5);
                                item.Compensacion_IGV = reader.GetDecimal(6);
                                item.Devuelto_CNC_Exportad = reader.GetDecimal(7);
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }

        public void Modificar_Anual(Entidad_Determinacion_Igv Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_DETERMINACION_IGV_MODIFICAR_ANUAL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Clas_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Clas_Enti.Anio;
                        command.Parameters.Add("@Credito_Fiscal_Mes_Anterior", SqlDbType.Decimal).Value = Clas_Enti.Credito_Fiscal_Mes_Anterior;
                        command.Parameters.Add("@Per_Mes_Anterior", SqlDbType.Decimal).Value = Clas_Enti.Per_Mes_Anterior;
                        command.Parameters.Add("@Ret_Mes_Anterior", SqlDbType.Decimal).Value = Clas_Enti.Ret_Mes_Anterior;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }

        public void Modificar_Mensual(Entidad_Determinacion_Igv Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_DETERMINACION_IGV_MODIFICAR_MENSUAL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = Clas_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = Clas_Enti.Anio;
                        command.Parameters.Add("@Periodo", SqlDbType.Char, 2).Value = Clas_Enti.Periodo;
                        command.Parameters.Add("@Per_Aplicadas", SqlDbType.Decimal).Value = Clas_Enti.Per_Aplicadas;
                        command.Parameters.Add("@Ret_Aplicadas", SqlDbType.Decimal).Value = Clas_Enti.Ret_Aplicadas;
                        command.Parameters.Add("@Compensacion_IGV", SqlDbType.Decimal).Value = Clas_Enti.Compensacion_IGV;
                        command.Parameters.Add("@Devuelto_CNC_Exportad", SqlDbType.Decimal).Value = Clas_Enti.Devuelto_CNC_Exportad;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }


    }
}
