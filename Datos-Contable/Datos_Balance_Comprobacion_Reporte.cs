﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
  public  class Datos_Balance_Comprobacion_Reporte
    {
        public List<Entidad_Balance_Comprobacion_Reporte> Balance_Acumulado(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            List<Entidad_Balance_Comprobacion_Reporte> ListaItems = new List<Entidad_Balance_Comprobacion_Reporte>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_RPT_BALANCE_COMPROBACION_ACUMULADO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Nivel", System.Data.SqlDbType.Int).Value = Cls_Enti.Es_Num_Digitos;
                        Cmd.Parameters.Add("@Mon_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Moneda;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Balance_Comprobacion_Reporte
                                {
                                    Cuenta=Dr.GetString(0),
                                    Descripcion=Dr.GetString(1),
                                    IsFuncion=Dr.GetBoolean(2),
                                    IsNaturaleza=Dr.GetBoolean(3),
                                    IsInventario=Dr.GetBoolean(4),
                                    Debe=Dr.GetDecimal(5),
                                    Haber=Dr.GetDecimal(6)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Balance_Comprobacion_Reporte> Balance_Mensual(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            List<Entidad_Balance_Comprobacion_Reporte> ListaItems = new List<Entidad_Balance_Comprobacion_Reporte>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_RPT_BALANCE_COMPROBACION_MENSUAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Nivel", System.Data.SqlDbType.Int).Value = Cls_Enti.Es_Num_Digitos;
                        Cmd.Parameters.Add("@Mon_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Moneda;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Balance_Comprobacion_Reporte
                                {
                                    Cuenta = Dr.GetString(0),
                                    Descripcion = Dr.GetString(1),
                                    IsFuncion = Dr.GetBoolean(2),
                                    IsNaturaleza = Dr.GetBoolean(3),
                                    IsInventario = Dr.GetBoolean(4),
                                    Debe = Dr.GetDecimal(5),
                                    Haber = Dr.GetDecimal(6)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Balance_Comprobacion_Reporte> Ver_Detalle_Cuenta_Balance_General(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            List<Entidad_Balance_Comprobacion_Reporte> ListaItems = new List<Entidad_Balance_Comprobacion_Reporte>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_Detalle_Cuenta_Balance_General", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@PMon_cCodigo", System.Data.SqlDbType.Char,3).Value = Cls_Enti.Id_Moneda;
                        Cmd.Parameters.Add("@PCta_Contable", System.Data.SqlDbType.Char,12).Value = Cls_Enti.Cuenta;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Balance_Comprobacion_Reporte
                                {
                                    Ctb_Cuenta = Dr.GetString(0),
                                    Id_Voucher = Dr.GetString(1),
                                    Ctb_Tipo_Doc_det = Dr.GetString(2),
                                    Nombre_Comprobante = Dr.GetString(3),
                                    Doc_Id_Sunat = Dr.GetString(4),
                                    Ctb_Serie_det = Dr.GetString(5),
                                    Ctb_Numero_det = Dr.GetString(6),
                                    Ctb_Ruc_dni_det=Dr.GetString(7),
                                    Entidad=Dr.GetString(8),
                                    Ctb_Fecha_Mov_det=Dr.GetDateTime(9),
                                    Nombre_Moneda=Dr.GetString(10),
                                    Mon_Id_Sunat=Dr.GetString(11),
                                    Ctb_Tipo_Cambio_Cod_Det=Dr.GetString(12),
                                    Ctb_Tipo_Cambio_Valor_Det=Dr.GetDecimal(13),
                                    Ctb_Importe_Debe=Dr.GetDecimal(14),
                                    Ctb_Importe_Haber=Dr.GetDecimal(15),
                                    Ctb_Importe_Debe_Extr=Dr.GetDecimal(16),
                                    Ctb_Importe_Haber_Extr=Dr.GetDecimal(17),
                                    Nombre_Libro=Dr.GetString(18)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Balance_Comprobacion_Reporte> Ver_Detalle_Cuenta_Balance_General_Acumulado(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            List<Entidad_Balance_Comprobacion_Reporte> ListaItems = new List<Entidad_Balance_Comprobacion_Reporte>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_Detalle_Cuenta_Balance_General_Acumulado", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@PMon_cCodigo", System.Data.SqlDbType.Char,3).Value = Cls_Enti.Id_Moneda;
                        Cmd.Parameters.Add("@PCta_Contable", System.Data.SqlDbType.Char,12).Value = Cls_Enti.Cuenta;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Balance_Comprobacion_Reporte
                                {
                                    Ctb_Cuenta = Dr.GetString(0),
                                    Id_Voucher = Dr.GetString(1),
                                    Ctb_Tipo_Doc_det = Dr.GetString(2),
                                    Nombre_Comprobante = Dr.GetString(3),
                                    Doc_Id_Sunat = Dr.GetString(4),
                                    Ctb_Serie_det = Dr.GetString(5),
                                    Ctb_Numero_det = Dr.GetString(6),
                                    Ctb_Ruc_dni_det=Dr.GetString(7),
                                    Entidad=Dr.GetString(8),
                                    Ctb_Fecha_Mov_det_=Dr.GetString(9),
                                    Nombre_Moneda=Dr.GetString(10),
                                    Mon_Id_Sunat=Dr.GetString(11),
                                    Ctb_Tipo_Cambio_Cod_Det=Dr.GetString(12),
                                    Ctb_Tipo_Cambio_Valor_Det=Dr.GetDecimal(13),
                                    Ctb_Importe_Debe=Dr.GetDecimal(14),
                                    Ctb_Importe_Haber=Dr.GetDecimal(15),
                                    Ctb_Importe_Debe_Extr=Dr.GetDecimal(16),
                                    Ctb_Importe_Haber_Extr=Dr.GetDecimal(17),
                                    Nombre_Libro=Dr.GetString(18)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
