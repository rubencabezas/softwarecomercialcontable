﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
   public class Datos_Moneda
    {

        public void Insertar(Entidad_Moneda Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MONEDA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Moneda", System.Data.SqlDbType.Char, 3).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Nombre_Moneda", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Nombre_Moneda;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Sunat;
  Cmd.Parameters.Add("@Mon_Nacional", System.Data.SqlDbType.Bit).Value = Clas_Enti.Es_nacional;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Moneda = Cmd.Parameters["@Id_Moneda"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Moneda Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MONEDA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Moneda", System.Data.SqlDbType.Char, 3).Value=Clas_Enti.Id_Moneda;
                        Cmd.Parameters.Add("@Nombre_Moneda", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Nombre_Moneda;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Sunat;
                        Cmd.Parameters.Add("@Mon_Nacional", System.Data.SqlDbType.Bit).Value = Clas_Enti.Es_nacional;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Moneda Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MONEDA_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Moneda", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Moneda;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Moneda> Listar(Entidad_Moneda Cls_Enti)
        {
            List<Entidad_Moneda> ListaItems = new List<Entidad_Moneda>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MONEDA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Moneda", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Moneda;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Sunat;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Moneda
                                {
                                    Id_Moneda = Dr.GetString(0),
                                    Nombre_Moneda = Dr.GetString(1),
                                    Id_Sunat = Dr.GetString(2),
                                    Es_nacional=Dr.GetBoolean(3)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }



    }
}
