﻿using Contable;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
    public class Datos_Operaciones
    {

        public List<Entidad_Operaciones> Listar_Operaciones_Venta(Entidad_Operaciones Cls_Enti)
        {
            List<Entidad_Operaciones> ListaItems = new List<Entidad_Operaciones>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("up_FACTE_CFG_TIPO_AFECTACION_IGV_BUSCAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@PAfecIGV_Codigo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.AfecIGV_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Operaciones
                                {
                                    AfecIGV_Codigo = Dr.GetString(0),
                                    AfecIGV_Descripcion = Dr.GetString(1),
                                    AfecIGV_Tabla = Dr.GetString(2),
                                    Gen_Codigo_Interno = Dr.GetString(3),
                                    Gen_Descripcion_Det = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Operaciones> Traer_Operacion_venta_Por_Producto(Entidad_Operaciones Cls_Enti)
        {
            List<Entidad_Operaciones> ListaItems = new List<Entidad_Operaciones>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_TIPO_OPERACION_PRODUCTO_VENTA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Catalogo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Operaciones
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Tipo = Dr.GetString(1),
                                    Id_Catalogo = Dr.GetString(2),
                                    AfecIGV_Tabla = Dr.GetString(3),
                                    AfecIGV_Codigo = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}