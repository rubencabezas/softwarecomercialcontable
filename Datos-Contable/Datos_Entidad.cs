﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
   public class Datos_Entidad
    {
        public void Insertar(Entidad_Entidad Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char, 11).Value=Clas_Enti.Ent_RUC_DNI;
                        Cmd.Parameters.Add("@Ent_Tipo_Persona", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ent_Tipo_Persona;
                        Cmd.Parameters.Add("@Ent_Tipo_Doc", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ent_Tipo_Doc;
                        Cmd.Parameters.Add("@Ent_Razon_Social_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Razon_Social_Nombre;
                        Cmd.Parameters.Add("@Ent_Ape_Paterno", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Ape_Paterno;
                        Cmd.Parameters.Add("@Ent_Ape_Materno", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Ape_Materno;
                        Cmd.Parameters.Add("@Ent_Telefono", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Telefono;
                        Cmd.Parameters.Add("@Ent_Repre_Legal", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Repre_Legal;
                        Cmd.Parameters.Add("@Ent_Telefono_movil", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Telefono_movil;
                        Cmd.Parameters.Add("@Ent_Domicilio_Fiscal", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Domicilio_Fiscal;
                        Cmd.Parameters.Add("@Ent_Correo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Correo;
                        Cmd.Parameters.Add("@Ent_Pagina_Web", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Pagina_Web;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Entidad Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Ent_RUC_DNI;
                        Cmd.Parameters.Add("@Ent_Tipo_Persona", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ent_Tipo_Persona;
                        Cmd.Parameters.Add("@Ent_Tipo_Doc", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ent_Tipo_Doc;
                        Cmd.Parameters.Add("@Ent_Razon_Social_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Razon_Social_Nombre;
                        Cmd.Parameters.Add("@Ent_Ape_Paterno", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Ape_Paterno;
                        Cmd.Parameters.Add("@Ent_Ape_Materno", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Ape_Materno;
                        Cmd.Parameters.Add("@Ent_Telefono", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Telefono;
                        Cmd.Parameters.Add("@Ent_Repre_Legal", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Repre_Legal;
                        Cmd.Parameters.Add("@Ent_Telefono_movil", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Telefono_movil;
                        Cmd.Parameters.Add("@Ent_Domicilio_Fiscal", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Domicilio_Fiscal;
                        Cmd.Parameters.Add("@Ent_Correo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Correo;
                        Cmd.Parameters.Add("@Ent_Pagina_Web", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Pagina_Web;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;



                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Modificar_RAZON_DIRECCION(Entidad_Entidad Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_MODIFICAR_RAZON_Y_DIRECCION", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Ent_RUC_DNI;
                        Cmd.Parameters.Add("@Ent_Razon_Social_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Razon_Social_Nombre;
                        Cmd.Parameters.Add("@Ent_Ape_Paterno", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Ape_Paterno;
                        Cmd.Parameters.Add("@Ent_Ape_Materno", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Ape_Materno;
                        Cmd.Parameters.Add("@Ent_Domicilio_Fiscal", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Domicilio_Fiscal;
                        Cmd.Parameters.Add("@Ent_Correo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ent_Correo;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;



                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Entidad_Entidad Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Ent_RUC_DNI;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.Char, 50).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.Char, 50).Value = Actual_Conexion.Maquina;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Entidad> Listar(Entidad_Entidad Cls_Enti)
        {
            List<Entidad_Entidad> ListaItems = new List<Entidad_Entidad>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char,11).Value = Cls_Enti.Ent_RUC_DNI;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Entidad
                                {
                                    Ent_RUC_DNI = Dr.GetString(0),
                                    Ent_Tipo_Persona = Dr.GetString(1),
                                    Ent_Tipo_Persona_Cod_Interno = Dr.GetString(2),
                                    Ent_Tipo_Persona_Descripcion = Dr.GetString(3),
                                    Ent_Tipo_Doc = Dr.GetString(4),
                                    Ent_Tipo_Doc_Cod_Interno = Dr.GetString(5),
                                    Ent_Tipo_Doc_Descripcion=Dr.GetString(6),
                                    Ent_Razon_Social_Nombre=Dr.GetString(7),
                                    Ent_Ape_Paterno=Dr.GetString(8),
                                    Ent_Ape_Materno=Dr.GetString(9),
                                    Ent_Telefono=Dr.GetString(10),
                                    Ent_Repre_Legal=Dr.GetString(11),
                                    Ent_Telefono_movil=Dr.GetString(12),
                                    Ent_Domicilio_Fiscal=Dr.GetString(13),
                                    Ent_Correo=Dr.GetString(14),
                                    Ent_Pagina_Web=Dr.GetString(15)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public List<Entidad_Entidad> Listar_Tipo_Ent(Entidad_Entidad Cls_Enti)
        {
            List<Entidad_Entidad> ListaItems = new List<Entidad_Entidad>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_ENTIDAD_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Tipo_Ent", System.Data.SqlDbType.Char, 1).Value = Cls_Enti.Id_Tipo_Ent;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Entidad
                                {
                                    Id_Tipo_Ent=Dr.GetString(0),
                                    Ent_Descripcion=Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
