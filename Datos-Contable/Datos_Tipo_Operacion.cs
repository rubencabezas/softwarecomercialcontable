﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
 public   class Datos_Tipo_Operacion
    {

        public void Insertar(Entidad_Tipo_Operacion Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_TIPO_OPERACION_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Operacion", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Top_Entrada", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Entrada;
                        Cmd.Parameters.Add("@Top_Salida", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Salida;
                        Cmd.Parameters.Add("@Top_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Top_Descripcion;
                        Cmd.Parameters.Add("@Top_Sunat", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Top_Sunat;

                        Cmd.Parameters.Add("@Top_NecesitaCC", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_NecesitaCC;
                        Cmd.Parameters.Add("@Top_NecesitaCG", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_NecesitaCG;
                        Cmd.Parameters.Add("@Top_Genera_Asiento", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Genera_Asiento;
                        Cmd.Parameters.Add("@Top_Necesita_Doc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Necesita_Doc;

                        Cmd.Parameters.Add("@Top_Tipo_Doc_Defecto", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Top_Tipo_Doc_Defecto;
                        Cmd.Parameters.Add("@Top_Es_Saldo_Inicial", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Es_Saldo_Inicial;
                        Cmd.Parameters.Add("@Top_Es_Valorizado", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Es_Valorizado;
                        Cmd.Parameters.Add("@Top_Es_Salida_Produccion", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Es_Salida_Produccion;

                        Cmd.Parameters.Add("@Top_ES_Venta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_ES_Venta;
                        Cmd.Parameters.Add("@Top_Requiere_Transferecia_Almacen", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Requiere_Transferecia_Almacen;

                        Cmd.Parameters.Add("@Top_Requiere_Tipo_Entidad", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Requiere_Tipo_Entidad;
                        Cmd.Parameters.Add("@Top_Tipo_Entidad_Cod", System.Data.SqlDbType.Char).Value = Clas_Enti.Top_Tipo_Entidad_Cod;
                        

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Operacion = Cmd.Parameters["@Id_Operacion"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Tipo_Operacion Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_TIPO_OPERACION_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Operacion", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Operacion;
                        Cmd.Parameters.Add("@Top_Entrada", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Entrada;
                        Cmd.Parameters.Add("@Top_Salida", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Salida;
                        Cmd.Parameters.Add("@Top_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Top_Descripcion;
                        Cmd.Parameters.Add("@Top_Sunat", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Top_Sunat;

                        Cmd.Parameters.Add("@Top_NecesitaCC", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_NecesitaCC;
                        Cmd.Parameters.Add("@Top_NecesitaCG", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_NecesitaCG;
                        Cmd.Parameters.Add("@Top_Genera_Asiento", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Genera_Asiento;
                        Cmd.Parameters.Add("@Top_Necesita_Doc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Necesita_Doc;

                        Cmd.Parameters.Add("@Top_Tipo_Doc_Defecto", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Top_Tipo_Doc_Defecto;
                        Cmd.Parameters.Add("@Top_Es_Saldo_Inicial", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Es_Saldo_Inicial;
                        Cmd.Parameters.Add("@Top_Es_Valorizado", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Es_Valorizado;
                        Cmd.Parameters.Add("@Top_Es_Salida_Produccion", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Es_Salida_Produccion;

                        Cmd.Parameters.Add("@Top_ES_Venta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_ES_Venta;
                        Cmd.Parameters.Add("@Top_Requiere_Transferecia_Almacen", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Requiere_Transferecia_Almacen;

                        Cmd.Parameters.Add("@Top_Requiere_Tipo_Entidad", System.Data.SqlDbType.Bit).Value = Clas_Enti.Top_Requiere_Tipo_Entidad;
                        Cmd.Parameters.Add("@Top_Tipo_Entidad_Cod", System.Data.SqlDbType.Char).Value = Clas_Enti.Top_Tipo_Entidad_Cod;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina; Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public void Eliminar(Entidad_Centro_Costo_Gasto Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_CTB_CENTRO_COSTO_GASTO_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
        //                Cmd.Parameters.Add("@Id_Centro_cg", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Centro_cg;
        //                Cmd.Parameters.Add("@Id_Unidad", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Centro_cg;
        //                Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
        //                Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Tipo_Operacion> Listar(Entidad_Tipo_Operacion Cls_Enti)
        {
            List<Entidad_Tipo_Operacion> ListaItems = new List<Entidad_Tipo_Operacion>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_TIPO_OPERACION_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Operacion", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Operacion;
                                             Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Tipo_Operacion
                                {
                                    Id_Operacion = Dr.GetString(0),
                                    Top_Entrada = Dr.GetBoolean(1),
                                    Top_Salida = Dr.GetBoolean(2),
                                    Top_Descripcion = Dr.GetString(3),
                                    Top_Sunat = Dr.GetString(4),
                                    Top_NecesitaCC = Dr.GetBoolean(5),
                                    Top_NecesitaCG = Dr.GetBoolean(6),
                                    Top_Genera_Asiento = Dr.GetBoolean(7),
                                    Top_Necesita_Doc = Dr.GetBoolean(8),
                                    Top_Tipo_Doc_Defecto = Dr.GetString(9),
                                    Top_Tipo_Doc_Defecto_Desc = Dr.GetString(10),
                                    Top_Es_Saldo_Inicial = Dr.GetBoolean(11),
                                    Top_Es_Valorizado=Dr.GetBoolean(12),
                                    Top_Es_Salida_Produccion=Dr.GetBoolean(13),
                                    Top_ES_Venta=Dr.GetBoolean(14),
                                    Top_Requiere_Transferecia_Almacen=Dr.GetBoolean(15),
                                   Top_Requiere_Tipo_Entidad=Dr.GetBoolean(16),
                                    Top_Tipo_Entidad_Cod=Dr.GetString(17),
                                    Ent_Descripcion=Dr.GetString(18)

                            });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
