﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
  public  class Datos_Punto_Compra
    {
        public void Insertar(Entidad_Punto_Compra Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_PUNTO_COMPRA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdc_Codigo", System.Data.SqlDbType.VarChar, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Pdc_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdc_Nombre;
                        Cmd.Parameters.Add("@Pdc_Almacen", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdc_Almacen;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Empresa = Cmd.Parameters["@Pdc_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Punto_Compra Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_PUNTO_COMPRA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdc_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdc_Codigo;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Pdc_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdc_Nombre;
                        Cmd.Parameters.Add("@Pdc_Almacen", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdc_Almacen;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Punto_Compra Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_PUNTO_COMPRA_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdc_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdc_Codigo;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Punto_Compra> Listar(Entidad_Punto_Compra Cls_Enti)
        {
            List<Entidad_Punto_Compra> ListaItems = new List<Entidad_Punto_Compra>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_PUNTO_COMPRA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdc_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Pdc_Codigo) ? null : Cls_Enti.Pdc_Codigo); //Cls_Enti.@Pdv_Codigo;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Est_Codigo) ? null : Cls_Enti.Est_Codigo);

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Punto_Compra
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdc_Codigo = Dr.GetString(1),
                                    Est_Codigo = Dr.GetString(2),
                                    Est_Descripcion = Dr.GetString(3).Trim(),
                                    Pdc_Nombre = Dr.GetString(4).Trim(),
                                    Pdc_Almacen = Dr.GetString(5),
                                    Alm_Descrcipcion = Dr.GetString(6).Trim(),
                                    Pdc_Almacen_BSA_Cod_=Dr.GetString(7),
                                    Pdc_Almacen_Cod_BSA_Interno = Dr.GetString(8),
                                    Pdc_Almacen_BSA_Interno_Desc = Dr.GetString(9)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
