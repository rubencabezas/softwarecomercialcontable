﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
namespace Contable
{
 public   class Datos_Series_Tesoreria
    {

        public void Insertar(Entidad_Series_Tesorerira Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_SERIES_TESORERIA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Tip_IE", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ser_Tip_IE;
                        Cmd.Parameters.Add("@Ser_Tipo_Mov", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ser_Tipo_Mov;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Serie;
                        Cmd.Parameters.Add("@Ser_Numero_Inicio", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Inicio;
                        Cmd.Parameters.Add("@Ser_Numero_Fin", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ser_Numero_Fin;
                        Cmd.Parameters.Add("@Ser_Numero_Actual", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ser_Numero_Actual;
                        Cmd.Parameters.Add("@Ser_Emitiendose", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Emitiendose;
                        Cmd.Parameters.Add("@Ser_Es_CtaCorriente", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Es_CtaCorriente;
                        Cmd.Parameters.Add("@Ser_Cta_Corriente", System.Data.SqlDbType.VarChar,30).Value = Clas_Enti.Ser_Cta_Corriente;
                        Cmd.Parameters.Add("@Ser_Es_CajaChica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Es_CajaChica;
                        Cmd.Parameters.Add("@Ser_Codigo_Caja", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ser_Codigo_Caja;

                        Cmd.Parameters.Add("@Ser_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Ser_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Ser_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Cierre_Caja_Chica;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                 
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Series_Tesorerira Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_SERIES_TESORERIA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Tip_IE", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Tip_IE;
                        Cmd.Parameters.Add("@Ser_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Tipo_Mov;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Serie;
                        Cmd.Parameters.Add("@Ser_Numero_Inicio", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Inicio;
                        Cmd.Parameters.Add("@Ser_Numero_Fin", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Fin;
                        Cmd.Parameters.Add("@Ser_Emitiendose", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Emitiendose;
                        Cmd.Parameters.Add("@Ser_Es_CtaCorriente", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Es_CtaCorriente;
                        Cmd.Parameters.Add("@Ser_Cta_Corriente", System.Data.SqlDbType.VarChar, 30).Value = Clas_Enti.Ser_Cta_Corriente;
                        Cmd.Parameters.Add("@Ser_Es_CajaChica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Es_CajaChica;
                        Cmd.Parameters.Add("@Ser_Codigo_Caja", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ser_Codigo_Caja;

                        Cmd.Parameters.Add("@Ser_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Ser_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Ser_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ser_Cierre_Caja_Chica;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public void Eliminar(Entidad_Grupo Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_GRUPO_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
        //                Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
        //                Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
        //                Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Series_Tesorerira> Listar(Entidad_Series_Tesorerira Cls_Enti)
        {
            List<Entidad_Series_Tesorerira> ListaItems = new List<Entidad_Series_Tesorerira>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_SERIES_TESORERIA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Tip_IE", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ser_Tip_IE;
                        Cmd.Parameters.Add("@Ser_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ser_Tipo_Mov;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ser_Serie;
            
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Series_Tesorerira
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Ser_Tipo_Doc = Dr.GetString(1),
                                    Ser_Tipo_Doc_Des = Dr.GetString(2),
                                    Ser_Tipo_Doc_Sunat=Dr.GetString(3),
                                    Ser_Tip_IE=Dr.GetString(4),
                                    Ser_Tip_IE_Interno=Dr.GetString(5),
                                    Ser_Tip_IE_Desc=Dr.GetString(6),
                                    Ser_Tipo_Mov=Dr.GetString(7),
                                    Ser_Tipo_Mov_Interno=Dr.GetString(8),
                                    Ser_Tipo_Mov_Desc=Dr.GetString(9),
                                    Ser_Serie=Dr.GetString(10),
                                    Ser_Numero_Inicio=Dr.GetString(11),
                                    Ser_Numero_Fin=Dr.GetString(12),
                                    Ser_Numero_Actual=Dr.GetString(13),
                                    Ser_Emitiendose=Dr.GetBoolean(14),
                                    Ser_Es_CtaCorriente=Dr.GetBoolean(15),
                                    Ser_Cta_Corriente=Dr.GetString(16),
                                    Ser_Es_CajaChica=Dr.GetBoolean(17),
                                    Ser_Codigo_Caja=Dr.GetString(18),
                                    Ser_Caja_Desc=Dr.GetString(19),
                                    Ser_Rendicion_Caja_Chica = Dr.GetBoolean(20),
                                    Ser_Reembolso_Caja_Chica = Dr.GetBoolean(21),
                                    Ser_Cierre_Caja_Chica=Dr.GetBoolean(22)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Series_Tesorerira> Mostrar_Series_Caja(Entidad_Series_Tesorerira Cls_Enti)
        {
            List<Entidad_Series_Tesorerira> ListaItems = new List<Entidad_Series_Tesorerira>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_SERIES_CAJA_CHICA_MOSTRAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;

                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Tip_IE", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ser_Tip_IE;
                        Cmd.Parameters.Add("@Ser_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ser_Tipo_Mov;
                        Cmd.Parameters.Add("@Ser_Es_CajaChica", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ser_Es_CajaChica;
                        Cmd.Parameters.Add("@Ser_Codigo_Caja", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Ser_Codigo_Caja;

                        Cmd.Parameters.Add("@Ser_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ser_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Ser_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ser_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Ser_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ser_Cierre_Caja_Chica;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Series_Tesorerira
                                {
                                    Ser_Tipo_Doc = Dr.GetString(0),
                                    Ser_Tipo_Doc_Sunat = Dr.GetString(1),
                                    Ser_Tipo_Doc_Des = Dr.GetString(2),
                                    Ser_Serie = Dr.GetString(3),
                                   Ser_Numero_Actual = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
