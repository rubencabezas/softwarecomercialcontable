﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Impuesto
    {
        public void Insertar(Entidad_Impuesto Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_IMPUESTO_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Imc_Codigo", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Imc_Descripcion", System.Data.SqlDbType.Char, 200).Value=Clas_Enti.Imc_Descripcion;
                        Cmd.Parameters.Add("@Imc_Abreviado", System.Data.SqlDbType.Char, 50).Value = Clas_Enti.Imc_Abreviado;
                        Cmd.Parameters.Add("@Imc_EsVigente", System.Data.SqlDbType.Bit).Value = Clas_Enti.Imc_Es_Vigente;
                        
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Imc_Codigo = Cmd.Parameters["@Imc_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Impuesto Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_IMPUESTO_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Imc_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Imc_Codigo;
                        Cmd.Parameters.Add("@Imc_Descripcion", System.Data.SqlDbType.Char, 200).Value = Clas_Enti.Imc_Descripcion;
                        Cmd.Parameters.Add("@Imc_Abreviado", System.Data.SqlDbType.Char, 50).Value = Clas_Enti.Imc_Abreviado;
                        Cmd.Parameters.Add("@Imc_EsVigente", System.Data.SqlDbType.Bit).Value = Clas_Enti.Imc_Es_Vigente;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


  

        public List<Entidad_Impuesto> Listar(Entidad_Impuesto Cls_Enti)
        {
            List<Entidad_Impuesto> ListaItems = new List<Entidad_Impuesto>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_IMPUESTO_CAB_LISTRA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Imc_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Imc_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Impuesto
                                {
                                    Imc_Codigo = Dr.GetString(0),
                                    Imc_Descripcion = Dr.GetString(1).Trim(),
                                    Imc_Abreviado = Dr.GetString(2).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }

}
