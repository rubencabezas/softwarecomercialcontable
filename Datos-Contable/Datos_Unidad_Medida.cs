﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Unidad_Medida
    {

        public void Insertar(Entidad_Unidad_Medida Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_UNIDAD_MEDIDA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Und_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Und_Descripcion;
                        Cmd.Parameters.Add("@Und_Abreviado", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Und_Abreviado;
                        Cmd.Parameters.Add("@Und_SunaT", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Und_SunaT;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Und_Codigo_FE", System.Data.SqlDbType.VarChar).Value = Clas_Enti.UnmFE;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Unidad_Medida = Cmd.Parameters["@Id_Unidad_Medida"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Unidad_Medida Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_UNIDAD_MEDIDA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Unidad_Medida;
                        Cmd.Parameters.Add("@Und_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Und_Descripcion;

                        Cmd.Parameters.Add("@Und_Abreviado", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Und_Abreviado;
                        Cmd.Parameters.Add("@Und_SunaT", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Und_SunaT;
                        Cmd.Parameters.Add("@Und_Codigo_FE", System.Data.SqlDbType.VarChar).Value = Clas_Enti.UnmFE;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public void Eliminar(Entidad_Empresa Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_EMPRESA_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Unidad_Medida> Listar(Entidad_Unidad_Medida Cls_Enti)
        {
            List<Entidad_Unidad_Medida> ListaItems = new List<Entidad_Unidad_Medida>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_UNIDAD_MEDIDA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Unidad_Medida;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Unidad_Medida
                                {
                                    Id_Unidad_Medida = Dr.GetString(0),
                                    Und_Descripcion = Dr.GetString(1),
                                    Und_Abreviado = Dr.GetString(2),
                                    Und_SunaT = Dr.GetString(3),
                                    UnmFE = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Unidad_Medida> Listar_Unm_Producto(Entidad_Unidad_Medida Cls_Enti)
        {
            List<Entidad_Unidad_Medida> ListaItems = new List<Entidad_Unidad_Medida>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_UNIDAD_MEDIDA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Unidad_Medida;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char).Value = Cls_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Unm_Defecto", System.Data.SqlDbType.Bit).Value = Cls_Enti.Unm_Defecto;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Unidad_Medida
                                {
                                    Id_Unidad_Medida = Dr.GetString(0),
                                    Und_Descripcion = Dr.GetString(1),
                                    Und_Abreviado = Dr.GetString(2),
                                    Und_SunaT = Dr.GetString(3),
                                    Unm_Defecto = Dr.GetBoolean(4),
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Unidad_Medida> Listar_Cat_Unm(Entidad_Unidad_Medida Cls_Enti)
        {
            List<Entidad_Unidad_Medida> ListaItems = new List<Entidad_Unidad_Medida>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_UNM_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {

                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        //Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = Cls_Enti.Id_Item;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Unidad_Medida
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Tipo = Dr.GetString(1),
                                    Id_Catalogo = Dr.GetString(2),
                                    Id_Item_Det = Dr.GetInt32(3),
                                    Id_Unidad_Medida = Dr.GetString(4),
                                    Und_Descripcion = Dr.GetString(5),
                                    Unm_Base = Dr.GetDecimal(6),
                                    Unm_Defecto = Dr.GetBoolean(7)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
