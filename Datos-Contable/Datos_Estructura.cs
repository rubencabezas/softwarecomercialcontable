﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
 public   class Datos_Estructura


    {

        public void Insertar(Entidad_Estructura Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ESTRUCTURA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Est_Descripcion", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Est_Descripcion;
                        Cmd.Parameters.Add("@Est_Num_Digitos", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Est_Num_Digitos;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value =Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Estructura = Cmd.Parameters["@Id_Estructura"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Estructura Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ESTRUCTURA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Estructura;
                        Cmd.Parameters.Add("@Est_Descripcion", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Est_Descripcion;
                        Cmd.Parameters.Add("@Est_Num_Digitos", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Est_Num_Digitos;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = "";
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = "";
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Estructura Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ESTRUCTURA_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Estructura;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Estructura> Listar(Entidad_Estructura Cls_Enti)
        {
            List<Entidad_Estructura> ListaItems = new List<Entidad_Estructura>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ESTRUCTURA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                       Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 1).Value = (Cls_Enti.Id_Estructura == null ? null : Cls_Enti.Id_Estructura);
                        Cmd.Parameters.Add("@Est_Num_Digitos", System.Data.SqlDbType.Char, 2).Value = (Cls_Enti.Est_Num_Digitos == null ? null : Cls_Enti.Est_Num_Digitos);

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Estructura
                                {
                                    Id_Estructura = Dr.GetString(0),
                                    Est_Descripcion = Dr.GetString(1),
                                    Est_Num_Digitos=Dr.GetString(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
