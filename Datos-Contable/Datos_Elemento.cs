﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
   public class Datos_Elemento
    {
        public void Insertar(Entidad_Elemento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ELEMENTO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Char,1).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ele_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Ele_Descripcion;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = "";
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = "";

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Elemento = Cmd.Parameters["@Id_Elemento"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Elemento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ELEMENTO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Char,1).Value = Clas_Enti.Id_Elemento;
                        Cmd.Parameters.Add("@Ele_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Ele_Descripcion;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = "";
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = "";
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Elemento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ELEMENTO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Char,1).Value = Clas_Enti.Id_Elemento;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = "";
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = "";
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Elemento> Listar(Entidad_Elemento Cls_Enti)
        {
            List<Entidad_Elemento> ListaItems = new List<Entidad_Elemento>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ELEMENTO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        //Cmd.Parameters.Add("@PImporteDoc", SqlDbType.Decimal, 18, 2).Value = (Provision.Importe == 0 ? null : Provision.Importe);
                        //Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Int).Value = (Cls_Enti.Id_Elemento==0 ? 0 : /*Cls_Enti.Id_Elemento*/);
                        Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Char,1).Value = (Cls_Enti.Id_Elemento == null? null : Cls_Enti.Id_Elemento);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Elemento
                                {
                                    Id_Elemento = Dr.GetString(0),
                                    Ele_Descripcion = Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



    }
}
