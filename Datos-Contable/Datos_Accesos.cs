﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
    public class Datos_Accesos
    {
        public List<Entidad_Accesos> Listar(Entidad_Accesos Cls_Enti)
        {
            List<Entidad_Accesos> ListaItems = new List<Entidad_Accesos>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_ACCESOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Id_Opcion", System.Data.SqlDbType.Char).Value = Cls_Enti.Id_Opcion;
                        Cmd.Parameters.Add("@Modulo", System.Data.SqlDbType.Char).Value = Cls_Enti.Modulo_Cod;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Accesos
                                {
                                    //Usuario_dni=Dr.GetString(0),
                                    //Id_Opcion=Dr.GetString(1)
                                    Id_Grupo=Dr.GetString(0),
                                    Grupo_Descripcion=Dr.GetString(1),
                                    Id_Opcion=Dr.GetString(2),
                                    Opcion_Descripcion=Dr.GetString(3),
                                    Nombre_frm=Dr.GetString(4)
                                    
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }




        public List<Entidad_Accesos> Listar_Opciones_Por_Usuario(Entidad_Accesos Cls_Enti)
        {
            List<Entidad_Accesos> ListaItems = new List<Entidad_Accesos>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LISTAR_OPCIONES_USUARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Accesos
                                {

                                    Id_Grupo=Dr.GetString(0).Trim(),
                                    Grupo_Descripcion=Dr.GetString(1).Trim(),
                                    Id_Opcion=Dr.GetString(2).Trim(),
                                    Opcion_Descripcion=Dr.GetString(3).Trim(),
                                    Nombre_frm=Dr.GetString(4).Trim(),
                                    Icono_opc=Dr.GetString(5).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }












    }
}
