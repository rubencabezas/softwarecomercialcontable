﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Contable
{
 public   class Datos_Cuenta_Corriente
    {

        public void Insertar(Entidad_Cuenta_Corriente Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CUENTA_CORRIENTE_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta_Corriente", System.Data.SqlDbType.VarChar, 30).Value = Clas_Enti.Id_Cuenta_Corriente;
                        Cmd.Parameters.Add("@Cor_Ent_Financiera", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Cor_Ent_Financiera;

                        Cmd.Parameters.Add("@Cor_Moneda_cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cor_Moneda_cod;
                        Cmd.Parameters.Add("@Cor_Cuenta_Contable", System.Data.SqlDbType.VarChar, 10).Value = Clas_Enti.Cor_Cuenta_Contable;
                       Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Cuenta_Corriente Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CUENTA_CORRIENTE_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta_Corriente", System.Data.SqlDbType.VarChar, 30).Value = Clas_Enti.Id_Cuenta_Corriente;
                        Cmd.Parameters.Add("@Cor_Ent_Financiera", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Cor_Ent_Financiera;

                        Cmd.Parameters.Add("@Cor_Moneda_cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Cor_Moneda_cod;
                        Cmd.Parameters.Add("@Cor_Cuenta_Contable", System.Data.SqlDbType.VarChar, 10).Value = Clas_Enti.Cor_Cuenta_Contable;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public void Eliminar(Entidad_Medio_Pago Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Ent_RUC_DNI;
        //                Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.Char, 50).Value = Actual_Conexion.UserName;
        //                Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.Char, 50).Value = Actual_Conexion.Maquina;


        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}



        public List<Entidad_Cuenta_Corriente> Listar(Entidad_Cuenta_Corriente Clas_Enti)
        {
            List<Entidad_Cuenta_Corriente> ListaItems = new List<Entidad_Cuenta_Corriente>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CUENTA_CORRIENTE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta_Corriente", System.Data.SqlDbType.VarChar, 30).Value = Clas_Enti.Id_Cuenta_Corriente;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Cuenta_Corriente
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Cuenta_Corriente=Dr.GetString(2),
                                    Cor_Ent_Financiera = Dr.GetString(3),
                                    Fin_Descripcion = Dr.GetString(4),
                                    Cor_Moneda_cod=Dr.GetString(5),
                                    Nombre_Moneda=Dr.GetString(6),
                                    Cor_Cuenta_Contable=Dr.GetString(7),
                                    Cta_Descripcion=Dr.GetString(8)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
