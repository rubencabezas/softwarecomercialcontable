﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
  public  class Datos_Cargo
    {

        public void Insertar(Entidad_Cargo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_CARGO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Cargo", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Car_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Car_Descripcion;
                                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Cargo = Cmd.Parameters["@Id_Cargo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Cargo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_CARGO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Cargo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Cargo;
                        Cmd.Parameters.Add("@Car_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Car_Descripcion;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public void Eliminar(Entidad_Grupo Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_GRUPO_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
        //                Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
        //                Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
        //                Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Cargo> Listar(Entidad_Cargo Cls_Enti)
        {
            List<Entidad_Cargo> ListaItems = new List<Entidad_Cargo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_CARGO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Cargo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Cargo;
                  
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Cargo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Cargo = Dr.GetString(1),
                                    Car_Descripcion = Dr.GetString(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


    }
}
