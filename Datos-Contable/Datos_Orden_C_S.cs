﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
    public class Datos_Orden_C_S
    {

        public void Insertar(Entidad_Orden_C_S Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_ORDEN_C_S_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Id_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Orden;
                        Cmd.Parameters.Add("@Ord_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ord_Tipo_Doc;
                        Cmd.Parameters.Add("@Ord_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ord_Serie;

                        Cmd.Parameters.Add("@Ord_Numero", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ord_Numero;
                        Cmd.Parameters.Add("@Ord_Proveedor_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ord_Proveedor_Ruc_dni;
                        Cmd.Parameters.Add("@Ord_Responsable_Ruc_dni", System.Data.SqlDbType.VarChar,11).Value = Clas_Enti.Ord_Responsable_Ruc_dni;
                        Cmd.Parameters.Add("@Ord_Observacion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Ord_Observacion;
                        Cmd.Parameters.Add("@Ord_Condicion_Cod", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ord_Condicion_Cod;
                        Cmd.Parameters.Add("@Ord_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ord_Fecha;
                        Cmd.Parameters.Add("@Ord_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ord_Moneda_Cod;
                        Cmd.Parameters.Add("@Ord_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ord_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ord_Tipo_Cambio_Desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ord_Tipo_Cambio_Desc;
                        Cmd.Parameters.Add("@Ord_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ord_Sub_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Sub_Total;
                        Cmd.Parameters.Add("@Ord_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Igv;
                        Cmd.Parameters.Add("@Ord_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Importe_Total;
                        Cmd.Parameters.Add("@Ord_Ubicaion", System.Data.SqlDbType.Char,4).Value = (Clas_Enti.Ord_Ubicaion == null ? "" : Clas_Enti.Ord_Ubicaion) ;
                        Cmd.Parameters.Add("@Ord_Inventario", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ord_Inventario;
                        Cmd.Parameters.Add("Ord_Igv_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Igv_Tasa;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Ord_Dias", System.Data.SqlDbType.Int).Value = Clas_Enti.Ord_Dias;
                        Cmd.Parameters.Add("@Ord_Fecha_Vencimiento", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ord_Fecha_Venci;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();



                        foreach (Entidad_Orden_C_S ent in Clas_Enti.Detalle)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_LOG_ORDEN_C_S_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Id_TIpo_Orden", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Orden;

                            Cmd.Parameters.Add("@Ord_Item ", System.Data.SqlDbType.Int).Value = ent.Ord_Item;
                            Cmd.Parameters.Add("@Ord_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Ord_Tipo_BSA;
                            Cmd.Parameters.Add("@Ord_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Ord_Catalogo;
                            Cmd.Parameters.Add("@Ord_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = ent.Ord_Descripcion;
                            Cmd.Parameters.Add("@Ord_Almacen", System.Data.SqlDbType.Char, 5).Value =ent.Ord_Almacen ;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ord_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ord_Cantidad;
                            Cmd.Parameters.Add("@Ord_Precio_Unitario", System.Data.SqlDbType.Decimal).Value = ent.Ord_Precio_Unitario;
                            Cmd.Parameters.Add("@Ord_Incluye_Igv", System.Data.SqlDbType.Decimal).Value = ent.Ord_Incluye_Igv;
                            Cmd.Parameters.Add("@Ord_Igv_Porcentaje", System.Data.SqlDbType.Char).Value = ent.Ord_Igv_Porcentaje;//(string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);
                            
                            Cmd.ExecuteNonQuery();
                        }


                        foreach (Entidad_Orden_C_S ent in Clas_Enti.Detalle_Requerimiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_LOG_ORDEN_C_S_DOC_REFERENCIA_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Id_TIpo_Orden", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Orden;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Doc_Item;
                            Cmd.Parameters.Add("@Doc_Proceso", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Proceso;
                            Cmd.Parameters.Add("@Doc_Anio", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Anio;
                            Cmd.Parameters.Add("@Doc_Periodo", System.Data.SqlDbType.VarChar, 2).Value = ent.Doc_Periodo;
                            Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Doc_Folio;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                           Cmd.ExecuteNonQuery();
                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }



        }

        public void Modificar(Entidad_Orden_C_S Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_ORDEN_C_S_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Id_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Orden;
                        Cmd.Parameters.Add("@Ord_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ord_Tipo_Doc;
                        Cmd.Parameters.Add("@Ord_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ord_Serie;

                        Cmd.Parameters.Add("@Ord_Numero", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ord_Numero;
                        Cmd.Parameters.Add("@Ord_Proveedor_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ord_Proveedor_Ruc_dni;
                        Cmd.Parameters.Add("@Ord_Responsable_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ord_Responsable_Ruc_dni;
                        Cmd.Parameters.Add("@Ord_Observacion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Ord_Observacion;
                        Cmd.Parameters.Add("@Ord_Condicion_Cod", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ord_Condicion_Cod;
                        Cmd.Parameters.Add("@Ord_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ord_Fecha;
                        Cmd.Parameters.Add("@Ord_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ord_Moneda_Cod;
                        Cmd.Parameters.Add("@Ord_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ord_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ord_Tipo_Cambio_Desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ord_Tipo_Cambio_Desc;
                        Cmd.Parameters.Add("@Ord_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ord_Sub_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Sub_Total;
                        Cmd.Parameters.Add("@Ord_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Igv;
                        Cmd.Parameters.Add("@Ord_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Importe_Total;
                        Cmd.Parameters.Add("@Ord_Ubicaion", System.Data.SqlDbType.Char, 4).Value = (Clas_Enti.Ord_Ubicaion == null ? "" : Clas_Enti.Ord_Ubicaion);
                        Cmd.Parameters.Add("@Ord_Inventario", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ord_Inventario;
                        Cmd.Parameters.Add("Ord_Igv_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ord_Igv_Tasa;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.Parameters.Add("@Ord_Dias", System.Data.SqlDbType.Int).Value = Clas_Enti.Ord_Dias;
                        Cmd.Parameters.Add("@Ord_Fecha_Vencimiento", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ord_Fecha_Venci;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();



                        foreach (Entidad_Orden_C_S ent in Clas_Enti.Detalle)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_LOG_ORDEN_C_S_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Id_TIpo_Orden", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Orden;

                            Cmd.Parameters.Add("@Ord_Item ", System.Data.SqlDbType.Int).Value = ent.Ord_Item;
                            Cmd.Parameters.Add("@Ord_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Ord_Tipo_BSA;
                            Cmd.Parameters.Add("@Ord_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Ord_Catalogo;
                            Cmd.Parameters.Add("@Ord_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = ent.Ord_Descripcion;
                            Cmd.Parameters.Add("@Ord_Almacen", System.Data.SqlDbType.Char, 5).Value = ent.Ord_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ord_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ord_Cantidad;
                            Cmd.Parameters.Add("@Ord_Precio_Unitario", System.Data.SqlDbType.Decimal).Value = ent.Ord_Precio_Unitario;
                            Cmd.Parameters.Add("@Ord_Incluye_Igv", System.Data.SqlDbType.Decimal).Value = ent.Ord_Incluye_Igv;
                            Cmd.Parameters.Add("@Ord_Igv_Porcentaje", System.Data.SqlDbType.Char).Value = ent.Ord_Igv_Porcentaje;//(string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);

                            Cmd.ExecuteNonQuery();
                        }

                        foreach (Entidad_Orden_C_S ent in Clas_Enti.Detalle_Requerimiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_LOG_ORDEN_C_S_DOC_REFERENCIA_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Id_TIpo_Orden", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Orden;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Doc_Item;
                            Cmd.Parameters.Add("@Doc_Proceso", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Proceso;
                            Cmd.Parameters.Add("@Doc_Anio", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Anio;
                            Cmd.Parameters.Add("@Doc_Periodo", System.Data.SqlDbType.VarChar, 2).Value = ent.Doc_Periodo;
                            Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Doc_Folio;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.ExecuteNonQuery();
                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }



        }
        public List<Entidad_Orden_C_S> Listar(Entidad_Orden_C_S Cls_Enti)
        {
            List<Entidad_Orden_C_S> ListaItems = new List<Entidad_Orden_C_S>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_ORDEN_C_S_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Id_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Orden;// (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Orden_C_S
                                {
                                    Id_Empresa=Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Movimiento = Dr.GetString(3),
                                    Id_Tipo_Orden = Dr.GetString(4),
                                    Id_Tipo_Orden_det = Dr.GetString(5),
                                    Id_Tipo_Orden_des = Dr.GetString(6),
                                    Ord_Tipo_Doc = Dr.GetString(7),
                                    Ord_Tipo_Doc_desc = Dr.GetString(8),
                                    Ord_Serie = Dr.GetString(9),
                                    Ord_Numero = Dr.GetString(10),
                                    Ord_Proveedor_Ruc_dni = Dr.GetString(11),
                                    Ord_Proveedor_Ruc_dni_desc = Dr.GetString(12),
                                    Ord_Responsable_Ruc_dni = Dr.GetString(13),
                                    Ord_Responsable_Ruc_dni_desc = Dr.GetString(14),
                                    Ord_Observacion = Dr.GetString(15),
                                    Ord_Condicion_Cod = Dr.GetString(16),
                                    Ord_Condicion_det = Dr.GetString(17),
                                    Ord_Condicion_desc = Dr.GetString(18),
                                    Ord_Fecha=Dr.GetDateTime(19),
                                    Ord_Moneda_Cod = Dr.GetString(20),
                                    Ord_Moneda_desc = Dr.GetString(21),
                                    Ord_Tipo_Cambio_Cod = Dr.GetString(22),
                                    Ord_Tipo_Cambio_Desc = Dr.GetString(23),
                                    Ord_Tipo_Cambio_Valor=Dr.GetDecimal(24),
                                    Ord_Sub_Total=Dr.GetDecimal(25),
                                    Ord_Igv=Dr.GetDecimal(26),
                                    Ord_Importe_Total=Dr.GetDecimal(27),
                                    Ord_Dias=Dr.GetInt32(28),
                                    Ord_Fecha_Venci=Dr.GetDateTime(29)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Orden_C_S> Listar_Det(Entidad_Orden_C_S Cls_Enti)
        {
            List<Entidad_Orden_C_S> ListaItems = new List<Entidad_Orden_C_S>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_ORDEB_C_S_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Id_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Orden;// (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Orden_C_S
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Movimiento = Dr.GetString(3),
                                    Id_Tipo_Orden = Dr.GetString(4),
                                    Ord_Item=Dr.GetInt32(5),
                                    Ord_Tipo_BSA = Dr.GetString(6).Trim(),
                                    Ord_Tipo_BSA_Interno = Dr.GetString(7).Trim(),
                                    Ord_Tipo_BSA_Desc = Dr.GetString(8).Trim(),
                                    Ord_Catalogo = Dr.GetString(9).Trim(),
                                    Ord_Catalogo_desc = Dr.GetString(10).Trim(),
                                    Ord_Descripcion = Dr.GetString(11).Trim(),
                                    Ord_Almacen = Dr.GetString(12).Trim(),
                                    Ord_Almacen_desc = Dr.GetString(13).Trim(),
                                    Ord_Cantidad = Dr.GetDecimal(14),
                                    Ord_Precio_Unitario = Dr.GetDecimal(15),
                                    Ord_Incluye_Igv = Dr.GetBoolean(16),
                                    Ord_Igv_Porcentaje = Dr.GetDecimal(17)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    public List<Entidad_Orden_C_S> Listar_Req(Entidad_Orden_C_S Cls_Enti)
        {
            List<Entidad_Orden_C_S> ListaItems = new List<Entidad_Orden_C_S>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_ORDEN_C_S_DOC_REFERENCIA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Id_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Orden;// (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Orden_C_S
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Movimiento = Dr.GetString(3),
                                    Id_Tipo_Orden = Dr.GetString(4),
                                    Doc_Item=Dr.GetInt32(5),
                                    Doc_Proceso=Dr.GetString(6),
                                    Doc_Anio=Dr.GetString(7),
                                    Doc_Periodo=Dr.GetString(8),
                                    Doc_Folio=Dr.GetString(9)
                                  
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


    }
}
