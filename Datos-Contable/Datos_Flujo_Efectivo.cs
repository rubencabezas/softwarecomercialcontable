﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Datos_Flujo_Efectivo
    {

        public void Eliminar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_ESTADO_FLUJOS_EFECTIVO_ELIMINAR", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@id", SqlDbType.Int).Value = Clas_Enti.ID;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }
        public List<Entidad_Flujo_Efectivo> Flujo_Efectivo_Listar(Entidad_Flujo_Efectivo Cls_Enti)
        {
            List<Entidad_Flujo_Efectivo> list2;
            List<Entidad_Flujo_Efectivo> list = new List<Entidad_Flujo_Efectivo>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("pa_CTB_ESTADO_FLUJOS_EFECTIVO_LISTAR", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = IIf(Cls_Enti.ID == 0, null, Cls_Enti.ID);
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 6).Value = (Cls_Enti.Codigo == null) ? null : Cls_Enti.Codigo;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Flujo_Efectivo item = new Entidad_Flujo_Efectivo();
                                item.Flujo_Efectivo_Cod = reader.GetString(0).Trim();
                                item.Flujo_Efectivo_Desc = reader.GetString(1).Trim();
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }


        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }
        public List<Entidad_Flujo_Efectivo> Flujo_Efectivo_Reporte(Entidad_Flujo_Efectivo Cls_Enti)
        {
            List<Entidad_Flujo_Efectivo> list2;
            List<Entidad_Flujo_Efectivo> list = new List<Entidad_Flujo_Efectivo>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("Pa_FLUJO_EFECTIVO_REPORTE", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = (Cls_Enti.Empresa == null) ? null : Cls_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = (Cls_Enti.Anio == null) ? null : Cls_Enti.Anio;
                        command.Parameters.Add("@Periodo", SqlDbType.Char, 2).Value = (Cls_Enti.Periodo01 == null) ? null : Cls_Enti.Periodo01;
                        command.Parameters.Add("@Acumulado", SqlDbType.Bit).Value = Cls_Enti.Acumulado;
                        command.Parameters.Add("@Periodo2", SqlDbType.Char, 2).Value = (Cls_Enti.Periodo02 == null) ? null : Cls_Enti.Periodo02;
                        command.Parameters.Add("@Acumulado2", SqlDbType.Bit).Value = Cls_Enti.Acumulado2;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Flujo_Efectivo item = new Entidad_Flujo_Efectivo();
                                item.Flujo_Efectivo_Cod = reader.GetString(0).Trim();
                                item.Flujo_Efectivo_Desc = reader.GetString(1).Trim();
                                item.Montos = reader.GetDecimal(2);
                                item.Montos2 = reader.GetDecimal(3);
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }


        public void Insertar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_ESTADO_FLUJOS_EFECTIVO_INSERTAR", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 6).Value = Clas_Enti.Flujo_Efectivo_Cod;
                        command.Parameters.Add("@Descripcion", SqlDbType.VarChar, 300).Value = Clas_Enti.Flujo_Efectivo_Desc;
                        command.Parameters.Add("@Grupo", SqlDbType.Int).Value = Clas_Enti.Flujo_Efectivo_Grupo;
                        command.Parameters.Add("@Cod_Conasev", SqlDbType.Char, 6).Value = Clas_Enti.Flujo_Efectivo_Cod_conasev;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }

        public List<Entidad_Flujo_Efectivo> Listar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Clas_Enti)
        {
            List<Entidad_Flujo_Efectivo> list2;
            List<Entidad_Flujo_Efectivo> list = new List<Entidad_Flujo_Efectivo>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("pa_CTB_ESTADO_FLUJOS_EFECTIVO_LISTAR", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = IIf(Clas_Enti.ID == 0, null, Clas_Enti.ID);
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 6).Value = Clas_Enti.Codigo;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Flujo_Efectivo item = new Entidad_Flujo_Efectivo();
                                item.ID = reader.GetInt32(0) ;
                                item.Flujo_Efectivo_Cod = reader.GetString(1).Trim();
                                item.Flujo_Efectivo_Desc = reader.GetString(2).Trim();
                                item.Flujo_Efectivo_Grupo = reader.GetInt32(3);
                                item.Flujo_Efectivo_Cod_conasev = reader.GetString(5).Trim();
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }

        public void Modificar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_ESTADO_FLUJOS_EFECTIVO_MODIFICAR", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@id", SqlDbType.Int).Value = Clas_Enti.ID;
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 6).Value = Clas_Enti.Flujo_Efectivo_Cod;
                        command.Parameters.Add("@Descripcion", SqlDbType.VarChar, 300).Value = Clas_Enti.Flujo_Efectivo_Desc;
                        command.Parameters.Add("@Grupo", SqlDbType.Int).Value = Clas_Enti.Flujo_Efectivo_Grupo;
                        command.Parameters.Add("@Cod_Conasev", SqlDbType.Char, 6).Value = Clas_Enti.Flujo_Efectivo_Cod_conasev;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }

        public List<Entidad_Flujo_Efectivo> Patrimonio_Neto_Listar(Entidad_Flujo_Efectivo Cls_Enti)
        {
            List<Entidad_Flujo_Efectivo> list2;
            List<Entidad_Flujo_Efectivo> list = new List<Entidad_Flujo_Efectivo>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("pa_CTB_PATRIMONIO_NETO_LISTAR", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = IIf(Cls_Enti.ID == 0, null, Cls_Enti.ID);
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 4).Value = (Cls_Enti.Codigo == null) ? null : Cls_Enti.Codigo;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Flujo_Efectivo item = new Entidad_Flujo_Efectivo();
                                item.ID = reader.GetInt32(0) ;
                                item.Codigo = reader.GetString(1).Trim();
                                item.Descripcion = reader.GetString(2).Trim();
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }

        public List<Entidad_Flujo_Efectivo> Patrimonio_Neto_Reporte(Entidad_Flujo_Efectivo Cls_Enti)
        {
            List<Entidad_Flujo_Efectivo> list2;
            List<Entidad_Flujo_Efectivo> list = new List<Entidad_Flujo_Efectivo>();
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    SqlCommand command1 = new SqlCommand("Pa_ESTADO_CAMBIOS_PATRIMONIO_NETO", connection);
                    command1.CommandType = CommandType.StoredProcedure;
                    using (SqlCommand command = command1)
                    {
                        command.Parameters.Add("@Empresa", SqlDbType.Char, 3).Value = (Cls_Enti.Empresa == null) ? null : Cls_Enti.Empresa;
                        command.Parameters.Add("@Anio", SqlDbType.Char, 4).Value = (Cls_Enti.Anio == null) ? null : Cls_Enti.Anio;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                Entidad_Flujo_Efectivo item = new Entidad_Flujo_Efectivo();
                                item.Codigo = reader.GetString(0).Trim();
                                item.Descripcion = reader.GetString(1).Trim();
                                item.Capital = reader.GetDecimal(2);
                                item.Capital_Adicional = reader.GetDecimal(3);
                                item.Acciones_Inversion = reader.GetDecimal(4);
                                item.Exedente_Revaluacion = reader.GetDecimal(5);
                                item.Reserva_Legal = reader.GetDecimal(6);
                                item.Otras_Reservas = reader.GetDecimal(7);
                                item.Resultados_Acumulados = reader.GetDecimal(8);
                                list.Add(item);
                            }
                        }
                    }
                }
                list2 = list;
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
            return list2;
        }






        public void Insertar_Patrimonio_Neto_SMV(Entidad_Flujo_Efectivo Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_PATRIMONIO_NETO_INSERTAR", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 6).Value = Clas_Enti.Codigo;
                        command.Parameters.Add("@Descripcion", SqlDbType.VarChar, 300).Value = Clas_Enti.Descripcion;
                     
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }


        public void Modificar_Patrimonio_Neto_SMV(Entidad_Flujo_Efectivo Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_PATRIMONIO_NETO_MODIFICAR", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@id", SqlDbType.Int).Value = Clas_Enti.ID;
                        command.Parameters.Add("@Codigo", SqlDbType.Char, 6).Value = Clas_Enti.Codigo;
                        command.Parameters.Add("@Descripcion", SqlDbType.VarChar, 300).Value = Clas_Enti.Descripcion;
                     
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }



        public void Eliminar_Patrimonio_Neto_SMV(Entidad_Flujo_Efectivo Clas_Enti)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand command = new SqlCommand("pa_CTB_PATRIMONIO_NETO_ELIMINAR", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@id", SqlDbType.Int).Value = Clas_Enti.ID;
                        connection.Open();
                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception exception1)
            {
                throw new Exception(exception1.Message);
            }
        }


    }
}
