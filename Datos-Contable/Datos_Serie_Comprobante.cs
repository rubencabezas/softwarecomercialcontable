﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
 public   class Datos_Serie_Comprobante
    {
        public void Insertar(Entidad_Serie_Comprobante Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_SERIE_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Serie;
                        Cmd.Parameters.Add("@Ser_Numero_Ini", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Ini;
                        Cmd.Parameters.Add("@Ser_Numero_Fin", System.Data.SqlDbType.Char,8).Value = Clas_Enti.Ser_Numero_Fin;
                        Cmd.Parameters.Add("@Ser_Numero_Actual", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ser_Numero_Actual;
                        Cmd.Parameters.Add("@Ser_Pdv_Codigo", System.Data.SqlDbType.Char,2).Value = Clas_Enti.Ser_Pdv_Codigo;
                        Cmd.Parameters.Add("@Ser_Fecha_Impresion", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ser_Fecha_Impresion;
                        Cmd.Parameters.Add("@Ser_Fecha_Baja", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ser_Fecha_Baja;
                        Cmd.Parameters.Add("@Ser_Cod_Autorizacion_SUNAT", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ser_Cod_Autorizacion_SUNAT;
                        Cmd.Parameters.Add("@Ser_Max_Lineas", System.Data.SqlDbType.Int).Value = Clas_Enti.Ser_Max_Lineas;
                        Cmd.Parameters.Add("@Ser_Es_Electronico", System.Data.SqlDbType.Bit).Value = Clas_Enti.Es_Electronico;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Serie_Comprobante Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_SERIE_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Serie;
                        Cmd.Parameters.Add("@Ser_Numero_Ini", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Ini;
                        Cmd.Parameters.Add("@Ser_Numero_Fin", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Fin;
                        Cmd.Parameters.Add("@Ser_Numero_Actual", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ser_Numero_Actual;
                        Cmd.Parameters.Add("@Ser_Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Ser_Pdv_Codigo;
                        Cmd.Parameters.Add("@Ser_Fecha_Impresion", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ser_Fecha_Impresion;
                        Cmd.Parameters.Add("@Ser_Fecha_Baja", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ser_Fecha_Baja;
                        Cmd.Parameters.Add("@Ser_Cod_Autorizacion_SUNAT", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ser_Cod_Autorizacion_SUNAT;
                        Cmd.Parameters.Add("@Ser_Max_Lineas", System.Data.SqlDbType.Int).Value = Clas_Enti.Ser_Max_Lineas;
                        Cmd.Parameters.Add("@Ser_Es_Electronico", System.Data.SqlDbType.Bit).Value = Clas_Enti.Es_Electronico;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Dar_Baja(Entidad_Serie_Comprobante Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_SERIE_DAR_BAJA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Ser_Tipo_Doc;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ser_Serie;
                        Cmd.Parameters.Add("@Ser_Numero_Ini", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Ini;
                        Cmd.Parameters.Add("@Ser_Numero_Fin", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ser_Numero_Fin;
                        Cmd.Parameters.Add("@Ser_Fecha_Baja", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Ser_Fecha_Baja;

                      Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Serie_Comprobante> Listar(Entidad_Serie_Comprobante Cls_Enti)
        {
            List<Entidad_Serie_Comprobante> ListaItems = new List<Entidad_Serie_Comprobante>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_SERIE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(Cls_Enti.Ser_Tipo_Doc) ? null : Cls_Enti.Ser_Tipo_Doc); //Cls_Enti.@Pdv_Codigo;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(Cls_Enti.Ser_Serie) ? null : Cls_Enti.Ser_Serie); //Cls_Enti.@Pdv_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Serie_Comprobante
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Ser_Tipo_Doc = Dr.GetString(1),
                                    Id_Sunat = Dr.GetString(2),
                                    Nombre_Comprobante = Dr.GetString(3),
                                    Ser_Serie=Dr.GetString(4),
                                    Ser_Numero_Ini=Dr.GetString(5),
                                    Ser_Numero_Fin=Dr.GetString(6),
                                    Ser_Numero_Actual=Dr.GetString(7),
                                    Ser_Pdv_Codigo=Dr.GetString(8),
                                    Pdv_Nombre=Dr.GetString(9),
                                    Ser_Fecha_Impresion=Dr.GetDateTime(10),
                                    Ser_Fecha_Baja=Dr.GetDateTime(11),
                                    Ser_Estado_Baja=Dr.GetString(12),
                                    Ser_Estado_Baja_Desc=Dr.GetString(13),
                                    Ser_Cod_Autorizacion_SUNAT=Dr.GetString(14),
                                    Ser_Max_Lineas=Dr.GetInt32(15),
                                    Es_Electronico=Dr.GetBoolean(16)
                                    

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Serie_Comprobante> Listar_principal(Entidad_Serie_Comprobante Cls_Enti)
        {
            List<Entidad_Serie_Comprobante> ListaItems = new List<Entidad_Serie_Comprobante>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_DOCUMENTOS_SERIE_LISTAR_PRINCIPAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Ser_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(Cls_Enti.Ser_Tipo_Doc) ? null : Cls_Enti.Ser_Tipo_Doc); //Cls_Enti.@Pdv_Codigo;
                        Cmd.Parameters.Add("@Ser_Serie", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(Cls_Enti.Ser_Serie) ? null : Cls_Enti.Ser_Serie); //Cls_Enti.@Pdv_Codigo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Serie_Comprobante
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Ser_Tipo_Doc = Dr.GetString(1),
                                    Id_Sunat = Dr.GetString(2),
                                    Nombre_Comprobante = Dr.GetString(3),
                                    Ser_Serie = Dr.GetString(4),
                                    Ser_Numero_Ini = Dr.GetString(5),
                                    Ser_Numero_Fin = Dr.GetString(6),
                                    Ser_Numero_Actual = Dr.GetString(7),
                                    Ser_Pdv_Codigo = Dr.GetString(8),
                                    Pdv_Nombre = Dr.GetString(9),
                                    Ser_Fecha_Impresion = Dr.GetDateTime(10),
                                    Ser_Fecha_Baja = Dr.GetDateTime(11),
                                    Ser_Estado_Baja = Dr.GetString(12),
                                    Ser_Estado_Baja_Desc = Dr.GetString(13),
                                    Ser_Cod_Autorizacion_SUNAT = Dr.GetString(14),
                                    Ser_Max_Lineas = Dr.GetInt32(15),
                                    Es_Electronico = Dr.GetBoolean(16)


                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


    }
}
