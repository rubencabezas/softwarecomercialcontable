﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
  public  class Datos_Inicio_Sesion
    {

        public Entidad_Inicio_Sesion Iniciar(Entidad_Inicio_Sesion Clas_Enti)
        {
            Entidad_Inicio_Sesion Ent = new Entidad_Inicio_Sesion();
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_Iniciar_Sesion", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Usuario_dni;
                        Cmd.Parameters.Add("@Contrasenia", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_pass;
                        Cmd.Parameters.Add("@PTransac", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Ent.sms = Cmd.Parameters["@PTransac"].Value.ToString().Trim();
                        Trs.Commit();
                    }
                }
                return Ent;
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }
    }
}
