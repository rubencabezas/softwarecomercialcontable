﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
  public  class Datos_General
    {

        public List<Entidad_General> Listar(Entidad_General Cls_Enti)
        {
            List<Entidad_General> ListaItems = new List<Entidad_General>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GENERALES_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_General", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_General;
                        Cmd.Parameters.Add("@Gen_Codigo_Interno", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Gen_Codigo_Interno;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_General
                                {
                                    Id_General = Dr.GetString(0),
                                    Id_General_Det = Dr.GetString(1),
                                    Gen_Codigo_Interno = Dr.GetString(2),
                                    Gen_Descripcion_Det = Dr.GetString(3)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


    }
}
