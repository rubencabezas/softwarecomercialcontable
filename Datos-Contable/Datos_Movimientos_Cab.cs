﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
 public   class Datos_Movimientos_Cab
    {
        public void Insertar(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char,10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar,11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ctb_Condicion_Cod", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar,50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ctb_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible2;
                        Cmd.Parameters.Add("@Ctb_Igv2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv2;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible3;
                        Cmd.Parameters.Add("@Ctb_Igv3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv3;

                        Cmd.Parameters.Add("@Ctb_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ctb_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ctb_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ctb_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;


                        Cmd.Parameters.Add("@Ctb_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ctb_No_Gravadas", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_No_Gravadas;

                        Cmd.Parameters.Add("@Ctb_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;

                        Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Afecto_RE;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Ctb_Afecto_Tipo;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ctb_Afecto_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.Char,8).Value = Clas_Enti.Ctb_Afecto_Serie;
                        Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar,25).Value = Clas_Enti.Ctb_Afecto_Numero;
                        Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Porcentaje ;
                        Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Monto;

                        Cmd.Parameters.Add("@Ctb_Tasa_IGV", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;
                     Cmd.Parameters.Add("@Ctb_Analisis", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Ctb_Analisis;


                        Cmd.Parameters.Add("@Ctb_Afecto_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Afecto_Fecha;
                        Cmd.Parameters.Add("@Ctb_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Vencimiento;


                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value =Actual_Conexion.Maquina;

                        Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efectivo_Cod", System.Data.SqlDbType.Char, 6).Value = (string.IsNullOrEmpty(Clas_Enti.Flujo_Efectivo_Cod) ? null : Clas_Enti.Flujo_Efectivo_Cod);


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                     
                        Clas_Enti.Id_Voucher = Cmd.Parameters["@Id_Voucher"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Operacion_Cod;
                            Cmd.Parameters.Add("@Ctb_Igv_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Igv;
                            Cmd.Parameters.Add("@Ctb_Centro_CG", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Tipo_BSA", System.Data.SqlDbType.Char).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);
                            Cmd.Parameters.Add("@Ctb_Catalogo", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Ctb_Catalogo) ? null : ent.Ctb_Catalogo);

                            Cmd.Parameters.Add("@Ctb_Valor_unit", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Valor_Unit;
                            Cmd.Parameters.Add("@Ctb_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Cantidad;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Almacen", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Ctb_Almacen_cod) ? null : ent.Ctb_Almacen_cod);
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);

                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Ctb_Cta_Destino", System.Data.SqlDbType.Bit).Value = ent.Cta_Destino ;
                            Cmd.Parameters.Add("@Ctb_Patri_Neto_Cod", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Patri_Neto_Cod) ? null : ent.Patri_Neto_Cod);
                            Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efecctivo_Cod_Det", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Estado_Flujo_Efecctivo_Cod_Det) ? null : ent.Estado_Flujo_Efecctivo_Cod_Det);

                            Cmd.ExecuteNonQuery();
                        }



                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char,10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                           Cmd.ExecuteNonQuery();
                        }

                        if (Clas_Enti.DetalleDoc_Ref.Count > 0 )
                        {
                            foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleDoc_Ref)
                                    {
                                        Cmd.Parameters.Clear();
                                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_DOC_REFERENCIA";
                                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                                        Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                                        Cmd.Parameters.Add("@Anio_Ref", System.Data.SqlDbType.Char, 4).Value = ent.Id_Anio_Ref;
                                        Cmd.Parameters.Add("@Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = ent.Id_Periodo_Ref;
                                        Cmd.Parameters.Add("@Libro_Ref", System.Data.SqlDbType.Char, 3).Value = ent.Id_Libro_Ref;
                                        Cmd.Parameters.Add("@Voucher_Ref", System.Data.SqlDbType.Char,10).Value = ent.Id_Voucher_Ref;

                                        Cmd.Parameters.Add("@Doc_Origen", System.Data.SqlDbType.Char, 2).Value = ent.Ord_Origen;
                                        Cmd.Parameters.Add("@Doc_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = ent.Ord_tipo;
                                        Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Ord_Folio;

                                Cmd.ExecuteNonQuery();
                                    }
                        }
                        else
                        {
                            IIf(Clas_Enti.DetalleDoc_Ref == null, 0, Clas_Enti.DetalleDoc_Ref);
                        }

                        //orden de compra

                        if (Clas_Enti.DetalleDoc_Orden_CS.Count > 0)
                        {
                            foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleDoc_Orden_CS)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_DOC_REFERENCIA";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                                Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                                Cmd.Parameters.Add("@Anio_Ref", System.Data.SqlDbType.Char, 4).Value = ent.Id_Anio_Ref;
                                Cmd.Parameters.Add("@Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = ent.Id_Periodo_Ref;
                                Cmd.Parameters.Add("@Libro_Ref", System.Data.SqlDbType.Char, 3).Value = ent.Id_Libro_Ref;
                                Cmd.Parameters.Add("@Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;

                                Cmd.Parameters.Add("@Doc_Origen", System.Data.SqlDbType.Char, 2).Value = ent.Ord_Origen;
                                Cmd.Parameters.Add("@Doc_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = ent.Ord_tipo;
                                Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Ord_Folio;
                                Cmd.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            IIf(Clas_Enti.DetalleDoc_Orden_CS == null, 0, Clas_Enti.DetalleDoc_Orden_CS);
                        }

                        Cmd.Parameters.Clear();//DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_DESTINO_INSERTAR";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                        Cmd.ExecuteNonQuery();



                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }

        public void Eliminar(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char,10).Value=Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                     
                    
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar_Ingresos_Egresos(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            if (ent.Ctb_Es_Cuenta_Principal != true)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_REVERTIR_AMORTIZADO";

                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                                Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;

                                Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Id_Anio_Ref) ? null : ent.Id_Anio_Ref);
                                Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Id_Periodo_Ref) ? null : ent.Id_Periodo_Ref);
                                Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Id_Libro_Ref) ? null : ent.Id_Libro_Ref);
                                Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;
                                Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.Int).Value = ent.Id_Item_Ref;

                                Cmd.ExecuteNonQuery();
                            }

                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar_Tesoreria(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ELIMINAR_TESORERIA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Ctb_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Cierre_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio_Ref;
                        Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo_Ref;
                        Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro_Ref;
                        Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher_Ref;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            if (ent.Ctb_Es_Cuenta_Principal != true)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_REVERTIR_AMORTIZADO";
                            
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                                Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                        
                                Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Id_Anio_Ref) ? null : ent.Id_Anio_Ref);
                                Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Id_Periodo_Ref) ? null : ent.Id_Periodo_Ref);
                                Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Id_Libro_Ref) ? null : ent.Id_Libro_Ref);
                                Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;
                                Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.Int).Value = ent.Id_Item_Ref;

                                Cmd.ExecuteNonQuery();
                            }

                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Anular(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ANULAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char,10).Value=Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                     
                    
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
  public void Revertir(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_REVERTIR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char,10).Value=Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                     
                    
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char,10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher); 


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Glosa = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(6).Trim(),
                                    Nombre_Comprobante = Dr.GetString(7).Trim(),
                                    Ctb_Serie = Dr.GetString(8).Trim(),
                                    Ctb_Numero = Dr.GetString(9).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(10),
                                    Ctb_Tipo_Ent = Dr.GetString(11).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(12).Trim(),
                                    Entidad = Dr.GetString(13).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(14).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(15).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(16).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(17).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(18).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(19).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(20).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(21),
                                    Ctb_Base_Imponible = Dr.GetDecimal(22),
                                    Ctb_Igv = Dr.GetDecimal(23),
                                    Ctb_Base_Imponible2 = Dr.GetDecimal(24),
                                    Ctb_Igv2 = Dr.GetDecimal(25),
                                    Ctb_Base_Imponible3 = Dr.GetDecimal(26),
                                    Ctb_Igv3 = Dr.GetDecimal(27),
                                    Ctb_Importe_Total = Dr.GetDecimal(28),
                                    Ctb_Isc = Dr.GetBoolean(29),
                                    Ctb_Isc_Importe = Dr.GetDecimal(30),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(31),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(32),
                                    Ctb_No_Gravadas = Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(34),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(35),
                                    Ctb_Exonerada = Dr.GetDecimal(36),
                                    Ctb_Inafecta = Dr.GetDecimal(37),
                                    Ctb_Afecto_RE = Dr.GetBoolean(38),
                                    Ctb_Afecto_Tipo = Dr.GetString(39).Trim(),
                                    Ctb_Afecto_Tipo_cod_Interno = Dr.GetString(40).Trim(),
                                    Ctb_Afecto_Tipo_Descripcion = Dr.GetString(41).Trim(),
                                    Ctb_Afecto_Tipo_Doc = Dr.GetString(42).Trim(),
                                    Ctb_Afecto_Tipo_Doc_DEsc = Dr.GetString(43).Trim(),
                                    Ctb_Afecto_Serie = Dr.GetString(44).Trim(),
                                    Ctb_Afecto_Numero = Dr.GetString(45).Trim(),
                                    Ctb_Afecto_Porcentaje = Dr.GetDecimal(46),
                                    Ctb_Afecto_Monto = Dr.GetDecimal(47),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(48),
                                    Estado_Fila = Dr.GetString(49).Trim(),
                                    Ctb_Analisis = Dr.GetString(50).Trim(),
                                    Ctb_Analisis_Desc=Dr.GetString(51).Trim(),
                                    Ctb_Afecto_Fecha=Dr.GetDateTime(52),
                                    Ctb_Fecha_Vencimiento=Dr.GetDateTime(53),
                                    Ctb_Tipo_Doc_Sunat=Dr.GetString(54),
                                    Ctn_Moneda_Sunat=Dr.GetString(55),
                                    Id_Periodo_Desc = Dr.GetString(56),
                                    Flujo_Efectivo_Cod=Dr.GetString(57).Trim(),
                                    Flujo_Efectivo_Desc=Dr.GetString(58).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public List<Entidad_Movimiento_Cab> Listar_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Id_Item=Dr.GetInt32(5),
                                    Ctb_Cuenta=Dr.GetString(6),
                                    Ctb_Cuenta_Desc=Dr.GetString(7),
                                    Ctb_Operacion_Cod=Dr.GetString(8),
                                    Ctb_Operacion_Cod_Interno=Dr.GetString(9),
                                    Ctb_Operacion_Desc=Dr.GetString(10),
                                    Ctb_Centro_CG=Dr.GetString(11),
                                    Ctb_Centro_CG_Desc=Dr.GetString(12),
                                    Ctb_Tipo_DH=Dr.GetString(13),
                                    CCtb_Tipo_DH_Interno=Dr.GetString(14),
                                    Ctb_Tipo_DH_Desc=Dr.GetString(15),
                                    Ctb_Importe_Debe=Dr.GetDecimal(16),
                                    Ctb_Importe_Haber=Dr.GetDecimal(17),
                                    Ctb_Importe_Debe_Extr = Dr.GetDecimal(18),
                                    Ctb_Importe_Haber_Extr = Dr.GetDecimal(19),
                                    Ctb_Tipo_BSA =Dr.GetString(20),
                                    Ctb_Tipo_BSA_Interno=Dr.GetString(21),
                                    Ctb_Tipo_BSA_Desc=Dr.GetString(22),
                                    Ctb_Catalogo=Dr.GetString(23),
                                    Ctb_Catalogo_Desc=Dr.GetString(24),
                                    Ctb_Valor_Unit=Dr.GetDecimal(25),
                                    Ctb_Cantidad=Dr.GetDecimal(26),
                                    Ctb_Almacen_cod=Dr.GetString(27).Trim(),
                                    Ctb_Almacen_desc=Dr.GetString(28),
                                    Ctb_Tipo_Doc_det=Dr.GetString(29),
                                    Ctb_Tipo_Doc_det_desc=Dr.GetString(30),
                                    Ctb_Serie_det=Dr.GetString(31),
                                    Ctb_Numero_det=Dr.GetString(32),
                                    Ctb_Fecha_Mov_det=Dr.GetDateTime(33),
                                    Ctb_Tipo_Ent_det=Dr.GetString(34),
                                    Ctb_Ruc_dni_det=Dr.GetString(35),
                                    Entidad_det=Dr.GetString(36),
                                    Ctb_moneda_cod_det=Dr.GetString(37),
                                    Ctb_moneda_det_desc=Dr.GetString(38),
                                    Ctb_Tipo_Cambio_Cod_Det=Dr.GetString(39),
                                    Ctb_Tipo_Cambio_Desc_Det=Dr.GetString(40),
                                    Ctb_Tipo_Cambio_Valor_Det=Dr.GetDecimal(41),
                                    Cta_Destino=Dr.GetBoolean(42),
                                    Patri_Neto_Cod=Dr.GetString(43).Trim(),
                                    Patri_Neto_Desc=Dr.GetString(44).Trim(),
                                    Estado_Flujo_Efecctivo_Cod_Det = Dr.GetString(45).Trim(),
                                    Estado_Flujo_Efecctivo_Desc_Det = Dr.GetString(46).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Movimiento_Cab> Listar_Det_Administrativo(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_ADM_CONTABLE_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Adm_Item = Dr.GetInt32(5),
                                    Adm_Centro_CG = Dr.GetString(6).Trim(),
                                    Adm_Centro_CG_Desc = Dr.GetString(7).Trim(),
                                    Adm_Tipo_BSA = Dr.GetString(8).Trim(),
                                    Adm_Tipo_BSA_Interno = Dr.GetString(9).Trim(),
                                    Adm_Tipo_BSA_Desc = Dr.GetString(10).Trim(),
                                    Adm_Catalogo = Dr.GetString(11).Trim(),
                                    Adm_Catalogo_Desc = Dr.GetString(12).Trim(),
                                    Adm_Cantidad = Dr.GetDecimal(13),
                                    Adm_Valor_Unit = Dr.GetDecimal(14),
                                    Adm_Total = Dr.GetDecimal(15),
                                    Adm_Almacen=Dr.GetString(16).Trim(),
                                    Adm_Almacen_desc=Dr.GetString(17).Trim(),
                                    Adm_Unm_Id = Dr.GetString(18).Trim(),
                                    Adm_Unm_Desc = Dr.GetString(19).Trim(),
                                    Acepta_lotes = Dr.GetBoolean(20) ,
                                    Adm_Tipo_Operacion_Interno  = Dr.GetString(21).Trim(),
                                    Adm_Tipo_Operacion   = Dr.GetString(22).Trim(),
                                    Adm_Tipo_Operacion_Desc = Dr.GetString(23).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public void Modificar(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value=Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ctb_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ctb_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible2;
                        Cmd.Parameters.Add("@Ctb_Igv2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv2;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible3;
                        Cmd.Parameters.Add("@Ctb_Igv3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv3;

                        Cmd.Parameters.Add("@Ctb_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ctb_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ctb_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ctb_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;


                        Cmd.Parameters.Add("@Ctb_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ctb_No_Gravadas", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_No_Gravadas;

                        Cmd.Parameters.Add("@Ctb_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;

                        Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Afecto_RE;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Afecto_Tipo;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Afecto_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ctb_Afecto_Serie;
                        Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar, 25).Value = Clas_Enti.Ctb_Afecto_Numero;
                        Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Porcentaje;
                        Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Monto;

                        Cmd.Parameters.Add("@Ctb_Tasa_IGV", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;
                        Cmd.Parameters.Add("@Ctb_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;

                        Cmd.Parameters.Add("@Ctb_Afecto_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Afecto_Fecha;
                        Cmd.Parameters.Add("@Ctb_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Vencimiento;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efectivo_Cod", System.Data.SqlDbType.Char, 6).Value = (string.IsNullOrEmpty(Clas_Enti.Flujo_Efectivo_Cod) ? null : Clas_Enti.Flujo_Efectivo_Cod);


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Operacion_Cod;
                            Cmd.Parameters.Add("@Ctb_Igv_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Igv;
                            Cmd.Parameters.Add("@Ctb_Centro_CG", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;

                                 
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;//ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value =ent.Ctb_Importe_Haber;//ent.Ctb_Importe_Haber;

                            Cmd.Parameters.Add("@Ctb_Tipo_BSA", System.Data.SqlDbType.Char).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);
                            Cmd.Parameters.Add("@Ctb_Catalogo", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Ctb_Catalogo) ? null : ent.Ctb_Catalogo);

                            Cmd.Parameters.Add("@Ctb_Valor_unit", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Valor_Unit;
                            Cmd.Parameters.Add("@Ctb_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Cantidad;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Almacen", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Ctb_Almacen_cod) ? null : ent.Ctb_Almacen_cod);
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);

                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Ctb_Cta_Destino", System.Data.SqlDbType.Bit).Value = ent.Cta_Destino;
                            Cmd.Parameters.Add("@Ctb_Patri_Neto_Cod", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Patri_Neto_Cod) ? null : ent.Patri_Neto_Cod);
                            Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efecctivo_Cod_Det", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Estado_Flujo_Efecctivo_Cod_Det) ? null : ent.Estado_Flujo_Efecctivo_Cod_Det);

                            Cmd.ExecuteNonQuery();
                        }


                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.ExecuteNonQuery();
                        }

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleDoc_Ref)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_DOC_REFERENCIA";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Anio_Ref", System.Data.SqlDbType.Char, 4).Value = ent.Id_Anio_Ref;
                            Cmd.Parameters.Add("@Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = ent.Id_Periodo_Ref;
                            Cmd.Parameters.Add("@Libro_Ref", System.Data.SqlDbType.Char, 3).Value = ent.Id_Libro_Ref;
                            Cmd.Parameters.Add("@Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;
                            Cmd.ExecuteNonQuery();
                        }

                        //orden de compra

                        if (Clas_Enti.DetalleDoc_Orden_CS.Count > 0)
                        {
                            foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleDoc_Orden_CS)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_DOC_REFERENCIA";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                                Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                                Cmd.Parameters.Add("@Anio_Ref", System.Data.SqlDbType.Char, 4).Value = ent.Id_Anio_Ref;
                                Cmd.Parameters.Add("@Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = ent.Id_Periodo_Ref;
                                Cmd.Parameters.Add("@Libro_Ref", System.Data.SqlDbType.Char, 3).Value = ent.Id_Libro_Ref;
                                Cmd.Parameters.Add("@Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;

                                Cmd.Parameters.Add("@Doc_Origen", System.Data.SqlDbType.Char, 2).Value = ent.Ord_Origen;
                                Cmd.Parameters.Add("@Doc_Tipo_Orden", System.Data.SqlDbType.Char, 4).Value = ent.Ord_tipo;
                                Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Ord_Folio;
                                Cmd.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            IIf(Clas_Enti.DetalleDoc_Orden_CS == null, 0, Clas_Enti.DetalleDoc_Orden_CS);
                        }

                        Cmd.Parameters.Clear();//DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_DESTINO_INSERTAR";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                        Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public List<Entidad_Movimiento_Cab> Listar_Provision(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PROVISION_GENERAL_BUSQUEDA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Anio_Ref = Dr.GetString(0),
                                    Id_Periodo_Ref = Dr.GetString(1),
                                    Id_Libro_Ref = Dr.GetString(2),
                                    Id_Voucher_Ref = Dr.GetString(3),
                                    Id_Item_Ref = Dr.GetInt32(4),
                                    Ctb_Cuenta = Dr.GetString(5),
                                    Ctb_Serie_det = Dr.GetString(6),
                                    Ctb_Numero_det = Dr.GetString(7),
                                    Ctb_Tipo_Doc_det = Dr.GetString(8),
                                    Ctb_Tipo_Doc_det_desc = Dr.GetString(9),
                                    Ctb_Ruc_dni_det = Dr.GetString(10),
                                    Entidad_det = Dr.GetString(11),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(12),
                                    Ctb_moneda_cod_det = Dr.GetString(13),
                                    Ctb_moneda_det_desc = Dr.GetString(14),
                                    Ctb_Tipo_Cambio_Cod_Det = Dr.GetString(15),
                                    Ctb_Tipo_Cambio_Valor_Det = Dr.GetDecimal(16),
                                    Ctb_Importe_Total = Dr.GetDecimal(17)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Movimiento_Cab> Listar_Doc_Ref(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_DOC_REFERENCIA_BUSCAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Voucher;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                int s = Dr.GetInt32(4);

                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Anio_Ref = Dr.GetString(0),
                                    Id_Periodo_Ref = Dr.GetString(1),
                                    Id_Libro_Ref = Dr.GetString(2),
                                    Id_Voucher_Ref = Dr.GetString(3),
                                    Id_Item_Ref = Dr.GetInt32(4),
                                    Ctb_Serie_det = Dr.GetString(5),
                                    Ctb_Numero_det = Dr.GetString(6),
                                    Ctb_Tipo_Doc_det = Dr.GetString(7),
                                    Ctb_Tipo_Doc_det_desc = Dr.GetString(8),
                                    Ctb_Ruc_dni_det = Dr.GetString(9),
                                    Entidad_det = Dr.GetString(10),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(11),
                                    Ctb_moneda_cod_det = Dr.GetString(12),
                                    Ctb_moneda_det_desc = Dr.GetString(13),
                                    Ctb_Tipo_Cambio_Cod_Det = Dr.GetString(14),
                                    Ctb_Tipo_Cambio_Valor_Det = Dr.GetDecimal(15),
                                    Ctb_Importe_Total = Dr.GetDecimal(16)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Movimiento_Cab> Listar_Doc_Ref_Orden(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_DOC_REFERENCIA_BUSCAR_ORDEN", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Voucher;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                //int s = Dr.GetInt16(4);

                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Id_Item = Dr.GetInt32(5),
                                    Id_Anio_Ref = Dr.GetString(6),
                                    Id_Periodo_Ref = Dr.GetString(7),
                                    Id_Libro_Ref = Dr.GetString(8),
                                    Id_Voucher_Ref = Dr.GetString(9),
                                    Ord_Origen = Dr.GetString(10),
                                    Ord_tipo = Dr.GetString(11),
                                    Ord_Folio = Dr.GetString(12)                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


   

        public void Insertar_Tesoreria(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                      
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;

                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_IE;
                        Cmd.Parameters.Add("@Ctb_Tipo_Movimiento", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Medio_Pago", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Ctb_Medio_Pago;
                        Cmd.Parameters.Add("@Ctb_Numero_Transaccion", System.Data.SqlDbType.VarChar,30).Value = Clas_Enti.Ctb_Numero_Transaccion;

                       Cmd.Parameters.Add("@Ctb_Cuenta_Corriente", System.Data.SqlDbType.Char, 30).Value = Clas_Enti.Ctb_Cuenta_Corriente;
                        Cmd.Parameters.Add("@Ctb_Entidad_Financiera", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Entidad_Financiera;
                        Cmd.Parameters.Add("@Ctb_Importe_MN", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_MN;
                        Cmd.Parameters.Add("@Ctb_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe;
                        Cmd.Parameters.Add("@Ctb_Importe_ME", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_ME;


                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efectivo_Cod", System.Data.SqlDbType.Char,6).Value = (string.IsNullOrEmpty(Clas_Enti.Flujo_Efectivo_Cod) ? null : Clas_Enti.Flujo_Efectivo_Cod);

                        Cmd.Parameters.Add("@Tes_Cuenta", System.Data.SqlDbType.Char,12).Value = (string.IsNullOrEmpty(Clas_Enti.Tes_Cuenta) ? null : Clas_Enti.Tes_Cuenta);

                        Cmd.Parameters.Add("@Tes_DifCam", System.Data.SqlDbType.Bit).Value =  Clas_Enti.Tes_DifCam;
                        Cmd.Parameters.Add("@Tes_DifCambio", System.Data.SqlDbType.Decimal).Value =  Clas_Enti.Tes_DifCambio;
                        Cmd.Parameters.Add("@Tes_DifRed", System.Data.SqlDbType.Bit).Value = Clas_Enti.Tes_DifRed;
                        Cmd.Parameters.Add("@Tes_DifRedond", System.Data.SqlDbType.Decimal).Value =  Clas_Enti.Tes_DifRedond;

      
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Voucher = Cmd.Parameters["@Id_Voucher"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);

                            Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Id_Anio_Ref) ? null : ent.Id_Anio_Ref);
                            Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Id_Periodo_Ref) ? null : ent.Id_Periodo_Ref);
                            Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Id_Libro_Ref) ? null : ent.Id_Libro_Ref);
                            Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char,10).Value = ent.Id_Voucher_Ref;
                            Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.Int).Value =  ent.Id_Item_Ref;


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                            Cmd.Parameters.Add("@Ctb_Es_Cuenta_Principal", System.Data.SqlDbType.Bit).Value =ent.Ctb_Es_Cuenta_Principal;
                            Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efecctivo_Cod_Det", System.Data.SqlDbType.Char, 6).Value = (string.IsNullOrEmpty(ent.Estado_Flujo_Efecctivo_Cod_Det) ? null : ent.Estado_Flujo_Efecctivo_Cod_Det);

                            Cmd.Parameters.Add("@Cta_Dif", System.Data.SqlDbType.Bit).Value = ent.Cta_Dif;
                          
                            Cmd.Parameters.Add("@Cta_Es_Pago_Regimen_Igv", System.Data.SqlDbType.Bit).Value = ent.Ctb_Afecto_RE;
                          
                            
                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();//DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_DET_INSERTAR_DIFERENCIA";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.ExecuteNonQuery();



                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Tesoreria(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);
                        //Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value =(Cls_Enti.Ctb_Tipo_IE==""? null: Cls_Enti.Ctb_Tipo_IE);

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Tipo_IE = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_IE_Interno = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_IE_Desc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Movimiento = Dr.GetString(8).Trim(),
                                    Ctb_Tipo_Movimiento_Interno = Dr.GetString(9).Trim(),
                                    Ctb_Tipo_Movimiento_Desc = Dr.GetString(10).Trim(),
                                    Ctb_Glosa = Dr.GetString(11).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(12).Trim(),
                                    Nombre_Comprobante = Dr.GetString(13).Trim(),
                                    Ctb_Serie = Dr.GetString(14).Trim(),
                                    Ctb_Numero = Dr.GetString(15).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(16),
                                    Ctb_Tipo_Ent = Dr.GetString(17).Trim(),
                                    Ctb_Tipo_Ent_Desc = Dr.GetString(18),
                                    Ctb_Ruc_dni = Dr.GetString(19).Trim(),
                                    Entidad = Dr.GetString(20).Trim(),
                                    Ctb_Medio_Pago=Dr.GetString(21).Trim(),
                                    Ctb_Medio_Pago_desc=Dr.GetString(22).Trim(),
                                    Ctb_Numero_Transaccion=Dr.GetString(23).Trim(),
                                    Ctb_Cuenta_Corriente=Dr.GetString(24).Trim(),
                                    Ctb_Entidad_Financiera=Dr.GetString(25).Trim(),
                                    Ctb_Entidad_Financiera_Desc=Dr.GetString(26).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(27).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(28).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(29).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(30).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(31),
                                    Ctb_Importe=Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac=Dr.GetBoolean(35),
                                    Ctb_Tipo_Doc_Sunat=Dr.GetString(36),
                                    Ctn_Moneda_Sunat=Dr.GetString(37),
                                    Flujo_Efectivo_Cod=Dr.GetString(38).Trim(),
                                    Flujo_Efectivo_Desc=Dr.GetString(39).Trim(),
                                    Id_Periodo_Desc=Dr.GetString(40),
                                    Tes_Cuenta=Dr.GetString(41).Trim(),
                                    Tes_DifCam=Dr.GetBoolean(42),
                                    Tes_DifCambio=Dr.GetDecimal(43),
                                    Tes_DifRed=Dr.GetBoolean(44),
                                    Tes_DifRedond=Dr.GetDecimal(45)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Cab> Listar_Tesoreria_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_DET_TESORERIA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Id_Item = Dr.GetInt32(5),
                                    Ctb_Cuenta = Dr.GetString(6),
                                    Ctb_Cuenta_Desc = Dr.GetString(7),
                                    Ctb_Tipo_Doc_det = Dr.GetString(8),
                                    Ctb_Tipo_Doc_det_desc = Dr.GetString(9),
                                    Ctb_Serie_det = Dr.GetString(10),
                                    Ctb_Numero_det = Dr.GetString(11),
                                    Ctb_Fecha_Mov_det = Dr.GetDateTime(12),
                                    Ctb_moneda_cod_det = Dr.GetString(13),
                                    Ctb_moneda_det_desc = Dr.GetString(14),
                                    Ctb_Tipo_Cambio_Cod_Det = Dr.GetString(15),
                                    Ctb_Tipo_Cambio_Desc_Det = Dr.GetString(16),
                                    Ctb_Tipo_Cambio_Valor_Det = Dr.GetDecimal(17),
                                    Ctb_Tipo_Ent_det=Dr.GetString(18),
                                    Ctb_Tipo_Ent_det_Desc=Dr.GetString(19),
                                    Ctb_Ruc_dni_det = Dr.GetString(20),
                                    Entidad_det = Dr.GetString(21),
                                    Ctb_Tipo_DH = Dr.GetString(22),
                                    CCtb_Tipo_DH_Interno = Dr.GetString(23),
                                    Ctb_Tipo_DH_Desc = Dr.GetString(24),
                                    Ctb_Importe_Debe = Dr.GetDecimal(25),
                                    Ctb_Importe_Haber = Dr.GetDecimal(26),
                                    Ctb_Importe_Debe_Extr = Dr.GetDecimal(27),
                                    Ctb_Importe_Haber_Extr = Dr.GetDecimal(28),
                                    Id_Anio_Ref=Dr.GetString(29),
                                    Id_Periodo_Ref=Dr.GetString(30),
                                    Id_Libro_Ref=Dr.GetString(31),
                                    Id_Voucher_Ref=Dr.GetString(32),
                                    Id_Item_Ref=Dr.GetInt32(33),
                                    Ctb_Tipo_Doc_det_Sunat=Dr.GetString(34),
                                    Ctb_moneda_cod_det_Sunat=Dr.GetString(35),
                                    Ctb_Es_Cuenta_Principal=Dr.GetBoolean(36),
                                    Estado_Flujo_Efecctivo_Cod_Det=Dr.GetString(37),
                                    Cta_Dif=Dr.GetBoolean(38),
                                    Cta_Destino=Dr.GetBoolean(39)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public void Modificar_Tesoreria(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;

                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;

                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_IE;
                        Cmd.Parameters.Add("@Ctb_Tipo_Movimiento", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Medio_Pago", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Ctb_Medio_Pago;
                        Cmd.Parameters.Add("@Ctb_Numero_Transaccion", System.Data.SqlDbType.VarChar, 30).Value = Clas_Enti.Ctb_Numero_Transaccion;

                        Cmd.Parameters.Add("@Ctb_Cuenta_Corriente", System.Data.SqlDbType.Char, 30).Value = Clas_Enti.Ctb_Cuenta_Corriente;
                        Cmd.Parameters.Add("@Ctb_Entidad_Financiera", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Entidad_Financiera;
                        Cmd.Parameters.Add("@Ctb_Importe_MN", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_MN;
                        Cmd.Parameters.Add("@Ctb_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe;
                        Cmd.Parameters.Add("@Ctb_Importe_ME", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_ME;


                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efectivo_Cod", System.Data.SqlDbType.Char, 6).Value = (string.IsNullOrEmpty(Clas_Enti.Flujo_Efectivo_Cod) ? null : Clas_Enti.Flujo_Efectivo_Cod);

                        Cmd.Parameters.Add("@Tes_Cuenta", System.Data.SqlDbType.Char, 12).Value = (string.IsNullOrEmpty(Clas_Enti.Tes_Cuenta) ? null : Clas_Enti.Tes_Cuenta);

                        Cmd.Parameters.Add("@Tes_DifCam", System.Data.SqlDbType.Bit).Value = Clas_Enti.Tes_DifCam;
                        Cmd.Parameters.Add("@Tes_DifCambio", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tes_DifCambio;
                        Cmd.Parameters.Add("@Tes_DifRed", System.Data.SqlDbType.Bit).Value = Clas_Enti.Tes_DifRed;
                        Cmd.Parameters.Add("@Tes_DifRedond", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tes_DifRedond;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

              
                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);

                            Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Id_Anio_Ref) ? null : ent.Id_Anio_Ref);
                            Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Id_Periodo_Ref) ? null : ent.Id_Periodo_Ref);
                            Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Id_Libro_Ref) ? null : ent.Id_Libro_Ref);
                            Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;
                            Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.Int).Value = ent.Id_Item_Ref;


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                            Cmd.Parameters.Add("@Ctb_Es_Cuenta_Principal", System.Data.SqlDbType.Bit).Value = ent.Ctb_Es_Cuenta_Principal;
                            Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efecctivo_Cod_Det", System.Data.SqlDbType.Char, 6).Value = (string.IsNullOrEmpty(ent.Estado_Flujo_Efecctivo_Cod_Det) ? null : ent.Estado_Flujo_Efecctivo_Cod_Det);
                            Cmd.Parameters.Add("@Cta_Dif", System.Data.SqlDbType.Bit).Value = ent.Cta_Dif;

                            Cmd.Parameters.Add("@Cta_Es_Pago_Regimen_Igv", System.Data.SqlDbType.Bit).Value = ent.Ctb_Afecto_RE;

                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();//DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_TESORERIA_DET_INSERTAR_DIFERENCIA";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Cab> Lista_Documentos_por_Cobrar(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PROVISION_GENERAL_POR_COBRAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Anio_Ref = Dr.GetString(0),
                                    Id_Periodo_Ref = Dr.GetString(1),
                                    Id_Libro_Ref = Dr.GetString(2),
                                    Id_Voucher_Ref = Dr.GetString(3),
                                    Id_Item_Ref = Dr.GetInt32(4),
                                    Ctb_Cuenta = Dr.GetString(5),
                                    Ctb_Serie_det = Dr.GetString(6),
                                    Ctb_Numero_det = Dr.GetString(7),
                                    Ctb_Tipo_Doc_det = Dr.GetString(8),
                                    Ctb_Tipo_Doc_det_desc = Dr.GetString(9),
                                    Ctb_Ruc_dni_det = Dr.GetString(10),
                                    Entidad_det = Dr.GetString(11),
                                    Ctb_Fecha_Mov_det = Dr.GetDateTime(12),
                                    Ctb_moneda_cod_det = Dr.GetString(13),
                                    Ctb_moneda_det_desc = Dr.GetString(14),
                                    Ctb_Tipo_Cambio_Cod_Det = Dr.GetString(15),
                                    Ctb_Tipo_Cambio_Desc_Det = Dr.GetString(16),
                                    Ctb_Tipo_Cambio_Valor_Det = Dr.GetDecimal(17),
                                    Ctb_Importe = Dr.GetDecimal(18),
                                    Ctb_Importe_Amortizado = Dr.GetDecimal(19),
                                    Ctb_Importe_Saldo = Dr.GetDecimal(20),
                                    Ctb_Cuenta_Desc = Dr.GetString(21),
                                    Ctb_Tipo_DH = Dr.GetString(22),
                                    CCtb_Tipo_DH_Interno = Dr.GetString(23),
                                    Ctb_Tipo_DH_Desc = Dr.GetString(24),
                                    Ctb_Tipo_Ent_det = Dr.GetString(25),
                                    Ctb_Tipo_Ent_det_Desc = Dr.GetString(26),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(27),
                                    Ctb_Importe_Debe_Extr = Dr.GetDecimal(28),
                                    Ctb_Tipo_Doc_det_Sunat=Dr.GetString(29),
                                    Ctb_moneda_cod_det_Sunat=Dr.GetString(30),
                                    Id_Libro=Dr.GetString(31),
                                    Ctb_Afecto_RE=Dr.GetBoolean(32)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Movimiento_Cab> Lista_Documentos_por_Pagar(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PROVISION_GENERAL_POR_PAGAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Anio_Ref = Dr.GetString(0),
                                    Id_Periodo_Ref = Dr.GetString(1),
                                    Id_Libro_Ref = Dr.GetString(2),
                                    Id_Voucher_Ref = Dr.GetString(3),
                                    Id_Item_Ref = Dr.GetInt32(4),
                                    Ctb_Cuenta = Dr.GetString(5),
                                    Ctb_Serie_det = Dr.GetString(6),
                                    Ctb_Numero_det = Dr.GetString(7),
                                    Ctb_Tipo_Doc_det = Dr.GetString(8),
                                    Ctb_Tipo_Doc_det_desc = Dr.GetString(9),
                                    Ctb_Ruc_dni_det = Dr.GetString(10),
                                    Entidad_det = Dr.GetString(11),
                                    Ctb_Fecha_Mov_det = Dr.GetDateTime(12),
                                    Ctb_moneda_cod_det = Dr.GetString(13),
                                    Ctb_moneda_det_desc = Dr.GetString(14),
                                    Ctb_Tipo_Cambio_Cod_Det = Dr.GetString(15),
                                    Ctb_Tipo_Cambio_Desc_Det = Dr.GetString(16),
                                    Ctb_Tipo_Cambio_Valor_Det = Dr.GetDecimal(17),
                                    Ctb_Importe_Haber = Dr.GetDecimal(18),
                                    Ctb_Importe_Amortizado = Dr.GetDecimal(19),
                                    Ctb_Importe_Saldo = Dr.GetDecimal(20),
                                    Ctb_Cuenta_Desc = Dr.GetString(21),
                                    Ctb_Tipo_DH = Dr.GetString(22),
                                    CCtb_Tipo_DH_Interno = Dr.GetString(23),
                                    Ctb_Tipo_DH_Desc = Dr.GetString(24),
                                    Ctb_Tipo_Ent_det = Dr.GetString(25),
                                    Ctb_Tipo_Ent_det_Desc = Dr.GetString(26),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(27),
                                    Ctb_Importe_Haber_Extr = Dr.GetDecimal(28),
                                    Ctb_Tipo_Doc_det_Sunat = Dr.GetString(29),
                                    Ctb_moneda_cod_det_Sunat = Dr.GetString(30)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        #region Caja chica

        public void Insertar_Caja_Chica(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;

                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_IE;
                        Cmd.Parameters.Add("@Ctb_Tipo_Movimiento", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Codigo_Caja_Chica", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Codigo_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        //@ , 
                        //@ , 
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        //@
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Importe_MN", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_MN;
                        Cmd.Parameters.Add("@Ctb_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe;
                        Cmd.Parameters.Add("@Ctb_Importe_ME", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_ME;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Caja_CHica_Estado", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Caja_CHica_Estado;
                         Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.Parameters.Add("@Ctb_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Cierre_Caja_Chica;
                   
                        Cmd.Parameters.Add("@Ctb_Importe_Rendido", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Rendido;
                        Cmd.Parameters.Add("@Ctb_Importe_Rendido_Saldo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Rendido_Saldo;
                        Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Id_Anio_Ref;
                        Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char,2).Value = Clas_Enti.Id_Periodo_Ref;
                        Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Id_Libro_Ref;
                        Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char,10).Value = Clas_Enti.Id_Voucher_Ref;


                        Cmd.Parameters.Add("@Ctb_Es_Dif_Cambio", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Dif_Cambio;
                        Cmd.Parameters.Add("@Ctb_Importe_Dif_Cambio", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Dif_Cambio;
                        Cmd.Parameters.Add("@Ctb_Es_Redondeo", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Redondeo;
                        Cmd.Parameters.Add("@Ctb_Importe_Redondeo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Redondeo;
                        Cmd.Parameters.Add("@Ctb_Importe_Diferencias", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Diferencias;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Voucher = Cmd.Parameters["@Id_Voucher"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;

                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);

                            Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Id_Anio_Ref) ? null : ent.Id_Anio_Ref);
                            Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Id_Periodo_Ref) ? null : ent.Id_Periodo_Ref);
                            Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Id_Libro_Ref) ? null : ent.Id_Libro_Ref);
                            Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;
                            Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.Int).Value = ent.Id_Item_Ref;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                          Cmd.Parameters.Add("@Ctb_Es_Cuenta_Principal", System.Data.SqlDbType.Bit).Value = ent.Ctb_Es_Cuenta_Principal;

                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();//DIFERENCIAS -DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_DET_INSERTAR_DIFERENCIAS";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Modificar_Caja_Chica(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value=Clas_Enti.Id_Voucher;

                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_IE;
                        Cmd.Parameters.Add("@Ctb_Tipo_Movimiento", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Tipo_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Codigo_Caja_Chica", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Codigo_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;

                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;


                       Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Importe_MN", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_MN;
                        Cmd.Parameters.Add("@Ctb_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe;
                        Cmd.Parameters.Add("@Ctb_Importe_ME", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_ME;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Caja_CHica_Estado", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Caja_CHica_Estado;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.Parameters.Add("@Ctb_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Cierre_Caja_Chica;

                        Cmd.Parameters.Add("@Ctb_Importe_Rendido", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Rendido;
                        Cmd.Parameters.Add("@Ctb_Importe_Rendido_Saldo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Rendido_Saldo;
                        Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio_Ref;
                        Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo_Ref;
                        Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro_Ref;
                        Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher_Ref;

                        Cmd.Parameters.Add("@Ctb_Es_Dif_Cambio", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Dif_Cambio;
                        Cmd.Parameters.Add("@Ctb_Importe_Dif_Cambio", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Dif_Cambio;
                        Cmd.Parameters.Add("@Ctb_Es_Redondeo", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Redondeo;
                        Cmd.Parameters.Add("@Ctb_Importe_Redondeo", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Redondeo;
                        Cmd.Parameters.Add("@Ctb_Importe_Diferencias", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Diferencias;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Voucher = Cmd.Parameters["@Id_Voucher"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;

                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);

                            Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Id_Anio_Ref) ? null : ent.Id_Anio_Ref);
                            Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Id_Periodo_Ref) ? null : ent.Id_Periodo_Ref);
                            Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Id_Libro_Ref) ? null : ent.Id_Libro_Ref);
                            Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Voucher_Ref;
                            Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.Int).Value = ent.Id_Item_Ref;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Ctb_Es_Cuenta_Principal", System.Data.SqlDbType.Bit).Value = ent.Ctb_Es_Cuenta_Principal;

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Caja_Chica_Cab(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);
                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ctb_Tipo_IE;
                        Cmd.Parameters.Add("@Tipo_Apertura", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Tipo_Rendicion", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Rendicion_Caja_Chica;
                        Cmd.Parameters.Add("@Tipo_Reembolso", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Reembolso_Caja_Chica;
                        Cmd.Parameters.Add("@Tipo_Cierre", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Cierre_Caja_Chica;
                        //(@ IS NULL OR Mov.Ctb_Es_Caja_Chica = @Tipo_Apertura) AND
                        //(@ IS NULL OR mov.Ctb_Rendicion_Caja_Chica = @Tipo_Rendicion) AND
                        //(@ IS NULL OR mov.Ctb_Reembolso_Caja_Chica = @Tipo_Reembolso) AND
                        //(@ IS NULL OR mov.Ctb_Cierre_Caja_Chica = @Tipo_Cierre)
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Tipo_IE = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_IE_Interno = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_IE_Desc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Movimiento = Dr.GetString(8).Trim(),
                                    Ctb_Tipo_Movimiento_Interno = Dr.GetString(9).Trim(),
                                    Ctb_Tipo_Movimiento_Desc = Dr.GetString(10).Trim(),
                                    Ctb_Glosa = Dr.GetString(11).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(12).Trim(),
                                    Nombre_Comprobante = Dr.GetString(13).Trim(),
                                    Ctb_Serie = Dr.GetString(14).Trim(),
                                    Ctb_Numero = Dr.GetString(15).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(16),
                                    Ctb_Tipo_Ent = Dr.GetString(17).Trim(),
                                    Ctb_Tipo_Ent_Desc = Dr.GetString(18),
                                    Ctb_Ruc_dni = Dr.GetString(19).Trim(),
                                    Entidad = Dr.GetString(20).Trim(),
                                    Ctb_Medio_Pago = Dr.GetString(21).Trim(),
                                    Ctb_Medio_Pago_desc = Dr.GetString(22).Trim(),
                                    Ctb_Numero_Transaccion = Dr.GetString(23).Trim(),
                                    Ctb_Cuenta_Corriente = Dr.GetString(24).Trim(),
                                    Ctb_Entidad_Financiera = Dr.GetString(25).Trim(),
                                    Ctb_Entidad_Financiera_Desc = Dr.GetString(26).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(27).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(28).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(29).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(30).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(31),
                                    Ctb_Importe = Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(35),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(36),
                                    Ctn_Moneda_Sunat = Dr.GetString(37),
                                    Ctb_Codigo_Caja_Chica=Dr.GetString(38),
                                    Ctb_Codigo_Caja_Chica_Desc=Dr.GetString(39),
                                    Estado=Dr.GetString(40),
                                    Estado_Fila=Dr.GetString(41),
                                    Ctb_Caja_CHica_Estado=Dr.GetString(42),
                                    Ctb_Caja_CHica_Estado_Desc=Dr.GetString(43),
                                    Ctb_Importe_Rendido=Dr.GetDecimal(44),
                                    Ctb_Importe_Rendido_Saldo=Dr.GetDecimal(45),
                                    Id_Anio_Ref = Dr.GetString(46),
                                    Id_Periodo_Ref = Dr.GetString(47),
                                    Id_Libro_Ref = Dr.GetString(48),
                                    Id_Voucher_Ref = Dr.GetString(49),
                                    Ctb_Es_Dif_Cambio = Dr.GetBoolean(50),
                                    Ctb_Importe_Dif_Cambio = Dr.GetDecimal(51),
                                    Ctb_Es_Redondeo = Dr.GetBoolean(52),
                                    Ctb_Importe_Redondeo = Dr.GetDecimal(53),
                                    Ctb_Importe_Diferencias = Dr.GetDecimal(54),
                                    Id_Periodo_Desc=Dr.GetString(55)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Cab> Listar_Caja_Chica_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_DET_CAJA_CHICA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Id_Item = Dr.GetInt32(5),
                                    Ctb_Cuenta = Dr.GetString(6),
                                    Ctb_Cuenta_Desc = Dr.GetString(7),
                                    Ctb_Tipo_Doc_det = Dr.GetString(8),
                                    Ctb_Tipo_Doc_det_desc = Dr.GetString(9),
                                    Ctb_Serie_det = Dr.GetString(10),
                                    Ctb_Numero_det = Dr.GetString(11),
                                    Ctb_Fecha_Mov_det = Dr.GetDateTime(12),
                                    Ctb_moneda_cod_det = Dr.GetString(13),
                                    Ctb_moneda_det_desc = Dr.GetString(14),
                                    Ctb_Tipo_Cambio_Cod_Det = Dr.GetString(15),
                                    Ctb_Tipo_Cambio_Desc_Det = Dr.GetString(16),
                                    Ctb_Tipo_Cambio_Valor_Det = Dr.GetDecimal(17),
                                    Ctb_Tipo_Ent_det = Dr.GetString(18),
                                    Ctb_Tipo_Ent_det_Desc = Dr.GetString(19),
                                    Ctb_Ruc_dni_det = Dr.GetString(20),
                                    Entidad_det = Dr.GetString(21),
                                    Ctb_Tipo_DH = Dr.GetString(22),
                                    CCtb_Tipo_DH_Interno = Dr.GetString(23),
                                    Ctb_Tipo_DH_Desc = Dr.GetString(24),
                                    Ctb_Importe_Debe = Dr.GetDecimal(25),
                                    Ctb_Importe_Haber = Dr.GetDecimal(26),
                                    Ctb_Importe_Debe_Extr = Dr.GetDecimal(27),
                                    Ctb_Importe_Haber_Extr = Dr.GetDecimal(28),
                                    Id_Anio_Ref = Dr.GetString(29),
                                    Id_Periodo_Ref = Dr.GetString(30),
                                    Id_Libro_Ref = Dr.GetString(31),
                                    Id_Voucher_Ref = Dr.GetString(32),
                                    Id_Item_Ref = Dr.GetInt32(33),
                                    Ctb_Tipo_Doc_det_Sunat = Dr.GetString(34),
                                    Ctb_moneda_cod_det_Sunat = Dr.GetString(35),
                                    Ctb_Es_Cuenta_Principal=Dr.GetBoolean(36)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        //Traendo importes para la rendicion

        public List<Entidad_Movimiento_Cab> Traer_Importes_Rendicion(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_TRAER_IMPORTES", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);
                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ctb_Tipo_IE;
                        Cmd.Parameters.Add("@Ctb_Codigo_Caja_Chica", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Ctb_Codigo_Caja_Chica;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctn_Moneda_Cod = Dr.GetString(5).Trim(),
                                    Ctn_Moneda_Sunat=Dr.GetString(6).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(8).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(9).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(10),
                                    Ctb_Importe=Dr.GetDecimal(11),
                                    Ctb_Importe_Rendido=Dr.GetDecimal(12),
                                    Ctb_Importe_Rendido_Saldo=Dr.GetDecimal(13),
                                    Ctb_Es_moneda_nac=Dr.GetBoolean(14)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Cab> Listar_Caja_Chica_Estado(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAJA_CHICA_CAB_BUSCAR_ESTADOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.Id_Libro == "" ? null : Cls_Enti.Id_Libro);
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);
                        Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Ctb_Tipo_IE;
                        //Cmd.Parameters.Add("@Tipo_Apertura", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Es_Caja_Chica;
                        //Cmd.Parameters.Add("@Tipo_Rendicion", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Rendicion_Caja_Chica;
                        //Cmd.Parameters.Add("@Tipo_Reembolso", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Reembolso_Caja_Chica;
                        //Cmd.Parameters.Add("@Tipo_Cierre", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ctb_Cierre_Caja_Chica;
                        Cmd.Parameters.Add("@Ctb_Caja_CHica_Estado", System.Data.SqlDbType.Char,4).Value = Cls_Enti.Ctb_Caja_CHica_Estado;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Tipo_IE = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_IE_Interno = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_IE_Desc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Movimiento = Dr.GetString(8).Trim(),
                                    Ctb_Tipo_Movimiento_Interno = Dr.GetString(9).Trim(),
                                    Ctb_Tipo_Movimiento_Desc = Dr.GetString(10).Trim(),
                                    Ctb_Glosa = Dr.GetString(11).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(12).Trim(),
                                    Nombre_Comprobante = Dr.GetString(13).Trim(),
                                    Ctb_Serie = Dr.GetString(14).Trim(),
                                    Ctb_Numero = Dr.GetString(15).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(16),
                                    Ctb_Tipo_Ent = Dr.GetString(17).Trim(),
                                    Ctb_Tipo_Ent_Desc = Dr.GetString(18),
                                    Ctb_Ruc_dni = Dr.GetString(19).Trim(),
                                    Entidad = Dr.GetString(20).Trim(),
                                    Ctb_Medio_Pago = Dr.GetString(21).Trim(),
                                    Ctb_Medio_Pago_desc = Dr.GetString(22).Trim(),
                                    Ctb_Numero_Transaccion = Dr.GetString(23).Trim(),
                                    Ctb_Cuenta_Corriente = Dr.GetString(24).Trim(),
                                    Ctb_Entidad_Financiera = Dr.GetString(25).Trim(),
                                    Ctb_Entidad_Financiera_Desc = Dr.GetString(26).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(27).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(28).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(29).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(30).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(31),
                                    Ctb_Importe = Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(35),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(36),
                                    Ctn_Moneda_Sunat = Dr.GetString(37),
                                    Ctb_Codigo_Caja_Chica = Dr.GetString(38),
                                    Ctb_Codigo_Caja_Chica_Desc = Dr.GetString(39),
                                    Estado = Dr.GetString(40),
                                    Estado_Fila = Dr.GetString(41),
                                    Ctb_Caja_CHica_Estado = Dr.GetString(42),
                                    Ctb_Caja_CHica_Estado_Desc = Dr.GetString(43),
                                    Ctb_Importe_Rendido = Dr.GetDecimal(44),
                                    Ctb_Importe_Rendido_Saldo = Dr.GetDecimal(45),
                                    Ctb_Cuenta_Caja_Chica=Dr.GetString(46),
                                    Ctb_Cuenta_Caja_Chica_Desc=Dr.GetString(47)
                                  //,  Ctb_Caja_Chica_Responsable=Dr.GetString(48),
                                  //  Ctb_Caja_Chica_Responsable_Desc=Dr.GetString(49)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        #endregion


        public List<Entidad_Movimiento_Cab> Listar_Lotes_Compra(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Lot_Adm_item = Dr.GetInt32(5),
                                    Lot_item = Dr.GetInt32(6),
                                    Lot_Catalogo = Dr.GetString(7),
                                    Lot_Lote = Dr.GetString(8),
                                    Lot_FechaFabricacion = Dr.GetDateTime(9),
                                    Lot_FechaVencimiento = Dr.GetDateTime(10),
                                    Lot_Cantidad = Dr.GetDecimal(11)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Movimiento_Cab> Listar_adm(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_LISTAR_ADM", Cn) { CommandType = System.Data.CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Glosa = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(6).Trim(),
                                    Nombre_Comprobante = Dr.GetString(7).Trim(),
                                    Ctb_Serie = Dr.GetString(8).Trim(),
                                    Ctb_Numero = Dr.GetString(9).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(10),
                                    Ctb_Tipo_Ent = Dr.GetString(11).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(12).Trim(),
                                    Entidad = Dr.GetString(13).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(14).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(15).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(16).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(17).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(18).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(19).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(20).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(21),
                                    Ctb_Base_Imponible = Dr.GetDecimal(22),
                                    Ctb_Igv = Dr.GetDecimal(23),
                                    Ctb_Base_Imponible2 = Dr.GetDecimal(24),
                                    Ctb_Igv2 = Dr.GetDecimal(25),
                                    Ctb_Base_Imponible3 = Dr.GetDecimal(26),
                                    Ctb_Igv3 = Dr.GetDecimal(27),
                                    Ctb_Importe_Total = Dr.GetDecimal(28),
                                    Ctb_Isc = Dr.GetBoolean(29),
                                    Ctb_Isc_Importe = Dr.GetDecimal(30),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(31),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(32),
                                    Ctb_No_Gravadas = Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(34),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(35),
                                    Ctb_Exonerada = Dr.GetDecimal(36),
                                    Ctb_Inafecta = Dr.GetDecimal(37),
                                    Ctb_Afecto_RE = Dr.GetBoolean(38),
                                    Ctb_Afecto_Tipo = Dr.GetString(39).Trim(),
                                    Ctb_Afecto_Tipo_cod_Interno = Dr.GetString(40).Trim(),
                                    Ctb_Afecto_Tipo_Descripcion = Dr.GetString(41).Trim(),
                                    Ctb_Afecto_Tipo_Doc = Dr.GetString(42).Trim(),
                                    Ctb_Afecto_Tipo_Doc_DEsc = Dr.GetString(43).Trim(),
                                    Ctb_Afecto_Serie = Dr.GetString(44).Trim(),
                                    Ctb_Afecto_Numero = Dr.GetString(45).Trim(),
                                    Ctb_Afecto_Porcentaje = Dr.GetDecimal(46),
                                    Ctb_Afecto_Monto = Dr.GetDecimal(47),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(48),
                                    Estado_Fila = Dr.GetString(49).Trim(),
                                    Ctb_Analisis = Dr.GetString(50).Trim(),
                                    Ctb_Analisis_Desc = Dr.GetString(51).Trim(),
                                    Ctb_Afecto_Fecha = Dr.GetDateTime(52),
                                    Ctb_Fecha_Vencimiento = Dr.GetDateTime(53),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(54),
                                    Ctn_Moneda_Sunat = Dr.GetString(55),
                                    Id_Periodo_Desc = Dr.GetString(56).Trim(),
                                    ctb_pdc_codigo = Dr.GetString(57),
                                    ctb_pdc_Nombre = Dr.GetString(58).Trim(),
                                    ctb_pdv_codigo = Dr.GetString(59),
                                    ctb_pdv_Nombre = Dr.GetString(60).Trim(),
                                    Cja_Codigo = Dr.GetString(61),
                                    Cja_Aper_Cierre_Codigo = Dr.GetInt32(62),
                                    Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(63),
                                    cja_estado = Dr.GetString(64).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
       }

        public List<Entidad_Movimiento_Cab> Listar_adm_ventas(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_LISTAR_ADM_VENTAS", Cn) { CommandType = System.Data.CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Glosa = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(6).Trim(),
                                    Nombre_Comprobante = Dr.GetString(7).Trim(),
                                    Ctb_Serie = Dr.GetString(8).Trim(),
                                    Ctb_Numero = Dr.GetString(9).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(10),
                                    Ctb_Tipo_Ent = Dr.GetString(11).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(12).Trim(),
                                    Entidad = Dr.GetString(13).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(14).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(15).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(16).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(17).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(18).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(19).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(20).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(21),
                                    Ctb_Base_Imponible = Dr.GetDecimal(22),
                                    Ctb_Igv = Dr.GetDecimal(23),
                                    Ctb_Base_Imponible2 = Dr.GetDecimal(24),
                                    Ctb_Igv2 = Dr.GetDecimal(25),
                                    Ctb_Base_Imponible3 = Dr.GetDecimal(26),
                                    Ctb_Igv3 = Dr.GetDecimal(27),
                                    Ctb_Importe_Total = Dr.GetDecimal(28),
                                    Ctb_Isc = Dr.GetBoolean(29),
                                    Ctb_Isc_Importe = Dr.GetDecimal(30),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(31),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(32),
                                    Ctb_No_Gravadas = Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(34),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(35),
                                    Ctb_Exonerada = Dr.GetDecimal(36),
                                    Ctb_Inafecta = Dr.GetDecimal(37),
                                    Ctb_Afecto_RE = Dr.GetBoolean(38),
                                    Ctb_Afecto_Tipo = Dr.GetString(39).Trim(),
                                    //Ctb_Afecto_Tipo_cod_Interno = Dr.GetString(40).Trim(),
                                    //Ctb_Afecto_Tipo_Descripcion = Dr.GetString(41).Trim(),
                                    Ctb_Afecto_Tipo_Doc = Dr.GetString(40).Trim(),
                                    //Ctb_Afecto_Tipo_Doc_DEsc = Dr.GetString(43).Trim(),
                                    Ctb_Afecto_Serie = Dr.GetString(41).Trim(),
                                    Ctb_Afecto_Numero = Dr.GetString(42).Trim(),
                                    Ctb_Afecto_Porcentaje = Dr.GetDecimal(43),
                                    Ctb_Afecto_Monto = Dr.GetDecimal(44),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(45),
                                    Estado_Fila = Dr.GetString(46).Trim(),
                                    Ctb_Analisis = Dr.GetString(47).Trim(),
                                    //Ctb_Analisis_Desc = Dr.GetString(51).Trim(),
                                    Ctb_Afecto_Fecha = Dr.GetDateTime(48),
                                    Ctb_Fecha_Vencimiento = Dr.GetDateTime(49),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(50),
                                    Ctn_Moneda_Sunat = Dr.GetString(51),
                                    //Id_Periodo_Desc = Dr.GetString(55).Trim(),
                                    //ctb_pdc_codigo = Dr.GetString(57),
                                    //ctb_pdc_Nombre = Dr.GetString(58).Trim(),
                                    ctb_pdv_codigo = Dr.GetString(52),
                                    ctb_pdv_Nombre = Dr.GetString(53).Trim(),
                                    Cja_Codigo = Dr.GetString(54),
                                    Cja_Aper_Cierre_Codigo = Dr.GetInt32(55),
                                    Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(56),
                                    cja_estado = Dr.GetString(57).Trim(),
                                    Est_Facturacion = Dr.GetString(58).Trim(),
                                    Est_Facturacion_Interno = Dr.GetString(59).Trim(),
                                    Est_Facturacion_Desc = Dr.GetString(60).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public void Insertar_adm_Compra(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_INSERTAR_ADM", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ctb_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ctb_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible2;
                        Cmd.Parameters.Add("@Ctb_Igv2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv2;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible3;
                        Cmd.Parameters.Add("@Ctb_Igv3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv3;

                        Cmd.Parameters.Add("@Ctb_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ctb_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ctb_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ctb_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;


                        Cmd.Parameters.Add("@Ctb_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ctb_No_Gravadas", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_No_Gravadas;

                        Cmd.Parameters.Add("@Ctb_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;

                        Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Afecto_RE;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Afecto_Tipo;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Afecto_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ctb_Afecto_Serie;
                        Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar, 25).Value = Clas_Enti.Ctb_Afecto_Numero;
                        Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Porcentaje;
                        Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Monto;

                        Cmd.Parameters.Add("@Ctb_Tasa_IGV", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;
                        Cmd.Parameters.Add("@Ctb_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;


                        Cmd.Parameters.Add("@Ctb_Afecto_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Afecto_Fecha;
                        Cmd.Parameters.Add("@Ctb_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Vencimiento;

                        Cmd.Parameters.Add("@ctb_pdc_codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.ctb_pdc_codigo;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.ctb_pdv_codigo;


                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                       Cmd.Parameters.Add("@Ctb_Es_Compra_Comercial", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Compra_Comercial;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Voucher = Cmd.Parameters["@Id_Voucher"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.Parameters.Add("@Adm_Id_Unm", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Unm_Id;
                            Cmd.Parameters.Add("@Adm_Tiene_Lote", System.Data.SqlDbType.Bit).Value = ent.Acepta_lotes;
                            Cmd.Parameters.Add("@Adm_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = ent.Adm_Tipo_Operacion_Interno;

                            Cmd.ExecuteNonQuery();
                        }

                        //DETALLE DE LOTES SI TUVIERA
                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleLotes)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaFabricacion;
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;

                            Cmd.ExecuteNonQuery();
                        }

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Operacion_Cod;
                            Cmd.Parameters.Add("@Ctb_Igv_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Igv;
                            Cmd.Parameters.Add("@Ctb_Centro_CG", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Tipo_BSA", System.Data.SqlDbType.Char).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);
                            Cmd.Parameters.Add("@Ctb_Catalogo", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Ctb_Catalogo) ? null : ent.Ctb_Catalogo);

                            Cmd.Parameters.Add("@Ctb_Valor_unit", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Valor_Unit;
                            Cmd.Parameters.Add("@Ctb_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Cantidad;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Almacen", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Ctb_Almacen_cod) ? null : ent.Ctb_Almacen_cod);
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);

                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Ctb_Cta_Destino", System.Data.SqlDbType.Bit).Value = ent.Cta_Destino;

                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();//DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_DESTINO_INSERTAR";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                        Cmd.Parameters.Clear();//INSERTAR LA COMPRA A INVENTARIO COMO ENTRADA
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_INSERTAR_COMPRA_ADM";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Modificar_adm(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_MODIFICAR_ADM", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ctb_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ctb_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible2;
                        Cmd.Parameters.Add("@Ctb_Igv2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv2;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible3;
                        Cmd.Parameters.Add("@Ctb_Igv3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv3;

                        Cmd.Parameters.Add("@Ctb_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ctb_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ctb_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ctb_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;


                        Cmd.Parameters.Add("@Ctb_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ctb_No_Gravadas", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_No_Gravadas;

                        Cmd.Parameters.Add("@Ctb_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;

                        Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Afecto_RE;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Afecto_Tipo;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Afecto_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ctb_Afecto_Serie;
                        Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar, 25).Value = Clas_Enti.Ctb_Afecto_Numero;
                        Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Porcentaje;
                        Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Monto;

                        Cmd.Parameters.Add("@Ctb_Tasa_IGV", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;
                        Cmd.Parameters.Add("@Ctb_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;

                        Cmd.Parameters.Add("@Ctb_Afecto_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Afecto_Fecha;
                        Cmd.Parameters.Add("@Ctb_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Vencimiento;

                        Cmd.Parameters.Add("@ctb_pdc_codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.ctb_pdc_codigo;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.ctb_pdv_codigo;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;


                        Cmd.Parameters.Add("@Ctb_Es_Compra_Comercial", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Es_Compra_Comercial;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Operacion_Cod;
                            Cmd.Parameters.Add("@Ctb_Igv_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Igv;
                            Cmd.Parameters.Add("@Ctb_Centro_CG", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;


                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;//ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;//ent.Ctb_Importe_Haber;

                            Cmd.Parameters.Add("@Ctb_Tipo_BSA", System.Data.SqlDbType.Char).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);
                            Cmd.Parameters.Add("@Ctb_Catalogo", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Ctb_Catalogo) ? null : ent.Ctb_Catalogo);

                            Cmd.Parameters.Add("@Ctb_Valor_unit", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Valor_Unit;
                            Cmd.Parameters.Add("@Ctb_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Cantidad;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Almacen", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Ctb_Almacen_cod) ? null : ent.Ctb_Almacen_cod);
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);

                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Ctb_Cta_Destino", System.Data.SqlDbType.Bit).Value = ent.Cta_Destino;

                            Cmd.ExecuteNonQuery();
                        }


                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.Parameters.Add("@Adm_Id_Unm", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Unm_Id;
                            Cmd.Parameters.Add("@Adm_Tiene_Lote", System.Data.SqlDbType.Bit).Value = ent.Acepta_lotes;
                            Cmd.Parameters.Add("@Adm_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = ent.Adm_Tipo_Operacion_Interno;

                            Cmd.ExecuteNonQuery();
                        }

                        //DETALLE DE LOTES SI TUVIERA
                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleLotes)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaFabricacion;
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;

                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();//DESTINO
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_DESTINO_INSERTAR";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.ExecuteNonQuery();

                        Cmd.Parameters.Clear();//INSERTAR LA COMPRA A INVENTARIO COMO ENTRADA
                        Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_INSERTAR_COMPRA_ADM";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar_Adm_Compra(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ELIMINAR_ADM_COMPRA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Anular_Adm_Compra(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ANULAR_ADM_COMPRA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Cab> Listar_adm_Logistica(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_LISTAR_ADM_LOGISTICA", Cn) { CommandType = System.Data.CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Voucher == "" ? null : Cls_Enti.Id_Voucher);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Libro = Dr.GetString(3),
                                    Id_Voucher = Dr.GetString(4),
                                    Ctb_Glosa = Dr.GetString(5).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(6).Trim(),
                                    Nombre_Comprobante = Dr.GetString(7).Trim(),
                                    Ctb_Serie = Dr.GetString(8).Trim(),
                                    Ctb_Numero = Dr.GetString(9).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(10),
                                    Ctb_Tipo_Ent = Dr.GetString(11).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(12).Trim(),
                                    Entidad = Dr.GetString(13).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(14).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(15).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(16).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(17).Trim(),
                                    Ctn_Moneda_Desc = Dr.GetString(18).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(19).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(20).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(21),
                                    Ctb_Base_Imponible = Dr.GetDecimal(22),
                                    Ctb_Igv = Dr.GetDecimal(23),
                                    Ctb_Base_Imponible2 = Dr.GetDecimal(24),
                                    Ctb_Igv2 = Dr.GetDecimal(25),
                                    Ctb_Base_Imponible3 = Dr.GetDecimal(26),
                                    Ctb_Igv3 = Dr.GetDecimal(27),
                                    Ctb_Importe_Total = Dr.GetDecimal(28),
                                    Ctb_Isc = Dr.GetBoolean(29),
                                    Ctb_Isc_Importe = Dr.GetDecimal(30),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(31),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(32),
                                    Ctb_No_Gravadas = Dr.GetDecimal(33),
                                    Ctb_Es_moneda_nac = Dr.GetBoolean(34),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(35),
                                    Ctb_Exonerada = Dr.GetDecimal(36),
                                    Ctb_Inafecta = Dr.GetDecimal(37),
                                    Ctb_Afecto_RE = Dr.GetBoolean(38),
                                    Ctb_Afecto_Tipo = Dr.GetString(39).Trim(),
                                    Ctb_Afecto_Tipo_cod_Interno = Dr.GetString(40).Trim(),
                                    Ctb_Afecto_Tipo_Descripcion = Dr.GetString(41).Trim(),
                                    Ctb_Afecto_Tipo_Doc = Dr.GetString(42).Trim(),
                                    Ctb_Afecto_Tipo_Doc_DEsc = Dr.GetString(43).Trim(),
                                    Ctb_Afecto_Serie = Dr.GetString(44).Trim(),
                                    Ctb_Afecto_Numero = Dr.GetString(45).Trim(),
                                    Ctb_Afecto_Porcentaje = Dr.GetDecimal(46),
                                    Ctb_Afecto_Monto = Dr.GetDecimal(47),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(48),
                                    Estado_Fila = Dr.GetString(49).Trim(),
                                    Ctb_Analisis = Dr.GetString(50).Trim(),
                                    Ctb_Analisis_Desc = Dr.GetString(51).Trim(),
                                    Ctb_Afecto_Fecha = Dr.GetDateTime(52),
                                    Ctb_Fecha_Vencimiento = Dr.GetDateTime(53),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(54),
                                    Ctn_Moneda_Sunat = Dr.GetString(55),
                                    Id_Periodo_Desc = Dr.GetString(56),
                                    ctb_pdc_codigo = Dr.GetString(57),
                                    ctb_pdc_Nombre = Dr.GetString(58),
                                    Ctb_Es_Compra_Comercial=Dr.GetBoolean(59)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }




        public void Insertar_adm_Ventas(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_MOVIMIENTO_VENTA_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;

                        Cmd.Parameters.Add("@Ven_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ven_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;
                        Cmd.Parameters.Add("@Ven_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ven_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ven_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ven_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ven_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ven_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ven_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ven_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;

                        Cmd.Parameters.Add("@Ven_Igv_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;

                        Cmd.Parameters.Add("@Ven_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ven_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ven_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ven_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ven_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;
                        Cmd.Parameters.Add("@Ven_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ven_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ven_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;
                        Cmd.Parameters.Add("@Ven_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ven_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ven_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;

                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo_Item;
                        Cmd.Parameters.Add("@Cja_estado", System.Data.SqlDbType.Char).Value = Clas_Enti.cja_estado;

                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Voucher;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        
                        Cmd.Parameters.Add("@Est_Facturacion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Facturacion;

                        //Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Afecto_RE;
                        //Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Afecto_Tipo;
                        //Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Afecto_Tipo_Doc;
                        //Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ctb_Afecto_Serie;
                        //Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar, 25).Value = Clas_Enti.Ctb_Afecto_Numero;
                        //Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Porcentaje;
                        //Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Monto;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.Parameters.Add("@Adm_Unm_Id", System.Data.SqlDbType.VarChar).Value = ent.Adm_Unm_Id;
                            Cmd.Parameters.Add("@Adm_Tiene_Lote", System.Data.SqlDbType.VarChar).Value = ent.Acepta_lotes;

                            Cmd.Parameters.Add("@Adm_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Adm_FechaFabricacion", System.Data.SqlDbType.Date).Value = ent.Lot_FechaFabricacion;
                            Cmd.Parameters.Add("@Adm_FechaVencimiento", System.Data.SqlDbType.Date).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Adm_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = ent.Adm_CantidadAtendidaEnBaseAlCoefciente;

                            Cmd.Parameters.Add("@Ven_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = ent.Adm_Tipo_Operacion;
                            Cmd.Parameters.Add("@Ven_Afecto_IGV_FE", System.Data.SqlDbType.VarChar).Value = ent.Ven_Afecto_IGV_FE;
                            //@Adm_Valor_Venta DECIMAL(18,4)= NULL,
                            //@Adm_IgvTasa DECIMAL(18,4)= NULL,
                            //@Adm_Subtotal DECIMAL(18,4)= NULL,
                            //@Adm_IgvTotal DECIMAL(18,4)= NULL
                            Cmd.Parameters.Add("@Adm_Valor_Venta", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Venta;
                            Cmd.Parameters.Add("@Adm_IgvTasa", System.Data.SqlDbType.Decimal).Value = ent.DvtD_IGVTasaPorcentaje;
                            Cmd.Parameters.Add("@Adm_Subtotal", System.Data.SqlDbType.Decimal).Value = ent.DvtD_SubTotal;
                            Cmd.Parameters.Add("@Adm_IgvTotal", System.Data.SqlDbType.Decimal).Value = ent.DvtD_IGVMonto;


                            Cmd.ExecuteNonQuery();
                        }

                        //insertando lotes para venta

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleLotes)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_LOTE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;

                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = ent.Lot_FechaFabricacion;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;
                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();
                        Cmd.CommandText = "pa_VEN_MOVIMIENTO_INVENTARIO_INSERTAR_ADM";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.ExecuteNonQuery();



                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Insertar_ctb_Ventas(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_INSERTAR_ADM_VENTAS", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;

                        Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ctb_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ctb_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible2;
                        Cmd.Parameters.Add("@Ctb_Igv2", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv2;
                        Cmd.Parameters.Add("@Ctb_Base_Imponible3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible3;
                        Cmd.Parameters.Add("@Ctb_Igv3", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv3;

                        Cmd.Parameters.Add("@Ctb_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ctb_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ctb_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ctb_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;


                        Cmd.Parameters.Add("@Ctb_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ctb_No_Gravadas", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_No_Gravadas;

                        Cmd.Parameters.Add("@Ctb_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ctb_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;

                        Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Afecto_RE;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Afecto_Tipo;
                        Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Afecto_Tipo_Doc;
                        Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.Char, 8).Value = Clas_Enti.Ctb_Afecto_Serie;
                        Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar, 25).Value = Clas_Enti.Ctb_Afecto_Numero;
                        Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Porcentaje;
                        Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Afecto_Monto;

                        Cmd.Parameters.Add("@Ctb_Tasa_IGV", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;
                        Cmd.Parameters.Add("@Ctb_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;


                        Cmd.Parameters.Add("@Ctb_Afecto_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Afecto_Fecha;
                        Cmd.Parameters.Add("@Ctb_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Vencimiento;

                        Cmd.Parameters.Add("@ctb_pdc_codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.ctb_pdc_codigo;
                        Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.ctb_pdv_codigo;


                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo_Item;
                        Cmd.Parameters.Add("@cja_estado", System.Data.SqlDbType.Char).Value = Clas_Enti.cja_estado;
                        Cmd.Parameters.Add("@Est_Facturacion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Facturacion;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Voucher = Cmd.Parameters["@Id_Voucher"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_ADM_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.Parameters.Add("@Adm_Id_Unm", System.Data.SqlDbType.VarChar).Value = ent.Adm_Unm_Id;
                            Cmd.Parameters.Add("@Adm_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = ent.Adm_Tipo_Operacion_Interno;

                            Cmd.ExecuteNonQuery();
                        }

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleAsiento)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_MOVIMIENTO_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ctb_Cuenta;
                            Cmd.Parameters.Add("@Ctb_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Operacion_Cod;
                            Cmd.Parameters.Add("@Ctb_Igv_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Igv;
                            Cmd.Parameters.Add("@Ctb_Centro_CG", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber;
                            Cmd.Parameters.Add("@Ctb_Tipo_BSA", System.Data.SqlDbType.Char).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_BSA) ? null : ent.Ctb_Tipo_BSA);
                            Cmd.Parameters.Add("@Ctb_Catalogo", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(ent.Ctb_Catalogo) ? null : ent.Ctb_Catalogo);

                            Cmd.Parameters.Add("@Ctb_Valor_unit", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Valor_Unit;
                            Cmd.Parameters.Add("@Ctb_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Cantidad;
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Debe_Extr;
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = ent.Ctb_Importe_Haber_Extr;


                            Cmd.Parameters.Add("@Ctb_Almacen", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(ent.Ctb_Almacen_cod) ? null : ent.Ctb_Almacen_cod);
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Doc_det) ? null : ent.Ctb_Tipo_Doc_det);
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.Char, 4).Value = (string.IsNullOrEmpty(ent.Ctb_Serie_det) ? null : ent.Ctb_Serie_det);
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.Char, 8).Value = (string.IsNullOrEmpty(ent.Ctb_Numero_det) ? null : ent.Ctb_Numero_det);
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = ent.Ctb_Fecha_Mov_det;
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Ent_det) ? null : ent.Ctb_Tipo_Ent_det);
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.Char, 11).Value = (string.IsNullOrEmpty(ent.Ctb_Ruc_dni_det) ? null : ent.Ctb_Ruc_dni_det);
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_moneda_cod_det) ? null : ent.Ctb_moneda_cod_det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.Char, 3).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Cod_Det) ? null : ent.Ctb_Tipo_Cambio_Cod_Det);
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar, 50).Value = (string.IsNullOrEmpty(ent.Ctb_Tipo_Cambio_Desc_Det) ? null : ent.Ctb_Tipo_Cambio_Desc_Det);

                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = ((ent.Ctb_Tipo_Cambio_Valor_Det == 0) ? 0 : ent.Ctb_Tipo_Cambio_Valor_Det);


                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Ctb_Cta_Destino", System.Data.SqlDbType.Bit).Value = ent.Cta_Destino;

                            Cmd.ExecuteNonQuery();
                        }

                        //Cmd.Parameters.Clear();
                        //Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_INSERTAR_ADM";
                        //Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        //Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        //Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        //Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        //Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        //Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        //Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        //Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public void Eliminar_Venta(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ELIMINAR_ADM_VENTA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Anular_Venta(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_CONTABLE_CAB_ANULAR_ADM_VENTA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Voucher;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Listar_


        public List<Entidad_Movimiento_Cab> Listar_(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_CAB_LISTAR_SP", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.ctb_pdv_codigo == "" ? null : Cls_Enti.ctb_pdv_codigo);
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Movimiento == "" ? null : Cls_Enti.Id_Movimiento);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    ctb_pdv_codigo = Dr.GetString(3),
                                    ctb_pdv_Nombre = Dr.GetString(4).Trim(),
                                    Id_Movimiento = Dr.GetString(5),
                                    Ctb_Glosa = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(8).Trim(),
                                    Nombre_Comprobante = Dr.GetString(9).Trim(),
                                    Ctb_Serie = Dr.GetString(10).Trim(),
                                    Ctb_Numero = Dr.GetString(11).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(12),
                                    Ctb_Fecha_Vencimiento = Dr.GetDateTime(13),
                                    Ctb_Tipo_Ent = Dr.GetString(14).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(15).Trim(),
                                    Entidad = Dr.GetString(16).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(17).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(18).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(19).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(20).Trim(),
                                    Ctn_Moneda_Sunat = Dr.GetString(21),
                                    Ctn_Moneda_Desc = Dr.GetString(22).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(23).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(24).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(25),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(26),
                                    Ctb_Base_Imponible = Dr.GetDecimal(27),
                                    Ctb_Igv = Dr.GetDecimal(28),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(29),
                                    Ctb_Exonerada = Dr.GetDecimal(30),
                                    Ctb_Inafecta = Dr.GetDecimal(31),
                                   Ctb_Isc = Dr.GetBoolean(32),
                                    Ctb_Isc_Importe = Dr.GetDecimal(33),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(34),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(35),
                                    Ctb_Importe_Total = Dr.GetDecimal(36),
                                     Ctb_Analisis = Dr.GetString(37).Trim(),
                                    Ctb_Analisis_Desc = Dr.GetString(38).Trim(),
                                    Cja_Codigo=Dr.GetString(39),
                                    Cja_Aper_Cierre_Codigo=Dr.GetInt32(40),
                                    Cja_Aper_Cierre_Codigo_Item=Dr.GetInt32(41),
                                    cja_estado=Dr.GetString(42),
                                    Estado = Dr.GetString(43),
                                    Id_Libro = Dr.GetString(44),
                                    Id_Libro_Nombre = Dr.GetString(45),
                                    Id_Libro_Sunat = Dr.GetString(46),
                                    Id_Voucher =  Dr.GetString(47),
                                    Id_Periodo_Desc = Dr.GetString(48),
                                    Estado_Fila = Dr.GetString(50).Trim(),
                                    Ven_FactElectronica_Estado = Dr.GetString(53).Trim(),
                                    Ven_FactElectronica_Estado_Desc = Dr.GetString(54).Trim(),
                                    XML_DigestValue = Dr.GetString(55).Trim(),
                                    XML_SignatureValue = Dr.GetString(56).Trim(),
                                    Ent_TpDcEnt_SUNAT = Dr.GetString(57).Trim(),
                                    Cliente_Direccion = Dr.GetString(58).Trim(),
                                    Correo_Cliente = Dr.GetString(59).Trim(),
                                    Est_Facturacion = Dr.GetString(60).Trim(),
                                    Est_Facturacion_Interno = Dr.GetString(61).Trim(),
                                    Est_Facturacion_Desc = Dr.GetString(62).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Movimiento_Cab> Listar_Det_(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_DET_LISTAR_SP", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.ctb_pdv_codigo == "" ? null : Cls_Enti.ctb_pdv_codigo);
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Movimiento == "" ? null : Cls_Enti.Id_Movimiento);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    ctb_pdv_codigo = Dr.GetString(3),
                                    Id_Movimiento = Dr.GetString(4),
                                    Adm_Item = Dr.GetInt32(5),
                                    Adm_Tipo_BSA = Dr.GetString(6).Trim(),
                                    Adm_Tipo_BSA_Interno = Dr.GetString(7).Trim(),
                                    Adm_Tipo_BSA_Desc = Dr.GetString(8).Trim(),
                                    Adm_Catalogo = Dr.GetString(9).Trim(),
                                    Adm_Catalogo_Desc = Dr.GetString(10).Trim(),
                                    Adm_Almacen = Dr.GetString(11).Trim(),
                                    Adm_Almacen_desc = Dr.GetString(12).Trim(),
                                    Adm_Cantidad = Dr.GetDecimal(13),
                                    Adm_Valor_Unit = Dr.GetDecimal(14),
                                    Adm_Total = Dr.GetDecimal(15),
                                    Adm_Unm_Id = Dr.GetString(16).Trim(),
                                    Adm_Unm_Desc = Dr.GetString(17).Trim(),
                                    UNM_CoigodFE = Dr.GetString(18).Trim(),
                                    Acepta_lotes = Dr.GetBoolean(19),
                                    Lot_Lote = Dr.GetString(20),
                                    Lot_FechaFabricacion=Dr.GetDateTime(21),
                                    Lot_FechaVencimiento = Dr.GetDateTime(22),
                                    Adm_CantidadAtendidaEnBaseAlCoefciente=Dr.GetDecimal(23),
                                    Adm_Tipo_Operacion = Dr.GetString(24).Trim(),
                                    DvtD_OperCodigo = Dr.GetString(24).Trim(),
                                    Adm_Tipo_Operacion_Interno = Dr.GetString(25).Trim(),
                                    Adm_Tipo_Operacion_Desc = Dr.GetString(26).Trim(),
                                    Ven_Afecto_IGV_FE = Dr.GetString(27).Trim(),
                                    Ven_Afecto_IGV_FE_Desc = Dr.GetString(28).Trim(),
                                    DvtD_IGVTasaPorcentaje =Dr.GetDecimal(29),
                                    Adm_Valor_Venta = Dr.GetDecimal(30)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public void Modificar_adm_Ventas(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_MOVIMIENTO_VENTA_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                        Cmd.Parameters.Add("@Ven_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ven_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;
                        Cmd.Parameters.Add("@Ven_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ven_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ven_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ven_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ven_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ven_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ven_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ven_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;

                        Cmd.Parameters.Add("@Ven_Igv_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;

                        Cmd.Parameters.Add("@Ven_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ven_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ven_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ven_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ven_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;
                        Cmd.Parameters.Add("@Ven_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ven_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ven_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;
                        Cmd.Parameters.Add("@Ven_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ven_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ven_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;

                       // Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Cja_Codigo;
                        //Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        //Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo_Item;
                        //Cmd.Parameters.Add("@Cja_estado", System.Data.SqlDbType.Char).Value = Clas_Enti.cja_estado;

                       // Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Libro;
                        //Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Voucher;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                   
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.Parameters.Add("@Adm_Unm_Id", System.Data.SqlDbType.VarChar).Value = ent.Adm_Unm_Id;
                            Cmd.Parameters.Add("@Adm_Tiene_Lote", System.Data.SqlDbType.VarChar).Value = ent.Acepta_lotes;

                            Cmd.Parameters.Add("@Adm_Lote ", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Adm_FechaFabricacion", System.Data.SqlDbType.Date).Value = ent.Lot_FechaFabricacion;
                            Cmd.Parameters.Add("@Adm_FechaVencimiento", System.Data.SqlDbType.Date).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Adm_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = ent.Adm_CantidadAtendidaEnBaseAlCoefciente;

                            Cmd.Parameters.Add("@Ven_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = ent.Adm_Tipo_Operacion;
                            Cmd.Parameters.Add("@Ven_Afecto_IGV_FE", System.Data.SqlDbType.VarChar).Value = ent.Ven_Afecto_IGV_FE;

                            Cmd.ExecuteNonQuery();
                        }

                        //insertando lotes para venta

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleLotes)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_LOTE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;

                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = ent.Lot_FechaFabricacion;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;
                            Cmd.ExecuteNonQuery();
                        }


                        Cmd.Parameters.Clear();
                        Cmd.CommandText = "pa_VEN_MOVIMIENTO_INVENTARIO_INSERTAR_ADM";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Autor", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cmd.ExecuteNonQuery();



                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
   public void Anular_adm_Ventas(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_CAB_ANULAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
            
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Anular_adm_Nota_Credito(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_NC_CAB_ANULAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //BUSQEUDA PARA AFECTAR EN NOTA DE CREDITO

        public List<Entidad_Movimiento_Cab> Listar_Afectar_comprobante_NC_(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_CAB_LISTAR_AFECTAR_NC_SP", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.ctb_pdv_codigo == "" ? null : Cls_Enti.ctb_pdv_codigo);
                        Cmd.Parameters.Add("@Ven_Ruc_dni", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Ctb_Ruc_dni == "" ? null : Cls_Enti.Ctb_Ruc_dni);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    ctb_pdv_codigo = Dr.GetString(3),
                                    ctb_pdv_Nombre = Dr.GetString(4).Trim(),
                                    Id_Movimiento = Dr.GetString(5),
                                    Ctb_Glosa = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(8).Trim(),
                                    Nombre_Comprobante = Dr.GetString(9).Trim(),
                                    Ctb_Serie = Dr.GetString(10).Trim(),
                                    Ctb_Numero = Dr.GetString(11).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(12),
                                    Ctb_Fecha_Vencimiento = Dr.GetDateTime(13),
                                    Ctb_Tipo_Ent = Dr.GetString(14).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(15).Trim(),
                                    Entidad = Dr.GetString(16).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(17).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(18).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(19).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(20).Trim(),
                                    Ctn_Moneda_Sunat = Dr.GetString(21),
                                    Ctn_Moneda_Desc = Dr.GetString(22).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(23).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(24).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(25),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(26),
                                    Ctb_Base_Imponible = Dr.GetDecimal(27),
                                    Ctb_Igv = Dr.GetDecimal(28),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(29),
                                    Ctb_Exonerada = Dr.GetDecimal(30),
                                    Ctb_Inafecta = Dr.GetDecimal(31),
                                    Ctb_Isc = Dr.GetBoolean(32),
                                    Ctb_Isc_Importe = Dr.GetDecimal(33),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(34),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(35),
                                    Ctb_Importe_Total = Dr.GetDecimal(36),
                                    Ctb_Analisis = Dr.GetString(37).Trim(),
                                    Ctb_Analisis_Desc = Dr.GetString(38).Trim(),
                                    Cja_Codigo = Dr.GetString(39),
                                    Cja_Aper_Cierre_Codigo = Dr.GetInt32(40),
                                    Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(41),
                                    cja_estado = Dr.GetString(42),
                                    Estado = Dr.GetString(43),
                                    Id_Libro = Dr.GetString(44),
                                    Id_Libro_Nombre = Dr.GetString(45),
                                    Id_Libro_Sunat = Dr.GetString(46),
                                    Id_Voucher = Dr.GetString(47),
                                    Id_Periodo_Desc = Dr.GetString(48),
                                    Estado_Fila = Dr.GetString(50).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        //NOTA DE CREDITO

        public void Insertar_adm_NC(Entidad_Movimiento_Cab Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_MOVIMIENTO_NC_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.ctb_pdv_codigo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ven_Motivo_Cod", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ven_Motivo_Cod;


                        Cmd.Parameters.Add("@Ven_Glosa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ctb_Glosa;
                        Cmd.Parameters.Add("@Ven_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Doc;
                        Cmd.Parameters.Add("@Ven_Serie", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Serie;
                        Cmd.Parameters.Add("@Ven_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Ctb_Numero;
                        Cmd.Parameters.Add("@Ven_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ven_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = Clas_Enti.Ctb_Fecha_Movimiento;
                        Cmd.Parameters.Add("@Ven_Tipo_Ent", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Ctb_Tipo_Ent;
                        Cmd.Parameters.Add("@Ven_Ruc_dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Ctb_Ruc_dni;
                        Cmd.Parameters.Add("@Ven_Condicion_Cod", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Ctb_Condicion_Cod;
                        Cmd.Parameters.Add("@Ven_Moneda_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctn_Moneda_Cod;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Cod", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Tipo_Cambio_Cod;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Desc", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Ctb_Tipo_Cambio_desc;
                        Cmd.Parameters.Add("@Ven_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tipo_Cambio_Valor;

                        Cmd.Parameters.Add("@Ven_Igv_Tasa", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Tasa_IGV;

                        Cmd.Parameters.Add("@Ven_Base_Imponible", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Base_Imponible;
                        Cmd.Parameters.Add("@Ven_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Igv;
                        Cmd.Parameters.Add("@Ven_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Valor_Fact_Exonerada;
                        Cmd.Parameters.Add("@Ven_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Exonerada;
                        Cmd.Parameters.Add("@Ven_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Inafecta;
                        Cmd.Parameters.Add("@Ven_Isc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Isc;
                        Cmd.Parameters.Add("@Ven_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Isc_Importe;
                        Cmd.Parameters.Add("@Ven_Otros_Tributos", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ctb_Otros_Tributos;
                        Cmd.Parameters.Add("@Ven_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Otros_Tributos_Importe;
                        Cmd.Parameters.Add("@Ven_Importe_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Ctb_Importe_Total;
                        Cmd.Parameters.Add("@Ven_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Ctb_Analisis;

                        Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Cja_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                        Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = Clas_Enti.Cja_Aper_Cierre_Codigo_Item;
                        Cmd.Parameters.Add("@Cja_estado", System.Data.SqlDbType.Char).Value = Clas_Enti.cja_estado;

                        Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Libro;
                        Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Voucher;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                 
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleADM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_NC_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = ent.Adm_Item;
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Adm_Centro_CG;
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Adm_Tipo_BSA;
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Adm_Catalogo;
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.Char, 2).Value = ent.Adm_Almacen;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG)  ;
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Adm_Cantidad;
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Adm_Valor_Unit;
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = ent.Adm_Total;
                            Cmd.Parameters.Add("@Adm_Unm_Id", System.Data.SqlDbType.VarChar).Value = ent.Adm_Unm_Id;
                            Cmd.Parameters.Add("@Adm_Tiene_Lote", System.Data.SqlDbType.VarChar).Value = ent.Acepta_lotes;

                            Cmd.Parameters.Add("@Adm_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                            Cmd.Parameters.Add("@Adm_FechaFabricacion", System.Data.SqlDbType.Date).Value = ent.Lot_FechaFabricacion;
                            Cmd.Parameters.Add("@Adm_FechaVencimiento", System.Data.SqlDbType.Date).Value = ent.Lot_FechaVencimiento;
                            Cmd.Parameters.Add("@Adm_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = ent.Adm_CantidadAtendidaEnBaseAlCoefciente;

                            Cmd.Parameters.Add("@Ven_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = ent.Adm_Tipo_Operacion;
                            Cmd.Parameters.Add("@Ven_Afecto_IGV_FE", System.Data.SqlDbType.VarChar).Value = ent.Ven_Afecto_IGV_FE;

                            Cmd.ExecuteNonQuery();
                        }

                        //insertando lotes para venta

                        foreach (Entidad_Movimiento_Cab ent in Clas_Enti.DetalleComprobantes)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_MOVIMIENTO_NC_DOCS_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                            Cmd.Parameters.Add("@Id_Anio_Ref", System.Data.SqlDbType.Char, 4).Value = ent.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo_Ref", System.Data.SqlDbType.Char, 2).Value = ent.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Pdv_Codigo_Ref", System.Data.SqlDbType.Char, 3).Value = ent.ctb_pdv_codigo;
                            Cmd.Parameters.Add("@Id_Movimiento_Ref", System.Data.SqlDbType.Char, 10).Value = ent.Id_Movimiento;


                            Cmd.ExecuteNonQuery();
                        }


 


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Cab> Listar_NC(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_NC_CAB_LISTAR_SP", Cn) { CommandType = System.Data.CommandType.StoredProcedure ,CommandTimeout=0 })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.ctb_pdv_codigo == "" ? null : Cls_Enti.ctb_pdv_codigo);
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Movimiento == "" ? null : Cls_Enti.Id_Movimiento);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    ctb_pdv_codigo = Dr.GetString(3),
                                    ctb_pdv_Nombre = Dr.GetString(4).Trim(),
                                    Id_Movimiento = Dr.GetString(5),
                                    Ctb_Glosa = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_Doc = Dr.GetString(7).Trim(),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(8).Trim(),
                                    Nombre_Comprobante = Dr.GetString(9).Trim(),
                                    Ctb_Serie = Dr.GetString(10).Trim(),
                                    Ctb_Numero = Dr.GetString(11).Trim(),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(12),
                                    Ctb_Fecha_Vencimiento = Dr.GetDateTime(13),
                                    Ctb_Tipo_Ent = Dr.GetString(14).Trim(),
                                    Ctb_Ruc_dni = Dr.GetString(15).Trim(),
                                    Entidad = Dr.GetString(16).Trim(),
                                    Ctb_Condicion_Cod = Dr.GetString(17).Trim(),
                                    Ctb_Condicion_Interno = Dr.GetString(18).Trim(),
                                    Ctb_Condicion_Desc = Dr.GetString(19).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(20).Trim(),
                                    Ctn_Moneda_Sunat = Dr.GetString(21),
                                    Ctn_Moneda_Desc = Dr.GetString(22).Trim(),
                                    Ctb_Tipo_Cambio_Cod = Dr.GetString(23).Trim(),
                                    Ctb_Tipo_Cambio_desc = Dr.GetString(24).Trim(),
                                    Ctb_Tipo_Cambio_Valor = Dr.GetDecimal(25),
                                    Ctb_Tasa_IGV = Dr.GetDecimal(26),
                                    Ctb_Base_Imponible = Dr.GetDecimal(27),
                                    Ctb_Igv = Dr.GetDecimal(28),
                                    Ctb_Valor_Fact_Exonerada = Dr.GetDecimal(29),
                                    Ctb_Exonerada = Dr.GetDecimal(30),
                                    Ctb_Inafecta = Dr.GetDecimal(31),
                                    Ctb_Isc = Dr.GetBoolean(32),
                                    Ctb_Isc_Importe = Dr.GetDecimal(33),
                                    Ctb_Otros_Tributos = Dr.GetBoolean(34),
                                    Ctb_Otros_Tributos_Importe = Dr.GetDecimal(35),
                                    Ctb_Importe_Total = Dr.GetDecimal(36),
                                    //Ctb_Analisis = Dr.GetString(37).Trim(),
                                    //Ctb_Analisis_Desc = Dr.GetString(38).Trim(),
                                    //Cja_Codigo = Dr.GetString(39),
                                    //Cja_Aper_Cierre_Codigo = Dr.GetInt32(40),
                                    //Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(41),
                                    //cja_estado = Dr.GetString(42),
                                    Estado = Dr.GetString(37),
                                    //Id_Libro = Dr.GetString(44),
                                    //Id_Libro_Nombre = Dr.GetString(45),
                                    //Id_Libro_Sunat = Dr.GetString(46),
                                    //Id_Voucher = Dr.GetString(47),
                                    Id_Periodo_Desc = Dr.GetString(38),
                                    Estado_Fila = Dr.GetString(40).Trim(),
                                    Ven_FactElectronica_Estado = Dr.GetString(41).Trim(),
                                    Ven_FactElectronica_Estado_Desc = Dr.GetString(42).Trim(),
                                    Ven_Motivo_Cod=Dr.GetString(43).Trim(),
                                    Ven_Motivo_Cod_Interno=Dr.GetString(44).Trim(),
                                    Ven_Motivo_Desc=Dr.GetString(45).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Movimiento_Cab> Listar_NC_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_NC_DET_LISTAR_SP", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.ctb_pdv_codigo == "" ? null : Cls_Enti.ctb_pdv_codigo);
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Movimiento == "" ? null : Cls_Enti.Id_Movimiento);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    ctb_pdv_codigo = Dr.GetString(3),
                                    Id_Movimiento = Dr.GetString(4),
                                    Adm_Item = Dr.GetInt32(5),
                                    Adm_Tipo_BSA = Dr.GetString(6).Trim(),
                                    Adm_Tipo_BSA_Interno = Dr.GetString(7).Trim(),
                                    Adm_Tipo_BSA_Desc = Dr.GetString(8).Trim(),
                                    Adm_Catalogo = Dr.GetString(9).Trim(),
                                    Adm_Catalogo_Desc = Dr.GetString(10).Trim(),
                                    Adm_Almacen = Dr.GetString(11).Trim(),
                                    Adm_Almacen_desc = Dr.GetString(12).Trim(),
                                    Adm_Cantidad = Dr.GetDecimal(13),
                                    Adm_Valor_Unit = Dr.GetDecimal(14),
                                    Adm_Total = Dr.GetDecimal(15),
                                    Adm_Unm_Id = Dr.GetString(16).Trim(),
                                    Adm_Unm_Desc = Dr.GetString(17).Trim(),
                                    UNM_CoigodFE = Dr.GetString(18).Trim(),
                                    Acepta_lotes = Dr.GetBoolean(19),
                                    Lot_Lote = Dr.GetString(20),
                                    Lot_FechaFabricacion = Dr.GetDateTime(21),
                                    Lot_FechaVencimiento = Dr.GetDateTime(22),
                                    Adm_CantidadAtendidaEnBaseAlCoefciente = Dr.GetDecimal(23),
                                    Adm_Tipo_Operacion = Dr.GetString(24).Trim(),
                                    Adm_Tipo_Operacion_Interno = Dr.GetString(25).Trim(),
                                    Adm_Tipo_Operacion_Desc = Dr.GetString(26).Trim(),
                                    Ven_Afecto_IGV_FE = Dr.GetString(27).Trim(),
                                    Ven_Afecto_IGV_FE_Desc = Dr.GetString(28).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }




        public List<Entidad_Movimiento_Cab> Listar_Doc_Afecto_NC_(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("VEN_MOVIMIENTO_NC_DOCS_LISTAR_SP", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Pdv_Codigo", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.ctb_pdv_codigo == "" ? null : Cls_Enti.ctb_pdv_codigo);
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Movimiento == "" ? null : Cls_Enti.Id_Movimiento);


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    ctb_pdv_codigo = Dr.GetString(3),
                                    Id_Movimiento = Dr.GetString(4),
                                    Id_Periodo_Desc = Dr.GetString(5),
                                    Ctb_Tipo_Doc = Dr.GetString(6).Trim(),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(7).Trim(),
                                    Nombre_Comprobante = Dr.GetString(8).Trim(),
                                    Ctb_Serie = Dr.GetString(9).Trim(),
                                    Ctb_Numero = Dr.GetString(10).Trim(),
                                    Ctn_Moneda_Cod = Dr.GetString(11).Trim(),
                                    Ctn_Moneda_Sunat = Dr.GetString(12),
                                    Ctn_Moneda_Desc = Dr.GetString(13).Trim(),
                                    Ctb_Importe_Total = Dr.GetDecimal(14)
                                  
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }




        public List<Entidad_Movimiento_Cab> Listar_adm_Productos_Por_Proveedor(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("ps_PRODUCTOS_POR_PROVEEDOR", Cn) { CommandType = System.Data.CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        Cmd.Parameters.Add("@Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                      
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(0),
                                    Ctb_Tipo_Doc =Dr.GetString(1),
                                    Ctb_Tipo_Doc_Sunat = Dr.GetString(2),
                                    Nombre_Comprobante = Dr.GetString(3),
                                    Ctb_Serie = Dr.GetString(4),
                                    Ctb_Numero = Dr.GetString(5),
                                    Ctb_Ruc_dni = Dr.GetString(6),
                                    Entidad = Dr.GetString(7),
                                    Adm_Catalogo = Dr.GetString(8),
                                    Adm_Catalogo_Desc = Dr.GetString(9),
                                    Adm_Cantidad = Dr.GetDecimal(10),
                                    Adm_Valor_Unit = Dr.GetDecimal(11)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Cab> Listar_Pendientes_Por_Cobrar(Entidad_Movimiento_Cab Cls_Enti)
        {
            List<Entidad_Movimiento_Cab> ListaItems = new List<Entidad_Movimiento_Cab>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PENDIENTES_POR_COBRAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Movimiento_Cab
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Ctb_Tipo_Movimiento = Dr.GetString(3),
                                    Adm_Almacen = Dr.GetString(4),
                                    Id_Movimiento = Dr.GetString(5),
                                    ctb_pdv_codigo = Dr.GetString(6),
                                    ctb_pdv_Nombre = Dr.GetString(7),
                                    Ctb_Condicion_Cod = Dr.GetString(8),
                                    Ctb_Condicion_Interno = Dr.GetString(9),
                                    Ctb_Condicion_Desc = Dr.GetString(10),
                                    Ctb_Ruc_dni=  Dr.GetString(11),
                                    Entidad = Dr.GetString(12) ,
                                    Ctb_Tipo_Doc = Dr.GetString(13),
                                    Nombre_Comprobante = Dr.GetString(14),
                                    Ctb_Serie = Dr.GetString(15),
                                    Ctb_Numero = Dr.GetString(16),
                                    Ctb_Fecha_Movimiento = Dr.GetDateTime(17) ,
                                    Es_Venta = Dr.GetBoolean(18),
                                    Ctb_Base_Imponible = Dr.GetDecimal(19),
                                    Ctb_Igv = Dr.GetDecimal(20),
                                    Ctb_Importe_Total = Dr.GetDecimal(21),
                                    Cja_Codigo = Dr.GetString(22),
                                    Cja_Aper_Cierre_Codigo = Dr.GetInt32(23),
                                    Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(24),
                                    cja_estado = Dr.GetString(25),
                                    Id_Libro = Dr.GetString(26),
                                    Id_Voucher = Dr.GetString(27),
                                    Amortizado = Dr.GetDecimal(28)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
