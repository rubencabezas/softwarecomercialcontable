﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Comprobantes
    {

        public void Insertar(Entidad_Comprobantes Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_COMPROBANTE_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Comprobante", System.Data.SqlDbType.Char, 3).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Nombre_Comprobante", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Nombre_Comprobante;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_SUnat;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cmp_Es_Doc_Interno", System.Data.SqlDbType.Bit).Value = Clas_Enti.Es_Documento_Interno; 
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Comprobante = Cmd.Parameters["@Id_Comprobante"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Comprobantes Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_COMPROBANTE_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Comprobante", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Comprobante;
                        Cmd.Parameters.Add("@Nombre_Comprobante", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Nombre_Comprobante;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_SUnat;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cmp_Es_Doc_Interno", System.Data.SqlDbType.Bit).Value = Clas_Enti.Es_Documento_Interno;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Comprobantes Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_COMPROBANTE_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Comprobante", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Comprobante;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Comprobantes> Listar(Entidad_Comprobantes Cls_Enti)
        {
            List<Entidad_Comprobantes> ListaItems = new List<Entidad_Comprobantes>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_COMPROBANTE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Comprobante", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Comprobante;
                        Cmd.Parameters.Add("@Id_Sunat", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_SUnat;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Comprobantes
                                {
                                    Id_Comprobante = Dr.GetString(0),
                                    Nombre_Comprobante = Dr.GetString(1),
                                    Id_SUnat = Dr.GetString(2),
                                    Es_Documento_Interno=Dr.GetBoolean(3)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }



        }
    }
