﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Datos_Tipo_Especificacion
    {

        public void Insertar(Entidad_Tipo_Especificacion Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_ESPECIFICACION_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Esp_Codigo", System.Data.SqlDbType.Char, 5).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Esp_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Esp_Descripcion;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Esp_Codigo = Cmd.Parameters["@Esp_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Tipo_Especificacion Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_ESPECIFICACION_MODIFCAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Esp_Codigo", System.Data.SqlDbType.Char, 5).Value = Clas_Enti.Esp_Codigo;
                        Cmd.Parameters.Add("@Esp_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Esp_Descripcion;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Tipo_Especificacion> Listar(Entidad_Tipo_Especificacion Cls_Enti)
        {
            List<Entidad_Tipo_Especificacion> ListaItems = new List<Entidad_Tipo_Especificacion>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_ESPECIFICACION_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Esp_Codigo", System.Data.SqlDbType.Char, 5).Value = (string.IsNullOrEmpty(Cls_Enti.Esp_Codigo) ? null : Cls_Enti.Esp_Codigo);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Tipo_Especificacion
                                {
                                    Esp_Codigo = Dr.GetString(0),
                                    Esp_Descripcion = Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



    }
}
