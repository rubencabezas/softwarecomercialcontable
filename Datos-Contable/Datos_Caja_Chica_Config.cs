﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
namespace Contable
{
  public  class Datos_Caja_Chica_Config
    {

        public void Insertar(Entidad_Caja_Chica_Config Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CAJA_CHICA_CONFIG_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;


                        Cmd.Parameters.Add("@Id_Caja", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Cja_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Cja_Descripcion;
                        Cmd.Parameters.Add("@Cja_Responsable", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Cja_Responsable;
                        Cmd.Parameters.Add("@Cja_Cuenta", System.Data.SqlDbType.VarChar, 10).Value = Clas_Enti.Cja_Cuenta;
                        Cmd.Parameters.Add("@Cja_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cja_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Cja_Es_Caja_Central", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cja_Es_Caja_Central;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Caja = Cmd.Parameters["@Id_Caja"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
 


        public void Modificar(Entidad_Caja_Chica_Config Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CAJA_CHICA_CONFIG_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;


                        Cmd.Parameters.Add("@Id_Caja", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Caja;
                        Cmd.Parameters.Add("@Cja_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Cja_Descripcion;
                        Cmd.Parameters.Add("@Cja_Responsable", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Cja_Responsable;
                        Cmd.Parameters.Add("@Cja_Cuenta", System.Data.SqlDbType.VarChar, 10).Value = Clas_Enti.Cja_Cuenta;
                        Cmd.Parameters.Add("@Cja_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cja_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Cja_Es_Caja_Central", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cja_Es_Caja_Central;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
            
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Caja_Chica_Config Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CAJA_CHICA_CONFIG_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char,4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Caja", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Caja;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja_Chica_Config> Listar(Entidad_Caja_Chica_Config Clas_Enti)
        {
            List<Entidad_Caja_Chica_Config> ListaItems = new List<Entidad_Caja_Chica_Config>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CAJA_CHICA_CONFIG_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Caja", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Caja;
                        Cmd.Parameters.Add("@Cja_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cja_Es_Caja_Chica;
                        Cmd.Parameters.Add("@Cja_Es_Caja_Central", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cja_Es_Caja_Central;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Caja_Chica_Config
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Caja = Dr.GetString(2),
                                    Cja_Descripcion=Dr.GetString(3),
                                    Cja_Responsable=Dr.GetString(4),
                                    Cja_Responsable_Desc=Dr.GetString(5),
                                    Cja_Cuenta=Dr.GetString(6),
                                    Cja_Cuenta_Desc=Dr.GetString(7),
                                    Cja_Es_Caja_Chica=Dr.GetBoolean(8),
                                    Cja_Es_Caja_Central=Dr.GetBoolean(9),
                                    Cja_Estado_Caja=Dr.GetString(10),
                                    Cja_Estado_Caja_det=Dr.GetString(11),
                                    Cja_Estado_Caja_Desc=Dr.GetString(12)
,                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
