﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
    public class Datos_Grupo
    {
        public void Insertar(Entidad_Grupo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_GRUPO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Gru_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Gru_Descripcion;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Grupo = Cmd.Parameters["@Id_Grupo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Grupo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_GRUPO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Gru_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Gru_Descripcion;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Grupo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_GRUPO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Grupo> Listar(Entidad_Grupo Cls_Enti)
        {
            List<Entidad_Grupo> ListaItems = new List<Entidad_Grupo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_GRUPO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Grupo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Grupo = Dr.GetString(1),
                                    Id_Tipo = Dr.GetString(2),
                                    Gru_Descripcion = Dr.GetString(3),
                                    Id_TipoCod = Dr.GetString(4),
                                    Id_Tipo_Descripcion = Dr.GetString(5)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
