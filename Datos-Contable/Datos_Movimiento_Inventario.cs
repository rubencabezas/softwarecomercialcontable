﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Movimiento_Inventario
    {

 
            public void Insertar(Entidad_Movimiento_Inventario Clas_Enti)
            {
                SqlTransaction Trs = null;
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_CAB_INSERTAR", Cn))
                        {
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Tipo_Operacion;
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Glosa;

                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.Char, 1).Value = (string.IsNullOrEmpty(Clas_Enti.Inv_Tipo_Entidad) ? "" : Clas_Enti.Inv_Tipo_Entidad);// Clas_Enti.Inv_Tipo_Entidad;
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar, 15).Value = (string.IsNullOrEmpty(Clas_Enti.Inv_Ruc_Dni) ? "" : Clas_Enti.Inv_Ruc_Dni); //Clas_Enti.Inv_Ruc_Dni;
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Inv_Tipo_Doc;
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Inv_Serie;
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Inv_Numero;
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Inv_Fecha;
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Inv_Almacen_O_D;
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.Char, 15).Value = Clas_Enti.Inv_Responsable_Ruc_Dni;
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar, 3).Value = (string.IsNullOrEmpty(Clas_Enti.Inv_Libro) ? "" : Clas_Enti.Inv_Libro);

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                          Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = Clas_Enti.Inv_Es_Traspaso;


                        Cn.Open();
                            Trs = Cn.BeginTransaction();
                            Cmd.Transaction = Trs;
                            Cmd.ExecuteNonQuery();

                            Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();



                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.Detalle)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_DET_INSERTAR";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                                Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = ent.Invd_TipoBSA;
                                Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Invd_Catalogo;
                                Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Invd_Cantidad;
                                Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Invd_Valor_Unit;
                                Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = ent.Invd_Total;
                                Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Invd_Centro_CG;
                                Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.Char, 3).Value = ent.Invd_Unm;
                                Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = ent.Acepta_lotes;
                                Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = ent.Invd_CantidadAtendidaEnBaseAlCoefciente;

                                Cmd.ExecuteNonQuery();
                            }

                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.DetalleLotes)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_LOTE_INSERTAR";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;
                                Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                                Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                                Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                                Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaFabricacion;
                                Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaVencimiento;
                                Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;
                                Cmd.ExecuteNonQuery();
                            }


                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.DetalleDoc_Referencia)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Doc_Item", System.Data.SqlDbType.Int).Value = ent.Doc_Item;
                                Cmd.Parameters.Add("@Doc_Proceso", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Proceso;
                                Cmd.Parameters.Add("@Doc_Anio", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Anio;
                                Cmd.Parameters.Add("@Doc_Periodo", System.Data.SqlDbType.Char, 2).Value = ent.Doc_Periodo;
                                Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Doc_Folio;


                                Cmd.ExecuteNonQuery();
                            }

                            // ACTUALIZAMOS EL TRASPASO QEU SE ESTA DANDO INGRESO
                            // SE CAMBIA EL Es_Atendido_Traspaso = 1
                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.DetalleDoc_Referencia)
                            {
                                if (Clas_Enti.Id_Tipo_Mov == "0034" && Clas_Enti.Id_Tipo_Operacion == "09")
                                {
                                    Cmd.Parameters.Clear();
                                    Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_TRASPASO_ATENDIDO";
                                    Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                    Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Anio;
                                    Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = ent.Doc_Periodo;
                                    Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = "0035";
                                    Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                    Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = ent.Doc_Folio;
                                    Cmd.ExecuteNonQuery();
                                }
                            }


                            //CUANTO ES UN TRASPASO PARA QUE AUTOMATICAMENTE HAGA EL INGRESO
                            if (Clas_Enti.Id_Tipo_Mov== "0035" && Clas_Enti.Id_Tipo_Operacion == "09")
                            {

                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_HACER_INGRESO_DE_TRANSFERENCIA_AUTOMATICO";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio; ;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.ExecuteNonQuery();


                        }



                            Trs.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public void Insertar_Ventas_INV(Entidad_Movimiento_Inventario Clas_Enti)
            {
                SqlTransaction Trs = null;
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_CAB_INSERTAR_VENTAS", Cn))
                        {
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Tipo_Operacion;
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Glosa;

                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.Char, 1).Value = Clas_Enti.Inv_Tipo_Entidad;
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar, 11).Value = Clas_Enti.Inv_Ruc_Dni;
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Inv_Tipo_Doc;
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Inv_Serie;
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Inv_Numero;
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Inv_Fecha;
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Inv_Almacen_O_D;
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Inv_Responsable_Ruc_Dni;
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Inv_Libro;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                            Cmd.Parameters.Add("@Inv_Base", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Base;
                            Cmd.Parameters.Add("@Inv_Igv", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Igv;
                            Cmd.Parameters.Add("@Inv_Total", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Total;
                            Cmd.Parameters.Add("@Inv_pdc_codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Inv_pdc_codigo;
                            Cmd.Parameters.Add("@Inv_pdv_codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Inv_pdv_codigo;
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Codigo;
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Aper_Cierre_Codigo;
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cja_Aper_Cierre_Codigo_Item;
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = Clas_Enti.cja_estado;
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = Clas_Enti.Inv_Es_Venta_Sin_Ticket;

                            Cmd.Parameters.Add("@Inv_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Valor_Fact_Exonerada;
                            Cmd.Parameters.Add("@Inv_Exonerada", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Exonerada;
                            Cmd.Parameters.Add("@Inv_Inafecta", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Inafecta;
                            Cmd.Parameters.Add("@Inv_Isc_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Isc_Importe;
                            Cmd.Parameters.Add("@Inv_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Inv_Otros_Tributos_Importe;
                            Cmd.Parameters.Add("@Inv_Es_Venta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Inv_Es_Venta;
                            Cmd.Parameters.Add("@Inv_Condicion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Inv_Condicion;
                            Cmd.Parameters.Add("@Inv_Est_Facturacion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Est_Facturacion;


                            Cn.Open();
                            Trs = Cn.BeginTransaction();
                            Cmd.Transaction = Trs;
                            Cmd.ExecuteNonQuery();

                            Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();



                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.Detalle)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_DET_INSERTAR";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                                Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = ent.Invd_TipoBSA;
                                Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Invd_Catalogo;
                                Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Invd_Cantidad;
                                Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Invd_Valor_Unit;
                                Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = ent.Invd_Total;
                                Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Invd_Centro_CG;
                                Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.Char, 3).Value = ent.Invd_Unm;
                                Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = ent.Acepta_lotes;

                                Cmd.Parameters.Add("@Invd_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                                Cmd.Parameters.Add("@Invd_FechaFabricacion", System.Data.SqlDbType.Date).Value = ent.Lot_FechaFabricacion;
                                Cmd.Parameters.Add("@Invd_FechaVencimiento", System.Data.SqlDbType.Date).Value = ent.Lot_FechaVencimiento;
                                Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = ent.Invd_CantidadAtendidaEnBaseAlCoefciente;


                            Cmd.ExecuteNonQuery();
                            }


                            // Insertando lotes si tuviera
                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.DetalleLotes)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_LOTE_INSERTAR";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;
                                Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                                Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                                Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                                Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaFabricacion;
                                Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaVencimiento;
                                Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;
                                Cmd.ExecuteNonQuery();
                            }



                            Trs.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public void Modificar(Entidad_Movimiento_Inventario Clas_Enti)
            {
                SqlTransaction Trs = null;
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_CAB_MODIFICAR", Cn))
                        {
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Tipo_Operacion;
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Glosa;

                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.Char, 1).Value = Clas_Enti.Inv_Tipo_Entidad;
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar, 15).Value = Clas_Enti.Inv_Ruc_Dni;
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Inv_Tipo_Doc;
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Inv_Serie;
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Inv_Numero;
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Inv_Fecha;
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Inv_Almacen_O_D;
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.Char, 15).Value = Clas_Enti.Inv_Responsable_Ruc_Dni;
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Inv_Libro;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                           Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = Clas_Enti.Inv_Es_Traspaso;


                        Cn.Open();
                            Trs = Cn.BeginTransaction();
                            Cmd.Transaction = Trs;
                            Cmd.ExecuteNonQuery();



                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.Detalle)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_DET_INSERTAR";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                                Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = ent.Invd_TipoBSA;
                                Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Invd_Catalogo;
                                Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Invd_Cantidad;
                                Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = ent.Invd_Valor_Unit;
                                Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = ent.Invd_Total;
                                Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.Char, 3).Value = ent.Invd_Centro_CG;
                                Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.Char, 3).Value = ent.Invd_Unm;
                                Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = ent.Acepta_lotes;
                                Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = ent.Invd_CantidadAtendidaEnBaseAlCoefciente;

                            Cmd.ExecuteNonQuery();
                            }

                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.DetalleLotes)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_LOTE_INSERTAR";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Lot_Adm_item;
                                Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = ent.Lot_item;
                                Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Lot_Catalogo;
                                Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = ent.Lot_Lote;
                                Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaFabricacion;
                                Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.DateTime).Value = ent.Lot_FechaVencimiento;
                                Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Lot_Cantidad;
                                Cmd.ExecuteNonQuery();
                            }

                            foreach (Entidad_Movimiento_Inventario ent in Clas_Enti.DetalleDoc_Referencia)
                            {
                                Cmd.Parameters.Clear();
                                Cmd.CommandText = "pa_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA";
                                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                                Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                                Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                                Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                                Cmd.Parameters.Add("@Doc_Item", System.Data.SqlDbType.Int).Value = ent.Doc_Item;
                                Cmd.Parameters.Add("@Doc_Proceso", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Proceso;
                                Cmd.Parameters.Add("@Doc_Anio", System.Data.SqlDbType.Char, 4).Value = ent.Doc_Anio;
                                Cmd.Parameters.Add("@Doc_Periodo", System.Data.SqlDbType.Char, 2).Value = ent.Doc_Periodo;
                                Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.Char, 10).Value = ent.Doc_Folio;


                                Cmd.ExecuteNonQuery();
                            }


                        //CUANTO ES UN TRASPASO PARA QUE AUTOMATICAMENTE HAGA EL INGRESO
                        if (Clas_Enti.Id_Tipo_Mov == "0035" && Clas_Enti.Id_Tipo_Operacion == "09")
                        {

                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_HACER_INGRESO_DE_TRANSFERENCIA_AUTOMATICO";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio; ;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.ExecuteNonQuery();


                        }



                        Trs.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            public void Eliminar_Adm_Ingresos_Inv(Entidad_Movimiento_Inventario Clas_Enti)
            {
                SqlTransaction Trs = null;
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_INVENTARIO_CAB_ADM_ELIMINAR", Cn))
                        {
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = Clas_Enti.Inv_Es_Traspaso;

                        Cn.Open();
                            Trs = Cn.BeginTransaction();
                            Cmd.Transaction = Trs;
                            Cmd.ExecuteNonQuery();

                            Trs.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public void Eliminar_Adm_Ingresos_Inv_Libera_Trabsaferencia(Entidad_Movimiento_Inventario Clas_Enti)
            {
                SqlTransaction Trs = null;
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_INVENTARIO_CAB_ADM_ELIMINAR", Cn))
                        {
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                            Cn.Open();
                            Trs = Cn.BeginTransaction();
                            Cmd.Transaction = Trs;
                            Cmd.ExecuteNonQuery();


                            Cmd.Parameters.Clear();//INSERTAR LA COMPRA A INVENTARIO COMO ENTRADA
                            Cmd.CommandText = "pa_CTB_REVERTIR_ESTADO_TRANSFERENCIA";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.ExecuteNonQuery();

                            Trs.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public void Anular_Adm_Ingresos_Inv(Entidad_Movimiento_Inventario Clas_Enti)
            {
                SqlTransaction Trs = null;
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_INVENTARIO_CAB_ADM_ANULAR", Cn))
                        {
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                            Cn.Open();
                            Trs = Cn.BeginTransaction();
                            Cmd.Transaction = Trs;
                            Cmd.ExecuteNonQuery();

                            Trs.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            public List<Entidad_Movimiento_Inventario> Listar(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = Cls_Enti.Inv_Es_Venta_Sin_Ticket;
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Tipo_Operacion;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                                while (Dr.Read())
                            {
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Id_Anio = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Id_Tipo_Mov = Dr.GetString(3),
                                        Id_Tipo_Mov_Det = Dr.GetString(4),
                                        Id_Tipo_Mov_Desc = Dr.GetString(5),
                                        Id_Almacen = Dr.GetString(6).Trim(),
                                        Id_Almacen_Desc = Dr.GetString(7).Trim(),
                                        Id_Movimiento = Dr.GetString(8).Trim(),
                                        Id_Tipo_Operacion = Dr.GetString(9).Trim(),
                                        Id_Tipo_Operacion_Desc = Dr.GetString(10).Trim(),
                                        Inv_Tipo_Entidad = Dr.GetString(11),
                                        Inv_Tipo_Entidad_Desc = Dr.GetString(12),
                                        Inv_Ruc_Dni = Dr.GetString(13),
                                        Inv_Ruc_Dni_Desc = Dr.GetString(14),
                                        Inv_Tipo_Doc = Dr.GetString(15),
                                        Inv_Tipo_Doc_Desc = Dr.GetString(16).Trim(),
                                        Inv_Serie = Dr.GetString(17).Trim(),
                                        Inv_Numero = Dr.GetString(18).Trim(),
                                        Inv_Fecha = Dr.GetDateTime(19),
                                        Inv_Almacen_O_D = Dr.GetString(20).Trim(),
                                        Inv_Almacen_O_D_Desc = Dr.GetString(21).Trim(),
                                        Inv_Responsable_Ruc_Dni = Dr.GetString(22).Trim(),
                                        Inv_Responsable_Ruc_Dni_Desc = Dr.GetString(23).Trim(),
                                        Inv_Libro = Dr.GetString(24),
                                        Inv_Voucher = Dr.GetString(25),
                                        Glosa = Dr.GetString(26).Trim(),
                                        Inv_Es_Venta_Sin_Ticket = Dr.GetBoolean(27),
                                        Inv_Base = Dr.GetDecimal(28),
                                        Inv_Igv = Dr.GetDecimal(29),
                                        Inv_Total = Dr.GetDecimal(30),
                                        Cja_Codigo = Dr.GetString(31),
                                        Cja_Aper_Cierre_Codigo = Dr.GetInt32(32),
                                        Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(33),
                                        cja_estado = Dr.GetString(34),
                                        Inv_pdv_Nombre = Dr.GetString(35).Trim(),
                                        Estado_Fila = Dr.GetString(36),
                                        Inv_pdv_codigo = Dr.GetString(37),
                                        Es_Atendido_Traspaso = Dr.GetBoolean(38),
                                        Inv_Generado_En_Compra = Dr.GetBoolean(39),
                                        Periodo_Desc = Dr.GetString(40),
                                        Inv_Es_Traspaso = Dr.GetBoolean(41)

                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }



            public List<Entidad_Movimiento_Inventario> Listar_Ventas_Sin_Ticket(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_CAB_LISTAR_VENTAS_SIN_TICKET", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                            //Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = Cls_Enti.Inv_Es_Venta_Sin_Ticket;
                            //Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.Char,2).Value = Cls_Enti.Id_Tipo_Operacion;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Id_Anio = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Id_Tipo_Mov = Dr.GetString(3),
                                        Id_Tipo_Mov_Det = Dr.GetString(4),
                                        Id_Tipo_Mov_Desc = Dr.GetString(5),
                                        Id_Almacen = Dr.GetString(6).Trim(),
                                        Id_Almacen_Desc = Dr.GetString(7).Trim(),
                                        Id_Movimiento = Dr.GetString(8).Trim(),
                                        Id_Tipo_Operacion = Dr.GetString(9).Trim(),
                                        Id_Tipo_Operacion_Desc = Dr.GetString(10).Trim(),
                                        Inv_Tipo_Entidad = Dr.GetString(11),
                                        Inv_Tipo_Entidad_Desc = Dr.GetString(12),
                                        Inv_Ruc_Dni = Dr.GetString(13),
                                        Inv_Ruc_Dni_Desc = Dr.GetString(14),
                                        Inv_Tipo_Doc = Dr.GetString(15),
                                        Inv_Tipo_Doc_Desc = Dr.GetString(16).Trim(),
                                        Inv_Serie = Dr.GetString(17).Trim(),
                                        Inv_Numero = Dr.GetString(18).Trim(),
                                        Inv_Fecha = Dr.GetDateTime(19),
                                        Inv_Almacen_O_D = Dr.GetString(20).Trim(),
                                        Inv_Almacen_O_D_Desc = Dr.GetString(21).Trim(),
                                        Inv_Responsable_Ruc_Dni = Dr.GetString(22).Trim(),
                                        Inv_Responsable_Ruc_Dni_Desc = Dr.GetString(23).Trim(),
                                        Inv_Libro = Dr.GetString(24),
                                        Inv_Voucher = Dr.GetString(25),
                                        Glosa = Dr.GetString(26).Trim(),
                                        Inv_Es_Venta_Sin_Ticket = Dr.GetBoolean(27),
                                        Inv_Base = Dr.GetDecimal(28),
                                        Inv_Igv = Dr.GetDecimal(29),
                                        Inv_Total = Dr.GetDecimal(30),
                                        Cja_Codigo = Dr.GetString(31),
                                        Cja_Aper_Cierre_Codigo = Dr.GetInt32(32),
                                        Cja_Aper_Cierre_Codigo_Item = Dr.GetInt32(33),
                                        cja_estado = Dr.GetString(34),
                                        Inv_pdv_Nombre = Dr.GetString(35).Trim(),
                                        Estado_Fila = Dr.GetString(36),
                                        Inv_pdv_codigo = Dr.GetString(37),
                                        Es_Atendido_Traspaso = Dr.GetBoolean(38),
                                        Inv_Generado_En_Compra = Dr.GetBoolean(39),
                                        Periodo_Desc = Dr.GetString(40),
                                        Inv_Condicion = Dr.GetString(41),
                                        Inv_Condicion_Interno = Dr.GetString(42),
                                        Inv_Condicion_Desc = Dr.GetString(43),
                                        Est_Facturacion = Dr.GetString(44),
                                        Est_Facturacion_Interno = Dr.GetString(45),
                                        Est_Facturacion_Desc = Dr.GetString(46),
                                        Usuario = Dr.GetString(47),
                                        FechaOperacionVendedor = Dr.GetDateTime(48)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }

            public List<Entidad_Movimiento_Inventario> Listar_Det(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Id_Anio = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Id_Tipo_Mov = Dr.GetString(3),
                                        Id_Almacen = Dr.GetString(4),
                                        Id_Movimiento = Dr.GetString(5),
                                        Id_Item = Dr.GetInt32(6),
                                        Invd_TipoBSA = Dr.GetString(7),
                                        Invd_TipoBSA_Det = Dr.GetString(8),
                                        Invd_TipoBSA_Descipcion = Dr.GetString(9),
                                        Invd_Catalogo = Dr.GetString(10),
                                        Invd_Catalogo_Desc = Dr.GetString(11),
                                        Invd_Cantidad = Dr.GetDecimal(12),
                                        Invd_Valor_Unit = Dr.GetDecimal(13),
                                        Invd_Total = Dr.GetDecimal(14),
                                        Invd_Centro_CG = Dr.GetString(15),
                                        Invd_Centro_CG_Desc = Dr.GetString(16),
                                        Invd_Unm = Dr.GetString(17),
                                        Invd_Unm_Desc = Dr.GetString(18),
                                        Acepta_lotes = Dr.GetBoolean(19)

                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public List<Entidad_Movimiento_Inventario> Listar_Lotes_Inventario(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("CTB_MOVIMIENTO_INVENTARIO_LOTE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {

                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Id_Anio = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Id_Tipo_Mov = Dr.GetString(3),
                                        Id_Almacen = Dr.GetString(4),
                                        Id_Movimiento = Dr.GetString(5),
                                        Lot_Adm_item = Dr.GetInt32(6),
                                        Lot_item = Dr.GetInt32(7),
                                        Lot_Catalogo = Dr.GetString(8),
                                        Lot_Lote = Dr.GetString(9),
                                        Lot_FechaFabricacion = Dr.GetDateTime(10),
                                        Lot_FechaVencimiento = Dr.GetDateTime(11),
                                        Lot_Cantidad = Dr.GetDecimal(12),
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public List<Entidad_Movimiento_Inventario> Buscar_Doc_Dar_Ingreso(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_INVENTARIO_PARA_DAR_ENTRADA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Proceso_Cod = Dr.GetString(0),
                                        Proceso_Desc = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Periodo_Desc = Dr.GetString(3),
                                        Id_Movimiento = Dr.GetString(4),
                                        Tipo_Doc_Sunat = Dr.GetString(5),
                                        Inv_Serie = Dr.GetString(6),
                                        Inv_Numero = Dr.GetString(7),
                                        Inv_Fecha = Dr.GetDateTime(8),
                                        Id_Almacen = Dr.GetString(9),
                                        Id_Anio = Dr.GetString(10),
                                        Id_Tipo_Mov = Dr.GetString(11)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }



            public List<Entidad_Movimiento_Inventario> Buscar_Doc_Dar_Ingreso_DET(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_INVENTARIO_PARA_DAR_ENTRADA_DET", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Folio", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Invd_TipoBSA = Dr.GetString(0),
                                        Invd_TipoBSA_Det = Dr.GetString(1),
                                        Invd_TipoBSA_Descipcion = Dr.GetString(2),
                                        Invd_Catalogo = Dr.GetString(3),
                                        Invd_Catalogo_Desc = Dr.GetString(4),
                                        Invd_Cantidad = Dr.GetDecimal(5),
                                        Invd_Valor_Unit = Dr.GetDecimal(6),
                                        Invd_Total = Dr.GetDecimal(7),
                                        Invd_Centro_CG = Dr.GetString(8),
                                        Invd_Centro_CG_Desc = Dr.GetString(9)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public List<Entidad_Movimiento_Inventario> Buscar_Doc_Referenciados(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_BUSCAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Id_Tipo_Mov = Dr.GetString(1),
                                        Id_Almacen = Dr.GetString(2),
                                        Doc_Item = Dr.GetInt32(3),
                                        Doc_Proceso = Dr.GetString(4),
                                        Doc_Anio = Dr.GetString(5),
                                        Doc_Periodo = Dr.GetString(6),
                                        Doc_Folio = Dr.GetString(7)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            //buscamos los detalles para las provisiones

            public List<Entidad_Movimiento_Inventario> Buscar_Detalles_Orden(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_INVENTARIO_PARA_DAR_ENTRADA_DET_ORDEN", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Proceso", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Doc_Proceso;
                            Cmd.Parameters.Add("@Folio", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Doc_Folio;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Invd_TipoBSA = Dr.GetString(0),
                                        Invd_TipoBSA_Det = Dr.GetString(1),
                                        Invd_TipoBSA_Descipcion = Dr.GetString(2),
                                        Invd_Catalogo = Dr.GetString(3),
                                        Invd_Catalogo_Desc = Dr.GetString(4),
                                        Invd_Cantidad = Dr.GetDecimal(7),
                                        Invd_Valor_Unit = Dr.GetDecimal(8)

                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public List<Entidad_Movimiento_Inventario> Buscar_Detalles_Inventario(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_INVENTARIO_PARA_DAR_ENTRADA_DET_INVENTARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Doc_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Doc_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Proceso", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Doc_Proceso;
                            Cmd.Parameters.Add("@Folio", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Doc_Folio;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Invd_TipoBSA = Dr.GetString(0),
                                        Invd_TipoBSA_Det = Dr.GetString(1),
                                        Invd_TipoBSA_Descipcion = Dr.GetString(2),
                                        Invd_Catalogo = Dr.GetString(3),
                                        Invd_Catalogo_Desc = Dr.GetString(4),
                                        Invd_Cantidad = Dr.GetDecimal(5),
                                        Invd_Valor_Unit = Dr.GetDecimal(6),
                                        Invd_Total = Dr.GetDecimal(7),
                                        Invd_Centro_CG = Dr.GetString(8),
                                        Invd_Centro_CG_Desc = Dr.GetString(9),
                                        Acepta_lotes = Dr.GetBoolean(10)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public List<Entidad_Movimiento_Inventario> Buscar_Detalles_Inventario_Lotes(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_INVENTARIO_PARA_DAR_ENTRADA_DET_INVENTARIO_LOTES", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Doc_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Doc_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Proceso", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Doc_Proceso;
                            Cmd.Parameters.Add("@Folio", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Doc_Folio;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Id_Anio = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Id_Tipo_Mov = Dr.GetString(3),
                                        Id_Almacen = Dr.GetString(4),
                                        Id_Movimiento = Dr.GetString(5),
                                        Lot_Adm_item = Dr.GetInt32(6),
                                        Lot_item = Dr.GetInt32(7),
                                        Lot_Catalogo = Dr.GetString(8),
                                        Lot_Lote = Dr.GetString(9),
                                        Lot_FechaFabricacion = Dr.GetDateTime(10),
                                        Lot_FechaVencimiento = Dr.GetDateTime(11),
                                        Lot_Cantidad = Dr.GetDecimal(12)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public List<Entidad_Movimiento_Inventario> Buscar_Documentos_Referenciados(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_BUSCAR_INVENTARIO_DOCUMENTOS_REFERENCIADOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Folio", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Doc_Folio;
                            Cmd.Parameters.Add("@Proceso", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Proceso_Cod;


                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Proceso_Cod = Dr.GetString(0),
                                        Proceso_Desc = Dr.GetString(1),
                                        Id_Periodo = Dr.GetString(2),
                                        Periodo_Desc = Dr.GetString(3),
                                        Id_Movimiento = Dr.GetString(4),
                                        Tipo_Doc_Sunat = Dr.GetString(5),
                                        Inv_Serie = Dr.GetString(6),
                                        Inv_Numero = Dr.GetString(7),
                                        Inv_Fecha = Dr.GetDateTime(8),
                                        Id_Almacen = Dr.GetString(9),
                                        Id_Anio = Dr.GetString(10)
                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }






            public List<Entidad_Movimiento_Inventario> Reporte_Kardex(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("paRpt_INV_MovimientoAlmacen", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            // Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                            // Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo_Mov;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (string.IsNullOrEmpty(Cls_Enti.Invd_Catalogo) ? null : Cls_Enti.Invd_Catalogo);
                            Cmd.Parameters.Add("@Fecha_inicio", System.Data.SqlDbType.DateTime).Value = Cls_Enti.Fecha_inicio;
                            Cmd.Parameters.Add("@Fecha_Fin", System.Data.SqlDbType.DateTime).Value = Cls_Enti.Fecha_Fin;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Empresa = Dr.GetString(0),
                                        Empresa_desc = Dr.GetString(1),
                                        Empresa_ruc = Dr.GetString(2),
                                        Id_Anio = Dr.GetString(3),
                                        Id_Periodo = Dr.GetString(4),
                                        Periodo_Desc = Dr.GetString(5),
                                        Id_Movimiento = Dr.GetString(6),
                                        Id_Almacen = Dr.GetString(7),
                                        Id_Almacen_Desc = Dr.GetString(8),
                                        Id_Tipo_Mov = Dr.GetString(9),
                                        Id_Tipo_Mov_Desc = Dr.GetString(10),
                                        Invd_Catalogo = Dr.GetString(11),
                                        Invd_Catalogo_Desc = Dr.GetString(12),
                                        Invd_Unm = Dr.GetString(13),
                                        Invd_Unm_sunat = Dr.GetString(14),
                                        Invd_Unm_Desc = Dr.GetString(15),
                                        Inv_Tipo_Doc = Dr.GetString(16),
                                        Tipo_Doc_Sunat = Dr.GetString(17),
                                        Inv_Tipo_Doc_Desc = Dr.GetString(18),
                                        Inv_Serie = Dr.GetString(19),
                                        Inv_Numero = Dr.GetString(20),
                                        Inv_Fecha = Dr.GetDateTime(21),
                                        CantidadEntrada = Dr.GetDecimal(22),
                                        CostoUnitarioEntrada = Dr.GetDecimal(23),
                                        CostoTotalEntrada = Dr.GetDecimal(24),
                                        CantidadSalida = Dr.GetDecimal(25),
                                        CostoUnitarioSalida = Dr.GetDecimal(26),
                                        CostoTotalSalida = Dr.GetDecimal(27),
                                        Id_Tipo_Operacion_Desc = Dr.GetString(28),
                                        Invd_CantidadAtendidaEnBaseAlCoefciente = Dr.GetDecimal(29)

                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }




            public List<Entidad_Movimiento_Inventario> Reporte_Kardex_Resumen(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("paRpt_INV_MovimientoAlmacenReseumen", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (string.IsNullOrEmpty(Cls_Enti.Invd_Catalogo) ? null : Cls_Enti.Invd_Catalogo);
                            Cmd.Parameters.Add("@Fecha_inicio", System.Data.SqlDbType.DateTime).Value = Cls_Enti.Fecha_inicio;
                            Cmd.Parameters.Add("@Fecha_Fin", System.Data.SqlDbType.DateTime).Value = Cls_Enti.Fecha_Fin;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Invd_Catalogo = Dr.GetString(0),
                                        Invd_Catalogo_Desc = Dr.GetString(1),
                                        Invd_Serie = Dr.GetString(2),
                                        Invd_Unm = Dr.GetString(3),
                                        Invd_Unm_Desc = Dr.GetString(4),
                                        Stock = Dr.GetDecimal(5)

                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


            public List<Entidad_Movimiento_Inventario> Reporte_Kardex_Lote_Resumen(Entidad_Movimiento_Inventario Cls_Enti)
            {
                List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
                try
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                    {
                        using (SqlCommand Cmd = new SqlCommand("pa_REPORTE_RESUMEN_STOCK_PRODUCTO_LOTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (string.IsNullOrEmpty(Cls_Enti.Invd_Catalogo) ? null : Cls_Enti.Invd_Catalogo);
                            //Cmd.Parameters.Add("@Fecha_inicio", System.Data.SqlDbType.DateTime).Value = Cls_Enti.Fecha_inicio;
                            //Cmd.Parameters.Add("@Fecha_Fin", System.Data.SqlDbType.DateTime).Value = Cls_Enti.Fecha_Fin;

                            Cn.Open();
                            using (SqlDataReader Dr = Cmd.ExecuteReader())
                            {
                                while (Dr.Read())
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Almacen = Dr.GetString(0),
                                        Lot_Catalogo = Dr.GetString(1),
                                        Lot_Catalogo_Desc = Dr.GetString(2),
                                        Lot_Lote = Dr.GetString(3),
                                        Lot_FechaFabricacion = Dr.GetDateTime(4),
                                        Lot_FechaVencimiento = Dr.GetDateTime(5),
                                        Lot_Cantidad = Dr.GetDecimal(6)

                                    });
                                }
                            }
                        }
                    }
                    return ListaItems;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }



        public List<Entidad_Movimiento_Inventario> Traer_Saldo_Inicial(Entidad_Movimiento_Inventario Cls_Enti)
        {
            List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_SALDO_INICIAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                            while (Dr.Read())
                            {
                                {
                                    ListaItems.Add(new Entidad_Movimiento_Inventario
                                    {
                                        Id_Item = Dr.GetInt32(0),
                                        Invd_TipoBSA = Dr.GetString(1),
                                        Invd_TipoBSA_Det = Dr.GetString(2),
                                        Invd_TipoBSA_Descipcion = Dr.GetString(3),
                                        Invd_Catalogo = Dr.GetString(4),
                                        Invd_Catalogo_Desc = Dr.GetString(5),
                                        Invd_Unm = Dr.GetString(6),
                                        Invd_Unm_Desc = Dr.GetString(7),
                                        Invd_Cantidad = Dr.GetDecimal(8),
                                        Invd_Valor_Unit = Dr.GetDecimal(9),
                                        Acepta_lotes = Dr.GetBoolean(10)

                                    });
                                }
                            }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Inventario> Traer_Saldo_Inicial_Lotes(Entidad_Movimiento_Inventario Cls_Enti)
        {
            List<Entidad_Movimiento_Inventario> ListaItems = new List<Entidad_Movimiento_Inventario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_SALDO_INICIAL_LOTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = "2021";// Cls_Enti.Id_Anio;
                       Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Invd_Catalogo;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {

                                ListaItems.Add(new Entidad_Movimiento_Inventario
                                {
                                    Lot_item = Dr.GetInt32(0),
                                    Lot_Catalogo = Dr.GetString(1),
                                    Lot_Lote = Dr.GetString(2),
                                    Lot_FechaFabricacion = Dr.GetDateTime(3),
                                    Lot_FechaVencimiento = Dr.GetDateTime(4),
                                    Lot_Cantidad = Dr.GetDecimal(5)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
 