﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Datos_Formato_Balance_Tributario
    {

        public List<Entidad_Formato_Balance_Tributario> Listar(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            List<Entidad_Formato_Balance_Tributario> ListaItems = new List<Entidad_Formato_Balance_Tributario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LISTAR_CTB_FORMATO_BALANCE_TRIBUTARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = IIf(Cls_Enti.ID == 0, null, Cls_Enti.ID);
                        Cmd.Parameters.Add("@codigo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.codigo;
                        //Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Formato_Balance_Tributario
                                {
                                    ID=Dr.GetInt32(0),
                                    codigo=Dr.GetString(1),
                                    descripcion = Dr.GetString(2),
                                    formula = Dr.GetString(3),
                                    tipo = Dr.GetString(4)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }


        public List<Entidad_Formato_Balance_Tributario> Reporte_Balance_Tributario(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            List<Entidad_Formato_Balance_Tributario> ListaItems = new List<Entidad_Formato_Balance_Tributario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_Reporte_Balance_Tributario", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Formato_Balance_Tributario
                                {
                                    tipo = Dr.GetString(0),
                                    codigo = Dr.GetString(1),
                                    descripcion = Dr.GetString(2),
                                    monto = Dr.GetDecimal(3)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public void Insertar(Entidad_Formato_Balance_Tributario Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_FORMATO_BALANCE_TRIBUTARIO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.codigo;
                        Cmd.Parameters.Add("@descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.descripcion;
                        Cmd.Parameters.Add("@formula", System.Data.SqlDbType.VarChar).Value = Clas_Enti.formula;
                        Cmd.Parameters.Add("@tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.tipo;
 

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
 
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Formato_Balance_Tributario Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_FORMATO_BALANCE_TRIBUTARIO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@ID", System.Data.SqlDbType.VarChar).Value = Clas_Enti.ID;
                        Cmd.Parameters.Add("@codigo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.codigo;
                        Cmd.Parameters.Add("@descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.descripcion;
                        Cmd.Parameters.Add("@formula", System.Data.SqlDbType.VarChar).Value = Clas_Enti.formula;
                        Cmd.Parameters.Add("@tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.tipo; 
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Entidad_Formato_Balance_Tributario Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_FORMATO_BALANCE_TRIBUTARIO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@ID", System.Data.SqlDbType.Int).Value = Clas_Enti.ID;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
