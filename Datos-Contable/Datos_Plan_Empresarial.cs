﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
  public  class Datos_Plan_Empresarial
    {
        public void Insertar(Entidad_Plan_Empresarial Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PLAN_EMPRESARIAL_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Id_Cuenta;
                         Cmd.Parameters.Add("@Id_ELemento", System.Data.SqlDbType.Char, 1).Value = Clas_Enti.Id_Elemento;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Estructura;
                        Cmd.Parameters.Add("@Cta_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Cta_Descripcion;

                        Cmd.Parameters.Add("@Cta_Afecto_Doc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Afecto_Doc;
                        Cmd.Parameters.Add("@Cta_Area", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Area;
                        Cmd.Parameters.Add("@Cta_Centro_Costo", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Centro_Costo;
                        Cmd.Parameters.Add("@Cta_Centro_Gasto", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Centro_Gasto;
                        Cmd.Parameters.Add("@Cta_Funcion", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Funcion;
                        Cmd.Parameters.Add("@Cta_Naturaleza", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Naturaleza;
                        Cmd.Parameters.Add("@Cta_Inventario", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Inventario;
                        
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cta_Proceso_Tes", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Tag_Proceso;
                       Cmd.Parameters.Add("@Cta_Obligacion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Tag_Obligacion;
                        Cmd.Parameters.Add("@Cta_Afecto_Regimen", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Afecto_Regimen_tag;

                        Cmd.Parameters.Add("@Cta_Cod_Bal_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Rec_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Rec_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Nif_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Nif_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Rec_Nif_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Rec_Nif_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Res_Nif_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Res_Nif_Tributario;
                        //Cta_Cod_Bal_Tributario ,
                        //Cta_Cod_Bal_Rec_Tributario  ,
                        //Cta_Cod_Bal_Nif_Tributario  ,
                        //Cta_Cod_Bal_Rec_Nif_Tributario  ,
                        //Cta_Cod_Bal_Res_Nif_Tributario

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Plan_Empresarial ent in Clas_Enti.DetalleCta_Destino)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_PLAN_EMPRESARIAL_DESTINO_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Cuenta;
                            Cmd.Parameters.Add("@Id_Cuenta_Destino", System.Data.SqlDbType.VarChar, 10).Value = ent.Id_Cuenta_Destino;

                            Cmd.Parameters.Add("@Cta_Tipo", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Cta_Porcentaje_Debe", System.Data.SqlDbType.Decimal).Value = ent.Porcen_Debe;
                            Cmd.Parameters.Add("@Cta_Porcentaje_Haber", System.Data.SqlDbType.Decimal).Value = ent.Porcen_Haber;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.ExecuteNonQuery();
                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Modificar(Entidad_Plan_Empresarial Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PLAN_EMPRESARIAL_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Id_Cuenta;
                        Cmd.Parameters.Add("@Id_ELemento", System.Data.SqlDbType.Char, 1).Value = Clas_Enti.Id_Elemento;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Estructura;
                        Cmd.Parameters.Add("@Cta_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Cta_Descripcion;

                        Cmd.Parameters.Add("@Cta_Afecto_Doc", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Afecto_Doc;
                        Cmd.Parameters.Add("@Cta_Area", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Area;
                        Cmd.Parameters.Add("@Cta_Centro_Costo", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Centro_Costo;
                        Cmd.Parameters.Add("@Cta_Centro_Gasto", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Centro_Gasto;
                        Cmd.Parameters.Add("@Cta_Funcion", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Funcion;
                        Cmd.Parameters.Add("@Cta_Naturaleza", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Naturaleza;
                        Cmd.Parameters.Add("@Cta_Inventario", System.Data.SqlDbType.Bit).Value = Clas_Enti.Cta_Inventario;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cta_Proceso_Tes", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Tag_Proceso;
                        Cmd.Parameters.Add("@Cta_Obligacion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Tag_Obligacion;
                       
                        Cmd.Parameters.Add("@Cta_Afecto_Regimen", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Afecto_Regimen_tag;

                        Cmd.Parameters.Add("@Cta_Cod_Bal_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Rec_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Rec_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Nif_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Nif_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Rec_Nif_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Rec_Nif_Tributario;
                        Cmd.Parameters.Add("@Cta_Cod_Bal_Res_Nif_Tributario", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cta_Cod_Bal_Res_Nif_Tributario;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Plan_Empresarial ent in Clas_Enti.DetalleCta_Destino)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_PLAN_EMPRESARIAL_DESTINO_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Cuenta;
                            Cmd.Parameters.Add("@Id_Cuenta_Destino", System.Data.SqlDbType.VarChar, 10).Value = ent.Id_Cuenta_Destino;

                            Cmd.Parameters.Add("@Cta_Tipo", System.Data.SqlDbType.Char, 4).Value = ent.Ctb_Tipo_DH;
                            Cmd.Parameters.Add("@Cta_Porcentaje_Debe", System.Data.SqlDbType.Decimal).Value = ent.Porcen_Debe;
                            Cmd.Parameters.Add("@Cta_Porcentaje_Haber", System.Data.SqlDbType.Decimal).Value = ent.Porcen_Haber;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.ExecuteNonQuery();
                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Entidad_Plan_Empresarial Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PLAN_EMPRESARIAL_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Id_Cuenta;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public Entidad_Plan_Empresarial TraerDatos(Entidad_Plan_Empresarial EntCta)
        {
            Entidad_Plan_Empresarial Ent = new Entidad_Plan_Empresarial();
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("pa_GNL_PLAN_CONTABLE_BUSAR_ELE_EST_EMPRESARIAL", cn))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = EntCta.Id_Cuenta;
                        cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Char, 1).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Ele_Descripcion", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Est_Descripcion", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Est_Num_Digitos", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Cta_Descripcion", System.Data.SqlDbType.VarChar, 200).Direction = System.Data.ParameterDirection.Output;
                        cn.Open();
                        Trs = cn.BeginTransaction();
                        cmd.Transaction = Trs;
                        cmd.ExecuteNonQuery();
                        Ent.Id_Elemento = cmd.Parameters["@Id_Elemento"].Value.ToString().Trim();
                        Ent.Ele_Descripcion = cmd.Parameters["@Ele_Descripcion"].Value.ToString().Trim();
                        Ent.Id_Estructura = cmd.Parameters["@Id_Estructura"].Value.ToString().Trim();
                        Ent.Est_Descripcion = cmd.Parameters["@Est_Descripcion"].Value.ToString().Trim();
                        Ent.Est_Num_Digitos = cmd.Parameters["@Est_Num_Digitos"].Value.ToString().Trim();
                        Ent.Cta_Descripcion = cmd.Parameters["@Cta_Descripcion"].Value.ToString().Trim();

                        Trs.Commit();
                    }
                }
                return Ent;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Plan_Empresarial> Busqueda(Entidad_Plan_Empresarial Cls_Enti)
        {
            List<Entidad_Plan_Empresarial> ListaItems = new List<Entidad_Plan_Empresarial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PLAN_EMPRESARIAL_BUSQUEDA_GENERAL", Cn) { CommandType = System.Data.CommandType.StoredProcedure ,CommandTimeout=0})
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = (Cls_Enti.Id_Cuenta == null ? null : Cls_Enti.Id_Cuenta);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Plan_Empresarial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Cuenta = Dr.GetString(2),
                                    Cta_Descripcion = Dr.GetString(3),
                                    Cta_Afecto_Doc = Dr.GetBoolean(4),
                                    Cta_Area = Dr.GetBoolean(5),
                                    Cta_Centro_Costo=Dr.GetBoolean(6),
                                    Cta_Centro_Gasto=Dr.GetBoolean(7),
                                    Cta_Funcion=Dr.GetBoolean(8),
                                    Cta_Naturaleza=Dr.GetBoolean(9),
                                    Cta_Inventario=Dr.GetBoolean(10),
                                    Id_Elemento=Dr.GetString(11),
                                    Ele_Descripcion=Dr.GetString(12),
                                    Id_Estructura=Dr.GetString(13),
                                    Est_Descripcion=Dr.GetString(14),
                                    Tag_Proceso=Dr.GetString(15).Trim(),
                                    Cod_Proceso=Dr.GetString(16).Trim(),
                                    Proceso_Desc=Dr.GetString(17).Trim(),
                                    Tag_Obligacion=Dr.GetString(18).Trim(),
                                    Cod_Obligacion=Dr.GetString(19).Trim(),
                                    Obligacion_Desc=Dr.GetString(20).Trim(),
                                    Cta_Afecto_Regimen_tag = Dr.GetString(21).Trim(),
                                    Cta_Afecto_Regimen = Dr.GetString(22).Trim(),
                                    Cta_Afecto_Regimen_Desc = Dr.GetString(23).Trim(),
                                    Cta_Cod_Bal_Tributario = Dr.GetString(24).Trim(),
                                    Cta_Cod_Bal_Tributario_desc = Dr.GetString(25).Trim(),
                                    Cta_Cod_Bal_Rec_Tributario = Dr.GetString(26).Trim(),
                                    Cta_Cod_Bal_Rec_Tributario_desc = Dr.GetString(27).Trim(),
                                    Cta_Cod_Bal_Nif_Tributario = Dr.GetString(28).Trim(),
                                    Cta_Cod_Bal_Nif_Tributario_desc = Dr.GetString(29).Trim(),
                                    Cta_Cod_Bal_Rec_Nif_Tributario = Dr.GetString(30).Trim(),
                                    Cta_Cod_Bal_Rec_Nif_Tributario_desc = Dr.GetString(31).Trim(),
                                    Cta_Cod_Bal_Res_Nif_Tributario = Dr.GetString(32).Trim(),
                                    Cta_Cod_Bal_Res_Nif_Tributario_desc = Dr.GetString(33).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Plan_Empresarial> Busqueda_Cta_Por_Proceso(Entidad_Plan_Empresarial Cls_Enti)
        {
            List<Entidad_Plan_Empresarial> ListaItems = new List<Entidad_Plan_Empresarial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PLAN_EMPRESARIAL_BUSQUEDA_POR_PROCESO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = (Cls_Enti.Id_Cuenta == null ? null : Cls_Enti.Id_Cuenta);
                        Cmd.Parameters.Add("@Cta_Proceso", System.Data.SqlDbType.VarChar,4).Value = (Cls_Enti.Tag_Proceso == null ? null : Cls_Enti.Tag_Proceso);

                        Cn.Open();

                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Plan_Empresarial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Cuenta = Dr.GetString(2).Trim(),
                                    Cta_Descripcion = Dr.GetString(3).Trim(),
                                    Tag_Proceso = Dr.GetString(4).Trim(),
                                    Cod_Proceso = Dr.GetString(5).Trim(),
                                    Proceso_Desc = Dr.GetString(6).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Plan_Empresarial> Listar_Destino(Entidad_Plan_Empresarial Cls_Enti)
        {
            List<Entidad_Plan_Empresarial> ListaItems = new List<Entidad_Plan_Empresarial>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_PLAN_EMPRESARIAL_DESTINO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = (Cls_Enti.Id_Cuenta == null ? null : Cls_Enti.Id_Cuenta);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Plan_Empresarial
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Cuenta = Dr.GetString(2),
                                    Id_Cuenta_Destino = Dr.GetString(3),
                                    Cta_Descripcion_Destino = Dr.GetString(4),
                                    Ctb_Tipo_DH=Dr.GetString(5),
                                    CCtb_Tipo_DH_Interno=Dr.GetString(6),
                                    Ctb_Tipo_DH_Desc=Dr.GetString(7),
                                    Porcen_Debe=Dr.GetDecimal(8),
                                    Porcen_Haber=Dr.GetDecimal(9)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
