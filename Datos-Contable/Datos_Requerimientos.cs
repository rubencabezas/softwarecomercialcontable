﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
public    class Datos_Requerimientos
    {
        public void Insertar(Entidad_Requerimiento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_REQUERIMIENTO_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Req_Solicitante_Ruc_dni", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Req_Solicitante_Ruc_dni;
                        Cmd.Parameters.Add("@Rec_Id_Area", System.Data.SqlDbType.Char,2).Value = Clas_Enti.Rec_Id_Area;
                        Cmd.Parameters.Add("@Req_Id_Cargo", System.Data.SqlDbType.Char,2).Value = Clas_Enti.Req_Id_Cargo;

                        Cmd.Parameters.Add("@Req_Encardado_Ruc_Dni", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Req_Encardado_Ruc_Dni;
                        Cmd.Parameters.Add("@Req_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Req_Descripcion;
                        Cmd.Parameters.Add("@Req_Tipo_Doc", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Req_Tipo_Doc;
                        Cmd.Parameters.Add("@Req_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Req_Serie;
                        Cmd.Parameters.Add("@Req_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Req_Numero;
                        Cmd.Parameters.Add("@Req_Fecha_Envio", System.Data.SqlDbType.Date).Value = Clas_Enti.Req_Fecha_Envio;
                        Cmd.Parameters.Add("@Req_Hora_Envio", System.Data.SqlDbType.Time).Value = Clas_Enti.Req_Hora_Envio;
                        Cmd.Parameters.Add("@Req_O_C", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Req_O_C;
                        Cmd.Parameters.Add("@Req_Codigo_Proyecto", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Req_Codigo_Proyecto;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();



                        foreach (Entidad_Requerimiento ent in Clas_Enti.DetalleS)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_LOG_REQUERIMIENTO_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                          
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Req_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Req_Tipo_BSA;
                            Cmd.Parameters.Add("@Req_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Req_Catalogo;
                            Cmd.Parameters.Add("@Req_Detalle", System.Data.SqlDbType.VarChar, 300).Value = ent.Req_Detalle;
                            Cmd.Parameters.Add("@Req_Observacion", System.Data.SqlDbType.VarChar, 300).Value = ent.Req_Observacion;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Req_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Req_Cantidad;
                         
                            Cmd.ExecuteNonQuery();
                        }


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }


        public void Modificar(Entidad_Requerimiento Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_REQUERIMIENTO_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;
                        Cmd.Parameters.Add("@Req_Solicitante_Ruc_dni", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Req_Solicitante_Ruc_dni;
                        Cmd.Parameters.Add("@Rec_Id_Area", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Rec_Id_Area;
                        Cmd.Parameters.Add("@Req_Id_Cargo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Req_Id_Cargo;

                        Cmd.Parameters.Add("@Req_Encardado_Ruc_Dni", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Req_Encardado_Ruc_Dni;
                        Cmd.Parameters.Add("@Req_Descripcion", System.Data.SqlDbType.VarChar, 300).Value = Clas_Enti.Req_Descripcion;
                        Cmd.Parameters.Add("@Req_Tipo_Doc", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Req_Tipo_Doc;
                        Cmd.Parameters.Add("@Req_Serie", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Req_Serie;
                        Cmd.Parameters.Add("@Req_Numero", System.Data.SqlDbType.VarChar, 8).Value = Clas_Enti.Req_Numero;
                        Cmd.Parameters.Add("@Req_Fecha_Envio", System.Data.SqlDbType.Date).Value = Clas_Enti.Req_Fecha_Envio;
                        Cmd.Parameters.Add("@Req_Hora_Envio", System.Data.SqlDbType.Time).Value = Clas_Enti.Req_Hora_Envio;
                        Cmd.Parameters.Add("@Req_O_C", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Req_O_C;
                        Cmd.Parameters.Add("@Req_Codigo_Proyecto", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Req_Codigo_Proyecto;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;


                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Clas_Enti.Id_Movimiento = Cmd.Parameters["@Id_Movimiento"].Value.ToString();



                        foreach (Entidad_Requerimiento ent in Clas_Enti.DetalleS)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_LOG_REQUERIMIENTO_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Periodo;
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Movimiento;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Req_Tipo_BSA", System.Data.SqlDbType.Char, 4).Value = ent.Req_Tipo_BSA;
                            Cmd.Parameters.Add("@Req_Catalogo", System.Data.SqlDbType.Char, 10).Value = ent.Req_Catalogo;
                            Cmd.Parameters.Add("@Req_Detalle", System.Data.SqlDbType.VarChar, 300).Value = ent.Req_Detalle;
                            Cmd.Parameters.Add("@Req_Observacion", System.Data.SqlDbType.VarChar, 300).Value = ent.Req_Observacion;//(string.IsNullOrEmpty(ent.Ctb_Centro_CG) ? null : ent.Ctb_Centro_CG);
                            Cmd.Parameters.Add("@Req_Cantidad", System.Data.SqlDbType.Decimal).Value = ent.Req_Cantidad;

                            Cmd.ExecuteNonQuery();
                        }


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Entidad_Requerimiento> Listar(Entidad_Requerimiento Cls_Enti)
        {
            List<Entidad_Requerimiento> ListaItems = new List<Entidad_Requerimiento>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_REQUERIMIENTO_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;
                   
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Requerimiento
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Movimiento = Dr.GetString(3),
                                    Req_Solicitante_Ruc_dni = Dr.GetString(4),
                                    Req_Solicitante_Ruc_dni_desc = Dr.GetString(5),
                                    Rec_Id_Area = Dr.GetString(6),
                                    Rec_Id_Area_desc = Dr.GetString(7),
                                    Req_Id_Cargo = Dr.GetString(8),
                                    Req_Id_Cargo_desc = Dr.GetString(9),
                                    Req_Encardado_Ruc_Dni = Dr.GetString(10),
                                    Req_Encardado_Ruc_Dni_desc = Dr.GetString(11),
                                    Req_Descripcion = Dr.GetString(12),
                                    Req_Tipo_Doc = Dr.GetString(13),
                                    Req_Tipo_Doc_desc = Dr.GetString(14),
                                    Req_Serie = Dr.GetString(15),
                                    Req_Numero = Dr.GetString(16),
                                    Req_Fecha_Envio = Dr.GetDateTime(17),
                                    Req_Hora_Envio = Dr.GetTimeSpan(18),
                                    Req_O_C = Dr.GetString(19),
                                    Req_Codigo_Proyecto = Dr.GetString(20)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Requerimiento> Listar_Det(Entidad_Requerimiento Cls_Enti)
        {
            List<Entidad_Requerimiento> ListaItems = new List<Entidad_Requerimiento>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_LOG_REQUERIMIENTO_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Movimiento;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Requerimiento
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo = Dr.GetString(2),
                                    Id_Movimiento = Dr.GetString(3),
                                    Id_Item = Dr.GetInt32(4),
                                    Req_Tipo_BSA = Dr.GetString(5),
                                    Req_Tipo_BSA_Det = Dr.GetString(6),
                                    Req_Tipo_BSA_Descripcion = Dr.GetString(7),
                                    Req_Catalogo = Dr.GetString(8),
                                    Req_Catalogo_Desc = Dr.GetString(9),
                                    Req_Detalle = Dr.GetString(10),
                                    Req_Observacion = Dr.GetString(11),
                                    Req_Cantidad = Dr.GetDecimal(12)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
