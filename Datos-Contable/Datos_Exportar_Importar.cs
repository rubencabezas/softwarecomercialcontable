﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Contable
{
 public   class Datos_Exportar_Importar
    {
        // EXPORTAR CATALOGO
        public DataSet  EXPORTAR_MARCA_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MARCA_XML", Cn) 
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_FABRICANTE_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_LOG_FABRICANTE_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_UNIDAD_MEDIDA_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_UNIDAD_MEDIDA_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CATALOGO_GRUPO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_CATALOGO_GRUPO_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CATALOGO_FAMILIA_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_CATALOGO_FAMILIA_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CATALOGO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_CATALOGO_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CATALOGO_UNM_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_CATALOGO_UNM_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

     public DataSet EXPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        //IMPORTAR CATALOGO

        public void IMPORTAR_MARCA_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MARCA_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Marca", System.Data.SqlDbType.Char).Value = filas["Id_Marca"];
                            Cmd.Parameters.Add("@Mar_Descripcion", System.Data.SqlDbType.VarChar ).Value = filas["Mar_Descripcion"];
                            Cmd.Parameters.Add("@Mar_Estado", System.Data.SqlDbType.VarChar ).Value = filas["Mar_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar ).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.ExecuteNonQuery();
                        }
                       Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void IMPORTAR_FABRICANTE_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_LOG_FABRICANTE_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Fabricante", System.Data.SqlDbType.Char).Value = filas["Id_Fabricante"];
                            Cmd.Parameters.Add("@Fab_Descripcion", System.Data.SqlDbType.VarChar).Value = filas["Fab_Descripcion"];
                            Cmd.Parameters.Add("@Fab_Estado", System.Data.SqlDbType.VarChar).Value = filas["Fab_Estado"];
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void IMPORTAR_UNIDAD_MEDIDA_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_UNIDAD_MEDIDA_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char).Value = filas["Id_Unidad_Medida"];
                            Cmd.Parameters.Add("@Und_Descripcion", System.Data.SqlDbType.VarChar).Value = filas["Und_Descripcion"];
                            Cmd.Parameters.Add("@Und_Abreviado", System.Data.SqlDbType.VarChar).Value = filas["Und_Abreviado"];
                            Cmd.Parameters.Add("@Und_SunaT", System.Data.SqlDbType.Char).Value = filas["Und_SunaT"];
                            Cmd.Parameters.Add("@Und_Estado", System.Data.SqlDbType.VarChar).Value = filas["Und_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Und_Codigo_FE", System.Data.SqlDbType.VarChar).Value = filas["Und_Codigo_FE"];
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void IMPORTAR_CATALOGO_GRUPO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_GRUPO_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.VarChar).Value = filas["Id_Grupo"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Gru_Descripcion", System.Data.SqlDbType.Char).Value = filas["Gru_Descripcion"];
                            Cmd.Parameters.Add("@Gru_Estado", System.Data.SqlDbType.VarChar).Value = filas["Gru_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void IMPORTAR_CATALOGO_FAMILIA_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_FAMILIA_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.VarChar).Value = filas["Id_Grupo"];
                            Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.VarChar).Value = filas["Id_Familia"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Fam_Descripcion", System.Data.SqlDbType.VarChar).Value = filas["Fam_Descripcion"];
                            Cmd.Parameters.Add("@Fam_Estado", System.Data.SqlDbType.VarChar).Value = filas["Fam_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void IMPORTAR_CATALOGO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.VarChar).Value = filas["Id_Grupo"];
                            Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.VarChar).Value = filas["Id_Familia"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Id_Catalogo"];
                            Cmd.Parameters.Add("@Cat_Descripcion", System.Data.SqlDbType.VarChar).Value = filas["Cat_Descripcion"];
                            Cmd.Parameters.Add("@Cat_Serie", System.Data.SqlDbType.VarChar).Value = filas["Cat_Serie"];
                            Cmd.Parameters.Add("@Cat_Barra", System.Data.SqlDbType.VarChar).Value = filas["Cat_Barra"];
                            Cmd.Parameters.Add("@Id_Marca", System.Data.SqlDbType.VarChar).Value = filas["Id_Marca"];
                            Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.VarChar).Value = filas["Id_Unidad_Medida"];
                            Cmd.Parameters.Add("@Id_Tipo_Existencia", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo_Existencia"];
                            Cmd.Parameters.Add("@Cat_Estado", System.Data.SqlDbType.Char).Value = filas["Cat_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Cat_Unidades_Presentacion", System.Data.SqlDbType.Decimal).Value = filas["Cat_Unidades_Presentacion"];
                            Cmd.Parameters.Add("@Cat_Comentarios", System.Data.SqlDbType.NVarChar).Value = filas["Cat_Comentarios"];
                            Cmd.Parameters.Add("@Cat_Fabricante", System.Data.SqlDbType.VarChar).Value = filas["Cat_Fabricante"];
                            Cmd.Parameters.Add("@Acepta_Lote", System.Data.SqlDbType.Bit).Value = filas["Acepta_Lote"];
                            Cmd.Parameters.Add("@Facturar_Sin_Existencia", System.Data.SqlDbType.Bit).Value = filas["Facturar_Sin_Existencia"];
 
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void IMPORTAR_CATALOGO_UNM_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_UNM_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Id_Catalogo"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.VarChar).Value = filas["Id_Unidad_Medida"];
                            Cmd.Parameters.Add("@Unm_Coef_Base", System.Data.SqlDbType.Decimal).Value = filas["Unm_Coef_Base"];
                            Cmd.Parameters.Add("@Unm_Defecto", System.Data.SqlDbType.Bit).Value = filas["Unm_Defecto"];
                            Cmd.Parameters.Add("@Unm_Coef_Base_Unidad_Medida", System.Data.SqlDbType.VarChar).Value = filas["Unm_Coef_Base_Unidad_Medida"];
                            Cmd.Parameters.Add("@Presentacion_Descripcion", System.Data.SqlDbType.VarChar).Value = filas["Presentacion_Descripcion"];
 
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

    object ddd;
        public void IMPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(DataTable DT)
        {
           

            SqlTransaction Trs = null;
            try
            {
  
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                  
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();


                              ddd = filas["Id_Catalogo"];

                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.VarChar).Value = filas["Id_Grupo"];
                            Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.VarChar).Value = filas["Id_Familia"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Id_Catalogo"];
                            Cmd.Parameters.Add("@Id_Afecto", System.Data.SqlDbType.VarChar).Value = filas["Id_Afecto"];
             
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                object asss = ddd;
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        //EXPORTAR COMPRAS HECHAS POR LA VENTANA COMPRAS

        public DataSet EXPORTAR_GNL_ENTIDAD_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_GNL_ENTIDAD", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }
        public DataSet EXPORTAR_GNL_ENTIDAD_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_GNL_ENTIDAD_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }
        public DataSet EXPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }
        public DataSet EXPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }



        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }


        public void IMPORTAR_GNL_ENTIDAD_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_GNL_ENTIDAD", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char).Value = filas["Ent_RUC_DNI"];
                            Cmd.Parameters.Add("@Ent_Tipo_Persona", System.Data.SqlDbType.VarChar).Value = filas["Ent_Tipo_Persona"];
                            Cmd.Parameters.Add("@Ent_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Ent_Tipo_Doc"];
                            Cmd.Parameters.Add("@Ent_Razon_Social_Nombre", System.Data.SqlDbType.VarChar).Value = filas["Ent_Razon_Social_Nombre"];
                            Cmd.Parameters.Add("@Ent_Ape_Paterno", System.Data.SqlDbType.VarChar).Value = filas["Ent_Ape_Paterno"];
                            Cmd.Parameters.Add("@Ent_Ape_Materno", System.Data.SqlDbType.VarChar).Value = filas["Ent_Ape_Materno"];
                            Cmd.Parameters.Add("@Ent_Telefono", System.Data.SqlDbType.VarChar).Value = filas["Ent_Telefono"];
                            Cmd.Parameters.Add("@Ent_Repre_Legal", System.Data.SqlDbType.VarChar).Value = filas["Ent_Repre_Legal"];
                            Cmd.Parameters.Add("@Ent_Telefono_movil", System.Data.SqlDbType.VarChar).Value = filas["Ent_Telefono_movil"];
                            Cmd.Parameters.Add("@Ent_Domicilio_Fiscal", System.Data.SqlDbType.Char).Value = filas["Ent_Domicilio_Fiscal"];
                            Cmd.Parameters.Add("@Ent_Correo", System.Data.SqlDbType.VarChar).Value = filas["Ent_Correo"];
                            Cmd.Parameters.Add("@Ent_Pagina_Web", System.Data.SqlDbType.VarChar).Value = filas["Ent_Pagina_Web"];
                            Cmd.Parameters.Add("@Ent_Estado", System.Data.SqlDbType.VarChar).Value = filas["Ent_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.VarChar).Value = filas["Id_Libro"];
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Id_Voucher"];
                            Cmd.Parameters.Add("@Ctb_Glosa", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Glosa"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Doc"];
                            Cmd.Parameters.Add("@Ctb_Serie", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Serie"];
                            Cmd.Parameters.Add("@Ctb_Numero", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Numero"];
                            Cmd.Parameters.Add("@Ctb_Fecha_Movimiento", System.Data.SqlDbType.Date).Value = filas["Ctb_Fecha_Movimiento"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Ent"];
                            Cmd.Parameters.Add("@Ctb_Ruc_dni", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Ruc_dni"];
                            Cmd.Parameters.Add("@Ctb_Condicion_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Condicion_Cod"];
                            Cmd.Parameters.Add("@Ctn_Moneda_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctn_Moneda_Cod"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Cambio_Cod"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Cambio_Desc"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Tipo_Cambio_Valor"];
                            Cmd.Parameters.Add("@Ctb_Base_Imponible", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Base_Imponible"];
                            Cmd.Parameters.Add("@Ctb_Igv", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Igv"];
                            Cmd.Parameters.Add("@Ctb_Base_Imponible2", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Base_Imponible2"];
                            Cmd.Parameters.Add("@Ctb_Igv2", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Igv2"];
                            Cmd.Parameters.Add("@Ctb_Base_Imponible3", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Base_Imponible3"];
                            Cmd.Parameters.Add("@Ctb_Igv3", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Igv3"];
                            Cmd.Parameters.Add("@Ctb_Importe_Total", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Total"];
                            Cmd.Parameters.Add("@Ctb_Isc", System.Data.SqlDbType.Bit).Value = filas["Ctb_Isc"];
                            Cmd.Parameters.Add("@Ctb_Isc_Importe", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Isc_Importe"];
                            Cmd.Parameters.Add("@Ctb_Otros_Tributos", System.Data.SqlDbType.Bit).Value = filas["Ctb_Otros_Tributos"];
                            Cmd.Parameters.Add("@Ctb_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Otros_Tributos_Importe"];
                            Cmd.Parameters.Add("@Ctb_No_Gravadas", System.Data.SqlDbType.Decimal).Value = filas["Ctb_No_Gravadas"];
                            Cmd.Parameters.Add("@Ctb_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Valor_Fact_Exonerada"];
                            Cmd.Parameters.Add("@Ctb_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Exonerada"];
                            Cmd.Parameters.Add("@Ctb_Inafecta", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Inafecta"];
                            Cmd.Parameters.Add("@Ctb_Estado", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = IIf(filas["Fecha_Modi"] == null,Convert.ToDateTime("01/01/1900"), filas["Fecha_Modi"]); //filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Ctb_Afecto_RE", System.Data.SqlDbType.Bit).Value = filas["Ctb_Afecto_RE"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Tipo", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Afecto_Tipo"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Afecto_Tipo_Doc"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Serie", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Afecto_Serie"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Numero", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Afecto_Numero"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Porcentaje", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Afecto_Porcentaje"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Monto", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Afecto_Monto"];
                            Cmd.Parameters.Add("@Ctb_Tasa_IGV", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Tasa_IGV"];
                            Cmd.Parameters.Add("@Ctb_Analisis", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Analisis"];
                            Cmd.Parameters.Add("@Ctb_Afecto_Fecha", System.Data.SqlDbType.Date).Value = IIf(filas["Ctb_Afecto_Fecha"] == null, Convert.ToDateTime("01/01/1900"), filas["Ctb_Afecto_Fecha"]); //filas["Ctb_Afecto_Fecha"];
                            Cmd.Parameters.Add("@Ctb_Fecha_Vencimiento", System.Data.SqlDbType.Date).Value = IIf(filas["Ctb_Fecha_Vencimiento"] == null, Convert.ToDateTime("01/01/1900"), filas["Ctb_Fecha_Vencimiento"]); // filas["Ctb_Fecha_Vencimiento"];
                            Cmd.Parameters.Add("@Ctb_Tipo_IE", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_IE"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Movimiento"];
                            Cmd.Parameters.Add("@Ctb_Medio_Pago", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Medio_Pago"];
                            Cmd.Parameters.Add("@Ctb_Numero_Transaccion", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Numero_Transaccion"];
                            Cmd.Parameters.Add("@Ctb_Cuenta_Corriente", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Cuenta_Corriente"];
                            Cmd.Parameters.Add("@Ctb_Entidad_Financiera", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Entidad_Financiera"];
                            Cmd.Parameters.Add("@Ctb_Importe_MN", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_MN"];
                            Cmd.Parameters.Add("@Ctb_Importe", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe"];
                            Cmd.Parameters.Add("@Ctb_Importe_ME", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_ME"];
                            Cmd.Parameters.Add("@Ctb_Importe_Amortizado", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Amortizado"];
                            Cmd.Parameters.Add("@Ctb_Codigo_Caja_Chica", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Codigo_Caja_Chica"];
                            Cmd.Parameters.Add("@Ctb_Caja_CHica_Estado", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Caja_CHica_Estado"];
                            Cmd.Parameters.Add("@Ctb_Importe_Rendido", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Rendido"];
                            Cmd.Parameters.Add("@Ctb_Importe_Rendido_Saldo", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Rendido_Saldo"];
                            Cmd.Parameters.Add("@Ctb_Es_Caja_Chica", System.Data.SqlDbType.Bit).Value = filas["Ctb_Es_Caja_Chica"];
                            Cmd.Parameters.Add("@Ctb_Rendicion_Caja_Chica", System.Data.SqlDbType.Bit).Value = filas["Ctb_Rendicion_Caja_Chica"];
                            Cmd.Parameters.Add("@Ctb_Reembolso_Caja_Chica", System.Data.SqlDbType.Bit).Value = filas["Ctb_Reembolso_Caja_Chica"];
                            Cmd.Parameters.Add("@Ctb_Cierre_Caja_Chica", System.Data.SqlDbType.Bit).Value = filas["Ctb_Cierre_Caja_Chica"];
                            Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Anio_Ref"];
                            Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Periodo_Ref"];
                            Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Libro_Ref"];
                            Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Voucher_Ref"];
                            Cmd.Parameters.Add("@Ctb_Es_Dif_Cambio", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Es_Dif_Cambio"];
                            Cmd.Parameters.Add("@Ctb_Importe_Dif_Cambio", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Dif_Cambio"];
                            Cmd.Parameters.Add("@Ctb_Es_Redondeo", System.Data.SqlDbType.Bit).Value = filas["Ctb_Es_Redondeo"];
                            Cmd.Parameters.Add("@Ctb_Importe_Redondeo", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Redondeo"];
                            Cmd.Parameters.Add("@Ctb_Importe_Diferencias", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Diferencias"];
                            Cmd.Parameters.Add("@ctb_pdc_codigo", System.Data.SqlDbType.VarChar).Value = filas["ctb_pdc_codigo"];
                            Cmd.Parameters.Add("@ctb_pdv_codigo", System.Data.SqlDbType.VarChar).Value = filas["ctb_pdv_codigo"];
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Cja_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo_Item"];
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = filas["cja_Estado"];
                            Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efectivo_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Estado_Flujo_Efectivo_Cod"];
                            Cmd.Parameters.Add("@Tes_Cuenta", System.Data.SqlDbType.VarChar).Value = filas["Tes_Cuenta"];
                            Cmd.Parameters.Add("@Tes_DifCam", System.Data.SqlDbType.Bit).Value = filas["Tes_DifCam"];
                            Cmd.Parameters.Add("@Tes_DifCambio", System.Data.SqlDbType.Decimal).Value = filas["Tes_DifCambio"];
                            Cmd.Parameters.Add("@Tes_DifRed", System.Data.SqlDbType.Bit).Value = filas["Tes_DifRed"];
                            Cmd.Parameters.Add("@Tes_DifRedond", System.Data.SqlDbType.Decimal).Value = filas["Tes_DifRedond"];
                            Cmd.Parameters.Add("@Ctb_Es_Compra_Comercial", System.Data.SqlDbType.Bit).Value = filas["Ctb_Es_Compra_Comercial"];
 


                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        Object IIf(bool expression, object truePart, object falsePart)
        { return expression ? truePart : falsePart; }


        public void IMPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char).Value = filas["Id_Libro"];
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Id_Voucher"];
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = filas["Adm_Item"];
                            Cmd.Parameters.Add("@Adm_Centro_CG", System.Data.SqlDbType.VarChar).Value = filas["Adm_Centro_CG"];
                            Cmd.Parameters.Add("@Adm_Tipo_BSA", System.Data.SqlDbType.VarChar).Value = filas["Adm_Tipo_BSA"];
                            Cmd.Parameters.Add("@Adm_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Adm_Catalogo"];
                            Cmd.Parameters.Add("@Adm_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Adm_Almacen"];
                            Cmd.Parameters.Add("@Adm_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Adm_Cantidad"];
                            Cmd.Parameters.Add("@Adm_Valor_Unit", System.Data.SqlDbType.Decimal).Value = filas["Adm_Valor_Unit"];
                            Cmd.Parameters.Add("@Adm_Total", System.Data.SqlDbType.Decimal).Value = filas["Adm_Total"];
                            Cmd.Parameters.Add("@Adm_Id_Unm", System.Data.SqlDbType.VarChar).Value = filas["Adm_Id_Unm"];
                            Cmd.Parameters.Add("@Adm_Tiene_Lote", System.Data.SqlDbType.Bit).Value = filas["Adm_Tiene_Lote"];
                        
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char).Value = filas["Id_Libro"];
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Id_Voucher"];
                            Cmd.Parameters.Add("@Adm_Item", System.Data.SqlDbType.Int).Value = filas["Adm_Item"];
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = filas["Lot_Item"];
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Lot_Catalogo"];
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = filas["Lot_Lote"];
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.DateTime).Value = filas["Lot_FechaFabricacion"];
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.DateTime).Value = filas["Lot_FechaVencimiento"];
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Lot_Cantidad"];
                            Cmd.Parameters.Add("@Lot_Estado", System.Data.SqlDbType.VarChar).Value = filas["Lot_Estado"];
 
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Libro", System.Data.SqlDbType.Char).Value = filas["Id_Libro"];
                            Cmd.Parameters.Add("@Id_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Id_Voucher"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Ctb_Cuenta", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Cuenta"];
                            Cmd.Parameters.Add("@Ctb_Operacion_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Operacion_Cod"];
                            Cmd.Parameters.Add("@Ctb_Igv_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Igv_Cod"];
                            Cmd.Parameters.Add("@Ctb_Centro_CG", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Centro_CG"];
                            Cmd.Parameters.Add("@Ctb_Tipo_DH", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_DH"];
                            Cmd.Parameters.Add("@Ctb_Valor_unit", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Valor_unit"];
                            Cmd.Parameters.Add("@Ctb_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Cantidad"];
                            Cmd.Parameters.Add("@Ctb_Importe_Debe", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Debe"];
                            Cmd.Parameters.Add("@Ctb_Importe_Haber", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Haber"];
                            Cmd.Parameters.Add("@Ctb_Importe_Debe_Extr", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Debe_Extr"];
                            Cmd.Parameters.Add("@Ctb_Importe_Haber_Extr", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Importe_Haber_Extr"];
                            Cmd.Parameters.Add("@Ctb_Tipo_BSA", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_BSA"];
                            Cmd.Parameters.Add("@Ctb_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Catalogo"];
                            Cmd.Parameters.Add("@Ctb_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Almacen"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Doc_det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Doc_det"];
                            Cmd.Parameters.Add("@Ctb_Serie_det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Serie_det"];
                            Cmd.Parameters.Add("@Ctb_Numero_det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Numero_det"];
                            Cmd.Parameters.Add("@Ctb_Fecha_Mov_det", System.Data.SqlDbType.Date).Value = filas["Ctb_Fecha_Mov_det"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Ent_det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Ent_det"];
                            Cmd.Parameters.Add("@Ctb_Ruc_dni_det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Ruc_dni_det"];
                            Cmd.Parameters.Add("@Ctb_moneda_cod_det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_moneda_cod_det"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Cod_Det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Cambio_Cod_Det"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Desc_Det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Tipo_Cambio_Desc_Det"];
                            Cmd.Parameters.Add("@Ctb_Tipo_Cambio_Valor_Det", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Tipo_Cambio_Valor_Det"];
                            Cmd.Parameters.Add("@Ctb_Estado", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Ctb_Cta_Destino", System.Data.SqlDbType.Bit).Value = filas["Ctb_Cta_Destino"];
                            Cmd.Parameters.Add("@Ctb_Anio_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Anio_Ref"];
                            Cmd.Parameters.Add("@Ctb_Periodo_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Periodo_Ref"];
                            Cmd.Parameters.Add("@Ctb_Libro_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Libro_Ref"];
                            Cmd.Parameters.Add("@Ctb_Voucher_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Voucher_Ref"];
                            Cmd.Parameters.Add("@Ctb_Item_Ref", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Item_Ref"];
                            Cmd.Parameters.Add("@Ctb_Amortizado", System.Data.SqlDbType.Decimal).Value = filas["Ctb_Amortizado"];
                            Cmd.Parameters.Add("@Ctb_Es_Cuenta_Principal", System.Data.SqlDbType.Bit).Value = filas["Ctb_Es_Cuenta_Principal"];
                            Cmd.Parameters.Add("@Ctb_Patri_Neto_Cod", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Patri_Neto_Cod"];
                            Cmd.Parameters.Add("@Ctb_Estado_Flujo_Efecctivo_Cod_Det", System.Data.SqlDbType.VarChar).Value = filas["Ctb_Estado_Flujo_Efecctivo_Cod_Det"];
                            Cmd.Parameters.Add("@Cta_Dif", System.Data.SqlDbType.Bit).Value = filas["Cta_Dif"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo_Operacion"];
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar).Value = filas["Inv_Glosa"];
                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Entidad"];
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Doc"];
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.Char).Value = filas["Inv_Serie"];
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar).Value = filas["Inv_Numero"];
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = filas["Inv_Fecha"];
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char).Value = filas["Inv_Almacen_O_D"];
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Responsable_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar).Value = filas["Inv_Libro"];
                            Cmd.Parameters.Add("@Inv_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Inv_Voucher"];
                            Cmd.Parameters.Add("@Inv_Estado", System.Data.SqlDbType.VarChar).Value = filas["Inv_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Inv_Base", System.Data.SqlDbType.Decimal).Value = filas["Inv_Base"];
                            Cmd.Parameters.Add("@Inv_Igv", System.Data.SqlDbType.Decimal).Value = filas["Inv_Igv"];
                            Cmd.Parameters.Add("@Inv_Total", System.Data.SqlDbType.Decimal).Value = filas["Inv_Total"];
                            Cmd.Parameters.Add("@Inv_pdc_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdc_codigo"];
                            Cmd.Parameters.Add("@Inv_pdv_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdv_codigo"];
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Cja_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo_Item"];
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = filas["cja_Estado"];
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Venta_Sin_Ticket"];
                            Cmd.Parameters.Add("@Es_Atendido_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Es_Atendido_Traspaso"];
                            Cmd.Parameters.Add("@Inv_Generado_En_Compra", System.Data.SqlDbType.Bit).Value = filas["Inv_Generado_En_Compra"];
                            Cmd.Parameters.Add("@Inv_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Valor_Fact_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Inafecta", System.Data.SqlDbType.Decimal).Value = filas["Inv_Inafecta"];
                            Cmd.Parameters.Add("@Inv_Isc_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Isc_Importe"];
                            Cmd.Parameters.Add("@Inv_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Otros_Tributos_Importe"];
                            Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Traspaso"];
                               Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.VarChar).Value = filas["Invd_TipoBSA"];
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Invd_Catalogo"];
                            Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Invd_Cantidad"];
                            Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = filas["Invd_Valor_Unit"];
                            Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = filas["Invd_Total"];
                            Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.DateTime).Value = filas["Invd_Centro_CG"];
                            Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.VarChar).Value = filas["Invd_Unm"];
                            Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = filas["Invd_Tiene_Lote"];
                            Cmd.Parameters.Add("@Invd_Lote", System.Data.SqlDbType.VarChar).Value = filas["Invd_Lote"];
                            Cmd.Parameters.Add("@Invd_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Invd_FechaFabricacion"];
                            Cmd.Parameters.Add("@Invd_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Invd_FechaVencimiento"];
                            Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = filas["Invd_CantidadAtendidaEnBaseAlCoefciente"];
 
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = filas["Lot_Item"];
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Lot_Catalogo"];
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = filas["Lot_Lote"];
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Lot_FechaFabricacion"];
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Lot_FechaVencimiento"];
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Lot_Cantidad"];
                            Cmd.Parameters.Add("@Lot_Estado", System.Data.SqlDbType.VarChar).Value = filas["Lot_Estado"];
          
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Doc_Item", System.Data.SqlDbType.Int).Value = filas["Doc_Item"];
                            Cmd.Parameters.Add("@Doc_Proceso", System.Data.SqlDbType.VarChar).Value = filas["Doc_Proceso"];
                            Cmd.Parameters.Add("@Doc_Anio", System.Data.SqlDbType.VarChar).Value = filas["Doc_Anio"];
                            Cmd.Parameters.Add("@Doc_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Doc_Periodo"];
                            Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.VarChar).Value = filas["Doc_Folio"];
                            Cmd.Parameters.Add("@Inv_Almacen_Traspaso_Origen", System.Data.SqlDbType.VarChar).Value = filas["Inv_Almacen_Traspaso_Origen"];
              
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        //EXPORTAR SOLO MOVIMINETOS QEU FUEREON REGISTRADOS DE INVENTARIO
        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo_Operacion"];
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar).Value = filas["Inv_Glosa"];
                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Entidad"];
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Doc"];
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.Char).Value = filas["Inv_Serie"];
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar).Value = filas["Inv_Numero"];
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = filas["Inv_Fecha"];
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char).Value = filas["Inv_Almacen_O_D"];
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Responsable_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar).Value = filas["Inv_Libro"];
                            Cmd.Parameters.Add("@Inv_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Inv_Voucher"];
                            Cmd.Parameters.Add("@Inv_Estado", System.Data.SqlDbType.VarChar).Value = filas["Inv_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Inv_Base", System.Data.SqlDbType.Decimal).Value = filas["Inv_Base"];
                            Cmd.Parameters.Add("@Inv_Igv", System.Data.SqlDbType.Decimal).Value = filas["Inv_Igv"];
                            Cmd.Parameters.Add("@Inv_Total", System.Data.SqlDbType.Decimal).Value = filas["Inv_Total"];
                            Cmd.Parameters.Add("@Inv_pdc_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdc_codigo"];
                            Cmd.Parameters.Add("@Inv_pdv_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdv_codigo"];
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Cja_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo_Item"];
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = filas["cja_Estado"];
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Venta_Sin_Ticket"];
                            Cmd.Parameters.Add("@Es_Atendido_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Es_Atendido_Traspaso"];
                            Cmd.Parameters.Add("@Inv_Generado_En_Compra", System.Data.SqlDbType.Bit).Value = filas["Inv_Generado_En_Compra"];
                            Cmd.Parameters.Add("@Inv_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Valor_Fact_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Inafecta", System.Data.SqlDbType.Decimal).Value = filas["Inv_Inafecta"];
                            Cmd.Parameters.Add("@Inv_Isc_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Isc_Importe"];
                            Cmd.Parameters.Add("@Inv_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Otros_Tributos_Importe"];
                            Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Traspaso"];
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.VarChar).Value = filas["Invd_TipoBSA"];
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Invd_Catalogo"];
                            Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Invd_Cantidad"];
                            Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = filas["Invd_Valor_Unit"];
                            Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = filas["Invd_Total"];
                            Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.DateTime).Value = filas["Invd_Centro_CG"];
                            Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.VarChar).Value = filas["Invd_Unm"];
                            Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = filas["Invd_Tiene_Lote"];
                            Cmd.Parameters.Add("@Invd_Lote", System.Data.SqlDbType.VarChar).Value = filas["Invd_Lote"];
                            Cmd.Parameters.Add("@Invd_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Invd_FechaFabricacion"];
                            Cmd.Parameters.Add("@Invd_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Invd_FechaVencimiento"];
                            Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = filas["Invd_CantidadAtendidaEnBaseAlCoefciente"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = filas["Lot_Item"];
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Lot_Catalogo"];
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = filas["Lot_Lote"];
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Lot_FechaFabricacion"];
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Lot_FechaVencimiento"];
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Lot_Cantidad"];
                            Cmd.Parameters.Add("@Lot_Estado", System.Data.SqlDbType.VarChar).Value = filas["Lot_Estado"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }




        // EXPORTAR TRASPASOS
        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        //EXPORTAR TRASPASOS- INGRESOS

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        // IMPORTAR TRASPASOS

        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo_Operacion"];
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar).Value = filas["Inv_Glosa"];
                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Entidad"];
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Doc"];
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.Char).Value = filas["Inv_Serie"];
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar).Value = filas["Inv_Numero"];
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = filas["Inv_Fecha"];
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char).Value = filas["Inv_Almacen_O_D"];
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Responsable_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar).Value = filas["Inv_Libro"];
                            Cmd.Parameters.Add("@Inv_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Inv_Voucher"];
                            Cmd.Parameters.Add("@Inv_Estado", System.Data.SqlDbType.VarChar).Value = filas["Inv_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Inv_Base", System.Data.SqlDbType.Decimal).Value = filas["Inv_Base"];
                            Cmd.Parameters.Add("@Inv_Igv", System.Data.SqlDbType.Decimal).Value = filas["Inv_Igv"];
                            Cmd.Parameters.Add("@Inv_Total", System.Data.SqlDbType.Decimal).Value = filas["Inv_Total"];
                            Cmd.Parameters.Add("@Inv_pdc_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdc_codigo"];
                            Cmd.Parameters.Add("@Inv_pdv_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdv_codigo"];
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Cja_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo_Item"];
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = filas["cja_Estado"];
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Venta_Sin_Ticket"];
                            Cmd.Parameters.Add("@Es_Atendido_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Es_Atendido_Traspaso"];
                            Cmd.Parameters.Add("@Inv_Generado_En_Compra", System.Data.SqlDbType.Bit).Value = filas["Inv_Generado_En_Compra"];
                            Cmd.Parameters.Add("@Inv_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Valor_Fact_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Inafecta", System.Data.SqlDbType.Decimal).Value = filas["Inv_Inafecta"];
                            Cmd.Parameters.Add("@Inv_Isc_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Isc_Importe"];
                            Cmd.Parameters.Add("@Inv_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Otros_Tributos_Importe"];
                            Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Traspaso"];
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.VarChar).Value = filas["Invd_TipoBSA"];
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Invd_Catalogo"];
                            Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Invd_Cantidad"];
                            Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = filas["Invd_Valor_Unit"];
                            Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = filas["Invd_Total"];
                            Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.DateTime).Value = filas["Invd_Centro_CG"];
                            Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.VarChar).Value = filas["Invd_Unm"];
                            Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = filas["Invd_Tiene_Lote"];
                            Cmd.Parameters.Add("@Invd_Lote", System.Data.SqlDbType.VarChar).Value = filas["Invd_Lote"];
                            Cmd.Parameters.Add("@Invd_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Invd_FechaFabricacion"];
                            Cmd.Parameters.Add("@Invd_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Invd_FechaVencimiento"];
                            Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = filas["Invd_CantidadAtendidaEnBaseAlCoefciente"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = filas["Lot_Item"];
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Lot_Catalogo"];
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = filas["Lot_Lote"];
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Lot_FechaFabricacion"];
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Lot_FechaVencimiento"];
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Lot_Cantidad"];
                            Cmd.Parameters.Add("@Lot_Estado", System.Data.SqlDbType.VarChar).Value = filas["Lot_Estado"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        // IMPORTAR TRASPASOS - INGRESOS

        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo_Operacion"];
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar).Value = filas["Inv_Glosa"];
                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Entidad"];
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Doc"];
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.Char).Value = filas["Inv_Serie"];
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar).Value = filas["Inv_Numero"];
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = filas["Inv_Fecha"];
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char).Value = filas["Inv_Almacen_O_D"];
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Responsable_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar).Value = filas["Inv_Libro"];
                            Cmd.Parameters.Add("@Inv_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Inv_Voucher"];
                            Cmd.Parameters.Add("@Inv_Estado", System.Data.SqlDbType.VarChar).Value = filas["Inv_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Inv_Base", System.Data.SqlDbType.Decimal).Value = filas["Inv_Base"];
                            Cmd.Parameters.Add("@Inv_Igv", System.Data.SqlDbType.Decimal).Value = filas["Inv_Igv"];
                            Cmd.Parameters.Add("@Inv_Total", System.Data.SqlDbType.Decimal).Value = filas["Inv_Total"];
                            Cmd.Parameters.Add("@Inv_pdc_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdc_codigo"];
                            Cmd.Parameters.Add("@Inv_pdv_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdv_codigo"];
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Cja_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo_Item"];
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = filas["cja_Estado"];
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Venta_Sin_Ticket"];
                            Cmd.Parameters.Add("@Es_Atendido_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Es_Atendido_Traspaso"];
                            Cmd.Parameters.Add("@Inv_Generado_En_Compra", System.Data.SqlDbType.Bit).Value = filas["Inv_Generado_En_Compra"];
                            Cmd.Parameters.Add("@Inv_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Valor_Fact_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Inafecta", System.Data.SqlDbType.Decimal).Value = filas["Inv_Inafecta"];
                            Cmd.Parameters.Add("@Inv_Isc_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Isc_Importe"];
                            Cmd.Parameters.Add("@Inv_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Otros_Tributos_Importe"];
                            Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Traspaso"];
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.VarChar).Value = filas["Invd_TipoBSA"];
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Invd_Catalogo"];
                            Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Invd_Cantidad"];
                            Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = filas["Invd_Valor_Unit"];
                            Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = filas["Invd_Total"];
                            Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.DateTime).Value = filas["Invd_Centro_CG"];
                            Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.VarChar).Value = filas["Invd_Unm"];
                            Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = filas["Invd_Tiene_Lote"];
                            Cmd.Parameters.Add("@Invd_Lote", System.Data.SqlDbType.VarChar).Value = filas["Invd_Lote"];
                            Cmd.Parameters.Add("@Invd_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Invd_FechaFabricacion"];
                            Cmd.Parameters.Add("@Invd_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Invd_FechaVencimiento"];
                            Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = filas["Invd_CantidadAtendidaEnBaseAlCoefciente"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = filas["Lot_Item"];
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Lot_Catalogo"];
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = filas["Lot_Lote"];
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Lot_FechaFabricacion"];
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Lot_FechaVencimiento"];
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Lot_Cantidad"];
                            Cmd.Parameters.Add("@Lot_Estado", System.Data.SqlDbType.VarChar).Value = filas["Lot_Estado"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


     public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Doc_Item", System.Data.SqlDbType.Int).Value = filas["Doc_Item"];
                            Cmd.Parameters.Add("@Doc_Proceso", System.Data.SqlDbType.VarChar).Value = filas["Doc_Proceso"];
                            Cmd.Parameters.Add("@Doc_Anio", System.Data.SqlDbType.VarChar).Value = filas["Doc_Anio"];
                            Cmd.Parameters.Add("@Doc_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Doc_Periodo"];
                            Cmd.Parameters.Add("@Doc_Folio", System.Data.SqlDbType.VarChar).Value = filas["Doc_Folio"];
                            Cmd.Parameters.Add("@Inv_Almacen_Traspaso_Origen", System.Data.SqlDbType.VarChar).Value = filas["Inv_Almacen_Traspaso_Origen"];
 
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        //  PRECIO


        public DataSet EXPORTAR_VEN_LISTA_PRECIO_CAB_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_VEN_LISTA_PRECIO_CAB_PRECIO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@establecimiento", SqlDbType.VarChar).Value = pEntidad.establecimiento;

                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_VEN_LISTA_PRECIO_DET_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_VEN_LISTA_PRECIO_DET_PRECIO", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@establecimiento", SqlDbType.VarChar).Value = pEntidad.establecimiento;

                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }


        public void IMPORTAR_VEN_LISTA_PRECIO_CAB_PRECIO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_VEN_LISTA_PRECIO_CAB_PRECIO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Est_Codigo"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char).Value = filas["Id_Catalogo"];
                            Cmd.Parameters.Add("@Pre_Estado", System.Data.SqlDbType.VarChar).Value = filas["Pre_Estado"];
                            Cmd.Parameters.Add("@Pres_Es_Variable", System.Data.SqlDbType.Bit).Value = filas["Pres_Es_Variable"];
                            Cmd.Parameters.Add("@Pre_Minimo", System.Data.SqlDbType.Decimal).Value = filas["Pre_Minimo"];
                            Cmd.Parameters.Add("@Pre_Maximo", System.Data.SqlDbType.Decimal).Value = filas["Pre_Maximo"];
                            Cmd.Parameters.Add("@Pre_Fecha_Vigencia", System.Data.SqlDbType.DateTime).Value = filas["Pre_Fecha_Vigencia"];
                           
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        public void IMPORTAR_VEN_LISTA_PRECIO_DET_PRECIO_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_VEN_LISTA_PRECIO_DET_PRECIO", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Est_Codigo"];
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo"];
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char).Value = filas["Id_Catalogo"];
                            Cmd.Parameters.Add("@Pre_Correlativo", System.Data.SqlDbType.Int).Value = filas["Pre_Correlativo"];
                            Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.VarChar).Value = filas["Pre_Id_Unm"];
                            Cmd.Parameters.Add("@Pre_Id_Mod_Venta", System.Data.SqlDbType.VarChar).Value = filas["Pre_Id_Mod_Venta"];
                            Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar).Value = filas["Pre_Moneda_cod"];
                            Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.DateTime).Value = filas["Pre_Fecha_Ini"];
                            Cmd.Parameters.Add("@Pre_Fecha_Fin", System.Data.SqlDbType.DateTime).Value = filas["Pre_Fecha_Fin"];
                            Cmd.Parameters.Add("@Pres_Es_Variable", System.Data.SqlDbType.Bit).Value = filas["Pres_Es_Variable"];
                            Cmd.Parameters.Add("@Pre_Minimo", System.Data.SqlDbType.Decimal).Value = filas["Pre_Minimo"];
                            Cmd.Parameters.Add("@Pre_Maximo", System.Data.SqlDbType.Decimal).Value = filas["Pre_Maximo"];
                            Cmd.Parameters.Add("@Pre_Requiere_Codigo_Autoriza", System.Data.SqlDbType.Bit).Value = filas["Pre_Requiere_Codigo_Autoriza"];
                            Cmd.Parameters.Add("@Pre_Codigo_Autoriza", System.Data.SqlDbType.VarChar).Value = filas["Pre_Codigo_Autoriza"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        // EXPORTAR AMBAS VENTAS CON TICKET - SIN TICKET
        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBO_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBOS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(Entidad_Exportar pEntidad)
        {
            var Ds = new DataSet();
            using (var Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
            {
                try
                {
                    using (var Cmd = new SqlCommand("pa_EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS", Cn)
                    { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    {
                        using (var Da = new SqlDataAdapter(Cmd))
                        {
                            Cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = pEntidad.Empresa;
                            Cmd.Parameters.Add("@desde", SqlDbType.Date).Value = pEntidad.desde;
                            Cmd.Parameters.Add("@hasta", SqlDbType.Date).Value = pEntidad.hasta;
                            Da.Fill(Ds);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return Ds;
        }

        // IMPORTAR AMBAS VENTAS CON TICKET - SIN TICKET
        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBOS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBOS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Tipo_Operacion", System.Data.SqlDbType.VarChar).Value = filas["Id_Tipo_Operacion"];
                            Cmd.Parameters.Add("@Inv_Glosa", System.Data.SqlDbType.VarChar).Value = filas["Inv_Glosa"];
                            Cmd.Parameters.Add("@Inv_Tipo_Entidad", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Entidad"];
                            Cmd.Parameters.Add("@Inv_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Tipo_Doc", System.Data.SqlDbType.VarChar).Value = filas["Inv_Tipo_Doc"];
                            Cmd.Parameters.Add("@Inv_Serie", System.Data.SqlDbType.Char).Value = filas["Inv_Serie"];
                            Cmd.Parameters.Add("@Inv_Numero", System.Data.SqlDbType.VarChar).Value = filas["Inv_Numero"];
                            Cmd.Parameters.Add("@Inv_Fecha", System.Data.SqlDbType.Date).Value = filas["Inv_Fecha"];
                            Cmd.Parameters.Add("@Inv_Almacen_O_D", System.Data.SqlDbType.Char).Value = filas["Inv_Almacen_O_D"];
                            Cmd.Parameters.Add("@Inv_Responsable_Ruc_Dni", System.Data.SqlDbType.VarChar).Value = filas["Inv_Responsable_Ruc_Dni"];
                            Cmd.Parameters.Add("@Inv_Libro", System.Data.SqlDbType.VarChar).Value = filas["Inv_Libro"];
                            Cmd.Parameters.Add("@Inv_Voucher", System.Data.SqlDbType.VarChar).Value = filas["Inv_Voucher"];
                            Cmd.Parameters.Add("@Inv_Estado", System.Data.SqlDbType.VarChar).Value = filas["Inv_Estado"];
                            Cmd.Parameters.Add("@Usuario_Crea", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Crea"];
                            Cmd.Parameters.Add("@Fecha_Crea", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Crea"];
                            Cmd.Parameters.Add("@Maquina_Crea", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Crea"];
                            Cmd.Parameters.Add("@Usuario_Modi", System.Data.SqlDbType.VarChar).Value = filas["Usuario_Modi"];
                            Cmd.Parameters.Add("@Fecha_Modi", System.Data.SqlDbType.DateTime).Value = filas["Fecha_Modi"];
                            Cmd.Parameters.Add("@Maquina_Modi", System.Data.SqlDbType.VarChar).Value = filas["Maquina_Modi"];
                            Cmd.Parameters.Add("@Inv_Base", System.Data.SqlDbType.Decimal).Value = filas["Inv_Base"];
                            Cmd.Parameters.Add("@Inv_Igv", System.Data.SqlDbType.Decimal).Value = filas["Inv_Igv"];
                            Cmd.Parameters.Add("@Inv_Total", System.Data.SqlDbType.Decimal).Value = filas["Inv_Total"];
                            Cmd.Parameters.Add("@Inv_pdc_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdc_codigo"];
                            Cmd.Parameters.Add("@Inv_pdv_codigo", System.Data.SqlDbType.VarChar).Value = filas["Inv_pdv_codigo"];
                            Cmd.Parameters.Add("@Cja_Codigo", System.Data.SqlDbType.VarChar).Value = filas["Cja_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo"];
                            Cmd.Parameters.Add("@Cja_Aper_Cierre_Codigo_Item", System.Data.SqlDbType.Int).Value = filas["Cja_Aper_Cierre_Codigo_Item"];
                            Cmd.Parameters.Add("@cja_Estado", System.Data.SqlDbType.VarChar).Value = filas["cja_Estado"];
                            Cmd.Parameters.Add("@Inv_Es_Venta_Sin_Ticket", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Venta_Sin_Ticket"];
                            Cmd.Parameters.Add("@Es_Atendido_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Es_Atendido_Traspaso"];
                            Cmd.Parameters.Add("@Inv_Generado_En_Compra", System.Data.SqlDbType.Bit).Value = filas["Inv_Generado_En_Compra"];
                            Cmd.Parameters.Add("@Inv_Valor_Fact_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Valor_Fact_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Exonerada", System.Data.SqlDbType.Decimal).Value = filas["Inv_Exonerada"];
                            Cmd.Parameters.Add("@Inv_Inafecta", System.Data.SqlDbType.Decimal).Value = filas["Inv_Inafecta"];
                            Cmd.Parameters.Add("@Inv_Isc_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Isc_Importe"];
                            Cmd.Parameters.Add("@Inv_Otros_Tributos_Importe", System.Data.SqlDbType.Decimal).Value = filas["Inv_Otros_Tributos_Importe"];
                            Cmd.Parameters.Add("@Inv_Es_Traspaso", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Traspaso"];
                            Cmd.Parameters.Add("@Inv_Es_Venta", System.Data.SqlDbType.Bit).Value = filas["Inv_Es_Venta"];
                      
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.VarChar).Value = filas["Invd_TipoBSA"];
                            Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Invd_Catalogo"];
                            Cmd.Parameters.Add("@Invd_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Invd_Cantidad"];
                            Cmd.Parameters.Add("@Invd_Valor_Unit", System.Data.SqlDbType.Decimal).Value = filas["Invd_Valor_Unit"];
                            Cmd.Parameters.Add("@Invd_Total", System.Data.SqlDbType.Decimal).Value = filas["Invd_Total"];
                            Cmd.Parameters.Add("@Invd_Centro_CG", System.Data.SqlDbType.DateTime).Value = filas["Invd_Centro_CG"];
                            Cmd.Parameters.Add("@Invd_Unm", System.Data.SqlDbType.VarChar).Value = filas["Invd_Unm"];
                            Cmd.Parameters.Add("@Invd_Tiene_Lote", System.Data.SqlDbType.Bit).Value = filas["Invd_Tiene_Lote"];
                            Cmd.Parameters.Add("@Invd_Lote", System.Data.SqlDbType.VarChar).Value = filas["Invd_Lote"];
                            Cmd.Parameters.Add("@Invd_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Invd_FechaFabricacion"];
                            Cmd.Parameters.Add("@Invd_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Invd_FechaVencimiento"];
                            Cmd.Parameters.Add("@Invd_CantidadAtendidaEnBaseAlCoefciente", System.Data.SqlDbType.Decimal).Value = filas["Invd_CantidadAtendidaEnBaseAlCoefciente"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(DataTable DT)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (DataRow filas in DT.Rows)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = filas["Id_Empresa"];
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = filas["Id_Anio"];
                            Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.VarChar).Value = filas["Id_Periodo"];
                            Cmd.Parameters.Add("@Id_Tipo_Mov", System.Data.SqlDbType.Char).Value = filas["Id_Tipo_Mov"];
                            Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.VarChar).Value = filas["Id_Almacen"];
                            Cmd.Parameters.Add("@Id_Movimiento", System.Data.SqlDbType.VarChar).Value = filas["Id_Movimiento"];
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = filas["Id_Item"];
                            Cmd.Parameters.Add("@Lot_Item", System.Data.SqlDbType.Int).Value = filas["Lot_Item"];
                            Cmd.Parameters.Add("@Lot_Catalogo", System.Data.SqlDbType.VarChar).Value = filas["Lot_Catalogo"];
                            Cmd.Parameters.Add("@Lot_Lote", System.Data.SqlDbType.VarChar).Value = filas["Lot_Lote"];
                            Cmd.Parameters.Add("@Lot_FechaFabricacion", System.Data.SqlDbType.Date).Value = filas["Lot_FechaFabricacion"];
                            Cmd.Parameters.Add("@Lot_FechaVencimiento", System.Data.SqlDbType.Date).Value = filas["Lot_FechaVencimiento"];
                            Cmd.Parameters.Add("@Lot_Cantidad", System.Data.SqlDbType.Decimal).Value = filas["Lot_Cantidad"];
                            Cmd.Parameters.Add("@Lot_Estado", System.Data.SqlDbType.VarChar).Value = filas["Lot_Estado"];

                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }


        //public void IMPORTAR_CATALOGO_FAMILIA_XML(DataTable DT)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_FAMILIA_XML", Cn))
        //            {
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                foreach (DataRow filas in DT.Rows)
        //                {
        //                    Cmd.Parameters.Clear();
        //                    Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.Char).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.Char).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.DateTime).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.DateTime).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];

        //                    Cmd.ExecuteNonQuery();
        //                }
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Trs.Rollback();
        //        throw new Exception(ex.Message);
        //    }
        //}





        //public void IMPORTAR_CATALOGO_FAMILIA_XML(DataTable DT)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_IMPORTAR_CTB_CATALOGO_FAMILIA_XML", Cn))
        //            {
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                foreach (DataRow filas in DT.Rows)
        //                {
        //                    Cmd.Parameters.Clear();
        //                    Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.Char).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.Char).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.DateTime).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.DateTime).Value = filas[""];
        //                    Cmd.Parameters.Add("", System.Data.SqlDbType.VarChar).Value = filas[""];

        //                    Cmd.ExecuteNonQuery();
        //                }
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Trs.Rollback();
        //        throw new Exception(ex.Message);
        //    }
        //}


















    }
}
