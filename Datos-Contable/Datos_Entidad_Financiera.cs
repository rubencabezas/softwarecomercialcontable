﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Contable
{
 public   class Datos_Entidad_Financiera
    {
        public void Insertar(Entidad_Entidad_Financiera Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ENTIDAD_FINANCIERA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_SUNAT", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Id_SUNAT;
                        Cmd.Parameters.Add("@Fin_Descripcion", System.Data.SqlDbType.VarChar,500).Value = Clas_Enti.Fin_Descripcion;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Entidad_Financiera Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ENTIDAD_FINANCIERA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_SUNAT", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Id_SUNAT;
                        Cmd.Parameters.Add("@Fin_Descripcion", System.Data.SqlDbType.VarChar, 500).Value = Clas_Enti.Fin_Descripcion;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public void Eliminar(Entidad_Medio_Pago Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_ENTIDAD_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Ent_RUC_DNI", System.Data.SqlDbType.Char, 11).Value = Clas_Enti.Ent_RUC_DNI;
        //                Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.Char, 50).Value = Actual_Conexion.UserName;
        //                Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.Char, 50).Value = Actual_Conexion.Maquina;


        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}



        public List<Entidad_Entidad_Financiera> Listar(Entidad_Entidad_Financiera Cls_Enti)
        {
            List<Entidad_Entidad_Financiera> ListaItems = new List<Entidad_Entidad_Financiera>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ENTIDAD_FINANCIERA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_SUNAT", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_SUNAT;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Entidad_Financiera
                                {
                                    Id_SUNAT = Dr.GetString(0).Trim(),
                                    Fin_Descripcion = Dr.GetString(1).Trim(),
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
