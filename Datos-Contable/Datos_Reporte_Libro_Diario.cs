﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Contable
{
 public   class Datos_Reporte_Libro_Diario
    {
        public List<Entidad_Reporte_Libro_Diario> ListarLibroDiarioCentralizado(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_REPORTE_LIBRO_DIARIO_CENTRALIZADO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Acumulado", System.Data.SqlDbType.Bit).Value = Cls_Enti.Acumulado;
                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                    Correlativo = dr.GetString(0),
                                    FechaOperacion = dr.GetDateTime(1),
                                    NumMov = dr.GetString(2),
                                    Glosa = dr.GetString(3),
                                    Lib_Codigo = dr.GetString(4),
                                    Lib_Sunat = dr.GetString(5),
                                    Voucher = dr.GetString(6),
                                    TipoDoc = dr.GetString(7),
                                    DocSunat = dr.GetString(8),
                                   SerieDoc = dr.GetString(9),
                                    NumeroDoc = dr.GetString(10),
                                    Cuenta = dr.GetString(11),
                                    Descripcion = dr.GetString(12),
                                    DebeNacional = dr.GetDecimal(13),
                                    HaberNacional = dr.GetDecimal(14),
                                    Cta_Destino = dr.GetBoolean(15),
                                    TipoDebeHaber = dr.GetString(16)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Reporte_Libro_Diario> ListarLibroDiario(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_REPORTE_LIBRO_DIARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {


                        Cmd.Parameters.Add("@Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Lib_Codigo", System.Data.SqlDbType.Char,3).Value = Cls_Enti.Lib_Codigo;
                        Cmd.Parameters.Add("@Acumulado", System.Data.SqlDbType.Bit).Value = Cls_Enti.Acumulado;

                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                   FechaOperacion = dr.GetDateTime(0), 
                                    NumMov = dr.GetString(1), 
                                    Glosa = dr.GetString(2), 
                                    Lib_Codigo = dr.GetString(3), 
                                    Lib_Sunat = dr.GetString(4),
                                    Voucher = dr.GetString(5), 
                                    TipoDoc = dr.GetString(6), 
                                    DocSunat = dr.GetString(7), 
                                    SerieDoc = dr.GetString(8), 
                                    NumeroDoc = dr.GetString(9),
                                     Cuenta = dr.GetString(10), 
                                     Descripcion = dr.GetString(11), 
                                     DebeNacional = dr.GetDecimal(12), 
                                     HaberNacional = dr.GetDecimal(13)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Reporte_Libro_Diario> Ple_V5(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paCTB_PLE_5_0_LIBRO_DIARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {


                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Lib_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Lib_Codigo;
                        Cmd.Parameters.Add("@PAcumulado", System.Data.SqlDbType.Bit).Value = Cls_Enti.Acumulado;

                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                    ple_V5 = dr.GetString(0)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Reporte_Libro_Diario> Ple_V5_Cuentas(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paCTB_PLE_5_0_LIBRO_DIARIO_CUENTA_CONTABLE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {


                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
               
                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                    ple_V5_cuentas = dr.GetString(0)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Reporte_Libro_Diario> Ple_V5_Mayor(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paCTB_PLE_5_0_LIBRO_MAYOR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {


                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cmd.Parameters.Add("@Lib_Codigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Lib_Codigo;
                        Cmd.Parameters.Add("@PAcumulado", System.Data.SqlDbType.Bit).Value = Cls_Enti.Acumulado;

                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                    ple_V5 = dr.GetString(0)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public DataTable  Rep_LibroDiarioSimplificado(Entidad_Reporte_Libro_Diario Ent)  
        {
   DataTable Dt = new DataTable();
            try
            {

         
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using(SqlCommand cmd = new SqlCommand("paRpt_Cont_Libro_Diaio_Simplificado", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add("@PEmpr", SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
                        cmd.Parameters.Add("@PAnio", SqlDbType.Char, 4).Value = Actual_Conexion.AnioSelect;
                        cmd.Parameters.Add("@PMes", SqlDbType.Char, 2).Value = Ent.Id_Periodo;
                        cmd.Parameters.Add("@PNivel", SqlDbType.Int).Value = Ent.Nivel;
                        Cn.Open();
                        //Using Da As New SqlDataAdapter(cmd)
                        //    Da.Fill(Dt)
                        //End Using
                        using (SqlDataAdapter Da = new SqlDataAdapter(cmd))
                        {
                            Da.Fill(Dt);

                        }
                     
                    }
                   
                }
               return Dt;
            }
  

            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
   
        }


        public List<Entidad_Reporte_Libro_Diario> ListarLibro_Diario_Formato_Simplificado_PLE_5_0(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paCTB_PLE_5_0_LIBRO_DIARIO_FORMATO_SIMPLIFICADO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                    ple_V5 = dr.GetString(0),
                                    periodo_V5 = dr.GetString(1),
                                    cuo_V5 = dr.GetString(2),
                                    correlativo_V5 = dr.GetString(3),
                                    cod_plan_cuentas_V5 = dr.GetString(4),
                                    cod_unidad_operacion_V5 = dr.GetString(5),
                                    cod_centro_costos_V5 = dr.GetString(6),
                                    tipo_moneda_V5 = dr.GetString(7),
                                    tipo_doc_emisor_V5 = dr.GetString(8),
                                    numero_doc_emisor_V5 = dr.GetString(9),
                                    tipo_Cmoprobante_pago_V5 = dr.GetString(10),
                                    serie_V5 = dr.GetString(11),
                                    numero_V5 = dr.GetString(12),
                                    fecha_contable_V5 = dr.GetString(13),
                                    fecha_vencimiento_V5 = dr.GetString(14),
                                    fecha_operacion_V5 = dr.GetString(15),
                                    glosa_V5 = dr.GetString(16),
                                    glosa_referencial_V5 = dr.GetString(17),
                                    debe_V5 = dr.GetString(18),
                                    haber_V5 = dr.GetString(19),
                                    dato_estructurado_V5 = dr.GetString(20),
                                    estado_V5 = dr.GetString(21)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }





        public List<Entidad_Reporte_Libro_Diario> ListarLibro_Diario_Formato_Simplificado_PLE_5_0_Detalle_Plan(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            List<Entidad_Reporte_Libro_Diario> ListaItems = new List<Entidad_Reporte_Libro_Diario>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("paCTB_PLE_5_0_LIBRO_DIARIO_FORMATO_SIMPLIFICADO_CUENTA_CONTABLE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@PEmp_cCodigo", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@PPan_cAnio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@PPer_cPeriodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cn.Open();
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListaItems.Add(new Entidad_Reporte_Libro_Diario
                                {
                                       ple_V5 = dr.GetString(0),
                                periodo_V5 = dr.GetString(1),
                                cuenta_ctb_V5 = dr.GetString(2),
                                Cta_Descripcion = dr.GetString(3),
                                cod_plan_cuentas_V5 = dr.GetString(4),
                                desc_Plan_cuenta = dr.GetString(5),
                                estado_V5 = dr.GetString(6)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }





    }
}
