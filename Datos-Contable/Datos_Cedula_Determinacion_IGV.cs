﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Datos_Cedula_Determinacion_IGV
    {
        public List<Entidad_Cedula_Determinacion_IGV> Listar(Entidad_Cedula_Determinacion_IGV Cls_Enti)
        {
            List<Entidad_Cedula_Determinacion_IGV> ListaItems = new List<Entidad_Cedula_Determinacion_IGV>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_DETERMINACION_IGV", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Empresa", System.Data.SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
                        Cmd.Parameters.Add("@Anio", System.Data.SqlDbType.Char, 4).Value = Actual_Conexion.AnioSelect;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Cedula_Determinacion_IGV
                                {
                                    Anio=Dr.GetString(0),
                                    Id_Periodo =Dr.GetString(1),
                                    Periodo=Dr.GetString(2),
                                    EXPORTACIONES_FACTURADAS=Dr.GetDecimal(3),
                                    VENTAS_NO_GRABADAS =Dr.GetDecimal(4),
                                    VENTAS_NETAS=Dr.GetDecimal(5),
                                    DEBITO_FISCAL=Dr.GetDecimal(6),
                                    DESC_CONCED_DEV_VENTAS = Dr.GetDecimal(7),
                                    DEB_FISCAL_FISCAL_DESC_CONCED = Dr.GetDecimal(8),
                                    BASE_IMPONIBLE_NACIONAL=Dr.GetDecimal(9),
                                    IGV_NACIONAL=Dr.GetDecimal(10),
                                    BASE_IMPONIBLE_2_NACIONAL=Dr.GetDecimal(11),
                                    IGV_2_NACIONAL=Dr.GetDecimal(12),
                                    BASE_IMPONIBLE_3_NACIONAL=Dr.GetDecimal(13),
                                    IGV_3_NACIONAL=Dr.GetDecimal(14),
                                    ADQU_NO_GRADAS=Dr.GetDecimal(15),
                                    IMPUESTO_SELECTIVO_CONSUMO=Dr.GetDecimal(16),
                                    OTROS_TRIBUTOS_CARGO=Dr.GetDecimal(17),
                                    CREDITO_FISCAL_MES_ANTERIOR_X=Dr.GetDecimal(18),
                                    COMPENSACION_DEL_IGV=Dr.GetDecimal(19),
                                    DEVUELTO_CON_NCN_EXPORTADO = Dr.GetDecimal(20),
                                    PERCEPCIONES_DEL_MES=Dr.GetDecimal(21),
                                    PERCEPCION_MES_ANTERIOR_X=Dr.GetDecimal(22),
                                    PERCEPCIONES_APLICADAS=Dr.GetDecimal(23),
                                    RETENCIONES_DEL_MES = Dr.GetDecimal(24),
                                    RETENCION_MES_ANTERIOR_X = Dr.GetDecimal(25),
                                    RETENCION_APLICADAS = Dr.GetDecimal(26),
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
