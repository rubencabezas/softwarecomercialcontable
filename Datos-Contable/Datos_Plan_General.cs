﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
 public   class Datos_Plan_General
    {
        public void Insertar(Entidad_Plan_General Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_GENERAL_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Id_Cuenta;
                        Cmd.Parameters.Add("@Cta_Descripcion", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Cta_Descripcion;
                        Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Id_Elemento;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Id_Estructura;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Entidad_Plan_General ListarDatos(Entidad_Plan_General EntCta)
        {
            Entidad_Plan_General Ent = new Entidad_Plan_General();
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand("pa_GNL_PLAN_CONTABLE_BUSAR_ELE_EST", cn))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = EntCta.Id_Cuenta;
                        cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.Char, 1).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Ele_Descripcion", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char,2).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Est_Descripcion", System.Data.SqlDbType.VarChar,50).Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add("@Est_Num_Digitos", System.Data.SqlDbType.Char, 2).Direction = System.Data.ParameterDirection.Output;

                        cn.Open();
                        Trs = cn.BeginTransaction();
                        cmd.Transaction = Trs;
                        cmd.ExecuteNonQuery();
                        Ent.Id_Elemento = cmd.Parameters["@Id_Elemento"].Value.ToString().Trim();
                        Ent.Ele_Descripcion = cmd.Parameters["@Ele_Descripcion"].Value.ToString().Trim();
                        Ent.Id_Estructura = cmd.Parameters["@Id_Estructura"].Value.ToString().Trim();
                        Ent.Est_Descripcion = cmd.Parameters["@Est_Descripcion"].Value.ToString().Trim();
                        Ent.Est_Num_Digitos = cmd.Parameters["@Est_Num_Digitos"].Value.ToString().Trim();
               
                
                        Trs.Commit();
                    }
                }
                return Ent;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public void Modificar(Entidad_Plan_General Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_GENERAL_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Id_Cuenta;
                        Cmd.Parameters.Add("@Cta_Descripcion", System.Data.SqlDbType.VarChar, 50).Value = Clas_Enti.Cta_Descripcion;
                        Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.VarChar, 1).Value = Clas_Enti.Id_Elemento;
                        Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Id_Estructura;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Entidad_Plan_General Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_GENERAL_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = Clas_Enti.Id_Cuenta;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public void Eliminar(Entidad_Estructura Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_ESTRUCTURA_ELIMINAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Estructura;
        //                Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = "";
        //                Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = "";
        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Plan_General> Listar(Entidad_Plan_General Cls_Enti)
        {
            List<Entidad_Plan_General> ListaItems = new List<Entidad_Plan_General>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_GENERAL_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = (Cls_Enti.Id_Cuenta == null ? null : Cls_Enti.Id_Cuenta);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Plan_General
                                {
                                    Id_Cuenta = Dr.GetString(0),
                                    Cta_Descripcion = Dr.GetString(1),
                                    Id_Elemento = Dr.GetString(2),
                                    Ele_Descripcion=Dr.GetString(3),
                                    Id_Estructura=Dr.GetString(4),
                                    Est_Descripcion=Dr.GetString(5)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public Entidad_Plan_General TraerDigito()
        {
            Entidad_Plan_General Ent = new Entidad_Plan_General();
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_Cont_TraerDigito", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Digito", System.Data.SqlDbType.Char, 4).Direction = System.Data.ParameterDirection.Output;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Ent.Est_Num_Digitos = Cmd.Parameters["@Digito"].Value.ToString().Trim(); 
                        Trs.Commit();
                    }
                }
                return Ent;
            }
            catch (Exception ex)
            {
                // Trs.Rollback()
                throw new Exception(ex.Message);
            }
        }


        public void Insertar_En_Cantidad(Entidad_Plan_General Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_PLAN_GENERAL_INSERTAR", Cn))
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach (Entidad_Plan_General ent in Clas_Enti.DetallesCuentas)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Id_Cuenta", System.Data.SqlDbType.Char, 12).Value = ent.Id_Cuenta;
                            Cmd.Parameters.Add("@Cta_Descripcion", System.Data.SqlDbType.VarChar, 50).Value = ent.Cta_Descripcion;
                            Cmd.Parameters.Add("@Id_Elemento", System.Data.SqlDbType.VarChar, 1).Value = ent.Id_Elemento;
                            Cmd.Parameters.Add("@Id_Estructura", System.Data.SqlDbType.VarChar, 2).Value = ent.Id_Estructura;

                            Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                            Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                            Cmd.ExecuteNonQuery();
                        }
                   
                 
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
