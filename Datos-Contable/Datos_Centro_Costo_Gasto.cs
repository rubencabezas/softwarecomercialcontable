﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
 public   class Datos_Centro_Costo_Gasto
    {
        public void Insertar(Entidad_Centro_Costo_Gasto Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CENTRO_COSTO_GASTO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
                        Cmd.Parameters.Add("@Id_Centro_cg", System.Data.SqlDbType.VarChar, 3).Direction=System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Id_Unidad", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Unidad;
                        Cmd.Parameters.Add("@Ccg_Tipo_cg", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ccg_Tipo_cg;
                        Cmd.Parameters.Add("@Ccg_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Ccg_Descripcion;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        //Clas_Enti.Id_Libro = Cmd.Parameters("@Id_Libro").Value.;
                        Clas_Enti.Id_Centro_cg = Cmd.Parameters["@Id_Centro_cg"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Centro_Costo_Gasto Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CENTRO_COSTO_GASTO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
                        Cmd.Parameters.Add("@Id_Centro_cg", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Centro_cg;
                        Cmd.Parameters.Add("@Id_Unidad", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Centro_cg;
                        Cmd.Parameters.Add("@Ccg_Tipo_cg", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Ccg_Tipo_cg;
                        Cmd.Parameters.Add("@Ccg_Descripcion", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Ccg_Descripcion;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Centro_Costo_Gasto Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CENTRO_COSTO_GASTO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Actual_Conexion.CodigoEmpresa;
                        Cmd.Parameters.Add("@Id_Centro_cg", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Centro_cg;
                        Cmd.Parameters.Add("@Id_Unidad", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Id_Centro_cg;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Centro_Costo_Gasto> Listar(Entidad_Centro_Costo_Gasto Cls_Enti)
        {
            List<Entidad_Centro_Costo_Gasto> ListaItems = new List<Entidad_Centro_Costo_Gasto>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CENTRO_COSTO_GASTO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Centro_cg", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Centro_cg;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Centro_Costo_Gasto
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Centro_cg = Dr.GetString(1),
                                    Id_Unidad = Dr.GetString(2),
                                    Und_Descripcion=Dr.GetString(3),
                                    Ccg_Tipo_cg=Dr.GetString(4),
                                    Gen_Codigo_Interno=Dr.GetString(5),
                                    Gen_Descripcion_Det =Dr.GetString(6),
                                    Ccg_Descripcion=Dr.GetString(7)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
    }
}
