﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
    public class Datos_Tipo_Cambio
    {

        public void Insertar(Entidad_Tipo_Cambio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_CAMBIO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Tic_Fecha", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Tic_Fecha;
                        Cmd.Parameters.Add("@Tic_Id_Moneda", System.Data.SqlDbType.VarChar, 3).Value = Clas_Enti.Tic_Id_Moneda;

                        Cmd.Parameters.Add("@Tic_Compra_Vigente", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tic_Compra_Vigente;

                        Cmd.Parameters.Add("@Tic_Venta_Vigente", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tic_Venta_Vigente;


                        Cmd.Parameters.Add("@Tic_Venta_Publicacion", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tic_Venta_Publicacion;


                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Tipo_Cambio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_CAMBIO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Tic_Fecha", System.Data.SqlDbType.Date).Value = Clas_Enti.Tic_Fecha;
                        Cmd.Parameters.Add("@Tic_Id_Moneda", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Tic_Id_Moneda;


                        Cmd.Parameters.Add("@Tic_Compra_Vigente", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tic_Compra_Vigente;


                        Cmd.Parameters.Add("@Tic_Venta_Vigente", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tic_Venta_Vigente;


                        Cmd.Parameters.Add("@Tic_Venta_Publicacion", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Tic_Venta_Publicacion;

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Tipo_Cambio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_CAMBIO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Tic_Fecha", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Tic_Fecha;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Tipo_Cambio> Listar(Entidad_Tipo_Cambio Cls_Enti)
        {
            List<Entidad_Tipo_Cambio> ListaItems = new List<Entidad_Tipo_Cambio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_CAMBIO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        //Cmd.Parameters.Add("@Tic_Fecha", System.Data.SqlDbType.Date).Value = Cls_Enti.Tic_Fecha;//(Cls_Enti.Tic_Fecha = "#12:00:00 AM#" ? null : Cls_Enti.Tic_Fecha); 

                        Cmd.Parameters.Add("@Tic_Fecha", System.Data.SqlDbType.Date).Value = (Cls_Enti.Tic_Fecha == Convert.ToDateTime("01/01/0001").Date ? null : String.Format("{0:yyyy-MM-dd}", Cls_Enti.Tic_Fecha));
                        //Cmd.Parameters.Add("@PCmp_NPFecha", SqlDbType.Date).Value = (Cls_Enti.Dcm_VTFecha == "#12:00:00 AM#" ? null : Cls_Enti.Dcm_VTFecha);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Tipo_Cambio
                                {
                                    Tic_Fecha = Dr.GetDateTime(0),
                                    Tic_Id_Moneda = Dr.GetString(1),
                                    Moneda_des = Dr.GetString(2),
                                    Tic_Compra_Vigente = Dr.GetDecimal(3),
                                    Tic_Venta_Vigente = Dr.GetDecimal(4),
                                    Tic_Venta_Publicacion = Dr.GetDecimal(5)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }




        int SabeFila;
        public List<Entidad_Tipo_Cambio> Buscar_Tipo_Cambio(Entidad_Tipo_Cambio Cls_Enti)
        {
            List<Entidad_Tipo_Cambio> ListaItems = new List<Entidad_Tipo_Cambio>();
            List<Entidad_Tipo_Cambio> DatosDevueltos = new List<Entidad_Tipo_Cambio>();
            try
            {


                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_GNL_TIPO_CAMBIO_BUSCAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {

                        Cmd.Parameters.Add("@Tic_Fecha", System.Data.SqlDbType.Date).Value = (Cls_Enti.Tic_Fecha == Convert.ToDateTime("01/01/0001").Date ? null : String.Format("{0:yyyy-MM-dd}", Cls_Enti.Tic_Fecha));
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {

                            while (Dr.Read())
                            {

                                SabeFila += 1;
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "COM", Nombre = "COMPRA VIGENTE", Valor = Dr.GetDecimal(0), Modif = false, Tic_Fecha = Dr.GetDateTime(3) });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "VEN", Nombre = "VENTA VIGENTE", Valor = Dr.GetDecimal(1), Modif = false, Tic_Fecha = Dr.GetDateTime(3) });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "VEP", Nombre = "VENTA PUBLICACION", Valor = Dr.GetDecimal(2), Modif = false, Tic_Fecha = Dr.GetDateTime(3) });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "SCV", Nombre = "SIN CONVERSION", Valor = Convert.ToDecimal(1.0), Modif = false, Tic_Fecha = Dr.GetDateTime(3) });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "OTR", Nombre = "OTROS", Valor = Convert.ToDecimal(1.0), Modif = true, Tic_Fecha = Dr.GetDateTime(3) });


                            }
                            if (SabeFila <= 0)
                            {
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "COM", Nombre = "COMPRA VIGENTE", Valor = Convert.ToDecimal(1.0), Modif = false, Tic_Fecha = DateTime.Now });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "VEN", Nombre = "VENTA VIGENTE", Valor = Convert.ToDecimal(1.0), Modif = false, Tic_Fecha = DateTime.Now });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "VEP", Nombre = "VENTA PUBLICACION", Valor = Convert.ToDecimal(1.0), Modif = false, Tic_Fecha = DateTime.Now });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "SCV", Nombre = "SIN CONVERSION", Valor = Convert.ToDecimal(1.0), Modif = false, Tic_Fecha = DateTime.Now });
                                DatosDevueltos.Add(new Entidad_Tipo_Cambio { Codigo = "OTR", Nombre = "OTROS", Valor = Convert.ToDecimal(1.0), Modif = true, Tic_Fecha = DateTime.Now });


                            }
                        }
                    }
                    return DatosDevueltos;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
