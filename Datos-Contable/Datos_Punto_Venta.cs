﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
  public  class Datos_Punto_Venta
    {

        public void Insertar(Entidad_Punto_Venta Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_PUNTO_VENTA_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Pdv_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Nombre;
                        Cmd.Parameters.Add("@Pdv_Impresora", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Impresora;
                        Cmd.Parameters.Add("@Pdv_Nombre_PC", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Nombre_PC;
                        Cmd.Parameters.Add("@Pdv_Num_Maquina_Registradora", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Num_Maquina_Registradora;
                        Cmd.Parameters.Add("@Pdv_Almacen", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Almacen;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Empresa = Cmd.Parameters["@Pdv_Codigo"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Punto_Venta Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_PUNTO_VENTA_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Pdv_Nombre", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Nombre;
                        Cmd.Parameters.Add("@Pdv_Impresora", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Impresora;
                        Cmd.Parameters.Add("@Pdv_Nombre_PC", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Nombre_PC;
                        Cmd.Parameters.Add("@Pdv_Num_Maquina_Registradora", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Num_Maquina_Registradora;
                        Cmd.Parameters.Add("@Pdv_Almacen", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pdv_Almacen;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Punto_Venta Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_PUNTO_VENTA_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.VarChar, 2).Value = Clas_Enti.Pdv_Codigo;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserID;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                            Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Punto_Venta> Listar(Entidad_Punto_Venta Cls_Enti)
        {
            List<Entidad_Punto_Venta> ListaItems = new List<Entidad_Punto_Venta>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_PUNTO_VENTA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Pdv_Codigo) ? null : Cls_Enti.Pdv_Codigo); //Cls_Enti.@Pdv_Codigo;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Est_Codigo) ? null : Cls_Enti.Est_Codigo);

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Punto_Venta
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1).Trim(),
                                    Est_Codigo = Dr.GetString(2).Trim(),
                                    Est_Descripcion = Dr.GetString(3).Trim(),
                                    Pdv_Nombre = Dr.GetString(4).Trim(),
                                    Pdv_Impresora=Dr.GetString(5).Trim(),
                                    Pdv_Nombre_PC=Dr.GetString(6).Trim(),
                                    Pdv_Num_Maquina_Registradora=Dr.GetString(7).Trim(),
                                    Pdv_Almacen=Dr.GetString(8).Trim(),
                                    Alm_Descrcipcion=Dr.GetString(9).Trim()

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Punto_Venta> Buscar_Pdv_por_PC(Entidad_Punto_Venta Cls_Enti)
        {
            List<Entidad_Punto_Venta> ListaItems = new List<Entidad_Punto_Venta>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PUNTO_VENTA_ASIGNADO_PC_BUSCAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Nombre_PC", System.Data.SqlDbType.VarChar).Value = (string.IsNullOrEmpty(Cls_Enti.Pdv_Nombre_PC) ? null : Cls_Enti.Pdv_Nombre_PC); //Cls_Enti.@Pdv_Codigo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Punto_Venta
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1),
                                    Pdv_Nombre = Dr.GetString(2).Trim(),
                                    Est_Codigo = Dr.GetString(3),
                                    Est_Descripcion = Dr.GetString(4).Trim(),
                                    Pdv_Impresora = Dr.GetString(5).Trim(),
                                    Pdv_Nombre_PC = Dr.GetString(6).Trim(),
                                    Pdv_Num_Maquina_Registradora = Dr.GetString(7),
                                    Pdv_Almacen=Dr.GetString(8),
                                    Est_Direccion=Dr.GetString(9)
                                    
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Punto_Venta> Traer_Fecha(Entidad_Punto_Venta Cls_Enti)
        {
            List<Entidad_Punto_Venta> ListaItems = new List<Entidad_Punto_Venta>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_FECHA_ACTUAL_SERVIDOR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                          Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Punto_Venta
                                {
                                    FechaActual_Servidor = Dr.GetString(0),
                                    HoraActual_Servidor = Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Punto_Venta> Listar_Punto_Venta_Almacen(Entidad_Punto_Venta Cls_Enti)
        {
            List<Entidad_Punto_Venta> ListaItems = new List<Entidad_Punto_Venta>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_PUNTO_VENTA_ALMACEN_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Pdv_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Pdv_Codigo) ? null : Cls_Enti.Pdv_Codigo); //Cls_Enti.@Pdv_Codigo;
                       // Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = (string.IsNullOrEmpty(Cls_Enti.Est_Codigo) ? null : Cls_Enti.Est_Codigo);

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Punto_Venta
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Pdv_Codigo = Dr.GetString(1),
                                    Est_Codigo = Dr.GetString(2),
                                    Est_Descripcion = Dr.GetString(3),
                                    Pdv_Nombre = Dr.GetString(4),
                                    Pdv_Impresora = Dr.GetString(5),
                                    Pdv_Nombre_PC = Dr.GetString(6),
                                    Pdv_Num_Maquina_Registradora = Dr.GetString(7),
                                    Pdv_Almacen = Dr.GetString(8),
                                    Alm_Descrcipcion = Dr.GetString(9)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
