﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
   public class Datos_Analisis_Contable
    {

        public void Insertar(Entidad_Analisis_Contable Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("CTB_ANALISI_CONTABLE_CAB_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 4).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Ana_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ana_Descripcion;
                        Cmd.Parameters.Add("@Ana_Compra", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Compra;
                        Cmd.Parameters.Add("@Ana_Venta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Venta;
                        Cmd.Parameters.Add("@Ana_Inventario", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Inventario;
                        Cmd.Parameters.Add("@Ana_Asiento_Defecto", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Asiento_Defecto;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Analisis = Cmd.Parameters["@Id_Analisis"].Value.ToString();


                        foreach (Entidad_Analisis_Contable ent in Clas_Enti.Detalles)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "CTB_ANALISI_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Analisis;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ana_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ana_Operacion_Cod;
                            Cmd.Parameters.Add("@Ana_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ana_Cuenta;
                            Cmd.Parameters.Add("@Ana_Tipo", System.Data.SqlDbType.Char, 4).Value = ent.Ana_Tipo;
                           
                    
                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Analisis_Contable Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("CTB_ANALISI_CONTABLE_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 3).Value=Clas_Enti.Id_Analisis;
                        Cmd.Parameters.Add("@Ana_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Ana_Descripcion;
                        Cmd.Parameters.Add("@Ana_Compra", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Compra;
                        Cmd.Parameters.Add("@Ana_Venta", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Venta;
                        Cmd.Parameters.Add("@Ana_Inventario", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Inventario;
                        Cmd.Parameters.Add("@Ana_Asiento_Defecto", System.Data.SqlDbType.Bit).Value = Clas_Enti.Ana_Asiento_Defecto;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
     


                        foreach (Entidad_Analisis_Contable ent in Clas_Enti.Detalles)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "CTB_ANALISI_CONTABLE_DET_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                            Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Analisis;

                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Ana_Operacion_Cod", System.Data.SqlDbType.Char, 4).Value = ent.Ana_Operacion_Cod;
                            Cmd.Parameters.Add("@Ana_Cuenta", System.Data.SqlDbType.Char, 10).Value = ent.Ana_Cuenta;
                            Cmd.Parameters.Add("@Ana_Tipo", System.Data.SqlDbType.Char, 4).Value = ent.Ana_Tipo;


                            Cmd.ExecuteNonQuery();
                        }
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Analisis_Contable Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_ANALISIS_CAB_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char,3).Value = Clas_Enti.Id_Analisis;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Analisis_Contable> Listar(Entidad_Analisis_Contable Cls_Enti)
        {
            List<Entidad_Analisis_Contable> ListaItems = new List<Entidad_Analisis_Contable>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("CTB_ANALISI_CONTABLE_CAB_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Analisis;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Analisis_Contable
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Analisis = Dr.GetString(2),
                                    Ana_Descripcion = Dr.GetString(3),
                                    Ana_Compra=Dr.GetBoolean(4),
                                    Ana_Venta=Dr.GetBoolean(5),
                                    Ana_Inventario=Dr.GetBoolean(6),
                                    Ana_Asiento_Defecto=Dr.GetBoolean(7)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Analisis_Contable> Listar_det(Entidad_Analisis_Contable Cls_Enti)
        {
            List<Entidad_Analisis_Contable> ListaItems = new List<Entidad_Analisis_Contable>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("CTB_ANALISI_CONTABLE_DET_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Analisis;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Analisis_Contable
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Analisis = Dr.GetString(2),
                                    Id_Item = Dr.GetInt32(3),
                                    Ana_Operacion_Cod = Dr.GetString(4),
                                    Ana_Operacion_Det = Dr.GetString(5),
                                    Ana_Operacion_Desc = Dr.GetString(6),
                                    Ana_Cuenta = Dr.GetString(7),
                                    Ana_Cuenta_Desc = Dr.GetString(8),
                                    Ana_Tipo=Dr.GetString(9),
                                    Ana_Tipo_Det=Dr.GetString(10),
                                    Ana_Tipo_Desc=Dr.GetString(11)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Analisis_Contable> Listar_Analisis(Entidad_Analisis_Contable Cls_Enti)
        {
            List<Entidad_Analisis_Contable> ListaItems = new List<Entidad_Analisis_Contable>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("CTB_ANALISI_CONTABLE_CAB_BUSQUEDA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Analisis;
                        Cmd.Parameters.Add("@Ana_Compra", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ana_Compra;
                        Cmd.Parameters.Add("@Ana_Venta", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ana_Venta;
                        Cmd.Parameters.Add("@Ana_Inventario", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ana_Inventario;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Analisis_Contable
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Analisis = Dr.GetString(2),
                                    Ana_Descripcion = Dr.GetString(3),
                                    Ana_Compra = Dr.GetBoolean(4),
                                    Ana_Venta = Dr.GetBoolean(5),
                                    Ana_Inventario = Dr.GetBoolean(6)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public List<Entidad_Analisis_Contable> Traer_Analisis_Defecto(Entidad_Analisis_Contable Cls_Enti)
        {
            List<Entidad_Analisis_Contable> ListaItems = new List<Entidad_Analisis_Contable>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("CTB_ANALISIS_DEFECTO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Analisis;
                        Cmd.Parameters.Add("@Ana_Venta", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ana_Venta;
                        Cmd.Parameters.Add("@Ana_Compra", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ana_Compra;
                        Cmd.Parameters.Add("@Ana_Asiento_Defecto", System.Data.SqlDbType.Bit).Value = Cls_Enti.Ana_Asiento_Defecto;

                        //@Ana_Venta BIT = NULL,
                        //@Ana_Compra BIT = NULL,
                        //@Ana_Asiento_Defecto BIT = NULL
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Analisis_Contable
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Analisis = Dr.GetString(2),
                                    Id_Item = Dr.GetInt32(3),
                                    Ana_Operacion_Cod = Dr.GetString(4),
                                    Ana_Operacion_Det = Dr.GetString(5),
                                    Ana_Operacion_Desc = Dr.GetString(6),
                                    Ana_Cuenta = Dr.GetString(7),
                                    Ana_Cuenta_Desc = Dr.GetString(8),
                                    Ana_Tipo = Dr.GetString(9),
                                    Ana_Tipo_Det = Dr.GetString(10),
                                    Ana_Tipo_Desc = Dr.GetString(11)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


    }
}
