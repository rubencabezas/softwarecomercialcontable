﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Contable
{
    public class Datos_Catalogo
    {
        public void Insertar(Entidad_Catalogo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Familia;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Direction = System.Data.ParameterDirection.Output;
                        Cmd.Parameters.Add("@Cat_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cat_Descripcion;

                        Cmd.Parameters.Add("@Cat_Serie", System.Data.SqlDbType.Char, 50).Value = Clas_Enti.Cat_Serie;
                        Cmd.Parameters.Add("@Cat_Barra", System.Data.SqlDbType.VarChar, 100).Value = Clas_Enti.Cat_Barra;
                        Cmd.Parameters.Add("@Id_Marca", System.Data.SqlDbType.Char, 3).Value = (Clas_Enti.Id_Marca == "" ? null : Clas_Enti.Id_Marca);//Clas_Enti.Id_Marca;
                        Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = (Clas_Enti.Id_Unidad_Medida == "" ? null : Clas_Enti.Id_Unidad_Medida);// Clas_Enti.Id_Unidad_Medida;
                        Cmd.Parameters.Add("@Id_Tipo_Existencia", System.Data.SqlDbType.Char, 2).Value = (Clas_Enti.Id_Tipo_Existencia == "" ? null : Clas_Enti.Id_Tipo_Existencia);

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cat_Unidades_Presentacion", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cat_Unidades_Presentacion;
                        Cmd.Parameters.Add("@Cat_Comentarios", System.Data.SqlDbType.NVarChar).Value = Clas_Enti.Cat_Comentarios;
                        Cmd.Parameters.Add("@Acepta_Lote", System.Data.SqlDbType.Bit).Value = Clas_Enti.Acepta_Lote;
                        Cmd.Parameters.Add("@Facturar_Sin_Existencia", System.Data.SqlDbType.Bit).Value = Clas_Enti.Facturar_Sin_Existencia;
                        Cmd.Parameters.Add("@Cat_Fabricante", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Fabricante;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Id_Catalogo = Cmd.Parameters["@Id_Catalogo"].Value.ToString();


                        foreach (Entidad_Catalogo ent in Clas_Enti.Detalle_especificaciones)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_CATALOGO_ESPECIFICACION_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Esp_Codigo", System.Data.SqlDbType.Char, 5).Value = ent.Esp_Codigo;
                            Cmd.Parameters.Add("@Esp_Descripcion", System.Data.SqlDbType.VarChar).Value = ent.Esp_Desc;

                            Cmd.ExecuteNonQuery();
                        }

                        foreach (Entidad_Catalogo ent in Clas_Enti.Detalle_UNM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_CATALOGO_UNM_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item_Det;
                            Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = ent.Unm_Cod_Det;
                            Cmd.Parameters.Add("@Unm_Coef_Base", System.Data.SqlDbType.Decimal).Value = ent.Unm_Base;
                            Cmd.Parameters.Add("@Unm_Defecto", System.Data.SqlDbType.Bit).Value = ent.Unm_Defecto;
                            Cmd.Parameters.Add("@Unm_Coef_Base_Unidad_Medida", System.Data.SqlDbType.VarChar).Value = ent.Unm_Coef_Base_Unidad_Medida;
                            Cmd.Parameters.Add("@Presentacion_Descripcion", System.Data.SqlDbType.VarChar).Value = ent.Presentacion_Descripcion;
                            Cmd.ExecuteNonQuery();
                        }

                        Cmd.Parameters.Clear();//INSERTAR LA COMPRA A INVENTARIO COMO ENTRADA
                        Cmd.CommandText = "pa_CTB_CATALOGO_OPERACION_VENTA";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Familia;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Id_Afecto", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Afecto_Venta;
                        Cmd.ExecuteNonQuery();

                        //ANALISIS CONTABLE VENTA, TENINEDO EN CUENTA EL AñO

                        foreach (Entidad_Catalogo ent in Clas_Enti.Detalle_Cuenta)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "PA_CTB_CATALOGO_CUENTA_CONTABLE_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = ent.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = ent.Id_Anio;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Id_Cuenta_Venta", System.Data.SqlDbType.VarChar).Value = ent.Id_Cuenta_Venta;
                            Cmd.Parameters.Add("@Id_Cuenta_Compra", System.Data.SqlDbType.VarChar).Value = ent.Id_Cuenta_Compra;
                            Cmd.ExecuteNonQuery();
                        }


                        ////ANALISIS CONTABLE  COMPRA, TENINEDO EN CUENTA EL AñO
                        //Cmd.Parameters.Clear();
                        //Cmd.CommandText = "PA_CTB_CATALOGO_ANALISIS_CONTABLE_COMPRA_INSERTAR";
                        //Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Empresa;
                        //Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Anio;
                        //Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                        //Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                        //Cmd.Parameters.Add("@Id_Analisis", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Analisis_Compra;
                        //Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Catalogo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Familia;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Cat_Descripcion", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cat_Descripcion;

                        Cmd.Parameters.Add("@Cat_Serie", System.Data.SqlDbType.Char, 50).Value = Clas_Enti.Cat_Serie;
                        Cmd.Parameters.Add("@Cat_Barra", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Cat_Barra;
                        Cmd.Parameters.Add("@Id_Marca", System.Data.SqlDbType.Char, 3).Value = (Clas_Enti.Id_Marca == "" ? null : Clas_Enti.Id_Marca);//Clas_Enti.Id_Marca;
                        Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = (Clas_Enti.Id_Unidad_Medida == "" ? null : Clas_Enti.Id_Unidad_Medida);// Clas_Enti.Id_Unidad_Medida;
                        Cmd.Parameters.Add("@Id_Tipo_Existencia", System.Data.SqlDbType.Char, 2).Value = (Clas_Enti.Id_Tipo_Existencia == "" ? null : Clas_Enti.Id_Tipo_Existencia);

                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cmd.Parameters.Add("@Cat_Unidades_Presentacion", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Cat_Unidades_Presentacion;
                        Cmd.Parameters.Add("@Cat_Comentarios", System.Data.SqlDbType.NVarChar).Value = Clas_Enti.Cat_Comentarios;
                        Cmd.Parameters.Add("@Acepta_Lote", System.Data.SqlDbType.Bit).Value = Clas_Enti.Acepta_Lote;
                        Cmd.Parameters.Add("@Facturar_Sin_Existencia", System.Data.SqlDbType.Bit).Value = Clas_Enti.Facturar_Sin_Existencia;
                        Cmd.Parameters.Add("@Cat_Fabricante", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Fabricante;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Catalogo ent in Clas_Enti.Detalle_especificaciones)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_CATALOGO_ESPECIFICACION_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item;
                            Cmd.Parameters.Add("@Esp_Codigo", System.Data.SqlDbType.Char, 5).Value = ent.Esp_Codigo;
                            Cmd.Parameters.Add("@Esp_Descripcion", System.Data.SqlDbType.VarChar).Value = ent.Esp_Desc;

                            Cmd.ExecuteNonQuery();
                        }
                        foreach (Entidad_Catalogo ent in Clas_Enti.Detalle_UNM)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_CTB_CATALOGO_UNM_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = ent.Id_Item_Det;
                            Cmd.Parameters.Add("@Id_Unidad_Medida", System.Data.SqlDbType.Char, 3).Value = ent.Unm_Cod_Det;
                            Cmd.Parameters.Add("@Unm_Coef_Base", System.Data.SqlDbType.Decimal).Value = ent.Unm_Base;
                            Cmd.Parameters.Add("@Unm_Defecto", System.Data.SqlDbType.Bit).Value = ent.Unm_Defecto;
                            Cmd.Parameters.Add("@Unm_Coef_Base_Unidad_Medida", System.Data.SqlDbType.VarChar).Value = ent.Unm_Coef_Base_Unidad_Medida;
                            Cmd.Parameters.Add("@Presentacion_Descripcion", System.Data.SqlDbType.VarChar).Value = ent.Presentacion_Descripcion;
                            Cmd.ExecuteNonQuery();
                        }

                        Cmd.Parameters.Clear();//INSERTAR LA COMPRA A INVENTARIO COMO ENTRADA
                        Cmd.CommandText = "pa_CTB_CATALOGO_OPERACION_VENTA";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Familia;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Id_Afecto", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Afecto_Venta;
                        Cmd.ExecuteNonQuery();

                        //ANALISIS CONTABLE VENTA, TENINEDO EN CUENTA EL AñO
                        foreach (Entidad_Catalogo ent in Clas_Enti.Detalle_Cuenta)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "PA_CTB_CATALOGO_CUENTA_CONTABLE_INSERTAR";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = ent.Id_Empresa;
                            Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar).Value = ent.Id_Anio;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Id_Cuenta_Venta", System.Data.SqlDbType.VarChar).Value = ent.Id_Cuenta_Venta;
                            Cmd.Parameters.Add("@Id_Cuenta_Compra", System.Data.SqlDbType.VarChar).Value = ent.Id_Cuenta_Compra;
                            Cmd.ExecuteNonQuery();
                        }


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Eliminar(Entidad_Catalogo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.Char, 2).Value = Clas_Enti.Id_Familia;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Clas_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.UserName;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar).Value = Actual_Conexion.Maquina;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Catalogo> Listar(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Grupo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Grupo;
                        Cmd.Parameters.Add("@Id_Familia", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Familia;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Grupo = Dr.GetString(1).Trim(),
                                    Gru_Descripcion = Dr.GetString(2).Trim(),
                                    Id_Familia = Dr.GetString(3).Trim(),
                                    Fam_Descripcion = Dr.GetString(4).Trim(),
                                    Id_Tipo = Dr.GetString(5).Trim(),
                                    Id_Catalogo = Dr.GetString(6).Trim(),
                                    Cat_Descripcion = Dr.GetString(7).Trim(),
                                    Cat_Serie = Dr.GetString(8).Trim(),
                                    Cat_Barra = Dr.GetString(9).Trim(),
                                    Id_Marca = Dr.GetString(10).Trim(),
                                    Mar_Descripcion = Dr.GetString(11).Trim(),
                                    Id_Unidad_Medida = Dr.GetString(12).Trim(),
                                    Und_Descripcion = Dr.GetString(13).Trim(),
                                    Id_Tipo_Existencia = Dr.GetString(14).Trim(),
                                    Exs_Nombre = Dr.GetString(15).Trim(),
                                    Cat_Unidades_Presentacion = Dr.GetDecimal(16),
                                    Cat_Comentarios = Dr.GetString(17),
                                    Und_Abreviado = Dr.GetString(18),
                                    Id_Fabricante = Dr.GetString(19),
                                    Fab_Descripcion = Dr.GetString(20),
                                    Acepta_Lote = Dr.GetBoolean(21),
                                    Facturar_Sin_Existencia = Dr.GetBoolean(22),
                                    Id_TipoCod = Dr.GetString(23),
                                    Id_TipoDescripcion = Dr.GetString(24)


                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Catalogo> Cuenta_Con_Ingresos_Egresos_Inventario(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CUENTA_CON_INGRESOS_EGRESOS_INVENTARIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Catalogo = Dr.GetString(1).Trim() 

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public List<Entidad_Catalogo> Listar_Operacion_venta(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_OPERACION_VENTA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Grupo = Dr.GetString(1).Trim(),
                                    Id_Familia = Dr.GetString(2).Trim(),
                                    Id_Tipo = Dr.GetString(3).Trim(),
                                    Id_Catalogo = Dr.GetString(4).Trim(),
                                    Id_Afecto_Venta = Dr.GetString(5).Trim(),
                                    Afecto_Venta_Descripcion = Dr.GetString(6).Trim(),
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Catalogo> Listar_Cuenta_Catalogo(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_CUENTA_CONTABLE_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1).Trim(),
                                    Id_Tipo = Dr.GetString(2).Trim(),
                                    Id_Catalogo = Dr.GetString(3).Trim(),
                                    Id_Cuenta_Venta = Dr.GetString(4).Trim(),
                                    Id_Cuenta_Compra = Dr.GetString(5).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Catalogo> Listar_Analisis_Por_Producto_COMPRA(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_ANALISIS_CONTABLE_COMPRA_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1).Trim(),
                                    Id_Tipo = Dr.GetString(2).Trim(),
                                    Id_Catalogo = Dr.GetString(3).Trim(),
                                    Id_Analisis_Compra = Dr.GetString(4).Trim(),
                                    Id_Analisis_Compra_Desc = Dr.GetString(5).Trim()
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<Entidad_Catalogo> Listar_especificaciones(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_ESPECIFICACION_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {

                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        //Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = Cls_Enti.Id_Item;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Tipo = Dr.GetString(1),
                                    Id_Catalogo = Dr.GetString(2),
                                    Id_Item = Dr.GetInt32(3),
                                    Esp_Codigo = Dr.GetString(4),
                                    Esp_Descripcion = Dr.GetString(5),
                                    Esp_Desc = Dr.GetString(6)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Catalogo> Listar_Cat_Unm(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CTB_CATALOGO_UNM_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {

                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;
                        //Cmd.Parameters.Add("@Id_Item", System.Data.SqlDbType.Int).Value = Cls_Enti.Id_Item;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Tipo = Dr.GetString(1),
                                    Id_Catalogo = Dr.GetString(2),
                                    Id_Item_Det = Dr.GetInt32(3),
                                    Unm_Cod_Det = Dr.GetString(4),
                                    Unm_Desc_Det = Dr.GetString(5),
                                    Unm_Base = Dr.GetDecimal(6),
                                    Unm_Defecto = Dr.GetBoolean(7)
                                  //,  Unm_Coef_Base_Unidad_Medida = Dr.GetString(8),
                                   // Unm_Coef_Base_Unidad_Medida_Descripcion = Dr.GetString(9)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Consultar_Stock(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_STOCK", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Codigo_Barra", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Cat_Barra == "" ? null : Cls_Enti.Cat_Barra);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa =Dr.GetString(0).Trim(),
                                    Id_Catalogo = Dr.GetString(1).Trim(),
                                    //Cat_Descripcion = Dr.GetString(1).Trim(),
                                    //Cat_Serie = Dr.GetString(2).Trim(),
                                    Stock = Dr.GetDecimal(2)
                                    //, Und_Descripcion=Dr.GetString(4),
                                    // Unm_Sunat=Dr.GetString(5),
                                    // Unm_FE=Dr.GetString(6)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Consultar_Stock_Pro_Codigo_Barra(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_STOCK_POR_CODIGO_BARRA", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Codigo_Barra", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Cat_Barra == "" ? null : Cls_Enti.Cat_Barra);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0).Trim(),
                                    Id_Catalogo = Dr.GetString(1).Trim(),
                                    //Cat_Descripcion = Dr.GetString(1).Trim(),
                                    //Cat_Serie = Dr.GetString(2).Trim(),
                                    Stock = Dr.GetDecimal(2)
                                    //, Und_Descripcion=Dr.GetString(4),
                                    // Unm_Sunat=Dr.GetString(5),
                                    // Unm_FE=Dr.GetString(6)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Catalogo> Consultar_Stock_Lote(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_STOCK_LOTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Codigo_Barra", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Cat_Barra == "" ? null : Cls_Enti.Cat_Barra);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Catalogo = Dr.GetString(0).Trim(),
                                    Cat_Descripcion = Dr.GetString(1).Trim(),
                                    Cat_Serie = Dr.GetString(2).Trim(),
                                    Stock = Dr.GetDecimal(3),
                                    Acepta_Lote = Dr.GetBoolean(4),
                                    Id_Tipo = Dr.GetString(5),
                                    Unm_Cod_Det = Dr.GetString(6),
                                    Unm_Desc_Det = Dr.GetString(7),
                                 //  Unm_Base = Dr.GetInt32(8),
                                    Und_Abreviado = Dr.GetString(8),
                                    Unm_FE = Dr.GetString(9),
                                    Mar_Descripcion = Dr.GetString(10)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Catalogo> Consultar_Servicios(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_SERVICIOS_PDV", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Codigo_Barra", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Cat_Barra == "" ? null : Cls_Enti.Cat_Barra);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Catalogo = Dr.GetString(0).Trim(),
                                    Cat_Descripcion = Dr.GetString(1).Trim(),
                                    Cat_Serie = Dr.GetString(2).Trim(),
                                    Stock = Dr.GetDecimal(3),
                                    Acepta_Lote = Dr.GetBoolean(4),
                                    Id_Tipo = Dr.GetString(5),
                                    Unm_Cod_Det = Dr.GetString(6),
                                    Unm_Desc_Det = Dr.GetString(7),
                                    //  Unm_Base = Dr.GetInt32(8),
                                    Und_Abreviado = Dr.GetString(8),
                                    Unm_FE = Dr.GetString(9),
                                    Mar_Descripcion = Dr.GetString(10)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Catalogo> Consultar_Producto_Y_Sus_Presentaciones(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PRODUCTO_POR_PRESENTACION", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Catalogo = Dr.GetString(1),
                                    Id_Tipo = Dr.GetString(2),
                                    Unm_Cod_Det = Dr.GetString(3),
                                    Unm_Desc_Det = Dr.GetString(4),
                                    Und_Abreviado = Dr.GetString(5),
                                    Unm_FE = Dr.GetString(6),
                                    Unm_Base = Dr.GetDecimal(7)

                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // Listar stock de lote POR PRDUCTO

        public List<Entidad_Catalogo> Consultar_Stock_Producto_Lote(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_CONSULTAR_STOCK_PRODUCTO_LOTE", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Almacen", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Almacen;
                        Cmd.Parameters.Add("@Invd_TipoBSA", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Invd_Catalogo", System.Data.SqlDbType.Char, 10).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        //Cmd.Parameters.Add("@Codigo_Barra", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Cat_Barra == "" ? null : Cls_Enti.Cat_Barra);
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Almacen = Dr.GetString(0).Trim(),
                                    Id_Catalogo = Dr.GetString(1).Trim(),
                                    Lot_Lote = Dr.GetString(2).Trim(),
                                    Lot_FechaFabricacion = Dr.GetDateTime(3),
                                    Lot_FechaVencimiento = Dr.GetDateTime(4),
                                    Stock_lote = Dr.GetDecimal(5)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        /////

        public List<Entidad_Catalogo> Listar_Presentacion_Productos(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("BUSCAR_PRESENTACIONES_PRODUCTO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {

                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Tipo = Dr.GetString(1),
                                    Id_Catalogo = Dr.GetString(2),
                                    Unm_Cod_Det = Dr.GetString(3),
                                    Unm_Desc_Det = Dr.GetString(4),
                                    Unm_Base = Dr.GetDecimal(5),
                                    Unm_Coef_Base_Unidad_Medida = Dr.GetString(6),
                                    Unm_Coef_Base_Unidad_Medida_Descripcion = Dr.GetString(7),
                                    Und_Abreviado = Dr.GetString(8),
                                    Unm_FE = Dr.GetString(9)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Comprobar_Producto_ya_Registrado(Entidad_Catalogo Cls_Enti)
        {
            List<Entidad_Catalogo> ListaItems = new List<Entidad_Catalogo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_PRODUCTOS_PRECIOS_YA_REGISTRADOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {

                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Est_Codigo;

                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char, 10).Value = Cls_Enti.Id_Catalogo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Catalogo
                                {
                                    Cantidad = Dr.GetInt32(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




    }
}
