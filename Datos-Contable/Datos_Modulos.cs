﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
     public   class Datos_Modulos
    {
        public List<Entidad_Modulo> Listar(Entidad_Modulo Cls_Enti)
        {
            List<Entidad_Modulo> ListaItems = new List<Entidad_Modulo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_MODULO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_modulo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_modulo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Modulo
                                {
                                    Id_modulo=Dr.GetString(0),
                                    Mod_Descripcion=Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
        public List<Entidad_Modulo> Listar_Todos_Modulos(Entidad_Modulo Cls_Enti)
        {
            List<Entidad_Modulo> ListaItems = new List<Entidad_Modulo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_MOSTRAR_MODULOS_GRUPOS_OPCIONES", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_modulo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_modulo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Modulo
                                {
                                    Id_modulo = Dr.GetString(0),
                                    Mod_Descripcion = Dr.GetString(1),
                                    Id_Grupo=Dr.GetString(2),
                                    Gru_Descripcion=Dr.GetString(3),
                                    Id_Opcion=Dr.GetString(4),
                                    Opc_Descripcion=Dr.GetString(5)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Insertar(Entidad_Modulo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_OPCION_INSERTAR", Cn) )
                    {
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        foreach(Entidad_Modulo ent in Clas_Enti.ListaOpciones)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.VarChar).Value = ent.Usuario_dni;
                            Cmd.Parameters.Add("@Id_Opcion", System.Data.SqlDbType.VarChar).Value = ent.Id_Opcion;
                                 
                        Cmd.ExecuteNonQuery();
                        }
                        foreach (Entidad_Modulo ent in Clas_Enti.ListaEmpresas)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_ADM_USUARIO_EMPRESA_INSERTAR";
                            Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.VarChar).Value = ent.Usuario_dni;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = ent.Id_Empresas;

                            Cmd.ExecuteNonQuery();
                        }

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Modificar(Entidad_Modulo Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_OPCION_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Usuario_dni;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Modulo ent in Clas_Enti.ListaOpciones)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_ADM_USUARIO_OPCION_INSERTAR";
                            Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.VarChar).Value = ent.Usuario_dni;
                            Cmd.Parameters.Add("@Id_Opcion", System.Data.SqlDbType.VarChar).Value = ent.Id_Opcion;
                            Cmd.ExecuteNonQuery();
                        }

                        foreach (Entidad_Modulo ent in Clas_Enti.ListaEmpresas)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_ADM_USUARIO_EMPRESA_INSERTAR";
                            Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.VarChar).Value = ent.Usuario_dni;
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.VarChar).Value = ent.Id_Empresas;
                            Cmd.ExecuteNonQuery();
                        }


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Modulo> Contar_Opciones_Usuario(Entidad_Modulo Cls_Enti)
        {
            List<Entidad_Modulo> ListaItems = new List<Entidad_Modulo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_OPCION_CONTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Cls_Enti.Usuario_dni;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Modulo
                                {
                                    contar=Dr.GetInt32(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Modulo> Contar_Empresas_Usuario(Entidad_Modulo Cls_Enti)
        {
            List<Entidad_Modulo> ListaItems = new List<Entidad_Modulo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_EMPRESAS_CONTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Cls_Enti.Usuario_dni;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Modulo
                                {
                                    contar_Empre = Dr.GetInt32(0)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Modulo>Opciones_Asignadas_X_Usuario(Entidad_Modulo Cls_Enti)
        {
            List<Entidad_Modulo> ListaItems = new List<Entidad_Modulo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_OPCION_BUSCAR_ASIGNADOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Cls_Enti.Usuario_dni;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Modulo
                                {
                                    Usuario_dni=Dr.GetString(0),
                                    Id_Opcion=Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Modulo>Empresas_Asignadas_X_Usuario(Entidad_Modulo Cls_Enti)
        {
            List<Entidad_Modulo> ListaItems = new List<Entidad_Modulo>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_ADM_USUARIO_EMPRESAS_BUSCAR_ASIGNADOS", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Usuario_dni", System.Data.SqlDbType.Char, 8).Value = Cls_Enti.Usuario_dni;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Modulo
                                {
                                    Usuario_dni = Dr.GetString(0),
                                    Id_Empresas = Dr.GetString(1)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
