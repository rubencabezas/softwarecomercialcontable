﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Contable;

namespace Comercial
{
     public class Datos_Lista_Precio
    {
        //public void Insertar(Entidad_Lista_Precio Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_CAB", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
        //                Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
        //                Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
        //                Cmd.Parameters.Add("@Pre_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Pre_Tipo;
        //                Cmd.Parameters.Add("@Pre_Correltivo", System.Data.SqlDbType.VarChar, 200).Direction = System.Data.ParameterDirection.Output;
        //                Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Moneda_cod;
        //                Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Pre_Fecha_Ini;
        //                Cmd.Parameters.Add("@Pre_Fecha_Fin", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Pre_Fecha_Fin;

        //                Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Id_Unm;
        //                Cmd.Parameters.Add("@Pre_Precio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Pre_Precio_Valor;
        //              Cmd.Parameters.Add("@Pre_Id_Mod_Venta", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Id_Mod_Venta;

        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Clas_Enti.Pre_Correltivo = Cmd.Parameters["@Pre_Correltivo"].Value.ToString();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        public void Insertar(Entidad_Lista_Precio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_CAB", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                        //Cmd.Parameters.Add("@Pre_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Pre_Tipo;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Lista_Precio ent in Clas_Enti.Detalles_Precios)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_LISTA_PRECIO_DET";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Est_Codigo;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Pre_Correlativo", System.Data.SqlDbType.Int).Value = ent.Pre_Correltivo;
                            Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.Char).Value = ent.Pre_Id_Unm;
                            Cmd.Parameters.Add("@Pre_Id_Mod_Venta", System.Data.SqlDbType.Char).Value = ent.Pre_Id_Mod_Venta;
                            Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.Char).Value = ent.Pre_Moneda_cod;
                            Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.DateTime).Value = ent.Pre_Fecha_Ini;
                            Cmd.Parameters.Add("@Pre_Fecha_Fin", System.Data.SqlDbType.DateTime).Value = ent.Pre_Fecha_Fin;
                            Cmd.Parameters.Add("@Pres_Es_Variable", System.Data.SqlDbType.Decimal).Value = ent.Es_Precio_Variable;
                            Cmd.Parameters.Add("@Pre_Minimo", System.Data.SqlDbType.Decimal).Value = ent.Pre_Precio_Minimo;
                            Cmd.Parameters.Add("@Pre_Maximo", System.Data.SqlDbType.Decimal).Value = ent.Pre_Precio_Maximo;
                            Cmd.Parameters.Add("@Pre_Requiere_Codigo_Autoriza", System.Data.SqlDbType.Bit).Value = ent.Pre_Requiere_Codigo_Autoriza;
                            Cmd.Parameters.Add("@Pre_Codigo_Autoriza", System.Data.SqlDbType.VarChar).Value = ent.Pre_Codigo_Autoriza;
 
                         Cmd.ExecuteNonQuery();
                        }

                        Cmd.Parameters.Clear();  
                        Cmd.CommandText = "pa_ACTUALIZA_ULTIMO_PRECIO_EN_CABECERA";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //public void Modificar(Entidad_Lista_Precio Clas_Enti)
        //{
        //    SqlTransaction Trs = null;
        //    try
        //    {
        //        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
        //        {
        //            using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_MODIFICAR", Cn))
        //            {
        //                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
        //                Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
        //                Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
        //                Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
        //                Cmd.Parameters.Add("@Pre_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Pre_Tipo;
        //                Cmd.Parameters.Add("@Pre_Correltivo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Pre_Correltivo;
        //                Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Moneda_cod;
        //                Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Pre_Fecha_Ini;
        //                Cmd.Parameters.Add("@Pre_Fecha_Fin", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Pre_Fecha_Fin;
        //                //Cmd.Parameters.Add("@Pre_Precio_Valor", System.Data.SqlDbType.Decimal).Value = Clas_Enti.Pre_Precio_Valor;
        //                Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Id_Unm;
        //                Cmd.Parameters.Add("@Pre_Id_Mod_Venta", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Id_Mod_Venta;

        //                Cn.Open();
        //                Trs = Cn.BeginTransaction();
        //                Cmd.Transaction = Trs;
        //                Cmd.ExecuteNonQuery();
        //                Trs.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        public void Modificar(Entidad_Lista_Precio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_CAB_MODIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        foreach (Entidad_Lista_Precio ent in Clas_Enti.Detalles_Precios)
                        {
                            Cmd.Parameters.Clear();
                            Cmd.CommandText = "pa_VEN_LISTA_PRECIO_DET";
                            Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Empresa;
                            Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Est_Codigo;
                            Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Tipo;
                            Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Catalogo;
                            Cmd.Parameters.Add("@Pre_Correlativo", System.Data.SqlDbType.Int).Value = ent.Pre_Correltivo;
                            Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.Char).Value = ent.Pre_Id_Unm;
                            Cmd.Parameters.Add("@Pre_Id_Mod_Venta", System.Data.SqlDbType.Char).Value = ent.Pre_Id_Mod_Venta;
                            Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.Char).Value = ent.Pre_Moneda_cod;
                            Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.DateTime).Value = ent.Pre_Fecha_Ini;
                            Cmd.Parameters.Add("@Pre_Fecha_Fin", System.Data.SqlDbType.DateTime).Value = ent.Pre_Fecha_Fin;
                            Cmd.Parameters.Add("@Pres_Es_Variable", System.Data.SqlDbType.Decimal).Value = ent.Es_Precio_Variable;
                            Cmd.Parameters.Add("@Pre_Minimo", System.Data.SqlDbType.Decimal).Value = ent.Pre_Precio_Minimo;
                            Cmd.Parameters.Add("@Pre_Maximo", System.Data.SqlDbType.Decimal).Value = ent.Pre_Precio_Maximo;
                            Cmd.Parameters.Add("@Pre_Requiere_Codigo_Autoriza", System.Data.SqlDbType.Bit).Value = ent.Pre_Requiere_Codigo_Autoriza;
                            Cmd.Parameters.Add("@Pre_Codigo_Autoriza", System.Data.SqlDbType.VarChar).Value = ent.Pre_Codigo_Autoriza;

                            Cmd.ExecuteNonQuery();
                        }

                        Cmd.Parameters.Clear();
                        Cmd.CommandText = "pa_ACTUALIZA_ULTIMO_PRECIO_EN_CABECERA";
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                        Cmd.ExecuteNonQuery();


                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public void Eliminar(Entidad_Lista_Precio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_ELIMINAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Pre_Tipo", System.Data.SqlDbType.Char, 4).Value = Clas_Enti.Pre_Tipo;
                        Cmd.Parameters.Add("@Pre_Correltivo", System.Data.SqlDbType.VarChar, 200).Value = Clas_Enti.Pre_Correltivo;
                        Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Moneda_cod;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();

                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Lista_Precio> Listar(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar).Value =  Cls_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value =  Cls_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value =  Cls_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Pre_Tipo", System.Data.SqlDbType.Char).Value =  Cls_Enti.Pre_Tipo;
                        Cmd.Parameters.Add("@Pre_Correltivo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Pre_Correltivo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Id_Empresa=Dr.GetString(0),
                                    Est_Codigo=Dr.GetString(1),
                                    Est_Descripcion=Dr.GetString(2),
                                    Id_Tipo=Dr.GetString(3),
                                    Id_Tipo_Interno=Dr.GetString(4),
                                    Id_Tipo_Descripcion=Dr.GetString(5),
                                    Id_Catalogo=Dr.GetString(6),
                                    Id_Catalogo_Desc=Dr.GetString(7),
                                    Pre_Tipo=Dr.GetString(8),
                                    Pre_Tipo_Interno=Dr.GetString(9),
                                    Pre_Tipo_Desc=Dr.GetString(10),
                                    Pre_Correltivo=Dr.GetInt32(11),
                                    Pre_Moneda_cod =Dr.GetString(12),
                                    Pre_Moneda_Sunat=Dr.GetString(13),
                                    Pre_Moneda_Desc=Dr.GetString(14),
                                    Pre_Fecha_Ini=Dr.GetDateTime(15),
                                    Pre_Fecha_Fin=Dr.GetDateTime(16),
                                    Pre_Precio_Minimo = Dr.GetDecimal(17),
                                    Pre_Id_Unm=Dr.GetString(18),
                                    Pre_Unm_Desc=Dr.GetString(19),
                                    Pre_Id_Mod_Venta=Dr.GetString(20),
                                    Pre_Mod_Venta=Dr.GetString(21),
                                    Pre_Mod_Venta_Desc=Dr.GetString(22)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Lista_Precio> Traer_Precio_Procuto(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_TRAER_PRECIO_PRODUCTO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value = (Cls_Enti.Est_Codigo == "" ? null : Cls_Enti.Est_Codigo);
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Tipo == "" ? null : Cls_Enti.Id_Tipo);
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Pre_Tipo", System.Data.SqlDbType.Char, 4).Value = (Cls_Enti.Pre_Tipo == "" ? null : Cls_Enti.Pre_Tipo);
                        Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar, 3).Value = (Cls_Enti.Pre_Moneda_cod == "" ? null : Cls_Enti.Pre_Moneda_cod);
                        Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.Date).Value = Cls_Enti.Pre_Fecha_Ini;
                        //Cmd.Parameters.Add("@Mod_Venta", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Pre_Id_Mod_Venta;


                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Id_Catalogo = Dr.GetString(0),
                                    Pre_Precio_Minimo = Dr.GetDecimal(1)
                                });
                                //ListaItems.Add(new Entidad_Lista_Precio
                                //{
                                //    Id_Catalogo=Dr.GetString(0),
                                //    Pre_Precio_Valor=Dr.GetDecimal(1),
                                //    Pre_Cantidad_Mayoreo=Dr.GetDecimal(2),
                                //    Pre_Precio_Valor_2=Dr.GetDecimal(3),
                                //    Pre_Cantidad_Mayoreo_2=Dr.GetDecimal(4),
                                //    Pre_Precio_Valor_3=Dr.GetDecimal(5),
                                //    Pre_Cantidad_Mayoreo_3=Dr.GetDecimal(6),
                                //    Pre_Precio_Valor_4=Dr.GetDecimal(7),
                                //    Pre_Cantidad_Mayoreo_4=Dr.GetDecimal(8)
                                //});
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Lista_Precio> Traer_Precio_Procuto_Presentacion(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_TRAER_PRECIO_PRODUCTO_PRESENTACION", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value = (Cls_Enti.Est_Codigo == "" ? null : Cls_Enti.Est_Codigo);
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Tipo == "" ? null : Cls_Enti.Id_Tipo);
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.Pre_Id_Unm == "" ? null : Cls_Enti.Pre_Id_Unm);
                        Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar, 3).Value = (Cls_Enti.Pre_Moneda_cod == "" ? null : Cls_Enti.Pre_Moneda_cod);
                        Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.Date).Value = Cls_Enti.Pre_Fecha_Ini;
                      

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Id_Catalogo = Dr.GetString(0),
                                    Pre_Id_Unm = Dr.GetString(1),
                                    Pre_Id_Mod_Venta=Dr.GetString(2),
                                    Es_Precio_Variable=Dr.GetBoolean(3),
                                     Pre_Precio_Minimo=Dr.GetDecimal(4),
                                   Pre_Precio_Maximo=Dr.GetDecimal(5)
                                });
                       
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Lista_Precio> Traer_Precio_Procuto_Presentacion_Por_Unm(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_TRAER_PRECIO_PRODUCTO_PRESENTACION_POR_UNM", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value = (Cls_Enti.Est_Codigo == "" ? null : Cls_Enti.Est_Codigo);
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Tipo == "" ? null : Cls_Enti.Id_Tipo);
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.Char, 3).Value = (Cls_Enti.Pre_Id_Unm == "" ? null : Cls_Enti.Pre_Id_Unm);
                        Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar, 3).Value = (Cls_Enti.Pre_Moneda_cod == "" ? null : Cls_Enti.Pre_Moneda_cod);
                        Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.Date).Value = Cls_Enti.Pre_Fecha_Ini;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Id_Catalogo = Dr.GetString(0),
                                    Pre_Id_Unm = Dr.GetString(1),
                                    Pre_Id_Mod_Venta=Dr.GetString(2),
                                    Es_Precio_Variable=Dr.GetBoolean(3),
                                     Pre_Precio_Minimo=Dr.GetDecimal(4),
                                   Pre_Precio_Maximo=Dr.GetDecimal(5)
                                });
                       
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public Entidad_Lista_Precio Valida_Codigo_Autorizacion(Entidad_Lista_Precio Clas_Enti)
        {
            Entidad_Lista_Precio Ent = new Entidad_Lista_Precio();
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_TRAER_PRECIO_PRODUCTO_PRESENTACION_POR_UNM_ACTIVAR_CODIGO_AUTORIZA", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value = (Clas_Enti.Est_Codigo == "" ? null : Clas_Enti.Est_Codigo);
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = (Clas_Enti.Id_Tipo == "" ? null : Clas_Enti.Id_Tipo);
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = (Clas_Enti.Id_Catalogo == "" ? null : Clas_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.Char, 3).Value = (Clas_Enti.Pre_Id_Unm == "" ? null : Clas_Enti.Pre_Id_Unm);
                        Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.VarChar, 3).Value = (Clas_Enti.Pre_Moneda_cod == "" ? null : Clas_Enti.Pre_Moneda_cod);
                        Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.Date).Value = Clas_Enti.Pre_Fecha_Ini;
                        Cmd.Parameters.Add("@Contrasenia", System.Data.SqlDbType.VarChar).Value = Clas_Enti.Pre_Codigo_Autoriza;
                        Cmd.Parameters.Add("@PTransac", System.Data.SqlDbType.VarChar, 50).Direction = System.Data.ParameterDirection.Output;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Ent.Respuesta = Cmd.Parameters["@PTransac"].Value.ToString().Trim();
                        Trs.Commit();
                    }
                }
                return Ent;
            }
            catch (Exception ex)
            {
                Trs.Rollback();
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Lista_Precio> Buscar_Config_Precios_Posteriores(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("Pa_VERIFICAR_CONFIGURACIONES_POSTERIORES", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar, 2).Value = (Cls_Enti.Est_Codigo == "" ? null : Cls_Enti.Est_Codigo);
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Tipo == "" ? null : Cls_Enti.Id_Tipo);
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = (Cls_Enti.Id_Catalogo == "" ? null : Cls_Enti.Id_Catalogo);
                        Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.Date).Value = String.Format("{0:yyyy-MM-dd}", Cls_Enti.Pre_Fecha_Ini);
                        Cmd.Parameters.Add("@Pre_Fecha_Fin", System.Data.SqlDbType.Date).Value = String.Format("{0:yyyy-MM-dd}", Cls_Enti.Pre_Fecha_Fin); 

                        string car = String.Format("{0:yyyy-MM-dd}", Cls_Enti.Pre_Fecha_Ini);
                        string carr = String.Format("{0:yyyy-MM-dd}", Cls_Enti.Pre_Fecha_Fin);

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Cant_Precio_Posterior = Dr.GetInt32(0)
                                });
                       
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }





        public List<Entidad_Lista_Precio> Listar_Producto_Con_Precio(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTAR_PRODUCTO_CON_PRECIO", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Catalogo;

                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Est_Codigo=Dr.GetString(1),
                                    Est_Descripcion=Dr.GetString(2),
                                    Id_Tipo = Dr.GetString(3),
                                    Id_Tipo_Interno = Dr.GetString(4),
                                    Id_Tipo_Descripcion = Dr.GetString(5),
                                    Id_Catalogo = Dr.GetString(6),
                                    Id_Catalogo_Desc = Dr.GetString(7),
                                    Pre_Fecha_Ini=Dr.GetDateTime(8),
                                    Es_Precio_Variable= Dr.GetBoolean(9),
                                    Pre_Precio_Minimo = Dr.GetDecimal(10),
                                    Pre_Precio_Maximo = Dr.GetDecimal(11),
                                    marca=Dr.GetString(12),
                                    fabricante=Dr.GetString(13)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Lista_Precio> Listar_Producto_Con_Precio_Det(Entidad_Lista_Precio Cls_Enti)
        {
            List<Entidad_Lista_Precio> ListaItems = new List<Entidad_Lista_Precio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTAR_PRODUCTO_CON_PRECIO_DET", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Id_Catalogo;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.VarChar).Value = Cls_Enti.Est_Codigo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Lista_Precio
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Est_Codigo = Dr.GetString(1),
                                    Id_Tipo = Dr.GetString(2),
                                    Id_Catalogo = Dr.GetString(3),
                                    Pre_Correltivo = Dr.GetInt32(4),
                                    Pre_Id_Unm=Dr.GetString(5),
                                    Pre_Unm_Desc=Dr.GetString(6),
                                    Pre_Id_Mod_Venta = Dr.GetString(7), 
                                    Pre_Mod_Venta = Dr.GetString(8),
                                    Pre_Mod_Venta_Desc = Dr.GetString(9),
                                    Pre_Moneda_cod=Dr.GetString(10),
                                    Pre_Moneda_Sunat = Dr.GetString(11),
                                    Pre_Moneda_Desc=Dr.GetString(12),
                                    Pre_Fecha_Ini=Dr.GetDateTime(13),
                                    Pre_Fecha_Fin=Dr.GetDateTime(14),
                                    Es_Precio_Variable=Dr.GetBoolean(15),
                                    Pre_Precio_Minimo=Dr.GetDecimal(16),
                                    Pre_Precio_Maximo=Dr.GetDecimal(17),
                                    Pre_Requiere_Codigo_Autoriza=Dr.GetBoolean(18),
                                    Pre_Codigo_Autoriza=Dr.GetString(19)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Verificar_producto_Fecha_Modalidad(Entidad_Lista_Precio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_VEN_LISTA_PRECIO_DET_VERIFICAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Est_Codigo", System.Data.SqlDbType.Char).Value = Clas_Enti.Est_Codigo;
                        Cmd.Parameters.Add("@Id_Tipo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Tipo;
                        Cmd.Parameters.Add("@Id_Catalogo", System.Data.SqlDbType.Char).Value = Clas_Enti.Id_Catalogo;
                 
                        Cmd.Parameters.Add("@Pre_Id_Unm", System.Data.SqlDbType.Char).Value = Clas_Enti.Pre_Id_Unm;
                        Cmd.Parameters.Add("@Pre_Id_Mod_Venta", System.Data.SqlDbType.Char).Value = Clas_Enti.Pre_Id_Mod_Venta;
                        Cmd.Parameters.Add("@Pre_Moneda_cod", System.Data.SqlDbType.Char).Value = Clas_Enti.Pre_Moneda_cod;
                        Cmd.Parameters.Add("@Pre_Fecha_Ini", System.Data.SqlDbType.DateTime).Value = Clas_Enti.Pre_Fecha_Ini;
                        Cmd.Parameters.Add("@Mensaje", System.Data.SqlDbType.VarChar,300).Direction = System.Data.ParameterDirection.Output;
                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                        Clas_Enti.Mensaje = Cmd.Parameters["@Mensaje"].Value.ToString();
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
