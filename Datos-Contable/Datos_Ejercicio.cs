﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Contable
{
  public  class Datos_Ejercicio
    {
        public void Insertar(Entidad_Ejercicio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_EMPRESA_ANIO_INSERTAR", Cn))
                    {
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar, 50).Value = Actual_Conexion.UserName ;
                        Cmd.Parameters.Add("@Maquina", System.Data.SqlDbType.VarChar, 50).Value = Actual_Conexion.Maquina;

                        Cn.Open();
                        Trs = Cn.BeginTransaction();
                        Cmd.Transaction = Trs;
                        Cmd.ExecuteNonQuery();
                
                        Trs.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Ejercicio> Listar(Entidad_Ejercicio Cls_Enti)
        {
            List<Entidad_Ejercicio> ListaItems = new List<Entidad_Ejercicio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_EMPRESA_ANIO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Ejercicio
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Emp_Nombre = Dr.GetString(1),
                                    Id_Anio = Dr.GetString(2)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }



        }


        public List<Entidad_Ejercicio> Listar_Periodo(Entidad_Ejercicio Cls_Enti)
        {
            List<Entidad_Ejercicio> ListaItems = new List<Entidad_Ejercicio>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                {
                    using (SqlCommand Cmd = new SqlCommand("pa_EMPRESA_EJERCICIO_LISTAR", Cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        Cmd.Parameters.Add("@Id_Empresa", System.Data.SqlDbType.Char, 3).Value = Cls_Enti.Id_Empresa;
                        Cmd.Parameters.Add("@Id_Anio", System.Data.SqlDbType.Char, 4).Value = Cls_Enti.Id_Anio;
                        Cmd.Parameters.Add("@Id_Periodo", System.Data.SqlDbType.Char, 2).Value = Cls_Enti.Id_Periodo;
                        Cn.Open();
                        using (SqlDataReader Dr = Cmd.ExecuteReader())
                        {
                            while (Dr.Read())
                            {
                                ListaItems.Add(new Entidad_Ejercicio
                                {
                                    Id_Empresa = Dr.GetString(0),
                                    Id_Anio = Dr.GetString(1),
                                    Id_Periodo=Dr.GetString(2),
                                    Descripcion_Periodo=Dr.GetString(3)
                                });
                            }
                        }
                    }
                }
                return ListaItems;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Replicacion_General(Entidad_Ejercicio Clas_Enti)
        {
            SqlTransaction Trs = null;
            try
            {
                if (Clas_Enti.Es_Plan_contable)
                {
                        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                        {
                            using (SqlCommand Cmd = new SqlCommand("pa_REPLICACION_PLAN_CONTABLE", Cn))
                            {
                                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                Cmd.Parameters.Add("@Id_EmpresaOrigen", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_AnioOrigen", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_EmpresaDestino", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa_Destino;
                                Cmd.Parameters.Add("@Id_AnioDestino", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio_Destino;

                                Cn.Open();
                                Trs = Cn.BeginTransaction();
                                Cmd.Transaction = Trs;
                                Cmd.ExecuteNonQuery();

                                Trs.Commit();
                            }
                        }
                }

                if (Clas_Enti.Es_Analisis)
                {
                    using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                        {
                            using (SqlCommand Cmd = new SqlCommand("pa_REPLICACION_ANALISIS_CONTABLE", Cn))
                            {
                                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                Cmd.Parameters.Add("@Id_EmpresaOrigen", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_AnioOrigen", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_EmpresaDestino", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa_Destino;
                                Cmd.Parameters.Add("@Id_AnioDestino", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio_Destino;

                                Cn.Open();
                                Trs = Cn.BeginTransaction();
                                Cmd.Transaction = Trs;
                                Cmd.ExecuteNonQuery();

                                Trs.Commit();
                            }
                        }
                }

                if (Clas_Enti.Es_Cuenta_Corriente)
                {
                        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                        {
                            using (SqlCommand Cmd = new SqlCommand("pa_REPLICACION_CUENTA_CORRIENTE", Cn))
                            {
                                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                Cmd.Parameters.Add("@Id_EmpresaOrigen", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_AnioOrigen", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_EmpresaDestino", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa_Destino;
                                Cmd.Parameters.Add("@Id_AnioDestino", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio_Destino;

                                Cn.Open();
                                Trs = Cn.BeginTransaction();
                                Cmd.Transaction = Trs;
                                Cmd.ExecuteNonQuery();

                                Trs.Commit();
                            }
                        }
                }
                if (Clas_Enti.Es_Parametro_inicial)
                {
                        using (SqlConnection Cn = new SqlConnection(Dat_Conexion.ObtenerConnection()))
                        {
                            using (SqlCommand Cmd = new SqlCommand("pa_REPLICACION_PARAMETROS_INICIAL", Cn))
                            {
                                Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                Cmd.Parameters.Add("@Id_EmpresaOrigen", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa;
                                Cmd.Parameters.Add("@Id_AnioOrigen", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio;
                                Cmd.Parameters.Add("@Id_EmpresaDestino", System.Data.SqlDbType.Char, 3).Value = Clas_Enti.Id_Empresa_Destino;
                                Cmd.Parameters.Add("@Id_AnioDestino", System.Data.SqlDbType.VarChar, 4).Value = Clas_Enti.Id_Anio_Destino;

                                Cn.Open();
                                Trs = Cn.BeginTransaction();
                                Cmd.Transaction = Trs;
                                Cmd.ExecuteNonQuery();

                                Trs.Commit();
                            }
                        }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

      
    }
}
