﻿Public Class DocumentoElectronicoURLWebService
    Public Shared Function URl_WebService(Optional Centralizar As Boolean = False) As String
        If Not Centralizar Then
            'URL  del web service de cada servidor de datos
            Return "http://192.168.1.32:8087/ws_documentoelectronico.asmx"
        Else
            'URL del web service del servidor central, usado en la anulacion de documentos (comunicacion de baja)
            Return "http://192.168.1.32:8087/ws_documentoelectronico.asmx"
        End If
    End Function
End Class
