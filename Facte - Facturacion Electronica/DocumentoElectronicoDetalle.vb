﻿'Partial 
Public Class DocumentoElectronicoDetalle
    Property Item As Integer 'BOL(29):Obligatorio --//Invoice/cac:InvoiceLine/cbc:ID
    '//Invoice/cac:InvoiceLine/cac:Item/cac:SellersItemIdentification/cbc:ID
    Property Producto_Codigo As String 'BOL(30):Opcional 'BOL:Obligatorio en caso consigna codigo en vez de descripcion detallada
    '*Puede poner serie/numero , partidas arancelarias o codigo de existencia (LLevado de libros y registros Ple)
    '//Invoice/cac:InvoiceLine/cac:Item/cbc:Description
    Property Producto_Descripcion As String 'BOL(13):Obligatorio --//Invoice/cac:InvoiceLine/cac:Item/cbc:Description
    '*No será necesario colocar la unidad de medida si ésta es “NIU”(unidad) 0 “ZZ”
    '//Invoice/cac:InvoiceLine/cbc:InvoicedQuantity
    Property Producto_UnidadMedida_Codigo As String 'BOL(11):Obligatorio --//Invoice/cac:InvoiceLine/cbc:InvoicedQuantity
    Property Producto_Cantidad As Double 'BOL(12):Obligatorio --//Invoice/cac:InvoiceLine/cbc:InvoicedQuantity
    '//Invoice/cac:InvoiceLine/cac:Price/cbc:PriceAmount
    Property Unitario_Valor_Unitario As Double 'BOL(31):Obligatorio  --//Invoice/cac:InvoiceLine/cac:Price/cbc:PriceAmount

    Property Item_ValorVenta_Total As Decimal 'BOL(32):Obligatorio (Q x Valor Unitario) --//Invoice/cac:InvoiceLine/cbc:LineExtensionAmount
    '//Invoice/cac:InvoiceLine/cbc:LineExtensionAmount
    'Property Unitario_Valor_Unitario_PorItem As Double 'BOL:Obligatorio (Q x valor unitario)

    '*Se pone cuando la transferencia de bienes se efectue gratuitamente
    '//Invoice/cac:InvoiceLine/cac:PricingReference/cac:AlternativeConditionPrice
    'Property Unitario_Valor_Referencial_PorItem As Double 'BOL:Opcional

    '*Por cada bien o servicio. Esto incluye los tributos (IGV, ISC y otros Tributos) y la deducción de descuentos por ítem.
    '//Invoice/cac:InvoiceLine/cac:PricingReference/cac:AlternativeConditionPrice
    Property Uniatrio_Valor_Referencial As Double 'BOL(33):Opcional --//Invoice/cac:InvoiceLine/cac:PricingReference/cac:AlternativeConditionPrice
    Property Uniatrio_Valor_Referencial_Tipo_Codigo As String = "02"
    Property Unitario_Precio_Venta As Double 'BOL(14):Obligatorio --//Invoice/cac:InvoiceLine/cac:PricingReference/cac:AlternativeConditionPrice
    Property Unitario_Precio_Venta_Tipo_Codigo As String = "01" 'BOL:Obligatorio
    '--1000-IGV-VAT
    Property Item_Tipo_Afectacion_IGV_Codigo As String 'BOL(27):Obligatorio --Catalogo Nº 7
    Property Item_IGV_Total As Decimal
    Property Item_Total As Double
    '--2000-ISC-EXC
    Property Item_ISC_Total As Double 'BOL(28):Opcional ---//Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:TierRange
    Property Unitario_ISC_Codigo As String 'Cat 08
    Property Unitario_IGV_Porcentaje As Double

    '//Invoice/cac:InvoiceLine/cac:Allowancecharge/cbc:chargeIndicator
    'AllowanceCharge: Opcional. Descuentos aplicados a los ítems facturados en la línea.
    'ChargeIndicator: Obligatorio. Si es descuento (False).
    'Amount: Monto del descuento del ítem .Se debe especificar la moneda en la que se emite el descuento, para ello se utiliza el atributo currencyID.
    Property Unitario_Descuento As Double 'BOL(42):Opcional //Invoice/cac:InvoiceLine/cac:Allowancecharge/cbc:chargeIndicator
    Property Numero_Placa_del_Vehiculo As String 'Gastos art. 37 Renta

    'Para la impresion de la marca

    Property Marca_Descripcion As String

    Property Producto_UnidadMedida_Descripcion As String

    'DESCUENTO
    Property Unitario_Descuento_Codigo_Sunat As String
    Property Unitario_Descuento_Factor_Cargo_Dsto As Double
    Property Unitario_Descuento_Monto_Cargo_Dsto As Double 'BOL(42):Opcional //Invoice/cac:InvoiceLine/cac:Allowancecharge/cbc:chargeIndicator
    Property Unitario_Descuento_Monto_Base_Cargo_Dsto As Double
    ''''''//

End Class
