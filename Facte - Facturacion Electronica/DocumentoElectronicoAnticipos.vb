﻿Public Class DocumentoElectronicoAnticipos
    Property Anticipo_Moneda_Codigo As String
    Property Anticipo_Monto As Double
    Property Anticipo_Documento_Tipo As String
    Property Anticipo_Documento_Serie As String
    Property Anticipo_Documento_Numero As String
    Property Anticipo_Emisor_Documento_Numero As String
    Property Anticipo_Emisor_Documento_Tipo As String
End Class
