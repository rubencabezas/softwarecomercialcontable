﻿Public Class DocumentoElectronico

    Property Empresa As String
    Property Origen As String
    Property Anio As String
    Property Periodo As String
    Property Tipo_Doc_Sistema As String
    'Emisor
    'RegistrationName
    Property Emisor_ApellidosNombres_RazonSocial As String 'BOL(02):Obligatorio
    '//Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyName/cbc:Name
    Property Emisor_NombreComercial As String 'BOL(03):Opcional --(Oblig en caso declarado en ruc) --//Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyName/cbc:Name
    'CustomerAssignedAccountID
    Property Emisor_Documento_Numero As String 'BOL(5)1:Obligatorio --//Invoice/cac:AccountingSupplierParty/cbc:CustomerAssignedAccountID
    'AdditionalAccountID
    Property Emisor_Documento_Tipo As String 'BOL(05)2:Obligatorio --//Invoice/cac:AccountingSupplierParty/cbc:AdditionalAccountID
    'Domicilio Fiscal BOL:Opcional
    '//Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress
    Property Emisor_Direccion_Ubigeo As String 'BOL(04)1:Opcional(04) --cbc:ID: Ubicación Geográfica (UBIGEO)
    Property Emisor_Direccion_Calle As String 'BOL(04)2:Opcional(04) --cbc:StreetName: Dirección completa y detallada.
    Property Emisor_Direccion_Urbanizacion As String 'BOL(04)3:Opcional(04) --cbc:CitySubdivisionName: Urbanización o Zona.
    Property Emisor_Direccion_Departamento As String 'BOL(04)4:Opcional(04) --cbc:CityName: Departamento.
    Property Emisor_Direccion_Provincia As String 'BOL(04)5:Opcional(04) --cbc:CountrySubentity: Provincia.
    Property Emisor_Direccion_Distrito As String 'BOL(04)6:Opcional(04) --cbc:District: Distrito.
    Property Emisor_Direccion_PaisCodigo As String 'BOL(04)7:Opcional(04) --"PE" 'cac:Country:cbc:IdentificationCode:
    '//Invoice/cbc:InvoiceTypeCode

    <Xml.Serialization.XmlIgnore()>
    ReadOnly Property Emisor_Direccion_Calle_Urbanizacion As String
        Get
            Return Emisor_Direccion_Calle & " " & Emisor_Direccion_Urbanizacion
        End Get
    End Property
    <Xml.Serialization.XmlIgnore()>
    ReadOnly Property Emisor_Direccion_Departamento_Provincia_Distrito As String
        Get
            Return Emisor_Direccion_Departamento & " - " & Emisor_Direccion_Provincia & " - " & Emisor_Direccion_Distrito
        End Get
    End Property
    <Xml.Serialization.XmlIgnore()>
    ReadOnly Property Emisor_Direccion_Calle_Urbanizacion_Departamento_Provincia_Distrito As String
        Get
            Return Emisor_Direccion_Calle_Urbanizacion & " " & Emisor_Direccion_Departamento_Provincia_Distrito
        End Get
    End Property

    <Xml.Serialization.XmlIgnore()>
    ReadOnly Property Emisor_Telefonos_Imprimir As String
        Get
            Return "Teléfonos: 043-423414 / 043-423426"
        End Get
    End Property
    '<Xml.Serialization.XmlIgnore()>
    Property Cajero_Nombre As String
    Property Vendedor_Nombre As String
    Property TipoDocumento_Codigo As String 'BOL(06):Obligatorio --//Invoice/cbc:InvoiceTypeCode
    '<Xml.Serialization.XmlIgnore()> 'se necesita para el HTML, si ignoro
    Property TipoDocumento_Descripcion As String
    Property TipoDocumento_Serie As String 'BOL(07)1:Obligatorio --//Invoice/cbc:ID
    Property TipoDocumento_Numero As String 'BOL(07)2:Obligatorio --//Invoice/cbc:ID
    ReadOnly Property TipoDocumento_SerieGuionNumero As String
        Get
            Return TipoDocumento_Serie & "-" & TipoDocumento_Numero
        End Get
    End Property
    Property Fecha_Emision As Date
    Property Hora_Emision As DateTime 'BOL(01):Obligatorio ---cbc:IssueDate
    Property Fecha_Vencimiento As Date

    Property Moneda_Codigo As String 'BOL(24):Obligatorio   --//Invoice/cbc:DocumentCurrencyCode
    Property Moneda_Descripcion As String
    Property Moneda_Simbolo As String
    '*Existen casos: > S/ 700.00 ó > S/ 350.00 (Región selva) ó US$ 25.00 (Zona Comercial Tacna)
    '*En los demas casos solo se coloca "-"
    '//Invoice/cac:AccountingCustomerParty/cbc:CustomerAssignedAccountID
    '//Invoice/cac:AccountingCustomerParty/cbc:AdditionalAccountID
    '//Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName
    Property Cliente_Documento_Tipo As String 'BOL(08)1:Obligatorio ---//Invoice/cac:AccountingCustomerParty/cbc:AdditionalAccountID
    Property Cliente_Documento_Numero As String 'BOL(08)1:Obligatorio --/Invoice/cac:AccountingCustomerParty/cbc:CustomerAssignedAccountID
    Property Cliente_RazonSocial_Nombre As String 'BOL(09):Obligatorio --//Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName
    '*Obligatorio en caso > US$ 25.00 (Zona Comercial Tacna)
    '//Invoice/cac:AccountingCustomerParty/cac:Party/cac:PhysicalLocation/cbc:Description
    Property Cliente_Direccion As String 'BOL(10):Opcional  --//Invoice/cac:AccountingCustomerParty/cac:Party/cac:PhysicalLocation/cbc:Description
    Property Cliente_Correo As String '--Para actualizar el correo

    'BOL: Opcionales - La boleta debe tener al menos uno de estos: 15, 16, 17
    '//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    Property Total_ValorVenta_OperacionesGravadas As Double 'BOL(15):Opcional  --//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    'Property Total_ValorVenta_OperacionesGravadas_Codigo As String = "1001"
    Property Total_ValorVenta_OperacionesInafectas As Double 'BOL(16):Opcional --//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    'Property Total_ValorVenta_OperacionesInafectas_Codigo As String = "1002"
    Property Total_ValorVenta_OperacionesExoneradas As Double 'BOL(17):Opcional --//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    'Property Total_ValorVenta_OperacionesExoneradas_Codigo As String = "1003"
    Property Total_ValorVenta_OperacionesGratuitas As Double 'BOL(40):Opcional
    'Property Total_ValorVenta_OperacionesExoneradas_Codigo As String = "1004"

    '//Invoice/cac:TaxTotal/cbc:TaxAmount
    Property Total_IGV As Double 'BOL(18):Opcional --//Invoice/cac:TaxTotal/cbc:TaxAmount
    'Property Sumatoria_IGV_Codigo As String = "1000"
    'Property Sumatoria_IGV_Descripcion As String = "IGV"
    'Property Sumatoria_IGV_TipoCodigo As String = "VAT"
    Property IGV_Porcentaje As Double
    '//Invoice/cac:TaxTotal/cbc:TaxAmount
    Property Total_ISC As Double 'BOL(19):Opcional  -//Invoice/cac:TaxTotal/cbc:TaxAmount
    'Property Sumatoria_ISC_Codigo As String = "2000"
    'Property Sumatoria_ISC_Descripcion As String = "ISC"
    'Property Sumatoria_ISC_TipoCodigo As String = "EXC"

    '//Invoice/cac:TaxTotal/cbc:TaxAmount
    Property Total_OtrosTributos As Double 'BOL(20):Opcional --//Invoice/cac:TaxTotal/cbc:TaxAmount
    'Property Sumatoria_ISC_Codigo As String = "9999"
    'Property Sumatoria_ISC_Descripcion As String = "OTROS"
    'Property Sumatoria_ISC_TipoCodigo As String = "OTH"

    '*propinas, garantías para devolución de envases,etc
    '*Se define como el total de todos los cargos aplicados a nivel de total de la boleta de venta.
    '//Invoice/cac:LegalMonetaryTotal/cbc:ChargeTotalAmount
    Property Total_OtrosCargos As Double 'BOL(21):Opcional --//Invoice/cac:LegalMonetaryTotal/cbc:ChargeTotalAmount

    '*Descuentos de linea + descuentos globales
    '//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    'Property Descuentos_Globales As Double 'BOL(41):Opcional --//Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount
    Property Total_Descuentos As Double 'BOL(22):Opcional ---//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    'Property Sumatoria_Descuentos_Codigo As String = "2005"

    '* 15+16+17+18+19+20+21
    '//Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount
    Property Total_Importe_Venta As Double 'BOL(23):Obligatorio ---/Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount

    Property Total_Importe_Venta_Texto As String 'Solo para impresión


    '<System.Xml.Serialization.XmlElementAttribute("Detalles")> _
    <Xml.Serialization.XmlArrayItem("Detalle")>
    Property Detalles() As DocumentoElectronicoDetalle() 'BOL(11-14)(27-33)(42)


    '<System.Xml.Serialization.XmlElementAttribute("GuiasRemision")> _
    <Xml.Serialization.XmlArrayItem("Guia")>
    Property GuiasRemision As DocumentoElectronicoReferenciaDocumentos() 'BOL(25):Opcional

    'Leyendas
    '1000 - Monto expresado en letras   BOL:Opcional
    '1002 - “Transferencia gratuita” o “Servicio prestado Gratuitamente”   BOL:Opcional
    '2000 - "COMPROBANTE DE PERCEPCION”   BOL:Opcional
    '2001 - “Bienes transferidos en la Amazonía…"   BOL:Opcional
    '2002 - “Servicios prestados en la Amazonía…"   BOL:Opcional
    '2003 - “Contratos de construcción ejecutados en la Amazonía…”   BOL:Opcional
    <Xml.Serialization.XmlArrayItem("Leyenda")>
    Property Leyendas As DocumentoElectronicoLeyenda() '26

    'Importe de la percepción en moneda nacional “2001” catalogo 14
    '//Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal
    Property Total_Importe_Percepcion As Double 'BOL(34):Opcional

    '*Distinto al elemento Total Descuentos definido en el punto 22
    '//Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount

    'Property Otro_Doc_Relacionado_Codigo As String 'BOL(35)1:Opcional --cac:AdditionalDocumentReference/cbc:ID
    'Property Otro_Doc_Relacionado_Tipo As String 'BOL(35)2:Opcional -- cac:AdditionalDocumentReference/cbc:DocumentTypeCode

    <Xml.Serialization.XmlArrayItem("DocumentoAfectado")>
    Property DocumentoAfectados As DocumentoElectronicoReferenciaDocumentos()

    <Xml.Serialization.XmlArrayItem("DocumentoRelacionado")>
    Property DocumentoRelacionados As DocumentoElectronicoReferenciaDocumentos() 'BOL(35):Opcional

    <Xml.Serialization.XmlArrayItem("CampoPerzonalizado")>
    Property CamposPerzonalizados As DocumentoElectronicoReferenciaDocumentos() 'Solo para las empresas que desean cargar campos perzonalizados

    Property RazonAnulacion As String
    Property Numero_Placa_del_Vehiculo As String 'Gastos art. 37 Renta

    Property Modif_Doc_Tipo As String
    Property Modif_Doc_Serie As String
    Property Modif_Doc_Numero As String

    <Xml.Serialization.XmlArrayItem("Anticipo")>
    Property DocumentoAnticipos As DocumentoElectronicoAnticipos() '
    Property Total_Anticipos As Double
    Property Tipo_Operacion_Codigo As String 'Cat 17 --05 Venta itinerante, 06 factura guia, 08 factura comprobante percepcion , Venta interna, exportacion, no domiciliados, etc
    'Esto es para los datos de venta itinerante
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo As String '(UBIGEO)
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_Calle As String 'StreetName: Dirección completa y detallada.
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_Urbanizacion As String 'CitySubdivisionName: Urbanización o Zona.
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_Provincia As String 'CityName: Provincia.
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_Departamento As String 'CountrySubentity: Departamento.
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_Distrito As String 'District: Distrito.
    Property Adic_Lugar_Entrega_Bien_PrestaServicio_PaisCodigo As String 'Country:cbc:IdentificationCode: Pais

    Property GuiaFactura_Documento_Tipo_Codigo As String
    Property GuiaFactura_Documento_Serie As String
    Property GuiaFactura_Documento_Numero As String
    Property GuiaFactura_RemitenteDocumentoOriginal_NumDoc As String
    '-------------------------------------------------------
    '-----
    Property Tercero_Documento_Tipo As String
    Property Tercero_Documento_Numero As String
    Property Tercero_RazonSocial_Nombre As String

    Property Observaciones As String
    Property MotivoTraslad_Codigo As String 'Motivo traslado'HandlingCode
    Property MotivoTraslad_Detalle As String 'Motivo traslado'Information
    Property IndicTransProg_Codigo As String = "00" 'Indicador del transbordo programado 'SplitConsignmentIndicator

    Property PesoBrutoTotalGuia_UnidMed As String 'GrossWeightMeasure
    Property PesoBrutoTotalGuia_Valor As Double
    Property NumeroBultosoPallets As Integer
    Property ModalidadTraslado_Codigo As String
    Property ModalidadTraslado_Descripcion As String
    Property Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista As Date
    Property Transportista_Documento_Tipo As String
    Property Transportista_Documento_Numero As String
    Property Transportista_RazonSocial_Nombre As String
    Property Conductor_Documento_Tipo As String
    Property Conductor_Documento_Numero As String
    Property Numero_Contenedor As String
    'Property Transporte_Placa As String
    Property Transportes As DocumentoElectronicoTransporte()
    Property Contenedor_Numero As String
    Property Salida_Ubigeo As String
    Property Salida_Direccion As String
    'Property Salida_Urbanizacion As String '-185-2015 - No para 255-2015
    'Property Salida_Distrito As String '-185-2015 - No para 255-2015
    'Property Salida_Provincia As String '-185-2015 - No para 255-2015
    'Property Salida_Departamento As String '-185-2015 - No para 255-2015
    'Property Salida_Pais As String '-185-2015 - No para 255-2015
    Property Llegada_Ubigeo As String
    Property Llegada_Direccion As String
    'Property Llegada_Urbanizacion As String '-185-2015 - No para 255-2015
    'Property Llegada_Distrito As String '-185-2015 - No para 255-2015
    'Property Llegada_Provincia As String '-185-2015 - No para 255-2015
    'Property Llegada_Departamento As String '-185-2015 - No para 255-2015
    'Property Llegada_Pais As String '-185-2015 - No para 255-2015
    Property PuertoAeropuerto_EmbarqueDesembarque_Codigo As String

    Property Retenc_Tipo_Codigo As String
    Property Retenc_Porcentaje As Double
    Property Retenc_Importe_Total_Retenido As Double
    Property Retenc_Importe_Total_Pagado As Double

    Property Percep_Tipo_Codigo As String
    Property Percep_Porcentaje As Double
    Property Percep_Importe_Total_Percibido As Double
    Property Percep_Importe_Total_Cobrado As Double

    Property Impresion_Modelo_Codigo As String

    'Adjuntos --Una vez enviado elimina los archivos del disco
    '<System.Xml.Serialization.XmlIgnore()> _
    'ReadOnly Property ServidorRuta_Adjuntos As String
    '    Get
    '        Dim var As String = ""
    '        var = RutasServidor.Root_Adjuntos & TipoDocumento_Descripcion & "\" & TipoDocumento_Serie & TipoDocumento_Numero
    '        Return var
    '    End Get
    'End Property
    <System.Xml.Serialization.XmlIgnore()>
    Property SQL_Retorno As String
    <System.Xml.Serialization.XmlIgnore()>
    Property SQL_XML_Nombre As String
    <System.Xml.Serialization.XmlIgnore()>
    Property SQL_XML_Correlativo As Integer

    '------Para almacenar los archivos XML
    <Xml.Serialization.XmlIgnore()>
    Property RootPath_Files As String
    <Xml.Serialization.XmlIgnore()>
    ReadOnly Property RootPath_Enterprise As String
        Get
            Return RootPath_Files & "\" & Emisor_Documento_Numero
        End Get
    End Property
    '---------Digest Value
    <Xml.Serialization.XmlIgnore()>
    Property Print_DigestValue As String
    '---------SignatureValue Value
    <Xml.Serialization.XmlIgnore()>
    Property Print_SignatureValue As String
    '---------Codigo Barras
    <Xml.Serialization.XmlIgnore()>
    Property Print_BarCode As String

    <Xml.Serialization.XmlIgnore()>
    Property Print_BarCode_Armar As String
        Get
            '"RUC_EMISOR | TIPO DE COMPROBANTE PAGO | SERIE | NUMERO | SUMATORIA IGV | MONTO TOTAL DEL COMPROBANTE | FECHA DE EMISION | TIPO DE DOCUMENTO ADQUIRENTE | NUMERO DE DOCUMENTO ADQUIRENTE | VALOR RESUMEN (DigestValue) | VALOR DE LA FIRMA (SignatureValue) |"
            Return String.Format("{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10}|", Emisor_Documento_Numero, TipoDocumento_Codigo, TipoDocumento_Serie, TipoDocumento_Numero, Format(Total_IGV, "#0.00"), Format(Total_Importe_Venta, "#0.00"), Fecha_Emision.ToShortDateString(), Cliente_Documento_Tipo, Cliente_Documento_Numero, Print_DigestValue, Print_SignatureValue)
        End Get
        Set(value As String)

        End Set
    End Property
    '-----Tipo --emision - actualizacion - baja
    <Xml.Serialization.XmlIgnore()>
    Property Doc_XML_Tipo As String
    '-----Estado --sin enviar - aceptado - rechazado
    <Xml.Serialization.XmlIgnore()>
    Property Doc_XML_Estado As String
    <Xml.Serialization.XmlIgnore()>
    Property Doc_XML_SUNAT_Resp_Code As String
    <Xml.Serialization.XmlIgnore()>
    Property Doc_XML_SUNAT_Resp_Description As String
    '-----Nombre de XML SUNAT
    <Xml.Serialization.XmlIgnore()>
    Property Doc_SUNAT_NombreArchivoXML As String
    '-----WEB
    <Xml.Serialization.XmlIgnore()>
    Property Doc_NombreArchivoWeb As String

    Property Doc_XML_Dias_CDR_Aceptado As Integer
    '<Xml.Serialization.XmlIgnore()>
    'ReadOnly Property ActionModel As String
    '    Get
    '        If Emisor_Documento_Tipo = SUNATDocumento.Boleta Then
    '            Return "Invoice"
    '        ElseIf Emisor_Documento_Tipo = SUNATDocumento.Factura Then
    '            Return "Invoice"
    '        ElseIf Emisor_Documento_Tipo = SUNATDocumento.NotaCredito Then
    '            Return "CreditNote"
    '        ElseIf Emisor_Documento_Tipo = SUNATDocumento.NotaDebito Then
    '            Return "DebitNote"
    '        ElseIf Emisor_Documento_Tipo = SUNATDocumento.GuiaRemisionRemitente Then
    '            Return "DespatchAdvice"
    '        ElseIf Emisor_Documento_Tipo = SUNATDocumento.ComprobantePercepcion Then
    '            Return "Perception"
    '        ElseIf Emisor_Documento_Tipo = SUNATDocumento.ComprobanteRetencion Then
    '            Return "Retention"
    '        End If
    '    End Get
    'End Property
    '<Xml.Serialization.XmlIgnore()>
    'ReadOnly Property PostIDActionModel As String
    '    Get
    '        Return (ActionModel & "/" & Doc_NombreArchivoWeb)
    '    End Get
    'End Property
    Property Sunat_Autorizacion As String
    Property Emisor_Web_Visualizacion As String
    Property RepresentacionImpresaDela As String

    Property Forma_De_Pago As String
    'Homologacion
    <Xml.Serialization.XmlIgnore()>
    Property Homogacion_Grupo_Codigo As String
    <Xml.Serialization.XmlIgnore()>
    Property Homogacion_Grupo_Descripcion As String
    <Xml.Serialization.XmlIgnore()>
    Property Homogacion_Grupo_Operacion As String
    <Xml.Serialization.XmlIgnore()>
    Property PagoTarjeta() As DocumentoElectronicoPagoTarjeta()
    <Xml.Serialization.XmlIgnore()>
    Property Emisor_Establecimiento As String
    <Xml.Serialization.XmlIgnore()>
    Property Emisor_Establecimiento_Direccion As String

    <Xml.Serialization.XmlIgnore()>
    ReadOnly Property Total_ValorVenta_Total As Double
        Get
            Return Total_ValorVenta_OperacionesExoneradas + Total_ValorVenta_OperacionesInafectas + Total_ValorVenta_OperacionesGratuitas + Total_ValorVenta_OperacionesGravadas
        End Get
    End Property

End Class
