﻿Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml
Imports System.Xml
Imports System.Xml.Serialization
Imports UblLarsen.Ubl2.Cac
Imports UblLarsen.Ubl2.Ext
Imports UblLarsen.Ubl2.Qdt
Imports UblLarsen.Ubl2.Udt

Public Class Doc_Generar_Xml
    Shared appPassword As String = System.Configuration.ConfigurationSettings.AppSettings("RootPathFull")

    Shared RootPath_Enterprise As String = System.Configuration.ConfigurationSettings.AppSettings("Ruta_Certificado")
    Shared Ruta_Generado As String = System.Configuration.ConfigurationSettings.AppSettings("Ruta_Generado")
    Shared Credenciales_Contrasena As String = System.Configuration.ConfigurationSettings.AppSettings("Certificado_Pass")
    Shared Credenciales_Ruta As String = System.Configuration.ConfigurationSettings.AppSettings("Certificado_Ruta")

    Shared Ruta_GeneradoBaja As String = System.Configuration.ConfigurationSettings.AppSettings("Ruta_GeneradoBaja")

    Dim RootPathFull As String = "D:\WebAppsFiles"

    Public Function Documento_RegistrarActualizar(xmlDoc As XmlDocument) As String


        Try
            Dim Provision As New DocumentoElectronico
            Dim reader As New System.Xml.Serialization.XmlSerializer(Provision.[GetType]())
            Dim reader2 As XmlNodeReader = New XmlNodeReader(xmlDoc)

            Provision = DirectCast(reader.Deserialize(reader2), DocumentoElectronico)

            Provision.RootPath_Files = RootPathFull

            ''''''
            'Aqui se guarda el archivo generado el XML con parametros
            Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
            Dim xmlFilename_V1 As String = Filename & "_V1" & ".XML"
            Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename_V1

            Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
            Using writer As XmlWriter = XmlWriter.Create(Ruta_Generado & "\" & xmlFilename_V1, settings)
                Dim typeToSerialize As Type = GetType(DocumentoElectronico)
                Dim xs As New Serialization.XmlSerializer(typeToSerialize)
                xs.Serialize(writer, Provision)
            End Using
            ''''''
            ''''''


            Dim nProceso As fcProceso
            nProceso = Generar_BoletaVentaFactura(Provision)

            If nProceso.ArchivoCreadoCorrectamente Then

                XMLFirmar(Provision, nProceso)

                'Actualizar a estado :Sin Enviar-4105
                'Dim log As New Log_VTS_DOC_TRANSAC_CAB
                'Dim ent As New Ent_DOC_TRANSAC_CAB
                'ent.MyOrigen = Provision.Origen
                'ent.Emp_cCodigo = Provision.Empresa
                'ent.Pan_cAnio = Provision.Anio
                'ent.Per_cPeriodo = Provision.Periodo
                'ent.TpDc_Codigo = Provision.Tipo_Doc_Sistema
                'ent.Dvt_VTSerie = Provision.TipoDocumento_Serie
                'ent.Dvt_VTNumer = Provision.TipoDocumento_Numero
                'ent.Vnt_FactElectronica_Estado = "4105"
                'If log.Actualizar_Estado_Xml(ent) Then

                'End If
                ''''

            End If

            'Provision.SQL_XML_Correlativo
            Return Str(1) & " # " & Provision.Doc_NombreArchivoWeb & " # " & Provision.Print_DigestValue & " # " & Provision.Print_BarCode_Armar & " # - # Guardado Correctamente"


        Catch ex As Exception

        End Try


    End Function


    Sub XMLFirmar(Provision As DocumentoElectronico, pProceso As fcProceso)

        Try
            ' Vamos a firmar el XML con la ruta del certificado que está como serializado.

            Dim certificate = New X509Certificate2()
            certificate.Import(Credenciales_Ruta, Credenciales_Contrasena, X509KeyStorageFlags.MachineKeySet)

            Dim xmlDoc = New XmlDocument()
            xmlDoc.PreserveWhitespace = True
            xmlDoc.Load(Ruta_Generado & "\" & pProceso.FileNameXML) 'Provision.RootPath_Enterprise & "\xml_temp\" & pProceso.FileNameXML

            Dim nodoExtension = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(pProceso.ExtensionContent_Indice)
            'Dim nodoExtension = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(pProceso.ExtensionContent_Indice) '1 - 2 nodos, 0 un solo nodo
            If nodoExtension Is Nothing Then
                Throw New InvalidOperationException("No se pudo encontrar el nodo ExtensionContent en el XML")
            End If
            nodoExtension.RemoveAll()

            Dim signedXml = New SignedXml(xmlDoc) With {
            .SigningKey = DirectCast(certificate.PrivateKey, System.Security.Cryptography.RSA)
        }
            signedXml.SigningKey = certificate.PrivateKey
            Dim xmlSignature = signedXml.Signature

            Dim env = New XmlDsigEnvelopedSignatureTransform()

            Dim reference = New Reference(String.Empty)
            reference.AddTransform(env)
            xmlSignature.SignedInfo.AddReference(reference)

            Dim keyInfo = New KeyInfo()
            Dim x509Data = New KeyInfoX509Data(certificate)

            x509Data.AddSubjectName(certificate.Subject)
            keyInfo.AddClause(x509Data)
            xmlSignature.KeyInfo = keyInfo
            xmlSignature.Id = "SIGEDGO"

            signedXml.ComputeSignature()

            If reference.DigestValue IsNot Nothing Then
                Provision.Print_DigestValue = Convert.ToBase64String(reference.DigestValue)
            End If

            Provision.Print_SignatureValue = Convert.ToBase64String(signedXml.SignatureValue)

            nodoExtension.AppendChild(signedXml.GetXml()).Prefix = "ds"

            xmlDoc.Save(Ruta_Generado & "\" & pProceso.FileNameXML) 'Provision.RootPath_Enterprise & "\xml_temp\" & pProceso.FileNameXML

            pProceso.ArchivoFirmadoCorrectamente = True
        Catch ex As Exception
            pProceso.ArchivoFirmadoCorrectamente = False
        End Try


    End Sub


    Function Generar_BoletaVentaFactura(Provision As DocumentoElectronico) As fcProceso

        Try
            Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
            'ProcesoCompleto.ServidorRutaLocal = 'Provision.RootPath_Enterprise
            ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

            Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
            Dim xmlFilename As String = Filename & ".XML"
            Dim ZipFilename As String = Filename & ".ZIP"
            Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename


            Dim invoice As UblLarsen.Ubl2.InvoiceType
            UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "2.0"
            AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

            '   14 - 15 Documentos de referencia
            Dim DocumentosRelacionadas As New List(Of DocumentReferenceType)
            If Not IsNothing(Provision.DocumentoRelacionados) Then
                For Each Doc In Provision.DocumentoRelacionados
                    If IsNothing(DocumentosRelacionadas) Then
                        DocumentosRelacionadas = New List(Of DocumentReferenceType)
                    End If

                    Dim Item As New DocumentReferenceType
                    Item.ID = Doc.Documento_Serie & "-" & Doc.Documento_Numero
                    Item.DocumentTypeCode = Doc.Documento_Tipo
                    DocumentosRelacionadas.Add(Item)
                Next
            End If


            '   Creando los detalles
            Dim ListaDetalles As New List(Of InvoiceLineType)

            For Each ProvisionItem In Provision.Detalles
                Dim ItemDoc As New InvoiceLineType
                ItemDoc.ID = ProvisionItem.Item
                ItemDoc.InvoicedQuantity = New QuantityType() With {.Value = Format(ProvisionItem.Producto_Cantidad, "#0.000"),
                                                                .unitCode = ProvisionItem.Producto_UnidadMedida_Codigo,
                                                                .unitCodeListID = "UN/ECE rec 20",
                                                                .unitCodeListAgencyName = "United Nations Economic Commission for Europe"}
                '   27 Valor de venta por item
                ItemDoc.LineExtensionAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00")

                '   22 Precio de venta unitario por item y codigo
                ItemDoc.PricingReference = New PricingReferenceType With {
                     .AlternativeConditionPrice = New PriceType() {New PriceType With {.PriceAmount = Format(ProvisionItem.Unitario_Precio_Venta, "#0.00"),
                                                                                       .PriceTypeCode = New CodeType With {.Value = ProvisionItem.Unitario_Precio_Venta_Tipo_Codigo,
                                                                                                                           .listName = "Tipo de Precio",
                                                                                                                           .listAgencyName = "PE:SUNAT",
                                                                                                                           .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16"}}}}
                '   26 Descuentos por item
                If ProvisionItem.Unitario_Descuento_Monto_Cargo_Dsto > 0 Then 'Unitario_Descuento
                    ItemDoc.AllowanceCharge = New AllowanceChargeType() {New AllowanceChargeType With {.ChargeIndicator = False,
                                                                                                   .AllowanceChargeReasonCode = New AllowanceChargeReasonCodeType With {
                                                                                                        .Value = ProvisionItem.Unitario_Descuento_Codigo_Sunat,'"00"
                                                                                                        .listAgencyName = "PE:SUNAT",
                                                                                                        .listName = "Cargo/descuento",
                                                                                                        .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo53"},
                                                                                                        .MultiplierFactorNumeric = ProvisionItem.Unitario_Descuento_Factor_Cargo_Dsto,'0.5
                                                                                                        .Amount = Format(ProvisionItem.Unitario_Descuento_Monto_Cargo_Dsto, "#0.00"),
                                                                                                        .BaseAmount = Format(ProvisionItem.Unitario_Descuento_Monto_Base_Cargo_Dsto, "#0.00")}}
                End If


                '   23 Afectacion al IGV por item

                Dim Item_Afectacion_IGV As New TaxTotalType
                If ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo = "10" Then
                    Item_Afectacion_IGV = New TaxTotalType With {
                       .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                       .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {
                                   .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                                   .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                                   .TaxCategory = New TaxCategoryType With {
                                       .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                                       .TaxExemptionReasonCode = New CodeType With {.Value = ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo,
                                                                                    .listName = "Afectacion del IGV",
                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                    .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"},
                                       .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "1000",
                                                                                                           .schemeName = "Codigo de tributos",
                                                                                                           .schemeAgencyName = "PE:SUNAT",
                                                                                                           .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                              .Name = "IGV",
                                                                              .TaxTypeCode = "VAT"}}}}}

                ElseIf ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo = "20" Then

                    Item_Afectacion_IGV = New TaxTotalType With {
                      .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                      .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {
                       .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                       .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                       .TaxCategory = New TaxCategoryType With {
                           .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                           .TaxExemptionReasonCode = New CodeType With {.Value = ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo,
                                                                        .listName = "Afectacion del IGV",
                                                                        .listAgencyName = "PE:SUNAT",
                                                                        .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"},
                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9997",
                                                                                               .schemeName = "Codigo de tributos",
                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                               .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                  .Name = "EXO",
                                                                  .TaxTypeCode = "VAT"}}}}}
                End If



                '   24 Afectacion al ISC
                Dim Item_Afectacion_ISC As TaxTotalType
                If ProvisionItem.Item_ISC_Total > 0 Then
                    Item_Afectacion_ISC = New TaxTotalType With {
                    .TaxAmount = Format(ProvisionItem.Item_ISC_Total, "#0.00"),
                    .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType() With {
                            .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                            .TaxAmount = Format(ProvisionItem.Item_ISC_Total, "#0.00"),
                            .TaxCategory = New TaxCategoryType() With {
                                .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                                .TierRange = ProvisionItem.Unitario_ISC_Codigo,
                                .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "2000",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                     .Name = "ISC",
                                                                     .TaxTypeCode = "EXC"}
                                                       }
                                               }}
                                   }
                End If

                ItemDoc.TaxTotal = New TaxTotalType() {Item_Afectacion_IGV, Item_Afectacion_ISC}

                ItemDoc.Item = New ItemType With {.Description = New TextType() {ProvisionItem.Producto_Descripcion}}
                If Not String.IsNullOrEmpty(ProvisionItem.Producto_Codigo) Then
                    ItemDoc.Item.SellersItemIdentification = New ItemIdentificationType With {.ID = ProvisionItem.Producto_Codigo}
                End If
                '21 Valor Unitario por Item
                ItemDoc.Price = New PriceType() With {.PriceAmount = ProvisionItem.Unitario_Valor_Unitario}
                '27 Numero de placa del vehiculo Gastos Art 37
                If Not IsNothing(ProvisionItem.Numero_Placa_del_Vehiculo) Then
                    If ProvisionItem.Numero_Placa_del_Vehiculo.Trim <> "" Then
                        ItemDoc.Item.AdditionalItemProperty = New ItemPropertyType() {
                        New ItemPropertyType With {.Value = ProvisionItem.Numero_Placa_del_Vehiculo,
                                                    .Name = "Gastos Art. 37 Renta:  Número de Placa",
                                                    .NameCode = New CodeType With {.Value = "7000",
                                                                                    .listName = "Propiedad del item",
                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                    .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo55"}
                                                    }}
                    End If
                End If



                ListaDetalles.Add(ItemDoc)
            Next

            '   Totales de factura
            'Dim LeyendasTotalesDocumento As New List(Of AdditionalMonetaryTotalType)
            ''   28
            'If Provision.Total_ValorVenta_OperacionesGravadas > 0 Then
            '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1001", .PayableAmount = Provision.Total_ValorVenta_OperacionesGravadas})
            'End If
            ''   29
            'If Provision.Total_ValorVenta_OperacionesInafectas > 0 Then
            '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1002", .PayableAmount = Provision.Total_ValorVenta_OperacionesInafectas})
            'End If
            ''   30
            'If Provision.Total_ValorVenta_OperacionesExoneradas > 0 Then
            '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1003", .PayableAmount = Provision.Total_ValorVenta_OperacionesExoneradas})
            'End If
            ''   31
            'If Provision.Total_ValorVenta_OperacionesGratuitas > 0 Then
            '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1004", .PayableAmount = Provision.Total_ValorVenta_OperacionesGratuitas})
            'End If
            ''   32
            'If Provision.Total_Descuentos > 0 Then
            '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "2005", .PayableAmount = Provision.Total_Descuentos})
            'End If

            Dim ListaSumatorias As New List(Of TaxTotalType)

            Dim Sumatoria As New TaxTotalType
            Sumatoria.TaxAmount = New AmountType With {.Value = (Format(Provision.Total_IGV + Provision.Total_ISC + Provision.Total_OtrosTributos, "##0.00")),
                                                   .currencyID = Provision.Moneda_Codigo}
            Dim DetallesSumatoria As New List(Of TaxSubtotalType)

            '   33 Sumatoria IGV
            If Provision.Total_IGV > 0 Then
                Dim vSumatoria_IGV As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Provision.Total_IGV,
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With {
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "1000",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "IGV",
                                                                                                .TaxTypeCode = "VAT"}}}
                DetallesSumatoria.Add(vSumatoria_IGV)
            End If

            '   34 Sumatoria ISC
            If Provision.Total_ISC > 0 Then
                Dim vSumatoria_ISC As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(Provision.Total_ISC, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With {
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "2000",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "ISC",
                                                                                                .TaxTypeCode = "EXC"}}}
                DetallesSumatoria.Add(vSumatoria_ISC)
            End If

            '   35 Otros Tributos
            If Provision.Total_OtrosTributos > 0 Then
                Dim vSumatoria_Otrib As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(Provision.Total_OtrosTributos, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With {
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9999",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "OTROS",
                                                                                                .TaxTypeCode = "OTH"}}}
                DetallesSumatoria.Add(vSumatoria_Otrib)
            End If

            If Provision.Total_ValorVenta_OperacionesExoneradas > 0 Then
                Dim vSumatoria_Exonerado As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesExoneradas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(0, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With {
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9997",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "EXO",
                                                                                                .TaxTypeCode = "VAT"}}}
                DetallesSumatoria.Add(vSumatoria_Exonerado)
            End If

            If Provision.Total_ValorVenta_OperacionesInafectas > 0 Then
                Dim vSumatoria_Inafecto As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesInafectas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(0, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With {
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9998",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "INA",
                                                                                                .TaxTypeCode = "FRE"}}}
                DetallesSumatoria.Add(vSumatoria_Inafecto)
            End If

            If Provision.Total_ValorVenta_OperacionesGratuitas > 0 Then
                Dim vSumatoria_Gratuito As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGratuitas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(0, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With {
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9996",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "GRA",
                                                                                                .TaxTypeCode = "FRE"}}}
                DetallesSumatoria.Add(vSumatoria_Gratuito)
            End If

            Sumatoria.TaxSubtotal = DetallesSumatoria.ToArray
            ListaSumatorias.Add(Sumatoria)
            ''

            Dim TotalMonedas As New MonetaryTotalType
            '   36 Descuentos globales ????¿¿¿¿¿ <sac:AdditionalMonetaryTotal><cbc:ID>2005</cbc:ID><cbc:PayableAmount currencyID="PEN">59230.51</cbc:PayableAmount></sac:AdditionalMonetaryTotal>
            If Provision.Total_Descuentos > 0 Then
                TotalMonedas.AllowanceTotalAmount = Format(Provision.Total_Descuentos, "##0.00")
            End If

            'Dim Global_Descuentos_ As New List(Of AllowanceChargeType)
            'If Provision.Cargos_Descuentos_Globales > 0 Then
            '    Dim Global_Descuentos As New AllowanceChargeType With {.ChargeIndicator = False,
            '                                                           .AllowanceChargeReasonCode = New AllowanceChargeReasonCodeType With {
            '                                                               .Value = 0,
            '                                                               .listAgencyName = "",
            '                                                               .listName = "",
            '                                                               .listURI = ""},
            '                                                            .Amount = 0,
            '                                                            .BaseAmount = 0}



            '    Global_Descuentos_.Add(Global_Descuentos)
            'End If


            '   37 Sumatoria Otros cargos
            If Provision.Total_OtrosCargos > 0 Then
                TotalMonedas.ChargeTotalAmount = New AmountType With {.Value = Format(Provision.Total_OtrosCargos, "##0.00"), .currencyID = Provision.Moneda_Codigo}
            End If
            '   38 Importe total de la venta, cesion en uso o del servicio prestado
            TotalMonedas.LineExtensionAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_Total, "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}

            TotalMonedas.TaxInclusiveAmount = New AmountType With {.Value = Format(Provision.Total_Importe_Venta, "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}

            TotalMonedas.PayableAmount = New AmountType With {.Value = Format((Provision.Total_Importe_Venta - Provision.Total_Descuentos + Provision.Total_OtrosCargos), "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}

            '   39 Importe de la percepcion --- en moneda nacional -- revisar
            'If Provision.Total_Importe_Percepcion > 0 Then
            '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "2001", .ReferenceAmount = Provision.Total_Importe_Venta, .PayableAmount = Provision.Total_Importe_Percepcion, .TotalAmount = Provision.Percep_Importe_Total_Cobrado})
            'End If

            '   40-41-42 Anticipos
            Dim Anticipos As New List(Of PaymentType)
            '   43 Total anticipos
            If Provision.Total_Anticipos > 0 Then
                For Each pAnt In Provision.DocumentoAnticipos
                    Anticipos.Add(New PaymentType With {.PaidAmount = pAnt.Anticipo_Monto,
                                                    .ID = New IdentifierType With {.schemeID = pAnt.Anticipo_Documento_Tipo,
                                                                                   .Value = pAnt.Anticipo_Documento_Serie & "-" & pAnt.Anticipo_Documento_Numero,
                                                                                   .schemeName = "SUNAT:Identificador de Documentos Relacionados",
                                                                                   .schemeAgencyName = "PE:SUNAT"},
                                                    .InstructionID = New IdentifierType With {.schemeID = pAnt.Anticipo_Emisor_Documento_Tipo, .Value = pAnt.Anticipo_Emisor_Documento_Numero}})
                Next
                TotalMonedas.PrepaidAmount = New AmountType With {.Value = Format(Provision.Total_Anticipos, "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}
            End If
            '   44 Codigo del tipo de operacion - Catag. Nº 17
            'Dim TipoOperacion As New List(Of AdditionalPropertyType)
            'If Not String.IsNullOrEmpty(Provision.Tipo_Operacion_Codigo) Then
            '    TipoOperacion.Add(New AdditionalPropertyType With {.ID = Provision.Tipo_Operacion_Codigo})
            'End If

            '   46 Dirección del lugar en el que se entrega el bien o se presta el servicio
            'Dim LugarEntrega As SupplierPartyType
            'If Not String.IsNullOrEmpty(Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo) Then
            '    LugarEntrega = New SupplierPartyType With {.Party = New PartyType With {.PostalAddress = New AddressType() With {
            '                 .ID = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo,
            '                 .StreetName = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Calle,
            '                 .CitySubdivisionName = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Urbanizacion,
            '                 .CityName = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Provincia,
            '                 .CountrySubentity = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Departamento,
            '                 .District = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Distrito,
            '                 .Country = New CountryType() With {
            '                     .IdentificationCode = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_PaisCodigo
            '                }
            '            }}}
            'End If

            '   58, 59, 60, 61 Transportista
            'Dim Transportista As PartyType()
            'If Not String.IsNullOrEmpty(Provision.Transportista_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Transportista_Documento_Numero) Then
            '    Transportista = New PartyType() {New PartyType With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Transportista_Documento_Tipo, .Value = Provision.Transportista_Documento_Numero}}}, .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Transportista_RazonSocial_Nombre}}}}
            'End If

            '   62, 63 Conductor
            'Dim Conductor As PartyIdentificationType
            'If Not String.IsNullOrEmpty(Provision.Conductor_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Conductor_Documento_Numero) Then
            '    Conductor = New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Conductor_Documento_Tipo, .Value = Provision.Conductor_Documento_Numero}}
            'End If

            '   68 Placa
            'Dim Placa_Vehiculo As SUNATCostsType
            'If Not String.IsNullOrEmpty(Provision.Numero_Placa_del_Vehiculo) Then
            '    Placa_Vehiculo = New SUNATCostsType With {.RoadTransport = New RoadTransportType With {.LicensePlateID = Provision.Numero_Placa_del_Vehiculo}}
            'End If

            '   Guia Factura
            'Dim GuiaFact As SUNATEmbededDespatchAdviceType
            'If Not String.IsNullOrEmpty(Provision.GuiaFactura_Documento_Tipo_Codigo) And Not String.IsNullOrEmpty(Provision.GuiaFactura_Documento_Serie) And Not String.IsNullOrEmpty(Provision.GuiaFactura_Documento_Numero) Then
            'GuiaFact = New SUNATEmbededDespatchAdviceType
            ''   185-2015 - 48
            'GuiaFact.OriginAddress = New AddressType() With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
            'GuiaFact.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}

            '   47, 48, 49, 50 Transportista - Numero, serie, tipo doc, numero de remitente original
            'GuiaFact.OrderReference = New OrderReferenceType() {New OrderReferenceType With {.DocumentReference = New DocumentReferenceType With {.ID = Provision.GuiaFactura_Documento_Serie & "-" & Provision.GuiaFactura_Documento_Numero, .DocumentTypeCode = Provision.GuiaFactura_Documento_Tipo_Codigo}, .CustomerReference = Provision.GuiaFactura_RemitenteDocumentoOriginal_NumDoc}}

            '   51, 52, 53 Destinatario
            'GuiaFact.DeliveryCustomerParty = New CustomerPartyType With {.CustomerAssignedAccountID = New IdentifierType() With {.schemeID = Provision.Cliente_Documento_Tipo, .Value = Provision.Cliente_Documento_Numero},
            '.Party = New PartyType() With {.PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = New NameType With {.Value = Provision.Cliente_RazonSocial_Nombre}}}}}

            '   54, 55, 56, 57 Datos relacionados con el traslado de los bienes, 64, 65 Vehiculo
            'GuiaFact.Shipment = New ShipmentType() With {.ShippingPriorityLevelCode = Provision.MotivoTraslad_Codigo,
            '                                             .Consignment = New ConsignmentType With {.NetWeightMeasure = New MeasureType With {.unitCode = Provision.PesoBrutoTotalGuia_UnidMed, .Value = Provision.PesoBrutoTotalGuia_Valor}},
            '                                             .ShipmentStage = New ShipmentStageType() {New ShipmentStageType With {
            '                                                     .TransportModeCode = Provision.ModalidadTraslado_Codigo,
            '                                                     .TransitPeriod = New PeriodType() With {.StartDate = Provision.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista},
            '                                                     .CarrierParty = Transportista,
            '                                                     .DriverPerson = Conductor,
            '                                                     .TransportMeans = New TransportMeansType() With {.RoadTransport = New RoadTransportType() With {.LicensePlateID = Provision.Transportes(0).Placa}, .RegistrationNationalityID = Provision.Transportes(0).NumeroInscripcion_CertificadoHabilitacionVehicular}
            '                                                 }},
            '                                             .Delivery = New DeliveryType With {.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}},
            '                                             .OriginAddress = New AddressType With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
            '                                            }

            'GuiaFact.Shipment = New ShipmentType() With {.ShippingPriorityLevelCode = Provision.MotivoTraslad_Codigo,
            '                                            .Consignment = New ConsignmentType With {.NetWeightMeasure = New MeasureType With {.unitCode = Provision.PesoBrutoTotalGuia_UnidMed, .Value = Provision.PesoBrutoTotalGuia_Valor}},
            '                                            .ShipmentStage = New ShipmentStageType() {New ShipmentStageType With {
            '                                                    .TransportModeCode = Provision.ModalidadTraslado_Codigo,
            '                                                    .TransitPeriod = New PeriodType() With {.StartDate = Provision.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista},
            '                                                    .CarrierParty = Transportista,
            '                                                    .DriverPerson = Conductor,
            '                                                    .TransportMeans = New TransportMeansType() With {.RoadTransport = New RoadTransportType() With {.LicensePlateID = Provision.Transportes(0).Placa}, .RegistrationNationalityID = Provision.Transportes(0).NumeroInscripcion_CertificadoHabilitacionVehicular}
            '                                                }},
            '                                            .Delivery = New DeliveryType With {.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}},
            '                                            .OriginAddress = New AddressType With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
            '                                           }

            'If Not String.IsNullOrEmpty(Provision.Transportista_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Transportista_Documento_Numero) Then
            '    Transportista = New PartyType() {New PartyType With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Transportista_Documento_Tipo, .Value = Provision.Transportista_Documento_Numero}}}, .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Transportista_RazonSocial_Nombre}}}}
            '    GuiaFact.OrderReference = New OrderReferenceType() {New OrderReferenceType With {.DocumentReference = New DocumentReferenceType With {.ID = Provision.Transportista_Documento_Numero, .DocumentTypeCode = Provision.Transportista_Documento_Tipo}}}
            'End If

            'End If

            'Creando cabecera
            invoice = New UblLarsen.Ubl2.InvoiceType() With {
            .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero,
             .IssueDate = Provision.Fecha_Emision,
            .DueDate = Provision.Fecha_Vencimiento,
             .InvoiceTypeCode = New CodeType With {.Value = Provision.TipoDocumento_Codigo,
                                                   .listAgencyName = "PE:SUNAT",
                                                   .listName = "Tipo de Documento",
                                                   .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01",
                                                   .listID = "0101", .name = "Tipo de Operacion",
                                                   .listSchemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo51"},
            .DocumentCurrencyCode = New CurrencyCodeType() With {.Value = Provision.Moneda_Codigo,
                                                                 .listID = "ISO 4217 Alpha",
                                                                 .listName = "Currency",
                                                                 .listAgencyName = "United Nations Economic Commission for Europe"},
            .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero,
                        .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                                .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}},
                        .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}},
             .AccountingSupplierParty = New SupplierPartyType() With {
                 .Party = New PartyType() With {
                     .PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Emisor_Documento_Numero,
                                                                                     .schemeID = "6",
                                                                                     .schemeName = "Documento de Identidad",
                                                                                     .schemeAgencyName = "PE:SUNAT",
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                    .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}},
                    .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial,
                                                                                                    .RegistrationAddress = New AddressType With {
                                                                                                        .AddressTypeCode = New CodeType With {.Value = "0000",
                                                                                                                                            .listAgencyName = "PE:SUNAT",
                                                                                                                                            .listName = "Establecimientos anexos"},
                                                                                                        .ID = New IdentifierType With {.Value = Provision.Emisor_Direccion_Ubigeo,
                                                                                                                                      .schemeAgencyName = "PE:INEI",
                                                                                                                                      .schemeName = "Ubigeos"},
                                                                                                        .AddressLine = New AddressLineType() {New AddressLineType With {.Line = Provision.Emisor_Direccion_Calle}},
                                                                                                        .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
                                                                                                        .CityName = Provision.Emisor_Direccion_Provincia,
                                                                                                        .CountrySubentity = Provision.Emisor_Direccion_Departamento,
                                                                                                        .District = Provision.Emisor_Direccion_Distrito,
                                                                                                        .Country = New CountryType With {
                                                                                                        .IdentificationCode = New CountryIdentificationCodeType With {.Value = Provision.Emisor_Direccion_PaisCodigo,
                                                                                                                                                                     .listID = "ISO 3166-1",
                                                                                                                                                                     .listAgencyName = "United Nations Economic Commission for Europe",
                                                                                                                                                                      .listName = "Country"}}}}}}},
             .AccountingCustomerParty = New CustomerPartyType() With {
                 .Party = New PartyType() With {
                     .PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Cliente_Documento_Numero,
                                                                                     .schemeID = Provision.Cliente_Documento_Tipo,
                                                                                     .schemeName = "Documento de Identidad",
                                                                                     .schemeAgencyName = "PE:SUNAT",
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                     .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With
                                                                     {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}}
                }
            },
            .DespatchDocumentReference = DocumentosRelacionadas.ToArray,
             .TaxTotal = ListaSumatorias.ToArray,
             .LegalMonetaryTotal = TotalMonedas,
             .InvoiceLine = ListaDetalles.ToArray,
        .PrepaidPayment = Anticipos.ToArray
        } ' ,.AllowanceCharge = Global_Descuentos_.ToArray


            '.PostalAddress = New AddressType() With {
            '    .ID = Provision.Emisor_Direccion_Ubigeo,
            '    .StreetName = Provision.Emisor_Direccion_Calle,
            '    .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
            '    .CityName = Provision.Emisor_Direccion_Provincia,
            '    .CountrySubentity = Provision.Emisor_Direccion_Departamento,
            '    .District = Provision.Emisor_Direccion_Distrito,
            '    .Country = New CountryType() With {
            '        .IdentificationCode = Provision.Emisor_Direccion_PaisCodigo

            If Provision.Fecha_Vencimiento.Year = 1 Then
                invoice.DueDate = Nothing
            Else
                invoice.DueDate = Provision.Fecha_Vencimiento
            End If



            '.ID = "1001", .PayableAmount = DocumentoXMLCliente.Dvt_Total_Gravado

            'Cargando las leyendas adicionales
            'Dim LeyendasAdicionales As AdditionalPropertyType() = New AdditionalPropertyType(Provision.Leyendas.Count - 1) {}
            'For i = 0 To Provision.Leyendas.Count - 1
            '    LeyendasAdicionales(i) = New AdditionalPropertyType() With {
            '         .ID = Provision.Leyendas(i).Codigo, .Value = Provision.Leyendas(i).Descripcion
            '        }
            'Next

            'Notas
            Dim Notas As New List(Of TextType)
            If Not IsNothing(Provision.Leyendas) Then
                For Each Leyendas In Provision.Leyendas
                    Notas.Add(New TextType With {.Value = Leyendas.Descripcion,
                                             .languageLocaleID = Leyendas.Codigo
                          })
                Next
            End If

            invoice.Note = Notas.ToArray
            'invoice.LineCountNumeric = Provision.Detalles.Count


            'Agregando los extension: Firma del documento y campos personalizados
            '.AdditionalProperty = LeyendasAdicionales reemplazado por Notas
            invoice.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}
            'New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType() With
            '{.AdditionalInformation = New AdditionalInformationType() With {.AdditionalMonetaryTotal = LeyendasTotalesDocumento.ToArray, .SUNATTransaction = TipoOperacion.ToArray, .SUNATEmbededDespatchAdvice = GuiaFact, .SUNATCosts = Placa_Vehiculo}}},
            'invoice.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()},
            '                                        New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}

            'Agregando namespace
            Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
            ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
            'ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")

            ns.Add("ccts", "urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2")
            ns.Add("stat", "urn:oasis:names:specification:ubl:schema:xsd:DocumentStatusCode-1.0")

            Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
            Using writer As XmlWriter = XmlWriter.Create(Ruta_Generado & "\" & xmlFilename, settings) 'Provision.RootPath_Enterprise & "\xml_temp\"
                Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.InvoiceType)
                Dim xs As New XmlSerializer(typeToSerialize)
                xs.Serialize(writer, invoice, ns)
            End Using
            ProcesoCompleto.ExtensionContent_Indice = 0

            ProcesoCompleto.FileNameWithOutExtension = Filename
            ProcesoCompleto.ArchivoCreadoCorrectamente = True

            Return ProcesoCompleto

        Catch ex As Exception

        End Try


    End Function


    'BAJA DEL COMPROBANTE

    Public Function Dar_Baja(pDoc As DocumentoElectronico) As String

        Dim ComBaja As New DocumentoElectronicoComunicacionBaja
        Dim ListaAnulados As New List(Of DocumentoElectronico)

        ListaAnulados.Add(pDoc)

        ComBaja.Documentos = ListaAnulados.ToArray


        Dim settings As New System.Xml.XmlWriterSettings With {.ConformanceLevel = System.Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As System.Xml.XmlWriter = System.Xml.XmlWriter.Create("DocBajaFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
            Dim xs As New System.Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, ComBaja)
        End Using

        Dim doc12 As New System.Xml.XmlDocument()
        doc12.Load("DocBajaFactura.xml")

        Return Documento_DarBaja(doc12)

    End Function

    Public Function Documento_DarBaja(xmlDoc As XmlDocument) As String
        Dim Provision As New DocumentoElectronicoComunicacionBaja
        Try

            'Dim RootPathFull As String

            'RootPathFull = "D:\WebAppsFiles" 'Debug --Es el path que se debe indicar en la conf. de empresa

            'En el despliegue se debe de crear: Root/RUC/ --xml_pos - xml_pos_attached - xml_temp - xml_generated - xml_aceptados - xml_rechazados

            Dim reader As New System.Xml.Serialization.XmlSerializer(Provision.[GetType]())
            Dim reader2 As XmlNodeReader = New XmlNodeReader(xmlDoc)
            Provision = DirectCast(reader.Deserialize(reader2), DocumentoElectronicoComunicacionBaja)

            'Guardo el documento como pendiente
            'Ado_FE.Instancia.Documento_DarBaja(Provision)

            Dim Filename As String = Provision.Documentos(0).Emisor_Documento_Numero & "-" & Provision.Documentos(0).TipoDocumento_Codigo & "-" & Provision.Documentos(0).TipoDocumento_Serie & "-" & Provision.Documentos(0).TipoDocumento_Numero
            Dim xmlFilename_V2 As String = Filename & "_V2" & ".XML"
            Provision.Documentos(0).Doc_SUNAT_NombreArchivoXML = xmlFilename_V2


            'Guardo el xml en el directorio virtual
            Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
            Using writer As XmlWriter = XmlWriter.Create(Ruta_GeneradoBaja & "\" & xmlFilename_V2, settings)
                Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
                Dim xs As New Serialization.XmlSerializer(typeToSerialize)
                xs.Serialize(writer, Provision)
            End Using

            'Actualizar a estado :Sin Enviar-4105
            'Dim log As New Log_VTS_DOC_TRANSAC_CAB
            'Dim ent As New Ent_DOC_TRANSAC_CAB
            'ent.MyOrigen = Provision.Documentos(0).Origen
            'ent.Emp_cCodigo = Provision.Documentos(0).Empresa
            'ent.Pan_cAnio = Provision.Documentos(0).Anio
            'ent.Per_cPeriodo = Provision.Documentos(0).Periodo
            'ent.TpDc_Codigo = Provision.Documentos(0).Tipo_Doc_Sistema
            'ent.Dvt_VTSerie = Provision.Documentos(0).TipoDocumento_Serie
            'ent.Dvt_VTNumer = Provision.Documentos(0).TipoDocumento_Numero
            'ent.Vnt_FactElectronica_Estado = "4113"
            'If log.Actualizar_Estado_Xml(ent) Then

            'End If

        Catch ex As Exception
            'Provision.Documentos(0).SQL_Retorno = "-1"
        End Try

        Return 1
    End Function

End Class
