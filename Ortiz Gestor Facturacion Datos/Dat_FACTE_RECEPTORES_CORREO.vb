﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_RECEPTORES_CORREO
    Public Function Buscar() As List(Of eReceptor)
        Dim ListaItems As New List(Of eReceptor)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RECEPTORES_CORREO_PENDIENTES_REGISTRO_CLIENTE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New eReceptor With {.Ruc = Dr.GetString(0), .Email = Dr.GetString(1)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub Cambiar_Estado(ByVal pEntidad As eReceptor)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RECEPTORES_CORREO_PENDIENTES_CAMBIAR_ESTADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PReceptor_NumDoc", SqlDbType.VarChar, 11).Value = pEntidad.Ruc
                    Cmd.Parameters.Add("@PReceptor_Email", SqlDbType.VarChar, 50).Value = pEntidad.Email
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function Buscar_Lista(pEntidad As eReceptor) As List(Of eReceptor)
        Dim ListaItems As New List(Of eReceptor)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RECEPTORES_LISTA", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PReceptor_TipoDocIdent_Numero", SqlDbType.VarChar, 11).Value = pEntidad.Ruc
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New eReceptor With {.TipoDocIdent_Codigo = Dr.GetString(0), .Ruc = Dr.GetString(1), .Correlativo = Dr.GetInt32(2), .Email = Dr.GetString(3), .Validado = Dr.GetBoolean(4), .CopiadoACliente = Dr.GetBoolean(5), .FechaRegistro = Dr.GetDateTime(6)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
