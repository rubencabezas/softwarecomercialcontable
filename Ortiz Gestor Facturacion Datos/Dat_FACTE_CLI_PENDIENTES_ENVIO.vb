﻿Imports System.Data.SqlClient
Public Class Dat_FACTE_CLI_PENDIENTES_ENVIO
    Public Function Buscar_DocVenta(ByVal pTarea As eProgramacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_OBTENER_FACTURAS_BOLETAS_PENDIENTES_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@pEmp_RUC", SqlDbType.Char, 11).Value = pTarea.Emisor_RUC
                    If pTarea.Progra_Appli_Filtra_Serie And Not String.IsNullOrEmpty(pTarea.Progra_Appli_Serie) Then
                        Cmd.Parameters.Add("@pDocumentoSerie", SqlDbType.Char, 4).Value = pTarea.Progra_Appli_Serie
                    Else
                        Cmd.Parameters.Add("@pDocumentoSerie", SqlDbType.Char, 4).Value = Nothing
                    End If
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_RazonSocial_Nombre = Dr.GetString(0),
                                                                         .Emisor_RUC = Dr.GetString(1),
                                                                         .Emisor_Direccion_Ubigeo = Dr.GetString(2),
                                                                         .Emisor_Direccion_Calle = Dr.GetString(3),
                                                                         .Emisor_Direccion_Urbanizacion = Dr.GetString(4),
                                                                         .Emisor_Direccion_Departamento = Dr.GetString(5),
                                                                         .Emisor_Direccion_Provincia = Dr.GetString(6),
                                                                         .Emisor_Direccion_Distrito = Dr.GetString(7),
                                                                         .Emisor_Direccion_PaisCodigo = Dr.GetString(8),
                                                                         .TpDc_Codigo_St = Dr.GetString(9),
                                                                         .Dvt_VTSerie = Dr.GetString(10),
                                                                         .Dvt_VTNumer = Dr.GetString(11),
                                                                         .Dvt_Fecha_Emision = Dr.GetDateTime(12),
                                                                         .Dvt_Fecha_Vencimiento = Dr.GetDateTime(13),
                                                                         .Dvt_Moneda_Internacional = Dr.GetString(14),
                                                                           .Cliente_Documento_Tipo = Dr.GetString(15),
                                                                         .Cliente_Documento_Numero = Dr.GetString(16),
                                                                         .Cliente_RazonSocial_Nombre = Dr.GetString(17),
                                                                         .Cliente_Apellido_Primero = Dr.GetString(18),
                                                                         .Cliente_Apellido_Segundo = Dr.GetString(19),
                                                                         .Dvt_Total_IGV = Dr.GetDecimal(20),
                                                                         .Dvt_Total_Importe = Dr.GetDecimal(21),
                                                                         .Dvt_Origen_Codigo = Dr.GetString(22), .Emisor_Codigo = Dr.GetString(23),
                                                                           .Pan_cAnio = Dr.GetString(24), .Per_cPeriodo = Dr.GetString(25), .Dvt_Codigo = Dr.GetString(26),
                                                                           .Pdv_Descripcion = Dr.GetString(27), .TpDc_Descripcion = Dr.GetString(28), .Dvt_TipCambMont = Dr.GetDecimal(29), .Ent_cCodEntidad = Dr.GetString(30), .Dvt_Est_Fact_Ds = Dr.GetString(31), .Vnt_EstadoEnvio = Dr.GetString(32),
                                                                           .Dvt_Abrev_Moneda = Dr.GetString(33), .Vnt_Observacion = Dr.GetString(34), .Dvt_Des_Moneda = Dr.GetString(35), .Vnt_IGV_Porcentaje = Dr.GetDecimal(36), .Dvt_Total_Gravado = Dr.GetDecimal(37), .Dvt_Simbol_Moneda = Dr.GetString(38), .SPOT_Afecto = Dr.GetBoolean(39), .SPOT_Reg_Codigo = Dr.GetString(40), .SPOT_Porcentaje = Dr.GetDecimal(41), .SPOT_Monto = Dr.GetDecimal(42)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_DocDetalle(ByVal pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacionDetalle)
        Dim ListaItems As New List(Of eProvisionFacturacionDetalle)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_OBTENER_DETALLE_FACTURAS_BOLETAS_PENDIENTES_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = pEntidad.Dvt_Origen_Codigo
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = pEntidad.Emisor_Codigo
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = pEntidad.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = pEntidad.Per_cPeriodo
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = pEntidad.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New eProvisionFacturacionDetalle With {.Item_Orden = Dr.GetInt16(0), .Item_Unidad_Codigo = Dr.GetString(1), .Item_Cantidad = Dr.GetDecimal(2), .Item_Unitario_ValorVenta = Dr.GetDecimal(3), .Item_Unitario_Precio = Dr.GetDecimal(4), .Item_Unitario_Precio_Tipo = Dr.GetString(5), .Item_Unitario_IGV = Dr.GetDecimal(6), .Item_Afectacion_IGV = Dr.GetString(7), .Item_Trib_IGV_Porcentaje = Dr.GetDecimal(8), .Item_Trib_ISC_Valor = Dr.GetDecimal(9), .Item_Trib_Otros_Valor = Dr.GetDecimal(10), .Item_Descripcion = Dr.GetString(11), .Item_Unitario_Importe = Dr.GetDecimal(12), .Item_CodigoProducto = Dr.GetString(13)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_DocVentaDocumentoRelacionado(ByVal pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacionReferenciaDocumento)
        Dim ListaItems As New List(Of eProvisionFacturacionReferenciaDocumento)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_OBTENER_REFERENCIADOCUMENTO_FACTURAS_BOLETAS_PENDIENTES_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = pEntidad.Dvt_Origen_Codigo
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = pEntidad.Emisor_Codigo
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = pEntidad.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = pEntidad.Per_cPeriodo
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = pEntidad.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New eProvisionFacturacionReferenciaDocumento With {.Documento_Tipo_Sunat = Dr.GetString(0), .Documento_Serie = Dr.GetString(1), .Documento_Numero = Dr.GetString(2), .Referencia_Tipo_Codigo = Dr.GetString(3), .Referencia_Motivo = Dr.GetString(4)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_CAMBIAR_ESTADO_FACTURAS_BOLETAS_ENVIADOS", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = pEntidad.Dvt_Origen_Codigo
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = pEntidad.Emisor_Codigo
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = pEntidad.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = pEntidad.Per_cPeriodo
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = pEntidad.Dvt_Codigo
                    Cmd.Parameters.Add("@EstadoElectronico", SqlDbType.Char, 4).Value = pEntidad.EstadoElectronico
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_OtrosCPE(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_CAMBIAR_ESTADO_FACTURAS_BOLETAS_ENVIADOS", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = pEntidad.Dvt_Origen_Codigo
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = pEntidad.Emisor_Codigo
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = pEntidad.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = pEntidad.Per_cPeriodo
                    Cmd.Parameters.Add("@PTes_Codigo", SqlDbType.Char, 10).Value = pEntidad.Dvt_Codigo
                    Cmd.Parameters.Add("@EstadoElectronico", SqlDbType.Char, 4).Value = pEntidad.EstadoElectronico
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_BoletasVenta_Resumen_Pendientes(ByVal pTarea As eProgramacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("UP_FACTE_SENDSUMMARY_OBTENER_BOLETASVENTA_PENDIENTES_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@pEmp_RUC", SqlDbType.Char, 11).Value = pTarea.Emisor_RUC
                    If pTarea.Progra_Appli_Filtra_Serie And Not String.IsNullOrEmpty(pTarea.Progra_Appli_Serie) Then
                        Cmd.Parameters.Add("@pDocumentoSerie", SqlDbType.Char, 4).Value = pTarea.Progra_Appli_Serie
                    Else
                        Cmd.Parameters.Add("@pDocumentoSerie", SqlDbType.Char, 4).Value = Nothing
                    End If

                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Dvt_Fecha_Emision = Dr.GetDateTime(1), .TpDc_Codigo_St = Dr.GetString(2), .Dvt_VTSerie = Dr.GetString(3)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Obtener_Resumen_BoletaVenta(ByVal pPendiente As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("UP_FACTE_SENDSUMMARY_OBTENER_RESUMEN_BOLETASVENTA_PENDIENTES_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pPendiente.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_FechaEmision", SqlDbType.Date).Value = pPendiente.Dvt_Fecha_Emision
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pPendiente.Dvt_VTSerie
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_RazonSocial_Nombre = Dr.GetString(0),
                                                                         .Emisor_RUC = Dr.GetString(1),
                                                                         .Emisor_Direccion_Ubigeo = Dr.GetString(2),
                                                                         .Emisor_Direccion_Calle = Dr.GetString(3),
                                                                         .Emisor_Direccion_Urbanizacion = Dr.GetString(4),
                                                                         .Emisor_Direccion_Departamento = Dr.GetString(5),
                                                                         .Emisor_Direccion_Provincia = Dr.GetString(6),
                                                                         .Emisor_Direccion_Distrito = Dr.GetString(7),
                                                                         .Emisor_Direccion_PaisCodigo = Dr.GetString(8),
                                                                           .Dvt_Fecha_Generacion = Dr.GetDateTime(9),
                                                                           .Rs_Correlativo = Dr.GetString(10),
                                                                           .Rs_TotalResumen = Dr.GetDecimal(11),
                                                                           .Dvt_Moneda_Internacional = Dr.GetString(12),
                                                                           .Dvt_Fecha_Emision = Dr.GetDateTime(13),
                                                                         .TpDc_Codigo_St = Dr.GetString(14),
                                                                         .Dvt_VTSerie = Dr.GetString(15),
                                                                           .Rs_NumeroInicio = Dr.GetString(16),
                                                                           .Rs_NumeroFin = Dr.GetString(17),
                                                                           .Dvt_Total_Importe = Dr.GetDecimal(18),
                                                                         .Dvt_Total_IGV = Dr.GetDecimal(19),
                                                                           .Dvt_Total_Gravado = Dr.GetDecimal(20),
                                                                           .Dvt_Total_Exonerado = Dr.GetDecimal(21),
                                                                           .Dvt_Total_Inafecto = Dr.GetDecimal(22),
                                                                           .Dvt_Total_ISC = Dr.GetDecimal(23),
                                                                           .Dvt_Total_Otros = Dr.GetDecimal(24),
                                                                           .Rs_NumeroLinea = Dr.GetString(25), .Emisor_Codigo = Dr.GetString(26)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_Resumen(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_CAMBIAR_ESTADO_BOLETAS_RESUMEN_ENVIADOS", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor", SqlDbType.Char, 3).Value = pEntidad.Emisor_Codigo
                    Cmd.Parameters.Add("@PDoc_FechaEmision", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Emision
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 3).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_NumeroInicio", SqlDbType.Char, 8).Value = pEntidad.Rs_NumeroInicio
                    Cmd.Parameters.Add("@PDoc_NumeroFin", SqlDbType.Char, 8).Value = pEntidad.Rs_NumeroFin
                    Cmd.Parameters.Add("@EstadoElectronico", SqlDbType.Char, 4).Value = pEntidad.EstadoElectronico
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Resumen_BoletaVenta_Ticket_CambiarEstado(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SENDBILL_CAMBIAR_ESTADO_BOLETAS_RESUMEN_ENVIADOS", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_NumeroInicio", SqlDbType.Char, 8).Value = pEntidad.Rs_NumeroInicio
                    Cmd.Parameters.Add("@PDoc_NumeroFin", SqlDbType.Char, 8).Value = pEntidad.Rs_NumeroFin
                    Cmd.Parameters.Add("@EstadoElectronico", SqlDbType.Char, 4).Value = pEntidad.EstadoElectronico
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Retencion_ListaPendientes() As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("Up_FACTE_RETENCIONES_REGISTRADOS_PENDIENTES_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Dvt_Origen_Codigo = Dr.GetString(0),
                                                                         .Emisor_Codigo = Dr.GetString(1),
                                                                         .Pan_cAnio = Dr.GetString(2),
                                                                         .Per_cPeriodo = Dr.GetString(3),
                                                                         .Dvt_Codigo = Dr.GetString(4),
                                                                         .TpDc_Codigo_St = Dr.GetString(5),
                                                                         .Dvt_VTSerie = Dr.GetString(6),
                                                                         .Dvt_VTNumer = Dr.GetString(7),
                                                                         .Dvt_Fecha_Emision = Dr.GetDateTime(8)
                                                                          })
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Retencion_Detalles(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("Up_FACTE_RETENCIONES_REGISTRADOS_PENDIENTES_ENVIO_DETALLE", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_Origen_Codigo
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = Cls_Enti.Emisor_Codigo
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = Cls_Enti.Per_cPeriodo
                    Cmd.Parameters.Add("@PTes_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_RazonSocial_Nombre = Dr.GetString(0),
                                                                         .Emisor_RUC = Dr.GetString(1),
                                                                         .Emisor_Direccion_Ubigeo = Dr.GetString(2),
                                                                         .Emisor_Direccion_Calle = Dr.GetString(3),
                                                                         .Emisor_Direccion_Urbanizacion = Dr.GetString(4),
                                                                         .Emisor_Direccion_Departamento = Dr.GetString(5),
                                                                         .Emisor_Direccion_Provincia = Dr.GetString(6),
                                                                         .Emisor_Direccion_Distrito = Dr.GetString(7),
                                                                         .Emisor_Direccion_PaisCodigo = Dr.GetString(8),
                                                                           .Dvt_Moneda_Internacional = Dr.GetString(9),
                                                                         .TpDc_Codigo_St = Dr.GetString(10),
                                                                         .Dvt_VTSerie = Dr.GetString(11),
                                                                           .Dvt_VTNumer = Dr.GetString(12),
                                                                           .Dvt_Fecha_Emision = Dr.GetDateTime(13),
                                                                           .Ret_TipoCodigo = Dr.GetString(14),
                                                                           .Ret_Porcentaje = Dr.GetDecimal(15),
                                                                           .Vnt_Observacion = Dr.GetString(16),
                                                                         .Ret_ImporteRetenidoTotal = Dr.GetDecimal(17),
                                                                           .Ret_ImportePagadoTotal = Dr.GetDecimal(18),
                                                                           .Cliente_Documento_Tipo = Dr.GetString(19),
                                                                           .Cliente_Documento_Numero = Dr.GetString(20),
                                                                           .Cliente_RazonSocial_Nombre = Dr.GetString(21),
                                                                           .Cliente_Apellido_Primero = Dr.GetString(22),
                                                                           .Cliente_Apellido_Segundo = Dr.GetString(23),
                                                                           .Ref_Documento_St = Dr.GetString(24),
                                                                           .Ref_Documento_Serie = Dr.GetString(25),
                                                                           .Ref_Documento_Numero = Dr.GetString(26),
                                                                           .Ref_Documento_FechaEmision = Dr.GetDateTime(27),
                                                                           .Ref_Documento_ImporteTotal = Dr.GetDecimal(28),
                                                                           .Ref_Documento_MonedaInternacional = Dr.GetString(29),
                                                                           .Ref_Pag_Codigo = Dr.GetInt32(30), .Ref_Pag_Fecha = Dr.GetDateTime(31),
                                                                           .Ref_Pag_ImporteSinRetencion = Dr.GetDecimal(32),
                                                                           .Ref_Ret_ImporteRetenido = Dr.GetDecimal(33),
                                                                           .Ref_Ret_ImporteTotalInclRetencion = Dr.GetDecimal(34),
                                                                           .Ref_Ret_TC_Aplicado = Dr.GetDecimal(35),
                                                                           .Emisor_Codigo = Dr.GetString(36)
                                                                          })
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Percepcion_DocVenta(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())

                Using Cmd As New SqlCommand("Up_FACTE_VTS_MOVIMIENTOS_CAB_BOLETAS_RESUMEN_SINENVIAR", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_RazonSocial_Nombre = Dr.GetString(0),
                                                                         .Emisor_RUC = Dr.GetString(1),
                                                                         .Emisor_Direccion_Ubigeo = Dr.GetString(2),
                                                                         .Emisor_Direccion_Calle = Dr.GetString(3),
                                                                         .Emisor_Direccion_Urbanizacion = Dr.GetString(4),
                                                                         .Emisor_Direccion_Departamento = Dr.GetString(5),
                                                                         .Emisor_Direccion_Provincia = Dr.GetString(6),
                                                                         .Emisor_Direccion_Distrito = Dr.GetString(7),
                                                                         .Emisor_Direccion_PaisCodigo = Dr.GetString(8),
                                                                           .Dvt_Fecha_Generacion = Dr.GetDateTime(9),
                                                                           .Rs_Correlativo = Dr.GetString(10),
                                                                           .Rs_TotalResumen = Dr.GetDecimal(11),
                                                                           .Dvt_Moneda_Internacional = Dr.GetString(12),
                                                                           .Dvt_Fecha_Emision = Dr.GetDateTime(13),
                                                                         .TpDc_Codigo_St = Dr.GetString(14),
                                                                         .Dvt_VTSerie = Dr.GetString(15),
                                                                           .Rs_NumeroInicio = Dr.GetString(16),
                                                                           .Rs_NumeroFin = Dr.GetString(17),
                                                                           .Dvt_Total_Importe = Dr.GetDecimal(18),
                                                                         .Dvt_Total_IGV = Dr.GetDecimal(19),
                                                                           .Dvt_Total_Gravado = Dr.GetDecimal(20),
                                                                           .Dvt_Total_Exonerado = Dr.GetDecimal(21),
                                                                           .Dvt_Total_Inafecto = Dr.GetDecimal(22),
                                                                           .Dvt_Total_ISC = Dr.GetDecimal(23),
                                                                           .Dvt_Total_Otros = Dr.GetDecimal(24),
                                                                           .Rs_NumeroLinea = Dr.GetString(25), .Emisor_Codigo = Dr.GetString(26)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    '----------
    Public Function Actualizar_DatosReceptor(ByVal pEntidad As eReceptor) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_ACTUALIZAR_CORREO_RECEPTOR", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PReceptor_NumDoc", SqlDbType.VarChar, 11).Value = pEntidad.Ruc
                    Cmd.Parameters.Add("@PReceptor_Email", SqlDbType.VarChar, 50).Value = pEntidad.Email
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Buscar_BoletaVentaFactura_Pendientes_Baja(pTarea As eProgramacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("UP_FACTE_VOIDED_LISTA_BOLETASVENTA_PENDIENTES", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@pEmp_RUC", SqlDbType.Char, 11).Value = pTarea.Emisor_RUC
                    If pTarea.Progra_Appli_Filtra_Serie And Not String.IsNullOrEmpty(pTarea.Progra_Appli_Serie) Then
                        Cmd.Parameters.Add("@pDocumentoSerie", SqlDbType.Char, 4).Value = pTarea.Progra_Appli_Serie
                    Else
                        Cmd.Parameters.Add("@pDocumentoSerie", SqlDbType.Char, 4).Value = Nothing
                    End If
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_Codigo = Dr.GetString(0), .Dvt_Fecha_Emision = Dr.GetDateTime(1)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_BoletaVentaFactura_Pendientes_Documentos_Baja(pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim ListaItems As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)

                Using Cmd As New SqlCommand("UP_FACTE_VOIDED_LISTA_PENDIENTES_GRUPO_EMISOR_FECHA", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = pEntidad.Emisor_Codigo
                    Cmd.Parameters.Add("@PVnt_NPFecha", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Emision
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eProvisionFacturacion With {.Emisor_Codigo = Dr.GetString(0), .Emisor_RazonSocial_Nombre = Dr.GetString(1), .Emisor_RUC = Dr.GetString(2), .Item = Dr.GetInt32(3), .TpDc_Codigo_St = Dr.GetString(4), .Dvt_Fecha_Emision = Dr.GetDateTime(5), .Dvt_VTSerie = Dr.GetString(6), .Dvt_VTNumer = Dr.GetString(7), .Vnt_AnulacionRazon = Dr.GetString(8), .Vnt_AnulacionFecha = Dr.GetDateTime(9)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_Baja(ByVal pDocumentos As List(Of eProvisionFacturacion)) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_VOIDED_PENDIENTE_CAMBIO_ESTADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    For Each Documento In pDocumentos
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "UP_FACTE_VOIDED_PENDIENTE_CAMBIO_ESTADO"
                        Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = Documento.Emisor_Codigo
                        Cmd.Parameters.Add("@PTpDc_Sunat", SqlDbType.Char, 2).Value = Documento.TpDc_Codigo_St
                        Cmd.Parameters.Add("@PVnt_NPFecha", SqlDbType.Date).Value = Documento.Dvt_Fecha_Emision
                        Cmd.Parameters.Add("@PVnt_NPSerie", SqlDbType.Char, 4).Value = Documento.Dvt_VTSerie
                        Cmd.Parameters.Add("@PVnt_NPNumer", SqlDbType.Char, 8).Value = Documento.Dvt_VTNumer
                        Cmd.Parameters.Add("@PEstado", SqlDbType.Char, 4).Value = pDocumentos(0).EstadoElectronico

                        Cmd.ExecuteNonQuery()
                    Next
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_Baja_Ticket(ByVal pDocumentos As List(Of eProvisionFacturacion)) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_VOIDED_ENVIADOS_CAMBIO_ESTADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    For Each Documento In pDocumentos
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "UP_FACTE_VOIDED_ENVIADOS_CAMBIO_ESTADO"
                        Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = Documento.Emisor_Codigo
                        Cmd.Parameters.Add("@PTpDc_Sunat", SqlDbType.Char, 2).Value = Documento.TpDc_Codigo_St
                        Cmd.Parameters.Add("@PVnt_NPSerie", SqlDbType.Char, 4).Value = Documento.Dvt_VTSerie
                        Cmd.Parameters.Add("@PVnt_NPNumer", SqlDbType.Char, 8).Value = Documento.Dvt_VTNumer
                        Cmd.Parameters.Add("@PEstado", SqlDbType.Char, 4).Value = pDocumentos(0).EstadoElectronico

                        Cmd.ExecuteNonQuery()
                    Next
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
