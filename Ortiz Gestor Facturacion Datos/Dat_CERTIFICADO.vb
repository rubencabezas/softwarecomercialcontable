﻿Imports System.Data.SqlClient

Public Class Dat_CERTIFICADO
    Public Function Buscar(ByVal pEmp_RUC As String, pFechaEmision As Date, pTipDoc As String) As eCertificadoCredencial
        Dim rCertificado As eCertificadoCredencial
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_CERTIFICADO_OBTENER_VIGENTE", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEmp_RUC
                    Cmd.Parameters.Add("@PFecha_Emision", SqlDbType.Date).Value = pFechaEmision
                    Cmd.Parameters.Add("@PTipoDoc_Codigo", SqlDbType.Char, 2).Value = pTipDoc
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim tCertificado As New eCertificadoCredencial
                            tCertificado.Ruta = Dr.GetString(0)
                            tCertificado.Contrasena = Dr.GetString(1)

                            tCertificado.Sunat_Usuario = Dr.GetString(2)
                            tCertificado.Sunat_Contrasena = Dr.GetString(3)

                            tCertificado.Correo_Usuario = Dr.GetString(4)
                            tCertificado.Correo_Contrasena = Dr.GetString(5)
                            tCertificado.Correo_Servidor_SMTP = Dr.GetString(6)
                            tCertificado.Correo_Servidor_Puerto = Dr.GetString(7)
                            tCertificado.Correo_Servidor_SSL = Dr.GetBoolean(8)

                            tCertificado.Etapa_Codigo = Dr.GetString(9)

                            tCertificado.Path_Root_App = Dr.GetString(10)
                            'tCertificado.PathServer = Dr.GetString(11)

                            tCertificado.FTP_Direccion = Dr.GetString(11)
                            tCertificado.FTP_Carpeta = Dr.GetString(12)
                            tCertificado.FTP_Usuario = Dr.GetString(13)
                            tCertificado.FTP_Contrasena = Dr.GetString(14)

                            tCertificado.Sunat_Autorizacion = Dr.GetString(15)
                            tCertificado.Emisor_Web_Visualizacion = Dr.GetString(16)

                            tCertificado.Codigo = Dr.GetString(17)

                            tCertificado.Valido_Desde = Dr.GetDateTime(18)
                            tCertificado.Valido_Hasta = Dr.GetDateTime(19)

                            tCertificado.EsPropio = Dr.GetBoolean(20)
                            tCertificado.Propietario_RUC = Dr.GetString(21)
                            tCertificado.Propietario_RazonSocial = Dr.GetString(22)

                            tCertificado.Sunat_Servicio_URL = Dr.GetString(23)

                            tCertificado.Emisor_RUC = pEmp_RUC

                            rCertificado = tCertificado
                            Exit While
                        End While
                    End Using
                End Using
            End Using
            Return rCertificado
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar(ByVal pEmp_RUC As String) As eCertificadoCredencial
        Dim rCertificado As eCertificadoCredencial
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_CERTIFICADO_OBTENER", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEmp_RUC
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim tCertificado As New eCertificadoCredencial

                            tCertificado.Sunat_Usuario = Dr.GetString(0)
                            tCertificado.Sunat_Contrasena = Dr.GetString(1)

                            tCertificado.Correo_Usuario = Dr.GetString(2)
                            tCertificado.Correo_Contrasena = Dr.GetString(3)
                            tCertificado.Correo_Servidor_SMTP = Dr.GetString(4)
                            tCertificado.Correo_Servidor_Puerto = Dr.GetString(5)
                            tCertificado.Correo_Servidor_SSL = Dr.GetBoolean(6)

                            tCertificado.Etapa_Codigo = Dr.GetString(7)

                            tCertificado.Path_Root_App = Dr.GetString(8)
                            'tCertificado.PathServer = Dr.GetString(9)

                            tCertificado.FTP_Direccion = Dr.GetString(9)
                            tCertificado.FTP_Carpeta = Dr.GetString(10)
                            tCertificado.FTP_Usuario = Dr.GetString(11)
                            tCertificado.FTP_Contrasena = Dr.GetString(12)

                            tCertificado.Sunat_Autorizacion = Dr.GetString(13)
                            tCertificado.Emisor_Web_Visualizacion = Dr.GetString(14)

                            tCertificado.Emisor_RUC = pEmp_RUC

                            rCertificado = tCertificado
                            Exit While
                        End While
                    End Using
                End Using
            End Using
            Return rCertificado
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista(ByVal pEmp_RUC As String) As List(Of eCertificadoCredencial)
        Dim rCertificados As New List(Of eCertificadoCredencial)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_CERTIFICADO_LISTA", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEmp_RUC
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim tCertificado As New eCertificadoCredencial
                            tCertificado.Emisor_RUC = Dr.GetString(0)
                            tCertificado.Correlativo = Dr.GetInt32(1)
                            tCertificado.Codigo = Dr.GetString(2)
                            tCertificado.Valido_Desde = Dr.GetDateTime(3)
                            tCertificado.Valido_Hasta = Dr.GetDateTime(4)
                            tCertificado.Contrasena = Dr.GetString(5)
                            tCertificado.Ruta = Dr.GetString(6)
                            tCertificado.EsPropio = Dr.GetBoolean(7)
                            tCertificado.Propietario_RUC = Dr.GetString(8)
                            tCertificado.Propietario_RazonSocial = Dr.GetString(9)

                            rCertificados.Add(tCertificado)
                        End While
                    End Using
                End Using
            End Using
            Return rCertificados
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub Insertar(ByVal pEntidad As eCertificadoCredencial)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_CERTIFICADO_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PCert_Correlativo", SqlDbType.Int).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PCert_Codigo", SqlDbType.VarChar, 15).Value = pEntidad.Codigo
                    Cmd.Parameters.Add("@PCert_Vigencia_Desde", SqlDbType.Date).Value = pEntidad.Valido_Desde
                    Cmd.Parameters.Add("@PCert_Vigencia_Hasta", SqlDbType.Date).Value = pEntidad.Valido_Hasta
                    Cmd.Parameters.Add("@PCert_Contrasena", SqlDbType.VarChar, 100).Value = pEntidad.Contrasena
                    Cmd.Parameters.Add("@PCert_Ruta", SqlDbType.VarChar).Value = pEntidad.Ruta
                    Cmd.Parameters.Add("@PCert_EsPropio", SqlDbType.Bit).Value = pEntidad.EsPropio
                    Cmd.Parameters.Add("@PCert_Ext_RUC", SqlDbType.VarChar).Value = pEntidad.Propietario_RUC
                    Cmd.Parameters.Add("@PCert_Ext_RazonSocial", SqlDbType.VarChar).Value = pEntidad.Propietario_RazonSocial
                    'Cmd.Parameters.Add("@PCert_DigestValue", SqlDbType.VarChar).Value = pEntidad.Firma_DigestValue
                    'Cmd.Parameters.Add("@PCert_SignatureValue", SqlDbType.VarChar).Value = pEntidad.Firma_SignatureValue
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pEntidad.Correlativo = Cmd.Parameters("@PCert_Correlativo").Value
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Actualizar(ByVal pEntidad As eCertificadoCredencial)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_CERTIFICADO_UPDATE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PCert_Correlativo", SqlDbType.Int).Value = pEntidad.Correlativo
                    Cmd.Parameters.Add("@PCert_Codigo", SqlDbType.VarChar, 15).Value = pEntidad.Codigo
                    Cmd.Parameters.Add("@PCert_Vigencia_Desde", SqlDbType.Date).Value = pEntidad.Valido_Desde
                    Cmd.Parameters.Add("@PCert_Vigencia_Hasta", SqlDbType.Date).Value = pEntidad.Valido_Hasta
                    Cmd.Parameters.Add("@PCert_Contrasena", SqlDbType.VarChar, 100).Value = pEntidad.Contrasena
                    Cmd.Parameters.Add("@PCert_Ruta", SqlDbType.VarChar).Value = pEntidad.Ruta
                    Cmd.Parameters.Add("@PCert_EsPropio", SqlDbType.Bit).Value = pEntidad.EsPropio
                    Cmd.Parameters.Add("@PCert_Ext_RUC", SqlDbType.VarChar).Value = pEntidad.Propietario_RUC
                    Cmd.Parameters.Add("@PCert_Ext_RazonSocial", SqlDbType.VarChar).Value = pEntidad.Propietario_RazonSocial
                    'Cmd.Parameters.Add("@PCert_DigestValue", SqlDbType.VarChar).Value = pEntidad.Firma_DigestValue
                    'Cmd.Parameters.Add("@PCert_SignatureValue", SqlDbType.VarChar).Value = pEntidad.Firma_SignatureValue
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Eliminar(ByVal pEntidad As eCertificadoCredencial)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_CERTIFICADO_DELETE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PCert_Correlativo", SqlDbType.Int).Value = pEntidad.Correlativo
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
End Class
