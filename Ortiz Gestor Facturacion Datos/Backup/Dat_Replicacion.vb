﻿Imports System.Data.SqlClient

Public Class Dat_Replicacion
    'Listas
    Public Function Replicar_Pendientes_Socios_Negocio() As List(Of BACK_01_FACTE_SOCIO_NEGOCIO)
        Dim Lista_Socios As New List(Of BACK_01_FACTE_SOCIO_NEGOCIO)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_BACK_01_REPLICA_FACTE_SOCIO_NEGOCIO", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()

                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim bSocio As New BACK_01_FACTE_SOCIO_NEGOCIO
                            bSocio.TipoDocIdent_Codigo = Dr.GetString(0)
                            bSocio.SocNeg_TipoDocIdent_Numero = Dr.GetString(1)
                            bSocio.SocNeg_RazonSocialNombres = Dr.GetString(2)
                            bSocio.SocNeg_Correo_Electronico = Dr.GetString(3)
                            Lista_Socios.Add(bSocio)
                        End While
                    End Using
                End Using
            End Using

            Return Lista_Socios
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Replicar_Pendientes_Documentos_Cabecera() As List(Of BACK_02_FACTE_DOCUMENTO_ELECTRONICO)
        Dim Lista_Documentos As New List(Of BACK_02_FACTE_DOCUMENTO_ELECTRONICO)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_BACK_02_REPLICA_FACTE_DOCUMENTO_ELECTRONICO", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()

                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim bDocumento As New BACK_02_FACTE_DOCUMENTO_ELECTRONICO
                            bDocumento.Doc_Emisor_RUC = Dr.GetString(0)
                            bDocumento.Doc_Tipo = Dr.GetString(1)
                            bDocumento.Doc_Serie = Dr.GetString(2)
                            bDocumento.Doc_Numero = Dr.GetString(3)
                            bDocumento.Doc_FechaEmision = Dr.GetDateTime(4)
                            bDocumento.Doc_Moneda = Dr.GetString(5)
                            bDocumento.Doc_Oper_Gravadas = Dr.GetDecimal(6)
                            bDocumento.Doc_Oper_Exoneradas = Dr.GetDecimal(7)
                            bDocumento.Doc_Oper_Inafectas = Dr.GetDecimal(8)
                            bDocumento.Doc_Oper_Gratuitas = Dr.GetDecimal(9)
                            bDocumento.Doc_ISC = Dr.GetDecimal(10)
                            bDocumento.Doc_Otros_Tributos = Dr.GetDecimal(11)
                            bDocumento.Doc_Otros_Conceptos = Dr.GetDecimal(12)
                            bDocumento.Doc_IGV = Dr.GetDecimal(13)
                            bDocumento.Doc_Importe = Dr.GetDecimal(14)
                            bDocumento.Doc_Certificado_Correlativo = Dr.GetInt32(15)
                            bDocumento.Doc_Cliente_TipoDocIdent_Codigo = Dr.GetString(16)
                            bDocumento.Doc_Cliente_TipoDocIdent_Numero = Dr.GetString(17)
                            bDocumento.Doc_Modif_Doc_Tipo = Dr.GetString(18)
                            bDocumento.Doc_Modif_Doc_Serie = Dr.GetString(19)
                            bDocumento.Doc_Modif_Doc_Numero = Dr.GetString(20)
                            bDocumento.Doc_NombreArchivoXML = Dr.GetString(21)
                            bDocumento.Doc_NombreArchivoPDF = Dr.GetString(22)
                            bDocumento.Doc_NombreArchivoWeb = Dr.GetString(23)
                            bDocumento.Doc_Resumen_FechaGeneracion = Dr.GetDateTime(24)
                            bDocumento.Doc_Resumen_Correlativo = Dr.GetInt32(25)
                            bDocumento.Doc_Baja_GrpTipo_Codigo = Dr.GetString(26)
                            bDocumento.Doc_Baja_FechaGeneracion = Dr.GetDateTime(27)
                            bDocumento.Doc_Baja_Correlativo = Dr.GetInt32(28)
                            bDocumento.Doc_Correo_Enviado = Dr.GetBoolean(29)
                            bDocumento.Doc_Correo_EstaLeido = Dr.GetBoolean(30)
                            bDocumento.Doc_Correo_ContadorLectura = Dr.GetInt32(31)
                            bDocumento.Doc_Correo_LecturaFechaPrimera = Dr.GetDateTime(32)
                            bDocumento.Doc_Correo_LecturaFechaUltima = Dr.GetDateTime(33)
                            bDocumento.EstDoc_Codigo = Dr.GetString(34)
                            bDocumento.Path_Root_App = Dr.GetString(35)
                            bDocumento.EstEnv_Codigo = Dr.GetString(36) 'Para copiar en generado, aceptado o rechazado
                            Lista_Documentos.Add(bDocumento)
                        End While
                    End Using
                End Using
            End Using

            Return Lista_Documentos
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Replicar_Pendientes_Documentos_Adjunto(Cls_Cab As BACK_02_FACTE_DOCUMENTO_ELECTRONICO) As List(Of BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO)
        Dim Lista_Adjuntos As New List(Of BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_BACK_03_REPLICA_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Cls_Cab.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Cls_Cab.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Cls_Cab.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Cls_Cab.Doc_Numero
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim bAdjunto As New BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO
                            bAdjunto.Doc_Emisor_RUC = Dr.GetString(0)
                            bAdjunto.Doc_Tipo = Dr.GetString(1)
                            bAdjunto.Doc_Serie = Dr.GetString(2)
                            bAdjunto.Doc_Numero = Dr.GetString(3)
                            bAdjunto.XML_Archivo_Item = Dr.GetInt32(4)
                            bAdjunto.XML_Archivo_NombreLocal = Dr.GetString(5)
                            bAdjunto.XML_Archivo_Nombre = Dr.GetString(6)
                            bAdjunto.Aud_Registro_FechaHora = Dr.GetDateTime(7)
                            bAdjunto.Aud_Registro_Equipo = Dr.GetString(8)
                            bAdjunto.Aud_Registro_Usuario = Dr.GetString(9)
                            bAdjunto.Path_Root_App = Dr.GetString(10)
                            Lista_Adjuntos.Add(bAdjunto)
                        End While
                    End Using
                End Using
            End Using

            Return Lista_Adjuntos
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Replicar_Pendientes_Documentos_Cliente(Cls_Cab As BACK_02_FACTE_DOCUMENTO_ELECTRONICO) As List(Of BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE)
        Dim Lista_XML_Cliente As New List(Of BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_BACK_04_REPLICA_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE", Cn)
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Cls_Cab.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Cls_Cab.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Cls_Cab.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Cls_Cab.Doc_Numero
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Dim bArchivoCliente As New BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
                            bArchivoCliente.Doc_Emisor_RUC = Dr.GetString(0)
                            bArchivoCliente.Doc_Tipo = Dr.GetString(1)
                            bArchivoCliente.Doc_Serie = Dr.GetString(2)
                            bArchivoCliente.Doc_Numero = Dr.GetString(3)
                            bArchivoCliente.XML_Correlativo = Dr.GetInt32(4)
                            bArchivoCliente.XML_Reciente = Dr.GetBoolean(5)
                            bArchivoCliente.XML_Archivo = Dr.GetString(6)
                            bArchivoCliente.TipXML_Codigo = Dr.GetString(7)
                            bArchivoCliente.EstEnv_Codigo = Dr.GetString(8)
                            bArchivoCliente.XML_DigestValue = Dr.GetString(9)
                            bArchivoCliente.XML_SignatureValue = Dr.GetString(10)
                            bArchivoCliente.SUNAT_Respuesta_Codigo = Dr.GetString(11)
                            bArchivoCliente.XML_MotivoBaja = Dr.GetString(12)
                            bArchivoCliente.XML_FilaActiva = Dr.GetBoolean(13)
                            bArchivoCliente.XML_EnviadoFTP = Dr.GetBoolean(14)
                            bArchivoCliente.XML_Revercion_Anulacion = Dr.GetBoolean(15)
                            bArchivoCliente.XML_Revercion_Anulacion_FechaHora = Dr.GetDateTime(16)
                            bArchivoCliente.XML_Revercion_Anulacion_Razon = Dr.GetString(17)
                            bArchivoCliente.XML_Creado_FechaHora = Dr.GetDateTime(18)
                            bArchivoCliente.XML_Enviado_FechaHora = Dr.GetDateTime(19)
                            bArchivoCliente.Aud_Registro_FechaHora = Dr.GetDateTime(20)
                            bArchivoCliente.Aud_Registro_Equipo = Dr.GetString(21)
                            bArchivoCliente.Aud_Registro_Usuario = Dr.GetString(22)
                            bArchivoCliente.Path_Root_App = Dr.GetString(23)
                            Lista_XML_Cliente.Add(bArchivoCliente)
                        End While
                    End Using
                End Using
            End Using

            Return Lista_XML_Cliente
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    'Inserciones y Actualizaciones
    Public Function Replicar_Pendiente_ServidorX_Socio_Negocio(CnStr As String, Pend As BACK_01_FACTE_SOCIO_NEGOCIO) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(CnStr)
            Try
                Using Cmd As New SqlCommand("UP_BACK_01_UP_FACTE_SOCIO_NEGOCIO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PTipoDocIdent_Codigo", SqlDbType.Char, 1).Value = Pend.TipoDocIdent_Codigo
                    Cmd.Parameters.Add("@PSocNeg_TipoDocIdent_Numero", SqlDbType.VarChar, 15).Value = Pend.SocNeg_TipoDocIdent_Numero
                    Cmd.Parameters.Add("@PSocNeg_RazonSocialNombres", SqlDbType.VarChar, 150).Value = Pend.SocNeg_RazonSocialNombres
                    Cmd.Parameters.Add("@PSocNeg_Correo_Electronico", SqlDbType.VarChar, 150).Value = Pend.SocNeg_Correo_Electronico
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function
    Public Function Replicar_Pendiente_Cambio_Estado_Socio_Negocio(Pend As BACK_01_FACTE_SOCIO_NEGOCIO) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("UP_BACK_01_UP_REPLICA_FACTE_SOCIO_NEGOCIO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PTipoDocIdent_Codigo", SqlDbType.Char, 1).Value = Pend.TipoDocIdent_Codigo
                    Cmd.Parameters.Add("@PSocNeg_TipoDocIdent_Numero", SqlDbType.VarChar, 15).Value = Pend.SocNeg_TipoDocIdent_Numero
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function

    Public Function Replicar_Pendiente_ServidorX_Documentos_Cabecera(CnStr As String, Pend As BACK_02_FACTE_DOCUMENTO_ELECTRONICO) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(CnStr)
            Try
                Using Cmd As New SqlCommand("UP_BACK_02_UP_FACTE_DOCUMENTO_ELECTRONICO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Pend.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Numero
                    Cmd.Parameters.Add("@PDoc_FechaEmision", SqlDbType.Date).Value = Pend.Doc_FechaEmision
                    Cmd.Parameters.Add("@PDoc_Moneda", SqlDbType.Char, 3).Value = Pend.Doc_Moneda
                    Cmd.Parameters.Add("@PDoc_Oper_Gravadas", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Oper_Gravadas
                    Cmd.Parameters.Add("@PDoc_Oper_Exoneradas", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Oper_Exoneradas
                    Cmd.Parameters.Add("@PDoc_Oper_Inafectas", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Oper_Inafectas
                    Cmd.Parameters.Add("@PDoc_Oper_Gratuitas", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Oper_Gratuitas
                    Cmd.Parameters.Add("@PDoc_ISC", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_ISC
                    Cmd.Parameters.Add("@PDoc_Otros_Tributos", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Otros_Tributos
                    Cmd.Parameters.Add("@PDoc_Otros_Conceptos", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Otros_Conceptos
                    Cmd.Parameters.Add("@PDoc_IGV", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_IGV
                    Cmd.Parameters.Add("@PDoc_Importe", SqlDbType.Decimal, 15, 2).Value = Pend.Doc_Importe
                    Cmd.Parameters.Add("@PDoc_Certificado_Correlativo", SqlDbType.Int).Value = Pend.Doc_Certificado_Correlativo
                    Cmd.Parameters.Add("@PDoc_Cliente_TipoDocIdent_Codigo", SqlDbType.Char, 1).Value = Pend.Doc_Cliente_TipoDocIdent_Codigo
                    Cmd.Parameters.Add("@PDoc_Cliente_TipoDocIdent_Numero", SqlDbType.VarChar, 15).Value = Pend.Doc_Cliente_TipoDocIdent_Numero
                    Cmd.Parameters.Add("@PDoc_Modif_Doc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Modif_Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Modif_Doc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Modif_Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Modif_Doc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Modif_Doc_Numero
                    Cmd.Parameters.Add("@PDoc_NombreArchivoXML", SqlDbType.VarChar, 50).Value = Pend.Doc_NombreArchivoXML
                    Cmd.Parameters.Add("@PDoc_NombreArchivoPDF", SqlDbType.VarChar, 50).Value = Pend.Doc_NombreArchivoPDF
                    Cmd.Parameters.Add("@PDoc_NombreArchivoWeb", SqlDbType.VarChar, 50).Value = Pend.Doc_NombreArchivoWeb
                    Cmd.Parameters.Add("@PDoc_Resumen_FechaGeneracion", SqlDbType.Date).Value = Pend.Doc_Resumen_FechaGeneracion
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Value = Pend.Doc_Resumen_Correlativo
                    Cmd.Parameters.Add("@PDoc_Baja_GrpTipo_Codigo", SqlDbType.Char, 3).Value = Pend.Doc_Baja_GrpTipo_Codigo
                    Cmd.Parameters.Add("@PDoc_Baja_FechaGeneracion", SqlDbType.Date).Value = Pend.Doc_Baja_FechaGeneracion
                    Cmd.Parameters.Add("@PDoc_Baja_Correlativo", SqlDbType.Int).Value = Pend.Doc_Baja_Correlativo
                    Cmd.Parameters.Add("@PDoc_Correo_Enviado", SqlDbType.Bit).Value = Pend.Doc_Correo_Enviado
                    Cmd.Parameters.Add("@PDoc_Correo_EstaLeido", SqlDbType.Bit).Value = Pend.Doc_Correo_EstaLeido
                    Cmd.Parameters.Add("@PDoc_Correo_ContadorLectura", SqlDbType.Int).Value = Pend.Doc_Correo_ContadorLectura
                    Cmd.Parameters.Add("@PDoc_Correo_LecturaFechaPrimera", SqlDbType.DateTime).Value = Pend.Doc_Correo_LecturaFechaPrimera
                    Cmd.Parameters.Add("@PDoc_Correo_LecturaFechaUltima", SqlDbType.DateTime).Value = Pend.Doc_Correo_LecturaFechaUltima
                    Cmd.Parameters.Add("@PEstDoc_Codigo", SqlDbType.Char, 3).Value = Pend.EstDoc_Codigo
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function
    Public Function Replicar_Pendiente_Cambio_Estado_Documentos_Cabecera(Pend As BACK_02_FACTE_DOCUMENTO_ELECTRONICO) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("UP_BACK_02_REPLICA_UP_FACTE_DOCUMENTO_ELECTRONICO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Pend.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Numero
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function

    Public Function Replicar_Pendiente_ServidorX_Documentos_Adjunto(CnStr As String, Pend As BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(CnStr)
            Try
                Using Cmd As New SqlCommand("UP_BACK_03_UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Pend.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Numero
                    Cmd.Parameters.Add("@PXML_Archivo_Item", SqlDbType.Int).Value = Pend.XML_Archivo_Item
                    Cmd.Parameters.Add("@PXML_Archivo_NombreLocal", SqlDbType.VarChar, 30).Value = Pend.XML_Archivo_NombreLocal
                    Cmd.Parameters.Add("@PXML_Archivo_Nombre", SqlDbType.VarChar, 150).Value = Pend.XML_Archivo_Nombre
                    Cmd.Parameters.Add("@PAud_Registro_FechaHora", SqlDbType.DateTime).Value = Pend.Aud_Registro_FechaHora
                    Cmd.Parameters.Add("@PAud_Registro_Equipo", SqlDbType.VarChar, 50).Value = Pend.Aud_Registro_Equipo
                    Cmd.Parameters.Add("@PAud_Registro_Usuario", SqlDbType.VarChar, 50).Value = Pend.Aud_Registro_Usuario
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function
    Public Function Replicar_Pendiente_Cambio_Estado_Documentos_Adjunto(Pend As BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("UP_BACK_03_REPLICA_UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Pend.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Numero
                    Cmd.Parameters.Add("@PXML_Archivo_Item", SqlDbType.Int).Value = Pend.XML_Archivo_Item
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function

    Public Function Replicar_Pendiente_ServidorX_Documentos_XMLCliente(CnStr As String, Pend As BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(CnStr)
            Try
                Using Cmd As New SqlCommand("UP_BACK_04_UP_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Pend.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Numero
                    Cmd.Parameters.Add("@PXML_Correlativo", SqlDbType.Int).Value = Pend.XML_Correlativo
                    Cmd.Parameters.Add("@PXML_Reciente", SqlDbType.Bit).Value = Pend.XML_Reciente
                    Cmd.Parameters.Add("@PXML_Archivo", SqlDbType.VarChar, 150).Value = Pend.XML_Archivo
                    Cmd.Parameters.Add("@PTipXML_Codigo", SqlDbType.Char, 3).Value = Pend.TipXML_Codigo
                    Cmd.Parameters.Add("@PEstEnv_Codigo", SqlDbType.Char, 3).Value = Pend.EstEnv_Codigo
                    Cmd.Parameters.Add("@PXML_DigestValue", SqlDbType.VarChar, 50).Value = Pend.XML_DigestValue
                    Cmd.Parameters.Add("@PXML_SignatureValue", SqlDbType.VarChar, 500).Value = Pend.XML_SignatureValue
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Codigo", SqlDbType.Char, 4).Value = Pend.SUNAT_Respuesta_Codigo
                    Cmd.Parameters.Add("@PXML_MotivoBaja", SqlDbType.VarChar, 100).Value = Pend.XML_MotivoBaja
                    Cmd.Parameters.Add("@PXML_FilaActiva", SqlDbType.Bit).Value = Pend.XML_FilaActiva
                    Cmd.Parameters.Add("@PXML_EnviadoFTP", SqlDbType.Bit).Value = Pend.XML_EnviadoFTP
                    Cmd.Parameters.Add("@PXML_Revercion_Anulacion", SqlDbType.Bit).Value = Pend.XML_Revercion_Anulacion
                    Cmd.Parameters.Add("@PXML_Revercion_Anulacion_FechaHora", SqlDbType.DateTime).Value = Pend.XML_Revercion_Anulacion_FechaHora
                    Cmd.Parameters.Add("@PXML_Revercion_Anulacion_Razon", SqlDbType.VarChar, 150).Value = Pend.XML_Revercion_Anulacion_Razon
                    Cmd.Parameters.Add("@PXML_Creado_FechaHora", SqlDbType.DateTime).Value = Pend.XML_Creado_FechaHora
                    Cmd.Parameters.Add("@PXML_Enviado_FechaHora", SqlDbType.DateTime).Value = Pend.XML_Enviado_FechaHora
                    Cmd.Parameters.Add("@PAud_Registro_FechaHora", SqlDbType.DateTime).Value = Pend.Aud_Registro_FechaHora
                    Cmd.Parameters.Add("@PAud_Registro_Equipo", SqlDbType.VarChar, 50).Value = Pend.Aud_Registro_Equipo
                    Cmd.Parameters.Add("@PAud_Registro_Usuario", SqlDbType.VarChar, 50).Value = Pend.Aud_Registro_Usuario
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function
    Public Function Replicar_Pendiente_Cambio_Estado_Documentos_XMLCliente(Pend As BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE) As Boolean
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("UP_BACK_04_REPLICA_UP_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Pend.Doc_Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Pend.Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Pend.Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Pend.Doc_Numero
                    Cmd.Parameters.Add("@PXML_Correlativo", SqlDbType.Int).Value = Pend.XML_Correlativo
                    Cmd.ExecuteNonQuery()
                End Using
                Trs.Commit()
                Return True
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function
End Class
