﻿Imports System.Data.SqlClient

Public Class Dat_ERRORES
    Public Function Insertar_Error(ByVal pEntidad As eProvisionFacturacion, pArchivo As fcProceso) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_TABLA_OBSERVAC_ERRORES_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PObsErr_Maestro", SqlDbType.Char, 3).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PObsErr_Codigo", SqlDbType.Char, 4).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PObsErr_Descripcion", SqlDbType.VarChar, 150).Value = pEntidad.Dvt_VTSerie
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
