﻿Imports System.Data.SqlClient

Public Class Dat_VTS_TIPO_DOC
    Public Function Buscar(ByVal Clas_Enti As eTipoDocumento) As List(Of eTipoDocumento)
        Dim ListaItems As New List(Of eTipoDocumento)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_TIPO_LISTA", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PTipoDoc_Codigo", SqlDbType.Char, 2).Value = Clas_Enti.TipoDoc_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eTipoDocumento With {.TipoDoc_Codigo = Dr.GetString(0),
                                                                  .TipoDoc_Descripcion = Dr.GetString(1)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
