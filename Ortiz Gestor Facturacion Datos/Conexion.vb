﻿Public Class Conexion

    Shared appAmbiente As String = System.Configuration.ConfigurationSettings.AppSettings("DataBase")
    Shared appAmbienteFE As String = System.Configuration.ConfigurationSettings.AppSettings("DataBaseFE")

    Shared appServerBD As String = System.Configuration.ConfigurationSettings.AppSettings("ServerBD")
    Shared appServerAP As String = System.Configuration.ConfigurationSettings.AppSettings("ServerAP")
    Shared appUser As String = System.Configuration.ConfigurationSettings.AppSettings("User")
    Shared appPassword As String = System.Configuration.ConfigurationSettings.AppSettings("Password")

    Public Shared Function ObtenerConnection() As String
        'Local
        'Return "Data Source=192.168.1.173;Initial Catalog=DB_FEGO_1;User ID=sa;Password=as1mov"

        'Prueba
        'Return "Data Source=172.16.20.160;Initial Catalog=DB_FEGO_PRUEBA;User ID=sa;Password=as1mov"

        'Produccion
        'Return "Data Source=172.16.20.160;Initial Catalog=DB_FEGO;User ID=sa;Password=as1mov"
        Return "Data Source=" + appServerBD + ";Initial Catalog=" + appAmbienteFE + ";User Id=" + appUser + ";Password=" + appPassword + ";"

        'OMAR
        'Return "Data Source=172.16.20.160;Initial Catalog=DB_FEGO_PRUEBA;User ID=sa;Password=as1mov"
    End Function

    Public Shared Function ObtenerConnectionDBMBS() As String
        'Return My.Settings.CnBMS
        Return "Data Source=" + appServerBD + ";Initial Catalog=" + appAmbiente + ";User Id=" + appUser + ";Password=" + appPassword + ";"

        ' Return "Data Source=192.168.1.16;Initial Catalog=DB_FEGO;User ID=sa;Password=as1mov"
    End Function


End Class
