﻿Imports System.Data.SqlClient
Public Class Dat_VTS_MONEDA
    Public Function Buscar(ByVal Moneda As Ent_MONEDA) As List(Of Ent_MONEDA)
        Try
            Dim ListaItems As New List(Of Ent_MONEDA)
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS())
                Using Cmd As New SqlCommand("paVTS_TIPO_MONEDA_Find", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@Empresa", SqlDbType.Char).Value = DatosActualConexion.CodigoEmpresa
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_MONEDA With {.Emp_cCodigo = Dr.GetString(0), .Mon_cCodigo = Dr.GetString(1), .Mon_cNombreLargo = Dr.GetString(2), .Mon_cNombreCorto = Dr.GetString(3), .Mon_cCodSunat = Dr.GetString(4), .Mon_EsNacional = Dr.GetBoolean(5), .Mon_Sunat_CodigoNuevo = Dr.GetString(6).Trim})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function BuscarMonedasAll() As List(Of Ent_MONEDA)
        Try
            Dim ListaItems As New List(Of Ent_MONEDA)
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS())
                Using Cmd As New SqlCommand("paVTS_TIPO_MONEDA_FindAll", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_MONEDA With {.Emp_cCodigo = Dr.GetString(0), .Mon_cCodigo = Dr.GetString(1), .Mon_cNombreLargo = Dr.GetString(2), .Mon_cNombreCorto = Dr.GetString(3), .Mon_cCodSunat = Dr.GetString(4), .Mon_EsNacional = Dr.GetBoolean(5), .Mon_Sunat_CodigoNuevo = Dr.GetString(6).Trim})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
