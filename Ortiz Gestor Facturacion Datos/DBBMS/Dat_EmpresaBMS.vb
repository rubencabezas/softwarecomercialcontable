﻿Imports System.Data.SqlClient
Public Class Dat_EmpresaBMS
    Private Shared ReadOnly _instancia As Dat_EmpresaBMS = New Dat_EmpresaBMS


    Public Shared ReadOnly Property Instancia() As Dat_EmpresaBMS
        Get
            Return _instancia
        End Get
    End Property
    Function Listar(ByVal Emp As Ent_Empresa) As List(Of Ent_Empresa)
        Dim ListaItems As New List(Of Ent_Empresa)
        Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS)
            Using Cmd As New SqlCommand("paGrlFind_ListarEmpresasDisponiblesxUser", Cn) With {.CommandType = CommandType.StoredProcedure}
                Cmd.Parameters.Add("@User", SqlDbType.Char, 11).Value = Emp.User
                Cn.Open()
                Using Dr As SqlDataReader = Cmd.ExecuteReader
                    While Dr.Read
                        ListaItems.Add(New Ent_Empresa(Dr.GetString(0),
                                                       Dr.GetString(1),
                                                       Dr.GetString(2),
                                                       Dr.GetString(3),
                                                       Dr.GetString(4),
                                                       Dr.GetString(7),
                                                       Dr.GetString(8),
                                                       Dr.GetString(9),
                                                       Dr.GetString(10),
                                                       Dr.GetString(11)))
                    End While
                End Using
            End Using
        End Using
        Return ListaItems
    End Function
End Class
