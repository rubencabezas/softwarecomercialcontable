﻿Imports System.Data.SqlClient
Public Class Dat_VTS_DOC_TRANSAC_CAB
#Region "Cotizacion"
    Public Sub Insertar_Cotizacion(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("paVTS_VTS_COTIZACIONES_CAB_Insert", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = Cls_Enti.Pdv_Codigo
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PCot_CotSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PCot_CotNumer", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PCot_CotFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PDir_Codigo", SqlDbType.Char, 4).Value = Cls_Enti.Dir_Codigo
                    Cmd.Parameters.Add("@PCon_cCondicion", SqlDbType.Char, 2).Value = Cls_Enti.Con_cCondicion
                    Cmd.Parameters.Add("@PCot_DiasCondic", SqlDbType.SmallInt).Value = Cls_Enti.Dvt_DiasCondic
                    Cmd.Parameters.Add("@PTPr_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_Cod_TipoPrc
                    Cmd.Parameters.Add("@PCot_FechaVence", SqlDbType.Date).Value = CDate(Cls_Enti.Dvt_FechaVenci)
                    Cmd.Parameters.Add("@PCot_Cod_Moneda", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_Cod_Moneda
                    Cmd.Parameters.Add("@PCot_TipCambCod", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_TipCambCod
                    Cmd.Parameters.Add("@PCot_TipCambMont", SqlDbType.Decimal, 10, 3).Value = Cls_Enti.Dvt_TipCambMont
                    Cmd.Parameters.Add("@PCot_VencCoddEnt", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_VencCoddEnt
                    Cmd.Parameters.Add("@PCot_VenCodTipoEnti", SqlDbType.Char, 1).Value = Cls_Enti.Dvt_VenCodTipoEnti
                    Cmd.Parameters.Add("@PCot_Condicion", SqlDbType.VarChar, 200).Value = Cls_Enti.Dvt_Observaciones
                    Cmd.Parameters.Add("@PCot_SubTotal", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_SubTotal
                    Cmd.Parameters.Add("@PCot_IncluIGV", SqlDbType.Bit).Value = Cls_Enti.Dvt_IncluyeIGV
                    Cmd.Parameters.Add("@PCot_PorcjIGV", SqlDbType.Decimal, 10, 2).Value = Cls_Enti.Dvt_PrcIGV
                    Cmd.Parameters.Add("@PCot_MontoIGV", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoIGV
                    Cmd.Parameters.Add("@PCot_MontoTotal", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoTotal
                    Cmd.Parameters.Add("@PCot_Correo", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_CorreoAEnviar
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.Parameters.Add("@Estado", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    Cls_Enti.Dvt_Codigo = Cmd.Parameters("@PCot_Codigo").Value
                    Cls_Enti.Dvt_Est_Fact_Ds = Cmd.Parameters("@Estado").Value
                    For Each Item As Ent_DOC_VENTA_DET In Cls_Enti.Dvt_Detalle
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "paVTS_COTIZACIONES_DET_Insert"
                        Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                        Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                        Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                        Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                        Cmd.Parameters.Add("@PCotD_NumItem", SqlDbType.SmallInt).Value = Item.DvtD_NumItem

                        Cmd.Parameters.Add("@PAlm_Codigo", SqlDbType.Char, 5).Value = Item.DvtD_AlmaCod
                        Cmd.Parameters.Add("@PCotD_TipoBSA", SqlDbType.Char, 4).Value = Item.DvtD_TipoCod

                        Cmd.Parameters.Add("@PCTC_Codigo", SqlDbType.Char, 8).Value = Item.CTC_Codigo
                        Cmd.Parameters.Add("@PCTC_DescripcionItem", SqlDbType.VarChar, 200).Value = Item.CTC_DescripcionAdicional
                        Cmd.Parameters.Add("@PCTC_CodUnidad", SqlDbType.VarChar, 4).Value = Item.DvtD_UnidadCod

                        Cmd.Parameters.Add("@PCotD_Cantidad", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_Cantidad
                        Cmd.Parameters.Add("@PCotD_ValorVenta", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_ValorVenta

                        Cmd.Parameters.Add("@PCotD_IGVTasa", SqlDbType.Decimal, 10, 3).Value = Item.DvtD_IGVTasa
                        Cmd.Parameters.Add("@PCotD_IGVMonto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_IGVMonto
                        Cmd.Parameters.Add("@PCotD_PrecNeto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_PrecUnitario
                        Cmd.Parameters.Add("@PCotD_Importe", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_MontoTotal
                        Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                        Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                        Cmd.ExecuteNonQuery()
                    Next
                    Trs.Commit()
                End Using
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
    Public Sub Modificar_Cotizacion(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("paVTS_COTIZACIONES_CAB_Update", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    'Cmd.Parameters.Add("@PCot_TipoBSA", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_TipoBSA
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = Cls_Enti.Pdv_Codigo
                    'Cmd.Parameters.Add("@PAlm_Codigo", SqlDbType.Char, 5).Value = Cls_Enti.Alm_Codigo
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PCot_CotSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PCot_CotNumer", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PCot_CotFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PEnt_DirSelect", SqlDbType.Char, 4).Value = Cls_Enti.Dir_Codigo
                    Cmd.Parameters.Add("@PCon_cCondicion", SqlDbType.Char, 2).Value = Cls_Enti.Con_cCondicion
                    Cmd.Parameters.Add("@PCot_DiasCondic", SqlDbType.SmallInt).Value = Cls_Enti.Dvt_DiasCondic
                    Cmd.Parameters.Add("@PTPr_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_Cod_TipoPrc
                    Cmd.Parameters.Add("@PCot_FechaVence", SqlDbType.Date).Value = CDate(Cls_Enti.Dvt_FechaVenci)
                    Cmd.Parameters.Add("@PCot_Cod_Moneda", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_Cod_Moneda
                    Cmd.Parameters.Add("@PCot_TipCambCod", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_TipCambCod
                    Cmd.Parameters.Add("@PCot_TipCambMont", SqlDbType.Decimal, 10, 3).Value = Cls_Enti.Dvt_TipCambMont
                    Cmd.Parameters.Add("@PCot_VencCoddEnt", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_VencCoddEnt
                    Cmd.Parameters.Add("@PCot_VenCodTipoEnti", SqlDbType.Char, 1).Value = Cls_Enti.Dvt_VenCodTipoEnti
                    Cmd.Parameters.Add("@PCot_Condicion", SqlDbType.VarChar, 200).Value = Cls_Enti.Dvt_Observaciones
                    Cmd.Parameters.Add("@PCot_Correo", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_CorreoAEnviar
                    Cmd.Parameters.Add("@PCot_SubTotal", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_SubTotal
                    Cmd.Parameters.Add("@PCot_IncluIGV", SqlDbType.Bit).Value = Cls_Enti.Dvt_IncluyeIGV
                    Cmd.Parameters.Add("@PCot_PorcjIGV", SqlDbType.Decimal, 10, 2).Value = Cls_Enti.Dvt_PrcIGV
                    Cmd.Parameters.Add("@PCot_MontoIGV", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoIGV
                    Cmd.Parameters.Add("@PCot_MontoTotal", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoTotal
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    For Each Item As Ent_DOC_VENTA_DET In Cls_Enti.Dvt_Detalle
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "paVTS_COTIZACIONES_DET_Insert"
                        Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                        Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                        Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                        Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                        Cmd.Parameters.Add("@PCotD_NumItem", SqlDbType.SmallInt).Value = Item.DvtD_NumItem

                        Cmd.Parameters.Add("@PAlm_Codigo", SqlDbType.Char, 5).Value = Item.DvtD_AlmaCod
                        Cmd.Parameters.Add("@PCotD_TipoBSA", SqlDbType.Char, 4).Value = Item.DvtD_TipoCod

                        Cmd.Parameters.Add("@PCTC_Codigo", SqlDbType.Char, 8).Value = Item.CTC_Codigo
                        Cmd.Parameters.Add("@PCTC_DescripcionItem", SqlDbType.VarChar, 200).Value = Item.CTC_DescripcionAdicional
                        Cmd.Parameters.Add("@PCTC_CodUnidad", SqlDbType.VarChar, 4).Value = Item.DvtD_UnidadCod

                        Cmd.Parameters.Add("@PCotD_Cantidad", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_Cantidad
                        Cmd.Parameters.Add("@PCotD_ValorVenta", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_ValorVenta

                        Cmd.Parameters.Add("@PCotD_IGVTasa", SqlDbType.Decimal, 10, 3).Value = Item.DvtD_IGVTasa
                        Cmd.Parameters.Add("@PCotD_IGVMonto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_IGVMonto
                        Cmd.Parameters.Add("@PCotD_PrecNeto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_PrecUnitario
                        Cmd.Parameters.Add("@PCotD_Importe", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_MontoTotal
                        Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                        Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                        Cmd.ExecuteNonQuery()
                    Next
                    Trs.Commit()
                End Using
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
    Public Sub Eliminar_Cotizacion(ByVal Clas_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("paVTS_COTIZACIONES_CAB_Deleted", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = Clas_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    Trs.Commit()
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Anular_Cotizacion(ByVal Clas_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("paVTS_COTIZACIONES_CAB_Anular", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = Clas_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Clas_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    Trs.Commit()
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function Buscar_Cotizacion(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection(1))
                Using Cmd As New SqlCommand("paVTS_VTS_COTIZACIONES_CAB_SelectDinamic", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Per_cPeriodo), DatosActualConexion.PeriodoSelect, Cls_Enti.Per_cPeriodo)
                    Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Codigo), Nothing, Cls_Enti.Dvt_Codigo)
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo), Nothing, Cls_Enti.TpDc_Codigo)
                    Cmd.Parameters.Add("@PCot_CotSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTSerie), Nothing, Cls_Enti.Dvt_VTSerie)
                    Cmd.Parameters.Add("@PCot_CotNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTNumer), Nothing, Cls_Enti.Dvt_VTNumer)
                    Cmd.Parameters.Add("@PCot_CotFecha", SqlDbType.Date).Value = IIf(Cls_Enti.Dvt_VTFecha = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_VTFecha)
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ten_cTipoEntidad), Nothing, Cls_Enti.Ten_cTipoEntidad)
                    Cmd.Parameters.Add("@PCot_VencCoddEnt", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VencCoddEnt), Nothing, Cls_Enti.Dvt_VencCoddEnt)
                    Cmd.Parameters.Add("@PCot_VenCodTipoEnti", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VenCodTipoEnti), Nothing, Cls_Enti.Dvt_VenCodTipoEnti)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_TRANSAC_CAB With {.Emp_cCodigo = Dr.GetString(0), .Pan_cAnio = Dr.GetString(1), .Per_cPeriodo = Dr.GetString(2), .Dvt_Codigo = Dr.GetString(3), .Pdv_Codigo = Dr.GetString(4), .Pdv_Descripcion = Dr.GetString(5), .TpDc_Codigo = Dr.GetString(6), .TpDc_Descripcion = Dr.GetString(7), .Dvt_VTSerie = Dr.GetString(8), .Dvt_VTNumer = Dr.GetString(9), .Dvt_VTFecha = Dr.GetDateTime(10), .Ent_cCodEntidad = Dr.GetString(11), .Ten_cTipoEntidad = Dr.GetString(12), .Ent_DNIRUC = Dr.GetString(13), .Ent_NombreRzS = Dr.GetString(14), .Ent_ApelliPri = Dr.GetString(15), .Ent_ApelliSeg = Dr.GetString(16), .Dir_Codigo = Dr.GetString(17), .Con_cCondicion = Dr.GetString(18), .Con_cDescripcion = Dr.GetString(19), .Dvt_Cod_TipoPrc = Dr.GetString(20), .Dvt_Des_TipoPrc = Dr.GetString(21), .Dvt_DiasCondic = Dr.GetInt16(22), .Dvt_FechaVenci = Dr.GetDateTime(23), .Dvt_Cod_Moneda = Dr.GetString(24), .Dvt_Des_Moneda = Dr.GetString(25), .Dvt_TipCambCod = Dr.GetString(26), .Dvt_TipCambMont = Dr.GetDecimal(27), .Dvt_VencCoddEnt = Dr.GetString(28), .Dvt_VenCodTipoEnti = Dr.GetString(29), .Dvt_VenEnt_DNIRUC = Dr.GetString(30), .Dvt_VenEnt_NombreRzS = Dr.GetString(31), .Dvt_VenEnt_ApelliPri = Dr.GetString(32), .Dvt_VenEnt_ApelliSeg = Dr.GetString(33), .Dvt_Observaciones = Dr.GetString(34), .Dvt_SubTotal = Dr.GetDecimal(35), .Dvt_IncluyeIGV = Dr.GetBoolean(36), .Dvt_PrcIGV = Dr.GetDecimal(37), .Dvt_MntoIGV = Dr.GetDecimal(38), .Dvt_MntoTotal = Dr.GetDecimal(39), .Dvt_Est_Fact = Dr.GetString(40), .Dvt_Est_Fact_Ds = Dr.GetString(41), .Dvt_Est_Fila = Dr.GetString(42), .Dvt_CorreoAEnviar = Dr.GetString(43)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Detalle_Cotizacion(ByVal Clas_Enti As Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_VENTA_DET)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection(1))
                Using Cmd As New SqlCommand("paVTS_COTIZACIONES_DET_Select", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PCot_Codigo", SqlDbType.Char, 10).Value = Clas_Enti.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_VENTA_DET With {.Emp_cCodigo = Dr.GetString(0), .Pan_cAnio = Dr.GetString(1), .Per_cPeriodo = Dr.GetString(2), .Dvt_Codigo = Dr.GetString(3), .DvtD_NumItem = Dr.GetInt16(4), .CTC_Codigo = Dr.GetString(5), .CTC_DescripcionItem = Dr.GetString(6), .CTC_DescripcionAdicional = Dr.GetString(7), .DvtD_UnidadCod = Dr.GetString(8), .DvtD_UnidadDs = Dr.GetString(9), .DvtD_Cantidad = Dr.GetDecimal(10), .DvtD_ValorVenta = Dr.GetDecimal(11), .DvtD_IGVTasaPorcentaje = (Dr.GetDecimal(12) * 100), .DvtD_AlmaCod = Dr.GetString(17), .DvtD_AlmaDes = Dr.GetString(18), .DvtD_TipoCod = Dr.GetString(19), .DvtD_TipoOfi = Dr.GetString(20), .DvtD_TipoDes = Dr.GetString(21)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "Nota Pedido"
    Public Sub Insertar_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("paVTS_VTS_NOTA_PEDIDO_CAB_Insert", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = Cls_Enti.Pdv_Codigo
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PNoP_NPSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PNoP_NPNumer", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PNoP_NPFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PTpr_cCodigo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_Cod_TipoPrc
                    Cmd.Parameters.Add("@PMon_cCodigo", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_Cod_Moneda
                    Cmd.Parameters.Add("@PNoP_TipCambCod", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_TipCambCod
                    Cmd.Parameters.Add("@PNoP_TipCambMont", SqlDbType.Decimal, 10, 3).Value = Cls_Enti.Dvt_TipCambMont
                    Cmd.Parameters.Add("@PNoP_AtenFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_AtencFecha
                    Cmd.Parameters.Add("@PNoP_AtenHora", SqlDbType.VarChar, 5).Value = Cls_Enti.Dvt_AtencHora
                    Cmd.Parameters.Add("@PNoP_PersEntrega", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersEntrega
                    Cmd.Parameters.Add("@PNoP_PersNumDocu", SqlDbType.VarChar, 15).Value = Cls_Enti.Dvt_PersNumDocu
                    Cmd.Parameters.Add("@PNoP_PersVehicul", SqlDbType.VarChar, 20).Value = Cls_Enti.Dvt_PersVehicul
                    Cmd.Parameters.Add("@PNoP_PersVehiPla", SqlDbType.VarChar, 15).Value = Cls_Enti.Dvt_PersVehiPla
                    Cmd.Parameters.Add("@PNoP_PersVehiKmAn", SqlDbType.VarChar, 8).Value = Cls_Enti.Dvt_PersVehiKmAn
                    Cmd.Parameters.Add("@PNoP_PersVehiKmAc", SqlDbType.VarChar, 8).Value = Cls_Enti.Dvt_PersVehiKmAc
                    Cmd.Parameters.Add("@PNoP_PersAreaDest", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersAreaDest
                    Cmd.Parameters.Add("@PNoP_PersActivi", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersActivi
                    Cmd.Parameters.Add("@PNoP_PersCntCsto", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersCntCsto
                    Cmd.Parameters.Add("@PNoP_PersMotivo", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersMotivo
                    Cmd.Parameters.Add("@PNoP_VencCoddEnt", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_VencCoddEnt
                    Cmd.Parameters.Add("@PNoP_VenCodTipoEnti", SqlDbType.Char, 1).Value = Cls_Enti.Dvt_VenCodTipoEnti
                    Cmd.Parameters.Add("@PCont_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.Cont_Codigo
                    Cmd.Parameters.Add("@PNoP_FAnt_Pan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_FacAnt_Anio
                    Cmd.Parameters.Add("@PNoP_FAnt_Per_Periodo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_FacAnt_Periodo
                    Cmd.Parameters.Add("@PNoP_FAnt_Vnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_FacAnt_Correlativo
                    Cmd.Parameters.Add("@PNoP_FAnt_TpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_FacAnt_TpDc
                    Cmd.Parameters.Add("@PNoP_FAnt_DocSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_FacAnt_DcSerie
                    Cmd.Parameters.Add("@PNoP_FAnt_DocNumer", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_FacAnt_DcNumero
                    Cmd.Parameters.Add("@PNoP_MntoValFactExport", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoValFactExport
                    Cmd.Parameters.Add("@PNoP_MntoBaseImponible", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoBaseImponible
                    Cmd.Parameters.Add("@PNoP_MntoExonerado", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoExonerado
                    Cmd.Parameters.Add("@PNoP_MntoInafecta", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoInafecta
                    Cmd.Parameters.Add("@PNoP_MntoISC", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoISC
                    Cmd.Parameters.Add("@PNoP_PrcIGV", SqlDbType.Decimal, 8, 2).Value = Cls_Enti.Dvt_PrcIGV
                    Cmd.Parameters.Add("@PNoP_MntoIGV", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoIGV
                    Cmd.Parameters.Add("@PNoP_MntoOtr", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoOtr
                    Cmd.Parameters.Add("@PNoP_MntoTotal", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoTotal
                    Cmd.Parameters.Add("@PNoP_Est_Fact", SqlDbType.Char, 4).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.Parameters.Add("@Estado", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    Cls_Enti.Dvt_Codigo = Cmd.Parameters("@PNoP_Codigo").Value
                    Cls_Enti.Dvt_Est_Fact = Cmd.Parameters("@PNoP_Est_Fact").Value
                    Cls_Enti.Dvt_Est_Fact_Ds = Cmd.Parameters("@Estado").Value
                    For Each Item As Ent_DOC_VENTA_DET In Cls_Enti.Dvt_Detalle
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_DET_Insert"
                        Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                        Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                        Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                        Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                        Cmd.Parameters.Add("@PNoPD_NumItem", SqlDbType.SmallInt).Value = Item.DvtD_NumItem
                        Cmd.Parameters.Add("@PAlm_Codigo", SqlDbType.Char, 5).Value = Item.DvtD_AlmaCod
                        Cmd.Parameters.Add("@PNoPD_TipoBSA", SqlDbType.Char, 4).Value = Item.DvtD_TipoCod
                        Cmd.Parameters.Add("@PCTC_Codigo", SqlDbType.Char, 8).Value = Item.CTC_Codigo
                        Cmd.Parameters.Add("@PCTC_CodUnidad", SqlDbType.VarChar, 4).Value = Item.DvtD_UnidadCod
                        Cmd.Parameters.Add("@PCTC_DescripcionItem", SqlDbType.VarChar, 200).Value = Item.CTC_DescripcionAdicional
                        Cmd.Parameters.Add("@PNoPD_Cantidad", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_Cantidad
                        Cmd.Parameters.Add("@PNoPD_ValorVenta", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_ValorVenta
                        Cmd.Parameters.Add("@PNoPD_IGVTasa", SqlDbType.Decimal, 10, 3).Value = Item.DvtD_IGVTasa
                        Cmd.Parameters.Add("@PNoPD_IGVMonto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_IGVMonto
                        Cmd.Parameters.Add("@PNoPD_PrecNeto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_PrecUnitario
                        Cmd.Parameters.Add("@PNoPD_Importe", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_MontoTotal
                        'Cmd.Parameters.Add("@PNoPD_ImporteSaldo", SqlDbType.Decimal, 15, 4).Value = Item.DvtD_ImporteSaldo
                        Cmd.Parameters.Add("@PNoPD_Operaci", SqlDbType.Char, 4).Value = Item.DvtD_OperCodigo
                        Cmd.Parameters.Add("@PNoPD_ConcCble", SqlDbType.Char, 6).Value = Item.DvtD_CcbleOficial
                        Cmd.Parameters.Add("@PNoP_Est_Fact", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_Est_Fact
                        Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                        Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent

                        Cmd.Parameters.Add("@PNoPD_TipoCentro", SqlDbType.Char, 4).Value = Item.DvtD_CodTipoCentro
                        Cmd.Parameters.Add("@PNoPD_CodigoCentro", SqlDbType.Char, 6).Value = Item.DvtD_CodCentro
                        Cmd.Parameters.Add("@PNoPD_UnidadMaquinaria", SqlDbType.Char, 5).Value = Item.DvtD_CodUnidMaquinaria
                        Cmd.ExecuteNonQuery()
                    Next
                    '-----------------------------INVENTARIOS
                    'If String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_DcNumero.Trim) Then
                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_INVENTARIO_Insert"
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PVnt_Opcion", SqlDbType.Char, 6).Value = Cls_Enti.MyIDMenu
                    Cmd.Parameters.Add("@PVnt_FechaEmision", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PVnt_DocCod", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PVnt_DocSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PVnt_DocNumero", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PVnt_DocTipoEnt", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PVnt_DocEntidad", SqlDbType.Char, 5).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.ExecuteNonQuery()
                    'End If
                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_NOTA_PEDIDO_DET_VOUCHER_COSTO_VENTA_Insertar"
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PVnt_DocMon", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_Cod_Moneda
                    Cmd.Parameters.Add("@PVnt_DocTCCod", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_TipCambCod
                    Cmd.Parameters.Add("@PVnt_DocTCVal", SqlDbType.Decimal, 10, 3).Value = Cls_Enti.Dvt_TipCambMont
                    Cmd.Parameters.Add("@PVnt_DocCod", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PVnt_DocSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PVnt_DocNumero", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PVnt_DocFechEmi", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PVnt_DocFechVen", SqlDbType.Date).Value = Cls_Enti.Dvt_AtencFecha
                    Cmd.Parameters.Add("@PVnt_DocTipoEnt", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PVnt_DocEntidad", SqlDbType.Char, 5).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PLibro", SqlDbType.Char, 3).Value = Nothing
                    Cmd.Parameters.Add("@PVoucher", SqlDbType.Char, 10).Value = Nothing
                    Cmd.Parameters.Add("@PInterno", SqlDbType.Char, 10).Value = Nothing
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.ExecuteNonQuery()

                    Trs.Commit()
                End Using
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
    Public Sub Modificar_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("paVTS_VTS_NOTA_PEDIDO_CAB_Update", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = Cls_Enti.Pdv_Codigo
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PNoP_NPSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PNoP_NPNumer", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PNoP_NPFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PTpr_cCodigo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_Cod_TipoPrc
                    Cmd.Parameters.Add("@PMon_cCodigo", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_Cod_Moneda
                    Cmd.Parameters.Add("@PNoP_TipCambCod", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_TipCambCod
                    Cmd.Parameters.Add("@PNoP_TipCambMont", SqlDbType.Decimal, 10, 3).Value = Cls_Enti.Dvt_TipCambMont
                    Cmd.Parameters.Add("@PNoP_AtenFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_AtencFecha
                    Cmd.Parameters.Add("@PNoP_AtenHora", SqlDbType.VarChar, 5).Value = Cls_Enti.Dvt_AtencHora
                    Cmd.Parameters.Add("@PNoP_PersEntrega", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersEntrega
                    Cmd.Parameters.Add("@PNoP_PersNumDocu", SqlDbType.VarChar, 15).Value = Cls_Enti.Dvt_PersNumDocu
                    Cmd.Parameters.Add("@PNoP_PersVehicul", SqlDbType.VarChar, 20).Value = Cls_Enti.Dvt_PersVehicul
                    Cmd.Parameters.Add("@PNoP_PersVehiPla", SqlDbType.VarChar, 15).Value = Cls_Enti.Dvt_PersVehiPla
                    Cmd.Parameters.Add("@PNoP_PersVehiKmAn", SqlDbType.VarChar, 8).Value = Cls_Enti.Dvt_PersVehiKmAn
                    Cmd.Parameters.Add("@PNoP_PersVehiKmAc", SqlDbType.VarChar, 8).Value = Cls_Enti.Dvt_PersVehiKmAc
                    Cmd.Parameters.Add("@PNoP_PersAreaDest", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersAreaDest
                    Cmd.Parameters.Add("@PNoP_PersActivi", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersActivi
                    Cmd.Parameters.Add("@PNoP_PersCntCsto", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersCntCsto
                    Cmd.Parameters.Add("@PNoP_PersMotivo", SqlDbType.VarChar, 50).Value = Cls_Enti.Dvt_PersMotivo
                    Cmd.Parameters.Add("@PNoP_VencCoddEnt", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_VencCoddEnt
                    Cmd.Parameters.Add("@PNoP_VenCodTipoEnti", SqlDbType.Char, 1).Value = Cls_Enti.Dvt_VenCodTipoEnti
                    Cmd.Parameters.Add("@PCont_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.Cont_Codigo
                    Cmd.Parameters.Add("@PNoP_FAnt_Pan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_FacAnt_Anio
                    Cmd.Parameters.Add("@PNoP_FAnt_Per_Periodo", SqlDbType.Char, 2).Value = Cls_Enti.Dvt_FacAnt_Periodo
                    Cmd.Parameters.Add("@PNoP_FAnt_Vnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_FacAnt_Correlativo
                    Cmd.Parameters.Add("@PNoP_FAnt_TpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_FacAnt_TpDc
                    Cmd.Parameters.Add("@PNoP_FAnt_DocSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_FacAnt_DcSerie
                    Cmd.Parameters.Add("@PNoP_FAnt_DocNumer", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_FacAnt_DcNumero
                    Cmd.Parameters.Add("@PNoP_MntoValFactExport", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoValFactExport
                    Cmd.Parameters.Add("@PNoP_MntoBaseImponible", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoBaseImponible
                    Cmd.Parameters.Add("@PNoP_MntoExonerado", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoExonerado
                    Cmd.Parameters.Add("@PNoP_MntoInafecta", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoInafecta
                    Cmd.Parameters.Add("@PNoP_MntoISC", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoISC
                    Cmd.Parameters.Add("@PNoP_PrcIGV", SqlDbType.Decimal, 8, 2).Value = Cls_Enti.Dvt_PrcIGV
                    Cmd.Parameters.Add("@PNoP_MntoIGV", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoIGV
                    Cmd.Parameters.Add("@PNoP_MntoOtr", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoOtr
                    Cmd.Parameters.Add("@PNoP_MntoTotal", SqlDbType.Decimal, 15, 2).Value = Cls_Enti.Dvt_MntoTotal
                    Cmd.Parameters.Add("@PNoP_Est_Fact", SqlDbType.Char, 4).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    Cls_Enti.Dvt_Est_Fact = Cmd.Parameters("@PNoP_Est_Fact").Value
                    'Ingresamos otra vez los datos
                    For Each Item As Ent_DOC_VENTA_DET In Cls_Enti.Dvt_Detalle
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_DET_Insert"
                        Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                        Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                        Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                        Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                        Cmd.Parameters.Add("@PNoPD_NumItem", SqlDbType.SmallInt).Value = Item.DvtD_NumItem
                        Cmd.Parameters.Add("@PAlm_Codigo", SqlDbType.Char, 5).Value = Item.DvtD_AlmaCod
                        Cmd.Parameters.Add("@PNoPD_TipoBSA", SqlDbType.Char, 4).Value = Item.DvtD_TipoCod
                        Cmd.Parameters.Add("@PCTC_Codigo", SqlDbType.Char, 8).Value = Item.CTC_Codigo
                        Cmd.Parameters.Add("@PCTC_CodUnidad", SqlDbType.VarChar, 4).Value = Item.DvtD_UnidadCod
                        Cmd.Parameters.Add("@PCTC_DescripcionItem", SqlDbType.VarChar, 200).Value = Item.CTC_DescripcionAdicional
                        Cmd.Parameters.Add("@PNoPD_Cantidad", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_Cantidad
                        Cmd.Parameters.Add("@PNoPD_ValorVenta", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_ValorVenta
                        Cmd.Parameters.Add("@PNoPD_IGVTasa", SqlDbType.Decimal, 10, 3).Value = Item.DvtD_IGVTasa
                        Cmd.Parameters.Add("@PNoPD_IGVMonto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_IGVMonto
                        Cmd.Parameters.Add("@PNoPD_PrecNeto", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_PrecUnitario
                        Cmd.Parameters.Add("@PNoPD_Importe", SqlDbType.Decimal, 20, 8).Value = Item.DvtD_MontoTotal
                        'Cmd.Parameters.Add("@PNoPD_ImporteSaldo", SqlDbType.Decimal, 15, 4).Value = Item.DvtD_ImporteSaldo
                        Cmd.Parameters.Add("@PNoPD_Operaci", SqlDbType.Char, 4).Value = Item.DvtD_OperCodigo
                        Cmd.Parameters.Add("@PNoPD_ConcCble", SqlDbType.Char, 6).Value = Item.DvtD_CcbleOficial
                        Cmd.Parameters.Add("@PNoP_Est_Fact", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_Est_Fact
                        Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                        Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent

                        Cmd.Parameters.Add("@PNoPD_TipoCentro", SqlDbType.Char, 4).Value = Item.DvtD_CodTipoCentro
                        Cmd.Parameters.Add("@PNoPD_CodigoCentro", SqlDbType.Char, 6).Value = Item.DvtD_CodCentro
                        Cmd.Parameters.Add("@PNoPD_UnidadMaquinaria", SqlDbType.Char, 5).Value = Item.DvtD_CodUnidMaquinaria
                        Cmd.ExecuteNonQuery()
                    Next
                    '-----------------------------INVENTARIOS
                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_INVENTARIO_Remove"
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PVnt_Opcion", SqlDbType.Char, 6).Value = Cls_Enti.MyIDMenu
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.ExecuteNonQuery()
                    'If String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_DcNumero.Trim) Then
                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_INVENTARIO_Insert"
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PVnt_Opcion", SqlDbType.Char, 6).Value = Cls_Enti.MyIDMenu
                    Cmd.Parameters.Add("@PVnt_FechaEmision", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PVnt_DocCod", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PVnt_DocSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PVnt_DocNumero", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PVnt_DocTipoEnt", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PVnt_DocEntidad", SqlDbType.Char, 5).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.ExecuteNonQuery()

                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_NOTA_PEDIDO_DET_VOUCHER_COSTO_VENTA_Insertar"
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PVnt_DocMon", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_Cod_Moneda
                    Cmd.Parameters.Add("@PVnt_DocTCCod", SqlDbType.Char, 3).Value = Cls_Enti.Dvt_TipCambCod
                    Cmd.Parameters.Add("@PVnt_DocTCVal", SqlDbType.Decimal, 10, 3).Value = Cls_Enti.Dvt_TipCambMont
                    Cmd.Parameters.Add("@PVnt_DocCod", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PVnt_DocSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PVnt_DocNumero", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PVnt_DocFechEmi", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha
                    Cmd.Parameters.Add("@PVnt_DocFechVen", SqlDbType.Date).Value = Cls_Enti.Dvt_AtencFecha
                    Cmd.Parameters.Add("@PVnt_DocTipoEnt", SqlDbType.Char, 1).Value = Cls_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PVnt_DocEntidad", SqlDbType.Char, 5).Value = Cls_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PLibro", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Libro), Nothing, Cls_Enti.Dvt_Libro)
                    Cmd.Parameters.Add("@PVoucher", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VoucherContable), Nothing, Cls_Enti.Dvt_VoucherContable)
                    Cmd.Parameters.Add("@PInterno", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_InternoContable), Nothing, Cls_Enti.Dvt_InternoContable)
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.ExecuteNonQuery()

                    'End If
                    Trs.Commit()
                End Using
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
    Public Sub Eliminar_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("paVTS_VTS_NOTA_PEDIDO_CAB_Deleted", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo
                    Cmd.Parameters.Add("@PSerie", SqlDbType.Char, 4).Value = Cls_Enti.Dvt_VTSerie
                    Cmd.Parameters.Add("@PNumero", SqlDbType.Char, 8).Value = Cls_Enti.Dvt_VTNumer
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    '-----------------------------INVENTARIOS
                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_INVENTARIO_Remove"
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cmd.Parameters.Add("@PVnt_Opcion", SqlDbType.Char, 6).Value = Cls_Enti.MyIDMenu
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.ExecuteNonQuery()

                End Using
                Trs.Commit()
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
    Public Sub Anular_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim Trs As SqlTransaction = Nothing
        Using Cn As New SqlConnection(Conexion.ObtenerConnection)
            Try
                Using Cmd As New SqlCommand("paVTS_VTS_NOTA_PEDIDO_CAB_Anular", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Pan_cAnio 'DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = Cls_Enti.Per_cPeriodo 'DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    'Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = Cls_Enti.TpDc_Codigo

                    Cmd.Parameters.Add("@PNoP_AnulacionFecha", SqlDbType.Date).Value = Cls_Enti.Dvt_VTFecha_Anular
                    Cmd.Parameters.Add("@PNoP_AnulacionRazon", SqlDbType.VarChar, 100).Value = Cls_Enti.Dvt_Observaciones

                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    '-----------------------------INVENTARIOS
                    Cmd.Parameters.Clear()
                    Cmd.CommandText = "paVTS_VTS_NOTA_PEDIDO_INVENTARIO_Remove"
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Pan_cAnio 'DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = Cls_Enti.Per_cPeriodo 'DatosActualConexion.PeriodoSelect
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    'Cmd.Parameters.Add("@PVnt_Opcion", SqlDbType.Char, 6).Value = Cls_Enti.MyIDMenu
                    Cmd.Parameters.Add("@PModulo", SqlDbType.Char, 4).Value = DatosActualConexion.Modulo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.ExecuteNonQuery()
                    Trs.Commit()
                End Using
            Catch ex As Exception
                Trs.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
    Public Function Buscar_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("paVTS_GRF_VTS_NOTA_PEDIDO_CAB_BUSCAR", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Per_cPeriodo), Nothing, Cls_Enti.Per_cPeriodo)
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Codigo), Nothing, Cls_Enti.Dvt_Codigo)
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo), Nothing, Cls_Enti.TpDc_Codigo)
                    Cmd.Parameters.Add("@PNoP_NPSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTSerie), Nothing, Cls_Enti.Dvt_VTSerie)
                    Cmd.Parameters.Add("@PNoP_NPNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTNumer), Nothing, Cls_Enti.Dvt_VTNumer)
                    Cmd.Parameters.Add("@PNoP_NPFecha", SqlDbType.Date).Value = IIf(Cls_Enti.Dvt_VTFecha = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_VTFecha)
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ten_cTipoEntidad), Nothing, Cls_Enti.Ten_cTipoEntidad)
                    Cmd.Parameters.Add("@PCon_cCondicion", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Con_cCondicion), Nothing, Cls_Enti.Con_cCondicion)
                    Cmd.Parameters.Add("@PTpr_cCodigo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_TipoPrc), Nothing, Cls_Enti.Dvt_Cod_TipoPrc)
                    Cmd.Parameters.Add("@PMon_cCodigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_Moneda), Nothing, Cls_Enti.Dvt_Cod_Moneda)
                    Cmd.Parameters.Add("@PNoP_VencCoddEnt", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VencCoddEnt), Nothing, Cls_Enti.Dvt_VencCoddEnt)
                    Cmd.Parameters.Add("@PNoP_VenCodTipoEnti", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VenCodTipoEnti), Nothing, Cls_Enti.Dvt_VenCodTipoEnti)
                    Cmd.Parameters.Add("@PNoP_FAnt_Pan_cAnio", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_Anio), Nothing, Cls_Enti.Dvt_FacAnt_Anio)
                    Cmd.Parameters.Add("@PNoP_FAnt_TpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_TpDc), Nothing, Cls_Enti.Dvt_FacAnt_TpDc)
                    Cmd.Parameters.Add("@PNoP_FAnt_DocSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_DcSerie), Nothing, Cls_Enti.Dvt_FacAnt_DcSerie)
                    Cmd.Parameters.Add("@PNoP_FAnt_DocNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_DcNumero), Nothing, Cls_Enti.Dvt_FacAnt_DcNumero)
                    Cmd.Parameters.Add("@PNoP_Est_Fact", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Est_Fact), Nothing, Cls_Enti.Dvt_Est_Fact)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_TRANSAC_CAB With {.Emp_cCodigo = Dr.GetString(0),
                                                                         .Pan_cAnio = Dr.GetString(1),
                                                                         .Per_cPeriodo = Dr.GetString(2),
                                                                         .Dvt_Codigo = Dr.GetString(3),
                                                                         .Pdv_Codigo = Dr.GetString(4),
                                                                         .Pdv_Descripcion = Dr.GetString(5),
                                                                         .TpDc_Codigo = Dr.GetString(6),
                                                                         .TpDc_Descripcion = Dr.GetString(7),
                                                                         .Dvt_VTSerie = Dr.GetString(8),
                                                                         .Dvt_VTNumer = Dr.GetString(9),
                                                                         .Dvt_VTFecha = Dr.GetDateTime(10),
                                                                         .Ent_cCodEntidad = Dr.GetString(11),
                                                                         .Ten_cTipoEntidad = Dr.GetString(12),
                                                                         .Ent_DNIRUC = Dr.GetString(13),
                                                                         .Ent_NombreRzS = Dr.GetString(14),
                                                                         .Ent_ApelliPri = Dr.GetString(15),
                                                                         .Ent_ApelliSeg = Dr.GetString(16),
                                                                         .Dvt_Cod_TipoPrc = Dr.GetString(17),
                                                                         .Dvt_Des_TipoPrc = Dr.GetString(18),
                                                                         .Dvt_Cod_Moneda = Dr.GetString(19),
                                                                         .Dvt_Des_Moneda = Dr.GetString(20),
                                                                         .Dvt_TipCambCod = Dr.GetString(21),
                                                                         .Dvt_TipCambMont = Dr.GetDecimal(22),
                                                                         .Dvt_AtencFecha = Dr.GetDateTime(23),
                                                                         .Dvt_AtencHora = Dr.GetString(24),
                                                                         .Dvt_PersEntrega = Dr.GetString(25),
                                                                         .Dvt_PersNumDocu = Dr.GetString(26),
                                                                         .Dvt_PersVehicul = Dr.GetString(27),
                                                                         .Dvt_PersVehiPla = Dr.GetString(28),
                                                                         .Dvt_PersVehiKmAn = Dr.GetString(29),
                                                                         .Dvt_PersVehiKmAc = Dr.GetString(30),
                                                                         .Dvt_PersAreaDest = Dr.GetString(31),
                                                                         .Dvt_PersActivi = Dr.GetString(32),
                                                                         .Dvt_PersCntCsto = Dr.GetString(33),
                                                                         .Dvt_PersMotivo = Dr.GetString(34),
                                                                         .Dvt_VencCoddEnt = Dr.GetString(35),
                                                                         .Dvt_VenCodTipoEnti = Dr.GetString(36),
                                                                         .Dvt_VenEnt_DNIRUC = Dr.GetString(37),
                                                                         .Dvt_VenEnt_NombreRzS = Dr.GetString(38),
                                                                         .Dvt_VenEnt_ApelliPri = Dr.GetString(39),
                                                                         .Dvt_VenEnt_ApelliSeg = Dr.GetString(40),
                                                                         .Cont_Codigo = Dr.GetString(41),
                                                                         .Cont_Nombres = Dr.GetString(42),
                                                                         .Cont_ApelliPri = Dr.GetString(43),
                                                                         .Cont_ApelliSeg = Dr.GetString(44),
                                                                         .Dvt_FacAnt_Anio = Dr.GetString(45),
                                                                         .Dvt_FacAnt_TpDc = Dr.GetString(46),
                                                                         .Dvt_FacAnt_DcSerie = Dr.GetString(47),
                                                                         .Dvt_FacAnt_DcNumero = Dr.GetString(48),
                                                                         .Dvt_MntoValFactExport = Dr.GetDecimal(49),
                                                                         .Dvt_MntoBaseImponible = Dr.GetDecimal(50),
                                                                         .Dvt_MntoExonerado = Dr.GetDecimal(51),
                                                                         .Dvt_MntoInafecta = Dr.GetDecimal(52),
                                                                         .Dvt_MntoISC = Dr.GetDecimal(53),
                                                                         .Dvt_PrcIGV = Dr.GetDecimal(54),
                                                                         .Dvt_MntoIGV = Dr.GetDecimal(55),
                                                                         .Dvt_MntoOtr = Dr.GetDecimal(56),
                                                                         .Dvt_MntoTotal = Dr.GetDecimal(57),
                                                                         .Dvt_Est_Fact = Dr.GetString(58),
                                                                         .Dvt_Est_Fact_Ds = Dr.GetString(59),
                                                                         .Dvt_Est_Fila = Dr.GetString(60),
                                                                         .Dvt_VoucherContable = Dr.GetString(61)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_NotaPedido_HabilesFacturacion(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection(1))
                Using Cmd As New SqlCommand("paVTS_VTS_NOTA_PEDIDO_CAB_HABILESFACTURACION_Select", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Per_cPeriodo), Nothing, Cls_Enti.Per_cPeriodo)
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Codigo), Nothing, Cls_Enti.Dvt_Codigo)
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo), Nothing, Cls_Enti.TpDc_Codigo)
                    Cmd.Parameters.Add("@PNoP_NPSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTSerie), Nothing, Cls_Enti.Dvt_VTSerie)
                    Cmd.Parameters.Add("@PNoP_NPNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTNumer), Nothing, Cls_Enti.Dvt_VTNumer)
                    Cmd.Parameters.Add("@PNoP_NPFecha", SqlDbType.Date).Value = IIf(Cls_Enti.Dvt_VTFecha = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_VTFecha)
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ten_cTipoEntidad), Nothing, Cls_Enti.Ten_cTipoEntidad)
                    Cmd.Parameters.Add("@PCon_cCondicion", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Con_cCondicion), Nothing, Cls_Enti.Con_cCondicion)
                    Cmd.Parameters.Add("@PTpr_cCodigo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_TipoPrc), Nothing, Cls_Enti.Dvt_Cod_TipoPrc)
                    Cmd.Parameters.Add("@PMon_cCodigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_Moneda), Nothing, Cls_Enti.Dvt_Cod_Moneda)
                    Cmd.Parameters.Add("@PNoP_VencCoddEnt", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VencCoddEnt), Nothing, Cls_Enti.Dvt_VencCoddEnt)
                    Cmd.Parameters.Add("@PNoP_VenCodTipoEnti", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VenCodTipoEnti), Nothing, Cls_Enti.Dvt_VenCodTipoEnti)
                    Cmd.Parameters.Add("@PNoP_FAnt_Pan_cAnio", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_Anio), Nothing, Cls_Enti.Dvt_FacAnt_Anio)
                    Cmd.Parameters.Add("@PNoP_FAnt_TpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_TpDc), Nothing, Cls_Enti.Dvt_FacAnt_TpDc)
                    Cmd.Parameters.Add("@PNoP_FAnt_DocSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_DcSerie), Nothing, Cls_Enti.Dvt_FacAnt_DcSerie)
                    Cmd.Parameters.Add("@PNoP_FAnt_DocNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FacAnt_DcNumero), Nothing, Cls_Enti.Dvt_FacAnt_DcNumero)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_TRANSAC_CAB With {.Emp_cCodigo = Dr.GetString(0), .Pan_cAnio = Dr.GetString(1), .Per_cPeriodo = Dr.GetString(2), .Dvt_Codigo = Dr.GetString(3), .Pdv_Codigo = Dr.GetString(4), .Pdv_Descripcion = Dr.GetString(5), .TpDc_Codigo = Dr.GetString(6), .TpDc_Descripcion = Dr.GetString(7), .Dvt_VTSerie = Dr.GetString(8), .Dvt_VTNumer = Dr.GetString(9), .Dvt_VTFecha = Dr.GetDateTime(10), .Ent_cCodEntidad = Dr.GetString(11), .Ten_cTipoEntidad = Dr.GetString(12), .Ent_DNIRUC = Dr.GetString(13), .Ent_NombreRzS = Dr.GetString(14), .Ent_ApelliPri = Dr.GetString(15), .Ent_ApelliSeg = Dr.GetString(16), .Dvt_Cod_TipoPrc = Dr.GetString(17), .Dvt_Des_TipoPrc = Dr.GetString(18), .Dvt_Cod_Moneda = Dr.GetString(19), .Dvt_Des_Moneda = Dr.GetString(20), .Dvt_TipCambCod = Dr.GetString(21), .Dvt_TipCambMont = Dr.GetDecimal(22), .Dvt_AtencFecha = Dr.GetDateTime(23), .Dvt_AtencHora = Dr.GetString(24), .Dvt_PersEntrega = Dr.GetString(25), .Dvt_PersNumDocu = Dr.GetString(26), .Dvt_PersVehicul = Dr.GetString(27), .Dvt_PersVehiPla = Dr.GetString(28), .Dvt_PersVehiKmAn = Dr.GetString(29), .Dvt_PersVehiKmAc = Dr.GetString(30), .Dvt_PersAreaDest = Dr.GetString(31), .Dvt_PersActivi = Dr.GetString(32), .Dvt_PersCntCsto = Dr.GetString(33), .Dvt_PersMotivo = Dr.GetString(34), .Dvt_VencCoddEnt = Dr.GetString(35), .Dvt_VenCodTipoEnti = Dr.GetString(36), .Dvt_VenEnt_DNIRUC = Dr.GetString(37), .Dvt_VenEnt_NombreRzS = Dr.GetString(38), .Dvt_VenEnt_ApelliPri = Dr.GetString(39), .Dvt_VenEnt_ApelliSeg = Dr.GetString(40), .Cont_Codigo = Dr.GetString(41), .Cont_Nombres = Dr.GetString(42), .Cont_ApelliPri = Dr.GetString(43), .Cont_ApelliSeg = Dr.GetString(44), .Dvt_FacAnt_Anio = Dr.GetString(45), .Dvt_FacAnt_TpDc = Dr.GetString(46), .Dvt_FacAnt_DcSerie = Dr.GetString(47), .Dvt_FacAnt_DcNumero = Dr.GetString(48), .Dvt_MntoValFactExport = Dr.GetDecimal(49), .Dvt_MntoBaseImponible = Dr.GetDecimal(50), .Dvt_MntoExonerado = Dr.GetDecimal(51), .Dvt_MntoInafecta = Dr.GetDecimal(52), .Dvt_MntoISC = Dr.GetDecimal(53), .Dvt_PrcIGV = Dr.GetDecimal(54), .Dvt_MntoIGV = Dr.GetDecimal(55), .Dvt_MntoOtr = Dr.GetDecimal(56), .Dvt_MntoTotal = Dr.GetDecimal(57), .Dvt_Est_Fact = Dr.GetString(58), .Dvt_Est_Fact_Ds = Dr.GetString(59), .Dvt_Est_Fila = Dr.GetString(60), .Dvt_VoucherContable = Dr.GetString(61), .Dvt_FacAnt_Periodo = Dr.GetString(62), .Dvt_FacAnt_Correlativo = Dr.GetString(63)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Detalle_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_VENTA_DET)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("paVTS_GRF_NOTA_PEDIDO_DET_BUSCAR", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = DatosActualConexion.AnioSelect
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = Cls_Enti.Per_cPeriodo
                    Cmd.Parameters.Add("@PNoP_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            'DvtD_CcbleDescripcion = Dr.GetString(21),
                            ListaItems.Add(New Ent_DOC_VENTA_DET With {.Emp_cCodigo = Dr.GetString(0),
                                                                       .Pan_cAnio = Dr.GetString(1),
                                                                       .Per_cPeriodo = Dr.GetString(2),
                                                                       .Dvt_Codigo = Dr.GetString(3),
                                                                       .DvtD_NumItem = Dr.GetInt16(4),
                                                                       .CTC_Codigo = Dr.GetString(5),
                                                                       .CTC_DescripcionItem = Dr.GetString(6),
                                                                       .CTC_DescripcionAdicional = Dr.GetString(7),
                                                                       .DvtD_UnidadCod = Dr.GetString(8),
                                                                       .DvtD_UnidadDs = Dr.GetString(9),
                                                                       .DvtD_Cantidad = Dr.GetDecimal(10),
                                                                       .DvtD_ValorVenta = Dr.GetDecimal(11),
                                                                       .DvtD_IGVTasaPorcentaje = Dr.GetDecimal(12),
                                                                       .VntD_IGVMonto = Dr.GetDecimal(13),
                                                                       .VntD_PrecNeto = Dr.GetDecimal(14),
                                                                       .VntD_Importe = Dr.GetDecimal(15),
                                                                       .DvtD_OperCodigo = Dr.GetString(16),
                                                                       .DvtD_OperOficial = Dr.GetString(17),
                                                                       .DvtD_OperDescripcion = Dr.GetString(18),
                                                                       .DvtD_CcbleOficial = Dr.GetString(19),
                                                                       .DvtD_AlmaCod = Dr.GetString(20),
                                                                       .DvtD_AlmaDes = Dr.GetString(21),
                                                                       .DvtD_TipoCod = Dr.GetString(22),
                                                                       .DvtD_TipoOfi = Dr.GetString(23),
                                                                       .DvtD_TipoDes = Dr.GetString(24),
                                                                       .DvtD_CodTipoCentro = Dr.GetString(25),
                                                                       .DvtD_OfiTipoCentro = Dr.GetString(26),
                                                                       .DvtD_NomTipoCentro = Dr.GetString(27),
                                                                       .DvtD_CodCentro = Dr.GetString(28),
                                                                       .DvtD_NomCentro = Dr.GetString(29),
                                                                       .DvtD_CodUnidMaquinaria = Dr.GetString(30),
                                                                       .DvtD_NomUnidMaquinaria = Dr.GetString(31)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Reporte_Detalle_NotaPedido(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_VENTA_DET)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection(1))
                Using Cmd As New SqlCommand("paRpt_MOVIMIENTO_PEDIDOS_DETALLE_Select", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPtoVnt_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PMoneda_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_Moneda), Nothing, Cls_Enti.Dvt_Cod_Moneda)
                    Cmd.Parameters.Add("@PDesde", SqlDbType.Date).Value = Cls_Enti.Desde
                    Cmd.Parameters.Add("@PHasta", SqlDbType.Date).Value = Cls_Enti.Hasta
                    Cmd.Parameters.Add("@PCliente", SqlDbType.Char, 5).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_VENTA_DET With {.MyOrigen = Dr.GetString(0), .Emp_cCodigo = Dr.GetString(1), .Pan_cAnio = Dr.GetString(2), .Per_cPeriodo = Dr.GetString(3), .Dvt_Codigo = Dr.GetString(4), .DvtD_NumItem = Dr.GetInt16(5), .CTC_Codigo = Dr.GetString(6), .CTC_DescripcionItem = Dr.GetString(7), .CTC_DescripcionAdicional = Dr.GetString(8), .DvtD_UnidadCod = Dr.GetString(9), .DvtD_UnidadDs = Dr.GetString(10), .DvtD_Cantidad = Dr.GetDecimal(11), .DvtD_ValorVenta = Dr.GetDecimal(12), .DvtD_DsctoUnitTasaPorcentaje = Dr.GetDecimal(13), .DvtD_DsctoGralTasaPorcentaje = Dr.GetDecimal(14), .DvtD_IGVTasaPorcentaje = (Dr.GetDecimal(15) * 100), .DvtD_OperCodigo = Dr.GetString(19), .DvtD_OperOficial = Dr.GetString(20), .DvtD_OperDescripcion = Dr.GetString(21), .DvtD_CcbleOficial = Dr.GetString(22), .DvtD_CcbleDescripcion = Dr.GetString(23), .DvtD_AlmaCod = Dr.GetString(24), .DvtD_AlmaDes = Dr.GetString(25), .DvtD_TipoCod = Dr.GetString(26), .DvtD_TipoOfi = Dr.GetString(27), .DvtD_TipoDes = Dr.GetString(28), .Ped_Origen = Dr.GetString(30), .Ped_Anio = Dr.GetString(31), .Ped_Periodo = Dr.GetString(32), .Ped_Correlativo = Dr.GetString(33), .Ped_Item = Dr.GetString(34), .PuntoVenta_Descripcion = Dr.GetString(35), .Moneda_Descripcion = Dr.GetString(36), .Condicion_Descripcion = Dr.GetString(37), .TipDoc_Sunat = Dr.GetString(38), .TipDoc_Descripcion = Dr.GetString(39), .TipDoc_Serie = Dr.GetString(40), .TipDoc_Numero = Dr.GetString(41), .Fecha_Doc = Dr.GetDateTime(42), .Cliente_Ruc = Dr.GetString(43), .Cliente_Nombres = Dr.GetString(44), .Dvt_PersVehiPla = Dr.GetString(45), .Dvt_PersCntCsto = Dr.GetString(46)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "Doc Venta"

    Public Function Buscar_DocVenta(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS)
                Using Cmd As New SqlCommand("paVTS_GRF_MOVIMIENTO_VENTAS_CAB_BUSQUEDA", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pan_cAnio), Nothing, Cls_Enti.Pan_cAnio)
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Per_cPeriodo), Nothing, Cls_Enti.Per_cPeriodo)
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Codigo), Nothing, Cls_Enti.Dvt_Codigo)
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo), Nothing, Cls_Enti.TpDc_Codigo)
                    Cmd.Parameters.Add("@PVnt_NPSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTSerie), Nothing, Cls_Enti.Dvt_VTSerie)
                    Cmd.Parameters.Add("@PVnt_NPNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTNumer), Nothing, Cls_Enti.Dvt_VTNumer)
                    Cmd.Parameters.Add("@PVnt_NPFecha", SqlDbType.Date).Value = IIf(Cls_Enti.Dvt_VTFecha = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_VTFecha)
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ten_cTipoEntidad), Nothing, Cls_Enti.Ten_cTipoEntidad)
                    Cmd.Parameters.Add("@PCon_cCondicion", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Con_cCondicion), Nothing, Cls_Enti.Con_cCondicion)
                    Cmd.Parameters.Add("@PVnt_FechaVenci", SqlDbType.VarChar, 10).Value = IIf(Cls_Enti.Dvt_FechaVenci = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_FechaVenci)
                    Cmd.Parameters.Add("@PTpr_cCodigo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_TipoPrc), Nothing, Cls_Enti.Dvt_Cod_TipoPrc)
                    Cmd.Parameters.Add("@PMon_cCodigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_Moneda), Nothing, Cls_Enti.Dvt_Cod_Moneda)
                    Cmd.Parameters.Add("@PVnt_VencCoddEnt", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VencCoddEnt), Nothing, Cls_Enti.Dvt_VencCoddEnt)
                    Cmd.Parameters.Add("@PVnt_VenCodTipoEnti", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VenCodTipoEnti), Nothing, Cls_Enti.Dvt_VenCodTipoEnti)
                    Cmd.Parameters.Add("@PVnt_Est_Fact", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Est_Fact), Nothing, Cls_Enti.Dvt_Est_Fact)
                    Cmd.Parameters.Add("@Establecimiento", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Establecimiento_Codigo), Nothing, Cls_Enti.Establecimiento_Codigo)
                    Cmd.Parameters.Add("@Es_Electronico", SqlDbType.Bit).Value = IIf(Cls_Enti.Vnt_Es_Electronico = False, Nothing, Cls_Enti.Vnt_Es_Electronico)
                    Cmd.Parameters.Add("@PTpDc_Codigo_St", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo_St), Nothing, Cls_Enti.TpDc_Codigo_St)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read

                            ListaItems.Add(New Ent_DOC_TRANSAC_CAB With {.MyOrigen = Dr.GetString(0),
                                                                         .Emp_cCodigo = Dr.GetString(1),
                                                                         .Pan_cAnio = Dr.GetString(2),
                                                                         .Per_cPeriodo = Dr.GetString(3),
                                                                         .Dvt_Codigo = Dr.GetString(4),
                                                                         .Pdv_Codigo = Dr.GetString(5),
                                                                         .Pdv_Descripcion = Dr.GetString(6),
                                                                         .TpDc_Codigo = Dr.GetString(7),
                                                                         .TpDc_Codigo_St = Dr.GetString(8),
                                                                         .TpDc_Descripcion = Dr.GetString(9),
                                                                         .Dvt_VTSerie = Dr.GetString(10),
                                                                         .Dvt_VTNumer = Dr.GetString(11),
                                                                         .Dvt_VTFecha = Dr.GetDateTime(12),
                                                                         .Ent_cCodEntidad = Dr.GetString(13),
                                                                         .Ten_cTipoEntidad = Dr.GetString(14),
                                                                         .Ent_DNIRUC = Dr.GetString(15),
                                                                         .Ent_NombreRzS = Dr.GetString(16),
                                                                         .Ent_ApelliPri = Dr.GetString(17),
                                                                         .Ent_ApelliSeg = Dr.GetString(18),
                                                                         .Dir_Codigo = Dr.GetString(19),
                                                                         .Con_cCondicion = Dr.GetString(20),
                                                                         .Con_cDescripcion = Dr.GetString(21),
                                                                         .Dvt_DiasCondic = Dr.GetInt16(22),
                                                                         .Dvt_FechaVenci = Dr.GetDateTime(23),
                                                                         .Dvt_EsAnticipada = Dr.GetBoolean(24),
                                                                         .Dvt_Cod_TipoPrc = Dr.GetString(25),
                                                                         .Dvt_Des_TipoPrc = Dr.GetString(26),
                                                                         .Dvt_Cod_Moneda = Dr.GetString(27),
                                                                         .Dvt_Des_Moneda = Dr.GetString(28),
                                                                         .Dvt_TipCambCod = Dr.GetString(29),
                                                                         .Dvt_TipCambMont = Dr.GetDecimal(30),
                                                                         .Dvt_VencCoddEnt = Dr.GetString(31),
                                                                         .Dvt_VenCodTipoEnti = Dr.GetString(32),
                                                                         .Dvt_VenEnt_DNIRUC = Dr.GetString(33),
                                                                         .Dvt_VenEnt_NombreRzS = Dr.GetString(34),
                                                                         .Dvt_VenEnt_ApelliPri = Dr.GetString(35),
                                                                         .Dvt_VenEnt_ApelliSeg = Dr.GetString(36),
                                                                         .Dvt_PrDstoGeneral = Dr.GetDecimal(37),
                                                                         .Dvt_MntoRedondeo = Dr.GetDecimal(38),
                                                                         .Dvt_MntoValFactExport = Dr.GetDecimal(39),
                                                                         .Dvt_MntoBaseImponible = Dr.GetDecimal(40),
                                                                         .Dvt_MntoExonerado = Dr.GetDecimal(41),
                                                                         .Dvt_MntoInafecta = Dr.GetDecimal(42),
                                                                         .Dvt_MntoISC = Dr.GetDecimal(43),
                                                                         .Dvt_PrcIGV = Dr.GetDecimal(44),
                                                                         .Dvt_MntoIGV = Dr.GetDecimal(45),
                                                                         .Dvt_MntoOtr = Dr.GetDecimal(46),
                                                                         .Dvt_MntoTotal = Dr.GetDecimal(47),
                                                                         .Dvt_MntoTotalAlterno = Dr.GetDecimal(48),
                                                                         .Dvt_Est_Fact = Dr.GetString(49),
                                                                         .Dvt_Est_Fact_Ds = Dr.GetString(50),
                                                                         .Dvt_Est_Fila = Dr.GetString(51),
                                                                         .Dvt_VoucherContable = Dr.GetString(52),
                                                                         .Dvt_InternoContable = Dr.GetString(53),
                                                                         .Dvt_Libro = Dr.GetString(54),
                                                                         .Pdv_Motivo_Cod = Dr.GetString(55),
                                                                         .Pdv_Motivo_Ofi = Dr.GetString(56),
                                                                         .Pdv_Motivo_Des = Dr.GetString(57),
                                                                         .Dvt_Observaciones = Dr.GetString(58),
                                                                         .PVnt_IGV_Afecto = Dr.GetBoolean(59),
                                                                         .PVnt_IGV_Codigo = Dr.GetString(60),
                                                                         .PVnt_IGV_Codigo_Ofi = Dr.GetString(61),
                                                                         .PVnt_IGV_Descripcion = Dr.GetString(62),
                                                                         .PVnt_IGV_PerIncluyDoc_Venta = Dr.GetBoolean(63),
                                                                         .PVnt_IGV_PerTpDc_Codigo = Dr.GetString(64),
                                                                         .PVnt_IGV_PerTpDc_Codigo_Ofi = Dr.GetString(65),
                                                                         .PVnt_IGV_PerTpDc_Descripcion = Dr.GetString(66),
                                                                         .PVnt_IGV_PerSerie = Dr.GetString(67),
                                                                         .PVnt_IGV_PerNumero = Dr.GetString(68),
                                                                         .PVnt_IGV_Detr_TipoOperaci = Dr.GetString(69),
                                                                         .PVnt_IGV_Detr_TipoOperaci_Ds = Dr.GetString(70),
                                                                         .PVnt_IGV_Detr_TipoProduct = Dr.GetString(71),
                                                                         .PVnt_IGV_Detr_TipoProduct_Ds = Dr.GetString(72),
                                                                         .PVnt_IGV_Detr_Porcentaje = Dr.GetDecimal(73),
                                                                         .PVnt_IGV_Rete_Porcentaje = Dr.GetDecimal(74),
                                                                         .PVnt_IGV_Monto_Afecto = Dr.GetDecimal(75),
                                                                         .Vnt_IsAperturado = Dr.GetBoolean(76),
                                                                         .Caja_Estado = Dr.GetString(77),
                                                                         .Vnt_placa_Vehiculo = Dr.GetString(78),
                                                                         .Dvt_AtencHora = Dr.GetString(79),
                                                                         .CodigoCaja = Dr.GetString(80),
                                                                         .Cja_Codigo_Apertura = Dr.GetInt32(81),
                                                                         .Vnt_IsVenta_Automatica = Dr.GetBoolean(82),
                                                                         .Vnt_Es_Electronico = Dr.GetBoolean(83),
                                                                         .Ent_TpDcEnt_SUNAT = Dr.GetString(84).Trim,
                                                                         .Vnt_Cod_Autorizacion_Sunat = Dr.GetString(85),
                                                                         .Vnt_Modelo_Impresion = Dr.GetString(86),
                                                                         .Dvt_CorreoAEnviar = Dr.GetString(87).Trim
                                                                         })
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Detalle_DocVenta(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_VENTA_DET)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS)
                Using Cmd As New SqlCommand("paVTS_GRF_MOVIMIENTO_VENTAS_DET_BUSCAR", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = Cls_Enti.Emp_cCodigo
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = Cls_Enti.Per_cPeriodo
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_VENTA_DET With {.MyOrigen = Dr.GetString(0),
                                                                       .Emp_cCodigo = Dr.GetString(1),
                                                                       .Pan_cAnio = Dr.GetString(2),
                                                                       .Per_cPeriodo = Dr.GetString(3),
                                                                       .Dvt_Codigo = Dr.GetString(4),
                                                                       .DvtD_NumItem = Dr.GetInt16(5),
                                                                       .CTC_Codigo = Dr.GetString(6),
                                                                       .CTC_DescripcionItem = Dr.GetString(7),
                                                                       .CTC_DescripcionAdicional = Dr.GetString(8),
                                                                       .DvtD_UnidadCod = Dr.GetString(9),
                                                                       .DvtD_UnidadDs = Dr.GetString(10),
                                                                       .DvtD_Cantidad = Dr.GetDecimal(11),
                                                                       .DvtD_ValorVenta = Dr.GetDecimal(12),
                                                                       .DvtD_DsctoUnitTasaPorcentaje = Dr.GetDecimal(13),
                                                                       .DvtD_DsctoGralTasaPorcentaje = Dr.GetDecimal(14),
                                                                       .DvtD_IGVTasaPorcentaje = (Dr.GetDecimal(15) * 100),
                                                                       .Valor_Precio = Dr.GetDecimal(18),
                                                                       .DvtD_OperCodigo = Dr.GetString(19),
                                                                       .DvtD_OperOficial = Dr.GetString(20),
                                                                       .DvtD_OperDescripcion = Dr.GetString(21),
                                                                       .DvtD_CcbleOficial = Dr.GetString(22),
                                                                       .DvtD_AlmaCod = Dr.GetString(23),
                                                                       .DvtD_AlmaDes = Dr.GetString(24),
                                                                       .DvtD_TipoOfi = Dr.GetString(25),
                                                                        .DvtD_TipoCod = Dr.GetString(26),
                                                                       .DvtD_TipoDes = Dr.GetString(27),
                            .VntD_Est_Fila = Dr.GetString(28), .Unm_Descripcion = Dr.GetString(29), .UNM_CoigodFE = Dr.GetString(30)
                                                                      })
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
  
    Public Function Buscar_Detalle_MediosPago(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_VENTA_DET)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS())
                Using Cmd As New SqlCommand("paVTS_MOVIMIENTO_VENTAS_PAGOS_PUNTO_VENTA_Select", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = Cls_Enti.Pan_cAnio
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = Cls_Enti.Per_cPeriodo
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = Cls_Enti.Dvt_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_VENTA_DET With {.DvtD_NumItem = Dr.GetInt16(0),
                                                                       .MedioPagCodigo = Dr.GetString(1),
                                                                       .MedioPagSunat = Dr.GetString(2),
                                                                       .MedioPagDescripcion = Dr.GetString(3),
                                                                       .TarjetaCodigo = Dr.GetString(4),
                                                                       .TarjetaDescripcion = Dr.GetString(5),
                                                                       .TarjetaTipoCodigo = Dr.GetString(6),
                                                                       .TarjetaTipoDescripcion = Dr.GetString(7),
                                                                       .TarjetaCtaCble = Dr.GetString(8),
                                                                       .TarjetaNumero = Dr.GetString(9),
                                                                       .TarjetaLote = Dr.GetString(10),
                                                                       .MedioReferencia = Dr.GetString(11),
                                                                       .MedioImporte = Dr.GetDecimal(12),
                                                                       .TarjetaLibro = Dr.GetString(13),
                                                                       .TarjetaInterno = Dr.GetString(14),
                                                                       .TarjetaVoucher = Dr.GetString(15)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_NotaCredito(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS())
                Using Cmd As New SqlCommand("paVTS_MOVIMIENTO_NC_CAB_Select", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pan_cAnio), Nothing, Cls_Enti.Pan_cAnio)
                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Per_cPeriodo), Nothing, Cls_Enti.Per_cPeriodo)
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Codigo), Nothing, Cls_Enti.Dvt_Codigo)
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PCod_Moti_Nota", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Motivo_Cod), Nothing, Cls_Enti.Pdv_Motivo_Cod)
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo), Nothing, Cls_Enti.TpDc_Codigo)
                    Cmd.Parameters.Add("@PVnt_NPSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTSerie), Nothing, Cls_Enti.Dvt_VTSerie)
                    Cmd.Parameters.Add("@PVnt_NPNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTNumer), Nothing, Cls_Enti.Dvt_VTNumer)
                    Cmd.Parameters.Add("@PVnt_NPFecha", SqlDbType.Date).Value = IIf(Cls_Enti.Dvt_VTFecha = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_VTFecha)
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ten_cTipoEntidad), Nothing, Cls_Enti.Ten_cTipoEntidad)
                    Cmd.Parameters.Add("@PTpDc_Codigo_St", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo_St), Nothing, Cls_Enti.TpDc_Codigo_St)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_TRANSAC_CAB With {.Emp_cCodigo = Dr.GetString(0), .Pan_cAnio = Dr.GetString(1), .Per_cPeriodo = Dr.GetString(2),
                                                                         .Dvt_Codigo = Dr.GetString(3), .Pdv_Codigo = Dr.GetString(4), .Pdv_Descripcion = Dr.GetString(5), .Pdv_Motivo_Cod = Dr.GetString(6), .Pdv_Motivo_Ofi = Dr.GetString(7), .Pdv_Motivo_Des = Dr.GetString(8),
                                                                         .TpDc_Codigo = Dr.GetString(9), .TpDc_Descripcion = Dr.GetString(10),
                                                                         .Dvt_VTSerie = Dr.GetString(11), .Dvt_VTNumer = Dr.GetString(12),
                                                                         .Dvt_VTFecha = Dr.GetDateTime(13), .Ent_cCodEntidad = Dr.GetString(14),
                                                                         .Ten_cTipoEntidad = Dr.GetString(15), .Ent_DNIRUC = Dr.GetString(16),
                                                                         .Ent_NombreRzS = Dr.GetString(17), .Ent_ApelliPri = Dr.GetString(18),
                                                                         .Ent_ApelliSeg = Dr.GetString(19), .Dir_Codigo = Dr.GetString(20),
                                                                         .Dvt_Cod_TipoPrc = Dr.GetString(21), .Dvt_Des_TipoPrc = Dr.GetString(22),
                                                                         .Dvt_Cod_Moneda = Dr.GetString(23), .Dvt_Des_Moneda = Dr.GetString(24),
                                                                         .Dvt_TipCambCod = Dr.GetString(25), .Dvt_TipCambMont = Dr.GetDecimal(26),
                                                                         .Dvt_Otros = Dr.GetString(27), .Dvt_Observaciones = Dr.GetString(27), .Dvt_MntoValFactExport = Dr.GetDecimal(28),
                                                                         .Dvt_MntoBaseImponible = Dr.GetDecimal(29), .Dvt_MntoExonerado = Dr.GetDecimal(30),
                                                                         .Dvt_MntoInafecta = Dr.GetDecimal(31), .Dvt_MntoISC = Dr.GetDecimal(32),
                                                                         .Dvt_PrcIGV = Dr.GetDecimal(33), .Dvt_MntoIGV = Dr.GetDecimal(34),
                                                                         .Dvt_MntoOtr = Dr.GetDecimal(35), .Dvt_MntoTotal = Dr.GetDecimal(36),
                                                                         .Dvt_Est_Fila = Dr.GetString(37), .Dvt_Est_Fact_Ds = Dr.GetString(38),
                                                                         .Dvt_VoucherContable = Dr.GetString(39), .Dvt_InternoContable = Dr.GetString(40),
                                                                         .Dvt_Libro = Dr.GetString(41), .MyOrigen = Dr.GetString(42),
                                                                         .TpDc_Codigo_St = Dr.GetString(43), .Dvt_Abrev_Moneda = Dr.GetString(44),
                                                                         .Vnt_EstadoEnvio = Dr.GetString(45),
                                                                         .Vnt_Es_Electronico = Dr.GetBoolean(46),
                                                                         .Ent_TpDcEnt_SUNAT = Dr.GetString(47).Trim,
                                                                         .Vnt_Cod_Autorizacion_Sunat = Dr.GetString(48),
                                                                         .Vnt_Modelo_Impresion = Dr.GetString(49),
                            .Dvt_CorreoAEnviar = Dr.GetString(50).Trim
                                                                         })
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_NotaDebito(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Dim ListaItems As New List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS())
                Using Cmd As New SqlCommand("paVTS_MOVIMIENTO_VENTAS_CAB_Select", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@POrig_Codigo", SqlDbType.Char, 2).Value = Cls_Enti.MyOrigen
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PPan_cAnio", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pan_cAnio), Nothing, Cls_Enti.Pan_cAnio)

                    Cmd.Parameters.Add("@PSoloVenta", SqlDbType.Bit).Value = Cls_Enti.SoloVenta
                    Cmd.Parameters.Add("@PIncluirAniosAnteriores", SqlDbType.Bit).Value = Cls_Enti.IncluirAniosAnteriores

                    Cmd.Parameters.Add("@PPer_cPeriodo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Per_cPeriodo), Nothing, Cls_Enti.Per_cPeriodo)
                    Cmd.Parameters.Add("@PVnt_Codigo", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Codigo), Nothing, Cls_Enti.Dvt_Codigo)
                    Cmd.Parameters.Add("@PPdv_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Pdv_Codigo), Nothing, Cls_Enti.Pdv_Codigo)
                    Cmd.Parameters.Add("@PTpDc_Codigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo), Nothing, Cls_Enti.TpDc_Codigo)
                    Cmd.Parameters.Add("@PVnt_NPSerie", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTSerie), Nothing, Cls_Enti.Dvt_VTSerie)
                    Cmd.Parameters.Add("@PVnt_NPNumer", SqlDbType.Char, 8).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VTNumer), Nothing, Cls_Enti.Dvt_VTNumer)
                    Cmd.Parameters.Add("@PVnt_NPFecha", SqlDbType.Date).Value = IIf(Cls_Enti.Dvt_VTFecha = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_VTFecha)
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ent_cCodEntidad), Nothing, Cls_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Ten_cTipoEntidad), Nothing, Cls_Enti.Ten_cTipoEntidad)
                    Cmd.Parameters.Add("@PCon_cCondicion", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Con_cCondicion), Nothing, Cls_Enti.Con_cCondicion)
                    Cmd.Parameters.Add("@PVnt_FechaVenci", SqlDbType.VarChar, 10).Value = IIf(Cls_Enti.Dvt_FechaVenci = "#12:00:00 AM#", Nothing, Cls_Enti.Dvt_FechaVenci)
                    'Cmd.Parameters.Add("@PVnt_EsAnticipada", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_FechaVenci), Nothing, Cls_Enti.Dvt_FechaVenci)
                    Cmd.Parameters.Add("@PTpr_cCodigo", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_TipoPrc), Nothing, Cls_Enti.Dvt_Cod_TipoPrc)
                    Cmd.Parameters.Add("@PMon_cCodigo", SqlDbType.Char, 3).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Cod_Moneda), Nothing, Cls_Enti.Dvt_Cod_Moneda)
                    Cmd.Parameters.Add("@PVnt_VencCoddEnt", SqlDbType.Char, 10).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VencCoddEnt), Nothing, Cls_Enti.Dvt_VencCoddEnt)
                    Cmd.Parameters.Add("@PVnt_VenCodTipoEnti", SqlDbType.Char, 1).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_VenCodTipoEnti), Nothing, Cls_Enti.Dvt_VenCodTipoEnti)
                    Cmd.Parameters.Add("@PVnt_Est_Fact", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Cls_Enti.Dvt_Est_Fact), Nothing, Cls_Enti.Dvt_Est_Fact)
                    Cmd.Parameters.Add("@PTpDc_Codigo_St", SqlDbType.Char, 2).Value = IIf(String.IsNullOrEmpty(Cls_Enti.TpDc_Codigo_St), Nothing, Cls_Enti.TpDc_Codigo_St)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_DOC_TRANSAC_CAB With {.MyOrigen = Dr.GetString(0),
                                                                         .Emp_cCodigo = Dr.GetString(1),
                                                                         .Pan_cAnio = Dr.GetString(2),
                                                                         .Per_cPeriodo = Dr.GetString(3),
                                                                         .Dvt_Codigo = Dr.GetString(4),
                                                                         .Pdv_Codigo = Dr.GetString(5),
                                                                         .Pdv_Descripcion = Dr.GetString(6),
                                                                         .TpDc_Codigo = Dr.GetString(7),
                                                                         .TpDc_Codigo_St = Dr.GetString(8),
                                                                         .TpDc_Descripcion = Dr.GetString(9),
                                                                         .Dvt_VTSerie = Dr.GetString(10),
                                                                         .Dvt_VTNumer = Dr.GetString(11),
                                                                         .Dvt_VTFecha = Dr.GetDateTime(12),
                                                                         .Ent_cCodEntidad = Dr.GetString(13),
                                                                         .Ten_cTipoEntidad = Dr.GetString(14),
                                                                         .Ent_DNIRUC = Dr.GetString(15),
                                                                         .Ent_NombreRzS = Dr.GetString(16),
                                                                         .Ent_ApelliPri = Dr.GetString(17),
                                                                         .Ent_ApelliSeg = Dr.GetString(18),
                                                                         .Dir_Codigo = Dr.GetString(19),
                                                                         .Con_cCondicion = Dr.GetString(20),
                                                                         .Con_cDescripcion = Dr.GetString(21),
                                                                         .Dvt_DiasCondic = Dr.GetInt16(22),
                                                                         .Dvt_FechaVenci = Dr.GetDateTime(23),
                                                                         .Dvt_EsAnticipada = Dr.GetBoolean(24),
                                                                         .Dvt_Cod_TipoPrc = Dr.GetString(25),
                                                                         .Dvt_Des_TipoPrc = Dr.GetString(26),
                                                                         .Dvt_Cod_Moneda = Dr.GetString(27),
                                                                         .Dvt_Des_Moneda = Dr.GetString(28),
                                                                         .Dvt_TipCambCod = Dr.GetString(29),
                                                                         .Dvt_TipCambMont = Dr.GetDecimal(30),
                                                                         .Dvt_VencCoddEnt = Dr.GetString(31),
                                                                         .Dvt_VenCodTipoEnti = Dr.GetString(32),
                                                                         .Dvt_VenEnt_DNIRUC = Dr.GetString(33),
                                                                         .Dvt_VenEnt_NombreRzS = Dr.GetString(34),
                                                                         .Dvt_VenEnt_ApelliPri = Dr.GetString(35),
                                                                         .Dvt_VenEnt_ApelliSeg = Dr.GetString(36),
                                                                         .Dvt_PrDstoGeneral = Dr.GetDecimal(37),
                                                                         .Dvt_MntoRedondeo = Dr.GetDecimal(38),
                                                                         .Dvt_MntoValFactExport = Dr.GetDecimal(39),
                                                                         .Dvt_MntoBaseImponible = Dr.GetDecimal(40),
                                                                         .Dvt_MntoExonerado = Dr.GetDecimal(41),
                                                                         .Dvt_MntoInafecta = Dr.GetDecimal(42),
                                                                         .Dvt_MntoISC = Dr.GetDecimal(43),
                                                                         .Dvt_PrcIGV = Dr.GetDecimal(44),
                                                                         .Dvt_MntoIGV = Dr.GetDecimal(45),
                                                                         .Dvt_MntoOtr = Dr.GetDecimal(46),
                                                                         .Dvt_MntoTotal = Dr.GetDecimal(47),
                                                                         .Dvt_MntoTotalAlterno = Dr.GetDecimal(48),
                                                                         .Dvt_Est_Fact = Dr.GetString(49),
                                                                         .Dvt_Est_Fact_Ds = Dr.GetString(50),
                                                                         .Dvt_Est_Fila = Dr.GetString(51),
                                                                         .Dvt_VoucherContable = Dr.GetString(52),
                                                                         .Dvt_InternoContable = Dr.GetString(53),
                                                                         .Dvt_Libro = Dr.GetString(54),
                                                                         .Pdv_Motivo_Cod = Dr.GetString(55),
                                                                         .Pdv_Motivo_Ofi = Dr.GetString(56),
                                                                         .Pdv_Motivo_Des = Dr.GetString(57),
                                                                         .Dvt_Observaciones = Dr.GetString(58),
                                                                         .PVnt_IGV_Afecto = Dr.GetBoolean(59),
                                                                         .PVnt_IGV_Codigo = Dr.GetString(60),
                                                                         .PVnt_IGV_Codigo_Ofi = Dr.GetString(61),
                                                                         .PVnt_IGV_Descripcion = Dr.GetString(62),
                                                                         .PVnt_IGV_PerIncluyDoc_Venta = Dr.GetBoolean(63),
                                                                         .PVnt_IGV_PerTpDc_Codigo = Dr.GetString(64),
                                                                         .PVnt_IGV_PerTpDc_Codigo_Ofi = Dr.GetString(65),
                                                                         .PVnt_IGV_PerTpDc_Descripcion = Dr.GetString(66),
                                                                         .PVnt_IGV_PerSerie = Dr.GetString(67),
                                                                         .PVnt_IGV_PerNumero = Dr.GetString(68),
                                                                         .PVnt_IGV_Detr_TipoOperaci = Dr.GetString(69),
                                                                         .PVnt_IGV_Detr_TipoOperaci_Ds = Dr.GetString(70),
                                                                         .PVnt_IGV_Detr_TipoProduct = Dr.GetString(71),
                                                                         .PVnt_IGV_Detr_TipoProduct_Ds = Dr.GetString(72),
                                                                         .PVnt_IGV_Detr_Porcentaje = Dr.GetDecimal(73),
                                                                         .PVnt_IGV_Rete_Porcentaje = Dr.GetDecimal(74),
                                                                         .PVnt_IGV_Monto_Afecto = Dr.GetDecimal(75),
                                                                         .Dvt_Abrev_Moneda = Dr.GetString(76),
                                                                         .Vnt_IsAperturado = Dr.GetBoolean(77),
                                                                         .Vnt_EstadoEnvio = Dr.GetString(80),
                                                                         .Vnt_Es_Electronico = Dr.GetBoolean(81),
                                                                         .Ent_TpDcEnt_SUNAT = Dr.GetString(82).Trim,
                                                                         .Vnt_Cod_Autorizacion_Sunat = Dr.GetString(83),
                                                                         .Vnt_Modelo_Impresion = Dr.GetString(84),
                                                                         .Dvt_CorreoAEnviar = Dr.GetString(85).Trim})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
End Class
