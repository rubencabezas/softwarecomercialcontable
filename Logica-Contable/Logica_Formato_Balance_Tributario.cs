﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Logica_Formato_Balance_Tributario
    {
        private Datos_Formato_Balance_Tributario CapaDato = new Datos_Formato_Balance_Tributario();

        public List<Entidad_Formato_Balance_Tributario> Listar(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Formato_Balance_Tributario> Reporte_Balance_Tributario(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            try
            {
                return CapaDato.Reporte_Balance_Tributario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Insertar(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Eliminar(Entidad_Formato_Balance_Tributario Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
