﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
 public   class Logica_Movimiento_Caja
    {
        private Datos_Movimiento_Caja CapaDato = new Datos_Movimiento_Caja();

        public bool Insertar(Entidad_Movimiento_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Caja> Listar_Por_Pdv(Entidad_Movimiento_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Por_Pdv(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
