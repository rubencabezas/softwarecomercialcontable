﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{

   public class Logica_Determinacion_Igv
    {
        private Datos_Determinacion_Igv CapaDato = new Datos_Determinacion_Igv();

        public bool Insertar_Anual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Anual(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insertar_Mensual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Mensual(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar_Anual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_Anual(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Modificar_Mensual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_Mensual(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar_Anual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Anual(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar_Mensual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Mensual(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Determinacion_Igv> Listar_Anual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Anual(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Determinacion_Igv> Listar_Mensual(Entidad_Determinacion_Igv Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Mensual(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
