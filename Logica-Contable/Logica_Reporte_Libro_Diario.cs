﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
public    class Logica_Reporte_Libro_Diario
    {
        private Datos_Reporte_Libro_Diario CapaDato = new Datos_Reporte_Libro_Diario();
        public List<Entidad_Reporte_Libro_Diario> ListarLibroDiarioCentralizado(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.ListarLibroDiarioCentralizado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Reporte_Libro_Diario> ListarLibroDiario(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.ListarLibroDiario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Reporte_Libro_Diario> Ple_V5(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.Ple_V5(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Reporte_Libro_Diario> Ple_V5_Cuentas(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.Ple_V5_Cuentas(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Reporte_Libro_Diario> Ple_V5_Mayor(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.Ple_V5_Mayor(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable Reporte_LibroDiarioSimplificado(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.Rep_LibroDiarioSimplificado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public List<Entidad_Reporte_Libro_Diario> ListarLibro_Diario_Formato_Simplificado_PLE_5_0(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.ListarLibro_Diario_Formato_Simplificado_PLE_5_0(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Reporte_Libro_Diario> ListarLibro_Diario_Formato_Simplificado_PLE_5_0_Detalle_Plan(Entidad_Reporte_Libro_Diario Cls_Enti)
        {
            try
            {
                return CapaDato.ListarLibro_Diario_Formato_Simplificado_PLE_5_0_Detalle_Plan(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
