﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Logica_Ejercicio
    {

        private Datos_Ejercicio CapaDato = new Datos_Ejercicio();

        public bool Insertar(Entidad_Ejercicio Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Ejercicio> Listar(Entidad_Ejercicio Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Ejercicio> Listar_Periodo(Entidad_Ejercicio Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Periodo(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Replicacion_General(Entidad_Ejercicio Cls_Enti)
        {
            try
            {

                CapaDato.Replicacion_General(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
