﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Logica_Tipo_Cambio
    {


        private Datos_Tipo_Cambio CapaDato = new Datos_Tipo_Cambio();

        public bool Insertar(Entidad_Tipo_Cambio Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Tipo_Cambio Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Tipo_Cambio Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Tipo_Cambio> Listar(Entidad_Tipo_Cambio Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    public List<Entidad_Tipo_Cambio> Buscar_Tipo_Cambio(Entidad_Tipo_Cambio Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Tipo_Cambio(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
