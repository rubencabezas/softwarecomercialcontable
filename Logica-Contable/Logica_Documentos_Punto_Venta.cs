﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
public    class Logica_Documentos_Punto_Venta
    {
        private Datos_Documentos_Punto_Venta CapaDato = new Datos_Documentos_Punto_Venta();

        public bool Insertar(Entidad_Documentos_Punto_Venta Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Documentos_Punto_Venta Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public bool Eliminar(Entidad_Establecimiento Cls_Enti)
        //{
        //    try
        //    {

        //        CapaDato.Eliminar(Cls_Enti);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Documentos_Punto_Venta> Listar(Entidad_Documentos_Punto_Venta Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
