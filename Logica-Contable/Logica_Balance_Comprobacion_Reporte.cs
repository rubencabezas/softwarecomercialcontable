﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Logica_Balance_Comprobacion_Reporte
    {
        private Datos_Balance_Comprobacion_Reporte CapaDato = new Datos_Balance_Comprobacion_Reporte();
        public List<Entidad_Balance_Comprobacion_Reporte> Balance_Acumulado(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            try
            {
                return CapaDato.Balance_Acumulado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Balance_Comprobacion_Reporte> Balance_Mensual(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            try
            {
                return CapaDato.Balance_Mensual(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Balance_Comprobacion_Reporte> Ver_Detalle_Cuenta_Balance_General(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            try
            {
                return CapaDato.Ver_Detalle_Cuenta_Balance_General(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Balance_Comprobacion_Reporte> Ver_Detalle_Cuenta_Balance_General_Acumulado(Entidad_Balance_Comprobacion_Reporte Cls_Enti)
        {
            try
            {
                return CapaDato.Ver_Detalle_Cuenta_Balance_General_Acumulado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
