﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Logica_Plan_General
    {

        private Datos_Plan_General CapaDato = new Datos_Plan_General();

        public bool Insertar(Entidad_Plan_General Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public Entidad_Plan_General Traer(Entidad_Plan_General Ent_Cta)
        {
            try
            {
                return CapaDato.ListarDatos(Ent_Cta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Plan_General Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Plan_General Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Plan_General> Listar(Entidad_Plan_General Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Entidad_Plan_General TraerDigitoMayor() 
        {
            try
            {
                Entidad_Plan_General NumDigito = new Entidad_Plan_General();
              return  NumDigito = CapaDato.TraerDigito();
                //string Numero = null;
                //Numero = NumDigito.Est_Num_Digitos;
                //return Numero;
                //return NumDigito.Est_Num_Digitos;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insertar_En_Cantidad(Entidad_Plan_General Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_En_Cantidad(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
