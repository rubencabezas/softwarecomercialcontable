﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Contable
{
  public  class Logica_Exportar_Importar
    {
        private Datos_Exportar_Importar Dato = new Datos_Exportar_Importar();

        public DataSet EXPORTAR_MARCA_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_MARCA_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_FABRICANTE_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_FABRICANTE_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public DataSet EXPORTAR_UNIDAD_MEDIDA_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_UNIDAD_MEDIDA_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_CATALOGO_GRUPO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CATALOGO_GRUPO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet EXPORTAR_CATALOGO_FAMILIA_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CATALOGO_FAMILIA_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_CATALOGO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CATALOGO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CATALOGO_UNM_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CATALOGO_UNM_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //
        public bool IMPORTAR_MARCA_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_MARCA_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_FABRICANTE_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_FABRICANTE_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_UNIDAD_MEDIDA_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_UNIDAD_MEDIDA_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CATALOGO_GRUPO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CATALOGO_GRUPO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CATALOGO_FAMILIA_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CATALOGO_FAMILIA_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CATALOGO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CATALOGO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CATALOGO_UNM_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CATALOGO_UNM_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

      public bool IMPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_CATALOGO_OPERACION_VENTA_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //exportar compras 


        public DataSet EXPORTAR_GNL_ENTIDAD_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_GNL_ENTIDAD_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet EXPORTAR_GNL_ENTIDAD_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_GNL_ENTIDAD_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool IMPORTAR_GNL_ENTIDAD_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_GNL_ENTIDAD_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_CONTABLE_CAB_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_ADM_CONTABLE_DET_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_ADM_COMPRA_LOTE_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_CONTABLE_DET_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_COMPRAS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //EXPORTAR MOVIMIENTOS QEU SE HICIERON DESDE INVENTARIO

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_INVENTARIO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_INVENTARIO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_INVENTARIO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //exportar traspasos

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //importar traspasos
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_TRASPASO_INGRESO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_TRASPASO_INGRESO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_TRASPASO_INGRESO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DOC_REFERENCIA_TRASPASO_INGRESO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public DataSet EXPORTAR_VEN_LISTA_PRECIO_CAB_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_VEN_LISTA_PRECIO_CAB_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_VEN_LISTA_PRECIO_DET_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_VEN_LISTA_PRECIO_DET_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_VEN_LISTA_PRECIO_CAB_PRECIO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_VEN_LISTA_PRECIO_CAB_PRECIO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_VEN_LISTA_PRECIO_DET_PRECIO_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_VEN_LISTA_PRECIO_DET_PRECIO_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBO_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBO_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(Entidad_Exportar pEntidad)
        {
            try
            {
                return Dato.EXPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(pEntidad);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBOS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_CAB_VENTAS_AMBOS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_DET_VENTAS_AMBOS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(DataTable dt)
        {
            try
            {
                Dato.IMPORTAR_CTB_MOVIMIENTO_INVENTARIO_LOTE_VENTAS_AMBOS_XML(dt);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
