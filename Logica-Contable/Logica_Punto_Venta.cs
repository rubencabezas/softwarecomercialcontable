﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
   public class Logica_Punto_Venta
    {
        private Datos_Punto_Venta CapaDato = new Datos_Punto_Venta();

        public bool Insertar(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Punto_Venta> Listar(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Punto_Venta> Listar_Punto_Venta_Almacen(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Punto_Venta_Almacen(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public List<Entidad_Punto_Venta> Buscar_Pdv_por_PC(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Pdv_por_PC(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

       public List<Entidad_Punto_Venta> Traer_Fecha(Entidad_Punto_Venta Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Fecha(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
