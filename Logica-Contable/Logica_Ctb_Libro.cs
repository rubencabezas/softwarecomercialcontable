﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Contable
{
  public  class Logica_Ctb_Libro
    {
        
        static void Main()
        {
        }

        private Datos_Ctb_Libros CapaDato = new Datos_Ctb_Libros();

        public bool Insertar(Entidad_Ctb_Libro Cls_Enti)
        {
            try
            {
            
                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Ctb_Libro Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Ctb_Libro Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public object Buscar_Documentos(Entidad_Ctb_Libro Cls_Enti)
        //{
        //    try
        //    {
        //        return CapaDato.Listar(Cls_Enti);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        public List<Entidad_Ctb_Libro> Listar(Entidad_Ctb_Libro Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
