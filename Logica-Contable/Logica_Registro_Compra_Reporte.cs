﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Logica_Registro_Compra_Reporte
    {

        private Datos_Registro_Compra_Reporte CapaDato = new Datos_Registro_Compra_Reporte();
        public List<Entidad_Registro_Compra_Reporte> Listar(Entidad_Registro_Compra_Reporte Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_reporte(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Registro_Compra_Reporte> Ple_V5(Entidad_Registro_Compra_Reporte Cls_Enti)
        {
            try
            {
                return CapaDato.Ple_V5(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
