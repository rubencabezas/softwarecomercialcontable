﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Logica_Cedula_Determinacion_IGV
    {
        private Datos_Cedula_Determinacion_IGV CapaDato = new Datos_Cedula_Determinacion_IGV();

        public List<Entidad_Cedula_Determinacion_IGV> Listar(Entidad_Cedula_Determinacion_IGV Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
