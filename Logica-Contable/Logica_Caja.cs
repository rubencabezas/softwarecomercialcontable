﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
   public class Logica_Caja
    {

        private Datos_Caja CapaDato = new Datos_Caja();

        public bool Insertar(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Listar(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Listar_Caja_Aperturado(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Caja_Aperturado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Listar_Caja_Aperturado_DET(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Caja_Aperturado_DET(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insertar_Aperturando_Caja(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Aperturando_Caja(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insertar_Apertura_Det(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Apertura_Det(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
  
        public bool Modificar_Saldo_Inicial(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_Saldo_Inicial(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }    
        
        public bool Actualizar_Cierre_Total_caja(Entidad_Caja Cls_Enti)
        {
            try
            {

                CapaDato.Actualizar_Cierre_Total_caja(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Ventas_Contado_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Ventas_Contado_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Ventas_Credito_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Ventas_Credito_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Ventas_Contado_Inventario_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Ventas_Contado_Inventario_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

      public List<Entidad_Caja> Ventas_Credito_Inventario_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Ventas_Credito_Inventario_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Listar_Cortes_Totales(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Cortes_Totales(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Caja> Buscar_Ventas_Departamento_CierreTotal(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Ventas_Departamento_CierreTotal(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
   public List<Entidad_Caja> Buscar_Ventas_Departamento_Corte_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Ventas_Departamento_Corte_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Ventas_Contado_Credito_Corte_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Ventas_Contado_Credito_Corte_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Ventas_Contado_Credito_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Ventas_Contado_Credito_Cierre_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Buscar_Ventas_Sin_Documentos_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Ventas_Sin_Documentos_Cierre_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

     public List<Entidad_Caja> Buscar_Ventas_Sin_Documentos_Corte_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Ventas_Sin_Documentos_Corte_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Saldo_Inicial_Apertura_Caja(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Saldo_Inicial_Apertura_Caja(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja> Buscar_Otros_Ingresos_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Ingresos_Cierre_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Caja> Buscar_Otros_Ingresos_Corte_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Ingresos_Corte_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Otros_Ingresos_Corte_Total_Before(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Ingresos_Corte_Total_Before(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }   
        public List<Entidad_Caja> Buscar_Otros_Ingresos_Corte_Total_Todo(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Ingresos_Corte_Total_Todo(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Otros_Egresos_Cierre_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Egresos_Cierre_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Otros_Egresos_Corte_Total(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Egresos_Corte_Total(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Otros_Egresos_Corte_Total_Before(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Egresos_Corte_Total_Before(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Buscar_Otros_Egresos_Corte_Total_Todo(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Otros_Egresos_Corte_Total_Todo(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Caja> Listado_de_Todas_las_Cajas(Entidad_Caja Cls_Enti)
        {
            try
            {
                return CapaDato.Listado_de_Todas_las_Cajas(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
