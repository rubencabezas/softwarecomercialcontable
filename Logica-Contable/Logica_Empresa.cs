﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
  public  class Logica_Empresa
    {


        private Datos_Empresa CapaDato = new Datos_Empresa();

        public bool Insertar(Entidad_Empresa Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Empresa Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Empresa Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

       
        public List<Entidad_Empresa> Listar(Entidad_Empresa Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Empresa> Listar_Emp(Entidad_Empresa Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Emp(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Empresa> Traer_Anio_Actual_Server(Entidad_Empresa Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Anio_Actual_Server(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
