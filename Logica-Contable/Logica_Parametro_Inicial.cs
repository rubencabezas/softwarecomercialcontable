﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Logica_Parametro_Inicial
    {

        private Datos_Parametro_Inicial CapaDato = new Datos_Parametro_Inicial();
        public bool Insertar(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Parametro_Inicial> Listar(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_Compra(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Libro_Compra (Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_Diario(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Libro_Diario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_Inventario(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Libro_Inventario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_Venta(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Libro_Venta(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Parametro_Inicial> Traer_Libro_CajaBanco(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Libro_CajaBanco(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Parametro_Inicial> Traer_Libro_Rendicion_Caja_Chica(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Libro_RendicionesCajaChica(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Parametro_Inicial> Traer_Cuenta_Caja(Entidad_Parametro_Inicial Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Cuenta_Caja(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
