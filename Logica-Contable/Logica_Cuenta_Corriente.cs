﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
 public   class Logica_Cuenta_Corriente
    {

        private Datos_Cuenta_Corriente CapaDato = new Datos_Cuenta_Corriente();

        public bool Insertar(Entidad_Cuenta_Corriente Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Cuenta_Corriente Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public bool Eliminar(Entidad_Entidad Cls_Enti)
        //{
        //    try
        //    {

        //        CapaDato.Eliminar(Cls_Enti);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Cuenta_Corriente> Listar(Entidad_Cuenta_Corriente Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
