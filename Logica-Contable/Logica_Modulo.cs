﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
 public   class Logica_Modulo
    {


        private Datos_Modulos CapaDato = new Datos_Modulos();

        public List<Entidad_Modulo> Listar(Entidad_Modulo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Modulo> Listar_Todos_Modulos(Entidad_Modulo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Todos_Modulos(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public List<Entidad_Modulo> Contar_Opcion(Entidad_Modulo Cls_Enti)
        {
            try
            {
                return CapaDato.Contar_Opciones_Usuario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
      public List<Entidad_Modulo> Contar_Empre(Entidad_Modulo Cls_Enti)
        {
            try
            {
                return CapaDato.Contar_Empresas_Usuario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

               public List<Entidad_Modulo> Opciones_Asignadas_X_Usuario(Entidad_Modulo Cls_Enti)
        {
            try
            {
                return CapaDato.Opciones_Asignadas_X_Usuario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Modulo> Empresas_Asignadas_X_Usuario(Entidad_Modulo Cls_Enti)
        {
            try
            {
                return CapaDato.Empresas_Asignadas_X_Usuario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insertar(Entidad_Modulo Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Entidad_Modulo Cls_Enti)
        {
            try
            {
                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
