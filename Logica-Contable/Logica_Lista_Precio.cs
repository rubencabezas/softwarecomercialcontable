﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comercial
{
  public  class Logica_Lista_Precio
    {

        private Datos_Lista_Precio CapaDato = new Datos_Lista_Precio();

        public bool Insertar(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Verificar_producto_Fecha_Modalidad(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {

                CapaDato.Verificar_producto_Fecha_Modalidad(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Lista_Precio> Listar(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Lista_Precio> Listar_Producto_Con_Precio(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Producto_Con_Precio(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
      public List<Entidad_Lista_Precio> Listar_Producto_Con_Precio_Det(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Producto_Con_Precio_Det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Lista_Precio> Traer_Precio_Procuto(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Precio_Procuto(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
   public List<Entidad_Lista_Precio> Traer_Precio_Procuto_Presentacion(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Precio_Procuto_Presentacion(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }   
        
        public List<Entidad_Lista_Precio> Traer_Precio_Procuto_Presentacion_Por_Unm(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Precio_Procuto_Presentacion_Por_Unm(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public Entidad_Lista_Precio Valida_Codigo_Autorizacion(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Valida_Codigo_Autorizacion(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Lista_Precio> Buscar_Config_Precios_Posteriores(Entidad_Lista_Precio Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Config_Precios_Posteriores(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
