﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
  public  class Logica_Operaciones
    {
        private Datos_Operaciones CapaDato = new Datos_Operaciones();

        public List<Entidad_Operaciones> Listar_Operaciones_Venta(Entidad_Operaciones Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Operaciones_Venta(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Operaciones> Traer_Operacion_venta_Por_Producto(Entidad_Operaciones Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Operacion_venta_Por_Producto(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
