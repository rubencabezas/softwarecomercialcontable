﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
 public   class Logica_Plan_Empresarial
    {

        private Datos_Plan_Empresarial CapaDato = new Datos_Plan_Empresarial();

        public bool Insertar(Entidad_Plan_Empresarial Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Entidad_Plan_Empresarial Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar(Entidad_Plan_Empresarial Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Entidad_Plan_Empresarial TraerDatos(Entidad_Plan_Empresarial Ent_Cta)
        {
            try
            {
                return CapaDato.TraerDatos(Ent_Cta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Plan_Empresarial> Busqueda(Entidad_Plan_Empresarial Cls_Enti)
        {
            try
            {
                return CapaDato.Busqueda(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Plan_Empresarial> Busqueda_Cta_Por_Proceso(Entidad_Plan_Empresarial Cls_Enti)
        {
            try
            {
                return CapaDato.Busqueda_Cta_Por_Proceso(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Plan_Empresarial> Listar_Destino(Entidad_Plan_Empresarial Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Destino(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //public bool Modificar(Entidad_Plan_Empresarial Cls_Enti)
        //{
        //    try
        //    {

        //        CapaDato.Modificar(Cls_Enti);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}



    }
}
