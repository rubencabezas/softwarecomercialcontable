﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Logica_Series_Tesoreria
    {
        private Datos_Series_Tesoreria CapaDato = new Datos_Series_Tesoreria();

        public bool Insertar(Entidad_Series_Tesorerira Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Series_Tesorerira Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public bool Eliminar(Entidad_Cargo Cls_Enti)
        //{
        //    try
        //    {

        //        CapaDato.Eliminar(Cls_Enti);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Series_Tesorerira> Listar(Entidad_Series_Tesorerira Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Series_Tesorerira> Mostrar_Series_Caja(Entidad_Series_Tesorerira Cls_Enti)
        {
            try
            {
                return CapaDato.Mostrar_Series_Caja(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
