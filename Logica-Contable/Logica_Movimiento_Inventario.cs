﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Logica_Movimiento_Inventario
    {


        private Datos_Movimiento_Inventario CapaDato = new Datos_Movimiento_Inventario();

        public bool Insertar(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public bool Modificar(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Listar(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Listar_Det(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Buscar_Doc_Dar_Ingreso(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Doc_Dar_Ingreso(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Movimiento_Inventario> Buscar_Doc_Dar_Ingreso_DET(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Doc_Dar_Ingreso_DET(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Buscar_Doc_Referenciados(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Doc_Referenciados(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //
        //

        public List<Entidad_Movimiento_Inventario> Buscar_Detalles_Orden(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Detalles_Orden(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Buscar_Detalles_Inventario(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Detalles_Inventario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Buscar_Documentos_Referenciados(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Documentos_Referenciados(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Inventario> Listar_Lotes_Inventario(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Lotes_Inventario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Buscar_Detalles_Inventario_Lotes(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Buscar_Detalles_Inventario_Lotes(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar_Adm_Ingresos_Inv_Libera_Trabsaferencia(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                CapaDato.Eliminar_Adm_Ingresos_Inv_Libera_Trabsaferencia(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar_Adm_Ingresos_Inv(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                CapaDato.Eliminar_Adm_Ingresos_Inv(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Insertar_Ventas_INV(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Ventas_INV(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Listar_Ventas_Sin_Ticket(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Ventas_Sin_Ticket(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public bool Anular_Adm_Ingresos_Inv(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                CapaDato.Anular_Adm_Ingresos_Inv(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Inventario> Reporte_Kardex(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Reporte_Kardex(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Inventario> Reporte_Kardex_Resumen(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Reporte_Kardex_Resumen(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Movimiento_Inventario> Reporte_Kardex_Lote_Resumen(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Reporte_Kardex_Lote_Resumen(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Inventario> Traer_Saldo_Inicial(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Saldo_Inicial(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Inventario> Traer_Saldo_Inicial_Lotes(Entidad_Movimiento_Inventario Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Saldo_Inicial_Lotes(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }








    }
}
