﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Logica_Catalogo
    {

        private Datos_Catalogo CapaDato = new Datos_Catalogo();

        public bool Insertar(Entidad_Catalogo Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Catalogo Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Catalogo Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Listar(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Cuenta_Con_Ingresos_Egresos_Inventario(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Cuenta_Con_Ingresos_Egresos_Inventario(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Listar_Operacion_venta(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Operacion_venta(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Listar_Cuenta_Catalogo(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Cuenta_Catalogo(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Listar_especificaciones(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_especificaciones(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Listar_Cat_Unm(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Cat_Unm(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Catalogo> Consultar_Stock(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Consultar_Stock(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Catalogo> Consultar_Stock_Pro_Codigo_Barra(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Consultar_Stock_Pro_Codigo_Barra(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Consultar_Stock_Lote(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Consultar_Stock_Lote(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Catalogo> Consultar_Servicios(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Consultar_Servicios(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Consultar_Producto_Y_Sus_Presentaciones(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Consultar_Producto_Y_Sus_Presentaciones(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Catalogo> Consultar_Stock_Producto_Lote(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Consultar_Stock_Producto_Lote(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Listar_Presentacion_Productos(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Presentacion_Productos(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Catalogo> Comprobar_Producto_ya_Registrado(Entidad_Catalogo Cls_Enti)
        {
            try
            {
                return CapaDato.Comprobar_Producto_ya_Registrado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
