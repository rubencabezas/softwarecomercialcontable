﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
    public class Logica_Unidad_Medida
    {



        private Datos_Unidad_Medida CapaDato = new Datos_Unidad_Medida();

        public bool Insertar(Entidad_Unidad_Medida Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Unidad_Medida Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public bool Eliminar(Entidad_Marca Cls_Enti)
        //{
        //    try
        //    {

        //        CapaDato.Eliminar(Cls_Enti);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Unidad_Medida> Listar(Entidad_Unidad_Medida Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Unidad_Medida> Listar_Unm_Producto(Entidad_Unidad_Medida Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Unm_Producto(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Unidad_Medida> Listar_Cat_Unm(Entidad_Unidad_Medida Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Cat_Unm(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
