﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Logica_Movimiento_Cab
    {

        private Datos_Movimientos_Cab CapaDato = new Datos_Movimientos_Cab();

        public bool Insertar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Eliminar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }    
        public bool Eliminar_Ingresos_Egresos(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Ingresos_Egresos(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } 
        public bool Eliminar_Tesoreria(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Tesoreria(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
 public bool Anular(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Anular(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
 public bool Revertir(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Revertir(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Det_Administrativo(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Det_Administrativo(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Entidad_Movimiento_Cab> Listar_Provision(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Provision(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Doc_Ref(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Doc_Ref(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Movimiento_Cab> Listar_Doc_Ref_Orden(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Doc_Ref_Orden(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Movimiento_Cab> Lista_Documentos_por_Pagar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Lista_Documentos_por_Pagar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //TESORERIA
        public bool Insertar_Tesoreria(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Tesoreria(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar_Tesoreria(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_Tesoreria(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Movimiento_Cab> Listar_Tesoreria(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Tesoreria(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Tesoreria_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Tesoreria_Det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //CObrar
        public List<Entidad_Movimiento_Cab> Lista_Documentos_por_Cobrar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Lista_Documentos_por_Cobrar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //CAJA CHICA
        public bool Insertar_Caja_Chica(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Caja_Chica(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar_Caja_Chica(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                CapaDato.Modificar_Caja_Chica(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Caja_Chica(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Caja_Chica_Cab(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Caja_Chica_Det(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Caja_Chica_Det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Traer_Importes_Rendicion(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Importes_Rendicion(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public List<Entidad_Movimiento_Cab> Listar_Caja_Chica_Estado(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Caja_Chica_Estado(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_Lotes_Compra(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Lotes_Compra(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Movimiento_Cab> Listar_adm(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_adm(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Movimiento_Cab> Listar_adm_ventas(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_adm_ventas(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insertar_adm(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_adm_Compra(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar_adm(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_adm(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar_Adm_Compra(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Adm_Compra(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Anular_Adm_Compra(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Anular_Adm_Compra(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Movimiento_Cab> Listar_adm_Logistica(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_adm_Logistica(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Insertar_adm_Ventas(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_adm_Ventas(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Insertar_ctb_Ventas(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_ctb_Ventas(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public bool Eliminar_Venta(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Venta(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public bool Anular_Venta(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Anular_Venta(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// ///
        /// </summary>
   
        /// <returns></returns>
        public List<Entidad_Movimiento_Cab> Listar_(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    public List<Entidad_Movimiento_Cab> Listar_Det_(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Det_(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Modificar_adm_Ventas(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_adm_Ventas(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }  
        
        public bool Anular_adm_Ventas(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Anular_adm_Ventas(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Anular_adm_Nota_Credito(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Anular_adm_Nota_Credito(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


     public List<Entidad_Movimiento_Cab> Listar_Afectar_comprobante_NC_(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Afectar_comprobante_NC_(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Nota de credito adm
       public bool Insertar_adm_NC(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_adm_NC(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public List<Entidad_Movimiento_Cab> Listar_NC(Entidad_Movimiento_Cab Cls_Enti)
            {
                try
                {
                    return CapaDato.Listar_NC(Cls_Enti);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
       public List<Entidad_Movimiento_Cab> Listar_NC_Det(Entidad_Movimiento_Cab Cls_Enti)
            {
                try
                {
                    return CapaDato.Listar_NC_Det(Cls_Enti);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        public List<Entidad_Movimiento_Cab> Listar_Doc_Afecto_NC_(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Doc_Afecto_NC_(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public List<Entidad_Movimiento_Cab> Listar_adm_Productos_Por_Proveedor(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_adm_Productos_Por_Proveedor(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Entidad_Movimiento_Cab>Listar_Pendientes_Por_Cobrar(Entidad_Movimiento_Cab Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Pendientes_Por_Cobrar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }





    }
}
