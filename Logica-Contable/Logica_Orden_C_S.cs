﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Logica_Orden_C_S
    {
        private Datos_Orden_C_S CapaDato = new Datos_Orden_C_S();

        public bool Insertar(Entidad_Orden_C_S Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Entidad_Orden_C_S Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Orden_C_S> Listar(Entidad_Orden_C_S Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

       public List<Entidad_Orden_C_S> Listar_Det(Entidad_Orden_C_S Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public List<Entidad_Orden_C_S> Listar_Req(Entidad_Orden_C_S Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Req(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
