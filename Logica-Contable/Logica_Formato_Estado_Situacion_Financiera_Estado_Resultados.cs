﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Logica_Formato_Estado_Situacion_Financiera_Estado_Resultados
    {
        private Datos_Formato_Estado_Situacion_Financiera_Estado_Resultados CapaDato = new Datos_Formato_Estado_Situacion_Financiera_Estado_Resultados();

        public List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados> Listar(Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados> Reporte_Situacion_Financiera_SMV(Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Cls_Enti)
        {
            try
            {
                return CapaDato.Reporte_Situacion_Financiera_SMV(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Insertar(Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Eliminar(Entidad_Formato_Estado_Situacion_Financiera_Estado_Resultados Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
