﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
 public   class Logica_Caja_Chica_Config
    {
        private Datos_Caja_Chica_Config CapaDato = new Datos_Caja_Chica_Config();

        public bool Insertar(Entidad_Caja_Chica_Config Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Caja_Chica_Config Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar(Entidad_Caja_Chica_Config Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Caja_Chica_Config> Listar(Entidad_Caja_Chica_Config Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
