﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contable
{
   public class Logica_Analisis_Contable
    {

        private Datos_Analisis_Contable CapaDato = new Datos_Analisis_Contable();

        public bool Insertar(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Eliminar(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Analisis_Contable> Listar(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Analisis_Contable> Listar_det(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_det(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Analisis_Contable> Listar_Analisis(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Analisis(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Analisis_Contable> Traer_Analisis_Defecto(Entidad_Analisis_Contable Cls_Enti)
        {
            try
            {
                return CapaDato.Traer_Analisis_Defecto(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
