﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Logica_Tipo_Especificacion
    {

        private Datos_Tipo_Especificacion CapaDato = new Datos_Tipo_Especificacion();

        public bool Insertar(Entidad_Tipo_Especificacion Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Tipo_Especificacion Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public bool Eliminar(Entidad_Familia Cls_Enti)
        //{
        //    try
        //    {

        //        CapaDato.Eliminar(Cls_Enti);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}


        public List<Entidad_Tipo_Especificacion> Listar(Entidad_Tipo_Especificacion Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
