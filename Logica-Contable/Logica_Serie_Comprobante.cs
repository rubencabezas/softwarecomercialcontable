﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comercial
{
 public   class Logica_Serie_Comprobante
    {
        private Datos_Serie_Comprobante CapaDato = new Datos_Serie_Comprobante();

        public bool Insertar(Entidad_Serie_Comprobante Cls_Enti)
        {
            try
            {

                CapaDato.Insertar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar(Entidad_Serie_Comprobante Cls_Enti)
        {
            try
            {

                CapaDato.Modificar(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Dar_Baja(Entidad_Serie_Comprobante Cls_Enti)
        {
            try
            {

                CapaDato.Dar_Baja(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Serie_Comprobante> Listar(Entidad_Serie_Comprobante Cls_Enti)
        {
            try
            {
                return CapaDato.Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Serie_Comprobante> Listar_principal(Entidad_Serie_Comprobante Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_principal(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
