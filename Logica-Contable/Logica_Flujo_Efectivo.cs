﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contable
{
   public class Logica_Flujo_Efectivo
    {
        public Datos_Flujo_Efectivo CapaDato = new Datos_Flujo_Efectivo();
        public List<Entidad_Flujo_Efectivo> Patrimonio_Neto_Listar(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {
                return CapaDato.Patrimonio_Neto_Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Entidad_Flujo_Efectivo> Flujo_Efectivo_Listar(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {
                return CapaDato.Flujo_Efectivo_Listar(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Entidad_Flujo_Efectivo> Patrimonio_Neto_Reporte(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {
                return CapaDato.Patrimonio_Neto_Reporte(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       public List<Entidad_Flujo_Efectivo> Flujo_Efectivo_Reporte(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {
                return CapaDato.Flujo_Efectivo_Reporte(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //PLANTILLA FLUJO EFECTIVO
        public bool Insertar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Plantilla_Flujo_Efectivo(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_Plantilla_Flujo_Efectivo(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Plantilla_Flujo_Efectivo(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<Entidad_Flujo_Efectivo> Listar_Plantilla_Flujo_Efectivo(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {
                return CapaDato.Listar_Plantilla_Flujo_Efectivo(Cls_Enti);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public bool Insertar_Patrimonio_Neto_SMV(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {

                CapaDato.Insertar_Patrimonio_Neto_SMV(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Modificar_Patrimonio_Neto_SMV(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {

                CapaDato.Modificar_Patrimonio_Neto_SMV(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public bool Eliminar_Patrimonio_Neto_SMV(Entidad_Flujo_Efectivo Cls_Enti)
        {
            try
            {

                CapaDato.Eliminar_Patrimonio_Neto_SMV(Cls_Enti);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
